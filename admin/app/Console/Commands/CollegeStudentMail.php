<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\CronJobController;
use Illuminate\Console\Command;

class CollegeStudentMail extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'college_student:mail';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CronJobController $cronJobController) {
		parent::__construct();
		$this->cronJobController = $cronJobController;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->cronJobController->collegeStudentsSendMail();
	}
}
