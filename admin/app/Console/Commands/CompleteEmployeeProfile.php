<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\CronJobController;
use Illuminate\Console\Command;

class CompleteEmployeeProfile extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'complte_employee:profile';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CronJobController $cronJobController) {
		parent::__construct();
		$this->cronJobController = $cronJobController;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->cronJobController->employeeCompletedProfileSendMail();
	}
}
