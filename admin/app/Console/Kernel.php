<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		Commands\CollegeStudentMail::class,
		Commands\CompleteEmployeeProfile::class,
		Commands\CompleteEmployerProfile::class,
		Commands\UncompleteEmployeeProfile::class,
		Commands\UncompleteEmployerProfile::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule) {
		$schedule->command('college_student:mail')->dailyAt('06:00');
		//$schedule->command('complte_employee:profile')->dailyAt('04:00');
		$schedule->command('complete_employer:profile')->dailyAt('06:00');
		//$schedule->command('uncomplte_employee:profile')->dailyAt('04:00');
		$schedule->command('uncomplete_employer:profile')->dailyAt('06:00');

		// $schedule->command('inspire')
		//          ->hourly();
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
