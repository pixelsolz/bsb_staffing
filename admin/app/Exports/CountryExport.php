<?php

namespace App\Exports;

use App\Models\CountryMst;
use Maatwebsite\Excel\Concerns\FromCollection;

class CountryExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return CountryMst::all();
    }
}
