<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportEmployees implements FromView {
	/**
	 * @return \Illuminate\Support\Collection
	 */

	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('export.employee', [
			'employees' => User::whereIn('_id', $this->ids)->get(),
		]);
	}
}