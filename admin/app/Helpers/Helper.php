<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Storage;
use Image;

class Helper {

	public static function uploadFile($file, $folder_name) {
		$filename = '';
		$uploadpath = $file->store($folder_name, 's3');
		$extension = $file->getClientOriginalExtension();
		$uploadres = Storage::disk('s3')->setVisibility($uploadpath, 'public');
		if ($uploadres) {
			$filename = str_replace($folder_name . '/', '', $uploadpath);
		}

		if (in_array(strtolower($file->extension()), ['jpeg', 'jpg', 'png', 'gif']) && !empty($filename)) {
			$image_resize = Image::make($file->getRealPath());
			$image_resize->resize(350, 300, function ($constraint) {
				$constraint->aspectRatio();
			});
			$thumb_resource = $image_resize->stream();
			$thumbPath = $folder_name . '/thumb/' . $filename;
			Storage::disk('s3')->put($thumbPath, $thumb_resource->__toString(), 'public');
		}
		return $filename;
	}

}