<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AnnoucementTemplate;
use App\Models\Role;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class AnnoucementTemplateController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$announce_temp = AnnoucementTemplate::all();
		if ($request->ajax()) {
			return response()->json(['status' => 200, 'announce_temps' => $announce_temp]);
		} else {

		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = "Annoucement massage Create";
		$allRole = Role::all();
		return view('annoucement-template.create', compact('title', 'allRole'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'temp_name.required' => 'The subject field required.',
			'temp_content.required' => 'The message field required.',
			'entity_id.required' => 'The entity field required.',
		];

		$validator = Validator::make($request->all(), [
			'temp_name' => 'required',
			'temp_content' => 'required',
			'entity_id' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$annoucement = AnnoucementTemplate::create($allRequest + ['status' => 1, 'created_by' => Auth::guard('admin')->user()->_id]);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.announcement-template.list-all');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = "Annoucement massage edit";
		$allRole = Role::all();
		$annoucement = AnnoucementTemplate::find($id);
		return view('annoucement-template.edit', compact('title', 'annoucement', 'allRole'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'temp_name.required' => 'The subject field required.',
			'temp_content.required' => 'The message field required.',
			'entity_id.required' => 'The entity field required.',
		];

		$validator = Validator::make($request->all(), [
			'temp_name' => 'required',
			'temp_content' => 'required',
			'entity_id' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$annoucement_template = AnnoucementTemplate::find($id);
			$annoucement_template->fill($alldata);
			$annoucement_template->save();
			Session::flash('msg', 'Sucessfuly Updated');
			return redirect()->route('admin.announcement-template.list-all');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
	public function getAllAnnoucement() {
		$title = "Annoucement Template List";
		$announce_msg_temps = AnnoucementTemplate::paginate(25)->appends(request()->query());
		$allRole = Role::all();
		return view('annoucement-template.index', compact('title', 'announce_msg_temps', 'allRole'));

	}
}
