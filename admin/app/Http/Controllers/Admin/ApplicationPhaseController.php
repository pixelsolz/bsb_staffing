<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ApplicationPhaseMst;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class ApplicationPhaseController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

		$title = 'Application Phase';
		//return CityMst::all();
		$query = $request->input('q');
		if ($query) {
			$phases = ApplicationPhaseMst::where('phase_name', 'LIKE', "%{$query}%")->paginate(25);

		} else {
			$phases = ApplicationPhaseMst::paginate(25);
		}

		return view('application-phase.index', compact('title', 'phases'));
		// return view('city-manage.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Application Phase Create';
		return view('application-phase.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'phase_name.required' => 'The phase name field required.',
		];

		$validator = Validator::make($request->all(), [
			'phase_name' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$city = ApplicationPhaseMst::create($allRequest);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.application-phase.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$title = 'Application Edit';
		$phase = ApplicationPhaseMst::find($id);
		return view('application-phase.edit', compact('title', 'phase'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//echo $id;die;
		//dd($request->all());
		$message = [
			'phase_name.required' => 'The phase name field required.',
		];

		$validator = Validator::make($request->all(), [
			'phase_name' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$phase = ApplicationPhaseMst::find($id);
			$phase->fill($alldata);
			$phase->save();
			Session::flash('msg', 'Sucessfuly Updated');
			return redirect()->route('admin.application-phase.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
