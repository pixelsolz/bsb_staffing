<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttributeMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class AttributeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Attribute List';
		$attributes = AttributeMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('name', 'like', '%' . $request->q . '%');
			}
		})->orderBy('_id', 'desc')->paginate(25)->appends(request()->query());
		return view('attribute-manage.index', compact('title', 'attributes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Attribute Create';
		return view('attribute-manage.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'attr_type.required' => 'The attribute type field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'attr_type' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			AttributeMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->_id, 'status' => 1]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.attribute-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Attribute Edit';
		$attribute = AttributeMst::find($id);
		return view('attribute-manage.edit', compact('title', 'attribute'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'attr_type.required' => 'The attribute type field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'attr_type' => 'required',
			'status' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$attribute = AttributeMst::find($id);
			$attribute->fill($alldata);
			$attribute->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.attribute-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$attribute = AttributeMst::find($id);
		$attribute->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
