<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\LanguageMst;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class AuthController extends Controller {
	public function __construct() {
		$this->middleware('guest:admin')->except('logout');
	}

	public function showLogin() {

		$title = 'Job Portal/admin-login';
		return view('auth.login', compact('title'));
	}

	public function storeLogin(Request $request) {
		if (!Auth::guard('admin')->attempt(['email' => $request->email,
			'password' => $request->password])) {
			Session::flash('msg', ['status' => 'danger', 'msgs' => "Email and password doesn't match!"]);
			return back();
		}
		Session::flash('msg', ['status' => 'success', 'msgs' => "Welcome to Dashboard " . Auth::guard('admin')->user()->name]);
		if (Auth::guard('admin')->user()->is_admin == 1) {
			return redirect()->route('admin.dasboard');
		} elseif (Auth::guard('admin')->user()->roles->first()->slug == 'internal_employee') {
			return redirect()->route('internal-emp.dashboard');
		}

	}

	public function logout() {
		Auth::guard('admin')->logout();
		return redirect()->route('admin.login');
	}

	public function adminCreate() {

		//$user = User::where('user_name', 'superadmin')->first();
		$user = User::create(['user_name' => 'superadmin', 'email' => 'superadmin@gmail.com', 'password' => bcrypt('superadmin@123#'), 'is_admin' => 1, 'is_super_admin' => 1]);
		$role = Role::where('slug', 'superadmin')->first();
		RoleUser::create(['user_id' => $user->_id, 'role_id' => $role->_id]);
		$user->roles()->attach($role->_id);
		return 'ok';
	}

	public function insertAny() {
		//cource data
		/*$data = [
			['name' => 'Bengali', 'status' => 1],
			['name' => 'English', 'status' => 1],
			['name' => 'History', 'status' => 1],
			['name' => 'Math', 'status' => 1],

		];
		foreach ($data as $value) {
			Course::create($value);
		}*/
		//all other data
		/*$data = [
			['name' => '15', 'entity'=>'job_posted_to_be','orders'=>1,'status' => 1],
			['name' => '30', 'entity'=>'job_posted_to_be','orders'=>2,'status' => 1],
			['name' => '3', 'entity'=>'job_posted_to_be','orders'=>3,'status' => 1],
			['name' => 'International', 'entity'=>'emplyment_for','orders'=>1,'status' => 1],
			['name' => 'Domestic/International', 'entity'=>'emplyment_for','orders'=>2,'status' => 1],
			['name' => '0', 'entity'=>'experience','orders'=>1,'status' => 1],
			['name' => '>1', 'entity'=>'experience','orders'=>2,'status' => 1],
			['name' => '1', 'entity'=>'experience','orders'=>3,'status' => 1],
			['name' => '2', 'entity'=>'experience','orders'=>4,'status' => 1],
			['name' => '3', 'entity'=>'experience','orders'=>5,'status' => 1],
			['name' => '4', 'entity'=>'experience','orders'=>6,'status' => 1],
			['name' => '6', 'entity'=>'experience','orders'=>7,'status' => 1],
			['name' => '7', 'entity'=>'experience','orders'=>8,'status' => 1],
			['name' => '8', 'entity'=>'experience','orders'=>9,'status' => 1],
			['name' => '9', 'entity'=>'experience','orders'=>10,'status' => 1],
			['name' => '10', 'entity'=>'experience','orders'=>11,'status' => 1],
			['name' => 'Full Time', 'entity'=>'employement_type','orders'=>1,'status' => 1],
			['name' => 'part time', 'entity'=>'employement_type','orders'=>2,'status' => 1],
			['name' => 'International Candidate', 'entity'=>'emplyment_for','orders'=>1,'status' => 1],
			['name' => 'Domestic Candidate', 'entity'=>'emplyment_for','orders'=>2,'status' => 1],
			['name' => 'Domestic/International Candidate', 'entity'=>'emplyment_for','orders'=>3,'status' => 1],
			['name' => 'Walkin', 'entity'=>'interview_type','orders'=>1,'status' => 1],
			['name' => 'Spot', 'entity'=>'interview_type','orders'=>2,'status' => 1],
			['name' => 'Bulk hiring', 'entity'=>'interview_type','orders'=>3,'status' => 1],
			['name' => 'USD', 'entity'=>'currency','orders'=>1,'status' => 1],
			['name' => 'Rupees', 'entity'=>'currency','orders'=>2,'status' => 1],
		];
		foreach ($data as $value) {
			AllOtherMasterMst::create($value);
		}*/

		//AttributeMst
		/*$data = [
			['name' => 'Top Industries', 'attr_type'=>1, 'status' => 1],
			['name' => 'Other Industries', 'attr_type'=>1, 'status' => 1],
			['name' => "O' Level, 'A' Level or Below", 'attr_type'=>2, 'status' => 1],
			['name' => "Diploma / Vocational Course", 'attr_type'=>2, 'status' => 1],
			['name' => 'Graduation', 'attr_type'=>2 ,'status' => 1],
			['name' => 'PG or Equivalent', 'attr_type'=>3 ,'status' => 1],
			['name' => 'Phd / Mphil or Equivalent', 'attr_type'=>3, 'status' => 1],
			['name' => 'Hotel', 'attr_type'=>1 ,'status' => 1],

		];
		foreach ($data as $value) {
			AttributeMst::create($value);
		}*/

		//IndustryMst
		/*$data = [
			['name' => 'Hospitality(hotel)', 'attribute_id'=>1,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'Retail', 'attribute_id'=>1,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'P.Cruise', 'attribute_id'=>1,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'Restaurant', 'attribute_id'=>1,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'Catering', 'attribute_id'=>1,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'Evt Managent', 'attribute_id'=>1,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'Advertisment', 'attribute_id'=>2,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
			['name' => 'Agriculture', 'attribute_id'=>2,'service_id'=>'', 'created_by' => '5d271d3735b28038420409d2','updated_by'=>'5d271d3735b28038420409d2'],
		];
		foreach ($data as $value) {
			IndustryMst::create($value);
		}*/

		//DegreeMst
		/*$data = [
			['name' => "'O' Level", 'attribute_id'=>'5d28616b35b2802f32635c74', 'status' => 1],
			['name' => "'A' Level", 'attribute_id'=>'5d28616b35b2802f32635c74', 'status' => 1],
			['name' => "Below 'O' Level and 'A' Level", 'attribute_id'=>'5d28616b35b2802f32635c74', 'status' => 1],
			['name' => 'Vocational Course', 'attribute_id'=>'5d28616b35b2802f32635c75', 'status' => 1],
			['name' => 'Diploma', 'attribute_id'=>'5d28616b35b2802f32635c75', 'status' => 1],
			['name' => 'B.A', 'attribute_id'=>'5d28616b35b2802f32635c76', 'status' => 1],
			['name' => 'B.Arch', 'attribute_id'=>'5d28616b35b2802f32635c76', 'status' => 1],
			['name' => 'B.B.A / B.M.S', 'attribute_id'=>'5d28616b35b2802f32635c76', 'status' => 1],
			['name' => 'B.C.A', 'attribute_id'=>'5d28616b35b2802f32635c76', 'status' => 1],
			['name' => 'B.Com', 'attribute_id'=>'5d28616b35b2802f32635c76', 'status' => 1],
			['name' => 'B.Sc', 'attribute_id'=>'5d28616b35b2802f32635c76', 'status' => 1],
			['name' => 'C.A', 'attribute_id'=>'5d28616b35b2802f32635c77', 'status' => 1],
			['name' => 'C.F.A', 'attribute_id'=>'5d28616b35b2802f32635c77', 'status' => 1],
			['name' => 'M.A', 'attribute_id'=>'5d28616b35b2802f32635c77', 'status' => 1],
			['name' => 'M.C.A', 'attribute_id'=>'5d28616b35b2802f32635c77', 'status' => 1],
			['name' => 'Phd/ Doctorate', 'attribute_id'=>'5d28616b35b2802f32635c78', 'status' => 1],

		];
		foreach ($data as $value) {
			DegreeMst::create($value);
		}*/
		//LANGUAGRMST
		$data = [
			['language_name' => 'English', 'created_by' => '5d271d3735b28038420409d2', 'updated_by' => '5d271d3735b28038420409d2'],
			['language_name' => 'Bengali', 'created_by' => '5d271d3735b28038420409d2', 'updated_by' => '5d271d3735b28038420409d2'],
			['language_name' => 'Hindi', 'created_by' => '5d271d3735b28038420409d2', 'updated_by' => '5d271d3735b28038420409d2'],
			['language_name' => 'French', 'created_by' => '5d271d3735b28038420409d2', 'updated_by' => '5d271d3735b28038420409d2'],

		];
		foreach ($data as $value) {
			LanguageMst::create($value);
		}
		return 'ok';
	}
}
