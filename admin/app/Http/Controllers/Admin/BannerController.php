<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BannerImage;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class BannerController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$title = 'Banner Image List';
		$banner_images = BannerImage::paginate(25)->appends(request()->query());
		return view('banner-image-manage.index', compact('title', 'banner_images'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Banner Image Add';
		return view('banner-image-manage.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$message = [
			'banner_image.required' => 'The banner Image field required.',
		];

		$validator = Validator::make($request->all(), [
			'banner_image' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		//dd($request->all());
		try {
			$allRequest = $request->except(['banner_image']);

			$banner_image_name = '';
			if ($request->hasFile('banner_image')) {
				$image = $request->file('banner_image');
				$name = $request->input('title') . time() . '.' . $image->getClientOriginalExtension();
				$destinationPath = public_path('/upload_files/banner_image');
				$imagePath = $destinationPath . "/" . $name;
				$image->move($destinationPath, $name);
				$banner_image_name = $name;
			}
			$banner_image = BannerImage::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'banner_image' => $banner_image_name]);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.banner-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Banner Image Edit';
		$banner_image = BannerImage::find($id);
		return view('banner-image-manage.edit', compact('title', 'banner_image'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'title.required' => 'The title field required.',
		];

		$validator = Validator::make($request->all(), [
			'title' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			$banner_image = BannerImage::find($id);

			$banner_image->fill($request->except('banner_image'));
			$banner_image->save();
			if ($request->hasFile('banner_image')) {
				$image = $request->file('banner_image');
				$name = $request->input('name') . time() . '.' . $image->getClientOriginalExtension();
				$destinationPath = public_path('/upload_files/banner_image');
				$imagePath = $destinationPath . "/" . $name;
				$image->move($destinationPath, $name);
				$banner_image->banner_image = $name;
				$banner_image->save();
			}
			Session::flash('msg', 'Sucessfuly updated');
			return redirect()->route('admin.banner-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
