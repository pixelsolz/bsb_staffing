<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BenefitMst;
use App\Models\Role;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class BenefitController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Benefit List';
		//return CityMst::all();
		$query = $request->input('q');
		if ($query) {
			$benefits = BenefitMst::where('title', 'LIKE', "%{$query}%")
				->orWhere('type', 'LIKE', "%{$query}%")
				->paginate(25)->appends(request()->query());

		} else {
			$benefits = BenefitMst::paginate(25)->appends(request()->query());
		}
		if ($request->ajax()) {
			$benefits = BenefitMst::where('type', $request->type)->get();
			return response()->json(['status' => 200, 'data' => $benefits]);
		}

		return view('benefit-manage.index', compact('title', 'benefits', 'request'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Benefit Create';
		$benefit = BenefitMst::get();
		$allRole = Role::all();
		return view('benefit-manage.create', compact('title', 'benefit', 'allRole'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$message = [
		];

		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'benefit' => 'required',
			'type' => 'required',
			'entity_id' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$benefit = BenefitMst::create($allRequest + ['status' => "1", 'created_by' => Auth::guard('admin')->user()->_id]);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.benefit-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Benefit Edit';
		$befit = BenefitMst::find($id);
		$allRole = Role::all();
		return view('benefit-manage.edit', compact('title', 'befit', 'allRole'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
		];

		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'benefit' => 'required',
			'type' => 'required',
			'entity_id' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$benefit = BenefitMst::find($id);
			$benefit->fill($alldata);
			$benefit->save();
			Session::flash('msg', 'Sucessfuly Updated');
			return redirect()->route('admin.benefit-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$benefit = BenefitMst::find($id);
		$benefit->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
