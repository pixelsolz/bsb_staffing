<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class CityController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

		$title = 'City List';
		//return CityMst::all();
		$query = $request->input('q');
		if ($query) {
			$cities = CityMst::where('name', 'LIKE', "%{$query}%")
				->orWhereHas('country', function ($que) use ($request) {
					$que->where('name', 'like', '%' . $request->input("q") . '%');
				})
				->paginate(25)->appends(request()->query());

		} else {
			$cities = CityMst::paginate(25)->appends(request()->query());
		}

		return view('city-manage.index', compact('title', 'cities'));
		// return view('city-manage.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'City Create';
		$countries = CountryMst::get();
		return view('city-manage.create', compact('title', 'countries'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'name.required' => 'The name field required.',
			'country_code.required' => 'The country name field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'country_code' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$slug = str_slug('jobs-in-' . $request->name, "-");
			$city = CityMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'slug' => $slug]);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.city-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$title = 'City Edit';
		$city = CityMst::find($id);
		$countries = CountryMst::get();
		return view('city-manage.edit', compact('title', 'city', 'countries'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$message = [
			'name.required' => 'The name field required.',
			'country_code.required' => 'The country name field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'country_code' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$city = CityMst::find($id);
			$city->fill($alldata);
			$slug = str_slug('jobs-in-' . $request->name, "-");
			$city->slug = $slug;
			$city->save();
			Session::flash('msg', 'Sucessfuly Updated');
			return redirect()->route('admin.city-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
