<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CmsManage;
use Illuminate\Http\Request;
use Session;
use Validator;

class CmsManageController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$title = 'CMS Manage';
		$cmsManage = CmsManage::orderBy('id', 'desc')->paginate(10);
		return view('cms-manage.index', compact('title', 'cmsManage'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'CMS Manage Create';
		return view('cms-manage.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$message = [
			'title.required' => 'The Title field required.',
			'name.required' => 'The Name field required.',
			'slug.required' => 'The Slug field required.',
			'content.required' => 'The Content field required.',
		];

		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'name' => 'required',
			'slug' => 'required',
			'content' => 'required',
		], $message);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			CmsManage::create($request->all());
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.cms-manage.index');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'CMS Manage Edit';
		$cms_manage = CmsManage::find($id);
		return view('cms-manage.edit', compact('title', 'cms_manage'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'title.required' => 'The Title field required.',
			'name.required' => 'The Name field required.',
			'slug.required' => 'The Slug field required.',
			'content.required' => 'The Content field required.',
		];

		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'name' => 'required',
			'slug' => 'required',
			'content' => 'required',
		], $message);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$cms_manage = CmsManage::find($id);
			$cms_manage->fill($request->all());
			$cms_manage->save();
			Session::flash('msg', 'Sucessfuly updated');
			return redirect()->route('admin.cms-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$cms_manage = CmsManage::find($id);
			$cms_manage->delete();
			return response()->json(['status' => 200, 'status_text' => 'Successfuly deleted']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
}
