<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanySizeMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class CompanySizeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Company Size List';
		$comp_sizes = CompanySizeMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('name', 'like', '%' . $request->q . '%');
			}
		})->orderBy('_id', 'desc')->paginate(25)->appends(request()->query());
		return view('company-size.index', compact('title', 'comp_sizes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Company Size Create';
		return view('company-size.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'company_size.required' => 'The size field required.',
		];

		$validator = Validator::make($request->all(), [
			'company_size' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$comp_size = CompanySizeMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'status' => 1]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.companysize-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Company Size Edit';
		$comp_size = CompanySizeMst::find($id);
		return view('company-size.edit', compact('title', 'comp_size'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'company_size.required' => 'The size field required.',
		];

		$validator = Validator::make($request->all(), [
			'company_size' => 'required',
			'status' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$comp_size = CompanySizeMst::find($id);
			$comp_size->fill($alldata);
			$comp_size->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.companysize-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$comp_size = CompanySizeMst::find($id);
		$comp_size->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
