<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanyTypeMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class CompanyTypeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Company Type List';
		$comp_types = CompanyTypeMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('company_type', 'like', '%' . $request->q . '%');
			}
		})->orderBy('_id', 'desc')->paginate(25)->appends(request()->query());
		return view('company-type.index', compact('title', 'comp_types'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Company Type Create';
		return view('company-type.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'company_type.required' => 'The company type field required.',
		];

		$validator = Validator::make($request->all(), [
			'company_type' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			CompanyTypeMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->_id, 'status' => 1]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.companytype-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Company Type Edit';
		$comp_type = CompanyTypeMst::find($id);
		return view('company-type.edit', compact('title', 'comp_type'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			/*'name.required' => 'The name field required.',
            'attribute_id.required' => 'The attribute field required.',*/
		];

		$validator = Validator::make($request->all(), [
			'company_type' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$comp_type = CompanyTypeMst::find($id);
			$comp_type->fill($alldata);
			$comp_type->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.companytype-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$comp_type = CompanyTypeMst::find($id);
		$comp_type->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
