<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CountryMst;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;

class CountryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Country List';
		$query = $request->input('q');
		if ($query) {
			$countries = CountryMst::where('name', 'LIKE', "%{$query}%")
				->orWhere('country_code', 'LIKE', "%{$query}%")
				->orderBy('name')
				->paginate(250);
		} else {
			$countries = CountryMst::orderBy('name')->paginate(25)->appends(request()->query());
		}
		//echo "<pre>";print_r($countries);die;
		return view('country-manage.index', compact('title', 'countries'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Country Create';
		return view('country-manage.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$message = [
			'name.required' => 'The country name field required.',
			'code.required' => 'The country code field required.',
			'dial_code.required' => 'The dial code required.',

		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'code' => 'required',
			'dial_code' => 'required',
			'currency' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {

			$allRequest = $request->except(['image']);

			$image_name = '';
			if ($request->hasFile('image')) {
				$image = $request->file('image');

				$new_name = $request->file('image')->storePublicly(
					'upload_files/country_image'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/country_image' . '/', '', $new_name);
				}
				$image_name = $filename;
			}
			$slug = str_slug('jobs-in-' . $request->name, "-");
			$country = CountryMst::create($allRequest + ['image' => $image_name, 'slug' => $slug]);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.country-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Country Edit';
		$country = CountryMst::find($id);
		return view('country-manage.edit', compact('title', 'country'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'name.required' => 'The country name field required.',
			'code.required' => 'The country code field required.',
			'dial_code.required' => 'The dial code required.',

		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'code' => 'required',
			'dial_code' => 'required',
			'currency' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			$country = CountryMst::find($id);

			$country->fill($request->except('image'));
			$country->save();
			if ($request->hasFile('image')) {
				Storage::disk('s3')->delete('upload_files/country_image/' . $country->image);
				$image = $request->file('image');
				$name = $request->input('name') . time() . '.' . $image->getClientOriginalExtension();

				$new_name = $request->file('image')->storePublicly(
					'upload_files/country_image'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/country_image' . '/', '', $new_name);
				}

				$country->image = $filename;
				$country->save();
			}
			$slug = str_slug('jobs-in-' . $request->name, "-");
			$country->slug = $slug;
			$country->save();

			Session::flash('msg', 'Sucessfuly updated');
			return redirect()->route('admin.country-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function testUploadFile(Request $request) {
		return response()->json($request->all());
		/*if ($request->hasFile('profile_picture')) {
				$image = $request->file('profile_picture');
				$name = time() . '.' . $image->getClientOriginalExtension();
				return "test";
			}else{
				return "false";
			}*/
	}
}
