<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class CourseController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Course List';
		$courses = Course::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('name', 'like', '%' . $request->q . '%');
			}
		})->orderBy('_id', 'desc')->paginate(25)->appends(request()->query());
		return view('course.index', compact('title', 'courses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Course Create';
		return view('course.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'name.required' => 'The course name field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$city = Course::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'status' => 1]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.course-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Course Edit';
		$course = Course::find($id);
		return view('course.edit', compact('title', 'course'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'name.required' => 'The course name field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$course = Course::find($id);
			$course->fill($alldata);
			$course->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.course-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$course = Course::find($id);
		$course->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
