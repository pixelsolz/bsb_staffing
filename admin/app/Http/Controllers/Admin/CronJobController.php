<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\DegreeMst;
use App\Models\User;
use Carbon\Carbon;
use Mail;

class CronJobController extends Controller {

	public function employerCompletedProfileSendMail() {
		$sevenDays = Carbon::now()->subDays(7);
		$employeers = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employer');
		})->whereHas('employerUser', function ($q) {
			$q->whereNotNull('license_agree');
		})->where('last_login', '<=', $sevenDays)
			->where(function ($q) use ($sevenDays) {
				$q->whereNull('last_email_date');
				$q->orWhere('last_email_date', '<=', $sevenDays);
			})->get();

		$view = 'emails.not-loggedin.completed_employer1st';
		$view2 = 'emails.not-loggedin.completed_employer2nd';
		$subject1 = 'Bsbstaffing.com has the best talent resources for your organization to hire.';
		$subject2 = 'Bsbstaffing.com is Introducing millions of job seekers for your organization.';
		$lastEmail = '';
		foreach ($employeers as $employeer) {
			$employeer->last_email_date = Carbon::now()->format('Y-m-d');
			$lastEmail = ($employeer->last_email_no == '1' ? '2' : '1');
			$employeer->last_email_no = $lastEmail;
			$employeer->save();
			$data = ['text' => 'Millions of jobseekers nearby you are looking jobs why not post your requirements Jobs FREE on bsbstaffing and get talent for your company.', 'user' => $employeer->employerUser, 'email' => $employeer->email];
			$data1 = ['text' => 'Hire talent for your company free now,from bsbstaffing.com we offer unlimited job post free.', 'user' => $employeer->employerUser, 'email' => $employeer->email];
			if ($lastEmail == '2') {
				Mail::to($employeer->email)->send(new SendMail($data, $view2, $subject2));
			} else {
				Mail::to($employeer->email)->send(new SendMail($data, $view, $subject1));
			}

		}

	}

	public function employerIncompletedProfileSendMail() {
		$twoDays = Carbon::now()->subDays(2);
		$threeDays = Carbon::now()->subDays(3);
		$employeers = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employer');
		})->whereHas('employerUser', function ($q) {
			$q->whereNull('license_agree');
		})->where('last_login', '<=', $twoDays)
			->where(function ($q) use ($threeDays) {
				$q->whereNull('last_email_date');
				$q->orWhere('last_email_date', '<=', $threeDays);
			})->get();

		$view = 'emails.not-loggedin.notcompleted_employer1st';
		$view2 = 'emails.not-loggedin.notcompleted_employer2nd';
		$subject1 = 'Your bsbstaffing.com account registration is pending. We offer FREE Unlimited Job post.';
		$subject2 = 'Complete your registration on bsbstaffing.com. Millions of job seekers are waiting for jobs.';
		$lastEmail = '';
		foreach ($employeers as $employeer) {
			$employeer->last_email_date = Carbon::now()->format('Y-m-d');
			$lastEmail = ($employeer->last_email_no == '1' ? '2' : '1');
			$employeer->save();
			$data = ['text' => 'Complete your application NOW, Millions of jobseekers nearby you are looking jobs why not post your Jobs FREE on bsbstaffing and get talent for your company.', 'user' => $employeer->employerUser, 'email' => $employeer->email];
			$data1 = ['text' => '4 Reasons Why You Should Complete Your company Registration NOW. on bsbstaffing.com', 'user' => $employeer->employerUser, 'notComEmployeer' => true, 'email' => $employeer->email];
			if ($lastEmail == '2') {
				Mail::to($employeer->email)->send(new SendMail($data, $view2, $subject2));
			} else {
				Mail::to($employeer->email)->send(new SendMail($data, $view, $subject1));
			}
		}
	}

	public function employeeCompletedProfileSendMail() {
		$sevenDays = Carbon::now()->subDays(7);
		$employees = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employee');
		})->whereHas('employeeUser', function ($q) {
			$q->where('form_no', '!=', 1);
		})->where('last_login', '<=', $sevenDays)->where(function ($q) use ($sevenDays) {
			$q->whereNull('last_email_date');
			$q->orWhere('last_email_date', '<=', $sevenDays);
		})->get();

		/*$employees = User::whereHas('roles', function ($q) {
				$q->where('name', 'Employee');
			})->has('employeeUser.profile_picture')->has('employeeUser.date_of_birth')->has('employeeUser.profile_title')
				->has('employeeCvTxn')->has('employeePrefTxn')->has('employeeEmployementHistory')->has('employeeEducation')
		*/

		$view = 'emails.not-loggedin.completed_employee1st';
		$view2 = 'emails.not-loggedin.completed_employee2nd';
		$subject1 = 'bsbsataffing.com introduces you to top MNCs for your career options';
		$subject2 = 'On bsbstaffing.com to find the best career options. For local and international';
		foreach ($employees as $employee) {
			$employee->last_email_date = Carbon::now()->format('Y-m-d');
			$lastEmail = ($employee->last_email_no == '1' ? '2' : '1');
			$employee->last_email_no = $lastEmail;
			$employee->save();
			$data = ['text' => 'Millions of New jobs nearby you finding talent like you. You must apply all new arrival jobs on bsbstaffing.com NOW', 'user' => $employee->employeeUser];
			$data1 = ['text' => 'As per his/her job title and as per his or her industry they will get mail what jobs are available all over the world for them on bsbstaffing.com', 'user' => $employee->employeeUser];
			if ($lastEmail == '2') {
				Mail::to($employee->email)->send(new SendMail($data1, $view2, $subject2));
			} else {
				Mail::to($employee->email)->send(new SendMail($data, $view, $subject1));
			}

		}
	}

	public function employeeIncompletedProfileSendMail() {
		$twoDays = Carbon::now()->subDays(2);
		$employees = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employee');
		})->whereHas('employeeUser', function ($q) {
			$q->where('form_no', 1);
		})->where('last_login', '<=', $twoDays)->where(function ($q) use ($twoDays) {
			$q->whereNull('last_email_date');
			$q->orWhere('last_email_date', '<=', $twoDays);
		})->get();
		/*$employees = User::whereHas('roles', function ($q) {
				$q->where('name', 'Employee');
			})->where(function ($q) {
				$q->whereHas('employeeUser', function ($q) {
					$q->where('profile_picture', '=', '');
					$q->orWhere('date_of_birth', '=', '');
					$q->orWhere('profile_title', '=', '');
				})->orDoesntHave('employeeCvTxn')->orDoesntHave('employeePrefTxn')->orDoesntHave('employeeEmployementHistory')
					->orDoesntHave('employeeEducation')->orDoesntHave('employeeSkill')->orDoesntHave('employeeCertification')->orDoesntHave('employeeLanguage');
		*/

		$view = 'emails.not-loggedin.incomplete_employee1st';
		$view2 = 'emails.not-loggedin.incomplete_employee2nd';
		$subject1 = 'bsbstaffing.com has several jobs for you. Complete the registration to find the best job.';
		$subject2 = 'Bsbstaffing.com opens a new path for your success with top MNCs for your career.';
		foreach ($employees as $employee) {
			$employee->last_email_date = Carbon::now()->format('Y-m-d');
			$lastEmail = ($employee->last_email_no == '1' ? '2' : '1');
			$employee->save();
			$data = ['text' => 'Complete your Full application NOW on bsbstaffing.com Millions of jobs nearby you waiting. Do not miss it.', 'user' => $employee->employeeUser, 'user_email' => $employee->email];
			$data1 = ['text' => 'Complete your Full application NOW on bsbstaffing.com last two days many MNC post hug no of jobs on bsbstaffing.com with highly paid salary don’tmiss it APPLY NOW.', 'user' => $employee->employeeUser, 'user_email' => $employee->email];
			if ($lastEmail == '2') {
				Mail::to($employee->email)->send(new SendMail($data1, $view2, $subject2));
			} else {
				Mail::to($employee->email)->send(new SendMail($data, $view, $subject1));
			}

		}
	}

	public function collegeStudentsSendMail() {
		$sevenDays = Carbon::now()->subDays(7);
		$colleges = User::whereHas('roles', function ($q) {
			$q->where('name', 'College');
		})->where(function ($q) use ($sevenDays) {
			$q->whereNull('last_email_date');
			$q->orWhere('last_email_date', '<=', $sevenDays);
		})->get();

		$view = 'emails.not-loggedin.college1st';
		$view1 = 'emails.not-loggedin.college2nd';
		$subject1 = 'Bsbstaffing.com has ample career options for your students.';
		$subject2 = 'bsbstaffing.com allows you to become a part of your student’s success.';
		$lastEmail = '';
		foreach ($colleges as $college) {
			$college->last_email_date = Carbon::now()->format('Y-m-d');
			$lastEmail = (@$college->last_email_no == '1' ? '2' : '1');
			$college->save();
			$data = ['text' => 'Millions of New jobs nearby your city, country, and abroad for your students. You must update all your students about new arrival jobs on bsbstaffing.com NOW.', 'user' => $college->collegeUser];
			$data1 = ['text' => 'Many MNC are hiring fresher for their company like paid internship and for full time jobs you must update all your students about new arrival jobs on bsbstaffing.com NOW.', 'user' => $college->collegeUser];
			if ($lastEmail == '2') {
				Mail::to($college->email)->send(new SendMail($data1, $view1, $subject2));
			} else {
				Mail::to($college->email)->send(new SendMail($data, $view, $subject1));
			}

		}
	}

	public function updateCourse() {
		$courses = DegreeMst::where('attribute_id', '607462e58ddba613ef5d11e6')->get();
		foreach ($courses as $course) {
			$course->delete();
		}
	}

	public function updateUserName() {
		$users = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employee');
		})->get();

		foreach ($users as $user) {
			if ($user->employeeUser && $user->employeeUser->first_name) {
				$user->emp_name = $user->employeeUser->first_name . ' ' . ($user->employeeUser->last_name ? $user->employeeUser->last_name : '');
				$user->save();
			}

		}

	}

}
