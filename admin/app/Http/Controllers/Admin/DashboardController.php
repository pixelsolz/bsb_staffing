<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployeeAppliedJobsTxn;
use App\Models\EmployerJobTxn;
use App\Models\InternationalJobSavedAppliedTxn;
use App\Models\InternationalJobTxn;
use App\Models\InterviewSlotBook;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller {
	public function index(Request $request) {
		$title = 'Admin | Dashboard';
		$total_employee = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->count();
		$total_employer = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->count();

		$complete_employee = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->whereHas('employeeUser', function ($q) {
			$q->where('form_no', '!=', 1);
		})->count();

		$incomplete_employee = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->whereHas('employeeUser', function ($q) {
			$q->where('form_no', 1);
		})->count();

		$complete_employer = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employer');
		})->whereHas('employerUser', function ($q) {
			$q->whereNotNull('license_agree');
		})->count();

		$incomplete_employer = User::whereHas('roles', function ($q) {
			$q->where('name', 'Employer');
		})->whereHas('employerUser', function ($q) {
			$q->whereNull('license_agree');
		})->count();

		$total_franchise = User::whereHas('roles', function ($q) {
			$q->where('slug', 'franchise');
		})->count();
		$total_college = User::whereHas('roles', function ($q) {
			$q->where('slug', 'college');
		})->count();
		$total_agency = User::whereHas('roles', function ($q) {
			$q->where('slug', 'agency');
		})->count();
		$total_trainer = User::whereHas('roles', function ($q) {
			$q->where('slug', 'trainer');
		})->count();
		$total_other_user = User::whereHas('roles', function ($q) {
			$q->whereNotIn('slug', ['employee', 'employer']);
		})->count();
		$total_applied_job = EmployeeAppliedJobsTxn::whereHas('job', function ($q) use ($request) {

		})->count();

		$total_posted_job = EmployerJobTxn::where('job_status', 1)
			->where('is_published', 1)->count();

		$total_applied_domestic_international_job = InternationalJobSavedAppliedTxn::where('apply_user_type', '1')->whereHas('job', function ($q) use ($request) {
		})->count();

		$total_applied_international_job = InternationalJobSavedAppliedTxn::where('apply_user_type', '2')->whereHas('job', function ($q) use ($request) {
		})->count();

		$total_posted_international_job = InternationalJobTxn::where('job_status', 1)
			->where('is_published', 1)->count();

		$international_jobs = EmployeeAppliedJobsTxn::where('apply_user_type', '2')->whereHas('job', function ($q) use ($request) {
		})->count();
		$international_internationals = InternationalJobSavedAppliedTxn::where('apply_user_type', '2')->whereHas('job', function ($q) use ($request) {
		})->count();
		$international_walkings = InterviewSlotBook::where('apply_user_type', '2')->whereHas('job', function ($q) use ($request) {
		})->count();

		$donestic_jobs = EmployeeAppliedJobsTxn::where('apply_user_type', '1')->whereHas('job', function ($q) use ($request) {
		})->count();
		$domestic_internationals = InternationalJobSavedAppliedTxn::where('apply_user_type', '1')->whereHas('job', function ($q) use ($request) {
		})->count();
		$domestic_walkings = InterviewSlotBook::where('apply_user_type', '1')->whereHas('job', function ($q) use ($request) {
		})->count();

		return view('dashboard', compact('title', 'total_employee', 'total_employer', 'total_other_user', 'total_applied_job', 'total_posted_job', 'total_franchise', 'total_college', 'total_agency', 'total_trainer', 'complete_employer', 'incomplete_employer', 'complete_employee', 'incomplete_employee', 'total_applied_international_job', 'total_applied_domestic_international_job', 'total_posted_international_job', 'international_jobs', 'international_internationals', 'international_walkings', 'donestic_jobs', 'domestic_internationals', 'domestic_walkings'));
	}

	public function internalEmpDashboard(Request $request) {
		$title = 'Internal Employee | Dashboard';
		return view('internal-emp-dashboard', compact('title'));
	}
}
