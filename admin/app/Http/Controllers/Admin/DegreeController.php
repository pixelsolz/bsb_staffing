<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttributeMst;
use App\Models\DegreeMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class DegreeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Degree List';
		$degrees = DegreeMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('name', 'like', '%' . $request->q . '%');
			}
		})->orderBy('_id', 'desc')->paginate(25)->appends(request()->query());
		return view('degree.index', compact('title', 'degrees'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Degree Create';
		$attr_msts = AttributeMst::all();
		$degrees = DegreeMst::all();
		return view('degree.create', compact('title', 'attr_msts', 'degrees'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'name.required' => 'The name field required.',
			'attribute_id.required' => 'The attribute field required.',
			'parent_id.required' => 'The parent field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'attribute_id' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$city = DegreeMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->_id, 'status' => 1]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.degree-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Degree Edit';
		$degree = DegreeMst::find($id);
		$attr_msts = AttributeMst::all();
		$degrees = DegreeMst::all();
		return view('degree.edit', compact('title', 'degree', 'attr_msts', 'degrees'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			'name.required' => 'The name field required.',
			'attribute_id.required' => 'The attribute field required.',
			'parent_id.required' => 'The parent field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'attribute_id' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$degree = DegreeMst::find($id);
			$degree->fill($alldata);
			$degree->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->to($request->input('redirects_to'));
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$degree = DegreeMst::find($id);
		$degree->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
