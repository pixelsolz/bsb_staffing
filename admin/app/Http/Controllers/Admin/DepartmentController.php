<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DepartmentMst;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class DepartmentController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Department List';
		$query = $request->input('q');
		//echo DepartmentMst::all();exit();
		if ($query) {
			$department = DepartmentMst::where('name', 'LIKE', "%{$query}%")->orderBy('_id', 'desc')
				->paginate(25);

		} else {
			//return DepartmentMst::get();
			$department = DepartmentMst::orderBy('_id', 'desc')->paginate(25);
		}

		return view('department-manage.index', compact('title', 'department'));
		// return view('city-manage.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Department Create';
		$departments = DepartmentMst::get();
		return view('department-manage.create', compact('title', 'departments'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'name.required' => 'The name field required.',
			'parent_id.required' => 'The parent is required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'parent_id' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$city = DepartmentMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id]);
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.department-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$title = 'Department Edit';
		$depertment = DepartmentMst::find($id);
		$departments = DepartmentMst::get();
		return view('department-manage.edit', compact('title', 'depertment', 'departments'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$message = [
			'name.required' => 'The name field required.',
			'parent_id.required' => 'The parent is required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'parent_id' => 'required',

		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$city = DepartmentMst::find($id);
			$city->fill($alldata);
			$city->save();
			Session::flash('msg', 'Sucessfuly Updated');
			return redirect()->route('admin.department-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$department = DepartmentMst::find($id);
		$department->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfullu deleted']);
	}
}
