<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CountryMst;
use App\Models\Role;
use App\Models\User;
use App\Models\CityMst;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Session;
use Validator;

use App\Imports\CountryImport;
use App\Exports\CountryExport;
use Excel;

class ExcelImportExportController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Import';
		return view('import.import', compact('title'));
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		
	}

	public function importExcel(Request $request)
    {

        $request->validate([
            'import_file' => 'required'
        ]);
 		//$path = $request->file('import_file')->getRealPath();
 		
 		$rows = Excel::toArray(new CountryImport,request()->file('import_file'));
 		//echo "<pre>";print_r($rows);die;
 		
    	foreach ($rows as $key => $row) {
    		
    		$country = CountryMst::create([
                'name' => $row[0][0],
                'code' => $row[1][0],
                'dial_code' => $row[2][0],
                'currency' => $row[3][0],
                'currency_code' => $row[4][0],
            ]);
            $country_code = $country->code;
    		unset($row[0]); 
    		unset($row[1]); 
    		unset($row[2]); 
    		unset($row[3]); 
    		unset($row[4]); 

    		foreach($row as $city){
	    		CityMst::create([
	                'name' => $city[0],
	                'country_code' => $country_code,
	                
	            ]);
    		
    		}
    		//echo "<pre>";print_r($row).'<br>';
    		
    	}
    	
        return back()->with('success', 'Insert Record successfully.');
    }

    public function exportCountry() 
    {
        return Excel::download(new CountryExport, 'country.csv');
    }


}
