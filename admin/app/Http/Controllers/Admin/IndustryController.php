<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttributeMst;
use App\Models\IndustryMst;
use App\Models\ServicesMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class IndustryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Industry List';
		$industries = IndustryMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('name', 'like', '%' . $request->q . '%');
			}
		})->paginate(25)->appends(request()->query());
		//echo $industries;exit();
		return view('industry.index', compact('title', 'industries'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Industry Create';
		$serviceMsts = ServicesMst::all();
		$attrMsts = AttributeMst::all();
		return view('industry.create', compact('title', 'serviceMsts', 'attrMsts'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			//'company_type.required' => 'The company type field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'attribute_id' => 'required',
			//'service_id' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			$slug = str_slug($request->name . '-industry-jobs', "-");
			IndustryMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'status' => 1, 'slug' => $slug]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.industry-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Industry Edit';
		$serviceMsts = ServicesMst::all();
		$attrMsts = AttributeMst::all();
		$industry = IndustryMst::find($id);
		return view('industry.edit', compact('title', 'industry', 'serviceMsts', 'attrMsts'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			/*'name.required' => 'The name field required.',
            'attribute_id.required' => 'The attribute field required.',*/
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'attribute_id' => 'required',
			//'service_id' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$industry = IndustryMst::find($id);
			$industry->fill($alldata);
			$slug = str_slug($request->name . '-industry-jobs', "-");
			$industry->slug = $slug;
			$industry->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.industry-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$industry = IndustryMst::find($id);
		$industry->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
