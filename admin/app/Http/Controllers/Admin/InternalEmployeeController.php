<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\EmployerJobTxn;
use App\Models\IndustryMst;
use App\Models\Role;
use App\Models\User;
use App\Models\InternalEmployeeProfileTxn;
use App\Models\Course;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;
use Carbon\Carbon;

class InternalEmployeeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'User registration list';
		/*$countries = CountryMst::orderBy('name')->get();
		$courses = Course::orderBy('name')->get();
		$users = User::whereHas('roles', function ($q) {
			//$q->whereNotIn('slug', ['employee', 'employer']);
			$q->where('slug', request()->segment(2));
		})->where(function ($q) use ($request) {
			if (!empty($request->search)) {

				if(!empty($request->input('from_joined_date')) && !empty($request->input('to_joined_date'))){
				//echo $request->input('start_date');die;
					$start_date = Carbon::createFromDate($request->input('from_joined_date'));
					$end_date = Carbon::createFromDate($request->input('to_joined_date'));
					//echo $start_date;die;
					$q->whereBetween('created_at', array($start_date,$end_date));
				}
				if ($request->input('status') == 1 || $request->input('status') == '0') {
					//echo $request->input('status');die;
					$q->where('status', (int)$request->input('status'));
				}
				if(request()->segment(2)=='franchise'){
					if (!empty($request->search_by_country)) {
						$q->whereHas('franchiseUser', function ($q) use ($request) {
						$q->where('country_id', $request->search_by_country);
					});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('franchiseUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('franchiseUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('comp_name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

				if(request()->segment(2)=='agency'){
					if (!empty($request->search_by_country)) {
						$q->whereHas('agencyUser', function ($q) use ($request) {
							$q->where('country_id', $request->search_by_country);
						});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('agencyUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('agencyUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('comp_name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

				if(request()->segment(2)=='college'){
					if (!empty($request->search_by_country)) {
						$q->whereHas('collegeUser', function ($q) use ($request) {
						$q->where('country_id', $request->search_by_country);
					});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('collegeUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('collegeUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

				if(request()->segment(2)=='trainer'){
					if (!empty($request->search_by_country)) {
						$q->whereHas('trainerUser', function ($q) use ($request) {
						$q->where('country_id', $request->search_by_country);
					});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('trainerUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('trainerUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('name', 'like', '%' . $request->input_search . '%');
						});
					}
				}
			
			}
		})->orderBy('created_at', 'desc')->paginate(10)->appends(request()->query());*/
		//echo "<pre>";print_r($users);die;
		return view('internal-emp.index', compact('title'));
	}

	

}
