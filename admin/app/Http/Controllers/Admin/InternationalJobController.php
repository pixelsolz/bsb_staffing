<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AllOtherMasterMst;
use App\Models\ApplicationPhaseMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\Course;
use App\Models\DegreeMst;
use App\Models\DepartmentMst;
use App\Models\EmployeeAppliedJobsTxn;
use App\Models\ExperienceMst;
use App\Models\IndustryMst;
use App\Models\InternationalJobSavedAppliedTxn;
use App\Models\InternationalJobTxn;
use App\Models\InterviewSlotBook;
use App\Models\LanguageMst;
use App\Models\SkillMst;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class InternationalJobController extends Controller {

	public function employerInternationalPost(Request $request) {
		$title = 'Employer International Posted Job';
		$skills = SkillMst::all();
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();
		$employer_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->get();
		$experiences = ExperienceMst::all();

		$jobs = InternationalJobTxn::where(function ($q) use ($request) {

			if (!empty($request->input('id'))) {
				$q->where('employer_id', $request->input('id'));
			}
			if (!empty($request->input('country_id'))) {
				$q->where('loc_countries', $request->input('country_id'));
			}

			if (!empty($request->input('city_id'))) {
				$q->where('loc_cities', $request->input('city_id'));
			}
			if (!empty($request->input('min_experiance'))) {
				//echo $request->input('min_experiance');die;
				$q->whereHas('minExperiance', function ($q) use ($request) {
					$q->where('_id', $request->input('min_experiance'));
				});
			}
			if (!empty($request->input('max_experiance'))) {
				$q->whereHas('maxExperiance', function ($q) use ($request) {
					$q->where('_id', $request->input('max_experiance'));
				});

			}
			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				//echo $request->input('start_date');die;
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				//echo $start_date;die;
				$q->whereBetween('created_at', array($start_date, $end_date));
			}
			if (!empty($request->input('job_title'))) {
				$q->where('job_title', 'like', '%' . $request->job_title . '%');
			}
			if (!empty($request->input('skills'))) {
				$q->whereIn('required_skill', $request->input('skills'));
			}
			if ($request->input('status') == 1 || $request->input('status') == '0') {
				//echo $request->input('status');die;
				$q->where('job_status', (int) $request->input('status'));
			}
			if (!empty($request->input('industry'))) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->where('_id', $request->input('industry'));
				});
			}
			if (!empty($request->input('department'))) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->where('_id', $request->input('department'));
				});
			}

			if (!empty($request->input('salary'))) {
				if (!empty($request->input('salary')['currency'])) {
					$q->whereHas('currency', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['currency']);
					});
				}
				if (!empty($request->input('salary')['min_salary'])) {
					$q->whereHas('minSalary', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['min_salary']);
					});
				}
				if (!empty($request->input('salary')['max_salary'])) {
					$q->whereHas('maxSalary', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['max_salary']);
					});
				}

			}
			if (!empty($request->input_search)) {
				$q->whereHas('employer', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('employerUser', function ($q) use ($request) {
						$q->where('company_name', 'like', '%' . $request->input_search . '%');
					});
				});
			}
			if (!empty($request->search_by_employer)) {
				$bsb_unique_no = ltrim($request->search_by_employer, "BSBEMR");
				$q->whereHas('employer', function ($q) use ($request) {
					$q->where('bsb_unique_no', $bsb_unique_no);
				});
			}

		})
			->where('job_status', 1)
			->where('is_published', 1)
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());
		return view('jobs.employer-posted-internationaljob', compact('title', 'jobs', 'request', 'skills', 'experiences', 'employer_users', 'allCountry', 'allcity', 'allDeperment', 'allIndustry'));
	}

	public function employerInternationalJobApplicationReceived(Request $request) {
		$title = 'International Job Application Received';
		$job_id = $request->id;
		$job = InternationalJobTxn::find($job_id);
		$job_type = 'international';
		if ($job) {
			$applied_jobs = $job->jobSavedApplied()->orderBy('created_at', 'desc')->paginate(25);

		} else {
			$applied_jobs = [];
		}
		//echo "<pre>";print_r($applied_jobs);die;
		return view('jobs.job-application-received', compact('title', 'applied_jobs', 'job_type'));
	}

	public function employeeAppliedInternationalJobview(Request $request, $id) {
		$title = 'International Applied job view';
		$applied_job = InternationalJobSavedAppliedTxn::find($id);
		$type = 'international';
		return view('jobs.employee-applied-job-view', compact('title', 'applied_job', 'type'));
	}

	public function employeeAppliedWalkingJobview(Request $request, $id) {
		$title = 'Walking Applied job view';
		$applied_job = InterviewSlotBook::find($id);
		$type = 'walking';
		return view('jobs.employee-applied-job-view', compact('title', 'applied_job', 'type'));
	}

	public function getEmployeeAppliedInternationalJob(Request $request) {
		$title = 'Employee Applied International Job';
		$skills = SkillMst::all();
		$allCountry = CountryMst::orderBy('name')->get();
		$allcity = CityMst::orderBy('name')->get();
		$employee_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->get();
		$experiences = ExperienceMst::all();
		$allApplicationPhase = ApplicationPhaseMst::all();

		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();

		$jobs = InternationalJobSavedAppliedTxn::where(function ($q) use ($request) {

			if (!empty($request->input('id'))) {
				$q->where('employee_id', $request->input('id'));
			}
			if (!empty($request->input('application_phase'))) {
				$q->where('applied_status', $request->input('application_phase'));
			}
			if (!empty($request->input('apply_user_type'))) {
				$q->where('apply_user_type', $request->input('apply_user_type'));
			}
			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('loc_countries', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('loc_cities', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});

			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		$type = 'applied-job';
		$jbType = 'international';
		return view('jobs.employee-applied-job', compact('title', 'jobs', 'type', 'request', 'skills', 'allCountry', 'allcity', 'employee_users', 'experiences', 'allApplicationPhase', 'allDeperment', 'allIndustry', 'jbType'));
	}

	public function editJob($id) {
		$title = 'Edit Posted Jobs';
		$job = InternationalJobTxn::find($id);

		$skills = SkillMst::all();
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$selectedCityCode = CityMst::whereIn('_id', $job->cities)->pluck('country_code')->toArray();
		$selectedCity = CityMst::whereIn('country_code', array_merge($selectedCityCode, $job->country()->pluck('code')->toArray()))->get();
		$selectedLocationCities = CityMst::whereIn('country_code', array_merge($job->locationCity()->pluck('country_code')->toArray(), $job->locationCountry()->pluck('code')->toArray()))->get();
		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();
		$courses = Course::all();

		$degreesUnderGrad = DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 2);})->get();
		$degreesPostGrad = DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 3);})->get();
		$employementFor = AllOtherMasterMst::where('entity', 'emplyment_for')->get();
		$employementTypes = AllOtherMasterMst::where('entity', 'employement_type')->get();
		$employer_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->get();
		$experiences = ExperienceMst::all();
		$languages = LanguageMst::all();
		$jbType = 'international';

		return view('jobs.employer-job-edit', compact('title', 'job', 'skills', 'allCountry', 'allcity', 'allDeperment', 'allIndustry', 'employer_users', 'courses', 'employementFor', 'experiences', 'languages', 'employementTypes', 'degreesUnderGrad', 'degreesPostGrad', 'selectedCity', 'jbType', 'selectedLocationCities'));
	}

	public function updateJob(Request $request, $id) {
		$job = InternationalJobTxn::find($id);
		try {
			$allRequest = $request->except(['industry_id', 'candidate_type']);
			$job = InternationalJobTxn::find($id);
			$job->fill($allRequest + ['candidate_type' => (int) $request->candidate_type]);
			$job->updated_at = Carbon::now();
			$job->save();
			$job->industry()->detach();
			$job->language()->detach();
			$job->country()->detach();
			$job->city()->detach();
			$job->locationCountry()->detach();
			$job->locationCity()->detach();
			$job->industry()->attach($request->industry_id, ['type' => 'job']);
			$job->language()->attach($request->language_pref, ['type' => 'job']);
			$job->country()->attach($request->countries);
			$job->city()->attach($request->cities);
			$job->locationCountry()->attach($request->loc_countries);
			$job->locationCity()->attach($request->loc_cities);
			Session::flash('msg', 'Sucessfuly edited');
			return redirect()->route('admin.employer.international-posted-job');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	public function internationalJobStatusChange(Request $request) {
		$job = InternationalJobTxn::find($request->id);
		$job->job_status = $request->status;
		$job->save();
		return response()->json(['status' => 200]);
	}

	public function getDomesicJobApplicant(Request $request, $type) {
		$title = '';
		if ($type == 'jobs') {
			$title = 'Employee Applied Domestic Normal Job';
		} elseif ($type == 'international') {
			$title = 'Employee Applied Domestic International Job';
		} else {
			$title = 'Employee Applied Domestic Walking';
		}

		$skills = SkillMst::all();
		$allCountry = CountryMst::orderBy('name')->get();
		$allcity = CityMst::orderBy('name')->get();
		$employee_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->get();
		$experiences = ExperienceMst::all();
		$allApplicationPhase = ApplicationPhaseMst::all();

		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();

		$international = InternationalJobSavedAppliedTxn::where(function ($q) use ($request) {
			$q->where('apply_user_type', '1');
			if (!empty($request->input('id'))) {
				$q->where('employee_id', $request->input('id'));
			}
			if (!empty($request->input('application_phase'))) {
				$q->where('applied_status', $request->input('application_phase'));
			}

			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('loc_countries', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('loc_cities', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});

			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		$jobs = EmployeeAppliedJobsTxn::where(function ($q) use ($request) {
			$q->where('apply_user_type', '1');
			if (!empty($request->input('id'))) {
				$q->where('employee_id', $request->input('id'));
			}
			if (!empty($request->input('application_phase'))) {
				$q->where('applied_status', $request->input('application_phase'));
			}

			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('country_id', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('city_id', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});

			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		$walking = InterviewSlotBook::where(function ($q) use ($request) {
			$q->where('apply_user_type', '1');
			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('country_id', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('city_id', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});
			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		return view('jobs.domestic-applicant', compact('title', 'jobs', 'type', 'request', 'skills', 'allCountry', 'allcity', 'employee_users', 'experiences', 'allApplicationPhase', 'allDeperment', 'allIndustry', 'walking', 'international'));
	}

	public function getInternationalJobApplicant(Request $request, $type) {
		$title = '';
		if ($type == 'jobs') {
			$title = 'Employee Applied International Normal Job';
		} elseif ($type == 'international') {
			$title = 'Employee Applied International International Job';
		} else {
			$title = 'Employee Applied International Walking';
		}
		$skills = SkillMst::all();
		$allCountry = CountryMst::orderBy('name')->get();
		$allcity = CityMst::orderBy('name')->get();
		$employee_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->get();
		$experiences = ExperienceMst::all();
		$allApplicationPhase = ApplicationPhaseMst::all();

		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();

		$international = InternationalJobSavedAppliedTxn::where(function ($q) use ($request) {
			$q->where('apply_user_type', '2');
			if (!empty($request->input('id'))) {
				$q->where('employee_id', $request->input('id'));
			}
			if (!empty($request->input('application_phase'))) {
				$q->where('applied_status', $request->input('application_phase'));
			}

			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('loc_countries', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('loc_cities', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});

			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		$jobs = EmployeeAppliedJobsTxn::where(function ($q) use ($request) {
			$q->where('apply_user_type', '2');
			if (!empty($request->input('id'))) {
				$q->where('employee_id', $request->input('id'));
			}
			if (!empty($request->input('application_phase'))) {
				$q->where('applied_status', $request->input('application_phase'));
			}

			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('country_id', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('city_id', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});

			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		$walking = InterviewSlotBook::where(function ($q) use ($request) {
			$q->where('apply_user_type', '2');
			$q->whereHas('job', function ($q) use ($request) {

				if (!empty($request->input('country_id'))) {
					$q->where('country_id', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('city_id', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});
			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		return view('jobs.international-applicant', compact('title', 'jobs', 'type', 'request', 'skills', 'allCountry', 'allcity', 'employee_users', 'experiences', 'allApplicationPhase', 'allDeperment', 'allIndustry', 'walking', 'international'));
	}

	public function updateUserType() {
		$internationalJobs = EmployeeAppliedJobsTxn::all();

		foreach ($internationalJobs as $job) {
			$userCountry = $job->employee->employeeUser->country_id;
			if ($userCountry && $job->job) {
				if ($userCountry == $job->job->country_id) {
					$job->apply_user_type = '1';
				} else {
					$job->apply_user_type = '2';
				}
				/*if (in_array($userCountry, $job->job->loc_countries)) {
						$job->apply_user_type = '1';
					} else {
						$job->apply_user_type = '2';
				*/
				$job->save();
			}
		}
		return 'ok';
	}
}
