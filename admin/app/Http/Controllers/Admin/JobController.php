<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AllOtherMasterMst;
use App\Models\ApplicationPhaseMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\Course;
use App\Models\DegreeMst;
use App\Models\DepartmentMst;
use App\Models\EmployeeAppliedJobsTxn;
use App\Models\EmployerJobTxn;
use App\Models\ExperienceMst;
use App\Models\IndustryMst;
use App\Models\InternationalJobTxn;
use App\Models\LanguageMst;
use App\Models\SkillMst;
use App\Models\User;
use App\Models\WalkInInterviewTxn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class JobController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

	}

	public function getEmployeeAppliedJob(Request $request) {
		//echo $request->input('id');die;
		//dd($request->all());
		$title = 'Employee Applied Job';
		$skills = SkillMst::all();
		$allCountry = CountryMst::orderBy('name')->get();
		$allcity = CityMst::orderBy('name')->get();
		$employee_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->get();
		$experiences = ExperienceMst::all();
		$allApplicationPhase = ApplicationPhaseMst::all();

		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();
		// if($request->input('id') !=''){
		// 	$employee = User::find($request->input('id'));
		// 	$jobs = $employee->employeeAppliedJob()->paginate(25)->appends(request()->query());
		// }else{
		// 	$jobs =EmployeeAppliedJobsTxn::paginate(25)->appends(request()->query());
		// }
		$jobs = EmployeeAppliedJobsTxn::where(function ($q) use ($request) {

			if (!empty($request->input('id'))) {
				$q->where('employee_id', $request->input('id'));
			}
			if (!empty($request->input('application_phase'))) {
				$q->where('applied_status', $request->input('application_phase'));
			}
			$q->whereHas('job', function ($q) use ($request) {
				if (!empty($request->input('country_id'))) {
					$q->where('country_id', $request->input('country_id'));
				}
				if (!empty($request->input('city_id'))) {
					$q->where('city_id', $request->input('city_id'));
				}
				if (!empty($request->input('job_title'))) {
					$q->where('job_title', 'like', '%' . $request->job_title . '%');
				}
				if (!empty($request->input('skills'))) {
					$q->whereIn('required_skill', $request->input('skills'));
				}
				if (!empty($request->input('min_experiance'))) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('min_experiance'));
					});
				}
				if (!empty($request->input('max_experiance'))) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('_id', $request->input('max_experiance'));
					});

				}
			});

			if (!empty($request->input_search)) {
				$q->whereHas('employee', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
				});
			}

			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				//echo $request->input('start_date');die;
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				//echo $start_date;die;
				$q->whereBetween('created_at', array($start_date, $end_date));
			}

		})
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());

		$type = 'applied-job';
		$jbType = 'jobs';
		return view('jobs.employee-applied-job', compact('title', 'jobs', 'type', 'request', 'skills', 'allCountry', 'allcity', 'employee_users', 'experiences', 'allApplicationPhase', 'allDeperment', 'allIndustry', 'jbType'));
	}

	public function employerJobPost(Request $request) {
		//dd($request->all());
		$title = 'Employer Posted Job';
		$skills = SkillMst::all();
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();
		$employer_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->get();
		$experiences = ExperienceMst::all();
		// if($request->input('id') !=''){
		// 	$employer = User::find($request->input('id'));
		// 	$jobs = $employer->employerJobs()->where(function ($q) use ($request) {
		// 		if (!empty($request->input_search)) {
		// 			$q->where('job_title', 'like', '%' . $request->input_search . '%');
		// 			$q->orWhere('required_skill', 'like', '%' . $request->input_search . '%');
		// 		}
		// 	})->paginate(25)->appends(request()->query());
		// }else{
		// 	$jobs = EmployerJobTxn::paginate(25)->appends(request()->query());
		// }
		$jobs = EmployerJobTxn::where(function ($q) use ($request) {

			if (!empty($request->input('id'))) {
				$q->where('employer_id', $request->input('id'));
			}
			if (!empty($request->input('country_id'))) {
				$q->where('country_id', $request->input('country_id'));
			}

			if (!empty($request->input('city_id'))) {
				$q->where('city_id', $request->input('city_id'));
			}
			if (!empty($request->input('min_experiance'))) {
				//echo $request->input('min_experiance');die;
				$q->whereHas('minExperiance', function ($q) use ($request) {
					$q->where('_id', $request->input('min_experiance'));
				});
			}
			if (!empty($request->input('max_experiance'))) {
				$q->whereHas('maxExperiance', function ($q) use ($request) {
					$q->where('_id', $request->input('max_experiance'));
				});

			}
			if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
				//echo $request->input('start_date');die;
				$start_date = Carbon::createFromDate($request->input('start_date'));
				$end_date = Carbon::createFromDate($request->input('end_date'));
				//echo $start_date;die;
				$q->whereBetween('created_at', array($start_date, $end_date));
			}
			if (!empty($request->input('job_title'))) {
				$q->where('job_title', 'like', '%' . $request->job_title . '%');
			}
			if (!empty($request->input('skills'))) {
				$q->whereIn('required_skill', $request->input('skills'));
			}
			if ($request->input('status') == 1 || $request->input('status') == '0') {
				//echo $request->input('status');die;
				$q->where('job_status', (int) $request->input('status'));
			}
			if (!empty($request->input('industry'))) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->where('_id', $request->input('industry'));
				});
			}
			if (!empty($request->input('department'))) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->where('_id', $request->input('department'));
				});
			}

			if (!empty($request->input('salary'))) {
				if (!empty($request->input('salary')['currency'])) {
					$q->whereHas('currency', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['currency']);
					});
				}
				if (!empty($request->input('salary')['min_salary'])) {
					$q->whereHas('minSalary', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['min_salary']);
					});
				}
				if (!empty($request->input('salary')['max_salary'])) {
					$q->whereHas('maxSalary', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['max_salary']);
					});
				}

			}
			if (!empty($request->input_search)) {
				$q->whereHas('employer', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('employerUser', function ($q) use ($request) {
						$q->where('company_name', 'like', '%' . $request->input_search . '%');
					});
				});
			}
			if (!empty($request->search_by_employer)) {
				$bsb_unique_no = ltrim($request->search_by_employer, "BSBEMR");
				$q->whereHas('employer', function ($q) use ($request) {
					$q->where('bsb_unique_no', $bsb_unique_no);
				});
			}

		})
		//->doesntHave('employeeSavedJob')
		//->doesntHave('employeeAppliedJob')
			->where('job_status', 1)
			->where('is_published', 1)
			->orderBy('created_at', 'desc')
			->paginate(25)->appends(request()->query());
		return view('jobs.employer-posted-job', compact('title', 'jobs', 'request', 'skills', 'experiences', 'employer_users', 'allCountry', 'allcity', 'allDeperment', 'allIndustry'));
	}

	public function editJob($id) {
		$title = 'Edit Posted Jobs';
		$job = EmployerJobTxn::find($id);
		$skills = SkillMst::all();
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$selectedCity = CityMst::where('country_code', $job->country->code)->get();
		$allDeperment = DepartmentMst::orderBy('name')->get();
		$allIndustry = IndustryMst::orderBy('name')->get();
		$courses = Course::all();
		$degreesUnderGrad = DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 2);})->get();
		$degreesPostGrad = DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 3);})->get();
		$employementFor = AllOtherMasterMst::where('entity', 'emplyment_for')->get();
		$employementTypes = AllOtherMasterMst::where('entity', 'employement_type')->get();
		$employer_users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->get();
		$experiences = ExperienceMst::all();
		//dd($job->language_id);
		$languages = LanguageMst::all();
		$jbType = 'jobs';
		return view('jobs.employer-job-edit', compact('title', 'job', 'skills', 'allCountry', 'allcity', 'allDeperment', 'allIndustry', 'employer_users', 'courses', 'employementFor', 'experiences', 'languages', 'employementTypes', 'degreesUnderGrad', 'degreesPostGrad', 'selectedCity', 'jbType'));
	}

	public function updateJob(Request $request, $id) {
		$job = EmployerJobTxn::find($id);
		//dd($job);
		try {
			$allRequest = $request->except(['industry_id']);
			$job = EmployerJobTxn::find($id);
			$job->fill($allRequest);
			$job->updated_at = Carbon::now();
			$job->save();
			$job->industry()->detach();
			$job->language()->detach();
			$job->industry()->attach($request->industry_id, ['type' => 'job']);
			$job->language()->attach($request->language_pref, ['type' => 'job']);
			Session::flash('msg', 'Sucessfuly edited');
			return redirect()->route('admin.employer.posted-job');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	public function employeeAppliedJobview(Request $request, $id) {
		$title = 'Applied job view';
		$applied_job = EmployeeAppliedJobsTxn::find($id);
		//echo $applied_job->bsb_jb_app_id;die;
		$type = 'job';
		return view('jobs.employee-applied-job-view', compact('title', 'applied_job', 'type'));
	}

	public function employerJobApplicationReceived(Request $request) {
		$title = 'Job Application Received';
		$job_id = $request->id;
		$job = EmployerJobTxn::find($job_id);
		$job_type = 'jobs';
		if ($job) {
			$applied_jobs = $job->employeeAppliedJob()->orderBy('created_at', 'desc')->paginate(25);

		} else {
			$applied_jobs = [];
		}
		//echo "<pre>";print_r($applied_jobs);die;
		return view('jobs.job-application-received', compact('title', 'applied_jobs', 'job_type'));
	}

	public function employerDeletedJobs(Request $request) {
		$title = 'Employer Deleted Job';
		$jobs = EmployerJobTxn::onlyTrashed()->where(function ($q) use ($request) {

		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('deleted.jobs', compact('title', 'jobs'));
	}

	public function employerDeletedWalkings(Request $request) {
		$title = 'Employer Deleted Walking';
		$jobs = WalkInInterviewTxn::onlyTrashed()->where(function ($q) use ($request) {

		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('deleted.walking', compact('title', 'jobs'));
	}

	public function employerDeletedInternationalJobs(Request $request) {
		$title = 'Employer Deleted International jobs';
		$jobs = InternationalJobTxn::onlyTrashed()->where(function ($q) use ($request) {

		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('deleted.international-job', compact('title', 'jobs'));
	}

	public function expiredJobs(Request $request) {
		$title = 'Employer Expiry Job';
		$now_date = Carbon::now()->format('Y-m-d');
		$jobs = EmployerJobTxn::where(function ($q) use ($request) {

		})->where('job_ageing', '<=', $now_date)->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('expiry.jobs', compact('title', 'jobs'));
	}

	public function expiredWalkings(Request $request) {
		$title = 'Employer Expiry Walking';
		$now_date = Carbon::now()->format('Y-m-d');
		$jobs = WalkInInterviewTxn::onlyTrashed()->where(function ($q) use ($request) {

		})->where('job_ageing', '>=', $now_date)->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('expiry.jobs', compact('title', 'jobs'));
	}

	public function expiredInternationalJobs(Request $request) {
		$title = 'Employer Expiry International';
		$now_date = Carbon::now()->format('Y-m-d');
		$jobs = InternationalJobTxn::where(function ($q) use ($request) {

		})->where('job_ageing', '>=', $now_date)->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('expiry.international-job', compact('title', 'jobs'));
	}

	public function draftJobs(Request $request) {
		$title = 'Employer Draft Job';
		$jobs = EmployerJobTxn::where(function ($q) use ($request) {

		})->where('is_published', 0)->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('draft.jobs', compact('title', 'jobs'));
	}

	public function draftWalkings(Request $request) {
		$title = 'Employer Draft Walking';
		$jobs = WalkInInterviewTxn::onlyTrashed()->where(function ($q) use ($request) {

		})->where('is_published', 0)->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('draft.jobs', compact('title', 'jobs'));
	}

	public function draftInternationalJobs(Request $request) {
		$title = 'Employer Draft International Job';
		$jobs = InternationalJobTxn::where(function ($q) use ($request) {

		})->where('is_published', 0)->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return View('draft.international-job', compact('title', 'jobs'));
	}

}
