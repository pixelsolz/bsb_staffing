<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AllOtherMasterMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class OtherMasterController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Other Master List';
		$other_msts = AllOtherMasterMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('name', 'like', '%' . $request->q . '%');
				$q->orWhere('entity', 'like', '%' . $request->q . '%');
			}
		})->paginate(25)->appends(request()->query());
		return view('other-master.index', compact('title', 'other_msts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Other Master Create';
		return view('other-master.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			//'company_type.required' => 'The company type field required.',
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'entity' => 'required',
			'orders' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			AllOtherMasterMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.other-master-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Language Edit';
		$other_mst = AllOtherMasterMst::find($id);
		return view('other-master.edit', compact('title', 'other_mst'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			/*'name.required' => 'The name field required.',
            'attribute_id.required' => 'The attribute field required.',*/
		];

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'entity' => 'required',
			'orders' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$othermst = AllOtherMasterMst::find($id);
			$othermst->fill($alldata);
			$othermst->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.other-master-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$language = AllOtherMasterMst::find($id);
		$language->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
