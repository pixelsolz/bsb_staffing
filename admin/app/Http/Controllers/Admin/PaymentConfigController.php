<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CityMst;
use App\Models\PaymentConfig;

use App\Models\Role;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Session;
use Validator;

class PaymentConfigController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Other User List';
		$payment_config = PaymentConfig::first();
		//echo $payment_config->_id ;die;
		return view('payment-config.index', compact('title','payment_config'));
	}
	public function paymentStatusChange(Request $request){
		$value = $request->chekval;
		if($request->id ==''){
			$payment_config = new PaymentConfig;
			$payment_config->international_walk_in_interview = $value === 'international_walk_in_interview'? 1:0;
			$payment_config->premium_job_post = $value === 'premium_job_post'? 1:0;
			$payment_config->cv_download_view = $value === 'cv_download_view'? 1:0;
			$payment_config->mass_mail_service = $value === 'mass_mail_service'? 1:0;
			$payment_config->employee = $value === 'employee'? 1:0;
			$payment_config->employer = $value === 'employer'? 1:0;
			$payment_config->save();
		}else{
			$payment_config =PaymentConfig::find($request->id);
			$payment_config->$value = $request->status;
			$payment_config->save();
		}

		return response()->json(['status' => 200]);

	}
	
	
}
