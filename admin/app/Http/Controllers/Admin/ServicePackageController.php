<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\ServicesMst;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class ServicePackageController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'Service Package List';
		$servicesAll = ServicesMst::paginate(25)->appends(request()->query());
		return view('service-package.index', compact('title', 'servicesAll'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Service Package Create';
		$allRole = Role::all();
		return view('service-package.create', compact('title', 'allRole'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$message = [
			'entity_id.required' => 'The Applicable To field required.',
		];
		$validator = Validator::make($request->all(), [
			'service_type' => 'required',
			'service_name' => 'required',
			'quantity' => 'required',
			'fees' => 'required',
			'service_life' => 'required',
			'entity_id' => 'required',
			'order_of_results' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			$user = ServicesMst::create($request->all() + ['created_by' => Auth::guard('admin')->user()->id, 'status' => 1]);

			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.service-package.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Service Package Edit';
		$service_package = ServicesMst::find($id);
		$allRole = Role::all();
		return view('service-package.edit', compact('title', 'allRole', 'service_package'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$message = [
			'entity_id.required' => 'The Applicable To field required.',
		];
		$validator = Validator::make($request->all(), [
			'service_type' => 'required',
			'service_name' => 'required',
			'quantity' => 'required',
			'fees' => 'required',
			'service_life' => 'required',
			'entity_id' => 'required',
			'order_of_results' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			$service = ServicesMst::find($id);
			$service->fill($request->all());
			$service->save();
			Session::flash('msg', 'Sucessfuly updated');
			return redirect()->route('admin.service-package.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
