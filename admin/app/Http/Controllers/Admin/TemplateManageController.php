<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\EmployerEmailTemplateTxn;
use Illuminate\Http\Request;
use Session;

class TemplateManageController extends Controller {

	public function employerEmailTemplates(Request $request) {
		$title = 'Employer Email Template';
		$templates = EmployerEmailTemplateTxn::where(function ($q) use ($request) {
			if (!empty($request->input_search)) {
				$q->where('template_name', 'like', '%' . $request->input_search . '%');
				$q->orWhereHas('createdUser', function ($q) use ($request) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
				});
			}
		})->orderBy('updated_at', 'desc')->paginate(25)->appends(request()->query());
		return view('templates.email_template_list', compact('title', 'templates', 'request'));
	}

	public function editEmailTemplate($id) {
		$title = 'Edit Email Template';
		$template = EmployerEmailTemplateTxn::find($id);
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		return view('templates.email_temp_edit', compact('title', 'template', 'allCountry', 'allcity'));
	}

	public function updateEmailTemplate(Request $request, $id) {
		$template = EmployerEmailTemplateTxn::find($id);
		$template->reviewed_flag = $request->reviewed_flag;
		$template->save();
		Session::flash('msg', 'Sucessfully updated');
		return redirect('email-template/list');
	}
}
