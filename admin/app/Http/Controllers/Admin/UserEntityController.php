<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserEntityMst;
use Auth;
use Illuminate\Http\Request;
use Session;
use Validator;

class UserEntityController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$title = 'User Entity List';
		$userEntites = UserEntityMst::where(function ($q) use ($request) {
			if ($request->has('q') && !empty($request->input('q'))) {
				$q->where('entity_name', 'like', '%' . $request->q . '%');
			}
		})->paginate(25)->appends(request()->query());
		return view('user-entity.index', compact('title', 'userEntites'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'User Entity Create';
		return view('user-entity.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			//'company_type.required' => 'The company type field required.',
		];

		$validator = Validator::make($request->all(), [
			'entity_name' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$allRequest = $request->all();
			UserEntityMst::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id]);
			Session::flash('msg', 'Successfully created');
			return redirect()->route('admin.userentity-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'User Entity Edit';
		$user_entity = UserEntityMst::find($id);
		return view('user-entity.edit', compact('title', 'user_entity'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$message = [
			/*'name.required' => 'The name field required.',
            'attribute_id.required' => 'The attribute field required.',*/
		];

		$validator = Validator::make($request->all(), [
			'entity_name' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$alldata = $request->all();
			$user_entity = UserEntityMst::find($id);
			$user_entity->fill($alldata);
			$user_entity->save();
			Session::flash('msg', 'Successfully Updated');
			return redirect()->route('admin.userentity-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$user_entity = UserEntityMst::find($id);
		$user_entity->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
}
