<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\Course;
use App\Models\EmployerJobTxn;
use App\Models\IndustryMst;
use App\Models\InternalEmployeeGovtId;
use App\Models\InternalEmployeeProfileTxn;
use App\Models\Role;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;

class UserManageController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$user_role_type = request()->segment(2);
		$title = ucfirst(request()->segment(2)) . ' User List';
		$countries = CountryMst::orderBy('name')->get();
		$courses = Course::orderBy('name')->get();
		$cvCountTodate = '';
		$cvCountFromdate = '';

		if (!empty($request->input('search_by_cv_type'))) {
			if (!empty($request->input('from_cv_date')) && !empty($request->input('to_cv_date'))) {
				$start_date = Carbon::createFromDate($request->input('from_cv_date'));
				$end_date = Carbon::createFromDate($request->input('to_cv_date'));
				if ($request->input('search_by_cv_type') == 'by_upload_cv') {
					$cvCountTodate = $start_date;
					$cvCountFromdate = $end_date;
				} else {
					//$q->whereBetween('last_login', array($start_date, $end_date));
				}

			}
		}

		$users = User::whereHas('roles', function ($q) {
			//$q->whereNotIn('slug', ['employee', 'employer']);
			$q->where('slug', request()->segment(2));
		})->where(function ($q) use ($request) {
			if (!empty($request->search)) {
				if (!empty($request->input('search_by_date_type'))) {

					if (!empty($request->input('from_joined_date')) && !empty($request->input('to_joined_date'))) {

						$start_date = Carbon::createFromDate($request->input('from_joined_date'));
						$end_date = Carbon::createFromDate($request->input('to_joined_date'));
						if ($request->input('search_by_date_type') == 'created_date') {
							$q->whereBetween('created_at', array($start_date, $end_date));
						} else {
							$q->whereBetween('last_login', array($start_date, $end_date));
						}

					}
				}

				if ($request->input('status') == 1 || $request->input('status') == '0') {
					//echo $request->input('status');die;
					$q->where('status', (int) $request->input('status'));
				}
				if (request()->segment(2) == 'franchise') {
					if (!empty($request->search_by_country)) {
						$q->whereHas('franchiseUser', function ($q) use ($request) {
							$q->where('country_id', $request->search_by_country);
						});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('franchiseUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->gender)) {
						$q->whereHas('franchiseUser', function ($q) use ($request) {
							$q->where('gender', $request->gender);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('franchiseUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('comp_name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

				if (request()->segment(2) == 'agency') {
					if (!empty($request->search_by_country)) {
						$q->whereHas('agencyUser', function ($q) use ($request) {
							$q->where('country_id', $request->search_by_country);
						});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('agencyUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->gender)) {
						$q->whereHas('agencyUser', function ($q) use ($request) {
							$q->where('gender', $request->gender);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('agencyUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('comp_name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

				if (request()->segment(2) == 'college') {
					if (!empty($request->search_by_country)) {
						$q->whereHas('collegeUser', function ($q) use ($request) {
							$q->where('country_id', $request->search_by_country);
						});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('collegeUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->gender)) {
						$q->whereHas('collegeUser', function ($q) use ($request) {
							$q->where('gender', $request->gender);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('collegeUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

				if (request()->segment(2) == 'trainer') {
					if (!empty($request->search_by_country)) {
						$q->whereHas('trainerUser', function ($q) use ($request) {
							$q->where('country_id', $request->search_by_country);
						});
					}
					if (!empty($request->gender)) {
						$q->whereHas('trainerUser', function ($q) use ($request) {
							if ($request->gender == 'male') {
								$q->where('gender', '1');
							} else {
								$q->where('gender', '2');
							}

						});
					}
					if (!empty($request->search_by_city)) {
						$q->whereHas('trainerUser', function ($q) use ($request) {
							$q->where('city_id', $request->search_by_city);
						});
					}
					if (!empty($request->input_search)) {
						$q->where('email', 'like', '%' . $request->input_search . '%');
						$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
						//$q->orWhere('bsb_user_no', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('trainerUser', function ($q) use ($request) {
							$q->where('first_name', 'like', '%' . $request->input_search . '%');
							$q->orWhere('name', 'like', '%' . $request->input_search . '%');
						});
					}
				}

			}
		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		//echo "<pre>";print_r($users);die;
		return view('user-manage.index', compact('title', 'users', 'countries', 'request', 'user_role_type', 'courses', 'cvCountTodate', 'cvCountFromdate'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Other User create';
		$allCountry = CountryMst::all();
		$allRole = Role::whereNotIn('slug', ['employee', 'employer'])->get();
		$allcity = CityMst::all();
		$createUser = 'other';
		return view('user-manage.create', compact('title', 'allCountry', 'allRole', 'allcity', 'createUser'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());

		$message = [
			'f_name.required' => 'The first name field required.',
			'l_name.required' => 'The last name field required.',
			'role_ids.required' => 'The role field required.',
		];

		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			'user_name' => 'required',
			'email' => 'required',
			'phone_no' => 'required',
			'role_ids' => 'required',
			'password' => 'required|confirmed|min:6',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {

			$allRequest = $request->except(['role_ids', 'password']);

			$user = User::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'password' => hash::make($request->password)]);
			//$user = User::create($request->all()+['created_by'=>Auth::guard('admin')->user()->id,'password'=>$password]);
			//$user = User::create($request->only(['f_name', 'l_name', 'email']) + ['password' => bcrypt('user123')]);
			$user->roles()->attach($request->role_ids);

			Session::flash('msg', 'Sucessfully created');
			return redirect()->route('admin.user-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id, $user_type) {
		//$user_type;die;
		$title = ucfirst($user_type) . ' User View';
		$user = User::find($id);
		$user_role = $user->roles()->first()->slug;
		return view('user-manage.view', compact('title', 'user', 'user_role'));
		//echo "<pre>";print_r($user);die;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'User Edit';
		$user = User::find($id);
		$isEmployer = $user->isEmployer();
		$allCountry = CountryMst::all();
		$allRole = Role::all();
		$allcity = CityMst::all();
		//$updateUser = 'other';
		$updateUser = $user->roles[0]->slug;
		return view('user-manage.edit', compact('title', 'user', 'allCountry', 'allRole', 'allcity', 'updateUser'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$message = [
			'f_name.required' => 'The first name field required.',
			'l_name.required' => 'The last name field required.',
		];

		$validator = Validator::make($request->all(), [
			'f_name' => ($request->user_type == 'employee' || $request->user_type == 'employer') ? 'required' : '',
			'l_name' => ($request->user_type == 'employee' || $request->user_type == 'employer') ? 'required' : '',
			'user_name' => 'required',
			'email' => 'required',
			'mobile' => 'required',
			'password' => !empty($request->password) ? 'required|confirmed|min:6' : '',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			$user = User::find($id);
			$user->fill($request->except(['password', 'password_confirmation']));
			if ($request->has('password') && !empty($request->password)) {
				$user->password = bcrypt($request->password);
			}
			$user->save();
			Session::flash('msg', 'Sucessfuly updated');
			if ($request->input('user_type') == 'employee') {
				return redirect()->route('admin.user-manage.employee');
			} elseif ($request->input('user_type') == 'employer') {
				return redirect()->route('admin.user-manage.employer');
			} else {
				$url = 'user-manage/' . $request->input('user_type');
				return redirect($url);
				//return redirect()->route('admin.user-manage.index');
			}
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$user = User::find($id);
			//$user->delete();
			$user->forceDelete();
			//$user->roles()->detach();
			return response()->json(['status' => 200, 'status_text' => 'Successfuly deleted']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function employeeUser(Request $request) {
		$title = 'Employee List';
		$countries = CountryMst::orderBy('name')->get();
		$industries = IndustryMst::whereNotNull('name')->where('name', '!=', "")->orderBy('name')->get();
		$users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->where(function ($q) use ($request) {
			if (!empty($request->institute_code)) {
				$q->whereHas('employeeUser', function ($q) use ($request) {
					$q->where('institute_code', $request->institute_code);
				});
			}

			if (!empty($request->search)) {
				if (!empty($request->input_search)) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('employeeUser', function ($q) use ($request) {
						$q->where('first_name', 'like', '%' . $request->input_search . '%');
					});
				}

				if (!empty($request->input('from_joined_date')) && !empty($request->input('to_joined_date'))) {
					$start_date = Carbon::createFromDate($request->input('from_joined_date'));
					$end_date = Carbon::createFromDate($request->input('to_joined_date'));
					$q->whereBetween('created_at', array($start_date, $end_date));
				}
				if (!empty($request->input('from_login_date')) && !empty($request->input('to_login_date'))) {
					$start_date = Carbon::createFromDate($request->input('from_login_date'));
					$end_date = Carbon::createFromDate($request->input('to_login_date'));
					$q->whereBetween('last_login', array($start_date, $end_date));
				}
				if (!empty($request->search_by_country)) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('country_id', $request->search_by_country);
					});
				}
				if (!empty($request->industry_id)) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('industry_id', $request->industry_id);
					});
				}
				if (!empty($request->search_by_city)) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('city_id', $request->search_by_city);
					});
				}

				if ($request->input('status') == 1 || $request->input('status') == '0') {
					$q->where('status', (int) $request->input('status'));
				}

				if (!empty($request->gender)) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						if ($request->gender == 1) {
							$q->where('gender', $request->gender);
							$q->orWhere('gender', 'male');
						} elseif ($request->gender == 2) {
							$q->where('gender', $request->gender);
							$q->orWhere('gender', 'female');
						}

					});
				}
				if (!empty($request->employee_type)) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						if ($request->employee_type == 2) {
							$q->where('form_no', 1);
						} else {
							$q->where('form_no', '!=', 1);
						}
					});
				}
				if (!empty($request->input('salary_from')) && !empty($request->input('salary_to')) && !empty($request->input('search_by_salary_type')) && !empty($request->input('search_by_currency'))) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary_currency', $request->input('search_by_currency'));
						$q->where('current_salary_type', $request->input('search_by_salary_type'));
						$q->whereBetween('current_salary', array((int) $request->input('salary_from'), (int) $request->input('salary_to')));
					});
				}

			}
		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());

		return view('user-manage.employe-user', compact('title', 'users', 'countries', 'request', 'industries'));
	}

	public function createEmployee() {
		$title = 'Employee create';
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$createUser = 'employee';
		return view('user-manage.create', compact('title', 'allCountry', 'createUser', 'allcity'));
	}

	public function editEmployee($id) {
		$title = 'Employee Edit';
		$user = User::find($id);
		$isEmployer = $user->isEmployer();
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$updateUser = 'employee';
		return view('user-manage.edit', compact('title', 'user', 'allCountry', 'updateUser', 'allcity'));
	}

	public function employerUser(Request $request) {
		$title = 'Employer List';
		$countries = CountryMst::all();
		$industries = IndustryMst::whereNotNull('name')->where('name', '!=', "")->orderBy('name')->get();
		$users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->where(function ($q) use ($request) {
			if (!empty($request->search)) {
				if (!empty($request->input_search)) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('employerUser', function ($q) use ($request) {
						$q->where('first_name', 'like', '%' . $request->input_search . '%');
						$q->orWhere('company_name', 'like', '%' . $request->input_search . '%');
					});
				}
				if (!empty($request->input('from_joined_date')) && !empty($request->input('to_joined_date'))) {
					//echo $request->input('start_date');die;
					$start_date = Carbon::createFromDate($request->input('from_joined_date'));
					$end_date = Carbon::createFromDate($request->input('to_joined_date'));
					//echo $start_date;die;
					$q->whereBetween('created_at', array($start_date, $end_date));
				}
				if (!empty($request->input('from_login_date')) && !empty($request->input('to_login_date'))) {
					$start_date = Carbon::createFromDate($request->input('from_login_date'));
					$end_date = Carbon::createFromDate($request->input('to_login_date'));
					$q->whereBetween('last_login', array($start_date, $end_date));
				}
				if (!empty($request->search_by_country)) {
					$q->whereHas('employerUser', function ($q) use ($request) {
						$q->where('country_id', $request->search_by_country);
					});
				}
				if (!empty($request->search_by_city)) {
					$q->whereHas('employerUser', function ($q) use ($request) {
						$q->where('city_id', $request->search_by_city);
					});
				}
				if (!empty($request->search_by_industry)) {
					$q->wherehas('employerUser', function ($q) use ($request) {
						$q->where('industry_id', $request->search_by_industry);
					});
				}
				if (!empty($request->gender)) {
					$q->whereHas('employerUser', function ($q) use ($request) {
						$q->where('gender', $request->gender);

					});
				}

				if (!empty($request->employer_type)) {
					$q->whereHas('employerUser', function ($q) use ($request) {
						if ($request->employer_type == 1) {
							$q->whereNotNull('license_agree');
						} else {
							$q->whereNull('license_agree');
						}
					});
				}
			}
		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		return view('user-manage.employeer-user', compact('title', 'users', 'countries', 'industries', 'request'));
	}

	public function createEmployer() {
		$title = 'Employer create';
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$createUser = 'employer';
		return view('user-manage.create', compact('title', 'allCountry', 'createUser', 'allcity'));
	}

	public function editEmployer($id) {
		$title = 'Employer Edit';
		$user = User::find($id);
		$isEmployer = $user->isEmployer();
		$allCountry = CountryMst::all();
		$allcity = CityMst::all();
		$updateUser = 'employer';
		return view('user-manage.edit', compact('title', 'user', 'allCountry', 'updateUser', 'allcity'));
	}

	public function getCityAjax(Request $request) {
		$country = CountryMst::find($request->id);
		$cities = CityMst::where('country_code', $country->code)->get();
		return response()->json(['status' => 200, 'data' => $cities]);
	}

	public function getEmployeeSavedJob(Request $request, $id) {
		$title = 'Employee Saved Job';
		$employee = User::find($id);
		$jobs = $employee->employeeSavedJob()->whereNull('employee_application_id')->paginate(10)->appends(request()->query());
		$type = 'saved-job';
		return view('user-manage.employee-job', compact('title', 'jobs', 'type', 'request'));
	}

	public function employerJobStatusChange(Request $request) {
		$job = EmployerJobTxn::find($request->id);
		$job->job_status = $request->status;
		$job->save();
		return response()->json(['status' => 200]);
	}

	public function showEmployer($id) {
		$title = 'Employer View';
		$user = User::where('_id', $id)->whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->first();
		return view('user-manage.employer-view', compact('title', 'user'));
		//echo "<pre>";print_r($user);die;
	}
	public function showEmployee($id) {
		$title = 'Employee View';
		$user = User::where('_id', $id)->whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->first();
		//echo "<pre>";print_r($user);die;
		return view('user-manage.employee-view', compact('title', 'user'));
	}

	public function showDeletedUser(Request $request) {
		//echo "test";die;
		$title = 'Deleted User List';
		$deleted_user = User::onlyTrashed()->paginate(25)->appends(request()->query());
		return view('user-manage.deleted-user', compact('title', 'deleted_user'));

	}

	public function restoreDeletedUser($id) {
		try {
			$user = User::withTrashed()->find($id);
			//$user->roles()->attach($user->role_id);
			//echo "<pre>";print_r($user);die;
			$user->restore();
			Session::flash('msg', 'Sucessfully Restore User');
			return redirect()->route('admin.user-manage.deleted-user');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	public function showAllInternalEmployee(Request $request) {
		$title = 'Internal Employer View';
		$countries = CountryMst::all();
		$users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'internal_employee');
		})->where(function ($q) use ($request) {
			if (!empty($request->search)) {
				if (!empty($request->input('from_joined_date')) && !empty($request->input('to_joined_date'))) {
					//echo $request->input('start_date');die;
					$start_date = Carbon::createFromDate($request->input('from_joined_date'));
					$end_date = Carbon::createFromDate($request->input('to_joined_date'));
					//echo $start_date;die;
					$q->whereBetween('created_at', array($start_date, $end_date));
				}
				if (!empty($request->search_by_country)) {
					$q->whereHas('internalEmployeeUser', function ($q) use ($request) {
						$q->where('country_id', $request->search_by_country);
					});
				}
				if (!empty($request->search_by_city)) {
					$q->whereHas('internalEmployeeUser', function ($q) use ($request) {
						$q->where('city_id', $request->search_by_city);
					});
				}
				if (!empty($request->referrel_code)) {
					$q->where('user_unique_code', 'like', '%' . $request->referrel_code . '%');
				}

				if (!empty($request->input_search)) {
					$q->where('email', 'like', '%' . $request->input_search . '%');
					$q->orWhere('phone_no', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('internalEmployeeUser', function ($q) use ($request) {
						$q->where('first_name', 'like', '%' . $request->input_search . '%');
						//$q->where('company_name', 'like', '%' . $request->input_search . '%');
					});
				}
			}
		})->orderBy('created_at', 'desc')->paginate(25)->appends(request()->query());
		//echo "<pre>";print_r($users);
		return view('user-manage.internal-employee', compact('title', 'users', 'countries', 'request'));
	}
	public function createInternalEmployee() {
		$title = 'Internal Employer Create';
		$allCountry = CountryMst::all();
		$allRole = Role::whereNotIn('slug', ['employee', 'employer'])->get();
		$allcity = CityMst::all();
		return view('user-manage.create-internal-employee', compact('title', 'allCountry', 'allRole', 'allcity'));
	}
	public function storeInternalEmployee(Request $request) {
		//dd($request->all());
		//InternalEmployeeProfileTxn
		$message = [
			'f_name.required' => 'The first name field required.',
			'l_name.required' => 'The last name field required.',
			//'middle_name.required' => 'The middle name field required.',
		];

		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			//'middle_name' => 'required',
			'user_name' => 'required|unique:users',
			'email' => 'required|email|unique:users',
			'user_role' => 'required',
			'date_of_joining' => 'required',
			'phone_no' => 'required',
			'password' => 'required|confirmed|min:6',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			$user_unique_cd = 'bsbintemp' . '' . substr(md5(mt_rand()), 0, 6);
			$allRequest = $request->except(['password', 'role_id']);

			$user = User::create($allRequest + ['created_by' => Auth::guard('admin')->user()->id, 'password' => hash::make($request->password), 'user_unique_code' => $user_unique_cd]);
			$user->roles()->attach($request->role_id);

			$internalEmpPrf = InternalEmployeeProfileTxn::create(['user_id' => $user->_id, 'first_name' => $request->f_name, 'last_name' => $request->l_name, 'middle_name' => $request->middle_name, 'country_id' => $request->country_id, 'city_id' => $request->citi_id, 'user_role' => $request->user_role, 'office_name' => $request->office_name, 'address' => $request->address, 'date_of_joining' => $request->date_of_joining, 'designation' => $request->designation]);
			if ($request->hasFile('govt_id')) {
				/*$file = $request->file('govt_id');
					$name = time() . '.' . $file->getClientOriginalExtension();
					$filePath = '/upload_files/internal_employcity_idee/' . $name;
					Storage::disk('s3')->put($filePath, file_get_contents($file));
					$internalEmpPrf->govt_id = $name;
				*/
				foreach ($request->file('govt_id') as $key => $image) {
					//$name = time() . $file->getClientOriginalName();
					$name = $key . '_' . time() . '.' . $image->getClientOriginalExtension();

					$new_name = $image->storePublicly(
						'upload_files/internal_employee/govtId'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/internal_employee/govtId' . '/', '', $new_name);
					}

					$emp_photo = InternalEmployeeGovtId::create(['user_id' => $user->id, 'file_name' => $filename]);
				}
			}
			Session::flash('msg', 'Sucessfully created');
			return redirect()->route('admin.user-manage.list.internal-employee');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}
	public function editInternalEmployee($id) {
		$title = 'Internal Employer Edit';
		$user = User::find($id);
		$allCountry = CountryMst::all();
		$allRole = Role::whereNotIn('slug', ['employee', 'employer'])->get();
		$allcity = CityMst::all();
		return view('user-manage.edit-internal-employee', compact('title', 'allCountry', 'allRole', 'allcity', 'user'));
	}

	public function showInternalEmployee($id) {
		$title = 'Internal Employer View';
		$user = User::find($id);
		$allCountry = CountryMst::all();
		$allRole = Role::whereNotIn('slug', ['employee', 'employer'])->get();
		$allcity = CityMst::all();
		return view('user-manage.staff-view', compact('title', 'allCountry', 'allRole', 'allcity', 'user'));
	}

	public function updateInternalEmployee(Request $request, $id) {

		$message = [
			'first_name.required' => 'The first name field required.',
			'last_name.required' => 'The last name field required.',
			'middle_name.required' => 'The middle name field required.',
		];

		$validator = Validator::make($request->all(), [
			'first_name' => 'required',
			'last_name' => 'required',
			'middle_name' => 'required',
			'user_name' => 'required|unique:users' . $id,
			'email' => 'required|email|unique:users' . $id,
			'user_role' => 'required',
			'date_of_joining' => 'required',
			'phone_no' => 'required',
			'password' => !empty($request->password) ? 'required|confirmed|min:6' : '',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			//echo $id;die;
			//dd($request->all);
			$user = User::find($id);
			$user = $user->fill($request->only(['email', 'phone_no', 'status', 'updated_at']));
			if (!empty($request->input('password'))) {
				$user->password = bcrypt($request->input('password'));
			}
			$user->save();
			$internal_employee_details = $user->internalEmployeeUser->fill($request->only(['first_name', 'last_name', 'middle_name', 'address', 'country_id', 'city_id', 'user_role', 'office_name', 'date_of_joining', 'designation']));
			if ($request->hasFile('govt_id')) {
				/*$file = $request->file('govt_id');
					$name = time() . '.' . $file->getClientOriginalExtension();
					$filePath = '/upload_files/internal_employcity_idee/' . $name;
					Storage::disk('s3')->put($filePath, file_get_contents($file));
					$internalEmpPrf->govt_id = $name;
				*/
				foreach ($request->file('govt_id') as $key => $image) {
					//$name = time() . $file->getClientOriginalName();
					$name = $key . '_' . time() . '.' . $image->getClientOriginalExtension();
					$new_name = $image->storePublicly(
						'upload_files/internal_employee/govtId'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/internal_employee/govtId' . '/', '', $new_name);
					}

					$emp_photo = InternalEmployeeGovtId::create(['user_id' => $user->id, 'file_name' => $filename]);
				}
			}
			$internal_employee_details->save();
			Session::flash('msg', 'Sucessfully Update');
			return redirect()->route('admin.user-manage.list.internal-employee');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	public function internalEmpdestroy($id) {
		try {
			$interEmpGovtId = InternalEmployeeGovtId::find($id);
			$interEmpGovtId->delete();
			//$user->roles()->detach();
			return response()->json(['status' => 200, 'status_text' => 'Successfuly deleted']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function getTrainerSchedule(User $trainer, $type, Request $request) {
		$title = 'Trainer Schedule ' . $type;

		if ($type == 'upcoming') {
			$data = $trainer->trainerSchedule()->has('bookedSchedule')->where('training_actual_timestamp', '>=', Carbon::now()->timestamp)->orderBy('created_at', 'desc')->paginate(10)->appends(request()->query());
			return view('user-manage.trainer-schedule', compact('title', 'data', 'request', 'type'));

		} else {
			$data = $trainer->trainerSchedule()->has('bookedSchedule')->where('training_actual_timestamp', '<', Carbon::now()->timestamp)->orderBy('created_at', 'desc')->paginate(10)->appends(request()->query());
			return view('user-manage.trainer-schedule', compact('title', 'data', 'request', 'type'));

		}

	}

	public function checkEmailexist() {
		return User::where('email', 'info.sihmedu@gmail.com')->first();
	}

}
