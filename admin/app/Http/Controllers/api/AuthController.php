<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\Mail\SendMail;
use App\Models\AgencyDetail;
use App\Models\CollegeDetail;
use App\Models\EmployeeCvTxn;
use App\Models\EmployeePrefTxn;
use App\Models\EmployeeProfileTxn;
use App\Models\EmployerGallery;
use App\Models\EmployerProfileTxn;
use App\Models\FranchiseDetail;
use App\Models\PasswordReset;
use App\Models\Role;
use App\Models\TrainerDetail;
use App\Models\User;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;
use Mail;
//use Tymon\JWTAuth\Facades\JWTAuth;

//use JWTAuth;
//use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class AuthController extends Controller {

	public function registration(Request $request) {
		//return($request->all()); die();

		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'first_name' => 'required',
			'last_name' => 'required',
			//'country_id' => 'required',
			//'city_id' => 'required',
			//'industry_id' => 'required',
			'upload_cv' => $request->isEmployer != 1 ? 'required' : '',
			//'user_name' => 'required|unique:users',
			'email' => 'required|email|unique:users',
			//'mobile' => 'required',
			'password' => 'required|min:6',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		$userCode = '';

		if ( /*$request->isEmployer == 2 &&*/$request->input('user_unique_code') != 'null' && !empty($request->input('user_unique_code'))) {
			$userCode = User::where('user_unique_code', $request->input('user_unique_code'))->first();
			if (!$userCode && $request->isEmployer != 1) {
				return response()->json(['status' => 421, 'error' => 'User code does not matched']);
			}
		}

		$setUserName = $request->first_name . '' . substr(md5(mt_rand()), 0, 7);
		$registered_token = substr(number_format(time() * rand(), 0, '', ''), 0, 6);
		$bsbUniqueNo = User::count();
		$user = User::create(['user_name' => $setUserName, 'email' => strtolower($request->email), 'phone_no' => $request->mobile ? $request->mobile : '', 'password' => bcrypt($request->password), 'profile_update' => '0', 'register_token' => $registered_token, 'bsb_unique_no' => ($bsbUniqueNo + 1), 'status' => 1]);
		//$token = JWTAuth::fromUser($user);
		$token = '';
		if ($request->isEmployer == 1) {
			EmployerProfileTxn::create(['user_id' => $user->_id, 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'company_name' => $request->company_name, 'address' => $request->office_address, 'country_id' => $request->country_id, 'city_id' => $request->city_id, 'designation' => $request->designation, 'industry_id' => $request->industry_id, 'company_size' => $request->company_size, 'dob_date' => $request->dob_date, 'dob_month' => $request->dob_month, 'dob_year' => $request->dob_year, 'gender' => $request->gender, 'company_type' => $request->company_type, 'referral_code' => $userCode ? $userCode->user_unique_code : '']);
		} else {
			$user->emp_name = $request->first_name . ' ' . $request->last_name;
			$user->save();
			$prefTxn = EmployeeProfileTxn::create(['user_id' => $user->_id, 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'institute_code' => $userCode ? $userCode->user_unique_code : '', 'form_no' => 1]);
			/*if ($request->hasFile('upload_cv')) {
				$file = $request->file('upload_cv');
				$new_name = time() . '_' . str_replace(' ', '_', $file->getClientOriginalName());
				$filePath = '/upload_files/employee-resumes/' . $new_name;
				Storage::disk('s3')->put($filePath, file_get_contents($file));
				//$file->move(public_path('upload_files/employee-resumes'), $new_name);
				$employeeCvTxn = EmployeeCvTxn::create(['employee_id' => $user->_id, 'cv_path' => $new_name]);
			}*/
			if (!empty($request->upload_cv)) {
				$cv_path = $request->upload_cv;
				EmployeeCvTxn::create(['employee_id' => $user->_id, 'cv_path' => $cv_path]);
			}
		}

		$userType = ($request->isEmployer == 1) ? 'employer' : 'employee';

		$role = Role::where('slug', $userType)->first();
		//RoleUser::create(['user_id' => $user->_id, 'role_id' => $role->_id]);

		$user->roles()->attach($role->_id);
		if ($request->isEmployer == 1) {
			$data = ['text' => 'User Register', 'register_token' => $registered_token, 'user_name' => $request->first_name];
			$view = 'emails.employer-registration';
			$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';
			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
		} /*else {
			$data = ['text' => 'User Register', 'register_token' => $registered_token, 'user_name' => $request->first_name];
			$view = 'emails.employee-registration';
			$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';
			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
		}*/
		//login user
		if (!$token = auth('api')->attempt(['email' => strtolower($request->email), 'password' => $request->password/*, 'status' => 1*/])) {
			// if the credentials are wrong we send an unauthorized error in json format
			return response()->json(['status' => 401, 'status_text' => 'Invalid credentails!']);
		}
		$user = auth('api')->user();
		$user->last_login = Carbon::now()->toDateTimeString();
		$user->community_token = uniqid(base64_encode(str_random(60)));
		$user->save();

		return response()->json([
			'token' => $token,
			'user' => new UserResource(auth('api')->user()),
			'status' => 200,
			'status_text' => 'Succesfully login!',
			'type' => 'bearer', // you can ommit this
			'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration

		]);

		//return response()->json(['data' => $user, 'token' => $token, 'status' => 200]);

	}

	public function userMailVerificationSend(Request $request) {

		$validator = Validator::make($request->all(), [
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:6',
			'upload_cv' => $request->isEmployer != 1 ? 'required|mimes:doc,docx,pdf' : '',

		], []);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		$cv_path = '';
		if ($request->hasFile('upload_cv')) {
			$file = $request->file('upload_cv');
			$new_name = $request->file('upload_cv')->storePublicly(
				'upload_files/employee-resumes'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/employee-resumes' . '/', '', $new_name);
			}
			$cv_path = $filename;
		}

		$token = substr(number_format(time() * rand(), 0, '', ''), 0, 6);
		if ($request->isEmployer == 1) {
			$view1 = 'emails.emp_employer_mailVerify';
			$subject1 = 'Your application is pending with bsbstaffing.com for email verification registration process';
			Mail::to($request->email)->send(new SendMail(['user_name' => $request->first_name, 'token' => $token, 'cv_path' => $cv_path], $view1, $subject1));
		} else {
			$view1 = 'emails.employee_mailVerification';
			$subject1 = 'Your application is pending with bsbstaffing.com for email verification registration process';
			Mail::to($request->email)->send(new SendMail(['user_name' => $request->first_name, 'token' => $token, 'cv_path' => $cv_path], $view1, $subject1));
		}

		return response()->json(['status' => 200, 'token' => $token, 'cv_path' => $cv_path]);

	}

	public function collegeRegistration(Request $request) {
		$messages = [
			'name.required' => 'Name is required',
			'address.required' => 'Address is required',
			'contact_person_email.required' => 'Contact person email is required',
			'contact_person_email.unique' => 'Email is exist!',
			'contact_person_name.required' => 'Contact person first name is required',
			'contact_person_lastname.required' => 'Contact person last name is required',
			'contact_person_phone.required' => 'Contact Person phone is required',
			'date_of_birth.required' => 'Date Of Birth is required',
			'gender.required' => 'Gender is required',
			'country_id.required' => 'Country is required',
			'city_id.required' => 'City is required',
			'total_student.required' => 'Total Student is required',
			'total_student_place_per_year.required' => 'Total student place per year required',
			'contact_person_designation.required' => 'Contact person designation required',
			'courses.required' => 'Courses is required',
			'declaration_user.required' => 'Declaration checkbox is required',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'address' => 'required',
			'courses' => 'required',
			'about_college' => 'required|between:10,1000',
			'special_note_employer' => 'required|between:10,1000',
			'mission' => 'required|between:10,1000',
			'vision' => 'required|between:10,1000',
			'contact_person_email' => 'required|email|unique:users,email',
			'contact_person_name' => 'required',
			'contact_person_lastname' => 'required',
			'contact_person_phone' => 'required|min:8|max:20|regex:/^(\\+)?(\\d+)$/',
			'date_of_birth' => 'required',
			'gender' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'total_student' => 'required',
			'total_student_place_per_year' => 'required',
			'contact_person_designation' => 'required',
			'declaration_user' => 'required',

		], $messages);
		if ($validator->fails()) {

			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		if ($request->input('about_college')) {
			$request['about_college'] = $request->about_college_copy;
		}

		if ($request->input('special_note_employer')) {
			$request['special_note_employer'] = $request->special_note_employer_copy;
		}

		if ($request->input('mission')) {
			$request['mission'] = $request->mission_copy;
		}

		if ($request->input('vision')) {
			$request['vision'] = $request->vision_copy;
		}
		try {
			$user_unique_cd = strtolower(substr(preg_replace('/\s/', '', $request->name), 0, 1)) . '' . substr(md5(mt_rand()), 0, 6);
			$requestDateOfBirth = $request->input('date_of_birth') ? explode('/', $request->input('date_of_birth')) : [];
			$bsbUniqueNo = User::count();
			$user = User::create(['user_name' => $request->name, 'email' => $request->contact_person_email, 'phone_no' => $request->contact_person_phone, 'password' => bcrypt('user@123#'), 'user_unique_code' => strtolower(preg_replace('/\s/', '', $user_unique_cd)), 'bsb_unique_no' => ($bsbUniqueNo + 1), 'status' => 1, 'user_location' => ['type' => 'Point', 'coordinates' => [(float) $request['lat'], (float) $request['lng']]]]);

			$collegeDetail = CollegeDetail::create($request->only('name', 'website', 'country_id', 'city_id', 'address', 'total_student', 'total_student_place_per_year', 'about_college', 'special_note_employer', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_postal_address', 'mission', 'vision', 'institute_video') + ['user_id' => $user->id, 'dob_date' => $requestDateOfBirth[0], 'dob_month' => $requestDateOfBirth[1], 'dob_year' => $requestDateOfBirth[2]]);
			if ($request->input('gallery_length') > 0) {
				$images_name = [];
				for ($i = 0; $i < $request->input('gallery_length'); $i++) {
					$file = $request->file('gallery_' . $i);
					$new_name = $file->storePublicly(
						'upload_files/colleges/gallery_images'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/colleges/gallery_images' . '/', '', $new_name);
						array_push($images_name, $filename);
					}
				}
				$collegeDetail->gallery_images = implode(',', $images_name);
				$collegeDetail->save();
			}

			if ($request->hasFile('logo')) {
				$file = $request->file('logo');
				$new_name = $request->file('logo')->storePublicly(
					'upload_files/colleges'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/colleges' . '/', '', $new_name);
				}

				$collegeDetail->logo = $filename;
				$collegeDetail->save();
			}

			$user->collegeCourses()->attach(explode(',', $request->input('courses')));

			$role = Role::where('slug', 'college')->first();
			$user->roles()->attach($role->id);
			$user->last_login = Carbon::now()->toDateTimeString();

			$data = ['text' => 'User Register', 'unique_code' => $user_unique_cd, 'login_email' => $request->contact_person_email, 'password' => 'user@123#', 'user_name' => $request->name, 'user_role' => 'college'];
			$view = 'emails.other-user-registration';
			$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';

			Mail::to($user->email)->send(new SendMail($data, $view, $subject));

			return response()->json(['status' => 200, 'status_text' => 'Successfully registered']);

		} catch (\Exceptions $e) {
			return response()->json(['status_text' => 'something is wrong', 'status' => 500]);
		}
	}

	public function trainerRegistration(Request $request) {
		$messages = [
			'from_am_pm.required' => 'Please choose am. or pm.',
			'to_am_pm.required' => 'Please choose am. or pm.',
			'cv_path.required' => 'This field is required',
			'cv_path.mimes' => 'File only allow pdf or doc',
			'identity_proof.required' => 'This field is required',
			'identity_proof.mimes' => 'File only allow image',
			'declaration_user.required' => 'Declaration checkbox is required',
			'date_of_birth.required' => 'Date Of Birth is required',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'skype_id' => 'required',
			'email' => 'required|email|unique:users,email',
			'phone_no' => 'required|min:8|max:20|regex:/^(\\+)?(\\d+)$/',
			'gender' => 'required',
			'date_of_birth' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'industry_id' => 'required',
			'trainer_type' => 'required',
			'about_profile' => 'required',
			'address' => 'required',
			'current_designation' => 'required',
			'highest_education' => 'required',
			'year_of_exp' => 'required',
			'name_account_holder' => 'required',
			'skills' => 'required',
			'cv_path' => 'required|mimes:doc,docx,pdf',
			'identity_proof' => 'required|mimes:png,jpeg,jpg,gif,svg|image',
			'name_account_holder' => 'required',
			'bank_name' => 'required',
			'account_no' => 'required',
			'swift_code' => 'required',
			'ifsc_code' => 'required|regex:/^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/',
			'bank_portal_address' => 'required',
			'declaration_user' => 'required',
			/*'expected_days' => 'required',
				'from_time' => 'required',
				'from_am_pm' => 'required',
				'to_time' => 'required',
			*/

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		$request['highest_education'] = explode(',', $request->highest_education);
		if ($request->input('about_profile')) {
			$request['about_profile'] = $request->about_profile_copy;
		}
		try {
			$bsbUniqueNo = User::count();
			$requestDateOfBirth = $request->input('date_of_birth') ? explode('/', $request->input('date_of_birth')) : [];
			$user = User::create(['user_name' => $request->name, 'email' => $request->email, 'phone_no' => $request->phone_no, 'password' => bcrypt('user@123#'), 'bsb_unique_no' => ($bsbUniqueNo + 1), 'status' => 1]);

			$role = Role::where('slug', 'trainer')->first();
			$user->roles()->attach($role->_id);
			$user->last_login = Carbon::now()->toDateTimeString();
			$trainer = TrainerDetail::create($request->except('email', 'phone_no', 'date_of_birth') + ['user_id' => $user->_id, 'created_by' => $user->_id, 'dob_date' => $requestDateOfBirth[0], 'dob_month' => $requestDateOfBirth[1], 'dob_year' => $requestDateOfBirth[2]]);
			/*$trainer = $user->trainerUser()->create($request->except('email', 'phone_no') + ['user_id' => $user->_id, 'created_by' => $user->_id]);*/
			if ($request->hasFile('cv_path')) {
				$file = $request->file('cv_path');
				$new_name = $request->file('cv_path')->storePublicly(
					'upload_files/trainer-resumes'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/trainer-resumes' . '/', '', $new_name);
				}

				$trainer->cv_path = $filename;
				$trainer->save();
			}
			if ($request->hasFile('profile_image')) {
				$file = $request->file('profile_image');
				$new_name = $request->file('profile_image')->storePublicly(
					'upload_files/profile_picture'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
				}
				$trainer->profile_image = $filename;
				$trainer->save();
			}
			if ($request->hasFile('identity_proof')) {
				$file = $request->file('identity_proof');
				$new_name = $request->file('identity_proof')->storePublicly(
					'upload_files/other_users'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
				}
				$trainer->identity_proof = $filename;
				$trainer->save();
			}
			$data = ['text' => 'Trainer Register', 'unique_code' => '', 'login_email' => $user->email, 'password' => 'user@123#', 'user_name' => $request->name, 'user_role' => 'trainer'];
			$view = 'emails.other-user-registration';
			$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';

			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			return response()->json(['status' => 200, 'status_text' => 'Successfully registered']);

		} catch (\Exceptions $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function otherRegistration(Request $request) {
		$messages = [
			'comp_name.required' => 'Company name is required',
			'contact_person_email.required' => 'Contact person email is required',
			'contact_person_email.unique' => 'Email is exist!',
			'contact_person_phone.required' => 'Contact person phone is required',
			'contact_person_name.required' => 'Contact person first name is required',
			'contact_person_lastname.required' => 'Contact person last name is required',
			'date_of_birth.required' => 'Date Of Birth is required',
			'gender.required' => 'Gender is required',
			'country_id.required' => 'Country is required',
			'city_id.required' => 'City is required',
			'address.required' => 'Address is required',
			'contact_person_designation.required' => 'Designation is required',
			'registration_certificate.mimes' => 'The registration certificate must be an image.',
			'govt_id.mimes' => 'The govt id must be an image',
			'declaration_user.required' => 'Declaration checkbox is required',
		];
		$validator = Validator::make($request->all(), [
			'comp_name' => 'required',
			'contact_person_name' => 'required',
			'contact_person_lastname' => 'required',
			'contact_person_email' => 'required|email|unique:users,email',
			'contact_person_phone' => 'required|min:8|max:20|regex:/^(\\+)?(\\d+)$/',
			'date_of_birth' => 'required',
			'gender' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'address' => 'required',
			'contact_person_designation' => 'required',
			'registration_certificate' => 'required|mimes:png,jpeg,jpg,gif,svg|image',
			'govt_id' => 'required|mimes:png,jpeg,jpg,gif,svg|image',
			'declaration_user' => 'required',
			'name_account_holder' => 'required',
			'bank_name' => 'required',
			'account_no' => 'required',
			'swift_code' => 'required',
			'ifsc_code' => 'required|regex:/^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/',
			'bank_portal_address' => 'required',
			'declaration_user' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$requestDateOfBirth = $request->input('date_of_birth') ? explode('/', $request->input('date_of_birth')) : [];
			$user_unique_cd = strtolower(substr(preg_replace('/\s/', '', $request->comp_name), 0, 1)) . '' . substr(md5(mt_rand()), 0, 6);
			$bsbUniqueNo = User::count();
			$user = User::create(['user_name' => $request->comp_name, 'email' => $request->contact_person_email, 'phone_no' => $request->contact_person_phone, 'password' => bcrypt('user@123#'), 'user_unique_code' => $user_unique_cd, 'bsb_unique_no' => ($bsbUniqueNo + 1), 'status' => 1]);
			$user->last_login = Carbon::now()->toDateTimeString();
			if ($request->user_type == 'agency') {
				$agency = AgencyDetail::create($request->only('comp_name', 'comp_website', 'country_id', 'city_id', 'address', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_portal_address') + ['user_id' => $user->id, 'dob_date' => $requestDateOfBirth[0], 'dob_month' => $requestDateOfBirth[1], 'dob_year' => $requestDateOfBirth[2]]);
				if ($request->hasFile('registration_certificate')) {
					$file = $request->file('registration_certificate');
					$new_name = $request->file('registration_certificate')->storePublicly(
						'upload_files/other_users'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
					}

					$agency->registry_certificate = $filename;
					$agency->save();
				}
				if ($request->hasFile('govt_id')) {
					$file = $request->file('govt_id');
					$new_name = $request->file('govt_id')->storePublicly(
						'upload_files/other_users'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
					}
					$agency->govt_id = $filename;
					$agency->save();
				}

				$role = Role::where('slug', 'agency')->first();
				$user->roles()->attach($role->id);
				$data = ['text' => 'User Register', 'unique_code' => $user_unique_cd, 'login_email' => $user->email, 'password' => 'user@123#', 'user_name' => $request->comp_name, 'user_role' => 'agency'];
				$view = 'emails.other-user-registration';
				$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';

				Mail::to($user->email)->send(new SendMail($data, $view, $subject));

			} else {
				$franchise = FranchiseDetail::create($request->only('comp_name', 'country_id', 'city_id', 'address', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'dob_date', 'dob_month', 'dob_year', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_portal_address') + ['user_id' => $user->id]);
				if ($request->hasFile('registration_certificate')) {
					$file = $request->file('registration_certificate');
					$new_name = $request->file('registration_certificate')->storePublicly(
						'upload_files/other_users'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
					}
					$franchise->registration_certificate = $filename;
					$franchise->save();
				}
				if ($request->hasFile('govt_id')) {
					$file = $request->file('govt_id');
					$new_name = $request->file('govt_id')->storePublicly(
						'upload_files/other_users'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
					}
					$franchise->govt_id = $filename;
					$franchise->save();
				}
				$role = Role::where('slug', 'franchise')->first();
				$user->roles()->attach($role->_id);
				$data = ['text' => 'User Register', 'unique_code' => $user_unique_cd, 'login_email' => $user->email, 'password' => 'user@123#', 'user_name' => $request->comp_name, 'user_role' => 'franchise'];
				$view = 'emails.other-user-registration';
				$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';

				Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			}

			return response()->json(['status' => 200, 'status_text' => 'Successfully registered']);

		} catch (\Exceptions $e) {
			return response()->json(['status_text' => 'something is wrong', 'status' => 500]);
		}

	}

	public function login(Request $request) {

		// get email and password from request
		$credentials = request(['email', 'password']);

		// try to auth and get the token using api authentication
		if (!$token = auth('api')->attempt(['email' => strtolower($request->email), 'password' => $request->password/*, 'status' => 1*/])) {
			// if the credentials are wrong we send an unauthorized error in json format
			return response()->json(['status' => 401, 'status_text' => 'Invalid credentails!']);
		}
		$user = auth('api')->user();
		if ($request->input('user_type') && $user->roles()->first()->slug != $request->input('user_type')) {
			auth('api')->logout();
			return response()->json(['status' => 401, 'status_text' => 'Invalid credentails!']);
		}
		$user = auth('api')->user();
		$user->last_login = Carbon::now()->toDateTimeString();
		$user->community_token = uniqid(base64_encode(str_random(60)));
		$user->login_status = 1;
		$user->save();
		return response()->json([
			'token' => $token,
			'user' => new UserResource(auth('api')->user()),
			'status' => 200,
			'status_text' => 'Succesfully login!',
			'type' => 'bearer', // you can ommit this
			'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration

		]);
	}

	/*public function login(Request $request) {
		$validate = $this->validate($request, [
			'email' => 'required|email',
			'password' => 'required',
			'remember_me' => 'boolean',
		]);

		$credentials = request(['email', 'password']);
		if (!Auth::attempt($credentials + ['status' => 1])) {
			return response()->json([
				'status_text' => 'Invalid credentails!',
				'status' => 401,
			]);
		}
		$user = $request->user();
		if ($request->input('user_type') && $user->roles()->first()->slug != $request->input('user_type')) {
			return response()->json(['status' => 401, 'status_text' => 'Invalid credentails!']);
		}

		$tokenResult = $user->createToken('Personal Access Token');
		$token = $tokenResult->token;
		if ($request->remember_me) {
			$token->expires_at = Carbon::now()->addWeeks(1);
		}

		$token->save();
		$user->update(['last_login' => Carbon::now()]);
		return response()->json([
			'access_token' => $tokenResult->accessToken,
			'token' => $tokenResult->accessToken,
			'token_type' => 'Bearer',
			'user' => new UserResource(\Auth::user()),
			'status' => 200,
			'status_text' => 'Succesfully login!',
			'expires_at' => Carbon::parse(
				$tokenResult->token->expires_at
			)->toDateTimeString(),
		]);

	}*/

	public function forgetPassword(Request $request) {
		$user = User::where('email', $request->email)->whereHas('roles', function ($q) use ($request) {
			$q->where('slug', $request->user_type);
		})->first();
		if (!$user) {
			return response()->json(['status' => 401, 'msg' => 'email doesnt exists']);
		}
		$first_name = '';
		if ($request->user_type == 'employee') {
			$first_name = $user->employeeUser->first_name;
		} else if ($request->user_type == 'employer') {
			$first_name = $user->employerUser->first_name;
		} else {
			$first_name = $user->getOtherUserName($user);
		}

		if ($user) {
			$token = bin2hex(random_bytes(78));
			$data = array('token' => $token, 'user_type' => $request->user_type);
			PasswordReset::create(['email' => $user->email, 'token' => $token]);

			if ($request->user_type == 'employee' || $request->user_type == 'employer') {
				// Mail::send('emails.forgot-password', $data, function ($message) use ($user) {
				// 	$message->to($user->email, $user->name)->subject('Reset Password Mail');
				// 	$message->from('souravg1g2.sp@gmail.com');
				// });
				$data = ['text' => 'User Reset Password', 'token' => $token, 'user_type' => $request->user_type, 'user_name' => $first_name];
				$view = 'emails.forgot-password';
				$subject = 'Reset password initiated in bsbstaffing.com.';

				Mail::to($user->email)->send(new SendMail($data, $view, $subject));

			} else {
				// Mail::send('emails.other-forgot-password', $data, function ($message) use ($user) {
				// 	$message->to($user->email, $user->name)->subject('Reset Password Mail');
				// 	$message->from('souravg1g2.sp@gmail.com');
				// });
				$data = ['text' => 'User Reset Password', 'token' => $token, 'user_type' => $request->user_type, 'user_name' => $first_name];
				$view = 'emails.other-forgot-password';
				$subject = 'Reset password initiated in bsbstaffing.com.';

				Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			}

			return response()->json(['status' => 200]);
		} else {
			return response()->json(['status' => 401, 'msg' => 'email doesnt exists']);
		}
	}

	public function resetForm(Request $request, $token = null) {
		$title = 'Reset Password';
		$exist_token = PasswordReset::where('token', $token)->first();
		if ($exist_token) {
			return view('auth.reset')->with(
				['token' => $token, 'email' => $exist_token->email, 'title' => $title]
			);
		} else {
			return view('errors.unauthorize');
		}
	}

	public function resetPassword(Request $request) {
		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:6',
		]);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator);
		}
		$user = User::where('email', $request->email)->first();

		if ($user) {
			$user->password = bcrypt($request->password);
			$user->save();
			PasswordReset::where('token', $request->token)->update(['token' => NULL]);
			if ($user->roles()->first()->slug == 'employer') {
				return redirect(config('app.frontendUrl') . '/employer');
			} else {
				return redirect(config('app.frontendUrl'));
			}

		}

	}

	public function checkResetToken($token) {
		$tokenExist = PasswordReset::where('token', $token)->first();
		if (!$tokenExist) {
			return response()->json(['status' => 422, 'status_text' => 'Token is not valid']);
		} else {
			return response()->json(['status' => 200, 'data' => $tokenExist]);
		}
	}

	public function otherUserPasswordreset(Request $request) {
		//return $request;
		$tokenExist = PasswordReset::where('token', $request->token)->where('email', $request->email)->first();
		if (!$tokenExist) {
			return response()->json(['status' => 401, 'msg' => 'Token is not valid']);
		}
		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:6',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		$user = User::where('email', $request->email)->first();

		if ($user) {
			$user->password = bcrypt($request->password);
			$user->save();
			PasswordReset::where('token', $request->token)->update(['token' => NULL]);
			return response()->json(['status' => 200, 'status_text' => 'Successfully reset password']);

		}
	}

	public function checkForgetToken(Request $request) {
		try {
			$resetToken = ResetPassword::where('token', $request->token)->first();
			if ($resetToken->status === '0') {
				return response()->json(['status' => 200, 'data' => $resetToken]);
			} else {
				return response()->json(['status' => 401, 'msg' => 'unauthorized']);
			}
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'msg' => $e->getMessage()]);
		}

	}

	public function logout(Request $request) {
		$user = auth('api')->user();
		$user->community_token = '';
		$user->login_status = 0;
		$user->save();
		auth('api')->logout();

		return response()->json([
			'message' => 'successful logout',
			'status' => 200,
		]);
		//JWTAuth::invalidate(JWTAuth::getToken());
		/*$request->user()->token()->revoke();
		return response()->json([
			'message' => 'successful logout',
			'status' => 200,
		]);*/
	}

	public function employeeProfileUpdate(Request $request) {
		$validator = Validator::make($request->all(), [
			'country_id' => 'required',
			'city_id' => 'required',
			'designation' => 'required',
			'industry_id' => 'required',
			'date_of_birth' => 'required',
			'mobile' => 'required',
			'gender' => 'required',

		], []);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		$user = $request->user();

		$userCode = '';

		if ($request->input('user_unique_code') != 'null' && !empty($request->input('user_unique_code'))) {
			$userCode = User::where('user_unique_code', $request->input('user_unique_code'))->first();
			if (!$userCode && $request->isEmployer != 1) {
				return response()->json(['status' => 421, 'error' => "User code does not matched, If you don't have code, leave it blank"]);
			}
		}
		if ($userCode) {
			$user->user_unique_code = $userCode;
		}
		$user->phone_no = $request->mobile;
		$user->save();
		$employee_details = $user->employeeUser->fill($request->only(['country_id', 'city_id', 'date_of_birth', 'gender']) + ['form_no' => 2, 'industry_id' => $request->industry_id]);
		$employee_details->save();
		$newempPref = EmployeePrefTxn::create(['employee_id' => $user->_id]);
		$newempPref->industries()->attach($request->industry_id);
		$data = ['text' => 'User Register', 'register_token' => $user->register_token, 'user_name' => $employee_details->first_name];
		$view = 'emails.employee-registration';
		$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';
		Mail::to($user->email)->send(new SendMail($data, $view, $subject));

		return response()->json(['status' => 200, 'user' => new UserResource($user), 'status_text' => 'successfully updated']);

	}

	public function employerProfileUpdate(Request $request) {
		$validator = Validator::make($request->all(), [
			'company_name' => 'required',
			'company_type' => 'required',
			'company_size' => 'required',
			'company_logo' => 'required',
			'company_website' => 'required',
			'company_email' => 'required',
			'company_tagline' => 'required',
			'company_short_desc' => 'required',

		], []);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		$user = $request->user();
		try {
			$user = $user->fill($request->only(['email', 'phone_no']));
			if (!empty($request->input('password'))) {
				$user->password = bcrypt($request->input('password'));
			}
			$user->save();
			$employer_details = $user->employerUser->fill($request->only(['company_name', 'first_name', 'last_name', 'industry_id', 'address', 'city_id', 'zipcode', 'country_id', 'alternate_ph_no', 'company_type', 'company_size', 'company_tagline', 'company_short_desc', 'element', 'nearby_place', 'designation', 'company_website', 'company_email', 'business_licence_type', 'license_agree', 'dob_date', 'dob_month', 'dob_year', 'gender']));
			$employer_details->save();
			if ($request->hasFile('company_logo')) {
				$image = $request->file('company_logo');
				$name = $request->company_name . time() . '.' . $image->getClientOriginalExtension();
				//$destinationPath = public_path('/upload_files/employer/logo');
				//$imagePath = $destinationPath . "/" . $name;
				$img = Image::make($image);
				//->resize(null, 85)
				//->fit(300, 150)
				//->save($imagePath);
				$img->resize(null, 150, function ($constraint) {
					$constraint->aspectRatio();
				});
				Storage::disk('s3')->put('/upload_files/employer/logo/' . $name, $img->stream()->__toString(), 'public');
				//$img->save($imagePath);
				//$image->move($destinationPath, $name);
				$employer_details->company_logo = $name;
				$employer_details->save();
			}
			if ($request->hasFile('profile_picture')) {
				$image = $request->file('profile_picture');
				$new_name = $request->file('profile_picture')->storePublicly(
					'upload_files/profile_picture'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
				}
				$employer_details->profile_picture = $filename;
				$employer_details->save();
			}
			if ($request->hasFile('company_main_image')) {
				$image = $request->file('company_main_image');
				$new_name = $request->file('company_main_image')->storePublicly(
					'upload_files/employer/main-image'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employer/main-image' . '/', '', $new_name);
				}
				$employer_details->company_main_image = $filename;
				$employer_details->save();
			}
			if ($request->hasFile('images_files')) {
				//$user->galleryUser()->where('type', 'image')->delete();
				$employer_gallery = new EmployerGallery;
				foreach ($request->file('images_files') as $key => $image) {
					$name = $request->company_name . '_' . $key . time() . '.' . $image->getClientOriginalExtension();
					$new_name = $image->storePublicly(
						'upload_files/employer/gallery_images'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/employer/gallery_images' . '/', '', $new_name);
					}

					EmployerGallery::create(['user_id' => $user->id, 'name' => $filename, 'type' => 'image']);
				}
			}

			if ($request->has('remove_img_ids') && !empty($request->input('remove_img_ids'))) {
				foreach (explode(',', $request->input('remove_img_ids')) as $rm_image) {
					$removeImage = EmployerGallery::find($rm_image);
					//$removePath = public_path() . '/upload_files/employer/gallery_images/' . $removeImage->name;
					//chmod($removePath, 0644);
					//unlink($removePath);
					$removePath = Storage::disk('s3')->delete('upload_files/employer/gallery_images/' . $removeImage->name);
					$removeImage->delete();
				}

			}

			if ($request->hasFile('business_licence')) {
				$image = $request->file('business_licence');
				$name = $request->company_name . time() . '.' . $image->getClientOriginalExtension();

				$new_name = $request->file('business_licence')->storePublicly(
					'upload_files/employer/business_licence'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employer/business_licence' . '/', '', $new_name);
				}

				$employer_details->business_licence = $filename;
				$employer_details->save();
			}

			if ($request->video_link) {
				$user->videoGalleryUser()->where('type', 'url')->delete();
				//echo $user->id;die;
				EmployerGallery::create(['user_id' => $user->id, 'name' => $request->video_link, 'type' => 'url']);
			}
			if ($request->video_link == '') {
				$user->videoGalleryUser()->where('type', 'url')->delete();
			}
			$countrName = $employer_details->country->name;
			$cityName = $employer_details->city->name;
			$empAddress = $employer_details->address;
			$empzip = $employer_details->zipcode;
			//$full_address = $empAddress . ',' . $cityName . ',' . $countrName . ',' . $empzip;
			$full_address = $empAddress . ',' . $cityName . ',' . $empzip;
			$google_address = str_replace(" ", "+", $full_address);
			$json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$google_address&key=AIzaSyDzub4OkqZIbyJb5mrLTMkhW2RD4gk0ZG4");
			$json = json_decode($json, true);
			$jsonResult = (array) $json["results"];
			if (count($jsonResult) == 0) {
				return response()->json(['status' => 500, 'error' => 'Please enter valid address']);
			}
			$locationData = (array) $json["results"][0]["geometry"]["location"];
			//$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			//$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
			$lat = $locationData['lat'];
			$long = $locationData['lng'];
			$employer_details->lat = $lat;
			$employer_details->lng = $long;

			$employer_details->save();
			//DB::commit();
			//$session->commitTransaction();
			return response()->json(['status' => 200, 'user' => new UserResource($user), 'status_text' => 'successfully updated']);

		} catch (\Exception $e) {
			//DB::rollback();
			//$session->abortTransaction();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

		/*
				$address = 'Saltlake,Kolkata,Westbengal,India';
				$address = str_replace(" ", "+", $address);

			    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyD14Wf8FCUI6z6X-17Vw-S0em4K8NxfGhU");
			    //$json = json_decode($json);
			    return $json;
		*/

	}

	public function employerQuickProfileUpdate(Request $request) {

		$validator = Validator::make($request->all(), [
			'company_name' => 'required',
			'company_type' => 'required',
			'company_size' => 'required',
			'company_logo' => 'required',
			'company_website' => 'required',
			'company_email' => 'required',
			'company_tagline' => 'required',
			'company_short_desc' => 'required',

		], []);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		$user = $request->user();
		//DB::beginTransaction();
		try {
			$employer_details = $user->employerUser->fill($request->only(['company_name', 'industry_id', 'address', 'city_id', 'zipcode', 'country_id', 'alternate_ph_no', 'company_type', 'company_size', 'company_tagline', 'company_short_desc', 'designation', 'company_website', 'company_email', 'business_licence_type', 'license_agree']));
			$employer_details->save();
			if ($request->hasFile('company_logo')) {
				$image = $request->file('company_logo');
				$new_name = $request->file('company_logo')->storePublicly(
					'upload_files/employer/logo'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employer/logo' . '/', '', $new_name);
				}

				$employer_details->company_logo = $filename;
				$employer_details->save();
			}
			if ($request->hasFile('profile_picture')) {
				$image = $request->file('profile_picture');

				$new_name = $request->file('profile_picture')->storePublicly(
					'upload_files/profile_picture'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
				}
				$employer_details->profile_picture = $filename;
				$employer_details->save();
			}
			if ($request->hasFile('company_main_image')) {
				$image = $request->file('company_main_image');
				$new_name = $request->file('company_main_image')->storePublicly(
					'upload_files/employer/main-image'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employer/main-image' . '/', '', $new_name);
				}

				$employer_details->company_main_image = $filename;

			}
			if ($request->hasFile('images_files')) {
				$user->galleryUser()->where('type', 'image')->delete();
				$employer_gallery = new EmployerGallery;
				foreach ($request->file('images_files') as $key => $image) {
					$name = $request->company_name . '_' . $key . time() . '.' . $image->getClientOriginalExtension();

					$new_name = $request->file('images_files')->storePublicly(
						'upload_files/employer/gallery_images'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/employer/gallery_images' . '/', '', $new_name);
					}

					EmployerGallery::create(['user_id' => $user->id, 'name' => $filename, 'type' => 'image']);
				}
			}
			if ($request->hasFile('business_licence')) {
				$image = $request->file('business_licence');

				$new_name = $request->file('business_licence')->storePublicly(
					'upload_files/employer/business_licence'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employer/business_licence' . '/', '', $new_name);
				}
				$employer_details->business_licence = $filename;

			}

			$userCode = '';
			if ( /*$request->isEmployer == 2 &&*/$request->input('user_unique_code') != 'null' && !empty($request->input('user_unique_code'))) {
				$userCode = User::where('user_unique_code', $request->input('user_unique_code'))->first();
				if (!$userCode) {
					return response()->json(['status' => 421, 'error' => 'User code does not matched']);
				}
			}

			$employer_details->referral_code = $userCode ? $userCode->user_unique_code : '';
			$countrName = $employer_details->country->name;
			$cityName = $employer_details->city->name;
			$empAddress = $employer_details->address;
			$empzip = $employer_details->zipcode;
			//$full_address = $empAddress . ',' . $cityName . ',' . $countrName . ',' . $empzip;
			$full_address = $empAddress . ',' . $cityName . ',' . $empzip;
			//return $full_address;die;
			$google_address = str_replace(" ", "+", $full_address);
			//return $google_address;
			$json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$google_address&key=AIzaSyDzub4OkqZIbyJb5mrLTMkhW2RD4gk0ZG4");

			$json = json_decode($json, true);
			$jsonResult = (array) $json["results"];
			if (count($jsonResult) == 0) {
				return response()->json(['status' => 500, 'error' => 'Please enter valid address']);
			}
			$locationData = (array) $json["results"][0]["geometry"]["location"];
			/*$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};*/
			$lat = $locationData['lat'];
			$long = $locationData['lng'];
			$employer_details->lat = $lat;
			$employer_details->lng = $long;
			$employer_details->save();

			$user->profile_update = 1;
			$user->save();

			//DB::commit();
			return response()->json(['status' => 200, 'user' => new UserResource($user), 'status_text' => 'successfully updated']);

		} catch (\Exception $e) {
			//DB::rollback();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function studentCreate(Request $request) {
		//return $request->all();
		$messages = [
			'first_name.required' => 'first name is required',
			'email.required' => 'Email is required',
			'email.unique' => 'Email is exist!',
			'phone.required' => 'Phone is required',
			//'country_id.required' => 'Country is required',
			//'city_id.required' => 'City is required',
			//'permanent_address.required' => 'Permanent Address is required',
			//'gender.required' => 'Gender is required',
			//'date_of_birth.required' => 'Date of birth is required',
			//'nationality.required' => 'Nationality is required',

			//'cv_file.required' => 'CV is required.',
			//'cv_file.mimes' => 'Cv must be an doc,docx,pdf.',
		];
		$validator = Validator::make($request->all(), [
			'first_name' => 'required',
			'email' => 'required|email|unique:users,email',
			'phone' => 'required|numeric',
			//'country_id' => 'required',
			//'city_id' => 'required',
			//'permanent_address' => 'required',
			//'gender' => 'required',
			//'date_of_birth' => 'required',
			//'nationality' => 'required',
			//'cv_file' => 'required|mimes:doc,docx,pdf',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		$user = auth('api')->user();
		try {
			$bsbUniqueNo = User::count();
			$newStudent = User::create(['user_name' => $request->first_name, 'email' => $request->email, 'phone_no' => $request->phone, 'password' => bcrypt('student123#'), 'bsb_unique_no' => ($bsbUniqueNo + 1), 'last_login' => Carbon::now()->toDateTimeString(), 'profile_update' => 0, 'status' => 1, 'created_by' => $user->_id]);

			$role = Role::where('slug', 'employee')->first();
			//RoleUser::create(['user_id' => $user->_id, 'role_id' => $role->_id]);
			$newStudent->roles()->attach($role->_id);

			$newProfile = EmployeeProfileTxn::create(['user_id' => $newStudent->_id, 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'gender' => $request->gender, 'date_of_birth' => Carbon::parse(str_replace('/', '-', $request->date_of_birth))->format('Y-m-d'), 'nationality' => $request->nationality, 'country_id' => $request->country_id, 'city_id' => $request->city_id, 'permanent_address' => $request->permanent_address, 'present_address' => $request->present_address, 'institute_code' => $user->user_unique_code, 'created_by' => $user->_id, 'form_no' => 1, 'industry_id' => $request->industry_id]);

			$newempPref = EmployeePrefTxn::create(['employee_id' => $user->_id]);
			$newempPref->industries()->attach($request->industry_id);

			if ($request->hasFile('cv_file')) {
				$file = $request->file('cv_file');
				$new_name = $request->file('cv_file')->storePublicly(
					'upload_files/employee-resumes'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employee-resumes' . '/', '', $new_name);
				}
				//$newProfile->cv = $name;
				$employeeCvTxn = EmployeeCvTxn::create(['employee_id' => $newStudent->_id, 'cv_path' => $filename]);
				//$newProfile->save();
			}

			$data = ['text' => 'User Register', 'login_email' => $newStudent->email, 'password' => 'student123#', 'type' => 'student', 'unique_code' => $newProfile->institute_code, 'user_name' => $request->first_name, 'user_role' => 'student'];
			$view = 'emails.other-user-registration';
			$subject = 'Congratulations! You are successfully registered with bsbstaffing.com!';

			Mail::to($user->email)->send(new SendMail($data, $view, $subject));

			return response()->json(['status' => 200, 'status_text' => 'Succesfully create student', 'data' => '']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

}
