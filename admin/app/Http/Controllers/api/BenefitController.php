<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReferalTxn as ReferalTxnResource;
use App\Mail\SendMail;
use App\Models\AnnoucementTemplate;
use App\Models\AnnounceMent;
use App\Models\BenefitMst;
use App\Models\ReferalTxn;
use Illuminate\Http\Request;
use Mail;

class BenefitController extends Controller {

	public function annoucementBenefit(Request $request) {
		try {
			$user = auth('api')->user();
			$annoucement_template = AnnoucementTemplate::find($request->subject_id);
			AnnounceMent::create($request->except('emails') + ['user_id' => $user->_id, 'created_by' => $user->_id, 'emails' => $request->input('emails')]);
			$countEmails = AnnounceMent::where('benefit_id', $request->id)->where('user_id', $user->_id)->whereNull('complete_status')->count();
			if ($request->input('remaining_count') === 0) {
				AnnounceMent::where('benefit_id', $request->benefit_id)->where('user_id', $user->_id)->whereNull('complete_status')->update(['complete_status' => 1]);
			}
			$data = ['text' => 'Announcement Text', 'annoucement_template' => $annoucement_template];
			$view = 'emails.announcement';
			$subject = $annoucement_template->temp_name;
			foreach ($request->input('emails') as $value) {
				Mail::to($value)->send(new SendMail($data, $view, $subject));
			}
			return response()->json(['status' => 200, 'status_text' => 'Successfully send email']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function announcementWithOutLogin(Request $request) {
		try {
			$annoucement_template = AnnoucementTemplate::find($request->subject_id);
			AnnounceMent::create($request->except('emails') + ['emails' => $request->input('emails')]);
			$data = ['text' => 'Announcement Text', 'annoucement_template' => $annoucement_template];
			$view = 'emails.announcement';
			$subject = $annoucement_template->temp_name;
			foreach ($request->input('emails') as $value) {
				Mail::to($value)->send(new SendMail($data, $view, $subject));
			}
			return response()->json(['status' => 200, 'status_text' => 'Successfully send email']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function getAnnouncement(Request $request) {
		$user = auth('api')->user();
		$templates = AnnoucementTemplate::where('status', 1)->get();
		$benefit = BenefitMst::find($request->id);
		$sendedEmails = AnnounceMent::where('benefit_id', $request->id)->where('user_id', $user->_id)->whereNull('complete_status')->select('emails')->pluck('emails')->toArray();
		return response()->json(['status' => 200, 'data' => compact('templates', 'benefit', 'sendedEmails')]);
	}

	public function getReferalBenefit(Request $request) {
		$user = auth('api')->user();
		$all_refs = ReferalTxn::where('user_id', $user->_id)->get();
		$templates = AnnoucementTemplate::where('status', 1)->get();
		return response()->json(['status' => 200, 'data' => ReferalTxnResource::collection($all_refs), 'templates' => $templates]);

	}

	public function getAnnouncementTemplate(Request $request) {
		$templates = AnnoucementTemplate::where('status', 1)->get();
		return response()->json(['status' => 200, 'data' => $templates]);

	}

	public function referalBenefitSubmit(Request $request) {

		try {
			$user = auth('api')->user();
			foreach ($request->input('referrals') as $value) {
				$refTxn = ReferalTxn::create(['user_id' => $user->_id, 'benefit_id' => $request->benefit_id, 'referal_name' => $value['referal_name'], 'referal_email' => $value['referal_email'], 'referal_designation' => $value['referal_designation'], 'referal_company' => $value['referal_company'], 'referal_company_website' => $value['referal_company_website'], 'message' => '', 'is_registered' => 0, 'status' => 1]);
			}

			$data = ReferalTxn::where('user_id', $user->_id)->where('status', 1)->get();
			return response()->json(['status' => 200, 'status_text' => 'Successfully submitted', 'data' => ReferalTxnResource::collection($data)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 200, 'status_text' => $e->getMessage()]);
		}

	}

	public function referralSendMessage(Request $request) {
		try {
			//return $request->all();
			$user = auth('api')->user();
			$annoucement_template = AnnoucementTemplate::find($request->subject_id);
			$referralList = ReferalTxn::where('user_id', $user->_id)->where('status', 1)->get();
			//return $referralList;die;
			if (!$referralList->isEmpty()) {
				$data = ['text' => 'Referral Message', 'annoucement_template' => $annoucement_template];
				$view = 'emails.announcement';
				$subject = $annoucement_template->temp_name;
				foreach ($referralList as $value) {
					$value->message = $request->input('message');
					$value->save();
					Mail::to($value->referal_email)->send(new SendMail($data, $view, $subject));
				}
				return response()->json(['status' => 200, 'status_text' => 'Successfully send message']);
			} else {
				return response()->json(['status' => 422, 'status_text' => 'Please Add Referral details']);
			}

		} catch (\Exception $e) {
			return response()->json(['status' => 200, 'status_text' => $e->getMessage()]);
		}

	}
}
