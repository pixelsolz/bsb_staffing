<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarrerPage as CarrerPageResource;
use App\Http\Resources\EmployeerJobs as EmployeerJobsResource;
use App\Models\CareerPageTxn;
use App\Models\CareerPhoto;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class CarrerController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$user = auth('api')->user();
		$carrer = CareerPageTxn::where('user_id', $user->_id)->first();
		$carrerdata = new CarrerPageResource($carrer);
		$employer_opening_jobs = $user->employerJobs()->where('job_status', 1);
		$count_opening_jobs = $employer_opening_jobs->count();
		$employer_jobs = EmployeerJobsResource::collection($employer_opening_jobs->limit(10)->get());
		return response()->json(['status' => 200, 'data' => compact('carrerdata', 'employer_jobs', 'count_opening_jobs')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//return $request->input('keyForm');
		$user = auth('api')->user();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$user = User::find($id);

		return response()->json(['status' => 200, 'data' => new EmployeeDetailsResource($user)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function carrerPageSave(Request $request) {
		//return $request->all();
		$user = auth('api')->user();

		$company_name = str_replace(' ', '-', $user->employerUser->company_name);

		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'company_logo' => $request->hasFile('company_logo') ? 'image|mimes:jpeg,png,jpg,gif,svg' : '',
			'header_color' => 'required',
			'footer_color' => 'required',
			'background_color' => 'required',
			'text_color' => 'required',
			'page_headline' => 'required',
			'about_company' => 'required',
			'contact_info' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {

			$career_page = CareerPageTxn::updateOrCreate([
				//Add unique field combo to match here
				//For example, perhaps you only want one entry per user:
				'_id' => $request->id,

			], [
				'user_id' => auth('api')->user()->_id,
				'header_color' => $request->header_color,
				'footer_color' => $request->footer_color,
				'background_color' => $request->background_color,
				'text_color' => $request->text_color,
				'page_headline' => $request->page_headline,
				'about_company' => $request->about_company,
				'contact_info' => $request->contact_info,
				'company_name' => $company_name,
			]);

			if ($request->hasFile('company_logo')) {
				$file = $request->file('company_logo');
				$new_name = $request->file('company_logo')->storePublicly(
					'upload_files/career_page/logo'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/career_page/logo' . '/', '', $new_name);
				}
				$career_page->company_logo = $filename;
				$career_page->save();
			}

			if ($request->hasFile('images_files')) {
				//$user->carrerPhotos()->where('type', 'image')->delete();
				//$employer_gallery = new EmployerGallery;
				foreach ($request->file('images_files') as $key => $image) {
					$name = $company_name . '_' . $key . time() . '.' . $image->getClientOriginalExtension();
					$new_name = $image->storePublicly(
						'upload_files/career_page/photo'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/career_page/photo' . '/', '', $new_name);
					}
					CareerPhoto::create(['user_id' => auth('api')->user()->_id, 'name' => $filename]);
				}
			}
			if ($request->has('remove_img_ids') && !empty($request->input('remove_img_ids'))) {
				foreach (explode(',', $request->input('remove_img_ids')) as $rm_image) {
					$removeImage = CareerPhoto::find($rm_image);
					/*$removePath = public_path() . '/upload_files/career_page/photo/' . $removeImage->name;*/
					//chmod($removePath, 0644);
					//unlink($removePath);
					$removePath = Storage::disk('s3')->delete('upload_files/career_page/photo/' . $removeImage->name);
					$removeImage->delete();
				}

			}

			$carrerdata = new CarrerPageResource($career_page);
			//return response()->json(['status' => 200]);
			return response()->json(['status' => 200, 'data' => compact('carrerdata'), 'status_text' => 'successfully save']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function getCarrerByEmployer(Request $request) {
		$user_id = $request->ec;
		$user = User::find($user_id);
		//return $user;
		$carrer = CareerPageTxn::where('user_id', $user->_id)->first();
		$carrerdata = new CarrerPageResource($carrer);
		$employer_opening_jobs = $user->employerJobs()->where('job_status', 1);
		$count_opening_jobs = $employer_opening_jobs->count();
		$employer_jobs = $employer_opening_jobs->limit(10)->get();
		return response()->json(['status' => 200, 'data' => compact('carrerdata', 'employer_jobs', 'count_opening_jobs')]);
	}

	public function carrerWidetData($user_id) {
		$user = User::find($user_id);
		$employer_jobs = $user->employerJobs()->where('job_status', 1)->get();
		return view('widget', compact('employer_jobs'));
		//return response()->json(['status' => 200, 'data' => $employer_jobs]);

	}

}
