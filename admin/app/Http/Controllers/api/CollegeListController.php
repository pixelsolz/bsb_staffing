<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CollegeDetail as CollegeDetailResource;
use App\Models\CollegeDetail;
use App\Models\User;
use Illuminate\Http\Request;

class CollegeListController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$limit = empty($request->limit) ? 8 : (int) $request->limit;
		$collegeDetails = CollegeDetail::has('user')->where(function ($q) use ($request) {
			if (!empty($request->institute_name)) {
				$q->where('name', 'like', '%' . $request->institute_name . '%');
			}
			if (!empty($request->location_by_country)) {
				$q->where('country_id', $request->location_by_country);
			}
			if (!empty($request->location_by_city)) {
				$q->where('city_id', $request->location_by_city);
			}
			if (!empty($request->course_offered)) {
				$q->where('courses', $request->course_offered);
			}
		})->orderBy('updated_at', 'desc');

		return response()->json(['status' => 200, 'data' => CollegeDetailResource::collection($collegeDetails->offset(0)->limit($limit)->get()), 'count' => $collegeDetails->count()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$college = User::find($id)->collegeUser;
		return response()->json(['status' => 200, 'data' => new CollegeDetailResource($college)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
