<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\EmployerJobTxn;
use App\Models\CustomiseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class CustomServiceController extends Controller {

	public function __construct() {
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'designation' => 'required',
			'email' => 'required',
			'phone_no' => 'required',
			'service_type' => 'required',
			'queries' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$allRequest = $request->all();
			$custom_service = CustomiseService::create($allRequest + ['employer_id' => Auth::user()->id]);
			
			return response()->json(['status' => 200, 'data' => $custom_service]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		/*try {
			$jobs = EmployerJobTxn::find($id);
			$jobs->delete();
			return response()->json(['status' => 200, 'status_text' => 'successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}*/
	}

	/*public function jobStatus(Request $request, $id) {
		try {
			$jobs = EmployerJobTxn::find($id);
			$jobs->job_status = $request->input('status');
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully change status']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}*/

	
}
