<?php

namespace App\Http\Controllers\api;

use App\Exports\ExportEmployees;
use App\Http\Controllers\Controller;
use App\Http\Resources\City as CityResource;
use App\Http\Resources\Country as CountryResource;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\EmployeeCv as EmployeeCvResource;
use App\Http\Resources\EmployeeCvViewHistory as EmployeeCvViewResource;
use App\Http\Resources\Industry as IndustryResource;
use App\Models\AllOtherMasterMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\Course;
use App\Models\DegreeMst;
use App\Models\DepartmentMst;
use App\Models\EmployeeCvNote;
use App\Models\EmployeeCvTxn;
use App\Models\EmployeeCvViewHistory;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\EmployerFolderTxn;
use App\Models\ExperienceMst;
use App\Models\IndustryMst;
use App\Models\JobRoleMst;
use App\Models\PaymentConfig;
use App\Models\ReportedCandidateTxn;
use App\Models\ServicesMst;
use App\Models\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class CvManageController extends Controller {

	public function __construct() {
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

	}

	public function getCvBySearch(Request $request) {
		ini_set('memory_limit', '-1');
		/*if (!empty($request->institute_id)) {
			return $userUniqueCd = User::where('_id', $request->institute_id)->first();
		}*/
		$user = auth('api')->user();
		$countriesData = CountryMst::where(function ($q) use ($request) {
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('_id', $request->formData['country_id']);
			}
		})->whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee', function ($q) use ($request) {
				if (!empty($request->institute_id)) {
					$userUniqueCd = User::where('_id', $request->institute_id)->first()->user_unique_code;
					$q->whereHas('employeeUser', function ($q) use ($userUniqueCd) {
						$q->where('institute_code', $userUniqueCd);
					});

				}

				if (!empty($request->formData['search_candidate'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						if ($request->formData['search_by'] == 'search_by_name') {
							$name = explode(' ', $request->formData['search_candidate']);
							if (!empty($name[0])) {
								$q->where('first_name', 'like', '%' . $name[0] . '%');
							}
							if (!empty($name[1])) {
								$q->where('last_name', 'like', '%' . $name[1] . '%');
							}

						}
					});
					if ($request->formData['search_by'] == 'search_by_email') {
						$q->where('email', 'like', '%' . $request->formData['search_candidate'] . '%');
					}
				}

				/* This is for folder search*/
				if (!empty($request->formData['search_type']) && $request->formData['search_type'] == "folder_wise") {
					$q->whereHas('employeeCVLike', function ($q) use ($request) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
					if (!empty($request->formData['folder_id'])) {
						$q->whereHas('employerCvFolder', function ($q) use ($request) {
							$q->where('folder_id', $request->formData['folder_id']);
							$q->where('employer_id', auth('api')->user()->_id);
						});
					}
				}
				/* If user has any of these skill*/
				if (!empty($request->formData['any_keyword'])) {
					if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['any_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							$q->whereIn('title', $request->formData['any_keyword']);
						})->orWhereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['any_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							$q->whereIn('title', $request->formData['any_keyword']);
						});
					}

				}
				/* If user must has these skill*/
				if (!empty($request->formData['must_keyword'])) {
					if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('title', $value);
							}
						})->orWhereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('title', $value);
							}
						});
					}

				}

				$q->whereHas('employeeEducation', function ($q) {
					$q->whereNotNull('_id');
				});
				$q->whereHas('employeeSkill', function ($q) {
					$q->whereNotNull('_id');
				});
				// additional form data

				if (!empty($request->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->additional['country_list']);
					});
				}
				if (!empty($request->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->additional['city_list']);
					});
				}
				if (!empty($request->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->additional['industry_list']);
					});
				}
				if (!empty($request->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->additional['department_list']);
					});
				}
				if (!empty($request->additional['degree_list'])) {
					$q->whereHas('employeeEducation', function ($q) use ($request) {
						$q->whereIn('degree_id', $request->additional['degree_list']);
					});
				}
				if (!empty($request->additional['other_filters'])) {
					if (in_array('email_verified_only', $request->additional['other_filters'])) {
						$q->whereNotNull('email_verified_at');
					}if (in_array('women_candidate_only', $request->additional['other_filters'])) {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							$q->where('gender', '2');
						});
					}if (in_array('hide_profile_wout_resume', $request->additional['other_filters'])) {
						$q->has('employeeCvTxn');
						/*$q->whereHas('employeeCvTxn', function($q) use($request){
						});*/
					}
				}
				if (!empty($request->additional['experince'])) {
					if (!empty($request->additional['experince']['min_exp']) && empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
						});
					} elseif (!empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp, $maxexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					} elseif (empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $maxexp) {
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					}
				}
				if (!empty($request->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->additional['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($request->additional['salary']['currency'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary_currency', $request->additional['salary']['currency']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && !empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
					});
				}

				if (!empty($request->additional['salary']['max_salary']) && empty($request->additional['salary']['min_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}

				if (!empty($request->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			});
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});

			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
			if (!empty($request->formData['current_salary'])) {
				$q->whereHas('employee.employeePrefTxn', function ($q) use ($request) {
					$q->where('min_salary', '>=', ((int) $request->formData['current_salary'] - 10000));
					$q->where('max_salary', '<=', ((int) $request->formData['current_salary'] + 10000));
				});
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}

		})->orderBy('_id', 'asc')->get();
		$countries = CountryResource::collection($countriesData)->addParam($request, 'employee');

		$courses = Course::all();
		$citiesdata = CityMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee', function ($q) use ($request) {
				if (!empty($request->institute_id)) {
					$userUniqueCd = User::where('_id', $request->institute_id)->first()->user_unique_code;
					$q->whereHas('employeeUser', function ($q) use ($userUniqueCd) {
						$q->where('institute_code', $userUniqueCd);
					});
				}
				if (!empty($request->formData['search_candidate'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						if ($request->formData['search_by'] == 'search_by_name') {
							$name = explode(' ', $request->formData['search_candidate']);
							if (!empty($name[0])) {
								$q->where('first_name', 'like', '%' . $name[0] . '%');
							}
							if (!empty($name[1])) {
								$q->where('last_name', 'like', '%' . $name[1] . '%');
							}

						}
					});
					if ($request->formData['search_by'] == 'search_by_email') {
						$q->where('email', 'like', '%' . $request->formData['search_candidate'] . '%');
					}
				}

				/* This is for folder search*/
				if (!empty($request->formData['search_type']) && $request->formData['search_type'] == "folder_wise") {
					$q->whereHas('employeeCVLike', function ($q) use ($request) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
					if (!empty($request->formData['folder_id'])) {
						$q->whereHas('employerCvFolder', function ($q) use ($request) {
							$q->where('folder_id', $request->formData['folder_id']);
							$q->where('employer_id', auth('api')->user()->_id);
						});
					}
				}
				/* If user has any of these skill*/
				if (!empty($request->formData['any_keyword'])) {
					if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['any_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							$q->whereIn('title', $request->formData['any_keyword']);
						})->orWhereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['any_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							$q->whereIn('title', $request->formData['any_keyword']);
						});
					}

				}
				/* If user must has these skill*/
				if (!empty($request->formData['must_keyword'])) {
					if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('title', $value);
							}
						})->orWhereHas('employeeUser', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('profile_title', 'like', '%' . $value . '%');
							}
						});
					} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
						$q->whereHas('employeeSkill', function ($q) use ($request) {
							foreach ($request->formData['must_keyword'] as $value) {
								$q->where('title', $value);
							}
						});
					}

				}
				$q->whereHas('employeeEducation', function ($q) {
					$q->whereNotNull('_id');
				});
				$q->whereHas('employeeSkill', function ($q) {
					$q->whereNotNull('_id');
				});
				// additional form data

				if (!empty($request->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->additional['country_list']);
					});
				}
				if (!empty($request->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->additional['city_list']);
					});
				}
				if (!empty($request->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->additional['industry_list']);
					});
				}
				if (!empty($request->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->additional['department_list']);
					});
				}
				if (!empty($request->additional['degree_list'])) {
					$q->whereHas('employeeEducation', function ($q) use ($request) {
						$q->whereIn('degree_id', $request->additional['degree_list']);
					});
				}
				if (!empty($request->additional['other_filters'])) {
					if (in_array('email_verified_only', $request->additional['other_filters'])) {
						$q->whereNotNull('email_verified_at');
					}if (in_array('women_candidate_only', $request->additional['other_filters'])) {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							$q->where('gender', '2');
						});
					}if (in_array('hide_profile_wout_resume', $request->additional['other_filters'])) {
						$q->has('employeeCvTxn');
						/*$q->whereHas('employeeCvTxn', function($q) use($request){
						});*/
					}
				}
				if (!empty($request->additional['experince'])) {
					if (!empty($request->additional['experince']['min_exp']) && empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
						});
					} elseif (!empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp, $maxexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					} elseif (empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $maxexp) {
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					}
				}
				if (!empty($request->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->additional['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($request->additional['salary']['currency'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary_currency', $request->additional['salary']['currency']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && !empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
					});
				}

				if (!empty($request->additional['salary']['max_salary']) && empty($request->additional['salary']['min_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}

				if (!empty($request->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			});
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});

			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
			if (!empty($request->formData['current_salary'])) {
				$q->whereHas('employee.employeePrefTxn', function ($q) use ($request) {
					$q->where('min_salary', '>=', ((int) $request->formData['current_salary'] - 10000));
					$q->where('max_salary', '<=', ((int) $request->formData['current_salary'] + 10000));
				});
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
		})->orderBy('_id', 'asc')->get();

		$cities = CityResource::collection($citiesdata)->addParam($request, 'employee');
		$industriesdata = IndustryMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee', function ($q) use ($request) {
				if (!empty($request->institute_id)) {
					$userUniqueCd = User::where('_id', $request->institute_id)->first()->user_unique_code;
					$q->whereHas('employeeUser', function ($q) use ($userUniqueCd) {
						$q->where('institute_code', $userUniqueCd);
					});
				}

				if (!empty($request->formData['search_candidate'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						if ($request->formData['search_by'] == 'search_by_name') {
							$name = explode(' ', $request->formData['search_candidate']);
							if (!empty($name[0])) {
								$q->where('first_name', 'like', '%' . $name[0] . '%');
							}
							if (!empty($name[1])) {
								$q->where('last_name', 'like', '%' . $name[1] . '%');
							}

						}
					});
					if ($request->formData['search_by'] == 'search_by_email') {
						$q->where('email', 'like', '%' . $request->formData['search_candidate'] . '%');
					}
				}

				/* This is for folder search*/
				if (!empty($request->formData['search_type']) && $request->formData['search_type'] == "folder_wise") {
					$q->whereHas('employeeCVLike', function ($q) use ($request) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
					if (!empty($request->formData['folder_id'])) {
						$q->whereHas('employerCvFolder', function ($q) use ($request) {
							$q->where('folder_id', $request->formData['folder_id']);
							$q->where('employer_id', auth('api')->user()->_id);
						});
					}
				}
				/*if (!empty($request->institute_id)) {
					$q->where('user_unique_code', $request->institute_id);
				}*/
				$q->whereHas('employeeEducation', function ($q) {
					$q->whereNotNull('_id');
				});
				$q->whereHas('employeeSkill', function ($q) {
					$q->whereNotNull('_id');
				});
				// additional form data

				if (!empty($request->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->additional['country_list']);
					});
				}
				if (!empty($request->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->additional['city_list']);
					});
				}
				if (!empty($request->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->additional['industry_list']);
					});
				}
				if (!empty($request->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->additional['department_list']);
					});
				}
				if (!empty($request->additional['degree_list'])) {
					$q->whereHas('employeeEducation', function ($q) use ($request) {
						$q->whereIn('degree_id', $request->additional['degree_list']);
					});
				}
				if (!empty($request->additional['other_filters'])) {
					if (in_array('email_verified_only', $request->additional['other_filters'])) {
						$q->whereNotNull('email_verified_at');
					}if (in_array('women_candidate_only', $request->additional['other_filters'])) {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							$q->where('gender', '2');
						});
					}if (in_array('hide_profile_wout_resume', $request->additional['other_filters'])) {
						$q->has('employeeCvTxn');
						/*$q->whereHas('employeeCvTxn', function($q) use($request){
						});*/
					}
				}
				if (!empty($request->additional['experince'])) {
					if (!empty($request->additional['experince']['min_exp']) && empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
						});
					} elseif (!empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp, $maxexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					} elseif (empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $maxexp) {
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					}
				}
				if (!empty($request->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->additional['candidate_active']);
					//$q->whereRaw(['last_login' => array('$lte' => $activeDays->timestamp)]);
					//$q->where('last_login', '>=', $activeDays->format('Y-m-d H:i:s'));
				}
				if (!empty($request->additional['salary']['currency'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary_currency', $request->additional['salary']['currency']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && !empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
					});
				}

				if (!empty($request->additional['salary']['max_salary']) && empty($request->additional['salary']['min_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}

				if (!empty($request->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			});
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});

			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
			if (!empty($request->formData['current_salary'])) {
				$q->whereHas('employee.employeePrefTxn', function ($q) use ($request) {
					$q->where('min_salary', '>=', ((int) $request->formData['current_salary'] - 10000));
					$q->where('max_salary', '<=', ((int) $request->formData['current_salary'] + 10000));
				});
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
		})->orderBy('_id', 'asc')->get();
		$industries = IndustryResource::collection($industriesdata)->addParam($request, 'employee');
		$roles = JobRoleMst::orderBy('_id', 'asc')->get();
		$department_data = DepartmentMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee', function ($q) use ($request) {
				if (!empty($request->institute_id)) {
					$userUniqueCd = User::where('_id', $request->institute_id)->first()->user_unique_code;
					$q->whereHas('employeeUser', function ($q) use ($userUniqueCd) {
						$q->where('institute_code', $userUniqueCd);
					});
				}
				if (!empty($request->formData['search_candidate'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						if ($request->formData['search_by'] == 'search_by_name') {
							$name = explode(' ', $request->formData['search_candidate']);
							if (!empty($name[0])) {
								$q->where('first_name', 'like', '%' . $name[0] . '%');
							}
							if (!empty($name[1])) {
								$q->where('last_name', 'like', '%' . $name[1] . '%');
							}

						}
					});
					if ($request->formData['search_by'] == 'search_by_email') {
						$q->where('email', 'like', '%' . $request->formData['search_candidate'] . '%');
					}
				}
				/* This is for folder search*/
				if (!empty($request->formData['search_type']) && $request->formData['search_type'] == "folder_wise") {
					$q->whereHas('employeeCVLike', function ($q) use ($request) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
					if (!empty($request->formData['folder_id'])) {
						$q->whereHas('employerCvFolder', function ($q) use ($request) {
							$q->where('folder_id', $request->formData['folder_id']);
							$q->where('employer_id', auth('api')->user()->_id);
						});
					}
				}
				/*if (!empty($request->institute_id)) {
					$q->where('user_unique_code', $request->institute_id);
				}*/
				$q->whereHas('employeeEducation', function ($q) {
					$q->whereNotNull('_id');
				});
				$q->whereHas('employeeSkill', function ($q) {
					$q->whereNotNull('_id');
				});
				// additional form data

				if (!empty($request->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->additional['country_list']);
					});
				}
				if (!empty($request->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->additional['city_list']);
					});
				}
				if (!empty($request->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->additional['industry_list']);
					});
				}
				if (!empty($request->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->additional['department_list']);
					});
				}
				if (!empty($request->additional['degree_list'])) {
					$q->whereHas('employeeEducation', function ($q) use ($request) {
						$q->whereIn('degree_id', $request->additional['degree_list']);
					});
				}
				if (!empty($request->additional['other_filters'])) {
					if (in_array('email_verified_only', $request->additional['other_filters'])) {
						$q->whereNotNull('email_verified_at');
					}if (in_array('women_candidate_only', $request->additional['other_filters'])) {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							$q->where('gender', '2');
						});
					}if (in_array('hide_profile_wout_resume', $request->additional['other_filters'])) {
						$q->has('employeeCvTxn');
						/*$q->whereHas('employeeCvTxn', function($q) use($request){
						});*/
					}
				}
				if (!empty($request->additional['experince'])) {
					if (!empty($request->additional['experince']['min_exp']) && empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
						});
					} elseif (!empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp, $maxexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					} elseif (empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $maxexp) {
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					}
				}
				if (!empty($request->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->additional['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($request->additional['salary']['currency'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary_currency', $request->additional['salary']['currency']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && !empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
					});
				}

				if (!empty($request->additional['salary']['max_salary']) && empty($request->additional['salary']['min_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}

				if (!empty($request->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			});
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});

			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
			if (!empty($request->formData['current_salary'])) {
				$q->whereHas('employee.employeePrefTxn', function ($q) use ($request) {
					$q->where('min_salary', '>=', ((int) $request->formData['current_salary'] - 10000));
					$q->where('max_salary', '<=', ((int) $request->formData['current_salary'] + 10000));
				});
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
		})->orderBy('_id', 'asc')->get();
		$departments = DepartmentResource::collection($department_data)->addParam($request, 'employee');
		$degrees = DegreeMst::orderBy('_id', 'asc')->get();
		$other_mst = AllOtherMasterMst::all();
		$employer_folders = EmployerFolderTxn::where('employer_id', auth('api')->user()->_id)->get();

		$limit = empty($request->limit) ? 12 : $request->limit;

		$employee_cv = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->where(function ($q) use ($request) {
			/* This is for folder search*/
			if (!empty($request->formData['search_type']) && $request->formData['search_type'] == "folder_wise") {
				$q->whereHas('employeeCVLike', function ($q) use ($request) {
					$q->where('employer_id', auth('api')->user()->_id);
				});
				if (!empty($request->formData['folder_id'])) {
					$q->whereHas('employerCvFolder', function ($q) use ($request) {
						$q->where('folder_id', $request->formData['folder_id']);
						$q->where('employer_id', auth('api')->user()->_id);
					});
				}
			}
			$q->whereHas('employeeEducation', function ($q) {
				$q->whereNotNull('_id');
			});
			$q->whereHas('employeeSkill', function ($q) {
				$q->whereNotNull('_id');
			});
			$q->whereHas('employeePrefTxn', function ($q) {
				$q->whereNotNull('_id');
			});
			/*$q->whereHas('employerUser', function ($q) {
				$q->whereNotNull('country_id');
				//$q->whereNotNull('city_id');
			});*/
			if (!empty($request->formData['key_skills'])) {
				$q->whereHas('employeeSkill', function ($q) use ($request) {
					$q->whereIn('title', $request->formData['key_skills']);
				});
			}
			/* If user has any of these skill*/
			if (!empty($request->formData['any_keyword'])) {
				if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						foreach ($request->formData['any_keyword'] as $value) {
							$q->where('profile_title', 'like', '%' . $value . '%');
						}
					});
				} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
					$q->whereHas('employeeSkill', function ($q) use ($request) {
						$q->whereIn('title', $request->formData['any_keyword']);
					})->orWhereHas('employeeUser', function ($q) use ($request) {
						foreach ($request->formData['any_keyword'] as $value) {
							$q->where('profile_title', 'like', '%' . $value . '%');
						}
					});
				} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
					$q->whereHas('employeeSkill', function ($q) use ($request) {
						$q->whereIn('title', $request->formData['any_keyword']);
					});
				}

			}
			/* If user must has these skill*/
			if (!empty($request->formData['must_keyword'])) {
				if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						foreach ($request->formData['must_keyword'] as $value) {
							$q->where('profile_title', 'like', '%' . $value . '%');
						}
					});
				} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
					$q->whereHas('employeeSkill', function ($q) use ($request) {
						foreach ($request->formData['must_keyword'] as $value) {
							$q->where('title', $value);
						}
					})->orWhereHas('employeeUser', function ($q) use ($request) {
						foreach ($request->formData['must_keyword'] as $value) {
							$q->where('profile_title', 'like', '%' . $value . '%');
						}
					});
				} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
					$q->whereHas('employeeSkill', function ($q) use ($request) {
						foreach ($request->formData['must_keyword'] as $value) {
							$q->where('title', $value);
						}
					});
				}

			}
			/* User donot have these skill*/
			if (!empty($request->formData['exclude_keyword'])) {
				if (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'full_profile') {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						foreach ($request->formData['exclude_keyword'] as $value) {
							$q->whereNot('profile_title', 'like', '%' . $value . '%');
						}
					});
				} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'profile_title_or_key_skills') {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						foreach ($request->formData['exclude_keyword'] as $value) {
							$q->whereNot('profile_title', 'like', '%' . $value . '%');
						}
					})->orWhereHas('employeeSkill', function ($q) use ($request) {
						$q->whereNotIn('title', $request->formData['exclude_keyword']);
					});
				} elseif (!empty($request->formData['profile_type']) && $request->formData['profile_type'] == 'key_skills') {
					$q->whereHas('employeeSkill', function ($q) use ($request) {
						$q->whereNotIn('title', $request->formData['exclude_keyword']);
					});
				}

			}

			if (!empty($request->formData['department_id'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('department_id', $request->formData['department_id']);
				});
			}

			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('industry_id', $request->formData['industry_id']);
				});
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('country_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('city_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['salary_type'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->where('salary_type', $request->formData['salary_type']);
				});
			}
			if (!empty($request->formData['currency'])) {
				$q->whereHas('employeePrefTxn.currency', function ($q) use ($request) {
					$q->where('_id', $request->formData['currency']);
				});
			}
			if (!empty($request->formData['min_monthly_salary'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->where('min_salary', '>=', $request->formData['min_monthly_salary']);
				});
			}
			if (!empty($request->formData['max_monthly_salary'])) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->where('max_salary', '<=', $request->formData['max_monthly_salary']);
				});
			}

			if (!empty($request->formData['min_qualification'])) {
				$q->whereHas('employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['min_qualification']);
					if (!empty($request->formData['education_type']) && $request->formData['education_type'] == 'underGraduate_or_postGraduate') {
						$q->orWhereIn('degree_id', $request->formData['max_qualification']);
					} elseif (!empty($request->formData['education_type']) && $request->formData['education_type'] == 'underGraduate_and_postGraduate') {
						if (!empty($request->formData['max_qualification'])) {
							$q->whereIn('degree_id', $request->formData['max_qualification']);
						}
					}
				});
			}
			if (!empty($request->formData['min_age']) && empty($request->formData['max_age'])) {
				$minAge = Carbon::now()->subYears($request->formData['min_age']);
				$q->whereHas('employeeUser', function ($q) use ($request, $minAge) {
					$q->where('date_of_birth', '>=', $minAge->format('Y-m-d'));
				});
			}
			if (!empty($request->formData['max_age']) && empty($request->formData['min_age'])) {
				$maxAge = Carbon::now()->subYears($request->formData['max_age']);
				$q->whereHas('employeeUser', function ($q) use ($request, $maxAge) {
					$q->where('date_of_birth', '<=', $maxAge->format('Y-m-d'));
				});
			}
			if (!empty($request->formData['min_age']) && !empty($request->formData['max_age'])) {
				$maxAge = Carbon::now()->subYears($request->formData['max_age']);
				$minAge = Carbon::now()->subYears($request->formData['min_age']);
				$q->whereHas('employeeUser', function ($q) use ($request, $maxAge, $minAge) {
					$q->where('date_of_birth', '>=', $minAge->format('Y-m-d'));
					$q->where('date_of_birth', '<=', $maxAge->format('Y-m-d'));
				});
			}
			if (!empty($request->formData['language_pref'])) {
				$q->whereHas('employeeLanguage', function ($q) use ($request) {
					$q->whereIn('lang_id', $request->formData['language_pref']);
				});
			}
			if (!empty($request->formData['active_in'])) {
				$activeDays = Carbon::now()->subDays($request->formData['active_in']);
				$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
			}
			if (!empty($request->formData['profile_view']) && $request->formData['profile_view'] == 'new_profile') {
				$prevDays = Carbon::now()->subDays(30);
				$q->where('created_at', '<=', $prevDays->format('Y-m-d'));
			}
			if (!empty($request->formData['min_experiance']) && !empty($request->formData['max_experiance'])) {
				$q->whereHas('employeeUser.totExprYr', function ($q) use ($request) {
					$q->where('_id', '>=', $request->formData['min_experiance']);
					$q->where('_id', '<=', $request->formData['max_experiance']);
				});
			}

			if (!empty($request->formData['women_only'])) {
				$q->whereHas('employeeUser', function ($q) use ($request) {
					$q->where('gender', 2);
				});
			}

			if (!empty($request->formData['search_candidate'])) {
				$q->whereHas('employeeUser', function ($q) use ($request) {
					if ($request->formData['search_by'] == 'search_by_name') {
						$name = explode(' ', $request->formData['search_candidate']);
						if (!empty($name[0])) {
							$q->where('first_name', 'like', '%' . $name[0] . '%');
						}
						if (!empty($name[1])) {
							$q->where('last_name', 'like', '%' . $name[1] . '%');
						}

					}
				});
				if ($request->formData['search_by'] == 'search_by_email') {
					$q->where('email', 'like', '%' . $request->formData['search_candidate'] . '%');
				}
			}
			// Additional Search
			if ($request->has('additional')) {

				if (!empty($request->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->additional['country_list']);
					});
				}

				if (!empty($request->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->additional['city_list']);
					});
				}
				if (!empty($request->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->additional['industry_list']);
					});
				}
				if (!empty($request->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->additional['department_list']);
					});
				}
				if (!empty($request->additional['degree_list'])) {
					$q->whereHas('employeeEducation', function ($q) use ($request) {
						$q->whereIn('degree_id', $request->additional['degree_list']);
					});
				}
				if (!empty($request->additional['other_filters'])) {
					if (in_array('email_verified_only', $request->additional['other_filters'])) {
						$q->whereNotNull('email_verified_at');
					}if (in_array('women_candidate_only', $request->additional['other_filters'])) {
						$q->whereHas('employeeUser', function ($q) use ($request) {
							$q->where('gender', '2');
						});
					}if (in_array('hide_profile_wout_resume', $request->additional['other_filters'])) {
						$q->has('employeeCvTxn');
						/*$q->whereHas('employeeCvTxn', function($q) use($request){
						});*/
					}
				}
				if (!empty($request->additional['experince'])) {
					if (!empty($request->additional['experince']['min_exp']) && empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
						});
					} elseif (!empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($request->additional['experince']['min_exp']);
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $minexp, $maxexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					} elseif (empty($request->additional['experince']['min_exp']) && !empty($request->additional['experince']['max_exp'])) {
						$maxexp = ExperienceMst::find($request->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($request, $maxexp) {
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					}
				}
				if (!empty($request->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->additional['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($request->additional['salary']['currency'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary_currency', $request->additional['salary']['currency']);
					});
				}

				if (!empty($request->additional['salary']['min_salary']) && !empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}
				if (!empty($request->additional['salary']['min_salary']) && empty($request->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '>=', (int) $request->additional['salary']['min_salary']);
					});
				}

				if (!empty($request->additional['salary']['max_salary']) && empty($request->additional['salary']['min_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($request) {
						$q->where('current_salary', '<=', (int) $request->additional['salary']['max_salary']);
					});
				}

				if (!empty($request->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					/*$q->whereRaw(['created_at' => array('$gt' => $prevDays->format('Y-m-d'), '$lt' => Carbon::now()->format('Y-m-d'))]);*/
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			}

			// check for institute students
			if (!empty($request->institute_id)) {
				$userUniqueCd = User::where('_id', $request->institute_id)->first()->user_unique_code;
				$q->whereHas('employeeUser', function ($q) use ($userUniqueCd) {
					$q->where('institute_code', $userUniqueCd);
				});

			}

			if (!empty($request->additional['course_list'])) {
				$q->whereHas('employeeUser.studentCourse', function ($q) use ($request) {
					$q->whereIn('_id', $request->additional['course_list']);
				});
			}

		})->orderBy('created_at', 'desc');

		$total_cv_count = $employee_cv->count();
		/*return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->skip(0)->take(10)->get()), 'countries' => $countries, 'industries' => $industries, 'departments' => $departments, 'roles' => $roles, 'other_mst' => $other_mst, 'employer_folders' => $employer_folders]);*/
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->skip(0)->take(10)->get()), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'roles' => $roles, 'other_mst' => $other_mst, 'employer_folders' => $employer_folders, 'departments' => $departments]);
	}

	public function getCvViewPackage() {
		$cv_view_packages = ServicesMst::where('service_type', 'cv_download_view')->where('needs_broadcast', 1)->orderBy('order_of_results', 'asc')->get();
		//echo "<pre>";print_r($cv_view_packages);
		return response()->json(['status' => 200, 'cv_view_packages' => $cv_view_packages]);
	}

	public function getCvByHomeSearch(Request $request) {
		$countries = CountryMst::orderBy('_id', 'asc')->get();
		$cities = CityMst::orderBy('_id', 'asc')->get();
		$industries = IndustryMst::orderBy('_id', 'asc')->get();
		$roles = JobRoleMst::orderBy('_id', 'asc')->get();
		$other_mst = AllOtherMasterMst::all();

		$limit = empty($request->limit) ? 12 : $request->limit;
		$employee_cv = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->where(function ($q) use ($request) {
			if (!empty($request->search_title)) {
				$q->whereHas('employeeEmployementHistory', function ($q) use ($request) {
					$q->where('job_title', 'like', '%' . $request->search_title . '%');
				});
			}
			if (!empty($request->search_industry)) {

				$q->whereHas('employeePrefTxn.industry', function ($q) use ($request) {
					$q->where('name', 'like', '%' . $request->search_industry . '%');
				});
			}

			if (!empty($request->search_location)) {
				$q->whereHas('employeePrefTxn.country', function ($q) use ($request) {
					$q->where('name', 'like', '%' . $request->search_location . '%');
				});
				$q->orWhereHas('employeePrefTxn.city', function ($q) use ($request) {
					$q->where('name', 'like', '%' . $request->search_location . '%');
				});
			}

		})->orderBy('created_at', 'desc');

		$total_cv_count = $employee_cv->count();
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->offset(0)->limit($limit)->get()), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'roles' => $roles, 'other_mst' => $other_mst]);
	}

	public function getCvListBySearch(Request $request) {
		//dd($request->all());
		//echo "<pre>";print_r($request->input('country_list'));die;
		$limit = empty($request->limit) ? 10 : $request->limit;
		$employee_cv = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->where(function ($q) use ($request) {

			if (!empty($request->input('country_list'))) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('country_id', $request->input('country_list'));
				});
			}
			if (!empty($request->input('city_list'))) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('city_id', $request->input('country_list'));
				});
			}
			if (!empty($request->input('industry_list'))) {
				$q->whereHas('employeePrefTxn', function ($q) use ($request) {
					$q->whereIn('industry_id', $request->input('country_list'));
				});
			}

		})->orderBy('created_at', 'desc');
		$total_cv_count = $employee_cv->count();
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->offset(0)->limit($limit)->get())]);
	}

	public function saveCvNote(Request $request) {
		//echo auth('api')->user()->_id;die;
		//dd($request->all());
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'note' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {

			//$allRequest = $request->except(['employer_id']);
			//$jobPost = EmployeeCvNote::create($allRequest + ['employer_id' => auth('api')->user()->_id]);
			$jobPost = EmployeeCvNote::updateOrCreate([
				//Add unique field combo to match here
				//For example, perhaps you only want one entry per user:
				'_id' => $request->id,
			], [
				'employer_id' => auth('api')->user()->_id,
				'employee_id' => $request->employee_id,
				'note' => $request->note,
			]);
			return response()->json(['status' => 200]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

		//dd($request->all());
	}

	public function saveCvHistory(Request $request) {

		//Check cv download and view payment requird
		$payment_config = PaymentConfig::first();
		if ($payment_config->cv_download_view == 1) {
			//check package activated/buy
			$service_ids = ServicesMst::where('service_type', 'cv_download_view')->pluck('_id');
			$current_date = Carbon::now();
			$active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0);

			$check_active_service_count = $active_service->count();
			if ($check_active_service_count != 0) {
				//update rmaining count
				$active_service_save = $active_service->first();
				$remaining_count = $active_service_save->remaining_count;
				$active_service_save->remaining_count = $remaining_count - 1;
				$active_service_save->save();
				try {
					//update cv view history
					$jobPost = EmployeeCvViewHistory::create(['employer_id' => auth('api')->user()->_id, 'employee_id' => $request->employee_id, 'remarks' => 'Mobile No viewed']);
					return response()->json(['status' => 200]);
				} catch (\Exception $e) {
					return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
				}
			} else {
				return response()->json(['status' => 422, 'status_text' => 'Please buy cv download and view package']);
			}

		} else {
			//update cv view history
			try {
				$jobPost = EmployeeCvViewHistory::create(['employer_id' => auth('api')->user()->_id, 'employee_id' => $request->employee_id, 'remarks' => 'Mobile No viewed']);
				return response()->json(['status' => 200]);
			} catch (\Exception $e) {
				return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
			}
		}
	}

	public function viewCvHistory(Request $request) {
		$cv_view_history = EmployeeCvViewHistory::where('employee_id', $request->employee_id)->where('employer_id', auth('api')->user()->_id)->orderBy('_id', 'desc')->get();
		//echo "<pre>";print_r($cv_view_history);die;
		return response()->json(['status' => 200, 'data' => EmployeeCvViewResource::collection($cv_view_history)]);
	}

	public function DownloadResume(Request $request) {
		try {
			$employee_idArr = explode(",", $request->employee_ids);

			$employee = User::whereIn('_id', $employee_idArr)->get();
			$temp_name_time = Carbon::now();
			/*$tempFolder = Storage::disk('s3')->url('/download_files/resume_download_/' . $temp_name_time);*/
			$tempFolder = public_path('/download_files/resume_download_' . $temp_name_time);
			if (!file_exists($tempFolder)) {
				mkdir($tempFolder);
			}

			foreach ($employee as $cv) {
				if (!empty($cv->employeeCvTxn)) {
					//$file = public_path("/upload_files/employee-resumes/" . $cv->employeeCvTxn->cv_path);
					//$file = Storage::disk('s3')->url('/upload_files/employee-resumes/1605674836_sample.pdf');
					if (Storage::disk('s3')->exists('/upload_files/employee-resumes/' . $cv->employeeCvTxn->cv_path)) {
						$file = Storage::disk('s3')->url('/upload_files/employee-resumes/' . $cv->employeeCvTxn->cv_path);
						if ($file) {
							copy($file, $tempFolder . '/' . $cv->employeeCvTxn->cv_path);
						}
					}

				}
			}

			$rootPath = realpath($tempFolder);

			$archiveFolder = $rootPath . ".zip";
			// Initialize archive object
			$zip = new \ZipArchive();
			$zip->open($archiveFolder, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = new \RecursiveIteratorIterator(
				new \RecursiveDirectoryIterator($rootPath),
				\RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file) {
				// Skip directories (they would be added automatically)
				if (!$file->isDir()) {

					// Get real and relative path for current file
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($rootPath) + 1);
					$zip->addFile($filePath, $relativePath);

				}
			}

			$zip->close();

			\File::deleteDirectory(public_path('/download_files/resume_download_' . $temp_name_time));
			// Set Header
			$headers = array(
				'Content-Type' => 'application/octet-stream',
			);
			$filetopath = public_path('/download_files/resume_download_' . $temp_name_time . '.zip');
			// Create Download Response
			if (file_exists($filetopath)) {
				return response()->download($filetopath, 'resume.zip', $headers)->deleteFileAfterSend(true);
			}
		} catch (\Exception $e) {
			return 'Some thing is wrong';
		}

	}

	public function saveReportedCandidate(Request $request) {

		try {
			if ($request->report_candidate_option == 'report_abuse') {
				ReportedCandidateTxn::create($request->all() + ['employer_id' => auth('api')->user()->_id]);
				return response()->json(['status' => 200]);
			} else {
				ReportedCandidateTxn::create(['employer_id' => auth('api')->user()->_id, 'employee_id' => $request->employee_id, 'report_candidate_option' => $request->report_candidate_option]);
				return response()->json(['status' => 200]);
			}
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function employeeCvsExport(Request $request) {
		$employee_ids = explode(',', $request->input('emp_id'));
		return Excel::download(new ExportEmployees($employee_ids), 'resumes.xlsx');
	}

	public function employeeDownloadResumeLink(Request $request) {
		$empCV = EmployeeCvTxn::where('_id', $request->resume_id)->first();
		$pathToFile = Storage::disk('s3')->url('upload_files/employee-resumes/' . $empCV->cv_path);
		//$pathToFile = public_path('upload_files/employee-resumes/' . $empCV->cv_path);
		return response()->download($pathToFile);
	}

	public function viewSimilarProfile(Request $request, $empid) {
		$employee = User::find($empid);
		$obj1 = new \stdClass();
		$obj1->formData['country_id'] = [$employee->employeeUser->country_id];
		$obj1->formData['city_id'] = $employee->employeePrefTxn->cities()->pluck('_id')->toArray();
		$obj1->formData['total_experience_yr'] = $employee->employeeUser->total_experience_yr;
		$obj1->formData['current_salary'] = $employee->employeeUser->current_salary;
		$obj1->formData['degree_id'] = $employee->employeeEducation()->pluck('degree_id')->toArray();
		$obj1->formData['key_skills'] = $employee->employeeSkill()->pluck('title')->toArray();
		$obj1->formData['non_emp_id'] = $empid;
		$countriesData = CountryMst::where(function ($q) use ($request, $employee) {
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('_id', $request->formData['country_id']);
			}
			$q->where('_id', $employee->employeeUser->country_id);
		})->orderBy('_id', 'asc')->get();
		$countries = CountryResource::collection($countriesData)->addParam($obj1, 'employee');
		$courses = Course::all();
		$citiesdata = CityMst::orderBy('_id', 'asc')->where('country_code', $employee->employeeUser->country->code)->get();
		$cities = CityResource::collection($citiesdata)->addParam($obj1, 'employee');
		$industriesdata = IndustryMst::orderBy('_id', 'asc')->get();
		$industries = IndustryResource::collection($industriesdata)->addParam($obj1, 'employee');
		$roles = JobRoleMst::orderBy('_id', 'asc')->get();
		$department_data = DepartmentMst::orderBy('_id', 'asc')->get();
		$departments = DepartmentResource::collection($department_data)->addParam($obj1, 'employee');
		$degrees = DegreeMst::orderBy('_id', 'asc')->get();
		$other_mst = AllOtherMasterMst::all();

		$limit = empty($request->limit) ? 12 : $request->limit;

		$employee_cv = User::where('_id', '!=', $empid)->whereHas('employeeUser', function ($q) use ($employee) {
			$q->where('country_id', $employee->employeeUser->country_id);
			$q->where('city_id', $employee->employeeUser->city_id);
			$q->whereHas('totExprYr', function ($q) use ($employee) {
				//$q->where('name', '>=', ($employee->employeeUser->totExprYr->name - 1));
				//$q->where('name', '<=', ((string) $employee->employeeUser->totExprYr->name + 1));
			});

		})->whereHas('employeeSkill', function ($q) use ($employee) {
			$q->whereIn('title', $employee->employeeSkill()->pluck('title')->toArray());

		})->whereHas('employeePrefTxn', function ($q) use ($employee) {
			$q->where('min_salary', '>=', ((int) $employee->employeeUser->current_salary - 10000));
			$q->where('max_salary', '<=', ((int) $employee->employeeUser->current_salary + 10000));
		})->whereHas('employeeEducation', function ($q) use ($employee) {
			$q->whereIn('degree_id', $employee->employeeEducation()->pluck('degree_id')->toArray());
		})->orderBy('created_at', 'desc');

		$total_cv_count = $employee_cv->count();
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->offset(0)->limit($limit)->get()), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'departments' => $departments, 'degrees' => $degrees, 'roles' => $roles, 'other_mst' => $other_mst]);

	}

	public function viewSimilarProfileByPostSearch(Request $request) {
		$employee = User::find($request->emp_id);
		$obj1 = new \stdClass();
		$obj1->formData['country_id'] = !empty($request->country_list) ? $request->country_list : [$employee->employeeUser->country_id];
		$obj1->formData['city_id'] = !empty($request->city_list) ? $request->city_list : $employee->employeePrefTxn->cities()->pluck('_id')->toArray();
		$obj1->formData['total_experience_yr'] = $employee->employeeUser->total_experience_yr;
		$obj1->formData['current_salary'] = $employee->employeeUser->current_salary;
		$obj1->formData['degree_id'] = !empty($request->degree_list) ? $request->degree_list : $employee->employeeEducation()->pluck('degree_id')->toArray();
		$obj1->formData['key_skills'] = $employee->employeeSkill()->pluck('title')->toArray();
		if (!empty($request->industry_list)) {
			$obj1->formData['industry_id'] = $request->industry_list;
		}

		if (!empty($request->department_list)) {
			$obj1->formData['department_id'] = $request->department_list;
		}

		$countriesData = CountryMst::where(function ($q) use ($request, $employee) {
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('_id', $request->formData['country_id']);
			}
			$q->where('_id', $employee->employeeUser->country_id);
		})->orderBy('_id', 'asc')->get();
		$countries = CountryResource::collection($countriesData)->addParam($obj1, 'employee');
		$courses = Course::all();
		$citiesdata = CityMst::orderBy('_id', 'asc')->where('country_code', $employee->employeeUser->country->code)->get();
		$cities = CityResource::collection($citiesdata)->addParam($obj1, 'employee');
		$industriesdata = IndustryMst::orderBy('_id', 'asc')->get();
		$industries = IndustryResource::collection($industriesdata)->addParam($obj1, 'employee');
		$roles = JobRoleMst::orderBy('_id', 'asc')->get();
		$department_data = DepartmentMst::orderBy('_id', 'asc')->get();
		$departments = DepartmentResource::collection($department_data)->addParam($obj1, 'employee');
		$degrees = DegreeMst::orderBy('_id', 'asc')->get();
		$other_mst = AllOtherMasterMst::all();

		$limit = empty($request->limit) ? 12 : $request->limit;

		$employee_cv = User::where('_id', '!=', $empid)->whereHas('employeeUser', function ($q) use ($employee) {
			$q->where('country_id', $employee->employeeUser->country_id);
			if (!empty($request->city_list)) {
				$q->where('city_id', $request->city_list);
			} else {
				$q->where('city_id', $employee->employeeUser->city_id);
			}

			$q->whereHas('totExprYr', function ($q) use ($employee) {
				//$q->where('name', '>=', ($employee->employeeUser->totExprYr->name - 1));
				//$q->where('name', '<=', ((string) $employee->employeeUser->totExprYr->name + 1));
			});

		})->whereHas('employeeSkill', function ($q) use ($employee) {

			$q->whereIn('title', $employee->employeeSkill()->pluck('title')->toArray());

		})->whereHas('employeePrefTxn', function ($q) use ($employee) {
			$q->where('min_salary', '>=', ((int) $employee->employeeUser->current_salary - 10000));
			$q->where('max_salary', '<=', ((int) $employee->employeeUser->current_salary + 10000));
		})->whereHas('employeeEducation', function ($q) use ($employee) {
			$q->whereIn('degree_id', $employee->employeeEducation()->pluck('degree_id')->toArray());
		})->orderBy('created_at', 'desc');

		$total_cv_count = $employee_cv->count();
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->offset(0)->limit($limit)->get()), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'departments' => $departments, 'degrees' => $degrees, 'roles' => $roles, 'other_mst' => $other_mst]);

	}

	public function saveProfileViewHistory(Request $request) {
		try {
			//update cv view history
			$jobPost = EmployeeCvViewHistory::create(['employer_id' => auth('api')->user()->_id, 'employee_id' => $request->employee_id, 'remarks' => 'Profile view']);
			return response()->json(['status' => 200]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

}
