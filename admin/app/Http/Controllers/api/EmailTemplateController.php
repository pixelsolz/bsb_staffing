<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmailTemplate as EmailTemplateResource;
use App\Models\AllOtherMasterMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\EmployerEmailTemplateTxn;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\EmployerJobTxn;
use App\Models\ExperienceMst;
use App\Models\LanguageMst;
use App\Models\MailTxn;
use App\Models\PaymentConfig;
use App\Models\ServicesMst;
use App\Models\SkillMst;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\BSON\Regex;
use Validator;

class EmailTemplateController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$user = auth('api')->user();
		$allEmailTemplate = EmployerEmailTemplateTxn::with('employerJob')->where('created_by', $user->_id)->orderBy('updated_at', 'desc')->get();
		$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 25);
		$emailres = EmployerEmailTemplateTxn::where('created_by', $user->_id)->orderBy('updated_at', 'desc');
		$emailTemplates = $emailres->offset($offset)->limit(25)->get();
		$count = $emailres->count();
		return response()->json(['status' => 200, 'data' => EmailTemplateResource::collection($emailTemplates), 'data_count' => $count, 'allEmailTemplate' => $allEmailTemplate]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$user = auth('api')->user();
		$messages = [
		];
		$validator = Validator::make($request->all(), [
			'template_name' => 'required',
			//'cv_received_email' => 'required',
			'subject' => 'required',
			'about_job' => 'required',
			'skill' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages(), 'data' => $request->all()]);
		}

		if (EmployerEmailTemplateTxn::where('template_name', 'regex', new Regex($request->template_name, 'i'))->first()) {
			return response()->json(['status' => 422, 'error' => 'exist']);
		}
		try {
			$requestdata = $request->all();
			$jobTxn = EmployerJobTxn::create(['job_title' => $request->job_title, 'about_this_job' => $request->about_job, 'benefits' => $request->benefit, 'country_id' => $request->country_id, 'city_id' => $request->city_id, 'currency' => $request->currency_type, 'min_experiance' => $request->min_exp, 'max_experiance' => $request->max_exp, 'min_monthly_salary' => $request->min_salary, 'max_monthly_salary' => $request->max_salary, 'currency' => $request->currency_type, 'salary_type' => $request->salary_type, 'required_skill' => $request->skill, 'primary_responsibility' => $request->primary_responsibility, 'special_notes' => $request->special_note, 'job_type' => 3, 'created_by' => $user->_id, 'employer_id' => $user->_id]);
			$jobTxn->language()->attach($request->language_pref);

			$template = EmployerEmailTemplateTxn::create(['job_id' => $jobTxn->_id, 'template_name' => $request->template_name, 'subject' => $request->subject, 'about_job' => $request->about_job, 'reviewed_flag' => 0, 'created_by' => $user->_id]);
			return response()->json(['status' => 200, 'status_text' => 'Successfully saved.', 'data' => $template]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$user = auth('api')->user();
		$template = EmployerEmailTemplateTxn::with('employerJob')->find($id);
		$allEmailTemplate = EmployerEmailTemplateTxn::with('employerJob')->where('created_by', $user->_id)->get();
		return response()->json(['status' => 200, 'data' => $template, 'allEmailTemplate' => $allEmailTemplate]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$user = auth('api')->user();
		$messages = [
		];
		$validator = Validator::make($request->all(), [
			'template_name' => 'required',
			//'cv_received_email' => 'required',
			'subject' => 'required',
			'about_job' => 'required',
			'skill' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {
			$template = EmployerEmailTemplateTxn::find($id);
			$template->fill(['template_name' => $request->template_name, 'subject' => $request->subject, 'about_job' => $request->about_job, 'updated_by' => $user->_id]);
			$template->save();
			$job = $template->employerJob;
			$job->fill(['job_title' => $request->job_title, 'about_this_job' => $request->about_job, 'benefits' => $request->benefit, 'country_id' => $request->country_id, 'city_id' => $request->city_id, 'currency' => $request->currency_type, 'min_experiance' => $request->min_exp, 'max_experiance' => $request->max_exp, 'min_monthly_salary' => $request->min_salary, 'max_monthly_salary' => $request->max_salary, 'currency' => $request->currency_type, 'salary_type' => $request->salary_type, 'required_skill' => $request->skill, 'primary_responsibility' => $request->primary_responsibility, 'special_notes' => $request->special_note, 'updated_by' => $user->_id]);
			$job->save();
			return response()->json(['status' => 200, 'data' => $template]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$template = EmployerEmailTemplateTxn::find($id);
			$job = $template->employerJob;
			$job->delete();
			$template->delete();
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function deleteAll(Request $request) {
		try {
			$ids = $request->all();
			$templates = EmployerEmailTemplateTxn::whereIn('id', $ids)->whereNull('last_used_date')->get();
			foreach ($templates as $template) {
				$templates->employerJob()->delete();
				$templates->delete();
			}
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function getAllTemplate() {
		$user = auth('api')->user();
		$emailTemplates = EmployerEmailTemplateTxn::with('employerJob')->where('created_by', $user->_id)->where('reviewed_flag', '1')->get();

		return response()->json(['status' => 200, 'data' => $emailTemplates]);
	}

	public function getTemplateCommonData() {
		$other_mst_data = AllOtherMasterMst::all();
		$languages = LanguageMst::all();
		$cities = CityMst::all();
		$countries = CountryMst::all();
		$skills = SkillMst::all();
		$allExprience = ExperienceMst::all();
		return response()->json(['status' => 200, 'data' => compact('other_mst_data', 'languages', 'cities', 'countries', 'skills', 'allExprience')]);
	}

	public function sendEmailTemplate(Request $request) {
		try {
			$user = auth('api')->user();
			$job = EmployerJobTxn::find($request->job_id);
			$job->templateJobSend()->wherePivotIn('temp_send_emp_id', $request->employee_id)->detach();
			$job->templateJobSend()->attach($request->employee_id);
			$template = $job->employerEmailTemplate;
			$template->last_used_date = Carbon::now()->format('Y-m-d H:i');
			$template->save();
			$data = ['job' => $job, 'user' => $user];
			$view = 'emails.template-job-send';
			$subject = 'Job Notification';
			$employees = User::whereIn('_id', $request->employee_id)->get();
			foreach ($employees as $employee) {
				MailTxn::create([
					'mail_receive_id' => $employee->_id,
					'mail_sender_id' => $user->_id,
					'mail_subject' => $template->subject,
					'mail_body' => $template->subject,
					'mail_status' => 1,
					'parent_id' => '',
					'template_id' => $template->_id,
				]);
				//Mail::to($employee->email)->send(new SendMail($data, $view, $subject));
			}

			$payment_config = PaymentConfig::first();
			if (!empty($payment_config->premium_job_post) && $payment_config->premium_job_post == 1) {
				$service_ids = ServicesMst::where('service_type', 'mass_mail_service')->pluck('_id');
				$current_date = Carbon::now();
				$active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0)->first();
				$remaining_count = $active_service->remaining_count;
				$active_service->remaining_count = $remaining_count - 1;
				$active_service->save();
			}

			return response()->json(['status' => 200, 'status_text' => 'Successfully send mail']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}
}
