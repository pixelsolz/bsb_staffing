<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CityEmployee as CityEmployeeResource;
use App\Http\Resources\CountryStudent as CountryStudentResource;
use App\Http\Resources\DepartmentEmployee as DepartmentEmployeeResource;
use App\Http\Resources\EmployeeCv as EmployeeCvResource;
use App\Http\Resources\EmployeerFolders as EmployeerFoldersResource;
use App\Http\Resources\IndustryEmployee as IndustryEmployeeResource;
use App\Models\AllOtherMasterMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\DegreeMst;
use App\Models\DepartmentMst;
use App\Models\EmployeeCvViewHistory;
use App\Models\EmployerCvSaveFolderTxn;
use App\Models\EmployerFolderTxn;
use App\Models\IndustryMst;
use App\Models\JobRoleMst;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class FolderController extends Controller {

	public function __construct() {
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 10);

		$employeer_folders = auth('api')->user()->employerFolders();
		$folders_count = $employeer_folders->count();
		//echo "<pre>";print_r($employeer_folders->offset($offset)->limit(10)->get());die;
		$empFolders = EmployeerFoldersResource::collection($employeer_folders->offset($offset)->limit(10)->get());
		return response()->json(['status' => 200, 'data' => $empFolders, 'total_folder' => $folders_count]);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'folder_name' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$folder = EmployerFolderTxn::create($request->all() + ['employer_id' => auth('api')->user()->_id]);
			return response()->json(['status' => 200]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id) {
		$countriesData = CountryMst::where(function ($q) use ($request) {
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('_id', $request->formData['country_id']);
			}

		})->whereHas('employeeCountry.user', function ($q) use ($request) {
			$q->whereHas('employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
		})->whereHas('employeeJobPreference', function ($q) use ($request) {
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
		})->get();
		$countries = CountryStudentResource::collection($countriesData)->addParam($request, 'employee');

		$citiesdata = CityMst::where(function ($q) use ($request) {
			if (!empty($request->formData['city_id'])) {
				$q->whereIn('_id', $request->formData['city_id']);
			}
		})->whereHas('employeeCity.user', function ($q) use ($request) {
			$q->whereHas('employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
		})->whereHas('employeeJobPreference', function ($q) use ($request) {
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
		})->get();
		$cities = CityEmployeeResource::collection($citiesdata)->addParam($request, 'employee');
		$industriesdata = IndustryMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee.employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});

			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
		})->get();
		$industries = IndustryEmployeeResource::collection($industriesdata)->addParam($request, 'employee');

		$department_data = DepartmentMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee.employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_id']);
				});
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_id']);
				});

			}
			if (!empty($request->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_id']);
				});
			}
		})->get();
		$departments = DepartmentEmployeeResource::collection($department_data)->addParam($request, 'employee');
		$roles = JobRoleMst::orderBy('_id', 'asc')->get();
		$degrees = DegreeMst::orderBy('_id', 'asc')->get();
		$other_mst = AllOtherMasterMst::all();
		$employer_folders = EmployerFolderTxn::where('employer_id', auth('api')->user()->_id)->get();

		$limit = empty($request->limit) ? 12 : $request->limit;
		if ($id == 'likescand') {

			$employee_cv = User::whereHas('roles', function ($q) {
				$q->where('slug', 'employee');
			})->where(function ($q) use ($id) {
				if (!empty($id)) {
					$q->whereHas('employerCvLike', function ($q) use ($id) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
				}
			});
		} else {
			$employee_cv = User::whereHas('roles', function ($q) {
				$q->where('slug', 'employee');
			})->where(function ($q) use ($id) {
				if (!empty($id)) {
					$q->whereHas('employerCvFolder', function ($q) use ($id) {
						$q->where('folder_id', $id);
						$q->where('employer_id', auth('api')->user()->_id);
					});
				}
			});
		}
		$employee_cv = $employee_cv->where(function ($q) use ($request) {
			// Additional Search
			if ($request->has('additional')) {

				if (!empty($request->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->additional['country_list']);
					});
				}

				if (!empty($request->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->additional['city_list']);
					});
				}
				if (!empty($request->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->additional['industry_list']);
					});
				}
				if (!empty($request->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->additional['department_list']);
					});
				}
				if (!empty($request->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->additional['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($request->additional['salary']['currency'])) {
					$q->where('currency', $request->additional['salary']['currency']);
				}

				if (!empty($request->additional['salary']['min_salary']) && !empty($request->additional['salary']['max_salary'])) {
					$q->where('min_monthly_salary', '>=', $request->additional['salary']['min_salary']);
					$q->where('max_monthly_salary', '<=', $request->additional['salary']['max_salary']);
				}
				if (!empty($request->additional['salary']['min_salary']) && empty($request->additional['salary']['max_salary'])) {
					$q->where('min_monthly_salary', '>=', $request->additional['salary']['min_salary']);
				}

				if (!empty($request->additional['salary']['max_salary']) && empty($request->additional['salary']['min_salary'])) {
					$q->where('min_monthly_salary', '<=', $request->additional['salary']['max_salary']);
				}

				if (!empty($request->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					/*$q->whereRaw(['created_at' => array('$gt' => $prevDays->format('Y-m-d'), '$lt' => Carbon::now()->format('Y-m-d'))]);*/
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			}

		});
		$total_cv_count = $employee_cv->count();
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->offset(0)->limit($limit)->get()), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'departments' => $departments, 'degrees' => $degrees, 'roles' => $roles, 'other_mst' => $other_mst, 'employer_folders' => $employer_folders]);
	}

	public function getCVbyPost(Request $request) {
		$countriesData = CountryMst::where(function ($q) use ($request) {
			if (!empty($request->formData['country_list'])) {
				$q->whereIn('_id', $request->formData['country_list']);
			}

		})->whereHas('employeeCountry.user', function ($q) use ($request) {
			$q->whereHas('employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
		})->whereHas('employeeJobPreference', function ($q) use ($request) {
			if (!empty($request->formData['city_list'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_list']);
				});
			}
			if (!empty($request->formData['industry_list'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_list']);
				});
			}
			if (!empty($request->formData['department_list'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_list']);
				});
			}
			if (!empty($request->formData['degree_list'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_list']);
				});
			}
		})->get();
		$countries = CountryStudentResource::collection($countriesData)->addParam($request, 'employee');

		$citiesdata = CityMst::where(function ($q) use ($request) {
			if (!empty($request->formData['city_list'])) {
				$q->whereIn('_id', $request->formData['city_list']);
			}
		})->whereHas('employeeCity.user', function ($q) use ($request) {
			$q->whereHas('employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
		})->whereHas('employeeJobPreference', function ($q) use ($request) {
			if (!empty($request->formData['industry_list'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_list']);
				});
			}
			if (!empty($request->formData['department_list'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_list']);
				});
			}
			if (!empty($request->formData['degree_list'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_list']);
				});
			}
		})->get();
		$cities = CityEmployeeResource::collection($citiesdata)->addParam($request, 'employee');
		$industriesdata = IndustryMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee.employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
			if (!empty($request->formData['industry_list'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_list']);
				});

			}
			if (!empty($request->formData['country_list'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_list']);
				});
			}
			if (!empty($request->formData['city_list'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_list']);
				});
			}
			if (!empty($request->formData['department_list'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_list']);
				});
			}
			if (!empty($request->formData['degree_list'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_list']);
				});
			}
		})->get();
		$industries = IndustryEmployeeResource::collection($industriesdata)->addParam($request, 'employee');

		$department_data = DepartmentMst::whereHas('employeeJobPreference', function ($q) use ($request) {
			$q->whereHas('employee.employeeCVLike', function ($q) use ($request) {
				$q->where('employer_id', auth('api')->user()->_id);
			});
			if (!empty($request->formData['country_list'])) {
				$q->whereHas('countries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_list']);
				});
			}
			if (!empty($request->formData['city_list'])) {
				$q->whereHas('cities', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_list']);
				});
			}
			if (!empty($request->formData['department_list'])) {
				$q->whereHas('departments', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['department_list']);
				});
			}
			if (!empty($request->formData['industry_list'])) {
				$q->whereHas('industries', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['industry_list']);
				});

			}
			if (!empty($request->formData['degree_list'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
					$q->whereIn('degree_id', $request->formData['degree_list']);
				});
			}
		})->get();
		$departments = DepartmentEmployeeResource::collection($department_data)->addParam($request, 'employee');
		$roles = JobRoleMst::orderBy('_id', 'asc')->get();
		$degrees = DegreeMst::orderBy('_id', 'asc')->get();
		$other_mst = AllOtherMasterMst::all();
		$employer_folders = EmployerFolderTxn::where('employer_id', auth('api')->user()->_id)->get();

		$limit = empty($request->limit) ? 100 : $request->limit;
		$id = $request->formData['folder_id'];
		if ($id == 'likescand') {

			$employee_cv = User::whereHas('roles', function ($q) {
				$q->where('slug', 'employee');
			})->where(function ($q) use ($id) {
				if (!empty($id)) {
					$q->whereHas('employerCvLike', function ($q) use ($id) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
				}
			});
		} else {
			$employee_cv = User::whereHas('roles', function ($q) {
				$q->where('slug', 'employee');
			})->where(function ($q) use ($id) {
				if (!empty($id)) {
					$q->whereHas('employerCvFolder', function ($q) use ($id) {
						$q->where('folder_id', $id);
						$q->where('employer_id', auth('api')->user()->_id);
					});
				}
			});
		}
		$employee_cv = $employee_cv->where(function ($q) use ($request) {
			// Additional Search
			if ($request->has('formData')) {

				if (!empty($request->formData['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('country_id', $request->formData['country_list']);
					});
				}

				if (!empty($request->formData['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('city_id', $request->formData['city_list']);
					});
				}
				if (!empty($request->formData['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('industry_id', $request->formData['industry_list']);
					});
				}
				if (!empty($request->formData['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($request) {
						$q->whereIn('department_id', $request->formData['department_list']);
					});
				}
				if (!empty($request->formData['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($request->formData['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($request->formData['salary']['currency'])) {
					$q->where('currency', $request->formData['salary']['currency']);
				}

				if (!empty($request->formData['salary']['min_salary']) && !empty($request->formData['salary']['max_salary'])) {
					$q->where('min_monthly_salary', '>=', $request->formData['salary']['min_salary']);
					$q->where('max_monthly_salary', '<=', $request->formData['salary']['max_salary']);
				}
				if (!empty($request->formData['salary']['min_salary']) && empty($request->formData['salary']['max_salary'])) {
					$q->where('min_monthly_salary', '>=', $request->formData['salary']['min_salary']);
				}

				if (!empty($request->formData['salary']['max_salary']) && empty($request->formData['salary']['min_salary'])) {
					$q->where('min_monthly_salary', '<=', $request->formData['salary']['max_salary']);
				}

				if (!empty($request->formData['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					/*$q->whereRaw(['created_at' => array('$gt' => $prevDays->format('Y-m-d'), '$lt' => Carbon::now()->format('Y-m-d'))]);*/
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			}

		});
		$total_cv_count = $employee_cv->count();
		return response()->json(['status' => 200, 'total_cv_count' => $total_cv_count, 'cvResults' => EmployeeCvResource::collection($employee_cv->offset(0)->limit($limit)->get()), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'departments' => $departments, 'degrees' => $degrees, 'roles' => $roles, 'other_mst' => $other_mst, 'employer_folders' => $employer_folders]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$folder = EmployerFolderTxn::find($id);
			$folder->delete();
			return response()->json(['status' => 200, 'status_text' => 'successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function deleteAll(Request $request) {
		$ids = $request->ids;
		try {
			EmployerFolderTxn::whereIn('_id', explode(",", $ids))->delete();
			$folder = EmployerFolderTxn::all();
			$folders_count = $folder->count();
			$empFolders = EmployeerFoldersResource::collection($folder);
			return response()->json(['status' => 200, 'data' => $empFolders, 'total_folder' => $folders_count]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function saveCvFolder(Request $request) {
		//dd($request->all());
		$folder_id = '';
		if ($request->folder_save == 'create_new_folder') {
			$folder = EmployerFolderTxn::create(['folder_name' => $request->folder_name, 'employer_id' => auth('api')->user()->_id]);
			$folder_id = $folder->_id;

		} else if ($request->folder_save == 'existing_folder') {
			$folder_id = $request->folder_id;
		}

		if (!empty($request->employee_ids)) {
			$status = '';
			$cv_save_folder = EmployerCvSaveFolderTxn::create(['folder_id' => $folder_id, 'employer_id' => auth('api')->user()->_id, 'employee_id' => $request->employee_ids]);
			foreach ($request->employee_ids as $employee_id) {

				EmployeeCvViewHistory::create(['employer_id' => auth('api')->user()->_id, 'employee_id' => $employee_id, 'remarks' => 'Profile added to folder']);

			}
			return response()->json(['status' => 200]);
		}
	}

	public function getCvFolderById(Request $request) {
		$folderData = EmployerCvSaveFolderTxn::where('folder_id', $request->folder_id)->get();
		return response()->json(['status' => 200, 'folder_data' => $folderData]);
	}

	public function getEmployeeSkills(Request $request) {
		$user = User::find($request->user_id);
		$skills = $user->employeeSkill ? $user->employeeSkill()->pluck('title')->toArray() : [];
		return response()->json(['status' => 200, 'user_skills' => $skills]);
	}

}
