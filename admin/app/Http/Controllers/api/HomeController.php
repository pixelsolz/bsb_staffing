<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AllOtherMaster as AllOtherMasterResource;
use App\Http\Resources\CarrerPage as CarrerPageResource;
use App\Http\Resources\CityHome;
use App\Http\Resources\CountryHome;
use App\Http\Resources\Degree as DegreeMstResource;
use App\Http\Resources\EmployeeAppliedJobs as EmployeeAppliedJobsResource;
use App\Http\Resources\EmployeeProfile as EmployeeProfileResource;
use App\Http\Resources\EmployeerJobs as EmployerJobsResource;
use App\Http\Resources\EmployerJobsSummary as EmployerJobsSummaryResource;
use App\Http\Resources\IndustryHome;
use App\Http\Resources\JobCityFooter as JobCityFooterResource;
use App\Http\Resources\JobIndustryFooter as JobIndustryFooterResource;
use App\Http\Resources\JobTitleFooter as JobTitleFooterResource;
use App\Http\Resources\LanguageHome;
use App\Http\Resources\LogoBranding as LogoBrandingResource;
use App\Http\Resources\PopularSearchFooter as PopularSearchFooterResource;
use App\Http\Resources\User as UserResource;
use App\Models\AllOtherMasterMst;
use App\Models\BenefitMst;
use App\Models\CareerPageTxn;
use App\Models\CityMst;
use App\Models\CompanySizeMst;
use App\Models\CompanyTypeMst;
use App\Models\CountryMst;
use App\Models\Course;
use App\Models\DegreeMst;
use App\Models\DepartmentMst;
use App\Models\EmployeeAppliedJobsTxn;
use App\Models\EmployeeCvViewHistory;
use App\Models\EmployeeProfileTxn;
use App\Models\EmployeeSavedJobsTxn;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\EmployerJobTxn;
use App\Models\ExperienceMst;
use App\Models\IndustryMst;
use App\Models\JobRoleMst;
use App\Models\JobTitleMst;
use App\Models\LanguageMst;
use App\Models\PopularJobsearchTxn;
use App\Models\ServicesMst;
use App\Models\SkillMst;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

	public function searchJobDropdown(Request $request) {
		if ($request->type == 'TITLE_SKILL') {
			$skills = SkillMst::where('skill', 'like', $request->q . '%')->get();
			$jbTitles = JobTitleMst::where('job_title', 'like', $request->q . '%')->get();
			$jbTitlesCollect = collect($jbTitles)->map(function ($item, $key) {
				return ['display' => $item->job_title, 'value' => $item->job_title];
			})->all();
			$skillCollect = collect($skills)->map(function ($item, $key) {
				return ['display' => $item->skill, 'value' => $item->skill];
			})->all();
			$title_skills = collect($jbTitlesCollect)->merge($skillCollect)->all();
			return response()->json(['status' => 200, 'data' => $title_skills]);
		} elseif ($request->type == 'INDUSTRY') {
			$industries = IndustryMst::select('name')->where('name', 'like', $request->q . '%')->get();
			$industrieCollect = collect($industries)->map(function ($item, $key) {
				return ['display' => $item->name, 'value' => $item->name];
			})->all();
			return response()->json(['status' => 200, 'data' => $industrieCollect]);
		} elseif ($request->type == 'COUNTRY_CITY') {
			$countries = CountryMst::select('name')->where('name', 'like', $request->q . '%')->get();
			$cities = CityMst::select('name')->where('name', 'like', $request->q . '%')->get();
			$countriesCollect = collect($countries)->map(function ($item, $key) {
				return ['display' => $item->name, 'value' => $item->name];
			})->all();
			$citiesCollect = collect($cities)->map(function ($item, $key) {
				return ['display' => $item->name, 'value' => $item->name];
			})->all();
			$coutry_city = collect($countriesCollect)->merge($citiesCollect)->all();
			return response()->json(['status' => 200, 'data' => $coutry_city]);
		} elseif ($request->type == 'DEPATRMENT') {
			$departments = DepartmentMst::select('name')->where('name', 'like', $request->q . '%')->get();
			$departmentCollect = collect($departments)->map(function ($item, $key) {
				return ['display' => $item->name, 'value' => $item->name];
			})->all();
			return response()->json(['status' => 200, 'data' => $departmentCollect]);
		}

	}

	public function searchForDropdown(Request $request) {
		$type = $request->type;
		$keyword = $request->keyword;
		$data = '';
		$usedData = !empty($request->input('used')) ? explode(',', $request->input('used')) : [];
		switch ($type) {
		case 'JB_TITLE_WISE':
			$skills = SkillMst::where('skill', 'like', $keyword . '%')->whereNotIn('skill', $usedData)->get();
			$jbTitles = JobTitleMst::where('job_title', 'like', $keyword . '%')->whereNotIn('job_title', $usedData)->get();
			$jbTitlesCollect = collect($jbTitles)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['job_title'],
				];
			})->all();
			$skillCollect = collect($skills)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['skill'],
				];
			})->all();
			$data = collect($jbTitlesCollect)->merge($skillCollect)->all();
			break;
		case 'COUNTRY_WISE':
			$countries = CountryMst::select('name')->where('name', 'like', $keyword . '%')->whereNotIn('name', $usedData)->get();
			$countryData = collect($countries)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$cities = CityMst::select('name')->where('name', 'like', $keyword . '%')->whereNotIn('name', $usedData)->get();
			$cityData = collect($cities)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $countryData->merge($cityData)->all();

			break;
		case 'INDUSTRY_WISE':
			$industries = IndustryMst::select('name')->where('name', 'like', $keyword . '%')->whereNotIn('name', $usedData)->get();
			$industriesData = collect($industries)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $industriesData->all();

			break;
		case 'DEPARTMENT_WISE':
			$departments = DepartmentMst::select('name')->where('name', 'like', $keyword . '%')->whereNotIn('name', $usedData)->get();
			$departmentData = collect($departments)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $departmentData->all();

		default:
			// code...
			break;
		}

		return response()->json(['status' => 200, 'data' => $data, 'type' => $type]);
	}

	public function searchForDropdownEmployer(Request $request) {
		$type = $request->type;
		$keyword = $request->keyword;
		$data = '';

		switch ($type) {
		case 'JB_TITLE_WISE':
			$employee_pref = EmployeeProfileTxn::select('profile_title')->where('profile_title', 'like', '%' . $keyword . '%')->get();
			$employeePrefData = collect($employee_pref)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['profile_title'],
				];
			});
			$skill = SkillMst::select('skill')->where('skill', 'like', '%' . $keyword . '%')->get();
			$skillData = collect($skill)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['skill'],
				];
			});
			$data = $employeePrefData->merge($skillData)->all();
			break;
		case 'COUNTRY_WISE':
			$countries = CountryMst::select('name')->where('name', 'like', '%' . $keyword . '%')->get();
			$countryData = collect($countries)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$cities = CityMst::select('name')->where('name', 'like', '%' . $keyword . '%')->get();
			$cityData = collect($cities)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $countryData->merge($cityData)->all();
			break;
		case 'INDUSTRY_WISE':
			$industries = IndustryMst::select('name')->where('name', 'like', '%' . $keyword . '%')->get();
			$industriesData = collect($industries)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $industriesData->all();

		default:
			// code...
			break;
		}

		return response()->json(['status' => 200, 'data' => $data, 'type' => $type]);
	}

	public function getSearchJob(Request $request) {

	}

	public function getCountryList(Request $request) {
		$limit = empty($request->limit) ? 6 : (int) $request->limit;
		//$country = CountryMst::with(['jobs', 'internationalJobs'])->get();
		$country = CountryHome::collection(CountryMst::get());
		$countryList = collect($country)->sortByDesc('job_count')->values()->all();
		$user_ip = $this->getVisIpAddr();
		$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $user_ip));
		$country_name_by_ip = $ipdat->geoplugin_countryName;

		//return response()->json($countryList);
		return response()->json(['status' => 200, 'data' => $countryList, 'country_name_by_ip' => $country_name_by_ip]);
	}

	public function getCountriesAndIndustries() {
		$countries = CountryMst::orderBy('name')->get();
		$industries = collect(IndustryHome::collection(IndustryMst::whereNotNull('name')->where('name', '!=', "")->orderBy('name')->get()))->groupBy('attr_name')->toArray();
		$all_industries = IndustryMst::whereNotNull('name')->where('name', '!=', "")->orderBy('name')->get();
		$companySizes = CompanySizeMst::get(['company_size', '_id']);
		$companyTypes = CompanyTypeMst::get(['company_type', '_id']);
		$courses = collect(Course::orderBy('name')->get(['_id', 'name']))->groupBy('')->toArray();
		return response()->json(['status' => 200, 'countries' => CountryHome::collection($countries), 'industries' => $industries, 'all_industries' => IndustryHome::collection($all_industries), 'companySizes' => $companySizes, 'companyTypes' => $companyTypes, 'courses' => $courses]);
	}

	public function getcities() {
		$cities = CityMst::orderBy('name')->get();
		return response()->json(['status' => 200, 'cities' => CityHome::collection($cities)]);
	}

	public function getCommonData() {

		$all_currency = collect(CountryHome::collection(CountryMst::orderBy('currency')->get()))->groupBy('currency')->toArray();

		$courses = collect(Course::orderBy('name')->get(['name', '_id']))->groupBy('')->toArray();
		$jobRoles = JobRoleMst::all();
		$other_mst = AllOtherMasterResource::collection(AllOtherMasterMst::all());

		$under_graduate_deg = DegreeMstResource::collection(DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 2);})->with('child')->get());
		$post_graduate_deg = DegreeMstResource::collection(DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 3);})->with('child')->get());
		$under_graduate_parent = DegreeMstResource::collection(DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 2);})->where('parent_id', '0')->with('child')->orderBy('name')->get());
		$post_graduate_parent = DegreeMstResource::collection(DegreeMst::whereHas('attribute', function ($q) {$q->where('attr_type', 3);})->where('parent_id', '0')->with('child')->orderBy('name')->get());
		$post_under_parent = DegreeMstResource::collection(DegreeMst::whereHas('attribute', function ($q) {$q->whereIn('attr_type', [2, 3]);})->where('parent_id', '0')->with('child')->orderBy('name')->get());

		$under_graduate = collect($under_graduate_parent)->groupBy('attr_name')->toArray();
		$post_graduate = collect($post_graduate_parent)->groupBy('attr_name')->toArray();
		$post_under_graduate = collect($post_under_parent)->groupBy('attr_name')->toArray();
		$alllanguage = LanguageMst::all();
		$allExprience = ExperienceMst::all();
		$skills = SkillMst::all();
		$jbTitles = JobTitleMst::all();
		$benefit_data = BenefitMst::where('status', "1")->get();
		$degrees = DegreeMstResource::collection(DegreeMst::all());
		/*$user_ip = $this->getVisIpAddr();
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $user_ip));
		*/

		return response()->json(['status' => 200, 'all_currency' => $all_currency, 'job_roles' => $jobRoles, 'other_mst' => $other_mst, 'under_graduate' => $under_graduate, 'post_graduate' => $post_graduate, 'alllanguage' => LanguageHome::collection($alllanguage), 'all_department' => [], 'courses' => $courses, 'all_parent_department' => [], 'allExprience' => $allExprience, 'skills' => $skills, 'under_graduate_deg' => $under_graduate_deg, 'post_graduate_deg' => $post_graduate_deg, 'benefit_data' => $benefit_data, 'job_titles' => $jbTitles, 'departments' => [], 'degrees' => $degrees, 'post_under_parent' => $post_under_parent]);

	}

	public function getVisIpAddr() {

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			return $_SERVER['HTTP_CLIENT_IP'];
		} else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}

	// joblist by country
	public function getJobByCountry($country_name, Request $request) {
		$limit = empty($request->limit) ? 12 : $request->limit;
		$country = CountryMst::where('name', $country_name)->first();
		$countryJobs = EmployerJobTxn::where('country_id', $country->id)->offset(0)->limit($limit)->get();
		//echo "<pre>";print_r($countryJobs);
		return response()->json(['status' => 200, 'countryJobs' => EmployerJobsResource::collection($countryJobs)]);
	}

	//posted job list by employer login user

	public function employerPostedJobList(Request $request) {
		//return $request->user;
		$all_posted_job = EmployerJobTxn::all();
		return response()->json(['status' => 200, 'allPostedJob' => $all_posted_job, 'user' => auth('api')->user()]);
	}

	public function getJobById($id) {
		$user = auth('api')->user();
		$job = EmployerJobTxn::find($id);
		if ($user) {
			$job->jobViews()->detach($user->_id);
			$job->jobViews()->attach($user->_id);
		}

		return response()->json(['status' => 200, 'job' => new EmployerJobsResource($job)]);
	}

	public function otherUserHome(Request $request) {
		$user = auth('api')->user();
		$user_data = new UserResource($user);
		$referral_user = EmployeeProfileTxn::where('institute_code', $user->user_unique_code);
		$cv_uploaded = $referral_user->count();
		$referral_user_ids = $referral_user->pluck('user_id');

		$count_cv_view = EmployeeCvViewHistory::whereIn('employee_id', $referral_user_ids)->groupBy('employer_id')->get();
		//return $count_cv_view->count();
		$cv_view_employeer = $count_cv_view->count();

		$empployee_applied_job = EmployeeAppliedJobsTxn::whereIn('employee_id', $referral_user_ids);

		$no_std_applied_job = $empployee_applied_job->count();
		$no_std_shortlist = $empployee_applied_job->where('applied_status', '5d283d45a8beed4887df72d2')->count();
		$no_std_comp_interview = $empployee_applied_job->where('applied_status', '5d283d45a8beed4887df72d2')->count();
		$no_std_placed = $empployee_applied_job->where('applied_status', '5d53c9c27c78e044ed45ec82')->count();
		$no_prime_cv = 0;
		$no_cv_rejected_employeer = $empployee_applied_job->where('applied_status', '5d283d45a8beed4887df72d4')->count();
		$finance_summary = 0;
		$benefit_data = BenefitMst::where('status', "1")->get();
		if ($user->roles()->first()->slug == 'college') {
			return response()->json(['status' => 200, 'data' => compact('user_data', 'cv_uploaded', 'cv_view_employeer', 'no_std_applied_job', 'no_std_shortlist', 'no_std_comp_interview', 'no_std_placed', 'benefit_data')]);
		} else {
			return response()->json(['status' => 200, 'data' => compact('user_data', 'cv_uploaded', 'cv_view_employeer', 'no_std_applied_job', 'no_std_shortlist', 'no_std_comp_interview', 'no_std_placed', 'no_prime_cv', 'no_cv_rejected_employeer', 'finance_summary', 'benefit_data')]);
		}

	}

	public function getStudents(Request $request) {
		$type = $request->type;
		$user = auth('api')->user();
		$students = [];
		$institute_students = User::whereHas('employeeUser', function ($q) use ($user) {
			$q->where('institute_code', $user->user_unique_code);
		});
		$institute_students_ids = $institute_students->pluck('_id');
		//return $institute_students_ids;
		if ($type == 'cv-upload') {
			$students = $institute_students->get();
		}
		/*if($type == 'applied-job'){
			$empployee_applied_job = EmployeeAppliedJobsTxn::whereIn('employee_id',$institute_students_ids);
			return $students = $empployee_applied_job->get();
		}*/

		if (!empty($students)) {
			return response()->json(['status' => 200, 'data' => UserResource::collection($students)]);
		} else {
			return response()->json(['status' => 200, 'data' => []]);
		}
	}

	public function getEmployeeDashboardDet() {
		$user = auth('api')->user();
		$profile = new EmployeeProfileResource($user->employeeUser);
		$unread_msg_count = 0;
		$candType = ($user->employeeUser->gender == 1) ? 2 : 3;
		$userIndustry = ($user->employeePrefTxn && $user->employeePrefTxn->industries) ? $user->employeePrefTxn->industries()->pluck('_id')->toArray() : ($user->employeeUser->industry_id ? [$user->employeeUser->industry_id] : []);
		//$userExpValue = ExperienceMst::where('_id', $user->employeeUser->total_experience_yr)->first();
		$userExpValue = $user->employeeUser->total_experience_yr_value;
		//$profile_view_count = 0;

		$profile_view = EmployeeCvViewHistory::where('employee_id', $user->_id)->groupBy('employer_id')->get();
		$profile_view_count = $profile_view->count();
		$pending_action_count = 0;

		$recomended_jobs = EmployerJobTxn::whereHas('empJobAlert', function ($q) use ($user) {
			$q->where('emp_id', $user->_id);
			$q->where('alert_type', 'recommend');
		})->whereIn('industry_id', $userIndustry)
			->whereHas('minExperiance', function ($q) use ($userExpValue) {
				if ($userExpValue) {
					$q->where('value', '<=', $userExpValue);
				}

			})->where(function ($q) use ($candType) {
			$q->where('candidate_type', $candType);
			$q->orWhere('candidate_type', 1);
		})->doesntHave('employeeSavedJob')->doesntHave('employeeAppliedJob')->orderBy('_id', 'desc')->skip(0)->take(3)->get();

		$recomended_jobs_data = EmployerJobsSummaryResource::collection($recomended_jobs);

		$alert_jobs = EmployerJobTxn::whereHas('empJobAlert', function ($q) use ($user) {
			$q->where('emp_id', $user->_id);
			$q->where('alert_type', 'my-alert');
		})->whereIn('industry_id', $userIndustry)->whereHas('minExperiance', function ($q) use ($userExpValue) {
			if ($userExpValue) {
				$q->where('value', '<=', $userExpValue);
			}
		})->where(function ($q) use ($candType) {
			$q->where('candidate_type', $candType);
			$q->orWhere('candidate_type', 1);
		})->doesntHave('employeeSavedJob')->doesntHave('employeeAppliedJob')->orderBy('_id', 'desc');

		$alert_jobs_count = $alert_jobs->count();
		$alert_jobs_data = EmployerJobsSummaryResource::collection($alert_jobs->skip(0)->take(3)->get());

		$applied_jobs = EmployeeAppliedJobsResource::collection($user->employeeAppliedJob()->orderBy('_id', 'desc')->skip(0)->take(3)->get());
		$applied_jobs_count = $user->employeeAppliedJob()->count();

		$saved_jobs_count = EmployeeSavedJobsTxn::where('employee_id', $user->_id)->whereNull('employee_application_id')->orderBy('_id', 'desc')->count();

		//Start count Profile Strength
		$count_profle_strength = 0;
		if ($profile->profile_picture != '') {
			$count_profle_strength += 10;
		}
		if (!empty($user->employeeCvTxn)) {
			$count_profle_strength += 10;
		}
		//For Personal Information
		if (!empty($profile->date_of_birth)) {
			$count_profle_strength += 10;
		}

		if (!empty($user->employeePrefTxn)) {
			$count_profle_strength += 10;
		}
		// Summary of your profile
		if (!empty($profile->profile_title)) {
			$count_profle_strength += 10;
		}
		if (!empty($user->employeeEmployementHistory) && count($user->employeeEmployementHistory) != 0) {
			$count_profle_strength += 10;
		}
		if (!empty($user->employeeEducation) && count($user->employeeEducation) != 0) {
			$count_profle_strength += 10;
		}
		if (!empty($user->employeeSkill) && count($user->employeeSkill) != 0) {
			$count_profle_strength += 10;
		}
		if (!empty($user->employeeCertification) && count($user->employeeCertification) != 0) {
			$count_profle_strength += 10;
		}
		if (!empty($user->employeeLanguage) && count($user->employeeLanguage) != 0) {
			$count_profle_strength += 10;
		}
		//return $count_profle_strength;
		//End count Profile Strength

		return response()->json(['status' => 200, 'data' => compact('profile', 'unread_msg_count', 'saved_jobs_count', 'profile_view_count', 'pending_action_count', 'recomended_jobs_data', 'alert_jobs_count', 'alert_jobs_data', 'applied_jobs', 'applied_jobs_count', 'count_profle_strength')]);
	}

	public function getEmployerDashboardDeta() {
		$user = auth('api')->user();
		$employer_job = auth('api')->user()->employerJobs()->where('job_status', 1)->where('created_at', '>', Carbon::now()->subDays(30));
		$recent_jobs = EmployerJobsResource::collection($employer_job->orderBy('_id', 'desc')->skip(0)->take(3)->get());
		$recent_jobs_count = $employer_job->count();

		$total_closed_job = auth('api')->user()->employerJobs()->where('job_status', 0)->count();
		$total_open_job = auth('api')->user()->employerJobs()->where('job_status', 1)->count();
		//echo "<pre>";print_r($employer_job->get());
		$total_cv_received = EmployeeAppliedJobsTxn::whereHas('job', function ($q) use ($user) {
			$q->where('employer_id', $user->_id);
		})->count();

		$new_cv_received = EmployeeAppliedJobsTxn::whereHas('job', function ($q) use ($user) {
			$q->where('employer_id', $user->_id);
		})
			->where('applied_status', '5d283d45a8beed4887df72cf')
			->count();
		$cv_reviewed = EmployeeAppliedJobsTxn::whereHas('job', function ($q) use ($user) {
			$q->where('employer_id', $user->_id);
		})
			->where('applied_status', '5d283d45a8beed4887df72d0')
			->count();

		$current_date = Carbon::now();
		//check payment premium job post
		$premium_job_post_service_ids = ServicesMst::where('service_type', 'premium_job_post')->pluck('_id');
		$premium_job_post_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $premium_job_post_service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->get();
		//check cv download view service
		$cv_download_view_service_ids = ServicesMst::where('service_type', 'cv_download_view')->pluck('_id');
		$cv_download_view_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $cv_download_view_service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->get();
		// mass_mail_service
		$mass_mail_service_service_ids = ServicesMst::where('service_type', 'mass_mail_service')->pluck('_id');
		$mass_mail_service_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $mass_mail_service_service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->get();
		//international_walk_in_interview
		$international_walk_in_interview_service_ids = ServicesMst::where('service_type', 'international_walk_in_interview')->pluck('_id');
		$international_walk_in_interview_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $international_walk_in_interview_service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->get();

		$carrer = CareerPageTxn::where('user_id', $user->_id)->first();
		$carrerdata = [];
		if (!empty($carrer)) {
			$carrerdata = new CarrerPageResource($carrer);
		}
		//return $carrerdata;
		return response()->json(['status' => 200, 'data' => compact('recent_jobs', 'recent_jobs_count', 'total_cv_received', 'total_open_job', 'total_closed_job', 'new_cv_received', 'cv_reviewed', 'premium_job_post_service', 'cv_download_view_service', 'mass_mail_service_service', 'international_walk_in_interview_service', 'carrerdata')]);
	}

	public function getLogoBranding() {
		$employers = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})
			->whereHas('employerUser', function ($q) {
				$q->whereNotNull('company_logo');
			})
			->get();
		//return LogoBrandingResource::collection($employers);
		return response()->json(['status' => 200, 'data' => LogoBrandingResource::collection($employers)]);
	}

	public function getFooterJobs() {
		$job_title = EmployerJobTxn::where('job_status', 1)->groupBy('job_title')->take(40)->get();
		$jobs_by_title = JobTitleFooterResource::collection($job_title);
		$job_city = CityMst::/*orderBy('name')->*/limit(40)->get();
		$jobs_by_city = JobCityFooterResource::collection($job_city);
		$job_industry = IndustryMst::take(40)->get();
		$jobs_by_industry = JobIndustryFooterResource::collection($job_industry);
		$popular_job = PopularJobsearchTxn::take(40)->orderBy('count_search_key', 'DESC')->get();
		$popular_job_search = PopularSearchFooterResource::collection($popular_job);
		$citieswithCount = CityMst::raw(function ($collection) {
			return $collection->aggregate([
				['$addFields' => ['job_count' => ['$cond' => ['if' => ['$isArray' => '$job_id'], 'then' => ['$size' => '$job_id'], 'else' => 0]]]],
				['$addFields' => ["international_job_count" => ['$cond' => ['if' => ['$isArray' => '$international_job_id'], 'then' => ['$size' => '$international_job_id'], 'else' => 0]]]],
				['$addFields' => ['total_count' => ['$add' => ['$job_count', '$international_job_count']]]],
				['$sort' => ['total_count' => -1]],

				['$limit' => 40],
				['$project' => ['name' => 1, 'country_code' => 1, 'job_count' => 1, 'international_job_count' => 1, 'total_count' => 1]],
			]);
		});

		return response()->json(['status' => 200, 'data' => compact('citieswithCount', 'jobs_by_title', 'jobs_by_city', 'jobs_by_industry', 'popular_job_search')]);
	}

	public function saveSearchKeyword(Request $request) {
		$popular_search_key = PopularJobsearchTxn::firstOrNew(['search_key' => $request->keyword]);
		$popular_search_key->count_search_key = ($popular_search_key->count_search_key + 1);
		$popular_search_key->save();
		return response()->json(['status' => 200]);
	}

	public function getSkillFilter(Request $request) {
		$skills = SkillMst::where('skill', 'like', $request->q . '%')->get();
		$collections = collect($skills);
		$newData = $collections->map(function ($item) {
			return ['display' => $item->skill, 'value' => $item->skill];
		});

		return response()->json(['status' => 200, 'data' => $newData->all()]);
	}
	public function getSkillJobTitleFilter(Request $request) {
		$skills = SkillMst::where('skill', 'like', $request->q . '%')->get();
		$jbTitles = JobTitleMst::where('job_title', 'like', $request->q . '%')->get();
		$jbTitlesCollect = collect($jbTitles)->map(function ($item, $key) {
			return [
				'id' => $item['_id'],
				'name' => $item['job_title'],
			];
		})->all();
		$skillCollect = collect($skills)->map(function ($item, $key) {
			return [
				'id' => $item['_id'],
				'name' => $item['skill'],
			];
		})->all();
		$skillAndJobTitles = collect($jbTitlesCollect)->merge($skillCollect);

		$newData = $skillAndJobTitles->map(function ($item, $key) {
			//return $item->all();
			return ['display' => $item['name'], 'value' => $item['name']];
		});

		return response()->json(['status' => 200, 'data' => $newData->all()]);
	}

	public function getCityByCode($code) {
		$cities = CityMst::where('country_code', $code)->orderBy('name')->get();
		return response()->json(['status' => 200, 'data' => CityHome::collection($cities)]);
	}

}
