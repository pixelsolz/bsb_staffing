<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InternationalJobSavedApplied as InternationalJobSavedAppliedResource;
use App\Http\Resources\InternationalJobTxn as InternationalJobTxnResource;
use App\Models\ApplicationPhaseMst;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\InternationalJobSavedAppliedTxn;
use App\Models\InternationalJobTxn;
use App\Models\MailTxn;
use App\Models\PaymentConfig;
use App\Models\ServicesMst;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class InternationalJobController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		ini_set('memory_limit', '-1');
		$limit = 10;
		$offset = empty($request->off_set) ? 0 : (int) (($request->off_set - 1) * $limit);
		$employeer_Int_jobs = auth('api')->user()->emplyerInternationalJobs()->where(function ($q) use ($request) {
			if ($request->has('job_type')) {
				$q->where('job_type', (int) $request->job_type);
			}
		});
		$jobs_count = $employeer_Int_jobs->count();
		$empJobs = InternationalJobTxnResource::collection($employeer_Int_jobs->orderBy('updated_at', 'desc')->offset($offset)->limit($limit)->get());
		return response()->json(['status' => 200, 'data' => $empJobs, 'tot_jobs' => $jobs_count]);
	}

	public function checkSameInternationalJobExist(Request $request) {
		$jb_trans = InternationalJobTxn::where('employer_id', auth('api')->user()->_id)->where('job_title', $request->input('job_title'))->whereIn('loc_cities', $request->input('city_id'))->first();
		if ($jb_trans) {
			return response()->json(['status' => 200, 'is_exist' => 1]);
		} else {
			return response()->json(['status' => 200, 'is_exist' => 0]);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//return $request->all();
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'job_title' => 'required',
			'min_experiance' => 'required',
			'max_experiance' => 'required',
			'employment_type' => 'required',
			'number_of_vacancies' => 'required',
			'min_monthly_salary' => 'required',
			'max_monthly_salary' => 'required',
			//'job_ageing' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {
			$allRequest = $request->except(['industry_id', 'language_pref', 'department_id', 'countries', 'cities', 'candidate_type', 'min_qualification']);
			$bsb_jb_id = InternationalJobTxn::count();
			$jobPost = InternationalJobTxn::create($allRequest + ['employer_id' => auth('api')->user()->_id, 'qualifications' => $request->min_qualification, 'bsb_jb_id' => ($bsb_jb_id + 1), 'job_ageing' => Carbon::now()->addDays(30)->format('Y-m-d'), 'candidate_type' => (int) $request->candidate_type]);
			$jobPost->industry()->attach($request->industry_id, ['type' => 'job']);
			$jobPost->language()->attach($request->language_pref, ['type' => 'job']);
			//$jobPost->department()->attach($request->department_id, ['type' => 'job']);
			$jobPost->country()->attach($request->countries);
			$jobPost->city()->attach($request->cities);
			$jobPost->locationCountry()->attach($request->loc_countries);
			$jobPost->locationCity()->attach($request->loc_cities);

			/* Employee job Alert */
			if ($request->job_status == 1) {
				$jobPost->published_date = Carbon::now()->format('Y-m-d');
				$jobPost->save();
				/*$emp_for_jb = User::where(function ($q) use ($request) {
						$q->whereHas('roles', function ($q) {
							$q->where('slug', 'employee');
						});
						$q->whereHas('empMyAlerts', function ($q) use ($request) {
							$q->whereIn('keyword', $request->required_skill);
							$q->where('salary_type', $request->salary_type);
							$q->whereHas('country', function ($q) use ($request) {
								if (is_array($request->countries)) {
									$q->whereIn('_id', $request->countries);
								} else {
									$q->where('_id', $request->countries);
								}
							});
							$q->whereHas('city', function ($q) use ($request) {
								$q->whereIn('_id', $request->cities);
							});
							$q->whereHas('department', function ($q) use ($request) {
								$q->whereIn('_id', $request->department_id);
							});
							$q->whereHas('industry', function ($q) use ($request) {
								$q->whereIn('_id', $request->industry_id);
							});
						});
					});
					foreach ($emp_for_jb->get() as $employee) {
						EmployeeJobAlertTxn::create(['emp_id' => $employee->_id, 'job_id' => $jobPost->_id, 'alert_type' => 'my-alert', 'created_by' => auth('api')->user()->_id]);
						$data = ['text' => 'New Job Alert'];
						$view = 'emails.job-alert';
						$subject = 'New Job Alert';
					}
				*/
				/* End Employee job alert*/

				/* Recommend job alert start */
				/*$employees = User::whereNotIn('_id', $emp_for_jb->pluck('_id')->toArray())->where(function ($q) use ($request) {
						$q->whereHas('roles', function ($q) {
							$q->where('slug', 'employee');
						});
						foreach ($request->required_skill as $value) {
							$q->whereHas('employeeSkill', function ($q) use ($value) {
								$q->where('title', 'like', '%' . $value . '%');
							});
						}

					})->get();

					foreach ($employees as $employee) {
						EmployeeJobAlertTxn::create(['emp_id' => $employee->_id, 'job_id' => $jobPost->_id, 'alert_type' => 'recommend', 'created_by' => auth('api')->user()->_id]);
						$data = ['text' => 'New Job Alert'];
						$view = 'emails.job-alert';
						$subject = 'New Job Alert';
				*/
				/* End Recommend job alert */
			}

			//update remaining job count premium jobpost service
			if ($request->job_type == 2) {
				$payment_config = PaymentConfig::first();
				if ($payment_config) {
					if ($payment_config->premium_job_post == 1) {
						$service_ids = ServicesMst::where('service_type', 'premium_job_post')->pluck('_id');
						$current_date = Carbon::now();
						$active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0)->first();
						$remaining_count = $active_service->remaining_count;
						$active_service->remaining_count = $remaining_count - 1;
						$active_service->save();
					}
				}

			}

			return response()->json(['status' => 200, 'data' => new InternationalJobTxnResource($jobPost)]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$empJB_txn = InternationalJobTxn::find($id);
		//echo "<pre>";print_r($empJB_txn);die;
		return response()->json(['status' => 200, 'data' => new InternationalJobTxnResource($empJB_txn)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'job_title' => 'required',
			//'job_desc' => 'required',
			//'job_role_id' => 'required',
			//'industry_id' => 'required',
			//'benefits' => 'required',
			'min_experiance' => 'required',
			'max_experiance' => 'required',
			'employment_type' => 'required',
			'number_of_vacancies' => 'required',
			'min_monthly_salary' => 'required',
			'max_monthly_salary' => 'required',
			//'job_ageing' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {
			$allRequest = $request->except(['industry_id', 'candidate_type', 'min_qualification']);
			$job = InternationalJobTxn::find($id);
			$job->fill($allRequest + ['candidate_type' => (int) $request->candidate_type, 'qualifications' => $request->min_qualification]);
			$job->save();
			$job->industry()->detach();
			$job->language()->detach();
			$job->country()->detach();
			$job->city()->detach();
			$job->locationCountry()->detach();
			$job->locationCity()->detach();
			$job->industry()->attach($request->industry_id, ['type' => 'job']);
			$job->language()->attach($request->language_pref, ['type' => 'job']);
			$job->country()->attach($request->countries);
			$job->city()->attach($request->cities);
			$job->locationCountry()->attach($request->loc_countries);
			$job->locationCity()->attach($request->loc_cities);
			return response()->json(['status' => 200, 'data' => new InternationalJobTxnResource($job)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'data' => $e->getMessage()]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$jobs = InternationalJobTxn::find($id);
			$jobs->delete();
			return response()->json(['status' => 200, 'status_text' => 'successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function jobStatus(Request $request, $id) {
		try {
			$jobs = InternationalJobTxn::find($id);
			$jobs->job_status = (int) $request->input('status');
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully change status']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function appliedJob(Request $request) {
		$user = auth('api')->user();
		try {
			$isApplied = InternationalJobSavedAppliedTxn::where('international_job_id', $request->job_id)->where('employee_id', $user->id)->whereNotNull('applied_date')->first();
			$applicationPhase = ApplicationPhaseMst::where('phase_name', 'Applied')->first();
			if ($isApplied) {
				return response()->json(['status' => 422, 'status_text' => 'You already have applied this job']);
			} else {
				$userCountry = $user->employeeUser->country_id;
				$int_jobs = InternationalJobTxn::find($request->job_id);
				$jobUserType = '';
				if (in_array($userCountry, $int_jobs->loc_countries)) {
					$jobUserType = '1';
				} else {
					$jobUserType = '2';
				}
				$appId = InternationalJobSavedAppliedTxn::count();
				$txn = InternationalJobSavedAppliedTxn::create(['international_job_id' => $request->job_id, 'applied_date' => Carbon::now()->format('Y-m-d'), 'applied_status' => $applicationPhase->_id, 'bsb_app_id' => ($appId + 1), 'employee_id' => $user->_id, 'created_by' => $user->_id, 'apply_user_type' => $jobUserType]);
				if ($request->job_questions) {
					$txn->job_questions = $request->job_questions;
					$txn->save();
				}
				$mailbody = 'Your job ' . $int_jobs->bsb_jb_trans_id . ' posting has received a new application. Please check application receive.';
				$template = MailTxn::create([
					'mail_receive_id' => $int_jobs->employer_id,
					'mail_sender_id' => $user->_id,
					'mail_subject' => $int_jobs->job_title,
					'mail_body' => $mailbody,
					'mail_status' => 1,
					'parent_id' => $request->reply_id ? $request->reply_id : '',
				]);
				if ($request->job_questions) {
					$template->job_questions = $request->job_questions;
					$template->save();
				}
				$attFile = [];
				if ($user->employeeUser->profile_picture) {
					$attFile[] = '/upload_files/profile_picture/' . $user->employeeUser->profile_picture;
				}
				if ($user->employeeCvTxn->cv_path) {
					$attFile[] = '/upload_files/employee-resumes/' . $user->employeeCvTxn->cv_path;
				}
				$template->attached_file = $attFile;

				if ($user->employeeVideos->count() > 0) {
					$template->video_links = $user->employeeVideos->pluck('video_link')->toArray();
				}
				$template->save();
				return response()->json(['status' => 200, 'status_text' => 'Successfully applied job']);
			}
			//$empJob->jobViews()->detach($user->_id);
			//$empJob->jobViews()->attach($user->_id);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function savedJob(Request $request) {
		$user = auth('api')->user();
		try {
			$isSaved = InternationalJobSavedAppliedTxn::where('international_job_id', $request->job_id)->where('employee_id', $user->id)->whereNotNull('saved_on')->first();
			if ($isSaved) {
				return response()->json(['status' => 422, 'status_text' => 'You already have saved this job']);
			} else {
				/*$empJob = EmployerJobTxn::find($request->job_id);
					$empJob->jobViews()->detach($user->_id);
				*/
				$userCountry = $user->employeeUser->country_id;
				$int_jobs = InternationalJobTxn::find($request->job_id);
				$jobUserType = '';
				if (in_array($userCountry, $int_jobs->loc_countries)) {
					$jobUserType = '1';
				} else {
					$jobUserType = '2';
				}
				InternationalJobSavedAppliedTxn::create(['international_job_id' => $request->job_id, 'saved_on' => Carbon::now()->format('Y-m-d'), 'employee_id' => $user->_id, 'created_by' => $user->_id, 'apply_user_type' => $jobUserType]);
				return response()->json(['status' => 200, 'status_text' => 'Successfully saved job']);
			}

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function getAppliedJob(Request $request) {
		$limit = 6;
		$skip = $request->skip ? (int) $request->skip : 0;
		$user = auth('api')->user();
		$emp_appliedjobs = InternationalJobSavedAppliedTxn::where('employee_id', $user->_id)->orderBy('updated_at', 'desc');
		$applied_jobs = InternationalJobSavedAppliedResource::collection($emp_appliedjobs->skip($skip)->take($limit)->get());
		$application_phases = ApplicationPhaseMst::all();
		$total_count = $emp_appliedjobs->count();
		return response()->json(['status' => 200, 'data' => compact('applied_jobs', 'application_phases', 'total_count')]);

	}

	public function getSavedJob(Request $request) {
		$limit = 6;
		$skip = $request->skip ? (int) $request->skip : 0;
		$user = auth('api')->user();
		$emp_savedjobs = InternationalJobSavedAppliedTxn::where('employee_id', $user->id)->whereNull('applied_date')->orderBy('updated_at', 'desc');
		$total_count = $emp_savedjobs->count();
		$saved_jobs = InternationalJobSavedAppliedResource::collection($emp_savedjobs->skip($skip)->take($limit)->get());
		return response()->json(['status' => 200, 'data' => compact('saved_jobs', 'total_count')]);
	}

	public function getJobById($id) {
		$user = auth('api')->user();
		$job = InternationalJobTxn::find($id);
		if ($user) {
			$job->jobViews()->detach($user->_id);
			$job->jobViews()->attach($user->_id);
		}

		return response()->json(['status' => 200, 'job' => new InternationalJobTxnResource($job)]);
	}

	public function appliedSavedJob($id) {
		$user = auth('api')->user();
		$applicationPhase = ApplicationPhaseMst::where('phase_name', 'Applied')->first();
		$job = InternationalJobSavedAppliedTxn::find($id);
		$job->applied_date = Carbon::now()->format('Y-m-d');
		$job->applied_status = $applicationPhase->_id;
		$job->save();
		return response()->json(['status' => 200]);

	}

	public function removeSavedJob($id) {
		$user = auth('api')->user();
		$job = InternationalJobSavedAppliedTxn::find($id);
		$job->delete();
		return response()->json(['status' => 200]);
	}

	public function getApplicationReceivedJob(Request $request) {

	}

	public function jobPublished(Request $request, $id) {
		try {
			$jobs = InternationalJobTxn::find($id);
			$jobs->is_published = 1;
			$jobs->published_date = Carbon::now()->format('Y-m-d');
			$jobs->job_ageing = Carbon::now()->addMonths(6)->format('Y-m-d');
			$jobs->job_status = 1;
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Publised Job', 'data' => new InternationalJobTxnResource($jobs)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
	public function jobRePublished(Request $request, $id) {
		try {
			$jobs = InternationalJobTxn::find($id);
			$jobs->is_republished = 0;
			$jobs->republished_date = Carbon::now()->format('Y-m-d');
			$jobs->job_ageing = Carbon::now()->addMonths(6)->format('Y-m-d');
			$jobs->job_status = 1;
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Republised Job', 'data' => new InternationalJobTxnResource($jobs)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
}
