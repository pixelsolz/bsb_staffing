<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeerJobs as EmployerJobsResource;
use App\Http\Resources\EmployerJobsSummary as EmployerJobsSummaryResource;
use App\Http\Resources\InternationalJobsSummary as InternationalJobsSummaryResource;
use App\Http\Resources\WalkingJobsSummary as WalkingJobsSummaryResource;
use App\Mail\SendMail;
use App\Models\EmployeeJobAlertTxn;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\EmployerJobTxn;
use App\Models\ExperienceMst;
use App\Models\InternationalJobTxn;
use App\Models\JobTitleMst;
use App\Models\MyAlertTxn;
use App\Models\PaymentConfig;
use App\Models\ServicesMst;
use App\Models\User;
use App\Models\WalkInInterviewTxn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Validator;

class JobManageController extends Controller {

	public function __construct() {
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 10);
		$employeer_jobs = auth('api')->user()->employerJobs()->where(function ($q) use ($request) {
			if ($request->has('job_type')) {
				$q->where('job_type', (int) $request->job_type);
			}
		});
		$jobs_count = $employeer_jobs->count();
		$empJobs = EmployerJobsResource::collection($employeer_jobs->orderBy('updated_at', 'desc')->offset($offset)->limit(20)->get());
		return response()->json(['status' => 200, 'data' => $empJobs, 'tot_jobs' => $jobs_count]);

	}

	public function checkSameJobExist(Request $request) {
		$jb_trans = EmployerJobTxn::where('employer_id', auth('api')->user()->_id)->where('job_title', $request->input('job_title'))->where('city_id', $request->input('city_id'))->first();
		if ($jb_trans) {
			return response()->json(['status' => 200, 'is_exist' => 1]);
		} else {
			return response()->json(['status' => 200, 'is_exist' => 0]);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//return Carbon::now()->addDays(30)->format('Y-m-d');
		//return $request->all();die();
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'job_title' => 'required',
			//'job_desc' => 'required',
			//'job_role_id' => 'required',
			//'industry_id' => 'required',
			//'benefits' => 'required',
			'min_experiance' => 'required',
			'max_experiance' => 'required',
			'employment_type' => 'required',
			'number_of_vacancies' => 'required',
			'min_monthly_salary' => 'required',
			'max_monthly_salary' => 'required',
			//'job_ageing' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$allRequest = $request->except(['industry_id', 'language_pref', 'department_id', 'candidate_type', 'min_qualification']);
			$bsb_jb_id = EmployerJobTxn::count();
			$jobPost = EmployerJobTxn::create($allRequest + ['employer_id' => auth('api')->user()->_id, 'qualifications' => $request->min_qualification, 'bsb_jb_id' => ($bsb_jb_id + 1), 'candidate_type' => (int) $request->candidate_type, 'job_location' => ['type' => 'Point', 'coordinates' => [$request->job_lat, $request->job_long]]]);
			$jobPost->industry()->attach($request->industry_id, ['type' => 'job']);
			$jobPost->language()->attach($request->language_pref, ['type' => 'job']);

			//$jobPost->department()->attach($request->department_id, ['type' => 'job']);

			/* Employee job Alert */
			if ($request->job_status == 1) {

				$jobPost->job_ageing = Carbon::now()->addMonths(6)->format('Y-m-d');
				$jobPost->published_date = Carbon::now()->format('Y-m-d');
				$jobPost->save();
				$min_exp = ExperienceMst::where('_id', $request->min_experiance)->first();
				$emp_for_jb = User::where(function ($q) use ($request, $min_exp) {
					$q->whereHas('roles', function ($q) {
						$q->where('slug', 'employee');
					});
					$q->whereHas('employeeUser', function ($q) use ($request, $min_exp) {
						if ((int) $request->candidate_type == 2) {
							$q->where('gender', '1');
						} else if ((int) $request->candidate_type == 3) {
							$q->where('gender', '2');
						}
						if ($min_exp) {
							$q->where('total_experience_yr_value', '<=', $min_exp->value);
						}

					});
					$q->whereHas('empMyAlerts', function ($q) use ($request) {
						$q->whereIn('keyword', $request->required_skill);
						$q->where('salary_type', $request->salary_type);
						//$q->where('salary', '<=', $request->min_monthly_salary);
						//$q->where('salary', '>=', $request->max_monthly_salary);
						/*$q->whereBetween('salary', [$request->min_monthly_salary, $request->max_monthly_salary]);*/
						$q->whereHas('country', function ($q) use ($request) {
							$q->where('_id', $request->country_id);
						});
						$q->whereHas('city', function ($q) use ($request) {
							$q->where('_id', $request->city_id);
						});
						/*$q->whereHas('department', function ($q) use ($request) {
							$q->whereIn('_id', $request->department_id);
						});*/
						$q->whereHas('industry', function ($q) use ($request) {
							$q->whereIn('_id', $request->industry_id);
						});
					});
				});

				/* End Employee job alert*/

				/* Recommend job alert start */
				$employees = User::whereNotIn('_id', $emp_for_jb->pluck('_id')->toArray())->where(function ($q) use ($request) {
					$q->whereHas('roles', function ($q) {
						$q->where('slug', 'employee');
					});
					foreach ($request->required_skill as $value) {

						$q->whereHas('employeeSkill', function ($q) use ($value) {
							$q->where('title', 'like', '%' . $value . '%');
						});
						return $q;
					}

				})->get();

				$this->sendRecomendedAlertMail($employees, $jobPost);
				$this->jobAlertMail($emp_for_jb->get(), $jobPost);

			}
			/* End Recommend job alert */
			//update remaining job count premium jobpost service
			if ($request->job_type == 2) {
				$payment_config = PaymentConfig::first();
				if ($payment_config) {
					if ($payment_config->premium_job_post == 1) {
						$service_ids = ServicesMst::where('service_type', 'premium_job_post')->pluck('_id');
						$current_date = Carbon::now();
						$active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0)->first();
						$remaining_count = $active_service->remaining_count;
						$active_service->remaining_count = $remaining_count - 1;
						$active_service->save();
					}
				}

			}

			return response()->json(['status' => 200, 'data' => new EmployerJobsResource($jobPost)]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function sendRecomendedAlertMail($employees, $jobPost) {

		if ($employees) {
			foreach ($employees as $employee) {
				EmployeeJobAlertTxn::create(['emp_id' => $employee->_id, 'job_id' => $jobPost->_id, 'alert_type' => 'recommend', 'created_by' => auth('api')->user()->_id]);
				$name = $employee->employeeUser->first_name;
				$data = ['text' => 'New Job Alert', 'job_id' => $jobPost->_id, 'user_name' => $name, 'alert_type' => 'recommend'];
				$view = 'emails.job-alert';
				$subject = 'Please explore these recommended jobs for you in bsbstaffing.com!';

				Mail::to($employee->email)->send(new SendMail($data, $view, $subject));
			}
		}
	}

	public function jobAlertMail($employees, $jobPost) {
		if ($employees) {
			foreach ($employees as $employee) {
				EmployeeJobAlertTxn::create(['emp_id' => $employee->_id, 'job_id' => $jobPost->_id, 'alert_type' => 'my-alert', 'created_by' => auth('api')->user()->_id]);
				$name = $employee->employeeUser->first_name;
				$data = ['text' => 'New Job Alert', 'job_id' => $jobPost->_id, 'user_name' => $name, 'alert_type' => 'my-alert'];
				$view = 'emails.job-alert';
				$subject = 'Check out this new job posting matching your profile in bsbstaffing.com!';

				Mail::to($employee->email)->send(new SendMail($data, $view, $subject));
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$empJB_txn = EmployerJobTxn::find($id);
		//echo "<pre>";print_r($empJB_txn);die;
		return response()->json(['status' => 200, 'data' => new EmployerJobsResource($empJB_txn)]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'job_title' => 'required',
			//'job_desc' => 'required',
			//'job_role_id' => 'required',
			//'industry_id' => 'required',
			//'benefits' => 'required',
			'min_experiance' => 'required',
			'max_experiance' => 'required',
			'employment_type' => 'required',
			'number_of_vacancies' => 'required',
			'min_monthly_salary' => 'required',
			'max_monthly_salary' => 'required',
			//'job_ageing' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$allRequest = $request->except(['industry_id', 'candidate_type', 'min_qualification']);
			$job = EmployerJobTxn::find($id);
			$job->fill($allRequest + ['candidate_type' => (int) $request->candidate_type, 'qualifications' => $request->min_qualification, 'job_location' => ['type' => 'Point', 'coordinates' => [$request->job_lat, $request->job_long]]]);
			$job->save();
			$job->industry()->detach();
			$job->language()->detach();
			$job->industry()->attach($request->industry_id, ['type' => 'job']);
			$job->language()->attach($request->language_pref, ['type' => 'job']);
			return response()->json(['status' => 200, 'data' => new EmployerJobsResource($job)]);
		} catch (\Exception $e) {

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$jobs = EmployerJobTxn::find($id);
			$jobs->delete();
			return response()->json(['status' => 200, 'status_text' => 'successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function jobStatus(Request $request, $id) {
		try {
			$jobs = EmployerJobTxn::find($id);
			$jobs->job_status = (int) $request->input('status');
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully change status']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function getDistance($lat1, $lon1, $lat2, $lon2, $range) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		if (round($miles) <= (int) $range) {
			return true;
		} else {
			return false;
		}
		//return $miles;

	}

	public function getSingleJobArray(Request $request, $id) {
		$job_data = EmployerJobTxn::where('_id', $id)->get();
		return response()->json(['status' => 200, 'data' => EmployerJobsSummaryResource::collection($job_data)]);
	}

	public function getSingleJobArrayByParam($id, $type) {
		if ($type == 'job-post') {
			$job_data = EmployerJobTxn::where('_id', $id)->get();
			return response()->json(['status' => 200, 'data' => EmployerJobsSummaryResource::collection($job_data)]);
		} elseif ($type == 'international-walking') {
			$job_data = WalkInInterviewTxn::where('_id', $id)->get();
			return response()->json(['status' => 200, 'data' => WalkingJobsSummaryResource::collection($job_data)]);
		} elseif ($type == 'local-walking') {
			$job_data = WalkInInterviewTxn::where('_id', $id)->get();
			return response()->json(['status' => 200, 'data' => WalkingJobsSummaryResource::collection($job_data)]);
		} elseif ($type == 'international-job') {
			$job_data = InternationalJobTxn::where('_id', $id)->get();
			return response()->json(['status' => 200, 'data' => InternationalJobsSummaryResource::collection($job_data)]);
		}
	}

	public function getJobBySearch(Request $request) {
		ini_set('memory_limit', '-1');
		$limit = empty($request->limit) ? 12 : (int) $request->limit;
		//return $request->all();
		$employerJobTxn = new EmployerJobTxn();
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$employerJobTxn = $employerJobTxn->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
		}

		$employeer_jobs = $employerJobTxn->where(function ($q) use ($request) {

			if (!empty($request->search_job_title)) {

				foreach (explode(',', $request->search_job_title) as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
				/*$q->whereIn('job_title', explode(',', $request->search_job_title));
				$q->orWhereIn('required_skill', explode(',', $request->search_job_title));*/

			}

			if (!empty($request->user_country)) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->where('name', $request->user_country);
				});
			}
			/*if (!empty($request->job_range)) {
				$distance = (int) $request->job_range * 1609.344;
				$q->where('job_location', 'near', [
					'$geometry' => [
						'type' => 'Point',
						'coordinates' => [
							(float) $request->user_lat, (float) $request->user_long,
						],
					],
					'$maxDistance' => $distance,
				]);
			}*/

			if (!empty($request->search_location)) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
			}
			if (!empty($request->search_industry)) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_industry));
				});
			}
			if (!empty($request->search_department)) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_department));
				});
			}
			if (!empty($request->search_employer_jobs)) {
				$q->where('employer_id', $request->search_employer_jobs);
			}
		})->where('job_status', 1)->where('is_published', 1)->orderBy('job_type', 'desc')->orderBy('updated_at', 'desc');

		$internationalJobTxn = new InternationalJobTxn();
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$internationalJobTxn = $internationalJobTxn->whereDoesntHave('jobSavedApplied', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
		}

		$internationalJobs = $internationalJobTxn->where(function ($q) use ($request) {
			if (!empty($request->user_country)) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->where('name', $request->user_country);
				});
			}

			/*if (!empty($request->job_range)) {
				$distance = (int) $request->range * 1609.344;
				$q->where('job_location', 'near', [
					'$geometry' => [
						'type' => 'Point',
						'coordinates' => [
							(float) $request->user_lat, (float) $request->user_long,
						],
					],
					'$maxDistance' => $distance,
				]);
			}*/

			if (!empty($request->search_location)) {
				$q->whereHas('locationCountry', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
				$q->orWhereHas('locationCity', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
			}
			if (!empty($request->search_job_title)) {

				foreach (explode(',', $request->search_job_title) as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}

			}
			if (!empty($request->search_industry)) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_industry));
				});
			}
			if (!empty($request->search_department)) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_department));
				});
			}
			if (!empty($request->search_employer_jobs)) {
				$q->where('employer_id', $request->search_employer_jobs);
			}

		})->where('job_status', 1)->orderBy('updated_at', 'desc'); //->orderBy('job_type', 'desc');

		$walkJobTxn = new WalkInInterviewTxn();
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$walkJobTxn = $walkJobTxn->whereDoesntHave('interviewSlotBooked', function ($q) use ($user) {
				$q->where('user_id', $user->_id);
			});
		}

		$walkingInterviews = $walkJobTxn->where(function ($q) use ($request) {
			if (!empty($request->search_job_title != '')) {
				foreach (explode(',', $request->search_job_title) as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
				/*$q->whereIn('job_title', explode(',', $request->search_job_title));
				$q->orWhereIn('required_skill', explode(',', $request->search_job_title));*/
			}

			if (!empty($request->job_range)) {
				$distance = (int) $request->job_range * 1609.344;
				/*$q->where('job_location', 'near', [
					'$geometry' => [
						'type' => 'Point',
						'coordinates' => [
							(float) $request->user_lat, (float) $request->user_long,
						],
					],
					'$maxDistance' => $distance,
				]);*/
			}
			if (!empty($request->user_country)) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->where('name', $request->user_country);
				});
			}

			if (!empty($request->search_location != '')) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
			}
			if (!empty($request->search_industry != '')) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_industry));
				});
			}
			if (!empty($request->search_department)) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_department));
				});
			}
			if (!empty($request->search_employer_jobs)) {
				$q->where('employer_id', $request->search_employer_jobs);
			}
		})->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->orderBy('created_at', 'desc');

		$interwalkingCount = collect($walkingInterviews->get())->where('walkin_location_type', 2)->count();
		$localWalkingCount = collect($walkingInterviews->get())->where('walkin_location_type', 1)->count();

		/* FOR RANGE SEARCH */
		if (!empty($request->range)) {
			$distance = (int) $request->range * 1609.344;
			$user = auth('api')->user();
			$candType = $user->employeeUser->gender == 1 ? 2 : 3;
			$userIndustry = ($user->employeePrefTxn && $user->employeePrefTxn->industries) ? $user->employeePrefTxn->industries()->pluck('_id')->toArray() : ($user->employeeUser->industry_id ? [$user->employeeUser->industry_id] : []);
			$userExpValue = $user->employeeUser->total_experience_yr_value;

			$empJobsData = $employeer_jobs->where('job_location', 'near', [
				'$geometry' => [
					'type' => 'Point',
					'coordinates' => [
						(float) $request->user_lat, (float) $request->user_long,
					],
				],
				'$maxDistance' => $distance,
			])->whereIn('industry_id', $userIndustry)->whereHas('minExperiance', function ($q) use ($userExpValue) {
				if ($userExpValue) {
					$q->where('value', '<=', $userExpValue);
				}
			})->where(function ($q) use ($candType) {
				$q->where('candidate_type', $candType);
				$q->orWhere('candidate_type', 1);
			});

			$employeerJobs = EmployerJobsSummaryResource::collection($empJobsData->offset((int) $request->skip)->limit(12)->get());
			return response()->json(['status' => 200, 'jobResults' => $employeerJobs]);

		}

		/* END RANGE SEARCH */

		if (!empty($request->job_type) && $request->job_type == 'job_post') {
			return response()->json(['status' => 200, 'jobResults' => !empty($request->skip) ? EmployerJobsSummaryResource::collection($employeer_jobs->offset((int) $request->skip)->limit(12)->get()) : EmployerJobsSummaryResource::collection($employeer_jobs->get())]);
		} elseif (!empty($request->job_type) && $request->job_type == 'international_job') {
			return response()->json(['status' => 200, 'international_jobs' => !empty($request->skip) ? InternationalJobsSummaryResource::collection($internationalJobs->offset((int) $request->skip)->limit(12)->get()) : InternationalJobsSummaryResource::collection($internationalJobs->get())]);
		} elseif (!empty($request->job_type) && $request->job_type == 'local_walking') {
			return response()->json(['status' => 200, 'walking_interviews' => !empty($request->skip) ? WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 1)->offset((int) $request->skip)->limit(12)->get()) : WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 1)->get())]);
		} elseif (!empty($request->job_type) && $request->job_type == 'international_walking') {
			return response()->json(['status' => 200, 'walking_interviews' => !empty($request->skip) ? WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 2)->offset((int) $request->skip)->limit(12)->get()) : WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 2)->get())]);
		} else {
			return response()->json(['status' => 200, 'jobResults' => EmployerJobsSummaryResource::collection($employeer_jobs->offset((int) $request->skip)->limit(12)->get()), 'jobResults_count' => $employeer_jobs->count(), 'international_jobs' => InternationalJobsSummaryResource::collection($internationalJobs->offset((int) $request->skip)->limit(12)->get()), 'international_jobs_count' => $internationalJobs->count(), 'walking_interviews' => WalkingJobsSummaryResource::collection($walkingInterviews->offset((int) $request->skip)->limit(12)->get()), 'inter_walking_count' => $interwalkingCount, 'local_walking_count' => $localWalkingCount, 'job_count' => ($employeer_jobs->count() + $internationalJobs->count() + $walkingInterviews->count())]);
			/*return response()->json(['status' => 200, 'jobResults' => EmployerJobsSummaryResource::collection($employeer_jobs->offset(0)->limit($limit)->get()), 'jobResults_count' => $employeer_jobs->count(), 'international_jobs' => InternationalJobsSummaryResource::collection($internationalJobs->offset(0)->limit($limit)->get()), 'international_jobs_count' => $internationalJobs->count(), 'walking_interviews' => WalkingJobsSummaryResource::collection($walkingInterviews->offset(0)->limit($limit)->get()), 'inter_walking_count' => $interwalkingCount, 'local_walking_count' => $localWalkingCount, 'job_count' => ($employeer_jobs->count() + $internationalJobs->count() + $walkingInterviews->count())]);*/
		}

	}

	public function getJobByAdvSearch(Request $request) {
		$limit = empty($request->limit) ? 12 : $request->limit;
		$employerJobTxn = new EmployerJobTxn();
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$employerJobTxn = $employerJobTxn->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
		}

		$employeer_jobs = $employerJobTxn->where(function ($q) use ($request) {
			if (!empty($request->formData['job_title'])) {
				foreach ($request->formData['job_title'] as $value) {
					if (!empty($value)) {
						$q->orWhere('job_title', 'like', '%' . substr($value, 0, 3) . '%');
						$q->orWhere('required_skill', 'like', '%' . substr($value, 0, 3) . '%');
					}
				}
			}
			if (!empty($request->formData['employement_for'])) {
				$q->where('employement_for', $request->formData['employement_for']);
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('city', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}

			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}

			if (!empty($request->formData['min_experiance'])) {
				$q->where('min_experiance', $request->formData['min_experiance']);
			}

			if (!empty($request->formData['max_experiance'])) {
				$q->where('max_experiance', $request->formData['max_experiance']);
			}

			if ($request->formData['salary_type'] != '') {
				$q->where('salary_type', $request->formData['salary_type']);
			}

			if ($request->formData['currency'] != '') {
				$q->whereHas('currencydata', function ($q) use ($request) {
					$q->where('_id', $request->formData['currency']);
				});
			}
			if ($request->formData['min_monthly_salary'] != '' && $request->formData['max_monthly_salary'] != '') {
				$q->whereBetween('min_monthly_salary', [$request->formData['min_monthly_salary'], $request->formData['max_monthly_salary']]);
			} elseif ($request->formData['min_monthly_salary'] != '' && $request->formData['max_monthly_salary'] == '') {
				$q->where('min_monthly_salary', $request->formData['min_monthly_salary']);
			} elseif ($request->formData['max_monthly_salary'] != '' && $request->formData['min_monthly_salary'] == '') {
				$q->where('max_monthly_salary', $request->formData['max_monthly_salary']);
			}

		})->where('job_status', 1)->where('is_published', 1)->orderBy('created_at', 'desc');

		$internationalJobTxn = new InternationalJobTxn();
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$internationalJobTxn = $internationalJobTxn->whereDoesntHave('jobSavedApplied', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
		}
		$internationalJobs = $internationalJobTxn->where(function ($q) use ($request) {

			if (!empty($request->formData['job_title'])) {
				foreach ($request->formData['job_title'] as $value) {
					if (!empty($value)) {
						$q->orWhere('job_title', 'like', '%' . substr($value, 0, 3) . '%');
						$q->orWhere('required_skill', 'like', '%' . substr($value, 0, 3) . '%');
					}
				}
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereHas('locationCountry', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('locationCity', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}

			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}

			if (!empty($request->formData['min_experiance'])) {
				$q->where('min_experiance', $request->formData['min_experiance']);
			}

			if (!empty($request->formData['max_experiance'])) {
				$q->where('max_experiance', $request->formData['max_experiance']);
			}

			if ($request->formData['salary_type'] != '') {
				$q->where('salary_type', $request->formData['salary_type']);
			}

			if ($request->formData['currency'] != '') {
				$q->whereHas('currencydata', function ($q) use ($request) {
					$q->where('_id', $request->formData['currency']);
				});
			}
			if ($request->formData['min_monthly_salary'] != '' && $request->formData['max_monthly_salary'] != '') {
				$q->whereBetween('min_monthly_salary', [$request->formData['min_monthly_salary'], $request->formData['max_monthly_salary']]);
			} elseif ($request->formData['min_monthly_salary'] != '' && $request->formData['max_monthly_salary'] == '') {
				$q->where('min_monthly_salary', $request->formData['min_monthly_salary']);
			} elseif ($request->formData['max_monthly_salary'] != '' && $request->formData['min_monthly_salary'] == '') {
				$q->where('max_monthly_salary', $request->formData['max_monthly_salary']);
			}

			if (!empty($request->search_location)) {
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_location));
				});
			}

			if (!empty($request->search_industry)) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_industry));
				});
			}
			if (!empty($request->search_department)) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', explode(',', $request->search_department));
				});
			}

		})->where('job_status', 1)->orderBy('updated_at', 'desc'); //->orderBy('job_type', 'desc');

		$walkJobTxn = new WalkInInterviewTxn();
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$walkJobTxn = $walkJobTxn->whereDoesntHave('interviewSlotBooked', function ($q) use ($user) {
				$q->where('user_id', $user->_id);
			});
		}

		$walkingInterviews = $walkJobTxn->where(function ($q) use ($request) {

			if (!empty($request->formData['job_title'])) {
				foreach ($request->formData['job_title'] as $value) {
					if (!empty($value)) {
						$q->orWhere('job_title', 'like', '%' . substr($value, 0, 3) . '%');
						$q->orWhere('required_skill', 'like', '%' . substr($value, 0, 3) . '%');
					}
				}
			}

			if (!empty($request->formData['country_id'])) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['country_id']);
				});
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereHas('city', function ($q) use ($request) {
					$q->whereIn('_id', $request->formData['city_id']);
				});
			}

			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}

			if (!empty($request->formData['min_experiance'])) {
				$q->where('min_experiance', $request->formData['min_experiance']);
			}

			if (!empty($request->formData['max_experiance'])) {
				$q->where('max_experiance', $request->formData['max_experiance']);
			}

			if ($request->formData['salary_type'] != '') {
				$q->where('salary_type', $request->formData['salary_type']);
			}

			if ($request->formData['currency'] != '') {
				$q->whereHas('currencydata', function ($q) use ($request) {
					$q->where('_id', $request->formData['currency']);
				});
			}
			if ($request->formData['min_monthly_salary'] != '' && $request->formData['max_monthly_salary'] != '') {
				$q->whereBetween('min_monthly_salary', [$request->formData['min_monthly_salary'], $request->formData['max_monthly_salary']]);
			} elseif ($request->formData['min_monthly_salary'] != '' && $request->formData['max_monthly_salary'] == '') {
				$q->where('min_monthly_salary', $request->formData['min_monthly_salary']);
			} elseif ($request->formData['max_monthly_salary'] != '' && $request->formData['min_monthly_salary'] == '') {
				$q->where('max_monthly_salary', $request->formData['max_monthly_salary']);
			}

		})->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->orderBy('created_at', 'desc');

		$interwalkingCount = collect($walkingInterviews->get())->where('walkin_location_type', 2)->count();
		$localWalkingCount = collect($walkingInterviews->get())->where('walkin_location_type', 1)->count();

		if (!empty($request->job_type) && $request->job_type == 'job_post') {
			return response()->json(['status' => 200, 'jobResults' => !empty($request->skip) ? EmployerJobsSummaryResource::collection($employeer_jobs->offset((int) $request->skip)->limit(12)->get()) : EmployerJobsSummaryResource::collection($employeer_jobs->get())]);
		} elseif (!empty($request->job_type) && $request->job_type == 'international_job') {
			return response()->json(['status' => 200, 'international_jobs' => !empty($request->skip) ? InternationalJobsSummaryResource::collection($internationalJobs->offset((int) $request->skip)->limit(12)->get()) : InternationalJobsSummaryResource::collection($internationalJobs->get())]);
		} elseif (!empty($request->job_type) && $request->job_type == 'local_walking') {
			return response()->json(['status' => 200, 'walking_interviews' => !empty($request->skip) ? WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 1)->offset((int) $request->skip)->limit(12)->get()) : WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 1)->get())]);
		} elseif (!empty($request->job_type) && $request->job_type == 'international_walking') {
			return response()->json(['status' => 200, 'walking_interviews' => !empty($request->skip) ? WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 2)->offset((int) $request->skip)->limit(12)->get()) : WalkingJobsSummaryResource::collection($walkingInterviews->where('walkin_location_type', 2)->get())]);
		} else {
			return response()->json(['status' => 200, 'jobResults' => EmployerJobsSummaryResource::collection($employeer_jobs->offset((int) $request->skip)->limit(12)->get()), 'jobResults_count' => $employeer_jobs->count(), 'international_jobs' => InternationalJobsSummaryResource::collection($internationalJobs->offset((int) $request->skip)->limit(12)->get()), 'international_jobs_count' => $internationalJobs->count(), 'walking_interviews' => WalkingJobsSummaryResource::collection($walkingInterviews->offset((int) $request->skip)->limit(12)->get()), 'inter_walking_count' => $interwalkingCount, 'local_walking_count' => $localWalkingCount, 'job_count' => ($employeer_jobs->count() + $internationalJobs->count() + $walkingInterviews->count())]);
		}

		/*return response()->json(['status' => 200, 'jobResults' => EmployerJobsSummaryResource::collection($employeer_jobs->offset(0)->limit($limit)->get()), 'jobResults_count' => $employeer_jobs->count(), 'international_jobs' => InternationalJobsSummaryResource::collection($internationalJobs->offset(0)->limit($limit)->get()), 'international_jobs_count' => $internationalJobs->count(), 'walking_interviews' => WalkingJobsSummaryResource::collection($walkingInterviews->offset(0)->limit($limit)->get()), 'inter_walking_count' => $interwalkingCount, 'local_walking_count' => $localWalkingCount, 'job_count' => ($employeer_jobs->count() + $internationalJobs->count())]);*/
	}

	public function getEmployeerPosetdJob() {
		$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 10);
		$employeer_jobs = auth('api')->user()->employerJobs()->where('job_status', 1)->where('created_at', '>', Carbon::now()->subDays(30));
		$jobs_count = $employeer_jobs->count();
		$empJobs = EmployerJobsResource::collection($employeer_jobs->offset($offset)->limit(10)->get());
		return response()->json(['status' => 200, 'data' => $empJobs, 'tot_jobs' => $jobs_count]);
	}

	public function createJobAlert(Request $request) {
		//return $request->job_location;
		MyAlertTxn::create(['email_id' => $request->email, 'job_title' => $request->job_title, 'location' => $request->job_location]);

		return response()->json(['status' => 200]);
	}

	public function getJobTitleSearch(Request $request) {
		$job_titles = JobTitleMst::where('job_title', 'like', $request->q . '%')->get();
		return response()->json(['status' => 200, 'data' => $job_titles]);
	}
	public function getJobByEmployeer($employeer_id) {
		$employeer_jobs = EmployerJobTxn::where('employer_id', $employeer_id)->where('job_status', 1)->orderBy('job_type', 'desc')->get();
		$base_url = url('/');
		return response()->json(['status' => 200, 'jobResults' => EmployerJobsResource::collection($employeer_jobs), 'base_url' => $base_url]);
	}

	public function jobPublished(Request $request, $id) {
		try {
			$jobs = EmployerJobTxn::find($id);
			$jobs->is_published = 1;
			$jobs->published_date = Carbon::now()->format('Y-m-d');
			$jobs->job_ageing = Carbon::now()->addMonths(6)->format('Y-m-d');
			$jobs->job_status = 1;
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Publised Job', 'data' => new EmployerJobsResource($jobs)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
	public function jobRePublished(Request $request, $id) {
		try {
			$jobs = EmployerJobTxn::find($id);
			$jobs->is_republished = 0;
			$jobs->republished_date = Carbon::now()->format('Y-m-d');
			$jobs->job_ageing = Carbon::now()->addMonths(6)->format('Y-m-d');
			$jobs->job_status = 1;
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Republised Job', 'data' => new EmployerJobsResource($jobs)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

}