<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AllOtherMaster as AllOtherMasterResource;
use App\Http\Resources\City as CityResource;
use App\Http\Resources\Country as CountryResource;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\EmployeerJobs as EmployeerJobsResource;
use App\Http\Resources\Industry as IndustryResource;
use App\Http\Resources\JobCityFooter as JobCityFooterResource;
use App\Http\Resources\JobCountryFooter as JobCountryFooterResource;
use App\Http\Resources\JobDepartmentFooter as JobDepartmentFooterResource;
use App\Http\Resources\JobIndustryFooter as JobIndustryFooterResource;
use App\Mail\SendMail;
use App\Models\AllOtherMasterMst;
use App\Models\ApplicationPhaseMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\DepartmentMst;
use App\Models\EmployeeAppliedJobsTxn;
use App\Models\EmployeeSavedJobsTxn;
use App\Models\EmployerJobTxn;
use App\Models\ExperienceMst;
use App\Models\IndustryMst;
use App\Models\JobRoleMst;
use App\Models\MailTxn;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

class JobSearchController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$search_cat = $request->input('cat');
		$result = '';
		$data = '';
		$limit = empty($request->limit) ? 100 : (int) $request->limit;
		switch ($search_cat) {
		case 'country':
			$result = CountryMst::where(function ($q) use ($request) {
				if (!empty($request->search)) {
					$q->where('name', 'like', $request->search . '%');
				}
			})->orderBy('name', 'asc');
			$alldata = !empty($request->searchById) ? $result->get() : $result->offset(0)->limit($limit)->get();
			$data = JobCountryFooterResource::collection($alldata);
			break;
		case 'city':
			$result = CityMst::where(function ($q) use ($request) {
				if (!empty($request->search)) {
					$q->where('name', 'like', $request->search . '%');
				}
			})->orderBy('name', 'asc');
			$alldata = !empty($request->searchById) ? $result->get() : $result->offset(0)->limit($limit)->get();
			$data = JobCityFooterResource::collection($alldata);
			break;
		case 'industry':
			$result = IndustryMst::where(function ($q) use ($request) {
				if (!empty($request->search)) {
					$q->where('name', 'like', $request->search . '%');
				}
			})->orderBy('name', 'asc');
			$alldata = !empty($request->searchById) ? $result->get() : $result->offset(0)->limit($limit)->get();
			$data = JobIndustryFooterResource::collection($alldata);
			break;
		case 'role':
			$result = JobRoleMst::where(function ($q) use ($request) {
				if (!empty($request->search)) {
					$q->where('name', 'like', $request->search . '%');
				}
			})->orderBy('id', 'asc');
			$data = !empty($request->searchById) ? $result->get() : $result->offset(0)->limit($limit)->get();
			break;
		case 'department':
			$result = DepartmentMst::where(function ($q) use ($request) {
				if (!empty($request->search)) {
					$q->where('name', 'like', $request->search . '%');
				}
			})->orderBy('name', 'asc');
			$alldata = !empty($request->searchById) ? $result->get() : $result->offset(0)->limit($limit)->get();
			$data = JobDepartmentFooterResource::collection($alldata);
			break;
		default:
			# code...
			break;
		}
		//$data = !empty($request->searchById) ? $result->get() : $result->offset(0)->limit($limit)->get();
		$total_count = $result->count();
		return response()->json(['status' => 200, 'data' => $data, 'total_count' => $total_count]);
	}

	public function getInitialData(Request $request) {
		$user = auth('api')->user();
		$limit = empty($request->limit) ? 200 : $request->limit;
		$countriesData = CountryMst::whereHas('jobs', function ($q) use ($request, $user) {
			$q->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
			if ($request->has('cat')) {
				if ($request->input('cat') == 'country') {
					$q->where('country_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'city') {
					$q->where('city_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'industry') {
					$q->where('industry_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'department') {
					$q->where('department_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'role') {
					$q->where('job_role_id', $request->input('cat_id'));
				}
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$countries = CountryResource::collection($countriesData)->addParam($request, 'jobs');
		$citiesdata = CityMst::whereHas('jobs', function ($q) use ($request, $user) {
			$q->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
			if ($request->has('cat')) {
				if ($request->input('cat') == 'country') {
					$q->where('country_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'city') {
					$q->where('city_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'industry') {
					$q->where('industry_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'department') {
					$q->where('department_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'role') {
					$q->where('job_role_id', $request->input('cat_id'));
				}
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$cities = CityResource::collection($citiesdata)->addParam($request, 'jobs');
		$industriesdata = IndustryMst::whereHas('job', function ($q) use ($request, $user) {
			$q->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
			if ($request->has('cat')) {
				if ($request->input('cat') == 'country') {
					$q->where('country_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'city') {
					$q->where('city_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'industry') {
					$q->where('industry_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'department') {
					$q->where('department_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'role') {
					$q->where('job_role_id', $request->input('cat_id'));
				}
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$industries = IndustryResource::collection($industriesdata)->addParam($request, 'jobs');
		$roles = JobRoleMst::orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$department_data = DepartmentMst::whereHas('jobs', function ($q) use ($request, $user) {
			$q->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			});
			if ($request->has('cat')) {
				if ($request->input('cat') == 'country') {
					$q->where('country_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'city') {
					$q->where('city_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'industry') {
					$q->where('industry_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'department') {
					$q->where('department_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'role') {
					$q->where('job_role_id', $request->input('cat_id'));
				}
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$departments = DepartmentResource::collection($department_data)->addParam($request, 'jobs');
		$employeerJobs = EmployerJobTxn::whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
			$q->where('employee_id', $user->_id);
		})->where(function ($q) use ($request) {
			if ($request->has('cat')) {
				if ($request->input('cat') == 'country') {
					$q->where('country_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'city') {
					$q->where('city_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'industry') {
					$q->where('industry_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'department') {
					$q->where('department_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'role') {
					$q->where('job_role_id', $request->input('cat_id'));
				}
			}
		})
		//->doesntHave('employeeSavedJob')
		/*->doesntHave('employeeSavedJob', function ($q) use ($request) {
					$q->where('employee_id', $user->_id);
				})*/
		//->doesntHave('employeeAppliedJob')
			->orderBy('_id', 'asc')
			->offset(0)
			->limit($limit)->get();

		$total_job_count = $employeerJobs->count();
		$other_master = AllOtherMasterResource::collection(AllOtherMasterMst::get());

		return response()->json(['status' => 200, 'data' => EmployeerJobsResource::collection($employeerJobs), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'roles' => $roles, 'other_master' => $other_master, 'total_job_count' => $total_job_count, 'departments' => $departments]);

	}

	public function getJobListBySearch(Request $request) {
		//return $request->all();
		//echo"<pre>";print_r($request->input('department_list'));
		$limit = empty($request->limit) ? 10 : $request->limit;
		$employeerJobs = EmployerJobTxn::where(function ($q) use ($request) {
			/*if ($request->has('cat')) {
				if ($request->input('cat') == 'country') {
					$q->where('country_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'city') {
					$q->where('city_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'industry') {
					$q->where('industry_id', $request->input('cat_id'));
				} elseif ($request->input('cat') == 'role') {
					$q->where('job_role_id', $request->input('cat_id'));
				}
			}*/
			if (!empty($request->input('country_list'))) {
				$q->whereIn('country_id', $request->input('country_list'));
			}

			if (!empty($request->input('city_list'))) {
				$q->whereIn('city_id', $request->input('city_list'));
			}
			if (!empty($request->input('industry_list'))) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('_id', $request->input('industry_list'));
				});
			}
			if (!empty($request->input('department_list'))) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('_id', $request->input('department_list'));
				});
			}

			if (!empty($request->input('role_list'))) {
				//$q->whereIn('job_role_id', $request->input('role_list'));
			}
			if (!empty($request->input('experince'))) {
				if (!empty($request->input('experince')['min_exp'])) {
					$q->whereHas('minExperiance', function ($q) use ($request) {
						$q->where('id', $request->input('experince')['min_exp']);
					});
				}
				if (!empty($request->input('experince')['max_exp'])) {
					$q->whereHas('maxExperiance', function ($q) use ($request) {
						$q->where('id', $request->input('experince')['max_exp']);
					});
				}
			}
			if (!empty($request->input('salary'))) {
				if (!empty($request->input('salary')['currency'])) {
					$q->whereHas('currency', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['currency']);
					});
				}
				if (!empty($request->input('salary')['min_salary'])) {
					$q->whereHas('minSalary', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['min_salary']);
					});
				}
				if (!empty($request->input('salary')['max_salary'])) {
					$q->whereHas('maxSalary', function ($q) use ($request) {
						$q->where('id', $request->input('salary')['max_salary']);
					});
				}

			}

		})
		//->doesntHave('employeeSavedJob')
		//->doesntHave('employeeAppliedJob')
			->orderBy('id', 'asc')
			->offset(0)
			->limit($limit)->get();
		$total_job_count = $employeerJobs->count();
		return response()->json(['status' => 200, 'data' => EmployeerJobsResource::collection($employeerJobs), 'total_job_count' => $total_job_count]);
	}

	public function getInitDataMultipleSearch(Request $request) {
		return $request->all();
	}

	public function getJobsMultipleSearch(Request $request) {
		//ini_set('memory_limit', '-1');
		$user = auth('api')->user();
		$limit = empty($request->pageLimit) ? 10 : $request->pageLimit;
		$offset = empty($request->pageOffset) ? 0 : $request->pageOffset;

		$countriesData = CountryMst::where(function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id'] && $request['cat'] == 'country'))) {
				$q->where('_id', $request['cat_id']);
			}
		})->whereHas('jobs', function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id']))) {
				if ($request['cat'] == 'country') {
					$q->where('country_id', $request['cat_id']);
				} elseif ($request['cat'] == 'city') {
					$q->where('city_id', $request['cat_id']);
				} elseif ($request['cat'] == 'department') {
					$q->where('department_id', $request['cat_id']);
				} elseif ($request['cat'] == 'industry') {
					$q->where('industry_id', $request['cat_id']);
				}
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereIn('required_skill', $request->formData['key_skills']);
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('country_id', $request->formData['country_id']);
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereIn('city_id', $request->formData['city_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}

			if (!empty($request->input('job_title'))) {
				foreach ($request->input('job_title') as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
			}

			/*if (!empty($request['job_title'])) {
				$q->whereIn('job_title', $request['job_title']);
				$q->orWhereIn('required_skill', $request['job_title']);
			}*/
			if (!empty($request['search_city'])) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
			}
			if (!empty($request['search_department'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', $request['search_department']);
				});
			}
			if (!empty($request['search_industry'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', $request['search_industry']);
				});
			}

			if (!empty($request['country_list'])) {
				$q->whereIn('country_id', $request['country_list']);
			}

			if (!empty($request['city_list'])) {
				$q->whereIn('city_id', $request['city_list']);
			}
			if (!empty($request['industry_list'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('_id', $request['industry_list']);
				});
			}
			if (!empty($request['department_list'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('_id', $request['department_list']);
				});
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();

		$totJobCountArr = $countriesData->map(function ($job) {
			return ['international_job_count' => $job->international_job_count, 'walking_job_count' => $job->walking_job_count, 'country_name' => $job->name];
		});

		$countries = CountryResource::collection($countriesData)->addParam($request, 'jobs');
		$citiesdata = CityMst::where(function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id'] && $request['cat'] == 'city'))) {
				$q->where('_id', $request['cat_id']);
			}
		})->whereHas('jobs', function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id']))) {
				if ($request['cat'] == 'country') {
					$q->where('country_id', $request['cat_id']);
				} elseif ($request['cat'] == 'city') {
					$q->where('city_id', $request['cat_id']);
				} elseif ($request['cat'] == 'department') {
					$q->where('department_id', $request['cat_id']);
				} elseif ($request['cat'] == 'industry') {
					$q->where('industry_id', $request['cat_id']);
				}
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereIn('required_skill', $request->formData['key_skills']);
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('country_id', $request->formData['country_id']);
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereIn('city_id', $request->formData['city_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}
			if (!empty($request->input('job_title'))) {
				foreach ($request->input('job_title') as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
			}
			if (!empty($request['search_city'])) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
			}
			if (!empty($request['search_department'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', $request['search_department']);
				});
			}
			if (!empty($request['search_industry'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', $request['search_industry']);
				});
			}

			if (!empty($request['country_list'])) {
				$q->whereIn('country_id', $request['country_list']);
			}

			if (!empty($request['city_list'])) {
				$q->whereIn('city_id', $request['city_list']);
			}
			if (!empty($request['industry_list'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('_id', $request['industry_list']);
				});
			}
			if (!empty($request['department_list'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('_id', $request['department_list']);
				});
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$cities = CityResource::collection($citiesdata)->addParam($request, 'jobs');
		$industriesdata = IndustryMst::where(function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id'] && $request['cat'] == 'industry'))) {
				$q->where('_id', $request['cat_id']);
			}
		})->whereHas('job', function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id']))) {
				if ($request['cat'] == 'country') {
					$q->where('country_id', $request['cat_id']);
				} elseif ($request['cat'] == 'city') {
					$q->where('city_id', $request['cat_id']);
				} elseif ($request['cat'] == 'department') {
					$q->where('department_id', $request['cat_id']);
				} elseif ($request['cat'] == 'industry') {
					$q->where('industry_id', $request['cat_id']);
				}
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereIn('required_skill', $request->formData['key_skills']);
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('country_id', $request->formData['country_id']);
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereIn('city_id', $request->formData['city_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}

			if (!empty($request->input('job_title'))) {
				foreach ($request->input('job_title') as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
			}
			if (!empty($request['search_city'])) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
			}
			if (!empty($request['search_department'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', $request['search_department']);
				});
			}
			if (!empty($request['search_industry'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', $request['search_industry']);
				});
			}

			if (!empty($request['country_list'])) {
				$q->whereIn('country_id', $request['country_list']);
			}

			if (!empty($request['city_list'])) {
				$q->whereIn('city_id', $request['city_list']);
			}
			if (!empty($request['industry_list'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('_id', $request['industry_list']);
				});
			}
			if (!empty($request['department_list'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('_id', $request['department_list']);
				});
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$industries = IndustryResource::collection($industriesdata)->addParam($request, 'jobs');
		$roles = JobRoleMst::orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$department_data = DepartmentMst::where(function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id'] && $request['cat'] == 'department'))) {
				$q->where('_id', $request['cat_id']);
			}
		})->whereHas('jobs', function ($q) use ($request) {
			if (!empty($request['cat'] && !empty($request['cat_id']))) {
				if ($request['cat'] == 'country') {
					$q->where('country_id', $request['cat_id']);
				} elseif ($request['cat'] == 'city') {
					$q->where('city_id', $request['cat_id']);
				} elseif ($request['cat'] == 'department') {
					$q->where('department_id', $request['cat_id']);
				} elseif ($request['cat'] == 'industry') {
					$q->where('industry_id', $request['cat_id']);
				}
			}
			if (!empty($request->formData['key_skills'])) {
				$q->whereIn('required_skill', $request->formData['key_skills']);
			}
			if (!empty($request->formData['industry_id'])) {
				$q->whereIn('industry_id', $request->formData['industry_id']);
			}
			if (!empty($request->formData['country_id'])) {
				$q->whereIn('country_id', $request->formData['country_id']);
			}
			if (!empty($request->formData['city_id'])) {
				$q->whereIn('city_id', $request->formData['city_id']);
			}
			if (!empty($request->formData['department_id'])) {
				$q->whereIn('department_id', $request->formData['department_id']);
			}

			if (!empty($request->input('job_title'))) {
				foreach ($request->input('job_title') as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
			}
			if (!empty($request['search_city'])) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', $request['search_city']);
				});
			}
			if (!empty($request['search_department'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', $request['search_department']);
				});
			}
			if (!empty($request['search_industry'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', $request['search_industry']);
				});
			}

			if (!empty($request['country_list'])) {
				$q->whereIn('country_id', $request['country_list']);
			}

			if (!empty($request['city_list'])) {
				$q->whereIn('city_id', $request['city_list']);
			}
			if (!empty($request['industry_list'])) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('_id', $request['industry_list']);
				});
			}
			if (!empty($request['department_list'])) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('_id', $request['department_list']);
				});
			}
		})->orderBy('_id', 'asc')->offset(0)
			->limit($limit)->get();
		$departments = DepartmentResource::collection($department_data)->addParam($request, 'jobs');
		$employeerJobs = EmployerJobTxn::whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
			$q->where('employee_id', $user->_id);
		})->where(function ($q) use ($request, $user) {
			if (!empty($request->input('job_title'))) {
				foreach ($request->input('job_title') as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
				/*$q->whereIn('job_title', $request->input('job_title'));
				$q->orWhereIn('required_skill', $request->input('job_title'));*/
			}
			if (!empty($request['cat'] && !empty($request['cat_id']))) {
				if ($request['cat'] == 'country') {
					$q->where('country_id', $request['cat_id']);
				} elseif ($request['cat'] == 'city') {
					$q->where('city_id', $request['cat_id']);
				} elseif ($request['cat'] == 'department') {
					$q->where('department_id', $request['cat_id']);
				} elseif ($request['cat'] == 'industry') {
					$q->where('industry_id', $request['cat_id']);
				}
			}
			if (!empty($request->input('search_city'))) {
				$q->whereHas('country', function ($q) use ($request) {
					$q->whereIn('name', $request->input('search_city'));
				});
				$q->orWhereHas('city', function ($q) use ($request) {
					$q->whereIn('name', $request->input('search_city'));
				});
			}
			if (!empty($request->input('search_department'))) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('name', $request->input('search_department'));
				});
			}
			if (!empty($request->input('search_industry'))) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('name', $request->input('search_industry'));
				});
			}

			if (!empty($request->input('country_list'))) {
				$q->whereIn('country_id', $request->input('country_list'));
			}

			if (!empty($request->input('city_list'))) {
				$q->whereIn('city_id', $request->input('city_list'));
			}
			if (!empty($request->input('industry_list'))) {
				$q->whereHas('industry', function ($q) use ($request) {
					$q->whereIn('_id', $request->input('industry_list'));
				});
			}
			if (!empty($request->input('department_list'))) {
				$q->whereHas('department', function ($q) use ($request) {
					$q->whereIn('_id', $request->input('department_list'));
				});
			}

			if (!empty($request->input('role_list'))) {
				//$q->whereIn('job_role_id', $request->input('role_list'));
			}
			if (!empty($request->input('experince'))) {
				if (!empty($request->input('experince')['min_exp']) && empty($request->input('experince')['max_exp'])) {
					$minexp = ExperienceMst::find($request->input('experince')['min_exp']);
					$q->whereHas('minExperiance', function ($q) use ($request, $minexp) {
						$q->where('value', '>=', $minexp->value);
					});
				} elseif (!empty($request->input('experince')['min_exp']) && !empty($request->input('experince')['max_exp'])) {
					$minexp = ExperienceMst::find($request->input('experince')['min_exp']);
					$maxexp = ExperienceMst::find($request->input('experince')['max_exp']);
					$q->whereHas('minExperiance', function ($q) use ($request, $minexp, $maxexp) {
						$q->where('value', '>=', $minexp->value);
						$q->where('value', '<=', $maxexp->value);
					});
				} elseif (empty($request->input('experince')['min_exp']) && !empty($request->input('experince')['max_exp'])) {
					$maxexp = ExperienceMst::find($request->input('experince')['max_exp']);
					$q->whereHas('maxExperiance', function ($q) use ($request, $maxexp) {
						$q->where('value', '<=', $maxexp->value);
					});
				}
			}
			if (!empty($request->input('salary'))) {
				if (!empty($request->input('salary')['currency'])) {
					$q->where('currency', $request->input('salary')['currency']);
				}
				if (!empty($request->input('salary')['min_salary']) && !empty($request->input('salary')['max_salary'])) {
					$q->where('min_monthly_salary', '>=', (int) $request->input('salary')['min_salary']);
					$q->where('min_monthly_salary', '<=', (int) $request->input('salary')['max_salary']);
				}
				if (!empty($request->input('salary')['min_salary']) && empty($request->input('salary')['max_salary'])) {
					$q->where('min_monthly_salary', '>=', (int) $request->input('salary')['min_salary']);
				}
				if (empty($request->input('salary')['min_salary']) && !empty($request->input('salary')['max_salary'])) {
					$q->where('max_monthly_salary', '<=', (int) $request->input('salary')['max_salary']);
				}

			}

		})->orderBy('created_at', 'desc')
			->skip($offset)->take($limit)->get();
		//->offset(0)->limit($limit)->get();

		$total_job_count = $employeerJobs->count();
		$other_master = AllOtherMasterResource::collection(AllOtherMasterMst::get());
		//EmployeerJobsResource::collection($employeerJobs)
		return response()->json(['status' => 200, 'data' => EmployeerJobsResource::collection($employeerJobs), 'countries' => $countries, 'cities' => $cities, 'industries' => $industries, 'roles' => $roles, 'other_master' => $other_master, 'total_job_count' => $total_job_count, 'departments' => $departments, 'totOtherJobCountArr' => $totJobCountArr]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$user = auth('api')->user();
		try {
			if ($user->employeeCvTxn) {
				$applicationPhase = ApplicationPhaseMst::where('phase_name', 'Applied')->first();
				$checkAppliedJob = EmployeeAppliedJobsTxn::where('job_id', $request->job_id)->where('employee_id', $user->_id)->first();
				if ($checkAppliedJob) {
					return response()->json(['status' => 422, 'status_text' => 'You already have applied this job']);
				} else {
					$empJob = EmployerJobTxn::find($request->job_id);
					$empJob->jobViews()->detach($user->_id);
					$empJob->jobViews()->attach($user->_id);
					$appId = EmployeeAppliedJobsTxn::count();
					$userCountry = $user->employeeUser->country_id;
					$jobUserType = '';
					if ($userCountry == $empJob->country_id) {
						$jobUserType = '1';
					} else {
						$jobUserType = '2';
					}
					$txn = EmployeeAppliedJobsTxn::create(['job_id' => $request->job_id, 'applied_date' => Carbon::now()->format('Y-m-d'), 'applied_status' => $applicationPhase->_id, 'apply_user_type' => $jobUserType, 'bsb_app_id' => ($appId + 1), 'employee_id' => $user->_id, 'created_by' => $user->_id]);
					if ($request->job_questions) {
						$txn->job_questions = $request->job_questions;
						$txn->save();
					}

					$name = $user->employeeUser->first_name;
					$data = ['text' => 'Applied job', 'job_id' => $empJob->bsb_jb_trans_id, 'user_name' => $name];
					$view = 'emails.employee-applied-job';
					$subject = 'Congratulations from bsbstaffing.com! Your job application has been received by the employer!';

					Mail::to($user->email)->send(new SendMail($data, $view, $subject));

					$employer_name = $empJob->employer->employerUser->first_name;
					$employerdata = ['text' => 'Application received', 'job_id' => $empJob->bsb_jb_trans_id, 'user_name' => $employer_name];
					$employerview = 'emails.application-received';
					$employersubject = 'Please review these new applications received by your posted jobs in bsbstaffing.com!';
					if ($empJob->employer->employerUser->company_email) {
						Mail::to($empJob->employer->employerUser->company_email)->send(new SendMail($employerdata, $employerview, $employersubject));
					}
					//mailbox save
					$mailbody = 'Your job ' . $empJob->bsb_jb_trans_id . ' posting has received a new application. Please check application receive.';
					$template = MailTxn::create([
						'mail_receive_id' => $empJob->employer_id,
						'mail_sender_id' => $user->_id,
						'mail_subject' => $empJob->job_title,
						'mail_body' => $mailbody,
						'mail_status' => 1,
						'parent_id' => $request->reply_id ? $request->reply_id : '',
					]);
					if ($request->job_questions) {
						$template->job_questions = $request->job_questions;
						$template->save();
					}
					$attFile = [];
					if ($user->employeeUser->profile_picture) {
						$attFile[] = '/upload_files/profile_picture/' . $user->employeeUser->profile_picture;
					}
					if ($user->employeeCvTxn->cv_path) {
						$attFile[] = '/upload_files/employee-resumes/' . $user->employeeCvTxn->cv_path;
					}
					$template->attached_file = $attFile;

					if ($user->employeeVideos->count() > 0) {
						$template->video_links = $user->employeeVideos->pluck('video_link')->toArray();
					}
					$template->save();

					return response()->json(['status' => 200, 'status_text' => 'Successfully applied job']);
				}
			} else {
				return response()->json(['status' => 422, 'status_text' => 'Please complete your profile ,once completed your profile then you can apply job']);
			}

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function savedJob(Request $request) {
		$user = auth('api')->user();
		try {
			$checkSavedJob = EmployeeSavedJobsTxn::where('job_id', $request->job_id)->where('employee_id', $user->_id)->first();
			$checkAppliedJob = EmployeeAppliedJobsTxn::where('job_id', $request->job_id)->where('employee_id', $user->_id)->first();
			if ($checkSavedJob) {
				return response()->json(['status' => 422, 'status_text' => 'You already have saved this job']);
			} elseif ($checkAppliedJob) {
				return response()->json(['status' => 422, 'status_text' => 'You already have applied this job']);
			} else {
				$empJob = EmployerJobTxn::find($request->job_id);
				$empJob->jobViews()->detach($user->_id);
				$empJob->jobViews()->attach($user->_id);
				EmployeeSavedJobsTxn::create(['job_id' => $request->job_id, 'applied_date' => Carbon::now()->format('Y-m-d'), 'employee_id' => $user->_id, 'saved_on' => Carbon::now()->format('Y-m-d'), 'created_by' => $user->_id]);
				return response()->json(['status' => 200, 'status_text' => 'Successfully saved job']);
			}

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function getSimilarJobs($job_id, $limit) {

		$job = EmployerJobTxn::find($job_id);
		//return $job;
		$limit = empty($limit) ? 4 : (int) $limit;
		$similarJobs = EmployerJobTxn::where('job_title', 'like', '%' . $job->job_title . '%')
			->where('city_id', $job->city_id)
			->where('_id', '!=', $job->_id)
			->orderBy('created_at', 'desc');
		//->offset(0)
		//->limit($limit)->get();
		$similar_job_count = $similarJobs->count();
		return response()->json(['status' => 200, 'similar_jobs' => EmployeerJobsResource::collection($similarJobs->offset(0)->limit($limit)->get()), 'similar_job_count' => $similar_job_count]);
	}

	public function removeSaveJob($job_id) {
		$user = auth('api')->user();
		EmployeeSavedJobsTxn::where('job_id', $job_id)->where('employee_id', $user->_id)->delete();
		return response()->json(['status' => 200]);
	}
}
