<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeAppliedJobs as EmployeeAppliedJobsResource;
use App\Http\Resources\EmployeeSavedJobs as EmployeeSavedJobsResource;
use App\Http\Resources\EmployerJobsSummary as EmployerJobsSummaryResource;
use App\Http\Resources\InternationalJobSavedApplied as InternationalJobSavedAppliedResource;
use App\Http\Resources\InterviewSlotBook as InterviewSlotBookResource;
use App\Http\Resources\WalkingJobsSummary as WalkInInterviewResource;
use App\Models\ApplicationPhaseMst;
use App\Models\EmployeeApplicationStatusHistTxn;
use App\Models\EmployeeAppliedJobsTxn;
use App\Models\EmployeeSavedJobsTxn;
use App\Models\EmployerJobTxn;
use App\Models\InternationalJobApplicationStatusHistTxn;
use App\Models\InternationalJobSavedAppliedTxn;
use App\Models\InternationalJobTxn;
use App\Models\InterviewSlotBook;
use App\Models\MailTxn;
use App\Models\WalkInInterviewTxn;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobsForController extends Controller {
	public function index() {
		$application_phases = ApplicationPhaseMst::all();
		return response()->json(['status' => 200, compact('application_phases')]);
	}

	public function getjobAlert(Request $request) {
		$user = auth('api')->user();
		$candType = $user->employeeUser->gender == 1 ? 2 : 3;
		$userIndustry = $user->employeePrefTxn->industries()->pluck('_id')->toArray();
		$userExpValue = $user->employeeUser->total_experience_yr_value;

		$alert_jobs = EmployerJobTxn::whereHas('empJobAlert', function ($q) use ($user, $request) {
			$q->where('emp_id', $user->_id);
			$q->where('alert_type', $request->type);
		})->whereIn('industry_id', $userIndustry)->whereHas('minExperiance', function ($q) use ($userExpValue) {
			if ($userExpValue) {
				$q->where('value', '<=', $userExpValue);
			}
		})->where(function ($q) use ($candType) {
			$q->where('candidate_type', $candType);
			$q->orWhere('candidate_type', 1);
		})->doesntHave('employeeSavedJob')->doesntHave('employeeAppliedJob')->orderBy('_id', 'desc')->get();

		/*$likeJob = EmployerJobTxn::where(function ($q) use ($user) {
			$q->whereHas('industry', function ($q) use ($user) {
				$q->where('_id', $user->employeePrefTxn->industry_id);
			});
			// skill should be checked
		})->doesntHave('employeeSavedJob')->doesntHave('employeeAppliedJob')->orderBy('_id', 'desc')->get();*/

		$job_data = EmployerJobsSummaryResource::collection($alert_jobs);
		//$like_job_data = EmployeerJobs::collection($likeJob);

		return response()->json(['status' => 200, 'data' => compact('job_data')]);
	}

	public function getAppliedJobs(Request $request) {
		$user = auth('api')->user();
		$limit = 10;
		$skip = $request->skip ? (int) $request->skip : 0;
		$emp_appliedjobs = EmployeeAppliedJobsTxn::where('employee_id', $user->_id)->orderBy('updated_at', 'desc');
		$applied_jobs = EmployeeAppliedJobsResource::collection($emp_appliedjobs->skip($skip)->take($limit)->get());
		$application_phases = ApplicationPhaseMst::all();
		$total_count = $emp_appliedjobs->count();
		return response()->json(['status' => 200, 'data' => compact('applied_jobs', 'application_phases', 'total_count')]);
	}

	public function getSavedJobs() {
		$user = auth('api')->user();
		$emp_savedjobs = EmployeeSavedJobsTxn::where('employee_id', $user->_id)->whereNull('employee_application_id')->orderBy('updated_at', 'desc')->get();
		$saved_jobs = EmployeeSavedJobsResource::collection($emp_savedjobs);
		return response()->json(['status' => 200, 'data' => compact('saved_jobs')]);
	}

	public function savedJobApplied(Request $request) {
		$user = auth('api')->user();
		try {
			$applicationPhase = ApplicationPhaseMst::where('phase_name', 'Applied')->first();
			$applied = EmployeeAppliedJobsTxn::create(['job_id' => $request->job_id, 'applied_date' => Carbon::now()->format('Y-m-d'), 'applied_status' => $applicationPhase->_id, 'employee_id' => $user->_id, 'created_by' => $user->_id]);
			if ($request->job_questions) {
				$applied->job_questions = $request->job_questions;
				$applied->save();
			}
			$empJob = $applied->job;
			$emp_saved = EmployeeSavedJobsTxn::find($request->saved_id);
			$emp_saved->employee_application_id = $applied->_id;
			$emp_saved->updated_by = $user->_id;
			$emp_saved->save();

			$mailbody = 'Your job ' . $empJob->bsb_jb_trans_id . ' posting has received a new application. Please check application receive.';
			$template = MailTxn::create([
				'mail_receive_id' => $empJob->employer_id,
				'mail_sender_id' => $user->_id,
				'mail_subject' => $empJob->job_title,
				'mail_body' => $mailbody,
				'mail_status' => 1,
				'parent_id' => '',
			]);
			if ($request->job_questions) {
				$template->job_questions = $request->job_questions;
				$template->save();
			}
			$attFile = [];
			if ($user->employeeUser->profile_picture) {
				$attFile[] = '/upload_files/profile_picture/' . $user->employeeUser->profile_picture;
			}
			if ($user->employeeCvTxn->cv_path) {
				$attFile[] = '/upload_files/employee-resumes/' . $user->employeeCvTxn->cv_path;
			}
			$template->attached_file = $attFile;

			if ($user->employeeVideos->count() > 0) {
				$template->video_links = $user->employeeVideos->pluck('video_link')->toArray();
			}
			$template->save();

			return response()->json(['status' => 200, 'status_text' => 'Successfully applied job']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function getLocalWalkingJobs(Request $request) {
		$limit = empty($request->limit) ? 12 : (int) $request->limit;
		$user = auth('api')->user();
		$candType = $user->employeeUser->gender == 1 ? 2 : 3;
		$userIndustry = $user->employeePrefTxn->industries()->pluck('_id')->toArray();
		$userExpValue = $user->employeeUser->total_experience_yr_value;
		$bookedSlotIds = $user->employeeWalkinBook()->whereHas('interview', function ($q) {
			$q->where('walkin_location_type', 1);
			$q->where('status', 1);
		})->pluck('interview_id')->toArray();

		$walkinTxn = WalkInInterviewTxn::where(function ($q) use ($request) {
			if (!empty($request->walking_id)) {
				$q->where('_id', $request->walking_id);
			}
		})->whereIn('industry_id', $userIndustry)->whereHas('minExperiance', function ($q) use ($userExpValue) {
			if ($userExpValue) {
				$q->where('value', '<=', $userExpValue);
			}
		})->where(function ($q) use ($candType) {
			$q->where('candidate_type', $candType);
			$q->orWhere('candidate_type', 1);
		})->where('interview_location_country', $user->employeeUser->country_id)->where('interview_location_city', $user->employeeUser->city_id)->where('walkin_location_type', 1)->where('status', 1)->whereNotIn('_id', $bookedSlotIds)->orderBy('updated_at', 'desc');
		$total_localwalkin = $walkinTxn->count();
		$data = WalkInInterviewResource::collection($walkinTxn->offset(0)->limit($limit)->get());
		$bookedSlot = InterviewSlotBook::whereHas('interview', function ($q) {
			$q->where('walkin_location_type', 1);
			//$q->where('status', 1);
		})->where(function ($q) use ($request) {
			if (!empty($request->walking_id)) {
				$q->where('interview_id', $request->walking_id);
			}
		})->get();

		$booked_data = InterviewSlotBookResource::collection($bookedSlot);
		return response()->json(['status' => 200, 'data' => $data, 'booked_slot' => $booked_data, 'total_localwalkin' => $total_localwalkin]);
	}

	public function getInternationalWalkingJobs(Request $request) {
		ini_set('memory_limit', '-1');
		$limit = empty($request->limit) ? 12 : (int) $request->limit;
		$user = auth('api')->user();
		$candType = $user->employeeUser->gender == 1 ? 2 : 3;
		$userIndustry = $user->employeePrefTxn->industries()->pluck('_id')->toArray();
		$userExpValue = $user->employeeUser->total_experience_yr_value;

		$bookedSlotIds = $user->employeeWalkinBook()->whereHas('interview', function ($q) {
			$q->where('walkin_location_type', 2);
			$q->where('status', 1);
		})->pluck('interview_id')->toArray();

		$walkinTxn = WalkInInterviewTxn::where(function ($q) use ($request) {
			if (!empty($request->walking_id)) {
				$q->where('_id', $request->walking_id);
			}
		})->whereIn('industry_id', $userIndustry)->whereHas('minExperiance', function ($q) use ($userExpValue) {
			if ($userExpValue) {
				$q->where('value', '<=', $userExpValue);
			}
		})->where(function ($q) use ($candType) {
			$q->where('candidate_type', $candType);
			$q->orWhere('candidate_type', 1);
		})->whereIn('interview_location_country', $user->employeePrefTxn->country_id)
			->whereIn('interview_location_city', $user->employeePrefTxn->city_id)->where('walkin_location_type', 2)
			->where('status', 1)->whereNotIn('_id', $bookedSlotIds)->orderBy('updated_at', 'desc');
		$total_internationalwalkin = $walkinTxn->count();
		$data = WalkInInterviewResource::collection($walkinTxn->offset(0)->limit($limit)->get());
		$bookedSlot = InterviewSlotBook::whereHas('interview', function ($q) {
			$q->where('walkin_location_type', 2);
			//$q->where('status', 1);
		})->where(function ($q) use ($request) {
			if (!empty($request->walking_id)) {
				$q->where('interview_id', $request->walking_id);
			}
		})->get();
		$booked_data = InterviewSlotBookResource::collection($bookedSlot);
		return response()->json(['status' => 200, 'data' => $data, 'booked_slot' => $booked_data, 'total_internationalwalkin' => $total_internationalwalkin]);
	}

	public function storeSlot(Request $request) {
		$user = auth('api')->user();

		try {
			$request_data = $request['data'];
			$userCountry = $user->employeeUser->country_id;
			$walking_job = WalkInInterviewTxn::find($request_data['interview_id']);
			$jobUserType = '';
			if ($userCountry == $walking_job->country_id) {
				$jobUserType = '1';
			} else {
				$jobUserType = '2';
			}

			$booked_date = Carbon::parse(str_replace("/", "-", $request_data['booked_date']))->format('Y-m-d');
			$booked_time = Carbon::parse($request_data['booked_time'])->format('H:i:s');
			$appId = InterviewSlotBook::count();

			$bookedSlot = InterviewSlotBook::create(['interview_id' => $request_data['interview_id'], 'user_id' => $user->_id, 'booked_date' => $booked_date, 'booked_time' => $booked_time, 'apply_user_type' => $jobUserType, 'bsb_app_id' => ($appId + 1)]);
			if ($request->job_questions) {
				$bookedSlot->job_questions = $request->job_questions;
				$bookedSlot->save();
			}
			$mailbody = 'Your walking job' . $walking_job->bsb_jb_trans_id . ' posting has received a new application. Please check application receive.';
			$template = MailTxn::create([
				'mail_receive_id' => $walking_job->employer_id,
				'mail_sender_id' => $user->_id,
				'mail_subject' => $walking_job->job_title,
				'mail_body' => $mailbody,
				'mail_status' => 1,
				'parent_id' => '',
			]);
			if ($request->job_questions) {
				$template->job_questions = $request->job_questions;
				$template->save();
			}
			$attFile = [];
			if ($user->employeeUser->profile_picture) {
				$attFile[] = '/upload_files/profile_picture/' . $user->employeeUser->profile_picture;
			}
			if ($user->employeeCvTxn->cv_path) {
				$attFile[] = '/upload_files/employee-resumes/' . $user->employeeCvTxn->cv_path;
			}
			$template->attached_file = $attFile;

			if ($user->employeeVideos->count() > 0) {
				$template->video_links = $user->employeeVideos->pluck('video_link')->toArray();
			}
			$template->save();

			return response()->json(['status' => 200, 'status_text' => 'Successfully Applied']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function getApplicationStatus(Request $request) {
		$user = auth('api')->user();
		$emp_appliedjobs = EmployeeAppliedJobsTxn::where('employee_id', $user->id)->orderBy('_id', 'desc')->get();
		$applied_jobs = EmployeeAppliedJobsResource::collection($emp_appliedjobs);
		//return $emp_appliedjobs;
		//return response()->json(['status' => 200, 'data' => ['applied_jobs' => $emp_appliedjobs]]);
		return response()->json(['status' => 200, 'data' => compact('applied_jobs')]);
	}

	public function getApplicationReceived(Request $request) {
		$user = auth('api')->user();
		//echo $request->job_type;die;
		$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 10);
		$applied_jobs = [];
		$applied_international_jobs = [];
		$applied_walk_in = [];

		$applied_list_jobs = [];
		$applied_jobs_count = 0;

		$applied_interntional_jobs_list = [];
		$applied_interntional_count = 0;

		$applied_walk_in_list = [];
		$applied_walk_in_count = 0;

		if (!empty($request->job_type)) {
			if ($request->job_type == 'free_job_post') {
				$applied_jobs = EmployeeAppliedJobsTxn::where(function ($q) use ($user) {
					$q->whereHas('job', function ($q) use ($user) {
						$q->where('employer_id', $user->_id);
						$q->where('job_type', 1);
					});
				});
				$posted_jobs = EmployerJobTxn::where('job_type', 1)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
			} elseif ($request->job_type == 'premium_job_post') {

				$applied_jobs = EmployeeAppliedJobsTxn::where(function ($q) use ($user) {
					$q->whereHas('job', function ($q) use ($user) {
						$q->where('employer_id', $user->_id);
						$q->where('job_type', 2);
					});
				});
				$posted_jobs = EmployerJobTxn::where('job_type', 2)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
			} elseif ($request->job_type == 'international_free_job') {

				$applied_international_jobs = InternationalJobSavedAppliedTxn::where(function ($q) use ($user) {
					$q->whereHas('job', function ($q) use ($user) {
						$q->where('employer_id', $user->_id);
						$q->where('job_type', 1);
					});
				});
				$posted_jobs = InternationalJobTxn::where('job_type', 1)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
			} elseif ($request->job_type == 'international_premium_job') {

				$applied_international_jobs = InternationalJobSavedAppliedTxn::where(function ($q) use ($user) {
					$q->whereHas('job', function ($q) use ($user) {
						$q->where('employer_id', $user->_id);
						$q->where('job_type', 2);
					});
				});
				$posted_jobs = InternationalJobTxn::where('job_type', 2)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
			} elseif ($request->job_type == 'local_walk_in') {

				//$applied_jobs = [];
				$applied_walk_in = InterviewSlotBook::where(function ($q) use ($user) {
					$q->whereHas('interview', function ($q) use ($user) {
						$q->where('employer_id', $user->_id);
						$q->where('walkin_location_type', 1);
					});
				});
				$posted_jobs = WalkInInterviewTxn::where('walkin_location_type', 1)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
			} elseif ($request->job_type == 'international_walk_in') {

				//$applied_jobs = [];
				$applied_walk_in = InterviewSlotBook::where(function ($q) use ($user) {
					$q->whereHas('interview', function ($q) use ($user) {
						$q->where('employer_id', $user->_id);
						$q->where('walkin_location_type', 2);
					});
				});
				$posted_jobs = WalkInInterviewTxn::where('walkin_location_type', 2)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
			}
		} else {
			//free job post
			$applied_jobs = EmployeeAppliedJobsTxn::where(function ($q) use ($user) {
				$q->whereHas('job', function ($q) use ($user) {
					$q->where('employer_id', $user->_id);
					$q->where('job_type', 1);
				});
			});

			$posted_jobs = EmployerJobTxn::where('job_type', 1)->where('employer_id', $user->_id)->orderBy('created_at', 'desc')->get();
		}

		//applied jobs count
		if ($applied_jobs) {
			$applied_jobs_count = $applied_jobs->count();
		}

		//applied jobs
		if ($applied_jobs) {
			$applied_list_jobs = EmployeeAppliedJobsResource::collection($applied_jobs->offset($offset)->orderBy('created_at', 'desc')->limit(10)->get());
		}

		// applied international job count
		if ($applied_international_jobs) {
			$applied_interntional_count = $applied_international_jobs->count();
		}

		// applied international jobs
		if ($applied_international_jobs) {
			$applied_interntional_jobs_list = InternationalJobSavedAppliedResource::collection($applied_international_jobs->offset($offset)->orderBy('created_at', 'desc')->limit(10)->get());
		}

		//applied walkin count
		if ($applied_walk_in) {
			$applied_walk_in_count = $applied_walk_in->count();
		}

		if ($applied_walk_in) {
			$applied_walk_in_list = InterviewSlotBookResource::collection($applied_walk_in->offset($offset)->orderBy('created_at', 'desc')->limit(10)->get());
			//return $applied_walk_in_list;
		}

		return response()->json(['status' => 200, 'data' => compact('applied_list_jobs', 'posted_jobs', 'applied_jobs_count', 'applied_walk_in_list', 'applied_walk_in_count', 'applied_interntional_jobs_list', 'applied_interntional_count')]);

	}

	public function getAllApplicationPhase() {
		$application_phases = ApplicationPhaseMst::all();
		return response()->json(['status' => 200, 'data' => compact('application_phases')]);
	}

	public function getApplicationJobById(Request $request) {
		//return $request->all();
		//echo $application_id;die;
		$user = auth('api')->user();
		$applied_list_job = [];
		$applied_walk_in = [];
		$applied_international_job = [];
		if ($request->job_type == 'job') {
			$applied_job = EmployeeAppliedJobsTxn::where('_id', $request->application_id)->where(function ($q) use ($user) {
				$q->whereHas('job', function ($q) use ($user) {
					$q->where('employer_id', $user->_id);
				});
			});
			$applied_list_job = EmployeeAppliedJobsResource::collection($applied_job->get());

		} elseif ($request->job_type == 'international-job') {
			$applied_international = InternationalJobSavedAppliedTxn::where('_id', $request->application_id)->where(function ($q) use ($user) {
				$q->whereHas('job', function ($q) use ($user) {
					$q->where('employer_id', $user->_id);
				});
			});
			$applied_international_job = InternationalJobSavedAppliedResource::collection($applied_international->get());

		} elseif ($request->job_type == 'walk-in') {
			$applied_wolkin = InterviewSlotBook::where('_id', $request->application_id)->where(function ($q) use ($user) {
				$q->whereHas('interview', function ($q) use ($user) {
					$q->where('employer_id', $user->_id);
				});
			});
			$applied_walk_in = InterviewSlotBookResource::collection($applied_wolkin->get());

		}
		return response()->json(['status' => 200, 'data' => compact('applied_list_job', 'applied_walk_in', 'applied_international_job')]);
	}

	public function updateAppliedJob(Request $request, $id) {
		//dd($request->all());
		$user = auth('api')->user();
		//DB::beginTransaction();
		try {
			$applied_job = EmployeeAppliedJobsTxn::find($id);
			$applied_job->applied_status = $request->application_status;
			$applied_job->save();
			$status_history = EmployeeApplicationStatusHistTxn::create(['applied_status' => $request->application_status, 'hist_date' => Carbon::now()->format('Y-m-d'), 'employer_comment' => $request->special_note, 'employee_application_id' => $id, 'created_by' => $user->_id]);
			//DB::commit();
			return response()->json(['status' => 200, 'status_text' => 'Successfully update applied job']);

		} catch (\Exception $e) {
			//DB::rollBack();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function updateInternationalAppliedJob(Request $request, $id) {
		$user = auth('api')->user();
		try {
			$applied_job = InternationalJobSavedAppliedTxn::find($id);
			$applied_job->applied_status = $request->application_status;
			$applied_job->save();
			$status_history = InternationalJobApplicationStatusHistTxn::create(['applied_status' => $request->application_status, 'hist_date' => Carbon::now()->format('Y-m-d'), 'employer_comment' => $request->special_note, 'employee_application_id' => $id, 'created_by' => $user->_id]);
			return response()->json(['status' => 200, 'status_text' => 'Successfully update']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function getWalkinInterviewById(Request $request) {
		$user = auth('api')->user();
		$bookedSlotIds = $user->employeeWalkinBook()->whereHas('interview', function ($q) {
			//$q->where('walkin_location_type', 1);
			$q->where('status', 1);
		})->pluck('interview_id')->toArray();
		$walkin_detais = WalkInInterviewTxn::where('_id', $request->id)->get();

		$bookedSlot = InterviewSlotBook::whereHas('interview', function ($q) {
			//$q->where('walkin_location_type', 1);
			//$q->where('status', 1);
		})->where(function ($q) use ($request) {
			if (!empty($request->id)) {
				$q->where('interview_id', $request->id);
			}
		})->get();
		$booked_data = InterviewSlotBookResource::collection($bookedSlot);
		return response()->json(['status' => 200, 'walking_interviews' => WalkInInterviewResource::collection($walkin_detais), 'booked_slot' => $booked_data]);
	}

	public function getApplicationHistory(Request $request) {
		//return $request->all();
		$allpication_history = EmployeeApplicationStatusHistTxn::where('employee_application_id', $request->application_id)->get();
		return response()->json(['status' => 200, 'allpication_history' => $allpication_history]);
	}

}
