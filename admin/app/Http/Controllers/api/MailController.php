<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Emails as EmailsResource;
use App\Models\MailLabel;
use App\Models\MailTxn;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class MailController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		ini_set('memory_limit', '-1');
		$user = auth('api')->user();
		$limit = empty($request->limit) ? 10 : (int) $request->limit;
		$offset = empty($request->offset) ? 0 : (int) ($request->offset);

		$mails = '';
		$mailCount = '';
		switch ($request->param) {
		case 'inbox':
			$recivedMails = MailTxn::where('mail_receive_id', $user->_id)->where('mail_status', 1)->whereNull('label_id')->where('parent_id', '')->pluck('_id')->toArray();
			$sendMails = MailTxn::where(function ($q) use ($user) {
				$q->where('mail_sender_id', $user->_id);
			})->orWhere(function ($q) use ($user) {
				$q->where('mail_receive_id', $user->_id);
			})->whereNotNull('parent_id')->where('mail_status', 1)->whereNull('label_id')->groupBy('parent_id')->pluck('parent_id')->toArray();
			$mails = MailTxn::whereIn('_id', array_unique(array_merge($recivedMails, $sendMails)))->whereNotIn('delete_users', [$user->_id])->whereDoesntHave('mailLabelUser', function ($q) use ($user) {
				$q->where('created_by', $user->_id);
			})->orderBy('updated_at', 'desc');
			$mailCount = $mails->count();
			break;
		case 'sentbox':
			$sendMails = MailTxn::where('mail_sender_id', $user->_id)->where('mail_status', 1)->whereNull('label_id')->where('parent_id', '')->pluck('_id')->toArray();
			$recivedMails = MailTxn::where(function ($q) use ($user) {
				$q->where('mail_receive_id', $user->_id);
			})->orWhere(function ($q) use ($user) {
				$q->where('mail_sender_id', $user->_id);
			})->whereNotNull('parent_id')->where('mail_status', 1)->whereNull('label_id')->groupBy('parent_id')->pluck('parent_id')->toArray();
			$mails = MailTxn::whereIn('_id', array_unique(array_merge($recivedMails, $sendMails)))->whereNotIn('delete_users', [$user->_id])->whereDoesntHave('mailLabelUser', function ($q) use ($user) {
				$q->where('created_by', $user->_id);
			})->orderBy('updated_at', 'desc');
			$mailCount = $mails->count();
			break;
		case 'delete':
			$recivedMails = MailTxn::where('mail_receive_id', $user->_id)->where('parent_id', '')->pluck('_id')->toArray();
			$sendMails = MailTxn::where('mail_sender_id', $user->_id)->where('parent_id', '')->pluck('_id')->toArray();

			$mails = MailTxn::whereIn('_id', array_unique(array_merge($recivedMails, $sendMails)))->whereIn('delete_users', [$user->_id])->orderBy('updated_at', 'desc');
			$mailCount = $mails->count();
			break;
		case 'starred':
			$recivedMails = MailTxn::where('mail_receive_id', $user->_id)->where('parent_id', '')->pluck('_id')->toArray();
			$sendMails = MailTxn::where('mail_sender_id', $user->_id)->where('parent_id', '')->pluck('_id')->toArray();

			$mails = MailTxn::whereIn('_id', array_unique(array_merge($recivedMails, $sendMails)))->whereNotIn('delete_users', [$user->_id])->whereIn('star_users', [$user->_id])->orderBy('updated_at', 'desc');
			$mailCount = $mails->count();

			/*$sendMails = MailTxn::where('mail_sender_id', $user->_id)->where('mail_status', 1)->whereNull('label_id')->where('parent_id', '')->where('is_star', 1)->pluck('_id')->toArray();
				$recivedMails = MailTxn::where(function ($q) use ($user) {
					$q->where('mail_receive_id', $user->_id);
				})->orWhere(function ($q) use ($user) {
					$q->where('mail_sender_id', $user->_id);
				})->whereNotNull('parent_id')->where('mail_status', 1)->whereNull('label_id')->groupBy('parent_id')->where('is_star', 1)->pluck('parent_id')->toArray();
			*/
			break;
		case 'label':
			$mails = MailTxn::whereHas('mailLabelUser', function ($q) use ($user) {
				$q->where('created_by', $user->_id);
			})->orderBy('updated_at', 'desc');
			$mailCount = $mails->count();
			break;
		default:
			// code...
			break;
		}

		/*$mails = MailTxn::where(function ($q) use ($request, $user) {
			if ($request->param == 'inbox') {
				$q->where('mail_receive_id', $user->_id);
				$q->where('mail_status', 1);
				$q->whereNull('label_id');
				$q->where('parent_id', '');

			} elseif ($request->param == 'sentbox') {
				$q->where('mail_sender_id', $user->_id);
				$q->where('mail_status', 1);
				$q->whereNull('label_id');
				$q->where('parent_id', '');
			} elseif ($request->param == 'delete') {
				$q->where('mail_sender_id', $user->_id);
				$q->orWhere('mail_receive_id', $user->_id);
				$q->where('mail_status', 0);
				$q->whereNull('label_id');
			} elseif ($request->param == 'starred') {
				$q->where('mail_sender_id', $user->_id);
				$q->orWhere('mail_receive_id', $user->_id);
				$q->where('is_star', 1);
			} elseif ($request->param == 'label') {
				$q->where('mail_sender_id', $user->_id);
				//$q->where('mail_status', 1);
				$q->orWhere('mail_receive_id', $user->_id);
				$q->where('label_id', $request->label_id);
			}
		})->orderBy('created_at', 'desc')->get();*/
		//return $mails;
		/*$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 50);
			$emailres = MailTxn::where('mail_sender_id', $user->id)->where('parent_id', 0)->with('children.children.children.children.children.children');
			$mails = $emailres->offset($offset)->limit(50)->get();
		*/
		//return $offset;
		//return $mails->skip($offset)->limit(10)->get();
		$labels = MailLabel::where('created_by', $user->_id)->get();
		//return $mails->skip($offset)->limit(10)->get();
		return response()->json(['status' => 200, 'data' => EmailsResource::collection($mails->skip($offset)->limit(10)->get()), 'labels' => $labels, 'count' => $mailCount, 'current_page' => ($offset / 10) + 1]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$user = auth('api')->user();
		$messages = [
		];
		$validator = Validator::make($request->all(), [
			'mailemail' => 'required',
			'mailbody' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages(), 'data' => $request->all()]);
		}
		try {
			//$requestEmails = !empty($request->company_email) ? array_slice(explode(',', $request->mailemail), 1) : explode(',', $request->mailemail);
			$requestEmails = explode(',', $request->mailemail);

			$recipientrs = User::whereIn('email', $requestEmails);
			if ($recipientrs->count() >= 1 || $request->company_email) {
				$data = ['text' => $request->mailbody, 'subject' => $request->mailsubject];
				$view = 'emails.send-mail';
				$subject = $request->mailsubject;
				if ($recipientrs->count() >= 1) {
					$recipients = $recipientrs->get();
					foreach ($recipients as $recipient) {
						$template = MailTxn::create([
							'mail_receive_id' => $recipient->_id,
							'mail_sender_id' => $user->_id,
							'mail_subject' => $request->mailsubject,
							'mail_body' => $request->mailbody,
							'mail_status' => 1,
							'parent_id' => $request->reply_id ? $request->reply_id : '',
						]);
						if (!empty($request->is_resume_trans) && $request->is_resume_trans == 1) {
							$template->is_resume_trans = 1;
							$template->save();
						}
						if (!empty($request->reply_id)) {
							$parentMail = MailTxn::find($request->reply_id);
							$parentMail->updated_at = $template->created_at;
							$parentMail->save();
						}
						if ($request->hasFile('attached_file')) {
							$attFile = [];
							$attachedFiles = [];
							foreach ($request->file('attached_file') as $key => $file) {
								/*$filename = $file->getClientOriginalName();
									$newfile = time() . '_' . $filename;
									$attFile[] = $newfile;
									$filePath = '/upload_files/email-files/' . $newfile;
									$attachedFiles[] = $filePath;
								*/
								$new_name = $file->storePublicly(
									'upload_files/email-files'
								);

								if ($new_name) {
									$attachedFiles[] = $new_name;
								}

							}

							$template->attached_file = $attachedFiles;
							$template->save();
						}
					}
				}
				/*if (!empty($request->company_email)) {
					if ($empProfile = EmployerProfileTxn::where('company_email', $request->company_email)->first()) {
						$data = ['text' => $request->mailbody, 'subject' => $request->mailsubject];
						$view = 'emails.send-mail';
						$subject = $request->mailsubject;
						$template = MailTxn::create([
							'mail_receive_id' => $empProfile->user->_id,
							'mail_sender_id' => $user->_id,
							'mail_subject' => $request->mailsubject,
							'mail_body' => $request->mailbody,
							'mail_status' => 1,
							'parent_id' => '',
						]);
						if ($request->hasFile('attached_file')) {
							$attFile = [];
							$attachedFiles = [];
							foreach ($request->file('attached_file') as $key => $file) {
								$filename = $file->getClientOriginalName();
								$newfile = time() . '_' . $filename;
								$attFile[] = $newfile;
								$filePath = '/upload_files/email-files/' . $newfile;
								$attachedFiles[] = $filePath;
								Storage::disk('s3')->put($filePath, file_get_contents($file));
							}

							$template->attached_file = $attachedFiles;
							$template->save();
						}

					}
				}*/

				return response()->json(['status' => 200, 'status_text' => 'Successfully submitted.', 'data' => 'ok']);
			} else {

				return response()->json(['status' => 422, 'status_text' => 'Email id did not match!']);
			}
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$template = EmployerEmailTemplateTxn::find($id);
		$other_mst_data = AllOtherMasterMst::all();
		$languages = LanguageMst::all();
		$cities = CityMst::all();
		$countries = CountryMst::all();
		return response()->json(['status' => 200, 'data' => $template, 'oth_mst_data' => $other_mst_data, 'cities' => $cities, 'countries' => $countries, 'languages' => $languages]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$messages = [
		];
		$validator = Validator::make($request->all(), [
			'template_name' => 'required',
			'cv_received_email' => 'required',
			'subject' => 'required',
			'about_job' => 'required',
			'skill' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {
			$requestdata = $request->all();
			unset($requestdata['language_pref']);
			$languages = !empty($request->language_pref) ? implode(',', $request->language_pref) : '';
			$template = EmployerEmailTemplateTxn::find($id);
			$template->fill($requestdata + ['updated_by' => auth('api')->user()->_id, 'language_pref' => $languages]);
			$template->save();
			return response()->json(['status' => 200, 'data' => $template]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$template = EmployerEmailTemplateTxn::find($id);
			$template->delete();
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function deleteAll(Request $request) {
		try {
			$user = auth('api')->user();
			$ids = $request->all();
			$mails = MailTxn::whereIn('_id', $ids)->get();
			foreach ($mails as $mail) {
				$mail->mailDeleteUser()->attach($user->_id);
			}
			//MailTxn::whereIn('_id', $ids)->update(['mail_status' => 0]);
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function deleteSingle($id) {
		try {
			MailTxn::where('_id', $id)->update(['mail_status' => 0]);
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function restoreMail(Request $request) {
		try {
			$ids = $request->all();
			$user = auth('api')->user();
			$mails = MailTxn::whereIn('_id', $ids)->get();
			foreach ($mails as $mail) {
				$mail->mailDeleteUser()->detach($user->_id);
			}
			return response()->json(['status' => 200, 'status_text' => 'Successfully restore']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function markStar($id) {
		$user = auth('api')->user();
		$mail = MailTxn::find($id);
		$mail->mailStaredUser()->attach($user->_id);
		return response()->json(['status' => 200, 'data' => $mail, 'status_text' => 'Successfully updated']);
	}

	public function getEmployees() {
		$user = auth('api')->user();
		$employees = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employee');
		})->get(['_id', 'email']);
		$labels = MailLabel::where('created_by', $user->_id)->get();
		return response()->json(['status' => 200, 'data' => $employees, 'labels' => $labels]);
	}

	public function getEmployer() {
		$user = auth('api')->user();
		$employees = User::whereHas('roles', function ($q) {
			$q->where('slug', 'employer');
		})->get();
		$labels = MailLabel::where('created_by', $user->_id)->get();
		return response()->json(['status' => 200, 'data' => $employees, 'labels' => $labels]);
	}

	public function createLabel(Request $request) {
		$user = auth('api')->user();
		$label = MailLabel::create($request->all() + ['created_by' => $user->_id]);
		return response()->json(['status' => 200, 'data' => $label]);
	}

	public function deletelabel($id) {
		$label = MailLabel::find($id);
		$label->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}

	public function sendToLabel(Request $request) {
		$mails = MailTxn::whereIn('_id', $request->mail_ids)->get();
		foreach ($mails as $mail) {
			$mail->mailLabelUser()->attach($request->label_id);
		}
		return response()->json(['status' => 200]);
		/*$mails = MailTxn::where('label_id', $request->label_id)->orderBy('created_at', 'desc')->with('children.children.children.children.children.children')->get();
		return response()->json(['status' => 200, 'data' => EmailsResource::collection($mails)]);*/
	}
}
