<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\MyAlert as MyAlertResource;
use App\Models\AllOtherMasterMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\DepartmentMst;
use App\Models\IndustryMst;
use App\Models\MyAlertTxn;
use App\Models\SkillMst;
use Illuminate\Http\Request;
use Validator;

class MyAlertController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$user = auth('api')->user();
		$countries = CountryMst::all();
		$countrie_array = collect(CountryMst::all())->groupBy('')->toArray();
		$cities = CityMst::all();
		$cities_array = collect(CityMst::all())->groupBy('')->toArray();
		$industries = collect(IndustryMst::all())->groupBy('attr_name')->toArray();
		$all_industries = IndustryMst::all();
		$department = DepartmentMst::whereNotNull('parent_id')->get();
		$all_department = collect(DepartmentResource::collection($department))->groupBy('parent_name')->toArray();
		$skills = SkillMst::all();
		$currency_data = AllOtherMasterMst::where('entity', 'currency')->get();
		$alertdata = MyAlertTxn::where('user_id', $user->_id)->get();
		$alerts = MyAlertResource::collection($alertdata);
		return response()->json(['status' => 200, 'data' => compact('alerts', 'countries', 'countrie_array', 'cities', 'cities_array', 'industries', 'all_industries', 'department', 'all_department', 'skills', 'currency_data')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			//'salary' => 'numeric',
			'email_id' => 'required | email',
			'name_of_alert' => 'required',
			'keyword' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		//return $request->all();

		try {
			$user = auth('api')->user();
			if (MyAlertTxn::where('user_id', $user->_id)->count() == 5) {
				return response()->json(['status' => 501, 'status_text' => 'You cannot create job alert morethan 5!']);
			}
			$alert = MyAlertTxn::create($request->all() + ['user_id' => $user->_id]);
			if (!empty($request->country_id)) {
				$alert->country()->attach($request->country_id);
			}
			if (!empty($request->city_id)) {
				$alert->city()->attach($request->city_id);
			}
			if (!empty($request->department_id)) {
				$alert->department()->attach($request->department_id);
			}
			if (!empty($request->industry_id)) {
				$alert->industry()->attach($request->industry_id);
			}

			return response()->json(['status' => 200, 'status_text' => 'Successfully created alert', 'data' => new MyAlertResource($alert)]);
		} catch (\Excetion $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'salary' => 'numeric',
			'email_id' => 'required | email',
			'name_of_alert' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$user = auth('api')->user();
			$alert = MyAlertTxn::find($id);
			$alert->fill($request->all());
			if (!empty($request->country_id)) {
				$alert->country()->detach();
				$alert->country()->attach($request->country_id);
			}
			if (!empty($request->city_id)) {
				$alert->city()->detach();
				$alert->city()->attach($request->city_id);
			}
			if (!empty($request->department_id)) {
				$alert->department()->detach();
				$alert->department()->attach($request->department_id);
			}
			if (!empty($request->industry_id)) {
				$alert->industry()->detach();
				$alert->industry()->attach($request->industry_id);
			}
			$alert->save();

			return response()->json(['status' => 200, 'data' => new MyAlertResource($alert), 'status_text' => 'Successfully update alert']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$alert = MyAlertTxn::find($id);
			$alert->delete();
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted alert']);
		} catch (\Exception $e) {

		}
	}
}
