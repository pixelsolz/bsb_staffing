<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\Models\CityMst;
use App\Models\CollegeDetail;
use App\Models\CountryMst;
use App\Models\Course;
use App\Models\User;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;
//use Tymon\JWTAuth\Facades\JWTAuth;

//use JWTAuth;
//use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class OtherUserController extends Controller {

	public function agencyProfileUpdate(Request $request) {
		//return $request->file('registry_certificate');
		$user = auth('api')->user();
		$messages = [
			'comp_name.required' => 'Company name is required',
			'contact_person_email.required' => 'Contact person email is required',
			'contact_person_email.unique' => 'Email is exist!',
			'contact_person_phone.required' => 'Contact person phone is required',
			'contact_person_name.required' => 'Contact person first name is required',
			'contact_person_lastname.required' => 'Contact person last name is required',
			'dob_date.required' => 'Date is required',
			'dob_month.required' => 'Month is required',
			'dob_year.required' => 'Year is required',
			'gender.required' => 'Gender is required',
			'country_id.required' => 'Country is required',
			'city_id.required' => 'City is required',
			'address.required' => 'Address is required',
			'contact_person_designation.required' => 'Designation is required',
			'registry_certificate.mimes' => 'The registration certificate must be an image.',
			'govt_id.mimes' => 'The govt id must be an image',
		];
		$validator = Validator::make($request->all(), [
			'comp_name' => 'required',
			'contact_person_name' => 'required',
			'contact_person_lastname' => 'required',
			'contact_person_email' => 'required|email|unique:users,email' . $user->_id,
			'contact_person_phone' => 'required|min:8|max:20|regex:/^(\\+)?(\\d+)$/',
			'dob_date' => 'required',
			'dob_month' => 'required',
			'dob_year' => 'required',
			'gender' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'address' => 'required',
			'contact_person_designation' => 'required',

			'registry_certificate' => $request->hasFile('registry_certificate') ? 'required|mimes:png,jpeg,jpg,gif,svg|image' : '',
			'govt_id' => $request->hasFile('govt_id') ? 'required|mimes:png,jpeg,jpg,gif,svg|image' : '',
			'password' => !empty($request->password) ? 'confirmed|min:6' : '',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$user = $user->fill($request->only(['comp_name', 'contact_person_email', 'contact_person_phone']));
			$user->save();
			$agency = $user->agencyUser->fill($request->only(['comp_name', 'comp_website', 'country_id', 'city_id', 'address', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'dob_date', 'dob_month', 'dob_year', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_portal_address'] + ['user_id' => $user->_id]));
			$agency->save();

			if ($request->hasFile('registry_certificate')) {
				$file = $request->file('registry_certificate');
				$new_name = $request->file('registry_certificate')->storePublicly(
					'upload_files/other_users'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
				}

				$agency->registry_certificate = $filename;
				$agency->save();
			}
			if ($request->hasFile('govt_id')) {
				$file = $request->file('govt_id');
				$new_name = $request->file('govt_id')->storePublicly(
					'upload_files/other_users'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
				}
				$agency->govt_id = $filename;
				$agency->save();
			}

			if (!empty($request->password)) {
				$user->password = bcrypt($request->password);
				$user->save();
			}

			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user())]);
		} catch (\Exception $e) {
			//DB::rollback();
			//$session->abortTransaction();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function agencyProfileImageUpdate(Request $request) {
		if ($request->hasFile('profile_image')) {
			$user = auth('api')->user();
			$agencyDetail = $user->agencyUser;
			$file = $request->file('profile_image');
			$new_name = $request->file('profile_image')->storePublicly(
				'upload_files/profile_picture'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
			}
			$agencyDetail->profile_image = $filename;
			$agencyDetail->save();
			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user())]);
		}
	}

	public function franchiseProfileUpdate(Request $request) {
		//return $request->file('registry_certificate');
		$user = auth('api')->user();
		$messages = [
			'comp_name.required' => 'Company name is required',
			'contact_person_email.required' => 'Contact person email is required',
			'contact_person_email.unique' => 'Email is exist!',
			'contact_person_phone.required' => 'Contact person phone is required',
			'contact_person_name.required' => 'Contact person first name is required',
			'contact_person_lastname.required' => 'Contact person last name is required',
			'dob_date.required' => 'Date is required',
			'dob_month.required' => 'Month is required',
			'dob_year.required' => 'Year is required',
			'gender.required' => 'Gender is required',
			'country_id.required' => 'Country is required',
			'city_id.required' => 'City is required',
			'address.required' => 'Address is required',
			'contact_person_designation.required' => 'Designation is required',
			'registry_certificate.mimes' => 'The registration certificate must be an image.',
			'govt_id.mimes' => 'The govt id must be an image',
		];
		$validator = Validator::make($request->all(), [
			'comp_name' => 'required',
			'contact_person_name' => 'required',
			'contact_person_lastname' => 'required',
			'contact_person_email' => 'required|email|unique:users,email' . $user->_id,
			'contact_person_phone' => 'required|min:8|max:20|regex:/^(\\+)?(\\d+)$/',
			'dob_date' => 'required',
			'dob_month' => 'required',
			'dob_year' => 'required',
			'gender' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'address' => 'required',
			'contact_person_designation' => 'required',

			'registry_certificate' => $request->hasFile('registry_certificate') ? 'required|mimes:png,jpeg,jpg,gif,svg|image' : '',
			'govt_id' => $request->hasFile('govt_id') ? 'required|mimes:png,jpeg,jpg,gif,svg|image' : '',
			'password' => !empty($request->password) ? 'confirmed|min:6' : '',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$user = $user->fill($request->only(['comp_name', 'contact_person_email', 'contact_person_phone']));
			$user->save();
			$franchise = $user->franchiseUser->fill($request->only(['comp_name', 'country_id', 'city_id', 'address', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'dob_date', 'dob_month', 'dob_year', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_portal_address'] + ['user_id' => $user->_id]));
			$franchise->save();

			if ($request->hasFile('registry_certificate')) {
				$file = $request->file('registry_certificate');
				$new_name = $request->file('registry_certificate')->storePublicly(
					'upload_files/other_users'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
				}
				$franchise->registry_certificate = $filename;
				$franchise->save();
			}
			if ($request->hasFile('govt_id')) {
				$file = $request->file('govt_id');
				$new_name = $request->file('govt_id')->storePublicly(
					'upload_files/other_users'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
				}
				$franchise->govt_id = $filename;
				$franchise->save();
			}

			if (!empty($request->password)) {
				$user->password = bcrypt($request->password);
				$user->save();
			}

			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user())]);
		} catch (\Exception $e) {
			//DB::rollback();
			//$session->abortTransaction();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function franchiseProfileImageUpdate(Request $request) {
		if ($request->hasFile('profile_image')) {
			$user = auth('api')->user();
			$franchiseDetail = $user->franchiseUser;
			$file = $request->file('profile_image');
			$new_name = $request->file('profile_image')->storePublicly(
				'upload_files/profile_picture'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
			}
			$franchiseDetail->profile_image = $filename;
			$franchiseDetail->save();
			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user())]);
		}
	}

	public function collegeProfileUpdate(Request $request) {
		$user = auth('api')->user();
		$messages = [
			'name.required' => 'Name is required',
			'address.required' => 'Address is required',
			'contact_person_email.required' => 'Contact person email is required',
			'contact_person_email.unique' => 'Email is exist!',
			'contact_person_name.required' => 'Contact person first name is required',
			'contact_person_lastname.required' => 'Contact person last name is required',
			'contact_person_phone.required' => 'Contact Person phone is required',
			'dob_date.required' => 'Date is required',
			'dob_month.required' => 'Month is required',
			'dob_year.required' => 'Year is required',
			'gender.required' => 'Gender is required',
			'country_id.required' => 'Country is required',
			'city_id.required' => 'City is required',
			'total_student.required' => 'Total Student is required',
			'total_student_place_per_year.required' => 'Total student place per year required',
			'contact_person_designation.required' => 'Contact person designation required',
			//'courses.required' => 'Courses is required',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'address' => 'required',
			'courses' => 'required',
			'contact_person_email' => 'required|email|unique:users,email' . $user->_id,
			'contact_person_name' => 'required',
			'contact_person_lastname' => 'required',
			'contact_person_phone' => 'required|min:8|max:20|regex:/^(\\+)?(\\d+)$/',
			'dob_date' => 'required',
			'dob_month' => 'required',
			'dob_year' => 'required',
			'gender' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'total_student' => 'required',
			'total_student_place_per_year' => 'required',
			'contact_person_designation' => 'required',
			'password' => !empty($request->password) ? 'confirmed|min:6' : '',

		], $messages);
		if ($validator->fails()) {

			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {

			$user = $user->fill($request->only(['user_name', 'email', 'phone_no'] + ['user_location' => ['type' => 'Point', 'coordinates' => [(float) $request['lat'], (float) $request['lng']]]]));
			$user->save();

			$collegeDetail = $user->collegeUser->fill($request->only(['name', 'website', 'country_id', 'city_id', 'address', 'total_student', 'total_student_place_per_year', 'about_college', 'special_note_employer', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'dob_date', 'dob_month', 'dob_year', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_postal_address'] + ['user_id' => $user->_id]));
			$collegeDetail->save();

			if ($request->input('gallery_length') > 0) {
				$images_name = [];
				for ($i = 0; $i < $request->input('gallery_length'); $i++) {
					$file = $request->file('gallery_' . $i);
					$name = $request->name . $i . '_' . time() . '.' . $file->getClientOriginalExtension();
					array_push($images_name, $name);
					/*$destinationPath = public_path('/upload_files/colleges/gallery_images');
					$file->move($destinationPath, $name);*/
					//$destinationPath = public_path('/upload_files/colleges/gallery_images/' . $name);
					$image_resize = Image::make($file->getRealPath());
					$image_resize->resize(370, 420);
					//$image_resize->save($destinationPath);

					$thumb_resource = $image_resize->stream();
					$thumbPath = '/upload_files/colleges/gallery_images/' . $name;
					Storage::disk('s3')->put($thumbPath, $thumb_resource->__toString(), 'public');

				}
				$collegeDetail->gallery_images = implode(',', $images_name);
				$collegeDetail->save();
			}

			if ($request->hasFile('logo')) {
				$file = $request->file('logo');
				$name = $request->name . 'logo_' . time() . '.' . $file->getClientOriginalExtension();

				//$destinationPath = public_path('/upload_files/colleges');
				//$destinationPath = public_path('/upload_files/colleges/' . $name);

				$image_resize = Image::make($file->getRealPath());
				$image_resize->resize(150, 66);
				//$image_resize->save($destinationPath);
				//$file->move($destinationPath, $name);
				$thumb_resource = $image_resize->stream();
				$thumbPath = '/upload_files/colleges/' . $name;
				Storage::disk('s3')->put($thumbPath, $thumb_resource->__toString(), 'public');
				$collegeDetail->logo = $name;
				$collegeDetail->save();
			}

			if (!empty($request->password)) {
				$user->password = bcrypt($request->password);
				$user->save();
			}
			$user->collegeCourses()->detach();
			$user->collegeCourses()->attach(explode(',', $request->input('courses')));

			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user()), 'status_text' => 'Successfully update']);

		} catch (\Exceptions $e) {
			return response()->json(['status_text' => 'something is wrong', 'status' => 500]);
		}
	}
	public function getAutocompleteSearch(Request $request) {
		//dd($request->all());
		$type = $request->type;
		$keyword = $request->keyword;
		$data = '';
		switch ($type) {
		case 'CLG_INSTITUTE_NAME':
			$college_names = CollegeDetail::where('name', 'like', $keyword . '%')->get();
			$clgNameCollect = collect($college_names)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $clgNameCollect->all();
			break;
		case 'COUNTRY_WISE':
			$countries = CountryMst::select('name')->where('name', 'like', $keyword . '%')->get();
			$countryData = collect($countries)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $countryData->all();

			break;
		case 'CITY_WISE':
			$cities = CityMst::select('name')->where('name', 'like', $keyword . '%')->get();
			$cityData = collect($cities)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $cityData->all();
			break;
		case 'COURSE_WISE':
			$courses = Course::select('name')->where('name', 'like', $keyword . '%')->get();
			$coursesData = collect($courses)->map(function ($item, $key) {
				return [
					'id' => $item['_id'],
					'name' => $item['name'],
				];
			});
			$data = $coursesData->all();

		default:
			// code...
			break;
		}

		return response()->json(['status' => 200, 'data' => $data, 'type' => $type]);
	}
}
