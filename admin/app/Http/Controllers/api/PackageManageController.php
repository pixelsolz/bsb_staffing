<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PaymentHistory as PaymentHistoryResource;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\PaymentConfig;
use App\Models\PaymentHistoryTxn;
use App\Models\ServicesMst;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageManageController extends Controller {

	public function __construct() {
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

	}

	public function getPackageList(Request $request) {
		//dd($request->all());
		if ($request->service_type != '') {
			$packages = ServicesMst::where('service_type', $request->service_type)->where('needs_broadcast', "1")->orderBy('order_of_results', 'asc')->get();
		} else {
			$packages = ServicesMst::where('needs_broadcast', "1")->get();
		}
		//echo "<pre>";print_r($packages);
		return response()->json(['status' => 200, 'packages' => $packages]);
	}
	public function getPackageById($id) {
		//echo $id;die;
		$package = ServicesMst::find($id);
		//echo "<pre>";print_r($packages);
		return response()->json(['status' => 200, 'package' => $package]);
	}

	public function payment(Request $request) {
		//return $request->all();
		$service = ServicesMst::find($request->id);
		//echo "<pre>";print_r($service);die;
		//\DB::beginTransaction();
		//echo Carbon::now()->addMonths(1);die;
		try {
			$payment_history = PaymentHistoryTxn::create(['user_id' => auth('api')->user()->_id, 'service_id' => $service->_id, 'payment_made_on' => Carbon::now(), 'payment_amount' => $service->fees, 'payment_mode	' => 'offline', 'remarks' => $request->remarks, 'created_by' => auth('api')->user()->_id]);

			$service_opted = EmployerEmployeeOptedServicesTxn::Create([
				//Add unique field combo to match here
				//For example, perhaps you only want one entry per user:
				'user_id' => auth('api')->user()->_id,
				'is_employee' => 'N',
				'is_employer' => 'Y',
				'service_id' => $service->id,
				'limit_count' => (int) $service->quantity,
				'remaining_count' => (int) $service->quantity,
				'total_used' => 0,
				'status' => 1,
				'service_expiry' => Carbon::now()->addMonths($service->service_life),
				'service_opted_on' => Carbon::now(),
				'current_status' => 'Y',
				'created_by' => auth('api')->user()->_id,

			]);
			return response()->json(['status' => 200, 'status_text' => 'Successfully Payment']);
			//\DB::commit();
		} catch (\Exception $e) {
			//\DB::rollback();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
			// something went wrong
		}

	}

	public function getPaymentHistory() {
		$payment_history = PaymentHistoryTxn::where('user_id', auth('api')->user()->_id)->get();

		return response()->json(['status' => 200, 'payment_history' => PaymentHistoryResource::collection($payment_history)]);
	}

	public function checkService(Request $request) {
		//return $date = new \MongoDB\BSON\UTCDateTime(new \DateTime(Carbon::now()->toDateTimeString()));
		$service_type = $request->type;
		$payment_config = PaymentConfig::first();
		if ($payment_config && $payment_config->$service_type == 1) {
			$service_ids = ServicesMst::where('service_type', $service_type)->pluck('_id');
			//return $service;
			$current_date = Carbon::now();
			//return $current_date;
			$check_active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0);
			$check_active_service_count = $check_active_service->count();
			$checkService = $check_active_service->get();
			//$check_active_service = EmployerEmployeeOptedServicesTxn::where('service_expiry','>=',$current_date)->get();
			//echo $check_active_service_count;
			return response()->json(['status' => 200, 'check_active_service_count' => $check_active_service_count, 'checkService' => $checkService]);
		} else {
			return response()->json(['status' => 200, 'check_active_service_count' => 1]);
		}
	}

}
