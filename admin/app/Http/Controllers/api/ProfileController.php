<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeCv as EmployeeCvResource;
use App\Http\Resources\EmployeeCv as EmployeeDetailsResource;
use App\Http\Resources\EmployeeCvViewHistory as EmployeeCvViewResource;
use App\Http\Resources\EmployeeEducation as EmployeeEducationResource;
use App\Http\Resources\EmployeementHistory as EmployeementHistoryResource;
use App\Http\Resources\EmployeePhoto as EmployeePhotoResource;
use App\Http\Resources\EmployeePreference;
use App\Http\Resources\EmployeeProfile as EmployeeProfileResource;
use App\Http\Resources\User as UserResource;
use App\Models\EmployeeCertification;
use App\Models\EmployeeCvTxn;
use App\Models\EmployeeCvViewHistory;
use App\Models\EmployeeEducation;
use App\Models\EmployeeLanguages;
use App\Models\EmployeementHistory;
use App\Models\EmployeePhoto;
use App\Models\EmployeePrefTxn;
use App\Models\EmployeeProfileLikeTxn;
use App\Models\EmployeeSkill;
use App\Models\EmployeeVideoTxn;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\ExperienceMst;
use App\Models\IndustryMst;
use App\Models\JobRoleMst;
use App\Models\PaymentConfig;
use App\Models\ServicesMst;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class ProfileController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$user = auth('api')->user();
		$profile = new EmployeeProfileResource($user->employeeUser);
		$employment_history = EmployeementHistoryResource::collection($user->employeeEmployementHistory);
		$employee_education = EmployeeEducationResource::collection($user->employeeEducation);
		$employee_skill = $user->employeeSkill;
		$employee_certification = $user->employeeCertification;
		$employee_language = $user->employeeLanguage;
		$employee_videos = $user->employeeVideos;
		$employee_pref = $user->employeePrefTxn ? new EmployeePreference($user->employeePrefTxn) : '';

		$employee_photos = EmployeePhotoResource::collection($user->employeePhotos);

		//Start count Profile Strength
		$count_profle_strength = 10;
		if ($profile->profile_picture != '') {
			$count_profle_strength += 9;
		}
		if (!empty($user->employeeCvTxn)) {
			$count_profle_strength += 9;
		}
		//For Personal Information
		if (!empty($profile->date_of_birth)) {
			$count_profle_strength += 9;
		}

		if (!empty($user->employeePrefTxn)) {
			$count_profle_strength += 9;
		}
		// Summary of your profile
		if (!empty($profile->profile_title)) {
			$count_profle_strength += 9;
		}
		if (!empty($user->employeeEmployementHistory) && count($user->employeeEmployementHistory) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($user->employeeEducation) && count($user->employeeEducation) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($user->employeeSkill) && count($user->employeeSkill) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($user->employeeCertification) && count($user->employeeCertification) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($user->employeeLanguage) && count($user->employeeLanguage) != 0) {
			$count_profle_strength += 9;
		}
		//return $count_profle_strength;
		//End count Profile Strength

		$employer_profile_like_count = $user->employerCvLike()->count();
		return response()->json(['status' => 200, 'data' => compact('profile', 'employment_history', 'employee_education', 'employee_skill', 'employee_certification', 'employee_language', 'employee_pref', 'employee_videos', 'employee_photos', 'count_profle_strength', 'employer_profile_like_count')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//return $request->input('keyForm');
		$user = auth('api')->user();

		//DB::beginTransaction();
		try {
			if (!empty($request->input('profileForm'))) {
				$profile = $user->employeeUser;
				$profile->fill($request->input('profileForm'));
				$user->fill($request->input('profileForm'));
				$user->emp_name = $request->input('profileForm')['first_name'] . ' ' . $request->input('profileForm')['last_name'];
				$user->save();
				$profile->save();
			} elseif (!empty($request->input('personal_infoForm'))) {
				$profile = $user->employeeUser;
				$profile->fill($request->input('personal_infoForm'));
				$profile->save();
			} elseif (!empty($request->input('desiredJobForm'))) {
				$prefTxn = $user->employeePrefTxn;
				$desiredCollect = collect($request->input('desiredJobForm'));
				if ($prefTxn) {
					$prefTxn->fill($desiredCollect->except(['city_id', 'country_id', 'department_id', 'industry_id', 'shift_type'])->all());
					$prefTxn->save();
					$prefTxn->countries()->detach();
					$prefTxn->cities()->detach();
					$prefTxn->industries()->detach();
					//$prefTxn->departments()->detach();
					$prefTxn->shiftTypes()->detach();

					$prefTxn->countries()->attach($request->input('desiredJobForm')['country_id']);
					$prefTxn->cities()->attach($request->input('desiredJobForm')['city_id']);
					$prefTxn->industries()->attach($request->input('desiredJobForm')['industry_id']);
					//$prefTxn->departments()->attach($request->input('desiredJobForm')['department_id']);
					$prefTxn->shiftTypes()->attach($request->input('desiredJobForm')['shift_type']);
				} else {
					$newempPref = EmployeePrefTxn::create($desiredCollect->except(['city_id', 'country_id', 'department_id', 'industry_id', 'shift_type'])->all() + ['employee_id' => $user->_id]);
					$newempPref->countries()->attach($request->input('desiredJobForm')['country_id']);
					$newempPref->cities()->attach($request->input('desiredJobForm')['city_id']);
					$newempPref->industries()->attach($request->input('desiredJobForm')['industry_id']);
					//$newempPref->departments()->attach($request->input('desiredJobForm')['department_id']);
					$newempPref->shiftTypes()->attach($request->input('desiredJobForm')['shift_type']);
				}

			} elseif (!empty($request->input('summaryForm'))) {
				$profile = $user->employeeUser;
				$profile->fill($request->input('summaryForm'));
				if (!empty($request->input('summaryForm')['total_experience_yr'])) {
					$profile->total_experience_yr_value = (int) ExperienceMst::find($request->input('summaryForm')['total_experience_yr'])->value;
				}
				$profile->save();
			} elseif (!empty($request->input('employeementHisForm'))) {
				EmployeementHistory::create($request->input('employeementHisForm') + ['user_id' => $user->_id]);
			} elseif (!empty($request->input('educationQualifyForm'))) {
				EmployeeEducation::create($request->input('educationQualifyForm') + ['user_id' => $user->_id]);
			} elseif (!empty($request->input('keyForm'))) {
				EmployeeSkill::where('user_id', $user->_id)->delete();
				foreach ($request->input('keyForm')['title'] as $value) {
					EmployeeSkill::create(['user_id' => $user->_id, 'title' => strtolower($value)]);
				}

			} elseif (!empty($request->input('certificationForm'))) {
				EmployeeCertification::create($request->input('certificationForm') + ['user_id' => $user->_id]);
			} elseif (!empty($request->input('editLangForm'))) {
				EmployeeLanguages::create($request->input('editLangForm') + ['user_id' => $user->_id]);
			}
			//DB::commit();

			$user->profile_update = Carbon::now()->format('Y-m-d H:i');
			$user->save();

			$profile = new EmployeeProfileResource($user->employeeUser);
			$employment_history = $user->employeeEmployementHistory;
			$employee_education = $user->employeeEducation;
			$employee_skill = $user->employeeSkill;
			$employee_certification = $user->employeeCertification;
			$employee_language = $user->employeeLanguage;

			$employee_pref = EmployeePrefTxn::where('employee_id', $user->_id)->first() ? new EmployeePreference(EmployeePrefTxn::where('employee_id', $user->_id)->first()) : '';
			$industries = collect(IndustryMst::all())->groupBy('attr_name')->toArray();
			$job_roles = JobRoleMst::all();
			return response()->json(['status' => 200, 'status_text' => 'Successfully updated', 'data' => compact('profile', 'employment_history', 'employee_education', 'employee_skill', 'employee_certification', 'employee_language', 'employee_pref', 'industries', 'job_roles')]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function storeFormdata(Request $request) {
		//DB::beginTransaction();
		try {
			$user = auth('api')->user();
			$returnData = '';
			if ($request->input('form_type') == 'employeement_history') {
				$existPreviousePresent = '';
				if (!empty($request->input('is_present_company'))) {
					$existPreviousePresent = EmployeementHistory::where('user_id', $user->_id)->where('is_present_company', 1)->first();
				}
				$employeement = EmployeementHistory::create($request->except('company_logo', 'from_date', 'to_date') + ['user_id' => $user->_id]);
				if ($request->has('from_date')) {
					$fromDate = str_replace('/', '-', $request->input('from_date'));
					$employeement->from_date = Carbon::parse($fromDate)->format('Y-m-d');
					$employeement->save();
					if ($existPreviousePresent) {
						$existPreviousePresent->to_date = Carbon::parse($fromDate)->format('Y-m-d');
						$existPreviousePresent->is_present_company = '';
						$existPreviousePresent->save();
					}
				}
				if (!empty($request->input('to_date')) && $request->has('to_date')) {
					$toDate = str_replace('/', '-', $request->input('to_date'));
					$employeement->to_date = Carbon::parse($toDate)->format('Y-m-d');
					$employeement->save();
				}
				if ($request->hasFile('company_logo')) {
					$file = $request->file('company_logo');
					//$new_name = time() . $file->getClientOriginalName();
					$new_name = $request->file('company_logo')->storePublicly(
						'upload_files/images'
					);
					//$filePath = 'upload_files/images/' . $new_name;
					//Storage::disk('s3')->put($filePath, file_get_contents($file));
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/images' . '/', '', $new_name);
					}
					//$file->move(public_path('upload_files/images'), $new_name);
					$employeement->company_logo = $filename;
					$employeement->save();
				}

				$returnData = new EmployeementHistoryResource($employeement);
			} elseif ($request->input('form_type') == 'certification_form') {
				$employeeCertificate = EmployeeCertification::create($request->except('institute_logo') + ['user_id' => $user->_id]);
				if ($request->hasFile('institute_logo')) {
					$file = $request->file('institute_logo');
					/*$new_name = time() . $file->getClientOriginalName();
						$filePath = 'upload_files/images/' . $new_name;
					*/
					$new_name = $request->file('institute_logo')->storePublicly(
						'upload_files/images'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/images' . '/', '', $new_name);
					}
					$employeeCertificate->institute_logo = $filename;
					$employeeCertificate->save();
				}

			} elseif ($request->input('form_type') == 'education') {
				$education = EmployeeEducation::create($request->except('institute_logo', 'degree_id', 'degree_parent') + ['user_id' => $user->id, 'course_degree_id' => $request->degree_id]);
				if ($request->hasFile('institute_logo')) {
					$file = $request->file('institute_logo');
					/*$new_name = time() . $file->getClientOriginalName();
						$filePath = 'upload_files/images/' . $new_name;
					*/

					$new_name = $request->file('institute_logo')->storePublicly(
						'upload_files/images'
					);
					$filename = '';

					if ($new_name) {
						$filename = str_replace('upload_files/images' . '/', '', $new_name);
					}
					$education->institute_logo = $filename;
					$education->save();
				}
				$returnData = new EmployeeEducationResource($education);

			}

			$user->profile_update = Carbon::now()->format('Y-m-d H:i');
			$user->save();
			//DB::commit();
			return response()->json(['status' => 200, 'status_text' => 'Successfully added', 'data' => $returnData]);
		} catch (\Exception $e) {
			//DB::rollback();

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$user = User::find($id);
		$loginuser = auth('api')->user();

		$employer = $loginuser->employerUser;
		$employee = $user;
		$employee_photos = EmployeePhotoResource::collection($user->employeePhotos);
		$employee_videos = $user->employeeVideos;
		$employment_history = EmployeementHistoryResource::collection($user->employeeEmployementHistory);
		$employee_education = EmployeeEducationResource::collection($user->employeeEducation);
		$employee_skill = $user->employeeSkill;
		$employee_certification = $user->employeeCertification;
		$employee_language = $user->employeeLanguage;
		$limit = empty($request->limit) ? 12 : $request->limit;
		$similar_employees = User::where('_id', '!=', $id)->whereHas('employeeUser', function ($q) use ($employee) {
			$q->where('country_id', $employee->employeeUser->country_id);
			if (!empty($request->city_list)) {
				$q->where('city_id', $request->city_list);
			} else {
				$q->where('city_id', $employee->employeeUser->city_id);
			}

			$q->whereHas('totExprYr', function ($q) use ($employee) {
				//$q->where('name', '>=', ($employee->employeeUser->totExprYr->name - 1));
				//$q->where('name', '<=', ((string) $employee->employeeUser->totExprYr->name + 1));
			});

		})->whereHas('employeeSkill', function ($q) use ($employee) {

			$q->whereIn('title', $employee->employeeSkill()->pluck('title')->toArray());

		})->whereHas('employeePrefTxn', function ($q) use ($employee) {
			$q->where('min_salary', '>=', ((int) $employee->employeeUser->current_salary - 10000));
			$q->where('max_salary', '<=', ((int) $employee->employeeUser->current_salary + 10000));
		})->whereHas('employeeEducation', function ($q) use ($employee) {
			$q->whereIn('degree_id', $employee->employeeEducation()->pluck('degree_id')->toArray());
		})->orderBy('created_at', 'desc');

		if ($loginuser->isEmployer()) {
			$payment_config = PaymentConfig::first();
			if ($payment_config && $payment_config->premium_job_post == 1) {
				$service_ids = ServicesMst::where('service_type', 'cv_download_view')->pluck('_id');
				$current_date = Carbon::now();
				$active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0)->first();
				$remaining_count = $active_service->remaining_count;
				$active_service->remaining_count = $remaining_count - 1;
				$active_service->save();
			}
		}

		return response()->json(['status' => 200, 'data' => new EmployeeDetailsResource($user), 'similar_profile' => EmployeeCvResource::collection($similar_employees->offset(0)->limit($limit)->get()), 'employee_photos' => $employee_photos, 'employee_videos' => $employee_videos, 'employment_history' => $employment_history, 'employee_education' => $employee_education, 'employee_skill' => $employee_skill, 'employee_certification' => $employee_certification, 'employee_language' => $employee_language]);

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function uploadResume(Request $request) {
		//return "test";die;
		if ($request->hasFile('employee_resume')) {
			try {
				$user = auth('api')->user();
				$file = $request->file('employee_resume');
				//$new_name = time() . '_' . str_replace(' ', '_', $file->getClientOriginalName());
				$new_name = $request->file('employee_resume')->storePublicly(
					'upload_files/employee-resumes'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/employee-resumes' . '/', '', $new_name);
				}

				$employeeCvTxn = EmployeeCvTxn::create(['employee_id' => $user->_id, 'cv_path' => $filename]);
				$user->profile_update = Carbon::now()->format('Y-m-d H:i');
				$user->save();
				$profile = new EmployeeProfileResource($user->employeeUser);
				//return response()->json(['status' => 200, 'status_text' => 'Successfully upload resume', 'profile' => $profile, 'cv' => $employeeCvTxn, 'cv_path' => url('public/upload_files/employee-resumes/' . $employeeCvTxn->cv_path)]);
				return response()->json(['status' => 200, 'status_text' => 'Successfully upload resume', 'profile' => $profile, 'cv' => $employeeCvTxn, 'cv_path' => Storage::disk('s3')->url('/upload_files/employee-resumes/' . $employeeCvTxn->cv_path)]);

			} catch (\Exception $e) {
				return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
			}

		}
	}

	public function uploadPhoto(Request $request) {
		//echo "<pre>";print_r($request->file('photo'));
		$messages = [
			'description.required' => 'The about this photo field is required',
		];
		$validator = Validator::make($request->all(), [
			'photo.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
			//'description' => 'required',

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		$user = auth('api')->user();
		if ($request->hasFile('photo')) {
			// $file = $request->file('photo');
			// $new_name = time() . $file->getClientOriginalName();
			// $file->move(public_path('upload_files/images'), $new_name);
			// $emp_photo = EmployeePhoto::create(['user_id' => $user->_id, 'description' => $request->description, 'photo' => $new_name]);
			// return response()->json(['status' => 200, 'status_text' => 'Successfully upload photo', 'data' => new EmployeePhotoResource($emp_photo)]);

			foreach ($request->file('photo') as $key => $image) {
				$new_name = $image->storePublicly(
					'upload_files/images'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/images' . '/', '', $new_name);
				}
				$emp_photo = EmployeePhoto::create(['user_id' => $user->id, 'description' => $request->description, 'photo' => $filename]);
			}

			return response()->json(['status' => 200, 'status_text' => 'Successfully upload photo', 'data' => new EmployeePhotoResource($emp_photo)]);
		}
	}

	public function updatePhoto(Request $request) {
		try {
			$user = auth('api')->user();
			$photo = EmployeePhoto::find($request->id);
			$messages = [
				'description.required' => 'The about this photo field is required',
			];
			$validator = Validator::make($request->all(), [
				'photo' => $request->hasFile('photo') ? 'image|mimes:jpeg,png,jpg,gif,svg' : '',

			], $messages);
			if ($validator->fails()) {
				return response()->json(['status' => 422, 'error' => $validator->messages()]);
			}

			if ($request->hasFile('photo')) {
				$file = $request->file('photo');
				$new_name = $request->file('photo')->storePublicly(
					'upload_files/images'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/images' . '/', '', $new_name);
				}
				$photo->photo = $filename;
				$photo->save();
			}
			if (!empty($request->description)) {
				$photo->description = $request->description;
				$photo->save();
			}
			$user->profile_update = Carbon::now()->format('Y-m-d H:i');
			$user->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully update photo', 'data' => new EmployeePhotoResource($photo)]);
		} catch (\Exception $e) {
			return respnse()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function deletePhoto($id) {
		try {
			$user = auth('api')->user();
			$photo = EmployeePhoto::find($id);
			Storage::disk('s3')->delete('upload_files/images/' . $photo->photo);
			$photo->delete();
			$user->profile_update = Carbon::now()->format('Y-m-d H:i');
			$user->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted photo']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function uploadVideo(Request $request) {
		$user = auth('api')->user();
		try {
			$emp_video = EmployeeVideoTxn::create($request->all() + ['employee_id' => $user->_id]);
			return response()->json(['status' => 200, 'data' => $emp_video, 'status_text' => "Successfully saved"]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function deleteVideo($id) {
		try {
			$video = EmployeeVideoTxn::find($id);
			$video->delete();
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted video']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function deleteResume($id) {
		try {
			$user = auth('api')->user();
			$resume = EmployeeCvTxn::find($id);
			//return $resume->cv_path;die;
			//return $resume;die;cv_path

			Storage::disk('s3')->delete('/upload_files/employee-resumes/' . $resume->cv_path);
			$resume->delete();
			$user->profile_update = Carbon::now()->format('Y-m-d H:i');
			$user->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully deleted Resume']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function profileImageChange(Request $request) {
		if ($request->hasFile('profile_image')) {
			try {
				$user = auth('api')->user();
				$file = $request->file('profile_image');
				$new_name = $request->file('profile_image')->storePublicly(
					'upload_files/profile_picture'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
				}
				$profile = $user->employeeUser;
				$profile->profile_picture = $filename;
				$profile->save();
				$user->profile_update = Carbon::now()->format('Y-m-d H:i');
				$user->save();
				$profile_data = new EmployeeProfileResource($user->employeeUser);
				$user = new UserResource(auth('api')->user());
				return response()->json(['status' => 200, 'status_text' => 'Successfully upload image', 'data' => $profile_data, 'user' => $user]);

			} catch (\Exception $e) {
				return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
			}

		}
	}

	public function updateEducation(Request $request) {
		$user = auth('api')->user();
		$education = EmployeeEducation::find($request->id);
		$education->fill($request->except('id', 'degree_id', 'degree_parent') + ['course_degree_id' => $request->degree_id]);
		if ($request->hasFile('institute_logo')) {
			$file = $request->file('institute_logo');
			$new_name = $request->file('institute_logo')->storePublicly(
				'upload_files/images'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/images' . '/', '', $new_name);
			}
			$education->institute_logo = $filename;
		}
		$education->save();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully update', 'data' => new EmployeeEducationResource($education)]);
	}

	public function deleteEducation($id) {
		$user = auth('api')->user();
		$education = EmployeeEducation::find($id);
		$education->delete();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}

	public function updateCertification(Request $request) {
		$user = auth('api')->user();
		$certificate = EmployeeCertification::find($request->id);
		$certificate->fill($request->except(['id', 'institute_logo']));
		if ($request->hasFile('institute_logo')) {
			$file = $request->file('institute_logo');
			$new_name = $request->file('institute_logo')->storePublicly(
				'upload_files/images'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/images' . '/', '', $new_name);
			}
			$certificate->institute_logo = $filename;
		}
		$certificate->save();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully update', 'data' => $certificate]);
	}

	public function deleteCertification($id) {
		$user = auth('api')->user();
		$certificate = EmployeeCertification::find($id);
		$certificate->delete();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}

	public function updateEmpHistory(Request $request) {
		$user = auth('api')->user();
		$emp_history = EmployeementHistory::find($request->id);
		$emp_history->fill($request->except('id', 'company_logo'));
		if ($request->hasFile('company_logo')) {
			$file = $request->file('company_logo');
			$new_name = $request->file('company_logo')->storePublicly(
				'upload_files/images'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/images' . '/', '', $new_name);
			}
			$emp_history->company_logo = $filename;
		}
		$emp_history->save();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully update', 'data' => new EmployeementHistoryResource($emp_history)]);
	}

	public function deleteEmpHistory($id) {
		$user = auth('api')->user();
		$emp_history = EmployeementHistory::find($id);
		$emp_history->delete();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}
	public function likeProfile(Request $request) {
		//echo $request->id;
		try {
			$likecount = EmployeeProfileLikeTxn::where('employee_id', $request->id)->where('employer_id', auth('api')->user()->_id)->count();
			if ($likecount > 0) {
				return response()->json(['status' => 422, 'status_text' => 'You are already like this profile']);
			} else {
				EmployeeProfileLikeTxn::updateOrCreate(['employee_id' => $request->id, 'employer_id' => auth('api')->user()->_id, 'status' => '1']);
				return response()->json(['status' => 200, 'status_text' => 'Successfully Like Profile']);
			}

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function disLikeProfile(Request $request) {
		try {
			EmployeeProfileLikeTxn::where('employee_id', $request->id)->where('employer_id', auth('api')->user()->_id)->delete();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Dis Like Profile']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function updateLanguage(Request $request) {
		$user = auth('api')->user();
		$emp_language = EmployeeLanguages::find($request->id);
		$emp_language->fill($request->except('id'));
		$emp_language->save();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully update', 'data' => $emp_language]);
	}

	public function deleteLanguage($id) {
		$user = auth('api')->user();
		$emp_language = EmployeeLanguages::find($id);
		$emp_language->delete();
		$user->profile_update = Carbon::now()->format('Y-m-d H:i');
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}

	public function downloadResume($emp_id) {
		$employee = User::find($emp_id);
		/*$srcFile = public_path('upload_files/employee-resumes/' . $employee->employeeCvTxn->cv_path);
		$destFile = public_path('upload_files/employee-resumes/saved-pdf/test.pdf');*/
		$srcFile = Storage::disk('s3')->url('upload_files/employee-resumes/' . $employee->employeeCvTxn->cv_path);
		$destFile = Storage::disk('s3')->url('upload_files/employee-resumes/saved-pdf/test.pdf');

/*
//Set header to show as PDF
header("Content-Type: application/pdf");
header("Content-Disposition: inline; filename=" . 'test.pdf');

//Create a temporary file for Word
$temp = tmpfile();
fwrite($temp, $srcFile); //Write the data in the file
$uri = stream_get_meta_data($temp)["uri"]; //Get the location of the temp file

//Convert the docx file in to an PhpWord Object
$doc = \PhpOffice\PhpWord\IOFactory::load($uri);

//Set the PDF Engine Renderer Path. Many engines are supported (TCPDF, DOMPDF, ...).
$domPdfPath = base_path('vendor/dompdf/dompdf');
\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

//Create a writer, which converts the PhpWord Object into an PDF
$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($doc, 'PDF');

//Create again an temp file for the new generated PDF.
$pdf_temp = tmpfile();
$pdf_uri = stream_get_meta_data($pdf_temp)["uri"];

//Save the PDF to the path
$xmlWriter->save($pdf_uri);

//Now print the file from the temp path.
echo file_get_contents($pdf_uri);*/

		return response()->json(['status' => 200]);

	}

	public function getEmployeeProfileView(Request $request) {
		$user = auth('api')->user();
		$limit = empty($request->limit) ? 3 : (int) ($request->limit);
		$offset = empty($request->off_set) ? 0 : (int) ($request->offset);
		try {
			$employer_profiles = EmployeeCvViewHistory::where('employee_id', $user->_id)->where(function ($q) use ($request) {
				if (!empty($request->input('from_date')) && !empty($request->input('to_date'))) {
					$start_date = Carbon::createFromDate($request->input('from_date'));
					$end_date = Carbon::createFromDate($request->input('to_date'));
					$q->whereBetween('created_at', array($start_date, $end_date));
				}
			})->groupBy('employer_id')->orderBy('created_at', 'desc')->skip($offset)->limit($limit)->get(['employee_id', 'employer_id', 'remarks', 'created_at', 'updated_at']);
			$employer_profilesCount = EmployeeCvViewHistory::where('employee_id', $user->_id)->where(function ($q) use ($request) {
				if (!empty($request->input('from_date')) && !empty($request->input('to_date'))) {
					$start_date = Carbon::createFromDate($request->input('from_date'));
					$end_date = Carbon::createFromDate($request->input('to_date'));
					$q->whereBetween('created_at', array($start_date, $end_date));
				}
			})->groupBy('employer_id')->get();

			return response()->json(['status' => 200, 'employer_profiles' => EmployeeCvViewResource::collection($employer_profiles), 'total_count' => count($employer_profilesCount)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function updateUserlatLng(Request $request) {
		$user = User::find($request->user_id);
		$user->user_location = ['type' => 'Point', 'coordinates' => [(float) $request['lat'], (float) $request['lng']]];
		$user->save();
		return response()->json(['status' => 200, 'status_text' => 'updated']);
	}
}
