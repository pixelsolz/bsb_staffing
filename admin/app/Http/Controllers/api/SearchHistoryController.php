<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SearchHistory as SearchHistoryResource;
use App\Models\SearchHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchHistoryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$user = auth('api')->user();
		$limit = empty($request->limit) ? 12 : $request->limit;
		$searches = SearchHistory::where(function ($q) use ($request) {
			if ($request->has('search_type') && $request->search_type == 'saved') {
				$q->where('is_saved', 1);
			} else {
				$q->where('is_saved', '!=', 1);
			}
			if (!empty($request->from_date) && !empty($request->to_date)) {
				$start_date = Carbon::createFromDate($request->input('from_date'));
				$end_date = Carbon::createFromDate($request->input('to_date'));
				$q->whereBetween('updated_at', array($start_date, $end_date));
			}

			if (!empty($request->type)) {
				$q->where('search_type', $request->type);
			}
		})->where('created_by', $user->id)->orderBy('_id', 'desc')->offset(0)->limit($limit)->get();
		return response()->json(['status' => 200, 'data' => SearchHistoryResource::collection($searches)]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		try {
			$user = auth('api')->user();
			if ($request->is_modified == 1) {
				$prevSearch = SearchHistory::orderBy('_id', 'desc')->first();
				if ($prevSearch->search_key == $request->search_key) {
					$prevSearch->fill(['search_data' => $request->search_data]);
					$prevSearch->save();
					return response()->json(['status' => 200]);
				}

			}
			$serach = SearchHistory::create(['search_key' => $request->search_key, 'search_data' => $request->search_data, 'search_type' => $request->search_type, 'created_by' => $user->id]);
			return response()->json(['status' => 200]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$search = SearchHistory::find($id);
		return response()->json(['status' => 200, 'data' => new SearchHistoryResource($search)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		try {
			$user = auth('api')->user();
			$search = SearchHistory::find($id);
			$search->fill(['search_key' => $request->search_key, 'search_data' => $request->search_data, 'updated_by' => $user->id]);
			$search->save();
			return response()->json(['status' => 200, 'data' => new SearchHistoryResource($search), 'status_text' => 'Successfully modified search']);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function saveSearch(Request $request, $id) {
		$user = auth('api')->user();
		$search = SearchHistory::find($id);
		$search->is_saved = 1;
		$search->saved_text = $request->save_text;
		$search->updated_by = $user->_id;
		$search->save();
		return response()->json(['status' => 200, 'data' => new SearchHistoryResource($search), 'status_text' => 'Successfully saved']);
	}

	public function savedSearchDelete(Request $request) {
		SearchHistory::whereIn('_id', $request->ids)->delete();
		return response()->json(['status' => 200, 'status_text' => 'Successfully deleted']);
	}

	public function getLastSearch() {
		$search = SearchHistory::orderBy('_id', 'desc')->first();
		return response()->json(['status' => 200, 'data' => $search]);
	}
}
