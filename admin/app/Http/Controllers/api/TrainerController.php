<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeProfile;
use App\Http\Resources\User as UserResource;
use App\Models\AllOtherMasterMst;
use App\Models\CityMst;
use App\Models\CountryMst;
use App\Models\DegreeMst;
use App\Models\EmployeeProfileTxn;
use App\Models\IndustryMst;
use App\Models\SkillMst;
use App\Models\TrainerSchedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class TrainerController extends Controller {

	public function index() {
		$countries = CountryMst::all();
		$cities = CityMst::all();
		$industries = IndustryMst::all();
		$degrees = DegreeMst::all();
		$skills = SkillMst::all();
		$expYears = AllOtherMasterMst::where('entity', 'experiance_year')->get();
		return response()->json(['status' => 200, 'data' => compact('countries', 'cities', 'industries', 'degrees', 'skills', 'expYears')]);

	}

	public function profileUpdate(Request $request) {
		$messages = [
			'from_am_pm.required' => 'Please choose am. or pm.',
			'to_am_pm.required' => 'Please choose am. or pm.',
			'cv_path.required' => 'This field is required',
			'cv_path.mimes' => 'File only allow pdf or doc',
			'identity_proof.required' => 'This field is required',
			'identity_proof.mimes' => 'File only allow image',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'skype_id' => 'required',
			//'email' => 'required|email|unique:users,email',
			'phone_no' => 'required|numeric|regex:/^([+]\d{2})?\d{10}$/',
			'gender' => 'required',
			'date_of_birth' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'industry_id' => 'required',
			'address' => 'required',
			'current_designation' => 'required',
			'highest_education' => 'required',
			'year_of_exp' => 'required',
			'name_account_holder' => 'required',
			'skills' => 'required',
			'cv_path' => $request->hasFile('cv_path') ? 'required|mimes:doc,docx,pdf' : '',
			'identity_proof' => $request->hasFile('identity_proof') ? 'required|mimes:png,jpeg,jpg,gif,svg|image' : '',
			'name_account_holder' => 'required',
			'bank_name' => 'required',
			'account_no' => 'required',
			'swift_code' => 'required',
			'ifsc_code' => 'required|regex:/^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/',
			'bank_portal_address' => 'required',
			'password' => !empty($request->password) ? 'confirmed|min:6' : '',
			/*'expected_days' => 'required',
				'from_time' => 'required',
				'from_am_pm' => 'required',
				'to_time' => 'required',
			*/

		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}
		try {
			$user = auth('api')->user();
			$trainerDetail = $user->trainerUser;
			$trainerDetail->fill($request->except('email', 'phone_no', 'cv_path', 'identity_proof', 'profile_image'));
			$trainerDetail->save();
			if ($request->hasFile('cv_path')) {
				$file = $request->file('cv_path');
				$new_name = $request->file('cv_path')->storePublicly(
					'upload_files/trainer-resumes'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/trainer-resumes' . '/', '', $new_name);
				}
				$trainerDetail->cv_path = $filename;
				$trainerDetail->save();
			}
			if ($request->hasFile('identity_proof')) {
				$file = $request->file('identity_proof');
				$new_name = $request->file('identity_proof')->storePublicly(
					'upload_files/other_users'
				);
				$filename = '';

				if ($new_name) {
					$filename = str_replace('upload_files/other_users' . '/', '', $new_name);
				}
				$trainerDetail->identity_proof = $filename;
				$trainerDetail->save();
			}

			if (!empty($request->password)) {
				$user->password = bcrypt($request->password);
				$user->save();
			}

			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user())]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function traininScheduleCreate(Request $request) {
		//return $request->all();
		$user = auth('api')->user();
		$createdSchedule = [];
		foreach ($request->input('scheduleArray') as $value) {
			$createdSchedule[] = TrainerSchedule::create((array) $value + ['trainer_id' => $user->_id, 'created_by' => $user->_id, 'training_actual_timestamp' => Carbon::parse(new Carbon($value['training_year'] . '-' . ($value['training_month'] + 1) . '-' . $value['training_date'] . ' ' . $value['from_time'] . ':00'))->timestamp]);
		}

		return response()->json(['status' => 200, 'data' => $createdSchedule, 'status_text' => 'Successfully created schedule']);

	}

	public function trainingScheduleUpdate(Request $request) {
		$user = auth('api')->user();
		$uptedSchedule = [];
		foreach ($request->input('scheduleArray') as $value) {
			$userdateschedule = TrainerSchedule::find($value['id']);
			$userdateschedule->fill((array) $value + ['updated_by' => $user->_id, 'training_actual_timestamp' => Carbon::parse(new Carbon($value['training_year'] . '-' . ($value['training_month'] + 1) . '-' . $value['training_date'] . ' ' . $value['from_time'] . ':00'))->timestamp]);
			$userdateschedule->save();
			$uptedSchedule[] = $userdateschedule;
		}

		return response()->json(['status' => 200, 'data' => $uptedSchedule, 'status_text' => 'Successfully created schedule']);
	}

	public function getSchedule($month, $year) {
		$user = auth('api')->user();
		$schedules = TrainerSchedule::where('trainer_id', $user->_id)->where('training_month', (int) $month)->where('training_year', (int) $year)->get();
		$trainerSchedules = $this->trainerScheduleData();
		$countbooked = $this->trainerBookedDate('current');
		return response()->json(['status' => 200, 'data' => $schedules, 'schedule_count' => count($trainerSchedules), 'booked_count' => count($countbooked)]);

	}

	public function profileImageUpdate(Request $request) {
		if ($request->hasFile('profile_image')) {
			$user = auth('api')->user();
			$trainerDetail = $user->trainerUser;
			$file = $request->file('profile_image');
			$new_name = $request->file('profile_image')->storePublicly(
				'upload_files/profile_picture'
			);
			$filename = '';

			if ($new_name) {
				$filename = str_replace('upload_files/profile_picture' . '/', '', $new_name);
			}
			$trainerDetail->profile_image = $filename;
			$trainerDetail->save();
			return response()->json(['status' => 200, 'data' => new UserResource(auth('api')->user())]);
		}
	}

	public function getTrainerList($type) {
		$trainers = User::whereHas('roles', function ($q) {
			$q->where('slug', 'trainer');
		})->whereHas('trainerUser', function ($q) use ($type) {
			$q->where('trainer_type', $type);
		})->get();
		return response()->json(['data' => UserResource::collection($trainers), 'status' => 200]);

	}

	public function searchByTrainer(Request $request) {
		$trainers = User::whereHas('roles', function ($q) {
			$q->where('slug', 'trainer');
		})->whereHas('trainerUser', function ($q) use ($request) {
			$q->where('trainer_type', $request->type);
			if (!empty($request->searchby_country)) {
				$q->where('country_id', $request->searchby_country);
			}
			if (!empty($request->searchby_industry)) {
				$q->where('industry_id', $request->searchby_industry);
			}
		})->get();
		return response()->json(['status' => 200, 'data' => UserResource::collection($trainers)]);
	}

	public function getTrainingSlots($month, $trainer_id) {
		$trainer = User::find($trainer_id);
		$currentYear = Carbon::now()->format('Y');
		$schedule = $trainer->trainerSchedule()->where('training_year', (int) $currentYear)->where('training_month', (int) $month)->with('bookedSchedule')->get();
		$scheduledata = collect($schedule)->groupBy('training_date')->toArray();
		return response()->json(['data' => $scheduledata, 'status' => 200]);

	}

	public function scheduleBookStore(Request $request) {
		$user = auth('api')->user();
		$schedule = TrainerSchedule::find($request->schedule_id);
		$schedule->bookedSchedule()->attach($user->_id, ['is_paid' => 0]);
		return response()->json(['status' => 200, 'status_text' => 'Successfully booked']);
	}

	public function getTrainerSchedules() {
		$schedules = $this->trainerScheduleData();
		return response()->json(['status' => 200, 'data' => $schedules]);
	}

	public function getTrainerBookedList($type) {
		$bookedData = $this->trainerBookedDate($type);
		return response()->json(['status' => 200, 'data' => $bookedData]);
	}

	private function trainerScheduleData() {
		$user = auth('api')->user();
		$current_timestamp = Carbon::now()->timestamp;
		$currentYear = Carbon::now()->parse()->format('Y');
		$currentMonth = Carbon::now()->parse()->format('n');
		$currentDate = Carbon::now()->parse()->format('j');
		$bookedSchedule = TrainerSchedule::where('trainer_id', $user->_id)->where('training_year', '>=', (int) $currentYear)->get();
		$data = collect($bookedSchedule)->map(function ($value, $key) {
			return [
				'_id' => $value->_id,
				'sc_date' => Carbon::createFromFormat('Y-m-d', $value->training_year . '-' . (int) ($value->training_month + 1) . '-' . $value->training_date)->format('d/m/Y'),
				'sc_timestamp' => Carbon::createFromFormat('Y-m-d H:i', $value->training_year . '-' . (int) ($value->training_month + 1) . '-' . $value->training_date . ' ' . $value->from_time)->timestamp,
				'from_time' => $value->from_time,
				'to_time' => $value->to_time,
				'sc_year' => $value->training_year,
				'sc_month' => $value->training_month,
				'created_date' => Carbon::parse($value->created_at)->format('d/m/Y'),
				'updated_date' => Carbon::parse($value->updated_at)->format('d/m/Y'),
			];
		});
		return $data->where('sc_timestamp', '>=', $current_timestamp)->all();
	}

	private function trainerBookedDate($type) {
		$user = auth('api')->user();
		$current_timestamp = Carbon::now()->timestamp;
		$currentYear = Carbon::now()->parse()->format('Y');
		$currentMonth = Carbon::now()->parse()->format('n');
		$currentDate = Carbon::now()->parse()->format('j');
		if ($type == 'previous') {
			$bookedSchedule = TrainerSchedule::where('trainer_id', $user->_id)->has('bookedSchedule')->with('bookedSchedule')->get();
		} else {
			$bookedSchedule = TrainerSchedule::where('trainer_id', $user->_id)->where('training_year', '>=', (int) $currentYear)->has('bookedSchedule')->with('bookedSchedule')->get();
		}
		$data = collect($bookedSchedule)->map(function ($value, $key) {
			return [
				'_id' => $value->_id,
				'employee_detail' => new EmployeeProfile(EmployeeProfileTxn::where('user_id', $value->employee_id[0])->first()),
				'sc_date' => Carbon::createFromFormat('Y-m-d', $value->training_year . '-' . (int) ($value->training_month + 1) . '-' . $value->training_date)->format('d/m/Y'),
				'sc_timestamp' => Carbon::createFromFormat('Y-m-d H:i', $value->training_year . '-' . (int) ($value->training_month + 1) . '-' . $value->training_date . ' ' . $value->from_time)->timestamp,
				'from_time' => $value->from_time,
				'to_time' => $value->to_time,
				'created_date' => Carbon::parse($value->created_at)->format('d/m/Y'),
				'updated_date' => Carbon::parse($value->updated_at)->format('d/m/Y'),
			];
		});

		if ($type == 'previous') {
			return $data->where('sc_timestamp', '<', $current_timestamp)->all();
		} else {
			return $data->where('sc_timestamp', '>=', $current_timestamp)->all();
		}

	}

}
