<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WalkInInterview as WalkInInterviewResource;
use App\Models\EmployerEmployeeOptedServicesTxn;
use App\Models\PaymentConfig;
use App\Models\ServicesMst;
use App\Models\WalkInInterviewTxn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class WalkInInterviewController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		ini_set('memory_limit', '-1');
		$offset = empty($request->off_set) ? 0 : (int) ($request->off_set * 10);
		$employeer_walkin = auth('api')->user()->employerWalkin()->where(function ($q) use ($request) {
			if ($request->has('walkin_location_type')) {
				$q->where('walkin_location_type', (int) $request->walkin_location_type);
			}
		});
		$walkin_count = $employeer_walkin->count();
		$empWalkin = WalkInInterviewResource::collection($employeer_walkin->orderBy('updated_at', 'desc')->offset($offset)->limit(10)->get());
		return response()->json(['status' => 200, 'data' => $empWalkin, 'total_walkin' => $walkin_count]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	public function checkSameInternationalJobExist(Request $request) {
		$jb_trans = WalkInInterviewTxn::where('employer_id', auth('api')->user()->_id)->where('job_title', $request->input('job_title'))->where('city_id', $request->input('city_id'))->first();
		if ($jb_trans) {
			return response()->json(['status' => 200, 'is_exist' => 1]);
		} else {
			return response()->json(['status' => 200, 'is_exist' => 0]);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		//echo "test";die;
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'job_title' => 'required',
			'about_this_job' => 'required',
			'interview_name' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$allRequest = $request->except(['industry_id', 'language_pref', 'department_id', 'candidate_type', 'min_qualification']);
			$bsb_jb_id = WalkInInterviewTxn::count();
			$walkInInterview = WalkInInterviewTxn::create($allRequest + ['employer_id' => auth('api')->user()->_id, 'qualifications' => $request->min_qualification, 'bsb_jb_id' => ($bsb_jb_id + 1), 'candidate_type' => (int) $request->candidate_type, 'job_location' => ['type' => 'Point', 'coordinates' => [$request->job_lat, $request->job_long]]]);
			$walkInInterview->industry()->attach($request->industry_id, ['type' => 'walkin']);
			$walkInInterview->language()->attach($request->language_pref, ['type' => 'walkin']);
			//$walkInInterview->department()->attach($request->department_id, ['type' => 'job']);
			if ($request->walkin_location_type = 2) {
				$payment_config = PaymentConfig::first();
				if ($payment_config && $payment_config->international_walk_in_interview == 1) {
					$service_ids = ServicesMst::where('service_type', 'international_walk_in_interview')->pluck('_id');
					$current_date = Carbon::now();
					$active_service = EmployerEmployeeOptedServicesTxn::where('user_id', auth('api')->user()->_id)->whereIn('service_id', $service_ids)->where('service_expiry', '>=', $current_date)->where('current_status', 'Y')->where('remaining_count', '!=', 0)->first();
					$remaining_count = $active_service->remaining_count;
					$active_service->remaining_count = $remaining_count - 1;
					$active_service->save();
				}
			}
			return response()->json(['status' => 200, 'data' => new WalkInInterviewResource($walkInInterview)]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$walkin_by_id = WalkInInterviewTxn::find($id);
		return response()->json(['status' => 200, 'data' => new WalkInInterviewResource($walkin_by_id)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$messages = [
			//'mobile_no.required' => 'We need to know your mobile no!',
			//'mobile_no.unique' => 'Phone number exist!',
		];
		$validator = Validator::make($request->all(), [
			'job_title' => 'required',
			'about_this_job' => 'required',
			'interview_name' => 'required',
		], $messages);
		if ($validator->fails()) {
			return response()->json(['status' => 422, 'error' => $validator->messages()]);
		}

		try {
			$allRequest = $request->except(['industry_id', 'language_pref', 'department_id', 'candidate_type', 'min_qualification']);
			$walkin = WalkInInterviewTxn::find($id);
			$walkin->fill($allRequest + ['candidate_type' => (int) $request->candidate_type, 'qualifications' => $request->min_qualification, 'job_location' => ['type' => 'Point', 'coordinates' => [$request->job_lat, $request->job_long]]]);
			$walkin->save();
			$walkin->industry()->detach();
			$walkin->language()->detach();
			//$walkin->department()->detach();
			$walkin->industry()->attach($request->industry_id, ['type' => 'walkin']);
			$walkin->language()->attach($request->language_pref, ['type' => 'walkin']);
			//$walkin->department()->attach($request->department_id, ['type' => 'walkin']);
			return response()->json(['status' => 200, 'data' => new WalkInInterviewResource($walkin)]);
		} catch (\Exception $e) {

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$walkin = WalkInInterviewTxn::find($id);
			$walkin->delete();
			return response()->json(['status' => 200, 'status_text' => 'successfully deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function walkingStatus(Request $request, $id) {
		try {
			$walking = WalkInInterviewTxn::find($id);
			$walking->status = $request->input('status');
			$walking->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully change status']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function walkingPublished(Request $request, $id) {
		try {
			$jobs = WalkInInterviewTxn::find($id);
			$jobs->is_published = 1;
			$jobs->published_date = Carbon::now()->format('Y-m-d');
			$jobs->job_status = 1;
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Publised Job', 'data' => new InternationalJobTxnResource($jobs)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
	public function walkingRePublished(Request $request, $id) {
		try {
			$jobs = WalkInInterviewTxn::find($id);
			$jobs->is_republished = 0;
			$jobs->republished_date = Carbon::now()->format('Y-m-d');
			$jobs->job_ageing = Carbon::now()->addMonths(6)->format('Y-m-d');
			$jobs->job_status = 1;
			$jobs->save();
			return response()->json(['status' => 200, 'status_text' => 'Successfully Republised Job', 'data' => new InternationalJobTxnResource($jobs)]);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
}
