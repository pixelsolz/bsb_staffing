<?php

namespace App\Http\Middleware;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLoginRequest {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (strpos($request->url(), "logout") === false) {
			$user = auth('api')->user();
			$user->last_login = Carbon::now()->toDateTimeString();
			$user->save();
		}
		return $next($request);
	}
}
