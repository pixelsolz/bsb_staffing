<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class InternalEmployee {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (Auth::guard('admin')->check()) {
			if (Auth::guard('admin')->user()->roles->first()->slug == 'internal_employee') {
				return $next($request);
				//return redirect('/internal-employee');
			} else {
				Auth::guard('admin')->logout();
				Session::flash('msg', ['status' => 'danger', 'msgs' => 'You are not Authenticated']);
				return redirect()->route('admin.login');
			}

		}
		Session::flash('msg', ['status' => 'danger', 'msgs' => 'You are not Authenticated']);
		return redirect()->route('admin.login');
	}
}
