<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class AgencyDetail extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'user' => $this->user,
			'name' => $this->comp_name,
			'website' => $this->comp_website,
			'country' => $this->country,
			'city' => $this->city,
			'address' => $this->address,
			'contact_person_name' => $this->contact_person_name,
			'contact_person_lastname' => $this->contact_person_lastname,
			'contact_person_designation' => $this->contact_person_designation,
			'contact_person_email' => $this->contact_person_email,
			'contact_person_phone' => $this->contact_person_phone,
			'gender' => $this->gender,
			'dob_date' => $this->dob_date,
			'dob_month' => $this->dob_month,
			'dob_year' => $this->dob_year,
			'registry_certificate' => $this->registry_certificate,
			'govt_id' => $this->govt_id,
			//'govt_id_url' => $this->govt_id ? url('public/upload_files/other_users/' . $this->govt_id) : '',
			'govt_id_url' => $this->govt_id ? Storage::disk('s3')->url('/upload_files/other_users/' . $this->govt_id) : '',
			//'registry_certificate_url' => $this->registry_certificate ? url('public/upload_files/other_users/' . $this->registry_certificate) : '',
			'registry_certificate_url' => $this->registry_certificate ? Storage::disk('s3')->url('/upload_files/other_users/' . $this->registry_certificate) : '',
			'name_account_holder' => $this->name_account_holder,
			'bank_name' => $this->bank_name,
			'account_no' => $this->account_no,
			'swift_code' => $this->swift_code,
			'ifsc_code' => $this->ifsc_code,
			'bank_portal_address' => $this->bank_portal_address,
			//'profile_image_url' => $this->profile_image ? url('public/upload_files/profile_picture/' . $this->profile_image) : '',
			'profile_image_url' => $this->profile_image ? Storage::disk('s3')->url('/upload_files/profile_picture/' . $this->profile_image) : '',

		];
	}
}
