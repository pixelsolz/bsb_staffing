<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AllOtherMaster extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'entity' => $this->entity,
			'orders' => $this->orders,
			'status' => $this->status,
		];
	}
}
