<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
class CarrerPage extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			//'company_logo' => $this->company_logo ? \URL::to('/public/upload_files/career_page/logo/' . $this->company_logo) : '',
			'company_logo' => $this->company_logo ? Storage::disk('s3')->url('/upload_files/career_page/logo/' . $this->company_logo) : '',
			'header_color' => $this->header_color,
			'footer_color' => $this->footer_color,
			'background_color' => $this->background_color,
			'text_color' => $this->text_color,
			'page_headline' => $this->page_headline,
			'about_company' => $this->about_company,
			'contact_info' => $this->contact_info,
			'carrer_photos' => $this->carrerPhotos,
			'company_name' => $this->company_name,
			'user_id' => $this->user_id,
			
		];
	}
}
