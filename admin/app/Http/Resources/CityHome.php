<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityHome extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'country_code' => $this->country_code,
			'name' => $this->name,
			//'lat' => $this->lat,
			//'lng' => $this->lng,
		];
	}
}
