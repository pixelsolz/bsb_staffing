<?php

namespace App\Http\Resources;

use App\Http\Resources\CityHome as CityHomeResource;
use App\Http\Resources\CountryHome as CountryHomeResource;
use App\Http\Resources\CourseResource as CourseResource;
use App\Models\EmployeeProfileTxn;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CollegeDetail extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'user' => $this->user,
			'name' => $this->name,
			'website' => $this->website,
			'country' => new CountryHomeResource($this->country),
			'city' => new CityHomeResource($this->city),
			'address' => $this->address,
			'logo' => $this->logo,
			//'logo_url' => $this->logo ? url('public/upload_files/colleges/' . $this->logo) : '',
			'logo_url' => $this->logo ? Storage::disk('s3')->url('/upload_files/colleges/' . $this->logo) : '',
			'gallery_images' => $this->gallery_images,
			'gallery_img_arr' => explode(',', $this->gallery_images),
			'total_student' => $this->total_student,
			'total_student_place_per_year' => $this->total_student_place_per_year,
			'about_college' => $this->about_college,
			'courses' => $this->user && $this->user->collegeCourses ? CourseResource::collection($this->user->collegeCourses) : [],
			//'courses' =>'',
			//'courses_name' =>'',
			'courses_name' => $this->user && $this->user->collegeCourses ? $this->user->collegeCourses()->pluck('name')->toArray() : [],
			'college_student_count' => !empty($this->user->user_unique_code) ? EmployeeProfileTxn::where('institute_code', $this->user->user_unique_code)->count() : 0,
			'special_note_employer' => $this->special_note_employer,
			'contact_person_name' => $this->contact_person_name,
			'contact_person_lastname' => $this->contact_person_lastname,
			'contact_person_designation' => $this->contact_person_designation,
			'contact_person_email' => $this->contact_person_email,
			'contact_person_phone' => $this->contact_person_phone,
			'gender' => $this->gender,
			'dob_date' => $this->dob_date,
			'dob_month' => $this->dob_month,
			'dob_year' => $this->dob_year,
			'name_account_holder' => $this->name_account_holder,
			'bank_name' => $this->bank_name,
			'account_no' => $this->account_no,
			'swift_code' => $this->swift_code,
			'ifsc_code' => $this->ifsc_code,
			'bank_postal_address' => $this->bank_postal_address,
			'mission' => $this->mission,
			'vision' => $this->vision,
			'institute_video' => $this->institute_video,
		];
	}
}
