<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountryCollection extends ResourceCollection {
	protected $addParam;
	protected $type;
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function addParam($value, $type_value) {
		$this->addParam = $value;
		$this->type = $type_value;
		return $this;
	}

	public function toArray($request) {
		return $this->collection->map(function (Country $resource) use ($request) {
			return $resource->addParam($this->addParam, $this->type)->toArray($request);
		})->all();
	}
}
