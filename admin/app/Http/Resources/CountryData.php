<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryData extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'code' => $this->code,
			'name' => $this->name,
			'currency' => $this->currency,
			'currency_code' => $this->currency_code,
			'dial_code' => $this->dial_code,
			'image' => $this->image,
			'seo_title' => $this->seo_title,
			'seo_description' => $this->seo_description,
			'seo_keywords' => $this->seo_keywords,
			'slug' => $this->slug,
		];
	}
}
