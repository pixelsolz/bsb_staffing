<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryHome extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'code' => $this->code,
			//'dial_code' => $this->dial_code,
			'currency' => $this->currency,
			'currency_code' => $this->currency_code,
			'image' => $this->image,
			'job_count' => $this->job_count ? $this->job_count : 0,
		];
	}
}
