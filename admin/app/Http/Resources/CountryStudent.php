<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryStudent extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	protected $addParam;
	protected $type;
	public function addParam($value, $type_value) {
		$this->addParam = $value;
		$this->type = $type_value;
		return $this;
	}

	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'employee_count' => $this->type == 'employee' ? $this->getEmployeeCount($this, $this->addParam) : '',
		];
	}

	public static function collection($resource) {
		return new CountryStudentCollection($resource);
	}

	public function getEmployeeCount($instance, $request) {
		return $instance->where(function ($q) use ($request) {
			$q->whereHas('employeeCountry.user', function ($q) use ($request) {
				$q->whereHas('employeeCVLike', function ($q) use ($request) {
					$q->where('employer_id', auth('api')->user()->_id);
				});
			});
			$q->whereHas('employeeJobPreference', function ($q) use ($request) {
				if (!empty($request->formData['city_id'])) {
					$q->whereHas('cities', function ($q) use ($request) {
						$q->whereIn('_id', $request->formData['city_id']);
					});
				}
				if (!empty($request->formData['industry_id'])) {
					$q->whereHas('industries', function ($q) use ($request) {
						$q->whereIn('_id', $request->formData['industry_id']);
					});
				}
				if (!empty($request->formData['department_id'])) {
					$q->whereHas('departments', function ($q) use ($request) {
						$q->whereIn('_id', $request->formData['department_id']);
					});
				}
				if (!empty($request->formData['degree_id'])) {
					$q->whereHas('employee.employeeEducation', function ($q) use ($request) {
						$q->whereIn('degree_id', $request->formData['degree_id']);
					});
				}
			});
		})->count();
	}
}
