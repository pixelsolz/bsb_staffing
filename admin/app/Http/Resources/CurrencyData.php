<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyData extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'code' => $this->code,
			'currency' => $this->currency,
			'currency_code' => $this->currency_code,
			'dial_code' => $this->dial_code,
		];
	}
}
