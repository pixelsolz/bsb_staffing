<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EmailTemplate extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'template_name' => $this->template_name,
			'review_status' => $this->admin_reviewd,
			'last_used_date' => $this->last_used_date ? Carbon::parse($this->last_used_date)->format('d/m/Y H:i') : '',
			'created_at' => Carbon::parse($this->created_at)->format('d/m/Y'),
			'updated_at' => Carbon::parse($this->updated_at)->format('d/m/Y'),

		];
	}
}
