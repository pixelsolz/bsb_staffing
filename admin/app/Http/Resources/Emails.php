<?php

namespace App\Http\Resources;

use App\Http\Resources\CollegeDetail as CollegeDetailResource;
use App\Http\Resources\EmployeeProfileEmail as EmployeeProfile;
use App\Http\Resources\EmployerProfile;
use App\Http\Resources\FranchiseDetail as FranchiseDetailResource;
use App\Http\Resources\MailSender as EmployeeMailSender;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class Emails extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$user = auth('api')->user();
		return [
			'_id' => $this->_id,
			'mail_sender_id' => $this->mail_sender_id,
			'mail_receive_id' => $this->mail_receive_id,
			'mail_subject' => $this->mail_subject,
			'mail_body' => $this->mail_body,
			'is_star' => ($this->star_users && in_array($user->_id, $this->star_users)) ? 1 : '',
			'mail_status' => $this->mail_status,
			'mail_sender' => $this->mailSender ? $this->getMailSender($this->mailSender->roles()->first(), $this->mailSender) : '',
			'mail_summary' => $this->mail_summary,
			'attached_file' => $this->attached_file ? is_array($this->attached_file) ? $this->setpathValue($this->attached_file) : [Storage::disk('s3')->url($this->attached_file)] : '',
			'mail_sender_detail' => $this->mailSender ? $this->getUserDetail($this->mailSender->roles()->first(), $this->mailSender) : '',
			'mail_receiver' => $this->mailReceiver ? $this->getMailReciever($this->mailReceiver->roles()->first(), $this->mailReceiver) : '',
			'mail_receiver_detail' => $this->mailReceiver ? $this->getUserDetail($this->mailReceiver->roles()->first(), $this->mailReceiver) : '',
			'video_links' => $this->video_links,
			'template_id' => $this->template_id ? $this->template_id : '',
			'template' => $this->template_id ? $this->emailTemplate : '',
			'created_at' => Carbon::parse($this->created_at)->format('M d,Y,H:i'),
			'day_diff' => Carbon::now()->diffInDays($this->created_at) == 0 ? 'Today' : Carbon::now()->diffInDays($this->created_at) . ' days ago',
			'updated_diff' => Carbon::now()->diffInDays($this->updated_at) == 0 ? 'Today' : Carbon::now()->diffInDays($this->updated_at) . ' days ago',
			'children' => $this->children()->orderBy('updated_at', 'desc')->get(),
			'job_questions' => $this->when(!empty($this->job_questions), $this->job_questions),
			'is_resume_trans' => $this->when(!empty($this->is_resume_trans), $this->is_resume_trans),
		];
	}

	private function setpathValue($files) {
		$attached = [];
		foreach ($files as $key => $value) {
			$attached[] = Storage::disk('s3')->url($value);
		}
		return $attached;
	}

	public function getMailSender($role, $detail) {
		if ($role->slug == 'employee') {
			return new EmployeeMailSender($detail);
		} else {
			return $detail;
		}
	}

	public function getMailReciever($role, $detail) {
		if ($role->slug == 'employee') {
			return new EmployeeMailSender($detail);
		} else {
			return $detail;
		}
	}

	public function getUserDetail($role, $detail) {
		$data = '';
		switch ($role->slug) {
		case 'employee':
			$data = new EmployeeProfile($detail->employeeUser);
			break;
		case 'employer':
			$data = new EmployerProfile($detail->employerUser);
			break;
		case 'college':
			$data = new CollegeDetailResource($detail->collegeUser);
			break;
		case 'agency':
			$data = new AgencyDetailResource($detail->agencyUser);
			break;
		case 'franchise':
			$data = new FranchiseDetailResource($detail->franchiseUser);
			break;
		default:
			// code...
			break;
		}

		return $data;
	}
}
