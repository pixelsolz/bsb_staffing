<?php

namespace App\Http\Resources;
use App\Http\Resources\EmployeeCvNew;
use App\Http\Resources\EmployeeData as EmployeeDataResources;
//use App\Http\Resources\EmployeerJobs;
use App\Http\Resources\EmployerJobsSummary as EmployerJobsSummaryResource;
use App\Models\ApplicationPhaseMst;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeAppliedJobs extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'job_id' => $this->job_id,
			'bsb_jb_app_id' => $this->bsb_jb_app_id,
			'job' => new EmployerJobsSummaryResource($this->job),
			'applied_date' => $this->applied_date ? Carbon::parse($this->applied_date)->format('Y-m-d') : '',
			'applied_status' => $this->applicationStatus,
			//'applied_status_hist' => $this->applicationStatusHist,
			'application_phase_complete' => ApplicationPhaseMst::where('_id', '<=', $this->applied_status)->pluck('_id')->toArray(),
			'employee' => new EmployeeDataResources($this->employee),
			'employee_profile' => new EmployeeCvNew($this->employee),
			'created_at' => $this->created_at ? Carbon::parse($this->created_at)->format('Y-m-d') : '',
			'day_diff' => Carbon::now()->diffInDays($this->created_at),

		];
	}
}
