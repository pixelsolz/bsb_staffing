<?php

namespace App\Http\Resources;
use App\Http\Resources\EmployeeEducation as EmployeeEducationResource;
use App\Http\Resources\EmployeementHistory as EmployeementHistoryResource;
use App\Http\Resources\EmployeePreferenceMail as EmployeePreferenceResource;
use App\Http\Resources\EmployeeProfile as EmployeeProfileResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EmployeeCvNew extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$employee_profile = new EmployeeProfileResource($this->employeeUser);
		return [
			'id' => $this->_id,
			'user_detail' => $employee_profile->employeeProfileCvSearchSummary($this->employeeUser),
			'employee_education' => EmployeeEducationResource::collection($this->employeeEducation),
			'employee_certification' => $this->employeeCertification,
			'skill' => $this->employeeSkill->pluck('title'),
			//'cv' => $this->employeeCvTxn ? \URL::to('/public/upload_files/employee-resumes/'.$this->employeeCvTxn->cv_path) : '',
			'cv' => $this->employeeCvTxn ? Storage::disk('s3')->url('/upload_files/employee-resumes/' . $this->employeeCvTxn->cv_path) : '',
			'desired_job' => new EmployeePreferenceResource($this->employeePrefTxn),
			'employment_history' => EmployeementHistoryResource::collection($this->employeeEmployementHistory),
			'employment_language' => $this->employeeLanguage->pluck('language_name'),
			'user_name' => $this->user_name,
			'email' => $this->email,
			'phone_no' => $this->phone_no,
			'note' => $this->employeeCvNote()->where('employer_id', auth('api')->user()->_id)->first(),
			'last_login' => $this->last_login ? Carbon::parse($this->last_login)->format('d M Y') : Carbon::parse($this->created_at)->format('d M Y'),
			'updated_at' => Carbon::parse($this->updated_at)->format('d M Y'),
			'created_by' => $this->created_by,
			'updated_by' => $this->updated_by,
			'created_at' => Carbon::parse($this->created_at)->format('d/m/Y'),
			'like_profile' => $this->employerCvLike()->where('employer_id', auth('api')->user()->_id)->count(),

		];
	}
}
