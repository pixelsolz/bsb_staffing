<?php

namespace App\Http\Resources;
use App\Http\Resources\EmployerProfile;
use App\Models\EmployerProfileTxn;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeCvViewHistory extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'employer_details' => EmployerProfileTxn::where('user_id', $this->employer_id)->first() ? new EmployerProfile(EmployerProfileTxn::where('user_id', $this->employer_id)->first()) : '',
			'remarks' => $this->remarks,
			'created_at' => $this->created_at ? Carbon::parse($this->created_at)->format('d/m/Y h:i A') : '',
			'created_date' => $this->created_at ? Carbon::parse($this->created_at)->format('d/m/Y') : '',
			'created_time' => $this->created_at ? Carbon::parse($this->created_at)->format('h:i A') : '',

		];
	}
}
