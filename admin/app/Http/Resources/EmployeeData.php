<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeData extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'user_name' => $this->user_name,
			'email' => $this->email,
			'phone_no' => $this->phone_no,
			'updated_at' => $this->updated_at,
			'created_at' => $this->created_at,
			'last_login' => $this->last_login,
			'status' => $this->status,
			'community_token' => $this->community_token,
			'profile_update' => $this->profile_update,
			'bsb_user_no' => $this->bsb_user_no,
		];
	}
}
