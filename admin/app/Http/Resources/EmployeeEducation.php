<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EmployeeEducation extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'user_id' => $this->user_id,
			'course_title' => $this->course_title,
			'course_degree_id' => $this->course_degree_id,
			'course_degree_data' => !empty($this->course_degree_id) ? $this->courseDegree : '',
			'degree_data' => !empty($this->degree_id) && $this->degree ? $this->degree : $this->courseDegree,
			'degree_name' => !empty($this->degree_id) && $this->degree ? @$this->degree->name : $this->course_degree_id ? @$this->courseDegree->name : '',
			'degree_id' => !empty($this->degree_id) ? $this->degree_id : $this->course_degree_id,
			'degree_parent' => !empty($this->degree_parent) && $this->degree ? $this->degree_parent : '',
			'degree_parent_name' => !empty($this->degree_parent) ? $this->degreeParentData->name : '',
			'course_name' => $this->course_name,
			'specialization' => $this->specialization,
			'name_of_institute' => $this->name_of_institute,
			'institute_logo' => $this->institute_logo,
			//'institute_logo_image' => $this->institute_logo ? url('public/upload_files/images/' . $this->institute_logo) : '',
			'institute_logo_image' => $this->institute_logo ? Storage::disk('s3')->url('/upload_files/images/' . $this->institute_logo) : '',
			'institute_website' => $this->institute_website,
			'course_type' => $this->course_type,
			'course_type_text' => $this->getCourseType($this->course_type),
			'passout_year' => $this->passout_year,
		];
	}

	public function getCourseType($type) {
		$value = '';
		switch ($type) {
		case 1:
			$value = 'Full Time';
			break;
		case 2:
			$value = 'Part Time';
			break;
		case 3:
			$value = 'Correspondance/Distance';
			break;

		default:
			// code...
			break;
		}

		return $value;
	}
}
