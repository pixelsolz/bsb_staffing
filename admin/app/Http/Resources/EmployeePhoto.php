<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
class EmployeePhoto extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'user_id' => $this->user_id,
			'description' => $this->description,
			'photo' => $this->photo,
			//'photo_image' => $this->photo ? url('public/upload_files/images/' . $this->photo) : '',
			'photo_image' => $this->photo ? Storage::disk('s3')->url('/upload_files/images/' . $this->photo) : '',
		];
	}
}
