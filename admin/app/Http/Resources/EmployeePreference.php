<?php

namespace App\Http\Resources;
use App\Http\Resources\CityHome as CityHomeResource;
use App\Http\Resources\CountryHome as CountryHomeResource;
use App\Http\Resources\DepartmentHome as DepartmentHomeResource;
use App\Http\Resources\IndustryHome as IndustryHomeResource;
use App\Http\Resources\ShiftType as ShiftTypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeePreference extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'employee_id' => $this->employee_id,
			'countries' => CountryHomeResource::collection($this->countries),
			'country_name' => $this->countries ? implode(',', $this->countries->pluck('name')->toArray()) : '',
			'job_location' => $this->job_location,
			'industries' => IndustryHomeResource::collection($this->industries),
			'industries_name' => $this->industries ? implode(',', $this->industries->pluck('name')->toArray()) : '',
			'departments' => DepartmentHomeResource::collection($this->departments),
			'department_name' => $this->departments ? implode(',', $this->departments->pluck('name')->toArray()) : '',
			'cities' => CityHomeResource::collection($this->cities),
			'cities_name' => $this->cities ? implode(',', $this->cities->pluck('name')->toArray()) : '',
			'job_role_id' => $this->job_role_id,
			'shift_types' => ShiftTypeResource::collection($this->shiftTypes),
			'shifts_name' => $this->shiftTypes ? implode(',', $this->shiftTypes->pluck('name')->toArray()) : '',
			'job_type' => $this->job_type,
			'job_type_text' => $this->job_type ? $this->employment->name : '',
			'currency_type' => $this->currency_type,
			'currency_details' => new CountryHomeResource($this->currency),
			'min_salary' => $this->min_salary,
			'max_salary' => $this->max_salary,
			'min_salary_id' => $this->min_salary,
			'max_salary_id' => $this->max_salary,
			'designation' => $this->designation,
			'salary_type' => $this->salary_type,
			'created_by' => $this->created_by,

		];
	}
}
