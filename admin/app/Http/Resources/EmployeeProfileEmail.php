<?php

namespace App\Http\Resources;

use App\Http\Resources\CityHome as CityHomeResource;
use App\Http\Resources\CountryHome as CountryHomeResource;
use App\Models\AllOtherMasterMst;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EmployeeProfileEmail extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'name' => $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'gender' => $this->gender,
			'gender_text' => $this->gender_text,
			'date_of_birth' => $this->date_of_birth ? Carbon::parse(str_replace('/', '-', $this->date_of_birth))->format('d-m-Y') : '',
			'dob_date' => $this->dob_date,
			'dob_month' => $this->dob_month,
			'dob_year' => $this->dob_year,
			'place' => $this->place,
			'nationality' => $this->nationality,
			'home_town' => $this->home_town,
			'country_id' => $this->country_id,
			'country' => new CountryHomeResource($this->country),
			'city_id' => $this->city_id,
			'city' => new CityHomeResource($this->city),
			'permanent_address' => $this->permanent_address,
			'present_address' => $this->present_address,
			'skype_id' => $this->skype_id,
			//'cv' => $this->cv,
			'emp_cv' => $this->user->employeeCvTxn,
			'profile_picture' => $this->profile_picture,
			//'profile_picture_image' => $this->profile_picture ? url('public/upload_files/profile_picture/' . $this->profile_picture) : '',
			'profile_picture_image' => $this->profile_picture ? Storage::disk('s3')->url('/upload_files/profile_picture/' . $this->profile_picture) : '',
			'profile_title' => $this->profile_title,
			'total_experience_yr' => $this->totExprYr,
			'total_experience_yr_value' => $this->total_experience_yr_value,
			'total_experience_mn' => $this->totExpMn,
			'notice_period' => $this->notice_period,
			'notice_period_data' => $this->notice_period ? AllOtherMasterMst::find($this->notice_period)->name : '',
			'current_salary_currency' => new CountryHomeResource($this->currency),
			'current_salary' => $this->current_salary,
			'current_salary_type' => $this->current_salary_type,
			'summary' => $this->summary,
			'maritual_status' => $this->maritual_status,
			'married_status' => $this->married_status,
			'profile_strength' => $this->profile_strength,
			$this->mergeWhen(!empty($this->institute_code), [
				'institute' => $this->institute,
			]),
		];
	}
}
