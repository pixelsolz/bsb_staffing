<?php

namespace App\Http\Resources;
use App\Http\Resources\DepartmentHome;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EmployeementHistory extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'user_id' => $this->user_id,
			'job_title' => $this->job_title,
			'company_logo' => $this->company_logo,
			//'company_logo_image' => $this->company_logo ? url('public/upload_files/images/' . $this->company_logo) : '',
			'company_logo_image' => $this->company_logo ? Storage::disk('s3')->url('/upload_files/images/' . $this->company_logo) : '',
			'company_name' => $this->company_name,
			'company_website' => $this->company_website,
			'industry_of_company' => $this->industry_of_company,
			'industry_name' => $this->industry ? $this->industry->name : '',
			'department' => new DepartmentHome($this->department_names),
			'department_name' => $this->department_names ? $this->department_names->name : '',
			'from_date' => $this->from_date ? Carbon::parse(str_replace('/', '-', $this->from_date))->format('d-m-Y') : '',
			'to_date' => $this->to_date ? Carbon::parse(str_replace('/', '-', $this->to_date))->format('d-m-Y') : '',
			'is_present_company' => $this->is_present_company,
			'about_job_profile' => $this->about_job_profile,
		];
	}
}
