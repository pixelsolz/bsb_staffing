<?php

namespace App\Http\Resources;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeerFolders extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'folder_name' => $this->folder_name,
			'created_at' => Carbon::parse($this->created_at)->format('d/m/Y'),
			'updated_at' => Carbon::parse($this->updated_at)->format('d/m/Y'),
			'count_cv' => $this->employerCvSavedFolder->count(),

		];
	}
}
