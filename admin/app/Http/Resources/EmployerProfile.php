<?php

namespace App\Http\Resources;

use App\Http\Resources\CityHome;
use App\Http\Resources\CountryData;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EmployerProfile extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'user' => $this->user,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'name' => $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name,
			'company_name' => $this->company_name,
			'industry' => $this->industry_id,
			'industry_details' => $this->industry_id ? new Industry($this->industry) : '',
			'address' => $this->address,
			'city_id' => $this->city_id,
			'city' => new CityHome($this->city),
			'zipcode' => $this->zipcode,
			'country_id' => $this->country_id,
			'country' => new CountryData($this->country),
			'designation' => $this->designation,
			//'company_logo' => $this->company_logo ? \URL::to('/public/upload_files/employer/logo/' . $this->company_logo) : '',
			'company_logo' => $this->company_logo ? Storage::disk('s3')->url('/upload_files/employer/logo/' . $this->company_logo) : '',
			'company_main_image' => $this->company_main_image,
			'alternate_ph_no' => $this->alternate_ph_no,
			'company_type' => $this->company_type,
			'company_type_details' => $this->company_type ? new CompanyType($this->companyType) : '',
			'company_size' => $this->company_size,
			'company_size_details' => $this->company_size ? new CompanySize($this->companySize) : '',
			'company_tagline' => $this->company_tagline,
			'company_short_desc' => $this->company_short_desc,
			'element' => $this->element,
			'nearby_place' => $this->nearby_place,
			'gender' => $this->gender,
			'gender_text' => $this->gender_text,
			'date_of_birth' => $this->date_of_birth,
			'dob_date' => $this->dob_date,
			'dob_month' => $this->dob_month,
			'dob_year' => $this->dob_year,
			'category' => $this->category,
			//'profile_picture' => $this->profile_picture ? \URL::to('/public/upload_files/profile_picture/' . $this->profile_picture) : '',
			'profile_picture' => $this->profile_picture ? Storage::disk('s3')->url('/upload_files/profile_picture/' . $this->profile_picture) : '',
			'is_complete' => $this->is_complete,
			'company_website' => $this->company_website,
			'company_email' => $this->company_email,
			'business_licence' => $this->business_licence,
			'business_licence_type' => $this->business_licence_type,
			'license_agree' => $this->license_agree,
		];
	}
}
