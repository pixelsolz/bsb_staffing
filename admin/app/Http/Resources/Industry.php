<?php

namespace App\Http\Resources;
use App\Models\ExperienceMst;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Industry extends JsonResource {
	protected $addParam;
	protected $type;

	public function addParam($value, $type_value) {
		$this->addParam = $value;
		$this->type = $type_value;
		return $this;
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'name' => $this->name,
			'job_count' => $this->type == 'employee' ? $this->getStudentCount($this->employeeJobPreference(), $this->addParam) : $this->getJobCount($this->job(), $this->addParam),
		];
	}

	public static function collection($resource) {
		return new IndustryCollection($resource);
	}

	public function getStudentCount($instance, $params) {
		return $instance->where(function ($q) use ($params) {
			$q->whereHas('employee', function ($q) {
				if (!empty($params->formData['non_emp_id'])) {
					$q->where('_id', '!=', $params->formData['non_emp_id']);
				}
				$q->whereHas('employeeEducation', function ($q) {
					$q->whereNotNull('_id');
				});
				$q->whereHas('employeeSkill', function ($q) {
					$q->whereNotNull('_id');
				});
				if (!empty($params->formData['search_candidate'])) {
					$q->whereHas('employeeUser', function ($q) use ($params) {
						if ($params->formData['search_by'] == 'search_by_name') {
							$name = explode(' ', $params->formData['search_candidate']);
							if (!empty($name[0])) {
								$q->where('first_name', 'like', '%' . $name[0] . '%');
							}
							if (!empty($name[1])) {
								$q->where('last_name', 'like', '%' . $name[1] . '%');
							}

						}
					});
					if ($params->formData['search_by'] == 'search_by_email') {
						$q->where('email', 'like', '%' . $params->formData['search_candidate'] . '%');
					}
				}
				/* This is for folder search*/
				if (!empty($params->formData['search_type']) && $params->formData['search_type'] == "folder_wise") {
					$q->whereHas('employeeCVLike', function ($q) use ($params) {
						$q->where('employer_id', auth('api')->user()->_id);
					});
					if (!empty($params->formData['folder_id'])) {
						$q->whereHas('employerCvFolder', function ($q) use ($params) {
							$q->where('folder_id', $params->formData['folder_id']);
							$q->where('employer_id', auth('api')->user()->_id);
						});
					}
				}
				// additional form data

				if (!empty($params->additional['country_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($params) {
						$q->whereIn('country_id', $params->additional['country_list']);
					});
				}
				if (!empty($params->additional['city_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($params) {
						$q->whereIn('city_id', $params->additional['city_list']);
					});
				}
				if (!empty($params->additional['industry_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($params) {
						$q->whereIn('industry_id', $params->additional['industry_list']);
					});
				}
				if (!empty($params->additional['department_list'])) {
					$q->whereHas('employeePrefTxn', function ($q) use ($params) {
						$q->whereIn('department_id', $params->additional['department_list']);
					});
				}
				if (!empty($params->additional['degree_list'])) {
					$q->whereHas('employeeEducation', function ($q) use ($params) {
						$q->whereIn('degree_id', $params->additional['degree_list']);
					});
				}
				if (!empty($params->additional['other_filters'])) {
					if (in_array('email_verified_only', $params->additional['other_filters'])) {
						$q->whereNotNull('email_verified_at');
					}if (in_array('women_candidate_only', $params->additional['other_filters'])) {
						$q->whereHas('employeeUser', function ($q) use ($params) {
							$q->where('gender', '2');
						});
					}if (in_array('hide_profile_wout_resume', $params->additional['other_filters'])) {
						$q->has('employeeCvTxn');
						/*$q->whereHas('employeeCvTxn', function($q) use($request){
						});*/
					}
				}
				if (!empty($params->additional['experince'])) {
					if (!empty($params->additional['experince']['min_exp']) && empty($params->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($params->additional['experince']['min_exp']);
						$q->whereHas('employeeUser', function ($q) use ($params, $minexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
						});
					} elseif (!empty($params->additional['experince']['min_exp']) && !empty($params->additional['experince']['max_exp'])) {
						$minexp = ExperienceMst::find($params->additional['experince']['min_exp']);
						$maxexp = ExperienceMst::find($params->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($params, $minexp, $maxexp) {
							$q->where('total_experience_yr_value', '>=', $minexp->value);
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					} elseif (empty($params->additional['experince']['min_exp']) && !empty($params->additional['experince']['max_exp'])) {
						$maxexp = ExperienceMst::find($params->additional['experince']['max_exp']);
						$q->whereHas('employeeUser', function ($q) use ($params, $maxexp) {
							$q->where('total_experience_yr_value', '<=', $maxexp->value);
						});
					}
				}
				if (!empty($params->additional['candidate_active'])) {
					$activeDays = Carbon::now()->subDays($params->additional['candidate_active']);
					$q->where('last_login', '<=', $activeDays->format('Y-m-d'));
				}
				if (!empty($params->additional['salary']['currency'])) {
					$q->whereHas('employeeUser', function ($q) use ($params) {
						$q->where('current_salary_currency', $params->additional['salary']['currency']);
					});
				}
				if (!empty($params->additional['salary']['min_salary']) && !empty($params->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($params) {
						$q->where('current_salary', '>=', (int) $params->additional['salary']['min_salary']);
						$q->where('current_salary', '<=', (int) $params->additional['salary']['max_salary']);
					});
				}
				if (!empty($params->additional['salary']['min_salary']) && empty($params->additional['salary']['max_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($params) {
						$q->where('current_salary', '>=', (int) $params->additional['salary']['min_salary']);
					});
				}

				if (!empty($params->additional['salary']['max_salary']) && empty($params->additional['salary']['min_salary'])) {
					$q->whereHas('employeeUser', function ($q) use ($params) {
						$q->where('current_salary', '<=', (int) $params->additional['salary']['max_salary']);
					});
				}

				if (!empty($params->additional['show_profile'])) {
					$prevDays = Carbon::now()->subDays(30);
					$q->where('created_at', '<=', new \DateTime('0 day'));
					$q->where('created_at', '>=', new \DateTime('-30 day'));

				}
			});
			if (!empty($params->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($params) {
					$q->whereIn('title', $params->formData['key_skills']);
				});
			}
			if (!empty($params->formData['industry_id'])) {
				$q->whereHas('industries', function ($q) use ($params) {
					$q->whereIn('_id', $params->formData['industry_id']);
				});

			}
			if (!empty($params->formData['country_id'])) {
				$q->whereHas('countries', function ($q) use ($params) {
					$q->whereIn('_id', $params->formData['country_id']);
				});
			}
			if (!empty($params->formData['city_id'])) {
				$q->whereHas('cities', function ($q) use ($params) {
					$q->whereIn('_id', $params->formData['city_id']);
				});
			}
			if (!empty($params->formData['department_id'])) {
				$q->whereHas('departments', function ($q) use ($params) {
					$q->whereIn('_id', $params->formData['department_id']);
				});
			}
			if (!empty($params->formData['total_experience_yr'])) {
				/*$q->whereHas('employee.employeeUser', function ($q) use ($params) {
					$q->whereBetween('total_experience_yr', [((int) $params->formData['total_experience_yr'] - 1), ((int) $params->formData['total_experience_yr'] + 1)]);
				});*/
			}
			if (!empty($params->formData['degree_id'])) {
				$q->whereHas('employee.employeeEducation', function ($q) use ($params) {
					$q->whereIn('degree_id', $params->formData['degree_id']);
				});
			}
			if (!empty($params->formData['current_salary'])) {
				$q->whereHas('employee.employeePrefTxn', function ($q) use ($params) {
					$q->where('min_salary', '>=', ((int) $params->formData['current_salary'] - 10000));
					$q->where('max_salary', '<=', ((int) $params->formData['current_salary'] + 10000));
				});
			}
			if (!empty($params->formData['key_skills'])) {
				$q->whereHas('employee.employeeSkill', function ($q) use ($params) {
					$q->whereIn('title', $params->formData['key_skills']);
				});
			}
		})->count();
	}

	public function getJobCount($instance, $params) {
		$user = auth('api')->user() ? auth('api')->user() : '';
		return $instance->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
			if ($user) {
				$q->where('employee_id', $user->_id);
			}
		})->where('job_status', 1)->where('is_published', 1)->where(function ($q) use ($params) {
			if (!empty($params['cat']) && !empty($params['cat_id'])) {
				if ($params['cat'] == 'country') {
					$q->where('country_id', $params['cat_id']);
				} elseif ($params['cat'] == 'city') {
					$q->where('city_id', $params['cat_id']);
				} elseif ($params['cat'] == 'department') {
					$q->where('department_id', $params['cat_id']);
				} elseif ($params['cat'] == 'industry') {
					$q->where('industry_id', $params['cat_id']);
				}
			}
			if (!empty($params->formData['key_skills'])) {
				$q->whereIn('required_skill', $params->formData['key_skills']);
			}
			if (!empty($params->formData['industry_id'])) {
				$q->whereIn('industry_id', $params->formData['industry_id']);
			}
			if (!empty($params->formData['country_id'])) {
				$q->whereIn('country_id', $params->formData['country_id']);
			}
			if (!empty($params->formData['city_id'])) {
				$q->whereIn('city_id', $params->formData['city_id']);
			}
			if (!empty($params->formData['department_id'])) {
				$q->whereIn('department_id', $params->formData['department_id']);
			}

			if (!empty($params['job_title'])) {
				foreach ($params->input('job_title') as $value) {
					$q->orWhere('job_title', 'like', '%' . $value . '%');
					$q->orWhere('required_skill', 'like', '%' . $value . '%');
				}
				/*$q->whereIn('job_title', $params['job_title']);
				$q->orWhereIn('required_skill', $params['job_title']);*/
			}
			if (!empty($params['search_city'])) {
				$q->whereHas('country', function ($q) use ($params) {
					$q->whereIn('name', $params['search_city']);
				});
				$q->orWhereHas('city', function ($q) use ($params) {
					$q->whereIn('name', $params['search_city']);
				});
			}
			if (!empty($params['search_department'])) {
				$q->whereHas('department', function ($q) use ($params) {
					$q->whereIn('name', $params['search_department']);
				});
			}
			if (!empty($params['search_industry'])) {
				$q->whereHas('industry', function ($q) use ($params) {
					$q->whereIn('name', $params['search_industry']);
				});
			}

			if (!empty($params['country_list'])) {
				$q->whereIn('country_id', $params['country_list']);
			}

			if (!empty($params['city_list'])) {
				$q->whereIn('city_id', $params['city_list']);
			}
			if (!empty($params['industry_list'])) {
				$q->whereHas('industry', function ($q) use ($params) {
					$q->whereIn('_id', $params['industry_list']);
				});
			}
			if (!empty($params['department_list'])) {
				$q->whereHas('department', function ($q) use ($params) {
					$q->whereIn('_id', $params['department_list']);
				});
			}if (!empty($params['experince'])) {
				if (!empty($params['experince']['min_exp']) && empty($params['experince']['max_exp'])) {
					$minexp = ExperienceMst::find($params->input('experince')['min_exp']);
					$q->whereHas('minExperiance', function ($q) use ($params, $minexp) {
						$q->where('value', '>=', $minexp->value);
					});
				} elseif (!empty($params['experince']['min_exp']) && !empty($params['experince']['max_exp'])) {
					$minexp = ExperienceMst::find($params['experince']['min_exp']);
					$maxexp = ExperienceMst::find($params['experince']['max_exp']);
					$q->whereHas('minExperiance', function ($q) use ($params, $minexp, $maxexp) {
						$q->where('value', '>=', $minexp->value);
						$q->where('value', '<=', $maxexp->value);
					});
				} elseif (empty($params['experince']['min_exp']) && !empty($params['experince']['max_exp'])) {
					$maxexp = ExperienceMst::find($params['experince']['max_exp']);
					$q->whereHas('maxExperiance', function ($q) use ($params, $maxexp) {
						$q->where('value', '<=', $maxexp->value);
					});
				}
			}
			if (!empty($params['salary'])) {
				if (!empty($params['salary']['currency'])) {
					$q->where('currency', $params['salary']['currency']);
				}
				if (!empty($params['salary']['min_salary']) && !empty($params['salary']['max_salary'])) {
					$q->where('min_monthly_salary', '>=', (int) $params['salary']['min_salary']);
					$q->where('min_monthly_salary', '<=', (int) $params['salary']['max_salary']);
				}
				if (!empty($params['salary']['min_salary']) && empty($params['salary']['max_salary'])) {
					$q->where('min_monthly_salary', '>=', (int) $params['salary']['min_salary']);
				}
				if (empty($params['salary']['min_salary']) && !empty($params['salary']['max_salary'])) {
					$q->where('max_monthly_salary', '<=', (int) $params['salary']['max_salary']);
				}

			}

		})->count();

	}
}
