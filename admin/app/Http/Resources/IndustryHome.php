<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IndustryHome extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'attribute_id' => $this->attribute_id,
			//'service_id' => $this->service_id,
			//'status' => $this->status,
		];
	}
}
