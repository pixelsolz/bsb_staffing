<?php

namespace App\Http\Resources;
use App\Http\Resources\EmployeeCv;
use App\Http\Resources\InternationalJobsSummary as InternationalJobsSummaryResource;
use App\Http\Resources\User;
//use App\Http\Resources\InternationalJobTxn;
use App\Models\ApplicationPhaseMst;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class InternationalJobSavedApplied extends JsonResource {
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'job' => new InternationalJobsSummaryResource($this->job),
			'bsb_jb_app_id' => $this->bsb_jb_app_id,
			'applied_date' => $this->applied_date ? Carbon::parse($this->applied_date)->format('Y-m-d') : '',
			'applied_status' => $this->applicationStatus,
			'employee' => new User($this->employee),
			'employee_profile' => new EmployeeCv($this->employee),
			'application_phase_complete' => $this->applied_status ? ApplicationPhaseMst::where('_id', '<=', $this->applied_status)->pluck('_id')->toArray() : '',
			'saved_on' => $this->saved_on ? Carbon::parse($this->saved_on)->format('Y-m-d') : '',
			'created_at' => $this->created_at ? Carbon::parse($this->created_at)->format('Y-m-d') : '',
			'day_diff' => Carbon::now()->diffInDays($this->created_at),
		];
	}
}
