<?php

namespace App\Http\Resources;
use App\Models\Course;
use App\Models\DegreeMst;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class InternationalJobsSummary extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'job_title' => $this->job_title,
			'job_desc' => $this->job_desc,
			'job_role' => $this->jobRole,
			'is_saved' => (auth('api')->user() && $this->jobSavedApplied()->where('employee_id', auth('api')->user()->_id)->whereNotNull('saved_on')->count()) ? 1 : 0,
			'employer_id' => $this->employer ? $this->employer->_id : '',
			//'employer_logo' => ($this->employer && $this->employer->employerUser) ? \URL::to('/public/upload_files/employer/logo/' . $this->employer->employerUser->company_logo) : '',
			'employer_logo' => ($this->employer && $this->employer->employerUser) ? Storage::disk('s3')->url('/upload_files/employer/logo/' . $this->employer->employerUser->company_logo) : '',
			'employer_name' => ($this->employer && $this->employer->employerUser) ? $this->employer->employerUser->full_name : '',
			'employer_gallery' => ($this->employer && $this->employer->galleryUser) ? $this->employer->galleryUser : '',
			'employer_company_name' => ($this->employer && $this->employer->employerUser) ? $this->employer->employerUser->company_name : '',
			'employer_company_tagline' => ($this->employer && $this->employer->employerUser) ? $this->employer->employerUser->company_tagline : '',
			'employer_city' => ($this->employer && $this->employer->employerUser && $this->employer->employerUser->city) ? $this->employer->employerUser->city->name : '',
			'employer_country' => ($this->employer && $this->employer->employerUser && $this->employer->employerUser->country) ? $this->employer->employerUser->country->name : '',
			'employer_industry' => ($this->employer && $this->employer->employerUser && $this->employer->employerUser->industry) ? $this->employer->employerUser->industry->name : '',
			'employer_compsize' => ($this->employer && $this->employer->employerUser && $this->employer->employerUser->companySize) ? $this->employer->employerUser->companySize->company_size : '',
			'employer_comp_sortdesc' => ($this->employer && $this->employer->employerUser) ? $this->employer->employerUser->company_short_desc : '',
			'employer_lat' => ($this->employer && $this->employer->employerUser) ? $this->employer->employerUser->lat : '',
			'employer_lng' => ($this->employer && $this->employer->employerUser) ? $this->employer->employerUser->lng : '',
			'industry_ids' => $this->industry()->pluck('_id')->toArray(),
			'industry_name' => $this->industry()->pluck('name')->toArray(),
			'department_ids' => $this->department()->pluck('_id')->toArray(),
			'department_name' => $this->department()->pluck('name')->toArray(),
			'about_this_job' => $this->about_this_job,
			'about_this_job_summary' => $this->about_this_job ? mb_strimwidth(trim($this->about_this_job), 0, 270, '...') : '',
			'job_posted_to_be' => $this->job_posted_to_be,
			'employement_for_id' => $this->employement_for,
			'employement_for' => $this->employmentFor,
			'required_skill' => $this->required_skill,
			'primary_responsibility' => $this->primary_responsibility,
			'special_notes' => $this->special_notes,
			'country' => $this->country->pluck('_id')->toArray(),
			'country_location' => $this->country ? implode(',', $this->country->pluck('name')->toArray()) : '',
			'location_coutry' => $this->locationCountry->pluck('_id')->toArray(),
			'location_coutry_name' => $this->locationCountry ? implode(',', $this->locationCountry->pluck('name')->toArray()) : '',
			'location_city' => $this->locationCity->pluck('_id')->toArray(),
			'location_city_name' => $this->locationCity ? implode(',', $this->locationCity->pluck('name')->toArray()) : '',
			'city' => $this->city->pluck('_id')->toArray(),
			'city_location' => $this->city ? implode(',', $this->city->pluck('name')->toArray()) : '',
			'benefits' => $this->benefits,
			'min_qualification' => is_array($this->min_qualification) ? DegreeMst::whereIn('_id', $this->min_qualification)->where('parent_id', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->min_qualification])->pluck('_id')->toArray(),
			'min_qualification_name' => is_array($this->min_qualification) ? DegreeMst::whereIn('_id', $this->min_qualification)->where('parent_id', '0')->pluck('name')->toArray() : DegreeMst::whereIn('_id', [$this->min_qualification])->pluck('name')->toArray(),
			'max_qualification' => is_array($this->max_qualification) ? DegreeMst::whereIn('_id', $this->max_qualification)->where('parent_id', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->max_qualification])->pluck('_id')->toArray(),
			'max_qualification_name' => is_array($this->max_qualification) ? DegreeMst::whereIn('_id', $this->max_qualification)->where('parent_id', '0')->pluck('name')->toArray() : DegreeMst::whereIn('_id', [$this->max_qualification])->pluck('name')->toArray(),
			'min_qualification_stream' => is_array($this->min_qualification) ? DegreeMst::whereIn('_id', $this->min_qualification)->where('parent_id', '!=', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->min_qualification])->pluck('_id')->toArray(),
			'max_qualification_stream' => is_array($this->max_qualification) ? DegreeMst::whereIn('_id', $this->max_qualification)->where('parent_id', '!=', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->max_qualification])->pluck('_id')->toArray(),
			'qualifications' => $this->qualifications,
			'qualifications_name' => !empty($this->qualifications) ? Course::whereIn('_id', $this->qualifications)->pluck('name')->toArray() : [],
			'graduate_and_or_post_graduate' => $this->graduate_and_or_post_graduate,
			'min_experiance_id' => $this->min_experiance,
			'min_experiance' => $this->minExperiance,
			'max_experiance_id' => $this->max_experiance,
			'max_experiance' => $this->maxExperiance,
			'language_pref_id' => $this->language()->pluck('_id')->toArray(),
			'language_pref_name' => $this->language()->pluck('language_name')->toArray(),
			'employment_type_id' => $this->employment_type,
			'employment_type' => $this->employmentType,
			'number_of_vacancies' => $this->number_of_vacancies,
			'min_monthly_salary' => $this->min_monthly_salary,
			'max_monthly_salary' => $this->max_monthly_salary,
			'received_email' => $this->received_email,
			'currency' => $this->currency,
			'currency_name' => $this->currencydata ? $this->currencydata->currency_code : '',
			'salary_type' => $this->salary_type,
			'job_status' => $this->job_status,
			'job_type' => $this->job_type == 1 ? 'free' : 'Premium',
			'job_ageing' => $this->job_ageing,
			'created_by' => $this->created_by,
			'updated_by' => $this->updated_by,
			'created_at' => Carbon::parse($this->created_at)->format('d/m/Y'),
			'expiry_at' => Carbon::parse($this->created_at)->addMonth(1)->format('d/m/Y'),
			'created_at_formate' => Carbon::parse($this->created_at)->format('M d,Y'),
			//'day_diff' => Carbon::now()->diffInDays($this->created_at),
			'day_diff' => Carbon::now()->diffInDays($this->updated_at),
			'job_views' => $this->jobViews->count(),
			'job_ageing' => Carbon::parse($this->job_ageing)->format('d/m/Y'),
			'bsb_jb_trans_id' => $this->bsb_jb_trans_id,
			'candidate_type' => $this->candidate_type,
			'candidate_type_text' => $this->candidate_type == 1 ? 'Male & Female both' : ($this->candidate_type == 2 ? 'Only for Male' : ($this->candidate_type == 3 ? 'Only for Female' : '')),
			'job_lat' => $this->job_lat,
			'job_long' => $this->job_lat,
			'condition_type' => $this->condition_type,
			'conditions' => $this->conditions,

		];
	}
}
