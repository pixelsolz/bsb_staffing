<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class JobDepartmentFooter extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'total_count' => $this->getCount($this->jobs()->where('job_status', 1)->where('is_published', 1), $this->internationalJobs()->where('job_status', 1), $this->walkingJobs()->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)),
		];
	}

	public function getCount($jobs, $internationalJobs, $walkingJobs) {
		if (auth('api')->user()) {
			$user = auth('api')->user();
			$jobCount = $jobs->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			})->count();
			$intJobCount = $internationalJobs->whereDoesntHave('jobSavedApplied', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			})->count();
			$walkingCount = $walkingJobs->whereDoesntHave('interviewSlotBooked', function ($q) use ($user) {
				$q->where('user_id', $user->_id);
			})->count();
			return $jobCount + $intJobCount + $walkingCount;
		} else {
			$jobCount = $jobs->count();
			$intJobCount = $internationalJobs->count();
			$walkingCount = $walkingJobs->count();
			return $jobCount + $intJobCount + $walkingCount;
		}

	}
}
