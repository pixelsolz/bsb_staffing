<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobTitleFooter extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'job_title' => $this->job_title,
			'total_count' => $this::where('job_title', $this->job_title)->count(),
		];
	}
}
