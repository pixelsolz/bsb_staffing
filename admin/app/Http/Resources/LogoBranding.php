<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\EmployerProfile as EmployerProfileResource;
use Illuminate\Support\Facades\Storage;
class LogoBranding extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			//'company_logo' => $this->employerUser->company_logo ? \URL::to('/public/upload_files/employer/logo/' . $this->employerUser->company_logo) : '',
			'company_logo' => $this->employerUser->company_logo ? Storage::disk('s3')->url('/upload_files/employer/logo/' . $this->employerUser->company_logo) : '',
			'employer_job_count' => $this->totalJobCount(),
		];
	}
}
