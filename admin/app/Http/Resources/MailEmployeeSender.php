<?php

namespace App\Http\Resources;
use App\Http\Resources\EmployeeData;
use Illuminate\Http\Resources\Json\JsonResource;

class MailEmployeeSender extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
 
                    "employee_cv_txn"=> $this->employeeCvTxn,
                    "employee_pref_txn"=> $this->employeePrefTxn
                    "employee_employement_history"=>$this->employeeEmployementHistory,
                    "employee_education"=>$this->employeeEducation,
                    "employee_skill"=>$this->employeeSkill,
                    "employee_certification"=>$this->employeeCertification,
                    "employee_language"=>$this->employeeLanguage,

        ];
    }
}
