<?php

namespace App\Http\Resources;

use App\Http\Resources\EmployeeCv;
use Illuminate\Http\Resources\Json\JsonResource;

class MailSender extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			"_id" => $this->_id,
			"user_name" => $this->user_name,
			"email" => $this->email,
			"phone_no" => $this->phone_no,
			"profile_update" => $this->profile_update,
			"register_token" => $this->register_token,
			"bsb_unique_no" => $this->bsb_unique_no,
			"status" => $this->status,
			"community_token" => $this->community_token,
			"last_login" => $this->last_login,
			"bsb_user_no" => $this->bsb_user_no,
			"employee_user_detail" => new EmployeeCv($this),
			"mail_summary" => $this->mail_summary,

		];
	}
}
