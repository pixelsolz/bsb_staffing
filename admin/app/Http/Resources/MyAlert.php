<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyAlert extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'user' => $this->employee,
			'keyword' => $this->keyword,
			'salary_type' => $this->salary_type,
			'currency' => $this->currency,
			'salary' => $this->salary,
			'experiance' => $this->experiance,
			'country_id' => $this->country()->pluck('_id')->toArray(),
			'country_name' => $this->country()->pluck('name')->toArray(),
			'city_id' => $this->city()->pluck('_id')->toArray(),
			'city_name' => $this->city()->pluck('name')->toArray(),
			'department_id' => $this->department()->pluck('_id')->toArray(),
			'department_name' => $this->department()->pluck('name')->toArray(),
			'industry_id' => $this->industry()->pluck('_id')->toArray(),
			'industry_name' => $this->industry()->pluck('name')->toArray(),
			'email_id' => $this->email_id,
			'name_of_alert' => $this->name_of_alert,
		];
	}
}
