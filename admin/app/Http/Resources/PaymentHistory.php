<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
class PaymentHistory extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'payment_amount' => $this->payment_amount,
			'remarks' => $this->remarks,
			'payment_made_on'=> Carbon::parse($this->payment_made_on)->format('d/m/Y'),
			//'payment_made_on'=> (string)$this->created_at,
			'service_details'=>$this->service,
			//'user_id' => $this->user_id,
		];
	}
}
