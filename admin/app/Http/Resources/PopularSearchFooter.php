<?php

namespace App\Http\Resources;
use App\Models\EmployerJobTxn;
use Illuminate\Http\Resources\Json\JsonResource;

class PopularSearchFooter extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'search_key' => $this->search_key,
			'total_count' => EmployerJobTxn::where('job_title', 'like', '%' . $this->search_key . '%')->count(),
		];
	}
}
