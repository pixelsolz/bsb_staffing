<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Role extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'slug' => $this->slug,
			'permissions' => $this->permissions,
			'permission_array' => $this->permission_array,
		];
	}
}
