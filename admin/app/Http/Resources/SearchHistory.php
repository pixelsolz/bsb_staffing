<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SearchHistory extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'search_key' => $this->search_key,
			'search_data' => $this->search_data,
			'search_type' => $this->search_type . ' search',
			'search_url' => $this->search_type . '-search',
			'is_saved' => $this->is_saved,
			'saved_text' => $this->saved_text,
			'created_by' => $this->employeer,
			'created_at' => $this->created_at,
			'updated_at' => Carbon::parse($this->updated_at)->format('d M Y g:i A'),
			'create_date' => Carbon::parse($this->created_at)->format('d M Y g:i A'),
		];
	}
}
