<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShiftType extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'name' => $this->name,
			'entity' => $this->entity,
			'orders' => $this->orders,
			'created_at' => $this->created_at,
			'status' => $this->status,
			'updated_at' => $this->updated_at,
		];
	}
}
