<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentDetail extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'name' => $this->employeeUser->first_name . ' ' . $this->employeeUser->last_name,
			'industry' => '',
			'department' => '',
			'employment_for' => '',
			'email' => $this->email,
			'phone' => $this->phone,
			'company' => '',
		];
	}
}
