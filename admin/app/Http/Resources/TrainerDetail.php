<?php

namespace App\Http\Resources;
use App\Models\DegreeMst;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
class TrainerDetail extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'user_id' => $this->user_id,
			'name' => $this->name,
			'mid_name' => $this->mid_name,
			'family_name' => $this->family_name,
			'skype_id' => $this->skype_id,
			'gender' => $this->gender,
			'gender_text' => $this->gender == 1 ? 'Male' : 'Female',
			'date_of_birth' => $this->date_of_birth,
			'country' => $this->country,
			'city' => $this->city,
			'industry' => $this->industry,
			'address' => $this->address,
			'current_designation' => $this->current_designation,
			'highest_education' => $this->highest_education,
			'education_name' => is_array($this->highest_education) ? DegreeMst::whereIn('_id', $this->highest_education)->where('parent_id', '0')->pluck('name')->toArray() : DegreeMst::whereIn('_id', [$this->highest_education])->pluck('name')->toArray(),
			'education_id' => is_array($this->highest_education) ? DegreeMst::whereIn('_id', $this->highest_education)->where('parent_id', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->highest_education])->pluck('_id')->toArray(),
			'education_stream' => is_array($this->highest_education) ? DegreeMst::whereIn('_id', $this->highest_education)->where('parent_id', '!=', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->highest_education])->pluck('_id')->toArray(),
			'education' => is_array($this->highest_education) ? DegreeMst::whereIn('_id', $this->highest_education)->where('parent_id', '0')->with('child')->get() : DegreeMst::whereIn('_id', [$this->highest_education])->with('child')->get(),
			'year_of_exp' => $this->year_of_exp,
			'expreiance' => $this->expreiance,
			'skills' => $this->skills,
			'about_profile' => $this->about_profile,
			'cv_path' => $this->cv_path,
			//'cv_path_url' => $this->cv_path ? url('public/upload_files/trainer-resumes/' . $this->cv_path) : '',
			'cv_path_url' => $this->cv_path ? Storage::disk('s3')->url('/upload_files/trainer-resumes/' . $this->cv_path) : '',
			'profile_image' => $this->profile_image,
			//'profile_image_url' => $this->profile_image ? url('public/upload_files/profile_picture/' . $this->profile_image) : '',
			'profile_image_url' => $this->profile_image ? Storage::disk('s3')->url('/upload_files/profile_picture/' . $this->profile_image) : '',
			'identity_proof' => $this->identity_proof,
			//'identity_proof_url' => $this->identity_proof ? url('public/upload_files/other_users/' . $this->identity_proof) : '',
			'identity_proof_url' => $this->identity_proof ? Storage::disk('s3')->url('/upload_files/other_users/' . $this->identity_proof) : '',
			'name_account_holder' => $this->name_account_holder,
			'bank_name' => $this->bank_name,
			'account_no' => $this->account_no,
			'swift_code' => $this->swift_code,
			'ifsc_code' => $this->ifsc_code,
			'bank_portal_address' => $this->bank_portal_address,
			'expected_days' => $this->expected_days,
			'from_time' => $this->from_time,
			'from_am_pm' => $this->from_am_pm,
			'to_time' => $this->to_time,
			'to_am_pm' => $this->to_am_pm,
			'created_at' => $this->created_at,
		];
	}
}
