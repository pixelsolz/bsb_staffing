<?php

namespace App\Http\Resources;
use App\Http\Resources\AgencyDetail as AgencyDetailResource;
use App\Http\Resources\CollegeDetail as CollegeDetailResource;
use App\Http\Resources\EmployeeProfile as EmployeeProfileResource;
use App\Http\Resources\EmployerProfile;
use App\Http\Resources\FranchiseDetail as FranchiseDetailResource;
use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\TrainerDetail as TrainerDetailResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class User extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id' => $this->_id,
			'user_name' => $this->user_name,
			'email' => $this->email,
			'password' => $this->password,
			'phone_no' => $this->phone_no,
			'community_token' => $this->community_token,
			'last_login' => $this->roles()->first()->slug == 'college' ? Carbon::parse(@$this->last_login)->format('d M Y') : Carbon::parse($this->created_at)->format('d M Y'),
			'profile_update' => $this->profile_update ? Carbon::parse($this->profile_update)->format('d.m.Y') : '',
			'updated_at' => Carbon::parse($this->updated_at)->format('d M Y'),
			'created_at' => Carbon::parse($this->created_at)->format('d M Y'),
			'is_employer' => auth('api')->user()->isEmployer() ? auth('api')->user()->isEmployer() : false,
			'user_detail' => $this->getUserDetail($this->roles()->first()),
			'gallery_images' => $this->galleryUser,
			'video_link' => $this->videoGalleryUser,
			'user_role' => $this->roles()->first()->slug,
			'role' => new RoleResource($this->roles()->first()),
			'cv' => $this->employeeCvTxn,
			'user_location' => $this->user_location ? $this->user_location : '',
			//'cv_path' => $this->employeeCvTxn ? url('public/upload_files/employee-resumes/' . $this->employeeCvTxn->cv_path) : '',
			'cv_path' => $this->employeeCvTxn ? Storage::disk('s3')->url('/upload_files/employee-resumes/' . $this->employeeCvTxn->cv_path) : '',
			'bsb_user_no' => $this->bsb_user_no,

		];
	}

	public function getLastLogin() {

	}

	public function getUserDetail($role) {
		$data = '';
		switch ($role->slug) {
		case 'employee':
			$employee_profile = new EmployeeProfileResource($this->employeeUser);
			$data = $employee_profile->employeeProfileCvSearch($this->employeeUser);
			//$data = $this->employeeUser;
			//$data = new EmployeeProfile($this->employeeUser);
			break;
		case 'employer':
			$data = new EmployerProfile($this->employerUser);
			break;
		case 'college':
			$data = new CollegeDetailResource($this->collegeUser);
			break;
		case 'agency':
			$data = new AgencyDetailResource($this->agencyUser);
			break;
		case 'franchise':
			$data = new FranchiseDetailResource($this->franchiseUser);
			break;
		case 'trainer':
			$data = new TrainerDetailResource($this->trainerUser);
			break;
		default:
			// code...
			break;
		}

		return $data;
	}
}
