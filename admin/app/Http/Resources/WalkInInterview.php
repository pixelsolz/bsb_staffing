<?php

namespace App\Http\Resources;
use App\Http\Resources\CityHome as CityHomeResource;
use App\Http\Resources\CountryHome as CountryHomeResource;
use App\Http\Resources\CurrencyData;
use App\Http\Resources\EmployerDetails;
use App\Models\Course;
use App\Models\DegreeMst;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class WalkInInterview extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'_id' => $this->_id,
			'job_title' => $this->job_title,
			'employeer' => new EmployerDetails($this->employer),
			'city_id' => $this->city_id,
			'job_city_name' => new CityHomeResource($this->jobCity),
			'country_id' => $this->country_id,
			'job_country_name' => new CountryHomeResource($this->jobCountry),
			'industry_ids' => $this->industry()->pluck('_id')->toArray(),
			'industry_name' => $this->industry()->pluck('name')->toArray(),
			'department_ids' => $this->department()->pluck('_id')->toArray(),
			'department_name' => $this->department()->pluck('name')->toArray(),
			'about_this_job' => $this->about_this_job,
			'employement_for_id' => $this->employement_for,
			'employement_for' => $this->employmentFor,
			'position_offered' => $this->position_offered,
			'number_of_vacancies' => $this->number_of_vacancies,
			'employment_type_id' => $this->employment_type,
			'employment_type' => $this->employmentType,
			'min_experiance' => $this->minExperiance,
			'max_experiance' => $this->maxExperiance,
			'min_qualification' => is_array($this->min_qualification) ? DegreeMst::whereIn('_id', $this->min_qualification)->where('parent_id', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->min_qualification])->pluck('_id')->toArray(),
			'min_qualification_name' => is_array($this->min_qualification) ? DegreeMst::whereIn('_id', $this->min_qualification)->where('parent_id', '0')->pluck('name')->toArray() : DegreeMst::whereIn('_id', [$this->min_qualification])->pluck('name')->toArray(),
			'max_qualification' => is_array($this->max_qualification) ? DegreeMst::whereIn('_id', $this->max_qualification)->where('parent_id', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->max_qualification])->pluck('_id')->toArray(),
			'max_qualification_name' => is_array($this->max_qualification) ? DegreeMst::whereIn('_id', $this->max_qualification)->where('parent_id', '0')->pluck('name')->toArray() : DegreeMst::whereIn('_id', [$this->max_qualification])->pluck('name')->toArray(),
			'min_qualification_stream' => is_array($this->min_qualification) ? DegreeMst::whereIn('_id', $this->min_qualification)->where('parent_id', '!=', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->min_qualification])->pluck('_id')->toArray(),
			'max_qualification_stream' => is_array($this->max_qualification) ? DegreeMst::whereIn('_id', $this->max_qualification)->where('parent_id', '!=', '0')->pluck('_id')->toArray() : DegreeMst::whereIn('_id', [$this->max_qualification])->pluck('_id')->toArray(),
			'qualifications' => $this->qualifications,
			'qualifications_name' => !empty($this->qualifications) ? Course::whereIn('_id', $this->qualifications)->pluck('name')->toArray() : [],
			'currency' => $this->currency,
			'currency_name' => new CurrencyData($this->currencydata),
			'salary_type' => $this->salary_type,
			'min_monthly_salary' => $this->min_monthly_salary,
			'max_monthly_salary' => $this->max_monthly_salary,
			'language_pref_id' => $this->language()->pluck('_id')->toArray(),
			'language_pref_name' => $this->language()->pluck('language_name')->toArray(),
			'edu_and_or' => $this->edu_and_or,
			'interview_name' => $this->interview_name,
			'interview_type_id' => $this->interview_type_id,
			'interview_type' => $this->interviewType,
			'interview_date' => $this->interview_date,
			'interview_dates' => $this->dateArr($this->interview_date),
			'interview_start_time' => $this->interview_start_time,
			'interview_end_time' => $this->interview_end_time,
			'interview_time_slot' => $this->interview_time_slot,
			'time_slots' => $this->getTimeSlots($this->interview_start_time, $this->interview_end_time, $this->interview_time_slot),
			'interview_vanue_name' => $this->interview_vanue_name,
			'interview_location_country' => $this->interview_location_country,
			'interview_location_country_name' => new CountryHomeResource($this->country),
			'interview_location_city' => new CityHomeResource($this->interViewLocation),
			'interview_venue_address' => $this->interview_venue_address,
			'walkin_location_type' => $this->walkin_location_type,
			'status' => $this->status,
			'created_by' => $this->created_by,
			'updated_by' => $this->updated_by,
			'created_at' => Carbon::parse($this->created_at)->format('d/m/Y'),
			'day_diff' => Carbon::now()->diffInDays($this->created_at),
			'applied_interview_count' => $this->interviewSlotBooked->count(),
			'benefits' => $this->benefits,
			'primary_responsibility' => $this->primary_responsibility,
			'special_notes' => $this->special_notes,
			'is_published' => $this->is_published == 1 ? 1 : 0,
			'published_date' => $this->published_date ? Carbon::parse($this->published_date)->format('d/m/Y') : '',
			'is_republished' => $this->is_republished,
			'republished_date' => $this->republished_date ? Carbon::parse($this->republished_date)->format('d/m/Y') : '',
			'bsb_jb_trans_id' => $this->bsb_jb_trans_id,
			'candidate_type' => $this->candidate_type,
			'candidate_type_text' => $this->candidate_type == 1 ? 'Male female both' : ($this->candidate_type == 2 ? 'Only for Male' : ($this->candidate_type == 3 ? 'Only for Female' : '')),
			'job_lat' => $this->job_lat,
			'job_long' => $this->job_lat,
			'condition_type' => $this->condition_type,
			'conditions' => $this->conditions,

		];
	}

	protected function getDates($dates) {
		$dateArray = [];
		$date = explode(',', $dates);
		$start = Carbon::parse($date[0]);
		$diff = $start->diffInDays(Carbon::parse($date[1]));
		for ($i = 0; $i <= $diff; $i++) {
			array_push($dateArray, Carbon::parse($date[0])->addDay($i)->format('d/m/Y'));
		}
		return $dateArray;
	}
	protected function dateArr($date) {
		$dateArray = explode(',', str_replace(' ', '', $date));
		return $dateArray;
	}

	protected function getTimeSlots($start_time, $end_time, $interv_slot) {
		$start = Carbon::parse($start_time);
		$end = Carbon::parse($end_time);
		$diff_in_minutes = $end->diffInMinutes($start);
		$slotArray = [];
		$diffSlot = $diff_in_minutes / $interv_slot;
		for ($i = 0; $i <= $diffSlot; $i++) {
			array_push($slotArray, Carbon::parse($start_time)->addMinutes($interv_slot * $i)->format('H:i'));
		}
		return $slotArray;
	}
}
