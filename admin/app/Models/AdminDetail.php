<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AdminDetail extends Eloquent {

	protected $connection = 'mongodb';
	protected $collection = 'admin_details';

	protected $fillable = [
		'user_id', 'first_name', 'last_name', 'address', 'profile_image',
	];

	public function user() {
		return $thsi->belongsTo(User::class, 'user_id', 'id');
	}
}
