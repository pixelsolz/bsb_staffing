<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AdminPaymentConfigTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'admin_payment_config_txn';

	protected $fiillable = ['service_id', 'payment_flag', 'date_from', 'date_to', 'remarks', 'created_by', 'updated_by'];

	public function service() {
		return $this->belongsTo(ServicesMst::class, 'service_id', 'id');
	}
}
