<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AgencyDetail extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'agency_details_txn';
	protected $fillable = ['user_id', 'comp_name', 'comp_website', 'country_id', 'city_id', 'address', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'dob_date', 'dob_month', 'dob_year', 'gender', 'registry_certificate', 'govt_id', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_portal_address', 'profile_image'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}

	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}

}
