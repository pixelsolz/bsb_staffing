<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AllOtherMasterMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'all_other_master_msts';

	protected $fillable = ['name', 'entity', 'orders', 'status'];
}
