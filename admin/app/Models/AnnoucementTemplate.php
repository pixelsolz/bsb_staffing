<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AnnoucementTemplate extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'annoucement_template_mst';
	protected $fillable = ['temp_name', 'temp_content','entity_id','created_by', 'updated_by', 'status'];


	public function role() {
		return $this->belongsTo(Role::class, 'entity_id', '_id');
	}
}
