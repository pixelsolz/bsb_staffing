<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AnnounceMent extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'announcement_txn';

	protected $fillable = ['user_id', 'benefit_id', 'emails', 'subject_id', 'message', 'signature', 'complete_status', 'created_by', 'updated_by'];
}
