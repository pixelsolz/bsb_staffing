<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ApplicationPhaseMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'application_phase_mst';

	protected $fillable = ['phase_name', 'created_by', 'updated_by'];

}
