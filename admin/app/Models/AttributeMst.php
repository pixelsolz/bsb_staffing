<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AttributeMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'attribute_mst';

	protected $fillable = ['name', 'attr_type', 'status'];

	protected $appends = ['attr_type_text'];

	public function getAttrTypeTextAttribute() {
		$val = '';
		switch ($this->attr_type) {
		case 1:
			$val = 'Industry';
			break;
		case 2:
			$val = 'Under graduate degree';
			break;
		case 3:
			$val = 'Post graduate degree';
			break;
		default:
			// code...
			break;
		}

		return $val;
	}
}
