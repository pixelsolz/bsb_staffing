<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BannerImage extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'banner_images';

	protected $fillable = [
		'title', 'banner_image', 'status',
	];
}
