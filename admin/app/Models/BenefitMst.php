<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BenefitMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'benefit_msts';
	protected $fillable = ['title', 'benefit', 'subject', 'type','entity_id','created_by', 'updated_by', 'status'];
	protected $appends = ['benefit_type'];

	public function createdUser() {
		return $this->belongsTo(User::class, 'created_by', '_id');
	}

	public function role() {
		return $this->belongsTo(Role::class, 'entity_id', '_id');
	}

	public function getBenefitTypeAttribute() {
		$value = '';
		if ($this->type == 1) {
			$value = 'Referal';
		} else {
			$value = 'Announcement';
		}
		return $value;
	}
}
