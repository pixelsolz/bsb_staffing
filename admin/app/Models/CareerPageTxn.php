<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CareerPageTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'career_page_txn';

	protected $fillable = ['user_id', 'company_logo', 'header_color', 'footer_color', 'background_color', 'text_color', 'page_headline', 'about_company', 'contact_info','company_name'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function carrerPhotos() {
		return $this->hasMany(CareerPhoto::class, 'user_id', 'user_id');
	}

	
}
