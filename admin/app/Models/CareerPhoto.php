<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CareerPhoto extends Eloquent {
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $connection = 'mongodb';
	protected $collection = 'career_photos';

	protected $fillable = [
		'user_id', 'name',
	];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}
