<?php

namespace App\Models;
use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CityMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'city_mst';

	protected $fillable = [
		'country_code', 'name', 'lat', 'lng', 'seo_title', 'seo_description', 'seo_keywords', 'slug',
	];

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_code', 'code');
	}

	public function jobs() {
		return $this->hasMany(EmployerJobTxn::class, 'city_id', '_id');
	}

	public function internationalJobs() {
		return $this->belongsToMany(InternationalJobTxn::class, 'null', 'cities', 'international_job_id');
	}

	public function walkingJobs() {
		return $this->hasMany(WalkInInterviewTxn::class, 'interview_location_city', '_id');
	}

	public function employeeJobPreference() {
		return $this->belongsToMany(EmployeePrefTxn::class, null, 'city_id', 'employee_pref_id');
	}

	public function employeeCity() {
		return $this->hasOne(EmployeeProfileTxn::class, 'city_id', '_id');
	}

	public function totalJobCountByCity() {
		$job_count = 0;
		if ($this->internationalJobs) {
			$job_count += $this->internationalJobs->where('job_status', 1)->count();
		}
		if ($this->jobs) {
			$job_count += $this->jobs->where('job_status', 1)->where('is_published', 1)->count();
		}
		if ($this->walkingJobs) {
			$job_count += $this->walkingJobs->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->count();
		}
		return $job_count;
	}
}
