<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CollegeDetail extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'college_details_txn';
	protected $fillable = ['user_id', 'name', 'website', 'country_id', 'city_id', 'address', 'logo', 'gallery_images', 'total_student', 'total_student_place_per_year', 'about_college', 'special_note_employer', 'contact_person_name', 'contact_person_lastname', 'contact_person_designation', 'contact_person_email', 'contact_person_phone', 'dob_date', 'dob_month', 'dob_year', 'gender', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_postal_address', 'mission', 'vision', 'institute_video',
	];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}

	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}

}
