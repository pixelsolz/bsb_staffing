<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CompanySizeMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'company_size_mst';

	protected $fillable = ['company_size', 'status', 'created_by', 'updated_by'];
}
