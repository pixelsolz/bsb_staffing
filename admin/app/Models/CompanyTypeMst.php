<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CompanyTypeMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'company_type_mst';
	protected $fillable = ['company_type', 'status', 'created_by', 'updated_by'];
}
