<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CountryImportMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'country1_mst';

	protected $fillable = [
		'name', 'code', 'dial_code', 'currency', 'image',
	];

}
