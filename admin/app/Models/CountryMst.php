<?php

namespace App\Models;
use App\Models\InternationalJobTxn;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CountryMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'country_mst';

	protected $fillable = [
		'name', 'code', 'dial_code', 'currency', 'currency_code', 'image', 'seo_title', 'seo_description', 'seo_keywords', 'slug',
	];

	protected $appends = ['job_count', 'international_job_count', 'walking_job_count'];

	public function cities() {
		return $this->hasMany(CityMst::class, 'country_id', '_id');
	}
	public function jobs() {
		return $this->hasMany(EmployerJobTxn::class, 'country_id', '_id');
	}

	public function internationalJobs() {
		return $this->belongsToMany(InternationalJobTxn::class, 'null', 'countries', 'international_job_id');
	}

	public function walkingJobs() {
		return $this->hasMany(WalkInInterviewTxn::class, 'interview_location_country', '_id');
	}

	public function employeeJobPreference() {
		return $this->belongsToMany(EmployeePrefTxn::class, null, 'country_id', 'employee_pref_id');
	}

	public function employeeCountry() {
		return $this->hasOne(EmployeeProfileTxn::class, 'country_id', '_id');
	}

	public function getInternationalJobCountAttribute() {
		return $this->internationalJobs->where('job_status', 1)->count();
	}

	public function getWalkingJobCountAttribute() {
		return $this->walkingJobs->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->count();
	}

	public function getJobCountAttribute() {
		$user = auth('api')->user();
		if (!$user) {
			return 0;
		}
		$job_count = 0;

		if ($this->internationalJobs) {
			$job_count += $this->internationalJobs->where('job_status', 1)->count();
		}
		if ($this->jobs) {
			$job_count += EmployerJobTxn::where('country_id', $this->_id)->whereDoesntHave('employeeAppliedJob', function ($q) use ($user) {
				$q->where('employee_id', $user->_id);
			})->where('job_status', 1)->where('is_published', 1)->count();
			//$job_count += $this->jobs->where('job_status', 1)->where('is_published', 1)->count();
		}
		if ($this->walkingJobs) {
			$job_count += $this->walkingJobs->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->count();
		}
		return $job_count;
	}
}
