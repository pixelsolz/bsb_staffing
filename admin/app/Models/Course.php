<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Course extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'courses_mst';

	protected $fillable = ['name', 'status'];
}
