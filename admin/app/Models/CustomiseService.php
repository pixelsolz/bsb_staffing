<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CustomiseService extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'customise_service';

	protected $fillable = [
		'name', 'designation', 'email', 'phone_no', 'skype_id', 'service_type', 'company_name', 'company_website', 'queries', 'created_by', 'updated_by',
	];

}
