<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DegreeMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'degrees_mst';

	protected $fillable = ['name', 'attribute_id', 'parent_id', 'status'];

	protected $appends = ['attr_name', 'parent_name'];

	public function attribute() {
		return $this->belongsTo(AttributeMst::class, 'attribute_id', '_id');
	}

	public function getAttrNameAttribute() {
		return $this->attribute->name;
	}

	public function child() {
		return $this->hasMany(self::class, 'parent_id', '_id');
	}

	public function parent() {
		return $this->hasOne(self::class, '_id', 'parent_id');
	}

	public function getParentNameAttribute() {
		if ($this->parent_id == 0) {
			return 'root';
		} else {
			return $this->parent->name;
		}

	}
}
