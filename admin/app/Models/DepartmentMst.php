<?php

namespace App\Models;
use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DepartmentMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'department_mst';

	protected $fillable = ['name', 'parent_id', 'created_by', 'updated_by'];

	public function parentDepartment() {
		return $this->belongsTo(DepartmentMst::class, 'parent_id');
	}

	public function childrenDepartment() {
		return $this->hasMany(DepartmentMst::class, 'parent_id');
	}

	public function employeeJobPreference() {
		return $this->belongsToMany(EmployeePrefTxn::class, null, 'department_id', 'employee_pref_id');
	}

	public function jobs() {
		return $this->belongsToMany(EmployerJobTxn::class, 'null', 'department_id', 'job_id');
	}
	public function internationalJobs() {
		return $this->belongsToMany(InternationalJobTxn::class, 'null', 'department_id', 'job_id');
	}

	public function walkingJobs() {
		return $this->hasMany(WalkInInterviewTxn::class, 'department_id', '_id');
	}

	public function totalJobCountByDepartment() {
		$job_count = 0;
		if ($this->internationalJobs) {
			$job_count += $this->internationalJobs->where('job_status', 1)->count();
		}
		if ($this->job) {
			$job_count += $this->job->where('job_status', 1)->where('is_published', 1)->count();
		}
		if ($this->walkingJobs) {
			$job_count += $this->walkingJobs->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->count();
		}
		return $job_count;
	}

}
