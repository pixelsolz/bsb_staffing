<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeApplicationStatusHistTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_application_status_hist_txn';

	protected $fillable = ['applied_status', 'hist_date', 'employer_comment', 'employee_application_id', 'email_id', 'created_by', 'updated_by'];

	public function empApplication() {
		return $this->belongsTo(EmployeeAppliedJobsTxn::class, 'employee_application_id', '_id');
	}
}
