<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeAppliedJobsTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_applied_jobs_txn';

	protected $fillable = ['job_id', 'applied_date', 'applied_status', 'bsb_app_id', 'employee_id', 'apply_user_type', 'job_questions', 'created_by', 'updated_by'];

	protected $appends = ['bsb_jb_app_id'];

	public function job() {
		return $this->belongsTo(EmployerJobTxn::class, 'job_id', '_id');
	}

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function applicationStatus() {
		return $this->belongsTo(ApplicationPhaseMst::class, 'applied_status', '_id');
	}

	public function getBsbJbAppIdAttribute() {
		if ($this->bsb_app_id) {
			return 'BSBJBAPP' . '-' . $this->bsb_app_id;
		} else {
			return 'BSBJBAPP';
		}

	}

	// public function applicationStatusHist(){
	// 	return $this->hasMany(EmployeeApplicationStatusHistTxn::class, 'employee_application_id', '_id');
	// }

}
