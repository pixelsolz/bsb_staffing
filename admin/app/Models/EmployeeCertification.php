<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Storage;

class EmployeeCertification extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_certifications_txn';

	protected $fillable = ['user_id', 'course_name', 'institute_name', 'details', 'certification_date', 'institute_logo', 'about_certification'];

	protected $appends = ['institute_logo_url'];

	public function getInstituteLogoUrlAttribute() {
		return $this->institute_logo ? Storage::disk('s3')->url('upload_files/images/' . $this->institute_logo) : '';
	}
}
