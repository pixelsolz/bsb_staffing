<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeCvTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_cv_txn';

	protected $fillable = ['employee_id', 'cv_path'];

	protected $appends = ['cv_full_path'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', 'id');
	}

	public function getCvFullPathAttribute() {
		return $this->cv_path ? url('public/upload_files/employee-resumes/' . $this->cv_path) : '';
	}
}
