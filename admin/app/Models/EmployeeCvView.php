<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeCvView extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_cv_view';

	protected $fillable = ['employee_id', 'employer_id', 'city_id', 'date_of_view', 'mail_id', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function employer() {
		return $this->belongsTo(User::class, 'employer_id', '_id');
	}

	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}
}
