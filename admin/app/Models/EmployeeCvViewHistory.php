<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeCvViewHistory extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_cv_view_history';

	protected $fillable = ['employee_id', 'employer_id', 'remarks'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function employer() {
		return $this->belongsTo(User::class, 'employer_id', '_id');
	}

}
