<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeEducation extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_educations_txn';

	protected $fillable = ['user_id', 'degree_id', 'degree_parent', 'course_degree_id', 'course_title', 'course_name', 'specialization', 'name_of_institute', 'institute_logo', 'institute_website', 'course_type', 'passout_year'];

	protected $appends = ['degree_name'];

	public function degree() {
		return $this->belongsTo(DegreeMst::class, 'degree_id', '_id');
	}

	public function degreeParentData() {
		return $this->belongsTo(DegreeMst::class, 'degree_parent', '_id');
	}

	public function courseDegree() {
		return $this->belongsTo(Course::class, 'course_degree_id', '_id');
	}

	public function getDegreeNameAttribute() {
		return $this->degree ? $this->degree->name : '';
	}
}
