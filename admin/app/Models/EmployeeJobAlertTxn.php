<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class EmployeeJobAlertTxn extends Eloquent {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $connection = 'mongodb';
	protected $collection = 'employee_job_alert_txn';

	protected $fillable = ['emp_id', 'job_id', 'alert_type', 'status', 'created_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'emp_id', '_id');
	}

	public function job() {
		return $this->belongsTo(EmployerJobTxn::class, 'job_id', '_id');
	}

}
