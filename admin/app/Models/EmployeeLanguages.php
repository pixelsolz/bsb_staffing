<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeLanguages extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_languages_txn';

	protected $fillable = ['user_id', 'lang_id', 'proficiency'];

	protected $appends = ['language_name', 'proficiency_text'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function language() {
		return $this->belongsTo(LanguageMst::class, 'lang_id', '_id');
	}

	public function getLanguageNameAttribute() {
		return $this->language ? $this->language->language_name : '';
	}

	public function getProficiencyTextAttribute() {
		$value = '';
		switch ($this->proficiency) {
		case 1:
			$value = 'elementary proficiency';
			break;
		case 2:
			$value = 'Limited Working proficiency';
			break;
		case 3:
			$value = 'Full Professional proficiency';
			break;
		case 4:
			$value = 'Native proficiency';
			break;
		default:
			// code...
			break;
		}
		return $value;
	}
}
