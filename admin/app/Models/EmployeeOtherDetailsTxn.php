<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeOtherDetailsTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_other_details_txn';

	protected $fillable = ['employee_id', 'cv_like_count', 'cv_locked', 'new_employee', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', 'id');
	}
}
