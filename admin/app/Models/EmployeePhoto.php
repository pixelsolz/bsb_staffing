<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeePhoto extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_photos_txn';

	protected $fillable = ['user_id', 'description', 'photo'];
}
