<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeePmtHistoryTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_pmt_history_txn';

	protected $fillable = ['employee_id', 'payment_made_on', 'payment_amount', 'payment_mode', 'service_id', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', 'id');
	}

	public function service() {
		return $this->belongsTo(ServicesMst::class, 'service_id', 'id');
	}
}
