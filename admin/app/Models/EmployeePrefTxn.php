<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeePrefTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_pref_txn';

	protected $fillable = ['employee_id', 'job_location', 'job_role_id', 'job_type', 'salary_type', 'currency_type', 'min_salary', 'max_salary', 'designation', 'created_by', 'updated_by'];

	protected $appends = ['cities_name'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function countries() {
		return $this->belongsToMany(CountryMst::class, null, 'employee_pref_id', 'country_id');
	}

	public function cities() {
		return $this->belongsToMany(CityMst::class, null, 'employee_pref_id', 'city_id');
	}

	public function getCitiesNameAttribute() {
		return $this->cities;
	}

	public function jobRole() {
		return $this->belongsTo(JobRoleMst::class, 'job_role_id', '_id');
	}

	public function industries() {
		return $this->belongsToMany(IndustryMst::class, null, 'employee_pref_id', 'industry_id');
	}

	public function departments() {
		return $this->belongsToMany(DepartmentMst::class, null, 'employee_pref_id', 'department_id');
	}

	public function shiftTypes() {
		return $this->belongsToMany(AllOtherMasterMst::class, null, 'employee_pref_id', 'shift_id');
	}

	public function minSalary() {
		return $this->belongsTo(AllOtherMasterMst::class, 'min_salary', '_id');
	}

	public function maxSalary() {
		return $this->belongsTo(AllOtherMasterMst::class, 'max_salary', '_id');
	}

	public function currency() {
		return $this->belongsTo(CountryMst::class, 'currency_type', '_id');
	}

	public function employment() {
		return $this->belongsTo(AllOtherMasterMst::class, 'job_type', '_id');
	}

}
