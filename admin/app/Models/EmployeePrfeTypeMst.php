<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeePrfeTypeMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_prfe_type_mst';

	protected $fillable = ['pref_type', 'created_by', 'updated_by'];
}
