<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeProfileLikeTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_profile_like_txn';

	protected $fillable = ['employee_id', 'employer_id', 'status', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', 'id');
	}
}
