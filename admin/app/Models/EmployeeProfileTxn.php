<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeProfileTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_profile_txn';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'institute_code', 'first_name', 'middle_name', 'last_name', 'gender', 'date_of_birth', 'nationality', 'home_town', 'country_id', 'city_id', 'place', 'permanent_address', 'present_address', 'skype_id', 'cv', 'profile_picture', 'profile_title', 'total_experience_yr', 'total_experience_yr_value', 'total_experience_mn', 'notice_period', 'current_salary_currency', 'current_salary', 'current_salary_type', 'summary', 'maritual_status', 'std_course_id', 'designation', 'industry_id', 'dob_date', 'dob_month', 'dob_year', 'form_no', 'created_by', 'updated_by',
	];

	protected $appends = ['gender_text', 'married_status', 'full_name', 'profile_strength'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function getGenderTextAttribute() {
		if ($this->gender == 1 || $this->gender == 'male') {
			return 'Male';
		} elseif ($this->gender == 2 || $this->gender == 'female') {
			return 'Female';
		} else {
			return $this->gender ? 'Custom' : '';
		}

	}

	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}

	public function currency() {
		return $this->belongsTo(CountryMst::class, 'current_salary_currency', '_id');
	}

	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}
	public function industry() {
		return $this->belongsTo(IndustryMst::class, 'industry_id', '_id');
	}

	public function getMarriedStatusAttribute() {
		$value = '';
		switch ($this->maritual_status) {
		case 1:
			$value = 'Single';
			break;
		case 2:
			$value = 'Married';
			break;
		case 3:
			$value = 'Unmarried';
			break;
		default:
			// code...
			break;
		}
		return $value;
	}

	public function totExprYr() {
		return $this->belongsTo(ExperienceMst::class, 'total_experience_yr', '_id');
	}

	public function totExpMn() {
		return $this->belongsTo(AllOtherMasterMst::class, 'total_experience_mn', '_id');
	}

	public function noticePeriod() {
		return $this->belongsTo(AllOtherMasterMst::class, 'notice_period', '_id');
	}

	public function institute() {
		return $this->belongsTo(User::class, 'user_unique_code', 'institute_code');
	}

	public function studentCourse() {
		return $this->belongsToMany(Course::class, 'null', 'emp_detail_id', 'std_course_id');
	}

	public function getProfileStrengthAttribute() {
		//Start count Profile Strength
		$count_profle_strength = 10;
		if ($this->profile_picture != '') {
			$count_profle_strength += 9;
		}
		if (!empty($this->user->employeeCvTxn)) {
			$count_profle_strength += 9;
		}
		//For Personal Information
		if (!empty($this->date_of_birth)) {
			$count_profle_strength += 9;
		}

		if (!empty($this->user->employeePrefTxn)) {
			$count_profle_strength += 9;
		}
		// Summary of your profile
		if (!empty($this->profile_title)) {
			$count_profle_strength += 9;
		}
		if (!empty($this->user->employeeEmployementHistory) && count($this->user->employeeEmployementHistory) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($this->user->employeeEducation) && count($this->user->employeeEducation) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($this->user->employeeSkill) && count($this->user->employeeSkill) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($this->user->employeeCertification) && count($this->user->employeeCertification) != 0) {
			$count_profle_strength += 9;
		}
		if (!empty($this->user->employeeLanguage) && count($this->user->employeeLanguage) != 0) {
			$count_profle_strength += 9;
		}

		return $count_profle_strength;
	}

	public function scopeGetDatewiseStudent($query, $todate, $fromdate) {
		if (!empty($todate) && !empty($fromdate)) {
			return $query->whereBetween('created_at', array($todate, $fromdate))->count();
		} else {
			return $query->count();
		}

	}
}
