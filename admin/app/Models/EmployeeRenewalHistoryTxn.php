<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeRenewalHistoryTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_renewal_history_txn';

	protected $fillable = ['employee_id', 'renewal_date', 'payment_id', 'opted_services_id', 'renewal_trigger', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', 'id');
	}

}
