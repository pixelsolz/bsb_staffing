<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class EmployeeSavedJobsTxn extends Eloquent {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $connection = 'mongodb';
	protected $collection = 'employee_saved_jobs_txn';

	protected $fillable = ['job_id', 'applied_date', 'applied_status', 'employee_id', 'employee_application_id', 'saved_on', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function job() {
		return $this->belongsTo(EmployerJobTxn::class, 'job_id', '_id');
	}

	public function empApplication() {
		return $this->belongsTo(EmployeeAppliedJobsTxn::class, 'employee_application_id', '_id');
	}
}
