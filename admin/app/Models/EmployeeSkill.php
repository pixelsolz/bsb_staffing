<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeSkill extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_skills_txn';

	protected $fillable = ['user_id', 'title'];
}
