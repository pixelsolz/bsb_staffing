<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeeVideoTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employee_videos_txn';

	protected $fillable = ['employee_id', 'description', 'video_link'];
}
