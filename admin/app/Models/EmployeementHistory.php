<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployeementHistory extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employment_histories_txn';

	protected $fillable = ['user_id', 'job_title', 'company_logo', 'company_name', 'company_website', 'industry_of_company', 'department', 'from_date', 'to_date', 'is_present_company', 'about_job_profile'];

	public function industry() {
		return $this->belongsTo(IndustryMst::class, 'industry_of_company', '_id');
	}

	public function department_names() {
		return $this->belongsTo(DepartmentMst::class, 'department', '_id');
	}
}
