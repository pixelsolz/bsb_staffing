<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployerCvSaveFolderTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employer_cv_save_to_folder_txn';

	protected $fillable = ['folder_id', 'employer_id','employee_id', 'created_by', 'updated_by'];


	/*public function employee()
	{
		return $this->belongsToMany(User::class, 'employee_id', '_id');
	}*/


}
