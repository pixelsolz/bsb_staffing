<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployerEmailTemplateTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employer_email_template_txn';

	protected $fillable = ['job_id', 'template_name', 'cv_received_email', 'subject', 'about_job', 'body', 'reviewed_flag', 'last_used_date', 'created_by', 'updated_by'];

	protected $appends = ['admin_reviewd'];

	public function createdUser() {
		return $this->belongsTo(User::class, 'created_by', '_id');
	}

	public function employerJob() {
		return $this->belongsTo(EmployerJobTxn::class, 'job_id', '_id');
	}

	public function getAdminReviewdAttribute() {
		$value = '';
		switch ($this->reviewed_flag) {
		case 1:
			$value = 'Approved';
			break;
		case 2:
			$value = 'Disapproved';
			break;
		default:
			$value = 'Pending';
			break;
		}
		return $value;

	}

}
