<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployerEmployeeOptedServicesTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employer_employee_opted_services_txn';
	protected $dates = ['service_expiry','service_opted_on'];

	protected $fillable = ['service_id', 'user_id', 'is_employee', 'is_employer', 'status', 'service_expiry', 'service_opted_on','limit_count','remaining_count','total_used','current_status', 'created_by', 'updated_by'];

	public function service() {
		return $this->belongsTo(ServicesMst::class, 'service_id', 'id');
	}

	public function employee() {
		if ($this->is_employee) {
			return $this->belongsTo(User::class, 'user_id', '_id');
		}

	}

	public function employer() {
		if ($this->is_employer) {
			return $this->belongsTo(User::class, 'user_id', '_id');
		}
	}
}
