<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployerFolderTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employer_folder_txn';

	protected $fillable = ['folder_name', 'employer_id', 'created_by', 'updated_by'];


	public function employeeCvSave()
	{
		return $this->belongsToMany(User::class, 'employee_id', '_id');
	}
	public function employerCvSavedFolder() {
		return $this->hasMany(EmployerCvSaveFolderTxn::class, 'folder_id', '_id');
	}


}
