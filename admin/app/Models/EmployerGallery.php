<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployerGallery extends Eloquent {
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $connection = 'mongodb';
	protected $collection = 'employer_galleries';

	protected $fillable = [
		'user_id', 'name', 'type',
	];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}
