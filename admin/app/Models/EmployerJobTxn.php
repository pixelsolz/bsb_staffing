<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class EmployerJobTxn extends Eloquent {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $connection = 'mongodb';
	protected $collection = 'employer_job_txn';

	protected $fillable = ['job_title', 'bsb_jb_id', 'job_desc', 'job_role_id', 'employer_id', 'industry_id', 'department_id', 'about_this_job', 'job_posted_to_be', 'employement_for', 'required_skill', 'primary_responsibility', 'special_notes',
		'country_id', 'city_id', 'benefits', 'qualifications', 'min_qualification', 'max_qualification', 'graduate_and_or_post_graduate', 'min_experiance', 'max_experiance', 'employment_type', 'number_of_vacancies', 'min_monthly_salary', 'max_monthly_salary', 'received_email', 'currency', 'salary_type', 'job_status', 'job_type', 'job_ageing', 'is_published', 'published_date', 'is_republished', 'republished_date', 'candidate_type', 'job_lat', 'job_long', 'job_location', 'condition_type', 'conditions', 'created_by', 'updated_by'];
	protected $appends = ['bsb_jb_trans_id'];

	public function jobRole() {
		return $this->belongsTo(JobRoleMst::class, 'job_role_id', '_id');
	}

	public function employer() {
		return $this->belongsTo(User::class, 'employer_id', '_id');
	}

	public function industry() {
		return $this->belongsToMany(IndustryMst::class, 'employer_job_industry_txn', 'job_id', 'industry_id')->withPivot('type');
	}

	public function department() {
		return $this->belongsToMany(DepartmentMst::class, 'null', 'job_id', 'department_id')->withPivot('type');
	}

	public function language() {
		return $this->belongsToMany(LanguageMst::class, 'language_jobs', 'job_txn_id', 'language_id')->withTimestamps()->withPivot('type');
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}
	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}
	public function employmentType() {
		return $this->belongsTo(AllOtherMasterMst::class, 'employment_type', '_id');
	}

	public function minExperiance() {
		return $this->belongsTo(ExperienceMst::class, 'min_experiance', '_id');
	}
	public function maxExperiance() {
		return $this->belongsTo(ExperienceMst::class, 'max_experiance', '_id');
	}
	public function employmentFor() {
		return $this->belongsTo(AllOtherMasterMst::class, 'employement_for', '_id');
	}

	/*public function minSalary() {
		return $this->belongsTo(AllOtherMasterMst::class, 'min_monthly_salary', '_id');
	}*/

	/*public function maxSalary() {
		return $this->belongsTo(AllOtherMasterMst::class, 'min_monthly_salary', '_id');
	}*/

	public function currencydata() {
		return $this->belongsTo(CountryMst::class, 'currency', '_id');
	}

	public function employeeSavedJob() {
		return $this->hasMany(EmployeeSavedJobsTxn::class, 'job_id', '_id');
	}

	public function employeeAppliedJob() {
		return $this->hasMany(EmployeeAppliedJobsTxn::class, 'job_id', '_id');
	}

	public function empJobAlert() {
		return $this->hasMany(EmployeeJobAlertTxn::class, 'job_id', '_id');
	}

	public function jobViews() {
		return $this->belongsToMany(User::class, null, 'job_id', 'user_id');
	}

	public function employerEmailTemplate() {
		return $this->hasOne(EmployerEmailTemplateTxn::class, 'job_id', '_id');
	}

	public function templateJobSend() {
		return $this->belongsToMany(User::class, null, 'job_id', 'temp_send_emp_id');
	}

	public function getBsbJbTransIdAttribute() {
		if ($this->bsb_jb_id) {
			return 'BSBJBID' . '-' . $this->bsb_jb_id;
		} else {
			return 'BSBJBID';
		}

	}

}
