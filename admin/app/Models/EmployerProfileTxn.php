<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmployerProfileTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'employer_profile_txn';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'first_name', 'middle_name', 'last_name', 'company_name', 'industry_id', 'address', 'city_id', 'zipcode', 'country_id', 'company_logo', 'company_main_image', 'alternate_ph_no', 'company_type', 'company_size', 'company_tagline', 'company_short_desc', 'element', 'nearby_place', 'gender', 'date_of_birth',
		'category', 'designation', 'profile_picture', 'company_email', 'company_website', 'business_licence_type', 'license_agree', 'lat', 'lng', 'business_licence','referral_code','dob_date','dob_month','dob_year','gender','created_by', 'updated_by',
	];

	protected $appends = ['gender_text', 'is_complete', 'full_name'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function getGenderTextAttribute() {
		return ($this->gender = 1) ? 'Male' : 'Female';
	}

	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name ? $this->last_name : '';
	}

	public function industry() {
		return $this->industry_id ? $this->belongsTo(IndustryMst::class, 'industry_id', '_id') : 0;
	}

	public function companyType() {
		return $this->belongsTo(CompanyTypeMst::class, 'company_type', '_id');
	}
	public function companySize() {
		return $this->belongsTo(CompanySizeMst::class, 'company_size', '_id');
	}

	public function getIsCompleteAttribute() {
		return $this->company_name ? 1 : 0;
	}
	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}
	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}

}
