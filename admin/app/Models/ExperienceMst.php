<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ExperienceMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'experience_mst';

	protected $fillable = [
		'name', 'value', 'order', 'status',
	];

	
}
