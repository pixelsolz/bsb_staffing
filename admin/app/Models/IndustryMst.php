<?php

namespace App\Models;
use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class IndustryMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'industry_mst';

	protected $fillable = ['name', 'attribute_id', 'service_id', 'status', 'created_by', 'updated_by', 'seo_title', 'seo_description', 'seo_keywords', 'slug'];

	protected $appends = ['attr_name'];

	public function services() {
		return $this->belongsTo(ServicesMst::class, 'service_id', '_id');
	}

	public function createdUser() {
		return $this->belongsTo(User::class, 'created_by', '_id');
	}

	public function attribute() {
		return $this->belongsTo(AttributeMst::class, 'attribute_id', '_id');
	}

	public function getAttrNameAttribute() {
		//return $this->attribute()->first()->name;
	}

	public function job() {
		return $this->belongsToMany(EmployerJobTxn::class, 'null', 'industry_id', 'job_id');
	}
	public function internationalJobs() {
		return $this->belongsToMany(InternationalJobTxn::class, 'null', 'industry_id', 'job_id');
	}

	public function walkingJobs() {
		return $this->hasMany(WalkInInterviewTxn::class, 'industry_id', '_id');
	}

	public function employeeJobPreference() {
		return $this->belongsToMany(EmployeePrefTxn::class, null, 'industry_id', 'employee_pref_id');
	}
	public function totalJobCountByIndustry() {
		$job_count = 0;
		if ($this->internationalJobs) {
			$job_count += $this->internationalJobs->where('job_status', 1)->count();
		}
		if ($this->job) {
			$job_count += $this->job->where('job_status', 1)->where('is_published', 1)->count();
		}
		if ($this->walkingJobs) {
			$job_count += $this->walkingJobs->where('interview_date', '>=', Carbon::now()->format('Y-m-d'))->where('status', 1)->count();
		}
		return $job_count;
	}
}
