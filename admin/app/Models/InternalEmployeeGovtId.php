<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class InternalEmployeeGovtId extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'internal_employee_govtid_txn';

	protected $fillable = ['user_id', 'file_name'];
}
