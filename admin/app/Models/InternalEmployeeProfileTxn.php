<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class InternalEmployeeProfileTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'internal_employee_profile_txn';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'first_name', 'middle_name', 'last_name', 'user_role', 'office_name', 'address', 'city_id', 'zipcode', 'country_id', 'company_logo', 'alternate_ph_no', 'gender', 'date_of_joining', 'designation', 'govt_id', 'profile_picture', 'company_email', 'lat', 'lng', 'created_by', 'updated_by', 'status',
	];

	protected $appends = ['gender_text', 'is_complete', 'full_name'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function getGenderTextAttribute() {
		return ($this->gender = 1) ? 'Male' : 'Female';
	}

	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name ? $this->last_name : '';
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}
	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}

}
