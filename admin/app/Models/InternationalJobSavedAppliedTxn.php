<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class InternationalJobSavedAppliedTxn extends Eloquent {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $connection = 'mongodb';
	protected $collection = 'international_job_saved_applied_txn';

	protected $fillable = ['international_job_id', 'applied_date', 'apply_user_type', 'applied_status', 'bsb_app_id', 'employee_id', 'saved_on', 'job_questions', 'created_by', 'updated_by'];

//apply_user_type==1'domestic',apply_user_type==2'international'

	protected $appends = ['bsb_jb_app_id'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function job() {
		return $this->belongsTo(InternationalJobTxn::class, 'international_job_id', '_id');
	}

	public function applicationStatus() {
		return $this->belongsTo(ApplicationPhaseMst::class, 'applied_status', '_id');
	}

	public function getBsbJbAppIdAttribute() {
		if ($this->bsb_app_id) {
			return 'BSBINTJBAPP' . '-' . $this->bsb_app_id;
		} else {
			return 'BSBINTJBAPP';
		}
	}

}
