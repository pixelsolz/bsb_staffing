<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class InterviewSlotBook extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'interview_slot_book_txn';

	protected $fillable = ['interview_id', 'user_id', 'booked_date', 'booked_time', 'apply_user_type', 'bsb_app_id', 'job_questions'];
	protected $appends = ['bsb_jb_app_id'];

	public function interview() {
		return $this->belongsTo(WalkInInterviewTxn::class, 'interview_id', '_id');
	}

	public function job() {
		return $this->belongsTo(WalkInInterviewTxn::class, 'interview_id', '_id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function employee() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function getBsbJbAppIdAttribute() {
		if ($this->bsb_app_id) {
			return 'BSBWOAPP' . '-' . $this->bsb_app_id;
		} else {
			return 'BSBWOAPP';
		}
	}

}
