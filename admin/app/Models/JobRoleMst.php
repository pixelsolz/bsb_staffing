<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class JobRoleMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'job_role_mst';

	protected $fillable = ['role_name', 'created_by', 'updated_by'];
}
