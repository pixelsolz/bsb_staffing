<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class JobSectorMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'job_sector_mst';

	protected $fillable = ['sector_name', 'created_by', 'updated_by'];

	public function user() {
		return $this->belongsto(User::class, 'created_by', '_id');
	}
}
