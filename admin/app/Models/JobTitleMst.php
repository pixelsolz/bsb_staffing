<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class JobTitleMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'job_title_mst';

	protected $fillable = ['job_title'];
}
