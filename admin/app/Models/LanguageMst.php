<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class LanguageMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'language_mst';

	protected $fillable = ['language_name', 'created_by', 'updated_by'];

	public function user() {
		return $this->belongsTo(User::class, 'created_by', '_id');
	}
}
