<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MaasTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'maas_txn';

	protected $fillable = ['mail_template_id', 'send_to_employee_id', 'send_on', 'additional_message', 'created_by', 'updated_by'];

	public function mailTemplate() {
		return $this->belongsTo(EmployerEmailTemplateTxn::class, 'mail_template_id', 'id');
	}

	public function sendedUser() {
		return $this->belongsTo(User::class, 'send_to_employee_id', 'id');
	}
}
