<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MailLabel extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'mail_label';
	protected $fillable = ['user_id', 'label_name', 'parent_id', 'created_by'];

	public function mail() {
		return $this->belongsToMany(Mail::class, 'null', 'user_labels', 'label_id');
	}

}
