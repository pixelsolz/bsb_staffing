<?php

namespace App\Models;
use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class MailTxn extends Eloquent {
	use SoftDeletes;
	protected $connection = 'mongodb';
	protected $collection = 'mail_txn';
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	];
	protected $fillable = ['mail_sender_id', 'mail_receive_id', 'parent_id', 'mail_subject', 'mail_body', 'is_star', 'mail_status', 'attached_file', 'attached_image', 'job_questions', 'label_id', 'video_links', 'template_id', 'is_resume_trans'];
	protected $appends = ['mail_summary', 'sender_name', 'sender_email', 'created_date'];

	public function children() {
		return $this->hasMany(self::class, 'parent_id', '_id');
	}

	public function parent() {
		return $this->belongsTo(self::class, 'parent_id');
	}

	public function mailSender() {
		return $this->belongsTo(User::class, 'mail_sender_id', '_id');
	}

	public function mailLabelUser() {
		return $this->belongsToMany(MailLabel::class, null, 'label_id', 'user_labels');
	}

	public function mailDeleteUser() {
		return $this->belongsToMany(User::class, null, 'user_id', 'delete_users');
	}

	public function mailStaredUser() {
		return $this->belongsToMany(User::class, null, 'user_id', 'star_users');
	}

	public function getCreatedDateAttribute() {
		return Carbon::now()->diffInDays($this->created_at) == 0 ? 'Today' : Carbon::now()->diffInDays($this->created_at) . ' days ago';
	}

	public function getSenderNameAttribute() {
		return $this->getUserDetail($this->mailSender->roles()->first(), $this->mailSender);
	}

	public function getSenderEmailAttribute() {
		return $this->mailSender->email;
	}

	public function mailReceiver() {
		return $this->belongsTo(User::class, 'mail_receive_id', '_id');
	}

	public function getMailSummaryAttribute() {
		return implode(' ', array_slice(explode(' ', $this->mail_body), 0, 5));
	}

	public function label() {
		return $this->belongsTo(MailLabel::class, 'label_id', '_id');
	}

	public function emailTemplate() {
		return $this->belongsTo(EmployerEmailTemplateTxn::class, 'template_id', '_id')->with('employerJob');
	}

	public function getUserDetail($role, $detail) {
		$data = '';
		switch ($role->slug) {
		case 'employee':
			$data = $detail->employeeUser->full_name;
			break;
		case 'employer':
			$data = $detail->employerUser->full_name;
			break;
		case 'college':
			$data = $detail->collegeUser->name;
			break;
		case 'agency':
			$data = $detail->agencyUser->contact_person_name;
			break;
		case 'franchise':
			$data = $detail->franchiseUser->contact_person_name;
			break;
		default:
			// code...
			break;
		}

		return $data;
	}

}
