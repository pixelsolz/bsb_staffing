<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MyAlertTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'my_alert_txn';
	protected $fillable = ['user_id', 'keyword', 'salary_type', 'currency', 'salary', 'experiance', 'email_id', 'name_of_alert','job_title','location'];

	public function employee() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function country() {
		return $this->belongsToMany(CountryMst::class, 'null', 'alert_id', 'country_id');
	}

	public function city() {
		return $this->belongsToMany(CityMst::class, 'null', 'alert_id', 'city_id');
	}

	public function department() {
		return $this->belongsToMany(DepartmentMst::class, 'null', 'alert_id', 'department_id');
	}

	public function industry() {
		return $this->belongsToMany(IndustryMst::class, 'null', 'alert_id', 'industry_id');
	}

}
