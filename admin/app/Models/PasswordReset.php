<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PasswordReset extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'password_reset';

	protected $fillable = ['email', 'token'];

}
