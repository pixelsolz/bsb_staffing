<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PaymentConfig extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'payment_configuration';

	protected $fillable = ['premium_job_post', 'international_walk_in_interview','cv_download_view','mass_mail_service','employee','employer'];

	
}
