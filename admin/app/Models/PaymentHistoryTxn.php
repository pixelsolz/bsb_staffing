<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PaymentHistoryTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'payment_history_txn';
	protected $dates = ['payment_made_on'];
	protected $fillable = ['user_id', 'service_id', 'payment_made_on', 'payment_amount', 'payment_mode', 'remarks', 'created_by', 'updated_by'];

	public function employer() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function service() {
		return $this->belongsTo(ServicesMst::class, 'service_id', '_id');
	}
}
