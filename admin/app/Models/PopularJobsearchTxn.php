<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PopularJobsearchTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'popular_job_search_txn';
	protected $fillable = ['search_key', 'count_search_key'];


}
