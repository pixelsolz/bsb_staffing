<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ReferalTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'referal_txn';

	protected $fillable = ['user_id', 'benefit_id', 'referal_name', 'referal_email', 'referal_designation', 'referal_company', 'referal_company_website', 'message', 'is_registered', 'status'];

	protected $appends = ['status_text'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function getStatusTextAttribute() {
		return $this->status == 1 ? 'Not Used' : 'Used';
	}
}
