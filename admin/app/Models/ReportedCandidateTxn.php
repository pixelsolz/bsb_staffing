<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ReportedCandidateTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'reported_candidate_txn';

	protected $fillable = ['employee_id', 'employer_id', 'report_candidate_option', 'incorrect_email_id', 'incorrect_mobile_no','mobile_not_reachable','incorrect_profile_or_resume', 'profile_not_updated'];

	public function employee() {
		return $this->belongsTo(User::class, 'employee_id', '_id');
	}

	public function employer() {
		return $this->belongsTo(User::class, 'employer_id', '_id');
	}

	
}
