<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RoleUser extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'role_users';

	protected $fillable = ['user_id', 'role_id'];
}
