<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SearchHistory extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'search_histories';

	protected $fillable = ['search_key', 'search_data', 'search_type', 'is_saved', 'saved_text', 'created_by', 'updated_by'];

	public function employeer() {
		return $this->belongsTo(User::class, 'created_by', '_id');
	}
}
