<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ServicesMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'services_mst';

	protected $fillable = ['service_name', 'service_type', 'quantity', 'service_desc', 'fees', 'service_life', 'special_service', 'needs_broadcast', 'entity_id', 'order_of_results', 'status', 'created_by', 'created_by'];

	protected $appends = ['role_name'];

	public function role() {
		return $this->belongsTo(Role::class, 'entity_id', '_id');
	}

	public function getRoleNameAttribute() {
		return $this->role ? $this->role->name : '';
	}

}
