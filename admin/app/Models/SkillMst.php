<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SkillMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'skill_mst';

	protected $fillable = ['skill'];
}
