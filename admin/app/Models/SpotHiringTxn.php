<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SpotHiringTxn extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'spot_hiring_txn';

	protected $fillable = ['employer_id', 'service_id', 'hiring_date', 'hiring_time', 'location', 'position', 'total_positions', 'total_vacancy', 'reviewed', 'created_by', 'updated_by'];

	public function employee() {
		return $this->belongsTo(User::class, 'employer_id', 'id');
	}

	public function service() {
		return $this->belongsTo(ServicesMst::class, 'service_id', 'id');
	}
}
