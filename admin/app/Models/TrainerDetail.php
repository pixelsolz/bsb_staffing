<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TrainerDetail extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'trainer_details';

	protected $fillable = ['user_id', 'name', 'mid_name', 'family_name', 'email', 'phone_no', 'skype_id',
		'gender', 'dob_date', 'dob_month', 'dob_year', 'date_of_birth', 'country_id', 'city_id', 'trainer_type', 'address', 'industry_id', 'current_designation', 'highest_education', 'year_of_exp', 'skills', 'about_profile', 'cv_path', 'profile_image', 'identity_proof', 'name_account_holder', 'bank_name', 'account_no', 'swift_code', 'ifsc_code', 'bank_portal_address', 'expected_days', 'from_time', 'from_am_pm', 'to_time', 'to_am_pm', 'created_by', 'updated_by',
	];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', '_id');
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}

	public function city() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}

	public function industry() {
		return $this->belongsTo(IndustryMst::class, 'industry_id', '_id');
		//return $this->belongsToMany(IndustryMst::class, null, 'trainer_id', 'industry_id');
	}

	public function expreiance() {
		return $this->belongsTo(AllOtherMasterMst::class, 'year_of_exp', '_id');
	}
	public function education() {
		//return Course::whereIn('_id', $this->highest_education)->get();
		return $this->belongsTo(Course::class, 'highest_education', '_id');
	}

	public function getEducation() {
		return Course::whereIn('_id', $this->highest_education)->pluck('name');
	}

}
