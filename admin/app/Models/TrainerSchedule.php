<?php

namespace App\Models;

use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TrainerSchedule extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'trainer_schedules';
	protected $fillable = ['trainer_id', 'training_actual_timestamp', 'training_year', 'training_month', 'training_date', 'from_time', 'from_am_pm', 'to_time', 'to_am_pm', 'created_by', 'updated_by'];

	public function trainer() {
		return $this->belongsTo(User::class, 'trainer_id', '_id');
	}

	public function bookedSchedule() {
		return $this->belongsToMany(User::class, 'null', 'schedule_id', 'employee_id')->withPivot('is_paid')->withTimestamps();
	}

	public function scopeTrainingDone($query) {
		return $data = $query->has('bookedSchedule')->where('training_actual_timestamp', '<', Carbon::now()->timestamp)->count();

	}

	public function scopeTrainingUpcoming($query) {
		return $data = $query->has('bookedSchedule')->where('training_actual_timestamp', '>=', Carbon::now()->timestamp)->count();

	}
}
