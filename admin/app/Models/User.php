<?php

namespace App\Models;

/*use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;*/

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Eloquent implements JWTSubject, AuthenticatableContract,
AuthorizableContract, CanResetPasswordContract {
	use Authenticatable, Authorizable, CanResetPassword, HybridRelations;
	use SoftDeletes;
	protected $dates = ['deleted_at', 'last_login', 'last_email_date'];
	protected $connection = 'mongodb';
	protected $collection = 'users';
	//use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_name', 'emp_name', 'email', 'email_verified_at', 'password', 'phone_no', 'register_token', 'phone_no_verified_at', 'last_login', 'last_email_date', 'last_email_no', 'profile_update', 'is_admin', 'user_unique_code', 'status', 'login_status', 'user_location', 'is_super_admin', 'bsb_unique_no', 'community_token', 'created_by',
	];

	protected $appends = ['bsb_user_no'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * Get the identifier that will be stored in the subject claim of the JWT.
	 *
	 * @return mixed
	 */
	public function getJWTIdentifier() {
		return $this->getKey();
	}
	public function getJWTCustomClaims() {
		return [];
	}

	public function roles() {
		return $this->belongsToMany(Role::class, null, 'user_id', 'role_id');
	}

	/**
	 * Checks if User has access to $permissions.
	 */
	public function hasAccess(array $permissions): bool {
		// check if the permission is available in any role
		foreach ($this->roles as $role) {
			if ($role->hasAccess($permissions)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the user belongs to role.
	 */
	public function inRole(string $roleSlug) {
		return $this->roles()->where('slug', $roleSlug)->count() == 1;
	}

	public function adminUser() {
		return $this->hasOne(AdminDetail::class, 'user_id', '_id');
	}

	public function employeeUser() {
		return $this->hasOne(EmployeeProfileTxn::class, 'user_id', '_id');
	}

	public function employerUser() {
		return $this->hasOne(EmployerProfileTxn::class, 'user_id', '_id');
	}
	public function internalEmployeeUser() {
		return $this->hasOne(InternalEmployeeProfileTxn::class, 'user_id', '_id');
	}

	public function collegeUser() {
		return $this->hasOne(CollegeDetail::class, 'user_id', '_id');
	}

	public function franchiseUser() {
		return $this->hasOne(FranchiseDetail::class, 'user_id', '_id');
	}

	public function agencyUser() {
		return $this->hasOne(AgencyDetail::class, 'user_id', '_id');
	}

	public function galleryUser() {
		return $this->hasMany(EmployerGallery::class, 'user_id', '_id')->where('type', 'image');
	}

	public function videoGalleryUser() {
		return $this->hasOne(EmployerGallery::class, 'user_id', '_id')->where('type', 'url');
	}

	public function isEmployer() {
		$roles = $this->roles()->pluck('slug')->toArray();
		if (in_array('employer', $roles)) {
			return true;
		}
	}

	public function employerJobs() {
		return $this->hasMany(EmployerJobTxn::class, 'employer_id', '_id');
	}

	public function emplyerInternationalJobs() {
		return $this->hasMany(InternationalJobTxn::class, 'employer_id', '_id');
	}

	public function employerFolders() {
		return $this->hasMany(EmployerFolderTxn::class, 'employer_id', '_id');
	}
	public function employerWalkin() {
		return $this->hasMany(WalkInInterviewTxn::class, 'employer_id', '_id');
	}

	public function employeeEmployementHistory() {
		return $this->hasMany(EmployeementHistory::class, 'user_id', '_id');
	}

	public function employeeEducation() {
		return $this->hasMany(EmployeeEducation::class, 'user_id', '_id');
	}

	public function employeeSkill() {
		return $this->hasMany(EmployeeSkill::class, 'user_id', '_id');
	}

	public function employeeCertification() {
		return $this->hasMany(EmployeeCertification::class, 'user_id', '_id');
	}

	public function employeeLanguage() {
		return $this->hasMany(EmployeeLanguages::class, 'user_id', '_id');
	}

	public function employeePrefTxn() {
		return $this->hasOne(EmployeePrefTxn::class, 'employee_id', '_id');
	}

	public function employeeVideos() {
		return $this->hasMany(EmployeeVideoTxn::class, 'employee_id', '_id');
	}

	public function employeeCvTxn() {
		return $this->hasOne(EmployeeCvTxn::class, 'employee_id', '_id')->orderBy('_id', 'desc');
	}

	public function getEmployeeCurrentCv() {
		$this->employeeCvTxn ? $this->employeeCvTxn()->orderBy('_id', 'desc')->first() : '';
	}

	public function employeePhotos() {
		return $this->hasMany(EmployeePhoto::class, 'user_id', '_id');
	}

	public function employeeWalkinBook() {
		return $this->hasMany(InterviewSlotBook::class, 'user_id', '_id');
	}

	public function collegeCourses() {
		return $this->belongsToMany(Course::class, 'null', 'user_id', 'course_id');
	}

	public function collegeStudents() {
		return $this->hasMany(EmployeeProfileTxn::class, 'institute_code', 'user_unique_code');
	}

	public function employeeJobAlert() {
		return $this->hasMany(EmployeeJobAlertTxn::class, 'emp_id', '_id');
	}
	public function employeeCvNote() {
		return $this->hasMany(EmployeeCvNote::class, 'employee_id', '_id');
	}

	public function employeeAppliedJob() {
		return $this->hasMany(EmployeeAppliedJobsTxn::class, 'employee_id', '_id');
	}

	public function employeeSavedJob() {
		return $this->hasMany(EmployeeSavedJobsTxn::class, 'employee_id', '_id');
	}

	public function employerCvFolder() {
		return $this->hasMany(EmployerCvSaveFolderTxn::class, 'employee_id', '_id');
	}

	public function empMyAlerts() {
		return $this->hasMany(MyAlertTxn::class, 'user_id', '_id');
	}

	public function employerCvLike() {
		return $this->belongsToMany(EmployeeProfileLikeTxn::class, 'null', 'employee_id', '_id');
	}

	public function employeeCVLike() {
		return $this->hasOne(EmployeeProfileLikeTxn::class, 'employee_id', '_id');
	}

	public function trainerUser() {
		return $this->hasOne(TrainerDetail::class, 'user_id', '_id');
	}

	public function trainerSchedule() {
		return $this->hasMany(TrainerSchedule::class, 'trainer_id', '_id');
	}

	public function carrerPhotos() {
		return $this->hasMany(CareerPhoto::class, 'user_id', '_id');
	}

	public function scopeGetOtherUserName($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'admin':
			return $user->adminUser()->first() ? $user->adminUser()->first()->first_name . ' ' . $this->adminUser()->first()->last_name : '';
			break;
		case 'college':
			return $user->collegeUser()->first() ? $user->collegeUser()->first()->contact_person_name : '';
			break;
		case 'agency':
			return $user->agencyUser()->first() ? $user->agencyUser()->first()->contact_person_name : '';
			break;
		case 'franchise':
			return $user->franchiseUser()->first() ? $user->franchiseUser()->first()->contact_person_name : '';
			break;
		case 'trainer':
			return $user->trainerUser()->first() ? $user->trainerUser()->first()->name : '';
			break;
		default:
			return '';
			break;
		}
	}

	public function scopeGetOtherUserAddress($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'admin':
			return $user->adminUser()->first() ? $user->adminUser()->first()->address : '';
			break;
		case 'college':
			return $user->collegeUser()->first() ? $user->collegeUser()->first()->address : '';
			break;
		case 'agency':
			return $user->agencyUser()->first() ? $user->agencyUser()->first()->address : '';
			break;
		case 'franchise':
			return $user->franchiseUser()->first() ? $user->franchiseUser()->first()->address : '';
			break;
		case 'trainer':
			return $user->trainerUser()->first() ? $user->trainerUser()->first()->address : '';
			break;
		default:
			return '';
			break;
		}
	}

	public function scopeGetOtherUserComp($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'agency':
			return $user->agencyUser()->first() ? $user->agencyUser()->first()->comp_name : '';
			break;
		case 'franchise':
			return $user->franchiseUser()->first() ? $user->franchiseUser()->first()->comp_name : '';
			break;
		case 'college':
			return $user->collegeUser()->first() ? $user->collegeUser()->first()->name : '';
			break;
		default:
			return '';
			break;
		}
	}
	public function scopeGetOtherUserCountry($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'agency':
			return $user->agencyUser()->first()->country ? $user->agencyUser()->first()->country->name : '';
			break;
		case 'franchise':
			return $user->franchiseUser()->first()->country ? $user->franchiseUser()->first()->country->name : '';
			break;
		case 'college':
			return $user->collegeUser()->first()->country ? $user->collegeUser()->first()->country->name : '';
			break;
		case 'trainer':
			return $user->trainerUser()->first()->country ? $user->trainerUser()->first()->country->name : '';
			break;
		default:
			return '';
			break;
		}
	}
	public function scopeGetOtherUserCity($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'agency':
			return $user->agencyUser()->first()->city ? $user->agencyUser()->first()->city->name : '';
			break;
		case 'franchise':
			return $user->franchiseUser()->first()->city ? $user->franchiseUser()->first()->city->name : '';
			break;
		case 'college':
			return $user->collegeUser()->first()->city ? $user->collegeUser()->first()->city->name : '';
			break;
		case 'trainer':
			return $user->trainerUser()->first()->city ? $user->trainerUser()->first()->city->name : '';
			break;
		default:
			return '';
			break;
		}
	}

	public function scopeGetOtherUserGender($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'agency':
			return $user->agencyUser()->first()->gender ? $user->agencyUser()->first()->gender : '';
			break;
		case 'franchise':
			return $user->franchiseUser()->first()->gender ? $user->franchiseUser()->first()->gender : '';
			break;
		case 'college':
			return $user->collegeUser()->first()->gender ? $user->collegeUser()->first()->gender : '';
			break;
		case 'trainer':
			return $user->trainerUser()->first()->gender ? ($user->trainerUser()->first()->gender == 1 ? 'male' : 'female') : '';
			break;
		default:
			return '';
			break;
		}
	}

	public function getBsbUserNoAttribute() {
		//there was a issue for frontent employee after login job serach result
		/*$no = '';
		if($this->roles){
			switch ($this->roles()->first()->slug) {
			case 'employee':
				$no = 'BSBEMY' . $this->bsb_unique_no;
				break;
			case 'employer':
				$no = 'BSBEMR' . $this->bsb_unique_no;
				break;
			case 'college':
				$no = 'BSBUNI' . $this->bsb_unique_no;
				break;
			case 'agency':
				$no = 'BSBAGC' . $this->bsb_unique_no;
				break;
			case 'franchise':
				$no = 'BSBFRA' . $this->bsb_unique_no;
				break;
			case 'trainer':
				$no = 'BSBTRA' . $this->bsb_unique_no;
				break;
			default:
				$no = '';
				break;
			}
			return $no;
		}*/
		return $no = '';
	}

	public function scopeGetOtherUserDetails($q, $user) {
		switch ($user->roles()->first()->slug) {
		case 'admin':
			return $user->adminUser ? $user->adminUser : '';
			break;
		case 'college':
			return $user->collegeUser ? $user->collegeUser : '';
			break;
		case 'agency':
			return $user->agencyUser ? $user->agencyUser : '';
			break;
		case 'franchise':
			return $user->franchiseUser ? $user->franchiseUser : '';
			break;
		case 'trainer':
			return $user->trainerUser ? $user->trainerUser : '';
			break;
		default:
			return '';
			break;
		}
	}

	public function internaleEployeeGovtids() {
		return $this->hasMany(InternalEmployeeGovtId::class, 'user_id', '_id');
	}

	public function totalJobCount() {
		$job_count = 0;
		if ($this->emplyerInternationalJobs) {
			$job_count += $this->emplyerInternationalJobs->where('job_status', 1)->count();
		}
		if ($this->employerJobs) {
			$job_count += $this->employerJobs->where('job_status', 1)->where('is_published', 1)->count();
		}
		if ($this->employerWalkin) {
			$job_count += $this->employerWalkin->where('status', 1)->count();
		}
		return $job_count;
	}

	public function jobCount() {
		if ($this->employerJobs) {
			return $this->employerJobs->where('job_status', 1)->where('is_published', 1)->count();
		}
	}

	public function internationalJobCount() {
		if ($this->emplyerInternationalJobs) {
			return $this->emplyerInternationalJobs->where('job_status', 1)->count();
		}
	}

	public function walkingJobCount() {
		if ($this->employerWalkin) {
			return $this->employerWalkin->where('status', 1)->count();
		}
	}

	public function staffReferalEmployes() {
		return $this->hasMany(EmployeeProfileTxn::class, 'institute_code', 'user_unique_code');
	}

	public function staffReferalEmployers() {
		return $this->hasMany(EmployerProfileTxn::class, 'referral_code', 'user_unique_code');
	}

	public function scopeGetLoginStatus($query, $user) {
		$now = Carbon::now();
		if ($user->login_status == 1 && $user->last_login) {
			$userLogin = Carbon::parse($user->last_login);
			$timeDiff = $userLogin->diffInHours($now);
			if ($timeDiff > 5 && $timeDiff < 10) {
				return 2;
			} elseif ($timeDiff > 10) {
				return 3;
			} else {
				return 1;
			}
		} else {
			return 3;
		}

	}

}
