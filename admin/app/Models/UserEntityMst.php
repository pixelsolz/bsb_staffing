<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserEntityMst extends Eloquent {
	protected $connection = 'mongodb';
	protected $collection = 'user_entity_mst';

	protected $fillable = ['entity_name', 'created_by', 'updated_by'];

	public function createdUser() {
		return $this->belongsTo(User::class, 'created_by', '_id');
	}
}
