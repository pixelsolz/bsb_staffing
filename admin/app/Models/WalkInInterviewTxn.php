<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class WalkInInterviewTxn extends Eloquent {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $connection = 'mongodb';
	protected $collection = 'walk_in_interview_txn';

	protected $fillable = ['job_title', 'bsb_jb_id', 'employer_id', 'country_id', 'city_id', 'industry_id', 'department_id', 'employement_for', 'about_this_job', 'position_offered', 'number_of_vacancies', 'employment_type', 'min_experiance', 'max_experiance', 'qualifications', 'min_qualification', 'max_qualification', 'salary_type', 'currency', 'min_monthly_salary', 'max_monthly_salary', 'language_pref', 'edu_and_or', 'interview_name', 'interview_type_id', 'interview_date', 'interview_start_time', 'interview_end_time', 'interview_time_slot', 'interview_vanue_name', 'interview_location_country', 'interview_location_city', 'interview_venue_address', 'walkin_location_type', 'status', 'special_notes', 'primary_responsibility', 'benefits', 'is_published', 'published_date', 'is_republished', 'republished_date', 'candidate_type', 'job_lat', 'job_long', 'job_location', 'condition_type', 'conditions', 'created_by', 'updated_by'];
	protected $appends = ['bsb_jb_trans_id'];

	/*public function jobRole() {
		return $this->belongsTo(JobRoleMst::class, 'job_role_id', 'id');
	}*/

	public function employer() {
		return $this->belongsTo(User::class, 'employer_id', '_id');
	}

	public function industry() {
		return $this->belongsToMany(IndustryMst::class, 'null', 'job_id', 'industry_id');
	}

	public function department() {
		return $this->belongsToMany(DepartmentMst::class, 'null', 'job_id', 'department_id');
	}

	public function language() {
		return $this->belongsToMany(LanguageMst::class, 'null', 'job_txn_id', 'language_id');
	}

	public function country() {
		return $this->belongsTo(CountryMst::class, 'interview_location_country', '_id');
	}

	public function city() {
		return $this->belongsTo(CityMst::class, 'interview_location_city', '_id');
	}
	public function jobCountry() {
		return $this->belongsTo(CountryMst::class, 'country_id', '_id');
	}

	public function jobCity() {
		return $this->belongsTo(CityMst::class, 'city_id', '_id');
	}

	public function interViewLocation() {
		return $this->belongsTo(CityMst::class, 'interview_location_city', '_id');
	}

	public function interviewType() {
		return $this->belongsTo(AllOtherMasterMst::class, 'interview_type_id', '_id')->where('entity', 'interview_type');
	}

	public function minExperiance() {
		return $this->belongsTo(ExperienceMst::class, 'min_experiance', '_id');
	}

	public function maxExperiance() {
		return $this->belongsTo(ExperienceMst::class, 'max_experiance', '_id');
	}

	public function interviewSlotBooked() {
		return $this->hasMany(InterviewSlotBook::class, 'interview_id', '_id');
	}

	public function employmentFor() {
		return $this->belongsTo(AllOtherMasterMst::class, 'employement_for', '_id');
	}

	public function employmentType() {
		return $this->belongsTo(AllOtherMasterMst::class, 'employment_type', '_id');
	}

	public function currencydata() {
		return $this->belongsTo(CountryMst::class, 'currency', '_id');
	}

	public function getBsbJbTransIdAttribute() {
		if ($this->bsb_jb_id) {
			return 'BSBWOID' . '-' . $this->bsb_jb_id;
		} else {
			return 'BSBWOID';
		}

	}
}
