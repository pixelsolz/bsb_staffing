<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_mst', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('country name');
            $table->integer('created_by')->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_mst');
    }
}
