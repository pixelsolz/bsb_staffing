<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name')->unique()->comment('unique login user name');
            $table->string('email')->unique()->comment('unique email id');
            $table->timestamp('email_verified_at')->nullable()->comment('email varified date time');
            $table->string('phone_no');
            $table->timestamp('phone_no_verified_at')->nullable()->comment('phone no varified date time');
            $table->string('password');
            $table->boolean('is_admin')->comment('check is admin')->default(0);
            $table->boolean('is_super_admin')->comment('check is super admin')->default(0);
            $table->rememberToken();
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
