<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesMstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_mst', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service_name');
            $table->text('service_desc')->nullable();
            $table->float('fees', 8, 2);
            $table->integer('service_life')->comment('Duration of the service');
            $table->string('special_service')->nullable()->comment('Y/N');
            $table->string('needs_broadcast')->nullable()->comment('Flag to mean whether this fees needs a broadcast');
            $table->unsignedInteger('entity_id')->comment('Id for which entity ( employee or empliyer or admin ) this is valid');
            $table->string('order_of_results')->nullable()->comment('Order of the result as displayed on screen( if applicabe )');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();
            
            $table->foreign('entity_id')
                ->references('id')->on('user_entity_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_mst');
    }
}
