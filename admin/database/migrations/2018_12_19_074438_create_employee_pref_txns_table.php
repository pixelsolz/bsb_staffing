<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePrefTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_pref_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->comment('employee_id comes from users table');
            $table->unsignedInteger('country_id')->comment('comes from country_mst table');
            $table->unsignedInteger('city_id')->comment('comes from city_mst table');
            $table->unsignedInteger('job_role_id')->comment('comes from job_role_mst table');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();
            
            $table->foreign('employee_id')->references('id')->on('users');
            $table->foreign('country_id')->references('id')->on('country_mst');
            $table->foreign('city_id')->references('id')->on('city_mst');
            $table->foreign('job_role_id')->references('id')->on('job_role_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_pref_txn');
    }
}
