<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeOtherDetailsTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_other_details_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->comment('Employee Id comes from users table');
            $table->integer('cv_like_count')->nullable();
            $table->string('cv_locked')->nullable()->comment('Y/N');
            $table->string('new_employee')->nullable()->comment('Y/N');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_other_details_txn');
    }
}
