<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeCvViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_cv_view', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->comment('Employee Id comes from users table');
            $table->unsignedInteger('employer_id')->comment('Employer Id comes from users table');
            $table->unsignedInteger('city_id')->comment('comes from city_mst table');
            $table->date('date_of_view');
            $table->string('mail_id')->nullable()->comment('Id of the mail sent for the view');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('city_mst');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_cv_view');
    }
}
