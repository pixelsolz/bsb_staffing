<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerJobTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_job_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_title');
            $table->text('job_desc');
            $table->unsignedInteger('job_sector_id')->comment('job_sector_id comes from job_sector_mst table');
            $table->unsignedInteger('job_role_id')->comment('job_role_id comes from job_role_mst table');
            $table->unsignedInteger('employer_id')->comment('Employee Id comes from users table');
            $table->string('job_status')->nullable();
            $table->string('job_ageing')->nullable();
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('job_sector_id')->references('id')->on('job_sector_mst');
            $table->foreign('job_role_id')->references('id')->on('job_role_mst');
            $table->foreign('employer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_job_txn');
    }
}
