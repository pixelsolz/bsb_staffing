<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeApplicationStatusHistTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_application_status_hist_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('applied_status')->comment('applied_status comes from application_phase_mst table');
            $table->date('hist_date');
            $table->text('employer_comment')->nullable();
            $table->unsignedInteger('employee_application_id')->comment('employee_application_id comes from employee_applied_jobs_txn');
            $table->string('email_id')->nullable();
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('employee_application_id','application_id')->references('id')->on('employee_applied_jobs_txn');
            $table->foreign('applied_status')->references('id')->on('application_phase_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_application_status_hist_txn');
    }
}
