<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSavedJobsTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_saved_jobs_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('job_id')->comment('job_id come from employer_job_txn table');
            $table->date('applied_date')->nullable()->comment('This will be null initially . But once it is applied it is marked with the date on which it was applied');
            $table->unsignedInteger('applied_status')->comment('applied_status come from application_phase_mst table');
            $table->unsignedInteger('employee_id')->comment('Id of the emloyee who has saved the job');
            $table->integer('employee_application_id')->nullable()->comment('Employee Id comes from employee_applied_jobs_txn table when save job to covert applied job');
            $table->date('saved_on')->comment('Date on which the job was saved');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('job_id')->references('id')->on('employer_job_txn');
            $table->foreign('applied_status')->references('id')->on('application_phase_mst');
            $table->foreign('employee_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_saved_jobs_txn');
    }
}
