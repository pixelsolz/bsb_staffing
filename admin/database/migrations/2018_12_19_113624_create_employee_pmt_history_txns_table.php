<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePmtHistoryTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_pmt_history_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->comment('Employee Id comes from users table');
            $table->string('payment_made_on')->nullable();
            $table->decimal('payment_amount',8,2);
            $table->string('payment_mode');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_pmt_history_txn');
    }
}
