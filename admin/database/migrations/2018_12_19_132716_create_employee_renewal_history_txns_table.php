<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeRenewalHistoryTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_renewal_history_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->comment('Employee Id comes from users table');
            $table->date('renewal_date');
            $table->integer('payment_id')->nullable()->comment('The idthrough which this payment was made. This should be a nullable field as all renewals may not need a payment');
            $table->integer('opted_services_id')->nullable()->comment('The id of the service which was opted for.');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_renewal_history_txn');
    }
}
