<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerEmployeeOptedServicesTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_employee_opted_services_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id')->comment('Comes from Service Master table');
            $table->string('is_employee')->comment('Y if this is employee data');
            $table->string('is_employer')->comment('Y if this is employer data');
            $table->integer('employee_id')->comment('This id in employee user id');
            $table->integer('employer_id')->comment('This id in employer user id');
            $table->string('status')->nullable();
            $table->string('service_expiry')->nullable();
            $table->string('service_opted_on')->nullable()->comment('Data on which the services was opted for');
            $table->string('current_status')->nullable()->comment('(Y/N)To mark whether this is a current status');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('service_id')->references('id')->on('services_mst');
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_employee_opted_services_txn');
    }
}
