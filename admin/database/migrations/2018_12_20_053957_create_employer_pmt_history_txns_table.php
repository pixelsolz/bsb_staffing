<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerPmtHistoryTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_pmt_history_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employer_id')->comment('Employer Id comes from users table');
            $table->unsignedInteger('service_id')->comment('service Id comes from services_mst table');
            $table->string('payment_made_on')->nullable();
            $table->decimal('payment_amount',8,2);
            $table->string('payment_mode')->nullable();
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('employer_id')->references('id')->on('users');
            $table->foreign('service_id')->references('id')->on('services_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_pmt_history_txn');
    }
}
