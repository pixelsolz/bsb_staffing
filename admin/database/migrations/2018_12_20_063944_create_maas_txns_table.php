<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaasTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maas_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mail_template_id')->comment('mail_template Id comes from employer_email_template_txn table');
            $table->unsignedInteger('send_to_employee_id')->comment('send_to_employee Id comes from users table');
            $table->string('send_on')->nullable();
            $table->text('additional_message')->nullable()->comment('This may not be required');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('send_to_employee_id')->references('id')->on('users');
            $table->foreign('mail_template_id')->references('id')->on('employer_email_template_txn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maas_txn');
    }
}
