<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotHiringTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_hiring_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employer_id')->comment('employer Id comes from users table');
            $table->unsignedInteger('service_id')->comment('service Id comes from services_mst table');
            $table->date('hiring_date');
            $table->time('hiring_time');
            $table->string('location');
            $table->string('position');
            $table->integer('total_positions');
            $table->integer('total_vacancy');
            $table->string('reviewed')->nullable()->comment('Y/N');
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('employer_id')->references('id')->on('users');
            $table->foreign('service_id')->references('id')->on('services_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_hiring_txn');
    }
}
