<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPaymentConfigTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_payment_config_txn', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id')->comment('service Id comes from services_mst table');
            $table->string('payment_flag')->nullable()->comment('Y/N');
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();

            $table->foreign('service_id')->references('id')->on('services_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_payment_config_txn');
    }
}
