<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceIdToEmployeePmtHistoryTxn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_pmt_history_txn', function (Blueprint $table) {
            $table->integer('service_id')->after('payment_mode')->comment('Comes from Service Master. Indicated for which serv this payment is made');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_pmt_history_txn', function (Blueprint $table) {
            //
        });
    }
}
