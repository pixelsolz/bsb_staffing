<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRenewalTriggerToEmployeeRenewalHistoryTxn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_renewal_history_txn', function (Blueprint $table) {
            $table->integer('renewal_trigger')->after('opted_services_id')->nullable()->comment('Trigger which caused the renewal. For like based renewal it is AUTO for user based it is MANUAL');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_renewal_history_txn', function (Blueprint $table) {
            //
        });
    }
}
