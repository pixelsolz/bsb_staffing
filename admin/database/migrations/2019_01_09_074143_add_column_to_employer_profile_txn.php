<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToEmployerProfileTxn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_profile_txn', function (Blueprint $table) {
            $table->string('company_name')->after('last_name');
            $table->integer('industry_id')->after('company_name');
            $table->text('address')->nullable()->after('industry_id');
            $table->integer('city')->after('address');
            $table->string('zipcode')->after('city');
            $table->integer('country')->after('zipcode');
            $table->string('alternate_ph_no')->nullable()->after('country');
            $table->string('company_type')->after('alternate_ph_no');
            $table->string('company_size')->after('company_type');
            $table->longText('company_tagline')->after('company_size');
            $table->longText('company_short_desc')->after('company_tagline');
            $table->longText('element')->after('company_short_desc');
            $table->longText('nearby_place')->after('element');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_profile_txn', function (Blueprint $table) {
            //
        });
    }
}
