<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyLogoToEmployerProfileTxn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_profile_txn', function (Blueprint $table) {
            $table->string('company_logo')->nullable()->after('country');
            $table->string('company_main_image')->nullable()->after('company_logo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_profile_txn', function (Blueprint $table) {
            //
        });
    }
}
