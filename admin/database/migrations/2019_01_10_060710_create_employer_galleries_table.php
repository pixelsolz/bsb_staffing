<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('comes from users table');
            $table->string('name')->comment('image name or youtube url');
            $table->string('type')->comment('image or youtube url');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_galleries');
    }
}
