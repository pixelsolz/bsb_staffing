<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteJobSectorIdToEmployerJobTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->dropForeign('bsb_employer_job_txn_job_sector_id_foreign');
            $table->dropColumn('job_sector_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            //
        });
    }
}
