<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToEmployerJobTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->unsignedInteger('industry_id')->after('employer_id')->comment('industry Id comes from industry_mst table');
            $table->foreign('industry_id')->references('id')->on('industry_mst');
            $table->longText('qualification')->after('industry_id');
            $table->longText('benefits')->after('qualification')->nullable();
            $table->integer('min_experiance')->after('benefits')->nullable()->comment('experiance in years');
            $table->integer('max_experiance')->after('min_experiance')->nullable()->comment('experiance in years');
            $table->string('employment_type')->after('max_experiance')->nullable();
            $table->integer('number_of_vacancies')->after('employment_type')->nullable();
            $table->decimal('min_monthly_salary',8,2)->after('number_of_vacancies')->nullable();
            $table->decimal('max_monthly_salary',8,2)->after('min_monthly_salary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            //
        });
    }
}
