<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountCityColumnToEmployerJobTxn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->unsignedInteger('country_id')->after('industry_id')->comment('country id comes from country_mst table');
            $table->unsignedInteger('city_id')->after('country_id')->comment('city Id comes from city_mst table');
            $table->foreign('country_id')->references('id')->on('country_mst');
            $table->foreign('city_id')->references('id')->on('city_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            //
        });
    }
}
