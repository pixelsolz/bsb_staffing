<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToEmployerProfileTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_profile_txn', function (Blueprint $table) {
            $table->string('company_name')->nullable()->change();
            $table->integer('industry_id')->nullable()->change();
            $table->integer('city_id')->nullable()->change();
            $table->string('zipcode')->nullable()->change();
            $table->integer('country_id')->nullable()->change();
            $table->string('company_type')->nullable()->change();
            $table->string('company_size')->nullable()->change();
            $table->longText('company_tagline')->nullable()->change();
            $table->longText('company_short_desc')->nullable()->change();
            $table->longText('element')->nullable()->change();
            $table->longText('nearby_place')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_profile_txn', function (Blueprint $table) {
            //
        });
    }
}
