<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTypeMstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_type_mst', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_type');
            $table->string('status')->default(1);
            $table->integer('created_by')->nullable()->comment('created user id');
            $table->integer('updated_by')->nullable()->comment('updated user id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_type_mst');
    }
}
