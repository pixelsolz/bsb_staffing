<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAboutThisJobToEmployerJobTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->longText('about_this_job')->nullable()->after('industry_id');
            $table->string('job_posted_to_be')->nullable()->after('about_this_job');
            $table->string('employement_for')->nullable()->after('job_posted_to_be');
            $table->longText('required_skill')->nullable()->after('employement_for');
            $table->longText('primary_responsibility')->nullable()->after('required_skill');
            $table->longText('special_notes')->nullable()->after('primary_responsibility');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            //
        });
    }
}
