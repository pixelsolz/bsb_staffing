<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinQualificationToEmployerJobTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->integer('min_qualification')->nullable()->after('benefits');
            $table->integer('max_qualification')->nullable()->after('min_qualification');
            $table->dropColumn('qualification');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->dropColumn('min_qualification');
            $table->dropColumn('max_qualification');
        });
    }
}
