<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyJobDescToEmployerJobTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            $table->text('job_desc')->nullable()->change();
            $table->string('min_qualification')->change();
            $table->string('max_qualification')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_job_txn', function (Blueprint $table) {
            //
        });
    }
}
