<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		User::create(['user_name' => 'superadmin', 'email' => 'superadmin@gmail.com', 'password' => bcrypt('superadmin@123#'),'is_admin' => 1, 'is_super_admin' => 1,'phone_no'=> '1234567876','created_by'=>'1']);
	}
}
