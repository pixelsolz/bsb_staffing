var urlPath = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var fullUrl = urlPath + '/job-portal/admin';
$(document).ready( function() {
    $('#example').dataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
//======================= Edit pages Image section ================
  $('.imgClose').off().on('click', function(evt){
      evt.preventDefault();
      let inputFile = $(this).parent('.hasImg').prev('.imgCls');
      inputFile.removeClass('hideMe').addClass('req');
      $(this).parent('.hasImg').remove();
    });

  

 });

function  deleteData(url, token){
event.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You want to delete this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
        function () {
            $.ajax({
                type: 'POST',
                url: url,
                data: { '_method': 'delete', '_token': token},
                async: false,
                success: (response) => {
                	
                    //swal("Deleted!", "Your has been deleted.", "success");
                    window.location.reload();
                },
                error: () => {

                }

            });

        });

}

function showCategory(role){
     var values = $("#roles:checked").map(function(){
      return $(this).val();
    }).get();

     if(values.indexOf('2') != -1){
      $('#bankinformation').show();
     }else{
      $('#bankinformation').hide();
     }

     if(values.indexOf('2') != -1 || values.indexOf('4') != -1){
       $('#category-div').show();
     }else{
       $('#category-div').hide();  
     }

}

$(document).ready(function(){
    $('[data-tog="tooltip"]').mouseover(function(){
      $(this).tooltip('show');
    });
    $('[data-tog="tooltip"]').click(function(){
      $(this).tooltip('destroy');
    });
   // $('[data-tog="tooltip"]').tooltip({ trigger:'hover focus' }); 
});

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
     CKEDITOR.replace('editor2');
      CKEDITOR.replace('editor3');
      CKEDITOR.replace('editor4');
    //skill multiple selec
     //$('.select2').select2()
     $('.employer-skill-select2').select2();
     //Date picker
     $('.datepicker').datepicker({
      autoclose: true
      });

  });

  function getCity(value, url){
    $.ajax({
      type: 'GET',
      url: url,
      data: {id:value},
      async: false,
      success: (response) => {
        let options ='';
        if(response.data.length){
          response.data.forEach((obj)=>{
             options += '<option value="'+obj._id+'">'+obj.name+'</option>';
          });          
        }
        $('.set_city').html('<option value="">select city</option>' + options);
        var urlParams = new URLSearchParams(location.search);
        if(urlParams.get('search_by_city')){
          $('[name="search_by_city"]').val(urlParams.get('search_by_city'));
        }
      },
      error: () => {
      }
    });    
  }

  $(document).ready(function() {
    var urlParams = new URLSearchParams(location.search);
    if(urlParams.get('search_by_country')){
      getCity(urlParams.get('search_by_country'), fullUrl + '/city-ajax');
    }
  });

  function statusChange(event, url, id){
    let status = event.target.checked? 1:0;
    $.ajax({
      type: 'GET',
      url: url,
      data: {id:id, status: status},
      async: false,
      success: (response) => {
        if (response.status == 200) {
          swal({
            title: "Successfully changed status",
            text: " Changed status",
            type: "success",
            showCancelButton: false,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false
          }, function (isConfirm) {
            if (isConfirm) {
            window.location.reload();
          }
        });
      }        
      },
      error: () => {
      }
    });
    console.log(event);
  }

  function paymentConfigChange(event,id,url){
    let status = event.target.checked? 1:0;
    let chekval= $(event.target).val();
    console.log(id);
    //console.log(chekval);
    $.ajax({
      type: 'GET',
      url: url,
      data: {id:id,status:status, chekval: chekval},
      async: false,
      success: (response) => {
        if (response.status == 200) {
          swal({
            title: "Successfully changed status",
            text: " Changed status",
            type: "success",
            showCancelButton: false,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false
          }, function (isConfirm) {
            if (isConfirm) {
            window.location.reload();
          }
        });
      }        
      },
      error: () => {
      }
    });
  }


  $(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><input type="file" name="govt_id[]" class="form-control"><a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="fa fa-minus-circle fa-3x"></i></a></div>'; //New input field html 
    //var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button"><img src="remove-icon.png"/></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
      //alert("sdfsf");
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});





