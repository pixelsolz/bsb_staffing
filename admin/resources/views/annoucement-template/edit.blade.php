@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Annoucement massage
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.announcement-template.list-all') }}"><i class="fa fa-dashboard"></i> Annoucement massage List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Annoucement massage</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Annoucement massage Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.announcement-template.update', $annoucement->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('temp_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Subject<span class="required_field">*</span></label>
                           <input type="text" name="temp_name" class="form-control" value="{{@$annoucement->temp_name}}">
                           @if($errors->has('temp_name'))
                           <span class="error">{{$errors->first('temp_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('temp_content'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Message<span class="required_field">*</span></label>
                           <textarea id="editor1" name="temp_content" >
                              {{@$annoucement->temp_content}}
                           </textarea>
                           @if($errors->has('temp_content'))
                           <span class="error">{{$errors->first('temp_content')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('entity_id'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="entity_id">Applicable To<span class="required_field">*</span></label>
                        <select class="form-control" name="entity_id">
                           <option value="">Select Applicable To</option>
                           @foreach($allRole as $role)
                           <option value="{{$role->id}}" @if(@$befit->entity_id == $role->id) {{'selected'}} @endif>{{$role->name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('entity_id'))
                           <span class="error">{{$errors->first('entity_id')}}</span>
                        @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection