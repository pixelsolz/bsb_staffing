@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Attribute Edit
      </h1>
      <ol class="breadcrumb">
         <!-- <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li> -->
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Attribute Edit</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Attribute Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.attribute-manage.update',$attribute->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Attribute Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{$attribute->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>


                     <div class="form-group row @if($errors->has('attr_type'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="attr_type">Attribute Type<span class="required_field">*</span></label>
                        <select class="form-control" name="attr_type">
                           <option value="">select</option>
                           <option value="1" @if($attribute->attr_type ==1) selected @endif>Industry</option>
                           <option value="2" @if($attribute->attr_type ==2) selected @endif>Under graduate degree</option>
                           <option value="3" @if($attribute->attr_type ==3) selected @endif>Post graduate degree</option>
                        </select>
                           @if($errors->has('attr_type'))
                           <span class="error">{{$errors->first('attr_type')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="status">Status<span class="required_field">*</span></label>
                        <select class="form-control" name="status">
                           <option value="">select</option>
                           <option value="1" @if($attribute->status ==1) selected @endif>Active</option>
                           <option value="2" @if($attribute->status ==2) selected @endif>Inactive</option>
                        </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>


                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection