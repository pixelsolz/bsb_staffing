@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Banner Image Add
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.banner-manage.index') }}"><i class="fa fa-dashboard"></i> Banner Immage List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Add Banner Image</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Banner Image Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.banner-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="title">Title<span class="required_field">*</span></label>
                           <input type="text" name="title" class="form-control" value="{{old('title')}}">
                           @if($errors->has('title'))
                              <span class="error">{{$errors->first('title')}}</span>
                           @endif
                        </div>
                     </div>
                  
                     <div class="form-group row">
                        <div class="col-xs-9">
                        <label for="banner_image">Banner Image<span class="required_field">*</span></label>
                        <input type="file" name="banner_image" class="form-control">
                         @if($errors->has('banner_image'))
                           <span class="error">{{$errors->first('banner_image')}}</span>
                           @endif
                        </div>
                     </div>
                    
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection