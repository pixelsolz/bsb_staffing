@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Benefit Create
      </h1>
      <ol class="breadcrumb">
         <!-- <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li> -->
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Benefit Create</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Benefit Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.benefit-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="title">Title<span class="required_field">*</span></label>
                           <input type="text" name="title" class="form-control" value="{{old('title')}}">
                           @if($errors->has('title'))
                           <span class="error">{{$errors->first('title')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('benefit'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="benefit">Benefit<span class="required_field">*</span></label>
                           <textarea class="form-control" name="benefit">{{old('benefit')}}</textarea>
                           @if($errors->has('benefit'))
                           <span class="error">{{$errors->first('benefit')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('subject'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="subject">Subject</label>
                           <textarea class="form-control" name="subject">{{old('subject')}}</textarea>
                           @if($errors->has('subject'))
                           <span class="error">{{$errors->first('subject')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('type'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="country_id">Benefit Type<span class="required_field">*</span></label>
                        <select class="form-control" name="type">
                           <option value="">select</option>
                           <option value="1">Referral</option>
                           <option value="2">Announcement</option>
                        </select>
                           @if($errors->has('type'))
                           <span class="error">{{$errors->first('type')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('entity_id'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="entity_id">Applicable To<span class="required_field">*</span></label>
                        <select class="form-control" name="entity_id">
                           <option value="">Select Applicable To</option>
                           @foreach($allRole as $role)
                           <option value="{{$role->_id}}">{{$role->name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('entity_id'))
                           <span class="error">{{$errors->first('entity_id')}}</span>
                        @endif
                        </div>
                     </div>


                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection