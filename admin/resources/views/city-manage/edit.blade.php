@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit City
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.city-manage.index') }}"><i class="fa fa-dashboard"></i> City List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit City</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">City Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.city-manage.update', $city->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">City Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{$city->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('country_code'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="country_code">Country<span class="required_field">*</span></label>
                        <select class="form-control" name="country_code">
                           <option value="">select</option>
                           @foreach($countries as $country)
                           <option value="{{$country->code}}" @if($city->country->code == $country->code) selected @endif>{{$country->name}}</option>
                           @endforeach
                        </select>
                           @if($errors->has('country_code'))
                           <span class="error">{{$errors->first('country_code')}}</span>
                           @endif

                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_title">SEO Title</label>
                           <input type="text" name="seo_title" class="form-control" value="{{@$city->seo_title}}">
                           @if($errors->has('seo_title'))
                           <span class="error">{{$errors->first('seo_title')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_description'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_description">SEO Description</label>
                           <textarea name="seo_description" class="form-control" placeholder="Enter SEO Description ">{{@$city->seo_description}}</textarea>
                           @if($errors->has('seo_description'))
                           <span class="error">{{$errors->first('seo_description')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_keywords'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_keywords">SEO Keywords</label>
                           <input type="text" name="seo_keywords" class="form-control" value="{{@$city->seo_keywords}}">
                           @if($errors->has('seo_keywords'))
                           <span class="error">{{$errors->first('seo_keywords')}}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection