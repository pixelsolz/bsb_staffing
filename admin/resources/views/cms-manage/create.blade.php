@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create CMS
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.cms-manage.index') }}"><i class="fa fa-dashboard"></i> CMS List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create CMS</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">CMS Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.cms-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="title">Title<span class="required_field">*</span></label>
                           <input type="text" name="title" class="form-control" value="{{old('title')}}">
                           @if($errors->has('title'))
                           <span class="error">{{$errors->first('title')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{old('name')}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('slug'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="slug">Slug<span class="required_field">*</span></label>
                           <input type="text" name="slug" class="form-control" value="{{old('slug')}}">
                           @if($errors->has('slug'))
                           <span class="error">{{$errors->first('slug')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('content'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="content">Content<span class="required_field">*</span></label>
                           <textarea class="form-control" rows="100" cols="80" id="editor" name="content"></textarea>
                           @if($errors->has('content'))
                           <span class="error">{{$errors->first('content')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection