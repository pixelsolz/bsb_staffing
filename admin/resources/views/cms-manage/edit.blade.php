@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit CMS
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.cms-manage.index') }}"><i class="fa fa-dashboard"></i>CMS List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit CMS</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">CMS Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.cms-manage.update',['id'=>$cms_manage->id])}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="title">Title<span class="required_field">*</span></label>
                           <input type="text" name="title" class="form-control" value="{{old('title')? old('title'): $cms_manage->title}}">
                           @if($errors->has('title'))
                           <span class="error">{{$errors->first('title')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{old('name')? old('name'): $cms_manage->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('slug'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="slug">Slug<span class="required_field">*</span></label>
                           <input type="text" name="slug" class="form-control" value="{{old('slug')?old('slug'): $cms_manage->slug }}">
                           @if($errors->has('slug'))
                           <span class="error">{{$errors->first('slug')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('content'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="content">Content<span class="required_field">*</span></label>
                           <textarea class="form-control" rows="100" cols="80" id="editor" name="content">{!! old('content')? old('content'): $cms_manage->content !!}</textarea>
                           @if($errors->has('content'))
                           <span class="error">{{$errors->first('content')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="gender">Status<span class="required_field">*</span></label>
                           <select class="form-control" name="gender">
                              <option value="">select</option>
                              <option value="1" @if(old('status') ? old('status')==1: ($cms_manage->status ==1)) selected @endif>Active</option>
                              <option value="0" @if(old('status') ? old('status')==0: ($cms_manage->status ==0)) selected @endif>Inactive</option>
                           </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection