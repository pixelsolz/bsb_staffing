@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Company Size
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.companysize-manage.index') }}"><i class="fa fa-dashboard"></i>  Company Size List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create Company Size</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Company Size Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin.companysize-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('company_size'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="company_size">Company Size<span class="required_field">*</span></label>
                           <input type="text" name="company_size" class="form-control" value="{{old('company_size')}}">
                           @if($errors->has('company_size'))
                           <span class="error">{{$errors->first('company_size')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection