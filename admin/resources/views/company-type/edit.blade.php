@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Company Type
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Company Type List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Company Type</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Company Type Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.companytype-manage.update',$comp_type->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('company_type'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="company_type">Company Type<span class="required_field">*</span></label>
                           <input type="text" name="company_type" class="form-control" value="{{$comp_type->company_type}}">
                           @if($errors->has('company_type'))
                           <span class="error">{{$errors->first('company_type')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="status">Status<span class="required_field">*</span></label>
                           <select class="form-control" name="status">
                              <option value="">select</option>
                              <option value="1" @if($comp_type->status ==1) selected @endif>Active</option>
                              <option value="0" @if($comp_type->status ==0) selected @endif>Inactive</option>
                           </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection