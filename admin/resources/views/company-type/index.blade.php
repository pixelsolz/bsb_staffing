@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Company Type Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <a href="{{route('admin.companytype-manage.create')}}" class="btn btn-primary">Creat New</a>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <form action="{{route('admin.companytype-manage.index')}}" method="get">
                <div class="row">
                  <div class="col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-offset-0 col-xs-12">
                    <div class="form-group">
                      <input type="text" name="q" class="form-control" placeholder="Search with Country Name or Country Code..." value="{{Request::input('q')}}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="searchOuter">
                      <input type="submit" name="" value="search">
                    </div>
                  </div>
                </div>
              </form>
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Company Type</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($comp_types as $company)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{$company->company_type}}</td>
                  <td>{{$company->status ==1 ? 'Active':'Inactive'}}</td>
                  <td>
                   <a href="{{route('admin.companytype-manage.edit', ['id'=>$company->_id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                   @php
                        $deleteroute = route('admin.companytype-manage.delete', ['id'=>$company->_id]);
                      @endphp
                         <a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove"></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>
              <div>{{ $comp_types->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
