@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Country
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.country-manage.index') }}"><i class="fa fa-dashboard"></i> Country List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Add Country</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Country Add Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('country-manage')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Country Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('code'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="code">Country Code<span class="required_field">*</span></label>
                           <input type="text" name="code" class="form-control" value="">
                           @if($errors->has('code'))
                           <span class="error">{{$errors->first('code')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('dial_code'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="code">Dial Code<span class="required_field">*</span></label>
                           <input type="text" name="dial_code" class="form-control" value="">
                           @if($errors->has('dial_code'))
                           <span class="error">{{$errors->first('dial_code')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('currency'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="currency">Currency<span class="required_field">*</span></label>
                           <input type="text" name="currency" class="form-control" value="">
                           @if($errors->has('currency'))
                           <span class="error">{{$errors->first('currency')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('currency_code'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="currency_code">Currency Code<span class="required_field">*</span></label>
                           <input type="text" name="currency_code" class="form-control" value="">
                           @if($errors->has('currency_code'))
                           <span class="error">{{$errors->first('currency_code')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_title">SEO Title</label>
                           <input type="text" name="seo_title" class="form-control" value="{{@$country->seo_title}}">
                           @if($errors->has('seo_title'))
                           <span class="error">{{$errors->first('seo_title')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_description'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_description">SEO Description</label>
                           <textarea name="seo_description" class="form-control" placeholder="Enter SEO Description ">{{@$country->seo_description}}</textarea>
                           @if($errors->has('seo_description'))
                           <span class="error">{{$errors->first('seo_description')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_keywords'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_keywords">SEO Keywords</label>
                           <input type="text" name="seo_keywords" class="form-control" value="{{@$country->seo_keywords}}">
                           @if($errors->has('seo_keywords'))
                           <span class="error">{{$errors->first('seo_keywords')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-9">
                           <label for="image">Country Image</label>
                           <input type="file" name="image" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Create</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection