@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Degree
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.degree-manage.index') }}"><i class="fa fa-dashboard"></i> Degree List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Degree</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Degree Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.degree-manage.update', $degree->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <input type="hidden" name="redirects_to" value="{{URL::previous()}}">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{$degree->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>


                     <div class="form-group row @if($errors->has('attribute_id'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="attribute_id">Attribute<span class="required_field">*</span></label>
                        <select class="form-control" name="attribute_id">
                           <option value="">select</option>
                           @foreach($attr_msts as $attr)
                           <option value="{{$attr->_id}}" @if($degree->attribute_id == $attr->_id) selected @endif>{{$attr->name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('attribute_id'))
                           <span class="error">{{$errors->first('attribute_id')}}</span>
                        @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('parent_id'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="parent_id">Parent<span class="required_field">*</span></label>
                        <select class="form-control" name="parent_id">
                           <option value="">select</option>
                           <option value="0"  @if($degree->parent_id == '0') selected @endif>root</option>
                           @foreach($degrees as $degreeOption)
                           <option value="{{$degreeOption->_id}}"  @if($degreeOption->_id == $degree->parent_id ) selected @endif>{{$degreeOption->name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('parent_id'))
                           <span class="error">{{$errors->first('parent_id')}}</span>
                        @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="status">Status<span class="required_field">*</span></label>
                        <select class="form-control" name="status">
                           <option value="">select</option>
                           <option value="1" @if($degree->status == 1) selected @endif>Active</option>
                            <option value="2" @if($degree->status == 2) selected @endif>Inactive</option>
                        </select>
                        @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                        @endif
                        </div>
                     </div>


                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection