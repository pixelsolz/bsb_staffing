<!DOCTYPE html>

<html>
<head>
    <title>BSB STAFFING</title>
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="600" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 26px; border: 1px solid #ddd; padding: 5px; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="position: relative; padding: 10px 0;">
        <a href="https://bsbstaffing.com/" style="display: inline-block; max-width: 300px; padding: 20px 0 10px;"><img style="width: 100%;" src="https://bsbinternational.org.uk/bsbstaffing_main/images/logo.png" title="BSB STAFFING" alt="bsbstaffing"/></a>
      </td>
    </tr>
    <tr>
      <td align="left" valign="middle" style="position: relative; text-align: justify; padding:0 20px 30px;">
        <p style="margin-bottom: 15px; text-transform: capitalize; font-weight: 600;">Hi {{@$user->full_name}},</p>
        <p style="margin-bottom: 20px;">{{$text}}</p>

        @if(@$notComEmployeer)
        <p style="margin-bottom: 20px;">
            1.We have Millions of CV/Resumes from across the globe <br/>
            2.Unlimited Free Job post <br/>
            3.Each Job post you will get 10 CV within a few days <br/>
            4.company logo visible to acres the global
        </p>
        @endif

      </td>
    </tr>
    <tr>
      <td align="center" valign="middle" style="position: relative; padding:0; background: #dadada; ">
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
          <tr>
              <td align="left" valign="middle" style="padding: 20px 30px;">
                <span style="display: block; color: #000;">Warm regards,</span>
                <span style="  text-decoration: none;
                font-weight: 600; text-transform: capitalize; display: inline-block; padding-top: 10px; color: #000;">Team BSB Staffing</span>
              </td>
              <td align="right" valign="middle"  style="padding: 20px 30px; background: none; width: 50%; line-height: 0;">
                <a href="https://bsbstaffing.com/" target="_blank" style="display: inline-block; margin-right:10px; line-height: 0"><img style="width: 100%;" src="https://bsbinternational.org.uk/bsbstaffing_main/images/logo.png" alt="bsbstaffing"/></a>
              </td>
          </tr>
        </table>
      </td>
    </tr>
</table>
</body>
</html>
