<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table style="margin: 0 auto; border-collapse: collapse;
		font-family: arial,sans-serif; font-size: 16px; line-height: 24px;" width="820">
		<tr>
			<td style="border: 5px solid #ddd; padding: 10px 20px;">
				<table width="100%;">
					<tr>
						<td align="center">
							<a href="#" style="display: inline-block; margin: 0 0 20px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" />
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding: 0 0 20px;">Dear {{$user->contact_person_name.' '.$user->contact_person_lastname}},</td>
					</tr>
					<tr>
						<td>
							<p style="margin: 0 0 20px; color: #000;"> Give your students to get the chance of pursuing their dream careers through <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>. Several local and international companies are posting jobs continuously on this global job portal.</p>
							<p style="margin: 0 0 20px; color: #000;">Update all your students about the latest jobs on <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>. Please take part in their growth by allowing them to fly over the sky. </p>
							<p style="margin: 0 0 20px; color: #000;">And update all your students to upload their CV/Resume on <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>.</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border: 5px solid #ddd; background: #f5f5f5; padding: 10px 20px;">
				<table width="100%" style="">
					<tr>
						<td>
							<span style="display: block; font-size: 15px;
							font-style: normal; padding: 2px 0;
							">Thanks & Regards </span>
							<span style="display: block; font-size: 14px;
								font-style: normal; padding: 2px 0;
							"> <a style="display: inline-block; color: #002359; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> </span>
						</td>
						<td align="right">
							<a href="#" style="display: inline-block;vertical-align: middle;max-width: 150px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" / width="100%">
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>