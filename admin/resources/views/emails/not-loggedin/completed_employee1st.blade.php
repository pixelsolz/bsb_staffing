<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table style="margin: 0 auto; border-collapse: collapse;
		font-family: arial,sans-serif; font-size: 16px; line-height: 24px;" width="820">
		<tr>
			<td style="border: 5px solid #ddd; padding: 10px 20px;">
				<table width="100%;">
					<tr>
						<td align="center">
							<a href="#" style="display: inline-block; margin: 0 0 20px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" />
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding: 0 0 20px;">Dear {{$user->first_name.' '.$user->last_name}},</td>
					</tr>
					<tr>
						<td>
							<p style="margin: 0 0 20px; color: #000;"><a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> provides you with several recruiters that are searching for the right person in the company. Apply for unlimited jobs on <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> to start your first step towards career success </p>
							<p style="margin: 0 0 20px; color: #000;">Several employers are waiting to hire you for the right position in their company. Find your job in the best organization on <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> to achieve your career success. </p>

						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border: 5px solid #ddd; background: #f5f5f5; padding: 10px 20px;">
				<table width="100%" style="">
					<tr>
						<td>
							<span style="display: block; font-size: 15px;
							font-style: normal; padding: 2px 0;
							">Thanks & Regards </span>
							<span style="display: block; font-size: 14px;
								font-style: normal; padding: 2px 0;
							"> <a style="display: inline-block; color: #002359; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> </span>
						</td>
						<td align="right">
							<a href="#" style="display: inline-block;vertical-align: middle;max-width: 150px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" / width="100%">
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>