<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table style="margin: 0 auto; border-collapse: collapse;
		font-family: arial,sans-serif; font-size: 16px; line-height: 24px;" width="800">
		<tr>
			<td style="border: 5px solid #ddd; padding: 10px 20px;">
				<table width="100%;">
					<tr>
						<td align="center">
							<a href="#" style="display: inline-block; margin: 0 0 20px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" />
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding: 0 0 20px;">Dear {{$user->first_name.' '.$user->last_name}},</td>
					</tr>
					<tr>
						<td>
							<p style="margin: 0 0 20px; color: #000;">You opened an alternative path to fulfil your recruitment needs with <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>. You are successfully registered with this global job portal that allows you to find a large group of candidates that might be a good fit in your organization</p>
							<p style="margin: 0 0 20px; color: #000;">Start posting for the vacancy available in the organization and get the talent resources from <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>. </p>
							<p style="margin: 0 0 20px; color: #000;">Just discover the ample talent near you on the global platform of <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>.</p>
						</td>
					</tr>

				</table>
			</td>
		</tr>
		<tr>
			<td style="border: 5px solid #ddd; background: #f5f5f5; padding: 10px 20px; width: 100%;">
				<table width="100%" style="">
					<tr>
						<td>
							<span style="display: block; font-size: 15px;
							font-style: normal; padding: 2px 0;
							">Thanks & Regards </span>
							<span style="display: block; font-size: 14px;
								font-style: normal; padding: 2px 0;
							"> <a style="display: inline-block; color: #002359; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> </span>
						</td>
						<td align="right">
							<a href="#" style="display: inline-block;vertical-align: middle;max-width: 150px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" / width="100%">
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>