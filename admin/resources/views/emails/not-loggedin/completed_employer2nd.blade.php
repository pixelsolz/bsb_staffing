<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table style="margin: 0 auto; border-collapse: collapse;
		font-family: arial,sans-serif; font-size: 16px; line-height: 24px;" width="820">
		<tr>
			<td style="border: 5px solid #ddd; padding: 10px 20px;">
				<table width="100%;">
					<tr>
						<td align="center">
							<a href="#" style="display: inline-block; margin: 0 0 20px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" />
							</a>
						</td>
					</tr>
					<tr>
						<td style="padding: 0 0 20px;">Dear {{$user->first_name.' '.$user->last_name}},</td>
					</tr>
					<tr>
						<td>
							<p style="margin: 0 0 20px; color: #000;">We have noticed that you are not logging into your bsbstaffing.com account. Millions of job seekers are there, right in front of you. Hire the most skilled employees from the global job portal <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a>. </p>
							<p style="margin: 0 0 20px; color: #000;"> <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> is here for your help. You will get the ocean of talented candidates that may be the best fit for your organization. </p>
							<p style="margin: 0 0 20px; color: #000;">Find out the applications of talented personnel for your company. <a style="display: inline-block; color: #eb4d5c; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> had talent resource from across the globe and every day adding new Professionals over 10,000 </p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border: 5px solid #ddd; background: #f5f5f5; padding: 10px 20px;">
				<table width="100%" style="">
					<tr>
						<td>
							<span style="display: block; font-size: 15px;
							font-style: normal; padding: 2px 0;
							">Thanks & Regards </span>
							<span style="display: block; font-size: 14px;
								font-style: normal; padding: 2px 0;
							"> <a style="display: inline-block; color: #002359; text-decoration: none;" href="https://bsbstaffing.com/">bsbstaffing.com</a> </span>
						</td>
						<td align="right">
							<a href="#" style="display: inline-block;vertical-align: middle;max-width: 150px;">
								<img src="https://bsbstaffing.com/admin/public/images/logo.png" alt="LOGO" / width="100%">
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>