
<!DOCTYPE html>

<html>
<head>
    <title>BSB Email Template</title>
    <link rel="shortcut icon" type="image/x-icon" href="https://pxlsystech.com/html/bsbJobPortal/images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td colspan="2" align="center" valign="middle" style="position: relative; padding: 10px 0; background: #6b7dc5;">
        <a href="https://urentme.com" style="display: inline-block;"><img src="https://pxlsystech.com/html/bsbJobPortal/images/logo-com.png" title="BSB Job Portal" alt="BSB Job Portal"/></a>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center" valign="middle" style="position: relative;">
        <h3 style="color: #225d63; font-size: 22px; line-height: 26px; font-weight: bold; padding: 10px; margin: 10px 0;background: #fff;">Mail From {{$user->employerUser->company_name}}</h3>
      </td>
    </tr>
    <tr>
      <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">
        <p style="margin-bottom: 10px; font-size: 16px; line-height: 22px; font-weight: bold; color: #666;">{{$job->job_title}}</p>
        <p style="margin: 0; font-size: 15px; line-height: 22px; color: #666;">Exp {{$job->minExperiance->name}}-{{$job->maxExperiance->name}} Yrs</p>
        <p style="margin: 0; font-size: 15px; line-height: 22px; color: #666;">Salary: {{$job->min_monthly_salary}}-{{$job->max_monthly_salary}} LPM</p>
        <p style="margin: 0; font-size: 15px; line-height: 22px; color: #666;">Location: {{$job->city->name}}</p>
      </td>
      <td style="padding: 10px 20px; width: 30%;" align="right" valign="middle">
        <a href="{{url(config('app.frontendUrl')).'/single-job-details/'.$job->_id}}" style="display: inline-block; background: #052195; color: #fff; padding: 10px 15px; font-size: 15px; line-height: 22px; text-decoration: none;">Apply Now</a>
      </td>
    </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" style="position: relative; padding: 10px 20px; ">
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">{{$job->job_title}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Call for more information: {{$user->phone_no}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">{{$job->about_this_job}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Benefit: {{$job->benefits}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Country: {{$job->country->name}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Responsibility: {{$job->primary_responsibility}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Language Requirement: {{implode(',',$job->language()->pluck('language_name')->toArray())}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Experiance: {{$job->minExperiance->name}}-{{$job->maxExperiance->name}} Yrs</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Salary: {{$job->min_monthly_salary}}-{{$job->max_monthly_salary}} PM</p>
            <br/><br/>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">A positive reply would be awaited</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">With regards and thanks</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Your sincerely,</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">{{$user->employerUser->first_name}} {{$user->employerUser->last_name}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Cell phone: {{$user->phone_no}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;"> {{$user->employerUser->company_name}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;"> {{$user->employerUser->address}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;"> {{$user->employerUser->company_type}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Email:  {{$user->email}}</p>
            <p style="margin: 0 0 10px 0; font-size: 15px; line-height: 22px; color: #666;">Website:  {{$user->company_website}}</p>
          </td>
        </tr>
    <tr>
      <td colspan="2" style="padding: 10px 20px; " align="center" valign="middle">
        <p style="margin: 0 0 20px 0;"><a href="{{url(config('app.frontendUrl')).'/single-job-details/'.$job->_id}}" style="display: inline-block; background: #052195; color: #fff; padding: 10px 15px; font-size: 15px; line-height: 22px; text-decoration: none;">Apply Now</a></p>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center" valign="middle" style="position: relative; padding: 10px 20px; background: #f5f5f5; ">
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
          <tr>
              <td valign="middle" align="left" style="padding: 4px 0 0 0;">
                <p style="margin: 0 0 0px 0; font-size: 15px; line-height: 22px; color: #666;">&copy; All Right Reserved</p>
              </td>
              <td valign="middle" style="text-align: right;">
                <a href="#" target="_blank" style="display: inline-block; margin-right: 5px; vertical-align: middle;
                "><img style="max-width: 100%;" src="https://pxlsystech.com/html/bsbJobPortal/images/facebook.png" alt="BSB Job Portal"/></a>
                 <a href="#" target="_blank" style="display: inline-block; margin-right: 5px; vertical-align: middle;
                "><img style="max-width: 100%;" src="https://pxlsystech.com/html/bsbJobPortal/images/twitter.png" alt="BSB Job Portal"/></a>
                 <a href="#" target="_blank" style="display: inline-block; vertical-align: middle;
                "><img style="max-width: 100%;" src="https://pxlsystech.com/html/bsbJobPortal/images/linkedin.png" alt="BSB Job Portal"/></a>
              </td>
          </tr>
        </table>
      </td>
    </tr>
</table>
</body>
</html>
