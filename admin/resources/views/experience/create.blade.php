@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Experience Master
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.experience-manage.index') }}"><i class="fa fa-dashboard"></i> Experience List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create Experience Master</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Experience Master Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.experience-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{old('name')}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('value'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="value">Value<span class="required_field">*</span></label>
                           <input type="text" name="value" class="form-control" value="{{old('value')}}">
                           @if($errors->has('value'))
                           <span class="error">{{$errors->first('value')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('order'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="order">Orders<span class="required_field">*</span></label>
                           <input type="text" name="order" class="form-control" value="{{old('order')}}">
                           @if($errors->has('order'))
                           <span class="error">{{$errors->first('order')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <input type="hidden" name="status" value="1">
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection