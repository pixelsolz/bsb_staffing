@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Experience Master
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.experience-manage.index') }}"><i class="fa fa-dashboard"></i> Experience Master List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Experience Master</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Other Master Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.experience-manage.update',$experience->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{@$experience->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('value'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="value">Value<span class="required_field">*</span></label>
                           <input type="text" name="value" class="form-control" value="{{@$experience->value}}">
                           @if($errors->has('value'))
                           <span class="error">{{$errors->first('value')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('order'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="order">Orders<span class="required_field">*</span></label>
                           <input type="text" name="order" class="form-control" value="{{@$experience->order}}">
                           @if($errors->has('order'))
                           <span class="error">{{$errors->first('order')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="status">Status</label>
                           <select name="status" class="form-control">
                              <option value="">select status</option>
                              <option value="1" @if($experience->status ==1) selected @endif>Active</option>
                              <option value="2" @if($experience->status ==2) selected @endif>Inactive</option>
                           </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection