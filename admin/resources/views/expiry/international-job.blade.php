@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>
              {{--<form action="{{url('employer/posted-job')}}" method="get">
                <div class="row">
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="start_date" class="form-control pull-right datepicker" placeholder="Start Date" value="{{!empty($request->start_date)?$request->start_date: '' }}">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="end_date" class="form-control pull-right datepicker" placeholder="End Date" value="{{!empty($request->end_date)?$request->end_date: '' }}">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="country_id">
                        <option value="">Select Country</option>
                        @foreach($allCountry as $country)
                        <option value="{{$country->_id}}" @if(!empty($request->country_id) && $request->country_id == $country->_id) selected @endif>{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="city_id">
                        <option value="">Select City</option>
                        @foreach($allcity as $city)
                        <option value="{{$city->_id}}" @if(!empty($request->city_id) && $request->city_id == $city->_id) selected @endif>{{$city->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="status">
                        <option value="">Select Status</option>
                        <option value="1" @if($request->status == 1) selected @endif>Active</option>
                        <option value="0" @if($request->status == "0") selected @endif>Inactive</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="industry">
                        <option value="">Select Industry</option>
                        @foreach($allIndustry as $industry)
                        <option value="{{$industry->_id}}" @if(!empty($request->industry) && $request->industry == $industry->_id) selected @endif>{{$industry->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="department">
                        <option value="">Select Department</option>
                        @foreach($allDeperment as $department)
                        <option value="{{$department->_id}}" @if(!empty($request->department) && $request->department == $department->_id) selected @endif>{{$department->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="job_title" placeholder="Search by job title" value="{{!empty($request->job_title)?$request->job_title: '' }}">
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by Employer name, email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>
                  <div class="col-xs-3">
                      <input type="text" class="form-control" name="search_by_employer" placeholder="Search by Search By Employer Id" value="{{!empty($request->search_by_employer)?$request->search_by_employer: '' }}">
                  </div>
                </div>


                  <div class="row ">
                    <div class="col-xs-12">
                    <button type="submit" class="btn btn-info">Search</button>
                    <a href="{{route('admin.employer.posted-job')}}" class="reset">Reset</a>
                    </div>
                  </div>
              </form>--}}
            </div>

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Company Name</th>
                  <th>Job Title</th>
                  <th>Country Name</th>
                  <th>City Name</th>
                  <th>Job Type</th>
                  <th>Candidate Type</th>
                  <th>Create Date</th>
                  <th>Expiry Date</th>
                 {{-- <th>Number Of CV Received</th> --}}
                  <!-- <th>Status</th> -->
                </tr>
                </thead>
                <tbody>
                @php $a=1; @endphp
                @forelse($jobs as $job)
               <tr>
                  <td>{{$a}}</td>
                  <td>{{@$job->employer->employerUser->company_name}}</td>
                  <td>{{$job->job_title}}</td>
                  <td>{{$job->locationCountry ? implode(',',$job->locationCountry()->pluck('name')->toArray()):'' }}</td>
                  <td>{{$job->locationCity ? implode(',',$job->locationCity()->pluck('name')->toArray()):'' }}</td>
                  <td>{{$job->job_type == 1?'Free job':'Premium Job'}}</td>
                  <td>{{$job->candidate_type == 1? 'Male female both':($job->candidate_type == 2?'Only for male':($job->candidate_type == 3?'Only for female':''))}}</td>
                  <td>{{\Carbon\Carbon::parse($job->created_at)->format('d/m/Y')}}</td>
                  <td>{{\Carbon\Carbon::parse($job->job_ageing)->format('d/m/Y')}}</td>
                  {{--<td>
                    @if($job->employeeAppliedJob->count())
                    <a href="{{url('employer/job-application-received/?id='.@$job->_id)}}">{{@$job->employeeAppliedJob->count()}}</a>
                    @else
                    {{@$job->employeeAppliedJob->count()}}
                    @endif
                  </td>--}}


                  @php $url = url('employer/job/status-change'); @endphp
                  {{--<td><input type="checkbox" @if($job->job_status ==1) checked @endif data-toggle="toggle" data-on="Active" data-off="Inactive" onchange="statusChange(event, '{{$url}}', '{{$job->_id}}')"></td>--}}
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>

                </tr>
               @endforelse
                </tbody>

              </table>
              </div>
              <div>{{ $jobs->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
