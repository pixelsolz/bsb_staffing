<h5>
    Downloaded on {{\Carbon\Carbon::now()->format('d/m/Y')}}
</h5>
<table>
    <thead>
    <tr>
        <th>SL no.</th>
        <th>Candidate Name</th>
        <th>DOB</th>
        <th>Email ID</th>
        <th>Contact Number</th>
        <th>Preferred Location</th>
        <th>Total Work Experience</th>
        <th>Current Annual Salary</th>
        <th>Current Job Title</th>
        <th>Current Functional Area</th>
        <th>Current Industry</th>
        <th>Current Company</th>
        <th>Years in Current Job</th>
        <th>Notice Period</th>
        <th>Highest Education Level</th>
        <th>Highest Education Stream</th>
        <th>Highest Education Institute</th>
        <th>Year Of Passing</th>
        <th>Highest Education Course Type</th>
        <th>Last Modified Date</th>
        <th>Last Active Date</th>
        <th>Note</th>
        <th>Resume</th>

    </tr>
    </thead>
    <tbody>
        @php $a=1; @endphp
    @foreach($employees as $employee)
       <tr>
            <td>{{$a}}</td>
            <td>{{ @$employee->employeeUser->full_name }}</td>
            <td>{{ @$employee->employeeUser->date_of_birth}}</td>
            <td>{{ @$employee->email}}</td>
            <td>{{ @$employee->phone_no}}</td>
            <td>{{ @$employee->employeePrefTxn->cities ? implode(',',$employee->employeePrefTxn->cities()->pluck('name')->toArray()):'' }}</td>
            <td>{{ @$employee->employeeUser->totExprYr->name}}</td>
            <td>{{ @$employee->employeeUser->current_salary}}</td>
            <td>{{ @$employee->employeePrefTxn->jobRole()->first()->role_name}}</td>
            <td>{{ @$employee->employeePrefTxn->industries()->first()->name}}</td>
            <td>{{-- $employee->employeePrefTxn->industries->name --}}</td>
            <td>{{ @$employee->employeeUser->totExprYr->name }}</td>
            <td></td>
            <td>{{ @$employee->employeeUser->noticePeriod->name}}</td>
            <td>{{ @$employee->employeeEducation()->orderBy('id','desc')->first()->degree->name}}</td>
            <td>{{ @$employee->employeeEducation()->orderBy('id','desc')->first()->course_name}}</td>
            <td>{{ @$employee->employeeEducation()->orderBy('id','desc')->first()->name_of_institute}}</td>
            <td>{{ @$employee->employeeEducation()->orderBy('id','desc')->first()->passout_year }}</td>
            <td>{{ @$employee->employeeEducation()->orderBy('id','desc')->first()->course_title }}</td>
            <td>{{ @$employee->last_login}}</td>
            <td>{{ @$employee->last_login}}</td>
            <td>{{ @$employee->employeeCvNote()->orderBy('id','desc')->first()->created_at}}</td>
            <td>@if(@$employee->employeeCvTxn->_id){{url('/').'/resume/download?resume_id='.@$employee->employeeCvTxn->_id}}@endif</td>
        </tr>
         @php $a++; @endphp
    @endforeach
    </tbody>
</table>

<p>Note: The data downloaded shall be used for the purpose of recruitment and hiring process only. Any misuse / spamming / unsolicited mailing of data will tantamount to termination or suspension of BSB jobportal service at the sole discretion of BSB jobportal.</p>