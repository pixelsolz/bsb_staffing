  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('public/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{\Auth::guard('admin')->user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     <!--  <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @if(Auth::guard('admin')->user()->is_admin == 1)
        <li class="{{Request::is('/')? 'active': ''}}">
          <a href="{{route('admin.dasboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>

         <li class="treeview {{(Request::is('user-manage') || Request::is('user-manage/*')) || Request::is('user/*')? 'active': ''}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>User Manage</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('user-manage/employee'))? 'active': ''}}"><a href="{{route('admin.user-manage.employee')}}"><i class="fa fa-circle-o"></i>Employee</a></li>
            <li class="{{(Request::is('user-manage/employer'))? 'active': ''}}"><a href="{{route('admin.user-manage.employer')}}"><i class="fa fa-circle-o"></i>Employer</a></li>
            <!-- <li class="{{(Request::is('user-manage/other-user'))? 'active': ''}}"><a href="{{route('admin.user-manage.index')}}"><i class="fa fa-circle-o"></i>Other Users</a></li> -->
            <li class="{{(Request::is('user-manage/franchise'))? 'active': ''}}"><a href="{{route('admin.user-manage.franchise')}}"><i class="fa fa-circle-o"></i>Franchise</a></li>
            <li class="{{(Request::is('user-manage/agency'))? 'active': ''}}"><a href="{{route('admin.user-manage.agency')}}"><i class="fa fa-circle-o"></i>Agency</a></li>
            <li class="{{(Request::is('user-manage/college'))? 'active': ''}}"><a href="{{route('admin.user-manage.college')}}"><i class="fa fa-circle-o"></i>College</a></li>
            <li class="{{(Request::is('user-manage/trainer'))? 'active': ''}}"><a href="{{route('admin.user-manage.trainer')}}"><i class="fa fa-circle-o"></i>Trainer</a></li>
            <!-- <li class="{{(Request::is('user/internal-employee/*'))? 'active': ''}}"><a href="{{route('admin.user-manage.list.internal-employee')}}"><i class="fa fa-circle-o"></i>Internal Employee</a></li> -->
            <li class="{{(Request::is('user-manage/all/deleted-user'))? 'active': ''}}"><a href="{{route('admin.user-manage.deleted-user')}}"><i class="fa fa-circle-o"></i>Deleted Users</a></li>
          </ul>
        </li>
        <li class="{{(Request::is('staff') || Request::is('staff/*'))? 'active': ''}}">
          <a href="{{route('admin.user-manage.list.internal-employee')}}">
            <i class="fa fa-group"></i> <span>Staff</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        <li class="{{(Request::is('employee/applied-job') || Request::is('employee/applied-job/*'))? 'active': ''}}">
          <a href="{{route('admin.employee.applied-job')}}">
            <i class="fa fa-group"></i> <span>Job application</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        <li class="{{(Request::is('employer/posted-job') || Request::is('employer/posted-job/*'))? 'active': ''}}">
          <a href="{{route('admin.employer.posted-job')}}">
            <i class="fa fa-group"></i> <span>Job posted</span>
          </a>

        </li>

        <li class="treeview {{(Request::is('domestic-job/applicant') || Request::is('domestic-job/applicant/*')) || Request::is('domestic-job/applicant/*')? 'active': ''}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Domestic Job Application</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('domestic-job/applicant/jobs'))? 'active': ''}}"><a href="{{url('domestic-job/applicant','jobs')}}"><i class="fa fa-circle-o"></i>Domestic Normal Job </a></li>
            <li class="{{(Request::is('domestic-job/applicant/international'))? 'active': ''}}"><a href="{{url('domestic-job/applicant','international')}}"><i class="fa fa-circle-o"></i>Domestic International Job </a></li>
             <li class="{{(Request::is('domestic-job/applicant/walking'))? 'active': ''}}"><a href="{{url('domestic-job/applicant','walking')}}"><i class="fa fa-circle-o"></i>Domestic Walking </a></li>

          </ul>
        </li>

       <li class="treeview {{(Request::is('international-job/applicant') || Request::is('international-job/applicant/*')) || Request::is('international-job/applicant/*')? 'active': ''}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>International Job Application</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('international-job/applicant/jobs'))? 'active': ''}}"><a href="{{url('international-job/applicant','jobs')}}"><i class="fa fa-circle-o"></i>International Normal Job</a></li>
            <li class="{{(Request::is('international-job/applicant/international'))? 'active': ''}}"><a href="{{url('international-job/applicant','international')}}"><i class="fa fa-circle-o"></i>International International Job</a></li>
             <li class="{{(Request::is('international-job/applicant/walking'))? 'active': ''}}"><a href="{{url('international-job/applicant','walking')}}"><i class="fa fa-circle-o"></i>International walking</a></li>

          </ul>
        </li>

        <li class="{{(Request::is('employer/international-job') || Request::is('employer/international-job/*'))? 'active': ''}}">
          <a href="{{route('admin.employer.international-posted-job')}}">
            <i class="fa fa-group"></i> <span>International Job posted</span>
          </a>

        </li>

        <li class="treeview {{(Request::is('deleted') || Request::is('deleted/*'))? 'active': ''}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>All Deleted Jobs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('deleted/jobs'))? 'active': ''}}"><a href="{{route('admin.deletd.jobs')}}"><i class="fa fa-circle-o"></i>Jobs</a></li>
            <li class="{{(Request::is('deleted/international-job'))? 'active': ''}}"><a href="{{route('admin.deletd.international-job')}}"><i class="fa fa-circle-o"></i>International Jobs</a></li>
           {{-- <li class="{{(Request::is('deleted/walking'))? 'active': ''}}"><a href="{{route('admin.deletd.walking')}}"><i class="fa fa-circle-o"></i>Walking</a></li>--}}
          </ul>
        </li>

        <li class="treeview {{(Request::is('expired') || Request::is('expired/*'))? 'active': ''}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>All Expired Jobs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('expired/jobs'))? 'active': ''}}"><a href="{{route('admin.expired.jobs')}}"><i class="fa fa-circle-o"></i>Jobs</a></li>
            <li class="{{(Request::is('expired/international-job'))? 'active': ''}}"><a href="{{route('admin.expired.international-job')}}"><i class="fa fa-circle-o"></i>International Jobs</a></li>
            {{--<li class="{{(Request::is('expired/walking'))? 'active': ''}}"><a href="{{route('admin.expired.walking')}}"><i class="fa fa-circle-o"></i>Walking</a></li>--}}
          </ul>
        </li>

        <li class="treeview {{(Request::is('draft') || Request::is('draft/*')) ? 'active': ''}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>All Draft Jobs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('draft/jobs'))? 'active': ''}}"><a href="{{route('admin.draft.jobs')}}"><i class="fa fa-circle-o"></i>Jobs</a></li>
            <li class="{{(Request::is('draft/international-job'))? 'active': ''}}"><a href="{{route('admin.draft.international-job')}}"><i class="fa fa-circle-o"></i>International Jobs</a></li>
            {{--<li class="{{(Request::is('draft/walking'))? 'active': ''}}"><a href="{{route('admin.draft.walking')}}"><i class="fa fa-circle-o"></i>Walking</a></li>--}}
          </ul>
        </li>


        <li class="{{(Request::is('role') || Request::is('role/*'))? 'active': ''}}">
          <a href="{{route('admin.role.index')}}">
            <i class="fa fa-group"></i> <span>Role Manage</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        <li class="{{(Request::is('benefit') || Request::is('benefit/*'))? 'active': ''}}">
          <a href="{{route('admin.benefit-manage.index')}}">
            <i class="fa fa-group"></i> <span>Benefit Manage</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        <li class="{{(Request::is('announcement-template') || Request::is('announcement-template/*'))? 'active': ''}}">
          <a href="{{route('admin.announcement-template.list-all')}}">
            <i class="fa fa-group"></i> <span>Announcement Massage</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        <li class="{{(Request::is('email-template/list') || Request::is('email-template/list'))? 'active': ''}}">
          <a href="{{url('email-template/list')}}">
            <i class="fa fa-envelope"></i> <span>Email Templates</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        @php $requestUrl = Request::url(); @endphp
        <li class="treeview {{(strpos($requestUrl,'country-manage')|| strpos($requestUrl,'city-manage') || strpos($requestUrl,'department-manage')|| strpos($requestUrl,'attribute-manage') || strpos($requestUrl,'banner-manage') || strpos($requestUrl,'course-manage') || strpos($requestUrl,'companysize-manage') || strpos($requestUrl,'companytype-manage') || strpos($requestUrl,'degree-manage') || strpos($requestUrl,'industry-manage') || strpos($requestUrl,'jobsector-manage') || strpos($requestUrl,'language-manage') || strpos($requestUrl,'userentity-manage') || strpos($requestUrl,'other-master-manage') || strpos($requestUrl,'application-phase'))? 'active': ''}}">
            <!-- <a href="{{route('admin.cms-manage.index')}}"> -->
            <a href="#">
              <i class="fa fa-files-o"></i> <span>All Masters</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{(strpos($requestUrl,'country-manage'))? 'active': ''}}"><a href="{{route('admin.country-manage.index')}}"><i class="fa fa-circle-o"></i>Country</a></li>

              <li class="{{(strpos($requestUrl,'city-manage'))? 'active': ''}}"><a href="{{route('admin.city-manage.index')}}"><i class="fa fa-circle-o"></i>City</a></li>

              <li class="{{(strpos($requestUrl,'banner-manage'))? 'active': ''}}"><a href="{{route('admin.banner-manage.index')}}"><i class="fa fa-circle-o"></i>Banner Image</a></li>

              <li class="{{(strpos($requestUrl,'department-manage'))? 'active': ''}}"><a href="{{route('admin.department-manage.index')}}"><i class="fa fa-circle-o"></i>Department</a></li>

              <li class="{{(strpos($requestUrl,'attribute-manage'))? 'active': ''}}"><a href="{{route('admin.attribute-manage.index')}}"><i class="fa fa-circle-o"></i>Attribute</a></li>

              <li class="{{(strpos($requestUrl,'course-manage'))? 'active': ''}}"><a href="{{route('admin.course-manage.index')}}"><i class="fa fa-circle-o"></i>Courses</a></li>

              <li class="{{(strpos($requestUrl,'companysize-manage'))? 'active': ''}}"><a href="{{route('admin.companysize-manage.index')}}"><i class="fa fa-circle-o"></i>Company size</a></li>

              <li class="{{(strpos($requestUrl,'companytype-manage'))? 'active': ''}}"><a href="{{route('admin.companytype-manage.index')}}"><i class="fa fa-circle-o"></i>Company Type</a></li>

              <li class="{{(strpos($requestUrl,'degree-manage'))? 'active': ''}}"><a href="{{route('admin.degree-manage.index')}}"><i class="fa fa-circle-o"></i>Degree</a></li>

              <li class="{{(strpos($requestUrl,'industry-manage'))? 'active': ''}}"><a href="{{route('admin.industry-manage.index')}}"><i class="fa fa-circle-o"></i>Industry</a></li>

              <li class="{{(strpos($requestUrl,'jobsector-manage'))? 'active': ''}}"><a href="{{route('admin.jobsector-manage.index')}}"><i class="fa fa-circle-o"></i>Job sector</a></li>

              <li class="{{(strpos($requestUrl,'language-manage'))? 'active': ''}}"><a href="{{route('admin.language-manage.index')}}"><i class="fa fa-circle-o"></i>Language</a></li>

              <li class="{{(strpos($requestUrl,'application-phase'))? 'active': ''}}"><a href="{{route('admin.application-phase.index')}}"><i class="fa fa-circle-o"></i>Application Phase</a></li>

              <li class="{{(strpos($requestUrl,'userentity-manage'))? 'active': ''}}"><a href="{{route('admin.userentity-manage.index')}}"><i class="fa fa-circle-o"></i>User Entity</a></li>
              <li class="{{(strpos($requestUrl,'experience-manage'))? 'active': ''}}"><a href="{{route('admin.experience-manage.index')}}"><i class="fa fa-circle-o"></i>Experience</a></li>

              <li class="{{(strpos($requestUrl,'other-master-manage'))? 'active': ''}}"><a href="{{route('admin.other-master-manage.index')}}"><i class="fa fa-circle-o"></i>Others master</a></li>
            </ul>
        </li>

        <li class="{{(Request::is('service-package') || Request::is('service-package/*'))? 'active': ''}}">
          <a href="{{route('admin.service-package.index')}}">
            <i class="fa fa-group"></i> <span>Service Package</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>

        <li class="{{(Request::is('payment-config') || Request::is('payment-config/*'))? 'active': ''}}">
          <a href="{{route('admin.payment-config.index')}}">
            <i class="fa fa-cog"></i> <span>Payment Configuration</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>

      {{--<li class="{{(Request::is('cms-manage') || Request::is('cms-manage/*'))? 'active': ''}}">
          <a href="{{route('admin.cms-manage.index')}}">
            <i class="fa fa-files-o"></i> <span>CMS Manage</span>
          </a>

      </li>--}}
      @endif

      @if(Auth::guard('admin')->user()->roles->first()->slug == 'internal_employee')
      <li class="{{Request::is('/')? 'active': ''}}">
          <a href="{{route('internal-emp.dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <li>
          <a href="{{route('internal-emp.list')}}">
            <i class="fa fa-files-o"></i> <span>User Register</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

      </li>
      @endif

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>