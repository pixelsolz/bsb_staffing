@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Industry
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Industry List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Industry</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Industry Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.industry-manage.update',$industry->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{$industry->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('attribute_id'))has-error @endif">
                      <div class="col-xs-9">
                      <label for="attribute_id">Attribute<span class="required_field">*</span></label>
                        <select class="form-control" name="attribute_id">
                           <option value="">select attribute</option>
                              @foreach($attrMsts as $attr)
                                 <option value="{{$attr->_id}}" @if($industry->attribute_id == $attr->_id) selected @endif>{{$attr->name}}</option>
                              @endforeach
                        </select>
                        @if($errors->has('attribute_id'))
                           <span class="error">{{$errors->first('attribute_id')}}</span>
                        @endif
                        </div>
                     </div>

                     <!-- <div class="form-group row @if($errors->has('service_id'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="service_id">Service<span class="required_field">*</span></label>
                        <select class="form-control" name="service_id">
                           <option value="">select</option>
                           @foreach($serviceMsts as $service)
                           <option value="{{$service->_id}}" @if($industry->service_id == $service->_id) selected @endif>{{$service->service_name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('service_id'))
                           <span class="error">{{$errors->first('service_id')}}</span>
                        @endif
                        </div>
                     </div> -->
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_title">SEO Title</label>
                           <input type="text" name="seo_title" class="form-control" value="{{@$industry->seo_title}}">
                           @if($errors->has('seo_title'))
                           <span class="error">{{$errors->first('seo_title')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_description'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_description">SEO Description</label>
                           <textarea name="seo_description" class="form-control" placeholder="Enter SEO Description ">{{@$industry->seo_description}}</textarea>
                           @if($errors->has('seo_description'))
                           <span class="error">{{$errors->first('seo_description')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_keywords'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_keywords">SEO Keywords</label>
                           <input type="text" name="seo_keywords" class="form-control" value="{{@$industry->seo_keywords}}">
                           @if($errors->has('seo_keywords'))
                           <span class="error">{{$errors->first('seo_keywords')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="status">Status<span class="required_field">*</span></label>
                        <select class="form-control" name="status">
                           <option value="">select status</option>
                           <option value="1" @if($industry->status ==1) selected @endif>Active</option>
                           <option value="2" @if($industry->status ==2) selected @endif>Inactive</option>
                        </select>
                        @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                        @endif
                        </div>
                     </div>


                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection