@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Job Sector
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.jobsector-manage.index') }}"><i class="fa fa-dashboard"></i> Job Sector List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create Job Sector</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Job Sector Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.jobsector-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('sector_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="sector_name">Sector Name<span class="required_field">*</span></label>
                           <input type="text" name="sector_name" class="form-control" value="{{old('sector_name')}}">
                           @if($errors->has('sector_name'))
                           <span class="error">{{$errors->first('sector_name')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection