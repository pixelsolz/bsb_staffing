@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}

      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>


    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>

                <form action="{{url('domestic-job/applicant',$type)}}" method="get">
                <div class="row">

                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="start_date" class="form-control pull-right datepicker" placeholder="Start Date" value="{{!empty($request->start_date)?$request->start_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="end_date" class="form-control pull-right datepicker" placeholder="End Date" value="{{!empty($request->end_date)?$request->end_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="country_id">
                        <option value="">Select Country</option>
                        @foreach($allCountry as $country)
                        <option value="{{$country->_id}}" @if(!empty($request->country_id) && $request->country_id == $country->_id) selected @endif>{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="city_id">
                        <option value="">Select City</option>
                        @foreach($allcity as $city)
                        <option value="{{$city->_id}}" @if(!empty($request->city_id) && $request->city_id == $city->_id) selected @endif>{{$city->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="application_phase">
                        <option value="">Select Application Phase</option>
                        @foreach($allApplicationPhase as $applicationPhase)
                        <option value="{{$applicationPhase->_id}}" @if(!empty($request->application_phase) && $request->application_phase == $applicationPhase->_id) selected @endif>{{$applicationPhase->phase_name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="industry">
                        <option value="">Select Industry</option>
                        @foreach($allIndustry as $industry)
                        <option value="{{$industry->_id}}" @if(!empty($request->industry) && $request->industry == $industry->_id) selected @endif>{{$industry->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="department">
                        <option value="">Select Department</option>
                        @foreach($allDeperment as $department)
                        <option value="{{$department->_id}}" @if(!empty($request->department) && $request->department == $department->_id) selected @endif>{{$department->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="job_title" placeholder="Search by job title" value="{{!empty($request->job_title)?$request->job_title: '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by Employee name, employee id , email or mobile no" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>


                </div>


                  <div class="row ">
                    <div class="col-xs-12">
                    <button type="submit" class="btn btn-info">Search</button>
                    <a href="{{url('domestic-job/applicant',$type)}}" class="reset">Reset</a>
                    </div>
                  </div>
              </form>

            </div>

            @if($type =='jobs')
            <div class="box-body table-holder">
               <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Employee Name</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Gender</th>
                  <th>Employer Company Name</th>
                  <th>Job Title</th>
                  <th>Applied Date</th>
                  <th>Counrty</th>
                  <th>City</th>
                  <th>Status</th>

                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($jobs as $job)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{@$job->employee->employeeUser->first_name}} {{@$job->employee->employeeUser->last_name}}</td>
                  <td>{{@$job->employee->employeeUser->country->name}}</td>
                  <td>{{@$job->employee->employeeUser->city->name}}</td>
                  <td>{{@$job->employee->employeeUser->gender_text}}</td>
                  <td>{{@$job->job->employer->employerUser->company_name}}</td>
                  <td>{{$job->job->job_title}}</td>
                  <td>{{\Carbon\Carbon::parse($job->created_at)->format('d/m/Y')}}</td>
                  <td>{{@$job->job->employer->employerUser->country->name}}</td>
                  <td>{{@$job->job->employer->employerUser->city->name}}</td>
                  <td>{{$job->applicationStatus->phase_name}}</td>
                  <td>
                    <a href="{{route('admin.employee.applied-job.show', ['id'=>$job->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>

              <div>{{ $jobs->links() }}</div>
              </div>
            </div>
            @elseif($type =='international')
            <div class="box-body table-holder">
               <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Employee Name</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Gender</th>
                  <th>Employer Company Name</th>
                  <th>Job Title</th>
                  <th>Applied Date</th>
                  <th>Counrty</th>
                  <th>City</th>
                  <th>Status</th>

                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($international as $job)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{@$job->employee->employeeUser->first_name}} {{@$job->employee->employeeUser->last_name}}</td>
                  <td>{{@$job->employee->employeeUser->country->name}}</td>
                  <td>{{@$job->employee->employeeUser->city->name}}</td>
                  <td>{{@$job->employee->employeeUser->gender_text}}</td>
                  <td>{{@$job->job->employer->employerUser->company_name}}</td>
                  <td>{{$job->job->job_title}}</td>
                  <td>{{\Carbon\Carbon::parse($job->created_at)->format('d/m/Y')}}</td>
                  <td>{{@$job->job->employer->employerUser->country->name}}</td>
                  <td>{{@$job->job->employer->employerUser->city->name}}</td>
                  <td>{{$job->applicationStatus->phase_name}}</td>
                  <td>
                    <a href="{{route('admin.employee.applied-internationaljob.show', ['id'=>$job->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>

              <div>{{ $international->links() }}</div>
              </div>
            </div>
            @elseif($type=='walking')
            <div class="box-body table-holder">
               <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Employee Name</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Gender</th>
                  <th>Employer Company Name</th>
                  <th>Job Title</th>
                  <th>Applied Date</th>
                  <th>Counrty</th>
                  <th>City</th>
                  <th>Book Date</th>

                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($walking as $job)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{@$job->user->employeeUser->first_name}} {{@$job->user->employeeUser->last_name}}</td>
                  <td>{{@$job->user->employeeUser->country->name}}</td>
                  <td>{{@$job->user->employeeUser->city->name}}</td>
                  <td>{{@$job->user->employeeUser->gender_text}}</td>
                  <td>{{@$job->interview->employer->employerUser->company_name}}</td>
                  <td>{{$job->interview->job_title}}</td>
                  <td>{{\Carbon\Carbon::parse($job->created_at)->format('d/m/Y')}}</td>
                  <td>{{@$job->interview->employer->employerUser->country->name}}</td>
                  <td>{{@$job->interview->employer->employerUser->city->name}}</td>
                  <td>{{\Carbon\Carbon::parse($job->booked_date)->format('d/m/Y')}}</td>
                  <td>
                    <a href="{{route('admin.employee.applied-walkingjob.show', ['id'=>$job->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>

              <div>{{ $walking->links() }}</div>
              </div>
            </div>
            @endif
          </div>



        </div>

      </div>

    </section>

  </div>

  @endsection
