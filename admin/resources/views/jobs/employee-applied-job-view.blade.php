@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        {{$title}}
      </h1>
      <ol class="breadcrumb">
         <li>
            @if($type =='job')
            <a href="{{ route('admin.employee.applied-job') }}"><i class="fa fa-dashboard"></i> Applied Job List</a>
            @elseif($type =='international')
            <a href="{{ route('admin.employee.applied-international-job') }}"><i class="fa fa-dashboard"></i> Applied Job List</a>
            @endif
         </li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> View Applied Job</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Applied Job View</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <div class="form-wrpr">
                  <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Application ID : </label>
                        <span>{{@$applied_job->bsb_jb_app_id}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name : </label>
                        <span>{{@$applied_job->employee->employeeUser->first_name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name : </label>
                        <span>{{@$applied_job->employee->employeeUser->last_name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>User Name : </label>
                        <span>{{@$applied_job->employee->user_name}}</span>
                     </div>

                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email : </label>
                        <span>{{@$applied_job->employee->email}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile : </label>
                        <span>{{@$applied_job->employee->phone_no}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Resume : </label>
                        <span><a href="{{Storage::disk('s3')->url('/upload_files/employee-resumes/' . @$applied_job->employee->employeeCvTxn->cv_path)}}" download target="_blank">{{@$applied_job->employee->employeeCvTxn->cv_path}}</a></span>
                     </div>
                  </div>
               </div>
               <div class="form-wrpr">

                  <div class="row">
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Job Title : </label>
                        <span>{{@$applied_job->job->job_title}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Country : </label>
                        @if($type =='job' || $type =='walking')
                        <span>{{@$applied_job->job->country->name}}</span>
                        @elseif($type =='international')
                        <span>{{@$applied_job->job->locationCountry ? implode(',',@$applied_job->job->locationCountry()->pluck('name')->toArray()):''}}</span>
                        @endif
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>City : </label>
                        @if($type =='job' || $type =='walking')
                        <span>{{@$applied_job->job->city->name}}</span>
                        @elseif($type =='international')
                        <span>{{@$applied_job->job->locationCity ? implode(',',@$applied_job->job->locationCity()->pluck('name')->toArray()) :''}}</span>
                        @endif
                     </div>

                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Required Skills : </label>
                        <span>{{@implode(',',$applied_job->job->required_skill)}}</span>
                     </div>

                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Experiance : </label>
                        <span>{{@$applied_job->job->minExperiance->name.' - '.$applied_job->job->maxExperiance->name. ' (Years)'}}</span>
                     </div>

                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Employement For: </label>
                        <span>{{@$applied_job->job->employmentFor->name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>About This Job: </label>
                        <span>{!!@$applied_job->job->about_this_job!!}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Benefits This Job: </label>
                        <span>{!!@$applied_job->job->benefits!!}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Employment Type: </label>
                        <span>{{@$applied_job->job->employmentType->name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Number of Vacancies: </label>
                        <span>{{@$applied_job->job->number_of_vacancies}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Salary: </label>
                        <span>{{@$applied_job->job->min_monthly_salary}} - {{@$applied_job->job->max_monthly_salary}} {{@$applied_job->job->currencydata->currency}} / {{@$applied_job->job->salary_type}}</span>
                     </div>

                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Special Notes: </label>
                        <span>{!!@$applied_job->job->special_notes!!}</span>
                     </div>

                  </div>
               </div>

            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection