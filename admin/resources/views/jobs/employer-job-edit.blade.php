@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Job
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.country-manage.index') }}"><i class="fa fa-dashboard"></i> Posted jobs</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Job</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Country Add Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" @if($jbType =='jobs') action="{{route('admin.employer.posted-job-update', $job->_id)}}" @elseif($jbType =='international') action="{{route('admin.employer.posted-international-job-update', $job->_id)}}" @endif method="POST" enctype="multipart/form-data">
                   @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('job_title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="job_title">Job Title<span class="required_field"></span></label>
                           <input type="text" name="job_title" class="form-control" value="{{$job->job_title}}">
                           @if($errors->has('job_title'))
                           <span class="error">{{$errors->first('job_title')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('country_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="country_id">Job Location Country<span class="required_field"></span></label>
                           @if($jbType =='jobs')
                           <select name="country_id" class="form-control">
                              <option value="">Select Country</option>
                               @foreach($allCountry as $country)
                              <option value="{{$country->_id}}" @if(!empty($job->country_id) && $job->country_id == $country->_id) selected @endif>{{$country->name}}</option>
                              @endforeach
                           </select>
                           @elseif($jbType =='international')
                             <select name="loc_countries" class="form-control" multiple="true">
                              <option value="">Select Country</option>
                               @foreach($allCountry as $country)
                              <option value="{{$country->_id}}" @if(!empty($job->loc_countries) && @$job->loc_countries && array_search($country->_id, $job->loc_countries) !== false) selected @endif>{{$country->name}}</option>
                              @endforeach
                           </select>
                           @endif
                           @if($errors->has('country_id'))
                           <span class="error">{{$errors->first('country_id')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('city_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="city_id">Job Location City<span class="required_field"></span></label>
                           @if($jbType =='jobs')
                           <select name="city_id" class="form-control">
                             <option value="">Select City</option>
                              @foreach($selectedCity as $city)
                              <option value="{{$city->_id}}" @if(!empty($job->city_id) && $job->city_id == $city->_id) selected @endif>{{$city->name}}</option>
                              @endforeach
                           </select>
                           @elseif($jbType =='international')
                           <select name="loc_cities" class="form-control" multiple="true">
                             <option value="">Select City</option>
                              @foreach($selectedLocationCities as $city)
                              <option value="{{$city->_id}}" @if(!empty($job->loc_cities) && @$job->loc_cities && array_search($city->_id, $job->loc_cities) !== false) selected @endif> {{$city->name}}</option>
                              @endforeach
                           </select>
                           @endif
                           @if($errors->has('city_id'))
                           <span class="error">{{$errors->first('city_id')}}</span>
                           @endif
                        </div>
                     </div>

                     @if($jbType =='international')
                     <div class="form-group row">
                        <div class="col-xs-9">
                           <label for="countries">Prefer Country<span class="required_field"></span></label>
                           <select name="countries" class="form-control">
                              <option value="">Select Country</option>
                               @foreach($allCountry as $country)
                              <option value="{{$country->_id}}" @if(!empty($job->countries) && $job->countries[0] == $country->_id) selected @endif>{{$country->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>

                     <div class="form-group row">
                        <div class="col-xs-9">
                           <label for="cities">Prefer City<span class="required_field"></span></label>
                           <select name="cities" class="form-control" multiple="true">
                             <option value="">Select City</option>
                              @foreach($selectedCity as $city)
                              <option value="{{$city->_id}}" @if(!empty($job->cities) && @$job->cities && array_search($city->_id, $job->cities) !== false) selected @endif> {{$city->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('city_id'))
                           <span class="error">{{$errors->first('city_id')}}</span>
                           @endif
                        </div>
                     </div>

                     @endif

                     <div class="form-group row @if($errors->has('about_this_job'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="about_this_job">About this job<span class="required_field"></span></label>
                           <textarea id="editor1" placeholder="About this job" name="about_this_job" class="form-control">{{@$job->about_this_job}}</textarea>
                           @if($errors->has('about_this_job'))
                           <span class="error">{{$errors->first('about_this_job')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('industry_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="industry_id">Select Industry<span class="required_field"></span></label>
                           <select name="industry_id[]" class="form-control" multiple="true">
                              @foreach($allIndustry as $industry)
                              <option value="{{$industry->_id}}" @if(!empty($job->industry_id) && @$job->industry_id && array_search($industry->_id, $job->industry_id) !== false) selected @endif>{{$industry->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('industry_id'))
                           <span class="error">{{$errors->first('industry_id')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('department_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="department_id">Current Department<span class="required_field"></span></label>
                           <select name="department_id[]" class="form-control" multiple="true">
                              @foreach($allDeperment as $department)
                              <option value="{{$department->_id}}" @if(!empty($job->department_id) && @$job->department_id && array_search($department->_id, $job->department_id) !== false) selected @endif>{{$department->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('department_id'))
                           <span class="error">{{$errors->first('department_id')}}</span>
                           @endif
                        </div>
                     </div>



                     <div class="form-group row @if($errors->has('received_email'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="received_email">Received Email<span class="required_field"></span></label>
                           <input type="text" name="received_email" class="form-control" value="{{$job->received_email}}">
                           @if($errors->has('received_email'))
                           <span class="error">{{$errors->first('received_email')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('employement_for'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="employement_for">Employement for<span class="required_field"></span></label>
                           <select name="employement_for" class="form-control">
                              <option value="">Select Country</option>
                               @foreach($employementFor as $emp_for)
                              <option value="{{$emp_for->_id}}" @if(!empty($job->employement_for) && $job->employement_for == $emp_for->_id) selected @endif>{{$emp_for->name}}</option>
                              @endforeach
                           </select>

                           @if($errors->has('employement_for'))
                           <span class="error">{{$errors->first('employement_for')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="required_skill">Add Skills</label>
                           <select name="required_skill[]" class="form-control" multiple="true">
                              @foreach($skills as $skill)
                              <option value="{{$skill->skill}}" @if(!empty($job->required_skill) && @$job->required_skill && array_search($skill->skill, $job->required_skill) !== false) selected @endif>{{$skill->skill}}</option>
                              @endforeach
                           </select>

                           @if($errors->has('required_skill'))
                           <span class="error">{{$errors->first('required_skill')}}</span>
                           @endif
                        </div>
                     </div>
                      <div class="form-group row @if($errors->has('benefits'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="benefits">Benefits<span class="required_field"></span></label>
                           <textarea id="editor2" placeholder="Benefits" name="benefits" class="form-control">{{@$job->benefits}}</textarea>
                           @if($errors->has('benefits'))
                           <span class="error">{{$errors->first('benefits')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('candidate_type'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="candidate_type">Candidate Type<span class="required_field">*</span></label>
                           <select name="candidate_type" class="form-control">
                              <option value="" selected>Select Candidate Type</option>
                              <option value="1" @if(@$job->candidate_type == 1) selected @endif>Male & Female both</option>
                              <option value="2" @if(@$job->candidate_type == 2) selected @endif>Only for Male</option>
                              <option value="3" @if(@$job->candidate_type == 3) selected @endif>Only for Female</option>
                           </select>
                           @if($errors->has('candidate_type'))
                           <span class="error">{{$errors->first('candidate_type')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="min_experiance">Minimum Experiance</label>
                           <select  name="min_experiance" class="form-control">
                              <option value="">Select Minimum Experiance</option>
                              @foreach($experiences as $experience)
                              <option value="{{$experience->_id}}" @if(!empty($job->min_experiance) && $job->min_experiance == $experience->_id) selected @endif>{{$experience->name}}</option>
                              @endforeach
                           </select>

                           @if($errors->has('min_experiance'))
                           <span class="error">{{$errors->first('min_experiance')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('max_experiance'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="max_experiance">Maximum Experiance</label>
                           <select  name="max_experiance" class="form-control">
                           <option value="">Select Maximum Experiance</option>
                              @foreach($experiences as $experience)
                              <option value="{{$experience->_id}}" @if(!empty($job->max_experiance) && $job->max_experiance == $experience->_id) !== false) selected @endif>{{$experience->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('max_experiance'))
                           <span class="error">{{$errors->first('max_experiance')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('min_qualification'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="min_qualification">UnderGraduate and Graduation</label>
                           <select name="min_qualification[]" class="form-control" multiple="true">
                              @foreach($degreesUnderGrad as $degree)
                              <option value="{{$degree->_id}}" @if(!empty($job->min_qualification) && @$job->min_qualification && array_search($degree->_id, $job->min_qualification) !== false) selected @endif>{{$degree->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('min_qualification'))
                           <span class="error">{{$errors->first('min_qualification')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('seo_keywords'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_keywords">And Or</label>
                           <div class="form-group">
                              <label class="checkbox-inline">
                                 <input type="radio" value="1" name="graduate_and_or_post_graduate" @if($job->graduate_and_or_post_graduate == 1) checked @endif> And
                               </label>
                               <label class="checkbox-inline">
                                 <input type="radio" value="2" name="graduate_and_or_post_graduate"  @if($job->graduate_and_or_post_graduate == 2) checked @endif> Or
                               </label>
                           </div>
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('max_qualification'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="max_qualification">PostGraduate and Doctorate</label>
                           <select name="max_qualification[]" class="form-control" multiple="true">
                              @foreach($degreesPostGrad as $degree)
                              <option value="{{$degree->_id}}" @if(!empty($job->max_qualification) && @$job->min_qualification && array_search(@$degree->_id, @$job->min_qualification)!== false) selected @endif>{{$degree->name}}</option>
                              @endforeach
                           </select>

                           @if($errors->has('max_qualification'))
                           <span class="error">{{$errors->first('max_qualification')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('primary_responsibility'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="primary_responsibility">Primary Responsibility</label>
                           <textarea id="editor3" placeholder="Primary Responsibility" class="form-control" name="primary_responsibility">{{@$job->primary_responsibility}}</textarea>
                           @if($errors->has('primary_responsibility'))
                           <span class="error">{{$errors->first('primary_responsibility')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="min_experiance">Employeement Type</label>
                           <select  name="min_experiance" class="form-control">
                              <option value="">Select Minimum Experiance</option>
                              @foreach($employementTypes as $employementType)
                              <option value="{{$employementType->_id}}" @if(!empty($job->employment_type) && $job->employment_type == $employementType->_id) selected @endif>{{$employementType->name}}</option>
                              @endforeach
                           </select>

                           @if($errors->has('min_experiance'))
                           <span class="error">{{$errors->first('min_experiance')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('number_of_vacancies'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="number_of_vacancies">No. of Vacancy</label>
                           <input type="text" value="{{@$job->number_of_vacancies}}" name="number_of_vacancies" placeholder="No. of Vacancy" class="form-control">
                           @if($errors->has('number_of_vacancies'))
                           <span class="error">{{$errors->first('number_of_vacancies')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('language_pref'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="language_pref">Language Preference</label>
                           <select name="language_pref[]" class="form-control" multiple="true">
                              @foreach($languages as $language)
                              <option value="{{$language->_id}}"  @if(!empty($job->language_id) && @$job->language_id && array_search(@$language->_id, @$job->language_id)!== false) selected @endif>{{$language->language_name}}</option>
                              @endforeach
                           </select>

                           @if($errors->has('language_pref'))
                           <span class="error">{{$errors->first('language_pref')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('seo_keywords'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="seo_keywords">Salary</label>
                           <div class="form-group">
                              <label class="checkbox-inline">
                                 <input type="radio" value="hourly" name="salary_type" @if($job->salary_type == 'hourly') checked @endif> Hourly
                               </label>
                               <label class="checkbox-inline">
                                 <input type="radio" value="weekly" name="salary_type" @if($job->salary_type == 'weekly') checked @endif> Weekly
                               </label>
                               <label class="checkbox-inline">
                                 <input type="radio" value="monthly" name="salary_type" @if($job->salary_type == 'monthly') checked @endif> Monthly
                               </label>
                           </div>
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('currency'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="currency">Select Currency<span class="required_field"></span></label>
                           <select name="currency" class="form-control">
                              <option></option>
                              @foreach($allCountry as $country)
                              <option value="{{$country->_id}}" @if(!empty($job->currency) && $job->currency == $country->_id) selected @endif>{{$country->currency}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('currency'))
                           <span class="error">{{$errors->first('currency')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('min_monthly_salary'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="min_monthly_salary">Min Salary</label>
                           <input type="text" name="min_monthly_salary" class="form-control" value="{{@$job->min_monthly_salary}}">
                           @if($errors->has('min_monthly_salary'))
                           <span class="error">{{$errors->first('min_monthly_salary')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('max_monthly_salary'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="max_monthly_salary">Max Salary</label>
                           <input type="text" name="max_monthly_salary" class="form-control" value="{{@$job->max_monthly_salary}}">
                           @if($errors->has('max_monthly_salary'))
                           <span class="error">{{$errors->first('max_monthly_salary')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('special_notes'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="special_notes">Special Notes</label>
                           <textarea id="editor4" placeholder="Special Notes" class="form-control" name="special_notes">{{@$job->special_notes}}</textarea>
                           @if($errors->has('special_notes'))
                           <span class="error">{{$errors->first('special_notes')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection