@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>


            </div>
            <!-- /.box-header -->

            <div class="box-body table-holder">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Employee Name</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Employer Name</th>
                  <th>Job Title</th>
                  <!-- <th>Benefit</th> -->
                  <!-- <th>Required Skills</th>
                  <th>Experiance</th> -->
                  <th>Applied Date</th>
                  <th>Counrty</th>
                  <th>City</th>
                  <th>Status</th>

                  <!-- <th>Employer Email</th>
                  <th>Employer Contact</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; @endphp
                @forelse($applied_jobs as $applied_job)
                <tr>
                  <td>{{$a}}</td>
                  <td>{{@$applied_job->employee->employeeUser->first_name}} {{@$applied_job->employee->employeeUser->last_name}}</td>
                  <td>{{@$applied_job->employee->employeeUser->country->name}}</td>
                  <td>{{@$applied_job->employee->employeeUser->city->name}}</td>
                  <td>{{@$applied_job->job->employer->employerUser->first_name}} {{@$applied_job->job->employer->employerUser->last_name}}</td>
                  <td>{{@$applied_job->job->job_title}}</td>
                  <td>{{\Carbon\Carbon::parse(@$applied_job->created_at)->format('d/m/Y')}}</td>
                  <td>{{@$applied_job->job->employer->employerUser->country->name}}</td>
                  <td>{{@$applied_job->job->employer->employerUser->city->name}}</td>
                  <td>{{$applied_job->applicationStatus->phase_name}}</td>
                  <td>
                    @if($job_type =='jobs')
                    <a href="{{route('admin.employee.applied-job.show', ['id'=>$applied_job->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                    @elseif($job_type =='international')
                    <a href="{{route('admin.employee.applied-internationaljob.show', ['id'=>$applied_job->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                    @endif
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>
              <div>{{ $applied_jobs->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
