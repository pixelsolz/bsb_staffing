@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Country
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.country-manage.index') }}"><i class="fa fa-dashboard"></i> Country List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Country</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Country Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.language-manage.update',$language->id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('language_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Language Name<span class="required_field">*</span></label>
                           <input type="text" name="language_name" class="form-control" value="{{@$language->language_name}}">
                           @if($errors->has('v'))
                           <span class="error">{{$errors->first('language_name')}}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection