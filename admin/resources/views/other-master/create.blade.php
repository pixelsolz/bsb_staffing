@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Other Master
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.other-master-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create Other Master</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Other Master Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.other-master-manage.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{old('name')}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('entity'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="entity">Entity<span class="required_field">*</span></label>
                           <input type="text" name="entity" class="form-control" value="{{old('entity')}}">
                           @if($errors->has('entity'))
                           <span class="error">{{$errors->first('entity')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('orders'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="orders">Orders<span class="required_field">*</span></label>
                           <input type="text" name="orders" class="form-control" value="{{old('orders')}}">
                           @if($errors->has('orders'))
                           <span class="error">{{$errors->first('orders')}}</span>
                           @endif
                        </div>
                     </div>
                     <input type="hidden" name="status" value=1>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection