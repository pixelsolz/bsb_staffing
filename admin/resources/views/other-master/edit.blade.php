@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Other Master
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.other-master-manage.index') }}"><i class="fa fa-dashboard"></i> Other Master List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Other Master</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Other Master Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.other-master-manage.update',$other_mst->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{@$other_mst->name}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('entity'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="entity">Entity<span class="required_field">*</span></label>
                           <input type="text" name="entity" class="form-control" value="{{@$other_mst->entity}}">
                           @if($errors->has('entity'))
                           <span class="error">{{$errors->first('entity')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('orders'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="orders">Orders<span class="required_field">*</span></label>
                           <input type="text" name="orders" class="form-control" value="{{@$other_mst->orders}}">
                           @if($errors->has('orders'))
                           <span class="error">{{$errors->first('orders')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="status">Status</label>
                           <select name="status" class="form-control">
                              <option value="">select status</option>
                              <option value=1 @if($other_mst->status ==1) selected @endif>Active</option>
                              <option value=2 @if($other_mst->status ==2) selected @endif>Inactive</option>
                           </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection