@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment Configration Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
             
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Payment For</th>
                  <th>Check Box</th>
                  <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  @php $url = url('payment-config/status-change'); 
                      $id= @$payment_config->_id;
                  @endphp
                  <td>Special Service ( Premium Job Post)</td>
                  <td><input type="checkbox" value="premium_job_post" @if(@$payment_config->premium_job_post ==1) checked @endif onchange="paymentConfigChange(event,'{{$id}}','{{$url}}')" ></td>
                  <td>Premium Job Post</td>
                </tr>
                <tr>
                  <td>Special Service (International Walk In Interview)</td>
                  <td><input type="checkbox" value="international_walk_in_interview" @if(@$payment_config->international_walk_in_interview ==1) checked @endif onchange="paymentConfigChange(event,'{{$id}}','{{$url}}')"></td>
                  <td>International Walk In Interview</td>
                </tr>
                <tr>
                  <td>Special Service (CV Download/View)</td>
                  <td><input type="checkbox" value="cv_download_view" @if(@$payment_config->cv_download_view ==1) checked @endif onchange="paymentConfigChange(event,'{{$id}}','{{$url}}')"></td>
                  <td>CV Download/View</td>
                </tr>
                <tr>
                  <td>Special Service (Mass Mail Services)</td>
                  <td><input type="checkbox" value="mass_mail_service" @if(@$payment_config->mass_mail_service ==1) checked @endif onchange="paymentConfigChange(event,'{{$id}}','{{$url}}')"></td>
                  <td>Mass Mail Services</td>
                </tr>  
                <tr>
                  <td>Employee</td>
                  <td><input type="checkbox" value="employee" @if(@$payment_config->employee ==1) checked @endif onchange="paymentConfigChange(event,'{{$id}}','{{$url}}')"></td>
                  <td>Employee Premium User</td>
                </tr>
                <tr>
                  <td>Employer</td>
                  <td><input type="checkbox" value="employer" @if(@$payment_config->employer ==1) checked @endif onchange="paymentConfigChange(event,'{{$id}}','{{$url}}')"></td>
                  <td>Employer Premium User</td>
                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
