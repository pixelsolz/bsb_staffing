@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Service Package
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create Service Package</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Service Package Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('service-package')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('service_type'))has-error @endif">
                        <div class="col-xs-12">
                        <label for="service_type">Service Type<span class="required_field">*</span></label>
                        <select class="form-control" name="service_type">
                           <option value="">Select Service Type</option>
                           <option value="premium_job_post">Premium Job Post</option>
                           <option value="cv_download_view">CV Download/View</option>
                           <option value="mass_mail_service">Mass Mail Services</option>
                           <option value="international_walk_in_interview">Post International Walk In Interview</option>
                        </select>
                        @if($errors->has('service_type'))
                           <span class="error">{{$errors->first('service_type')}}</span>
                        @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('service_name'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="service_name">Service Name<span class="required_field">*</span></label>
                           <input type="text" name="service_name" class="form-control" value="{{old('service_name')}}">
                           @if($errors->has('service_name'))
                           <span class="error">{{$errors->first('service_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-12">
                           <label for="service_desc">Service Description</label>
                           <textarea name="service_desc" class="form-control">{{old('service_desc')}}</textarea>
                           @if($errors->has('service_desc'))
                           <span class="error">{{$errors->first('service_desc')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('quantity'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="quantity">Quantity<span class="required_field">*</span></label>
                           <input type="number" min="1" name="quantity" class="form-control" value="{{old('quantity')}}" placeholder="Number of cv/job/mail">
                           @if($errors->has('quantity'))
                           <span class="error">{{$errors->first('quantity')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('fees'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="fees">Fees<span class="required_field">*</span></label>
                           <input type="number" min="1" name="fees" class="form-control" value="{{old('fees')}}" placeholder="Price in USD">
                           @if($errors->has('fees'))
                           <span class="error">{{$errors->first('fees')}}</span>
                           @endif
                        </div>
                     </div>
                      <div class="form-group row @if($errors->has('service_life'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="service_life">Service Life<span class="required_field">*</span></label>
                           <select class="form-control" name="service_life">
                              <option value="">Select Months</option>
                              <option value="1">1 Months</option>
                              <option value="2">2 Months</option>
                              <option value="3">3 Months</option>
                              <option value="4">4 Months</option>
                              <option value="5">5 Months</option>
                              <option value="6">6 Months</option>
                              <option value="7">7 Months</option>
                              <option value="8">8 Months</option>
                              <option value="9">9 Months</option>
                              <option value="10">10 Months</option>
                              <option value="11">11 Months</option>
                              <option value="12">12 Months</option>
                           </select>
                           @if($errors->has('service_life'))
                           <span class="error">{{$errors->first('service_life')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-12">
                           <label for="phone_no">Special Service</label>
                           <input type="radio" name="special_service" value="1">Yes
                           <input type="radio" name="special_service" value="0" checked>No
                        </div>
                     </div>


                     <div class="form-group row">
                        <div class="col-xs-12">
                        <label for="needs_broadcast">Needs Broadcast</label>
                           <input type="radio" name="needs_broadcast" value="1" checked>Yes
                           <input type="radio" name="needs_broadcast" value="0">No
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('entity_id'))has-error @endif">
                        <div class="col-xs-12">
                        <label for="entity_id">Applicable To<span class="required_field">*</span></label>
                        <select class="form-control" name="entity_id">
                           <option value="1">Select Applicable To</option>
                           @foreach($allRole as $role)
                           <option value="{{$role->_id}}">{{$role->name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('entity_id'))
                           <span class="error">{{$errors->first('entity_id')}}</span>
                        @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('order_of_results'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="order_of_results">Order Of Results<span class="required_field">*</span></label>
                           <input type="number" min="1" name="order_of_results" class="form-control" value="{{old('order_of_results')}}">
                           @if($errors->has('order_of_results'))
                           <span class="error">{{$errors->first('order_of_results')}}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection