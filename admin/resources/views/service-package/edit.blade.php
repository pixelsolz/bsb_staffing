@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Service Package
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.service-package.index') }}"><i class="fa fa-dashboard"></i> Service Package List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Service Package</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Service Package Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.service-package.update',$service_package->id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('service_type'))has-error @endif">
                        <div class="col-xs-12">
                        <label for="service_type">Service Type<span class="required_field">*</span></label>
                        <select class="form-control" name="service_type">
                           <option value="">Select Service Type</option>
                           <option value="premium_job_post" @if(@$service_package->service_type == 'premium_job_post') {{'selected'}} @endif>Premium Job Post</option>
                           <option value="cv_download_view" @if(@$service_package->service_type == 'cv_download_view') {{'selected'}} @endif>CV Download/View</option>
                           <option value="mass_mail_service" @if(@$service_package->service_type == 'mass_mail_service') {{'selected'}} @endif>Mass Mail Services</option>
                           <option value="international_walk_in_interview" @if(@$service_package->service_type == 'international_walk_in_interview') {{'selected'}} @endif>Post International Walk In Interview</option>
                        </select>
                        @if($errors->has('service_type'))
                           <span class="error">{{$errors->first('service_type')}}</span>
                        @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('service_name'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="service_name">Service Name<span class="required_field">*</span></label>
                           <input type="text" name="service_name" class="form-control" value="{{$service_package->service_name}}">
                           @if($errors->has('service_name'))
                           <span class="error">{{$errors->first('service_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-12">
                           <label for="service_desc">Service Description</label>
                           <textarea name="service_desc" class="form-control">{{$service_package->service_desc}}</textarea>
                           @if($errors->has('service_desc'))
                           <span class="error">{{$errors->first('service_desc')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('quantity'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="quantity">Quantity<span class="required_field">*</span></label>
                           <input type="number" min="1" name="quantity" class="form-control" value="{{$service_package->quantity}}" placeholder="Number of cv/job/mail">
                           @if($errors->has('quantity'))
                           <span class="error">{{$errors->first('quantity')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('fees'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="fees">Fees<span class="required_field">*</span></label>
                           <input type="number" min="1" name="fees" class="form-control" value="{{$service_package->fees}}" placeholder="Price in USD">
                           @if($errors->has('fees'))
                           <span class="error">{{$errors->first('fees')}}</span>
                           @endif
                        </div>
                     </div>
                      <div class="form-group row @if($errors->has('service_life'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="service_life">Service Life<span class="required_field">*</span></label>
                           <select class="form-control" name="service_life">
                              <option value="">Select Months</option>
                              <option value="1" @if(@$service_package->service_life == 1) {{'selected'}} @endif>1 Months</option>
                              <option value="2" @if(@$service_package->service_life == 2) {{'selected'}} @endif>2 Months</option>
                              <option value="3" @if(@$service_package->service_life == 3) {{'selected'}} @endif>3 Months</option>
                              <option value="4" @if(@$service_package->service_life == 4) {{'selected'}} @endif>4 Months</option>
                              <option value="5" @if(@$service_package->service_life == 5) {{'selected'}} @endif>5 Months</option>
                              <option value="6" @if(@$service_package->service_life == 6) {{'selected'}} @endif>6 Months</option>
                              <option value="7" @if(@$service_package->service_life == 7) {{'selected'}} @endif>7 Months</option>
                              <option value="8" @if(@$service_package->service_life == 8) {{'selected'}} @endif>8 Months</option>
                              <option value="9" @if(@$service_package->service_life == 9) {{'selected'}} @endif>9 Months</option>
                              <option value="10" @if(@$service_package->service_life == 10) {{'selected'}} @endif>10 Months</option>
                              <option value="11" @if(@$service_package->service_life == 11) {{'selected'}} @endif>11 Months</option>
                              <option value="12" @if(@$service_package->service_life == 12) {{'selected'}} @endif>12 Months</option>
                           </select>
                           @if($errors->has('service_life'))
                           <span class="error">{{$errors->first('service_life')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xs-12">
                           <label for="phone_no">Special Service</label>
                           <input type="radio" name="special_service" value="1" @if(@$service_package->special_service == 1) {{'checked'}} @endif >Yes
                           <input type="radio" name="special_service" value="0" @if(@$service_package->special_service == 0) {{'checked'}} @endif>No
                        </div>
                     </div>
                     

                     <div class="form-group row">
                        <div class="col-xs-12">
                        <label for="needs_broadcast">Needs Broadcast</label>
                           <input type="radio" name="needs_broadcast" value="1" @if(@$service_package->needs_broadcast == 1) {{'checked'}} @endif>Yes
                           <input type="radio" name="needs_broadcast" value="0" @if(@$service_package->needs_broadcast == 0) {{'checked'}} @endif>No
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('entity_id'))has-error @endif">
                        <div class="col-xs-12">
                        <label for="entity_id">Applicable To<span class="required_field">*</span></label>
                        <select class="form-control" name="entity_id">
                           <option value="">Select Applicable To</option>
                           @foreach($allRole as $role)
                           <option value="{{$role->id}}" @if(@$service_package->entity_id == $role->id) {{'selected'}} @endif>{{$role->name}}</option>
                           @endforeach
                        </select>
                        @if($errors->has('entity_id'))
                           <span class="error">{{$errors->first('entity_id')}}</span>
                        @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('order_of_results'))has-error @endif">
                        <div class="col-xs-12">
                           <label for="order_of_results">Order Of Results<span class="required_field">*</span></label>
                           <input type="number" min="1" name="order_of_results" class="form-control" value="{{@$service_package->order_of_results}}">
                           @if($errors->has('order_of_results'))
                           <span class="error">{{$errors->first('order_of_results')}}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection