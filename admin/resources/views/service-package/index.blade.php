@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Service Package Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <a href="{{route('admin.service-package.create')}}" class="btn btn-primary">Creat New</a>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <!-- <form action="{{route('admin.country-manage.index')}}" method="get">
                <div class="row">
                  <div class="col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-offset-0 col-xs-12">
                    <div class="form-group">
                      <input type="text" name="q" class="form-control" placeholder="Search with Country Name or Country Code..." value="{{Request::input('q')}}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="searchOuter">
                      <input type="submit" name="" value="search">
                    </div>
                  </div>
                </div>
              </form> -->
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Service Type</th>
                  <th>Service Name</th>
                  <th>Quantity</th>
                  <th>Fees</th>
                  <th>Service Life</th>
                  <th>Applicable To</th>
                  <th>Order of Results</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($servicesAll as $service)
                  <tr>
                    <td>{{$a +($page*25)}}</td>
                    <td>
                      @if($service->service_type == 'premium_job_post')
                        {{'Premium Job Post'}}
                      @elseif($service->service_type == 'cv_download_view')
                        {{'CV Download/View'}}
                      @elseif($service->service_type == 'mass_mail_service')
                        {{'Mass Mail Services'}}
                      @elseif($service->service_type == 'international_walk_in_interview')
                        {{'International Walk In Interview'}}
                      @else($service->service_type == '')

                      @endif

                    </td>
                    <td>{{$service->service_name}}</td>
                    <td>{{$service->quantity}}</td>
                    <td>{{$service->fees}} USD</td>
                    <td>{{$service->service_life}} Months</td>
                    <td>{{@$service->role_name}}</td>
                    <td>{{$service->order_of_results}}</td>
                    <td>{{$service->status == 1?'Active':'Inactive'}}</td>
                    <td>
                     <a href="{{route('admin.service-package.edit', ['id'=>$service->id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                  </tr>
                  @php $a++; @endphp
                @empty
                  <tr>
                    <td colspan="6" style="text-align: center;">No data available</td>
                  </tr>
                @endforelse
                </tbody>

              </table>
              </div>
              <div>{{ $servicesAll->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
