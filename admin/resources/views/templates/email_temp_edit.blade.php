@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Email Template
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('email-template/list') }}"><i class="fa fa-dashboard"></i> Template List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit Email Template</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Template Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('email-template/update', $template->_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('template_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="template_name">Template Name<span class="required_field">*</span></label>
                           <input type="text" name="template_name" class="form-control" value="{{$template->template_name ? $template->template_name : $template->template_name}}" readonly="true">
                           @if($errors->has('template_name'))
                           <span class="error">{{$errors->first('template_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('cv_received_email'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="cv_received_email">CV Received Email Id<span class="required_field"></span></label>
                           <input type="text" name="cv_received_email" class="form-control" value="{{@$template->employerJob->received_email }}" readonly="true">
                           @if($errors->has('cv_received_email'))
                           <span class="error">{{$errors->first('cv_received_email')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('subject'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="subject">Subject Line<span class="required_field">*</span></label>
                           <input type="text" name="subject" class="form-control" value="{{$template->subject}}" readonly="true">
                           @if($errors->has('subject'))
                           <span class="error">{{$errors->first('subject')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('about_job'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="about_job">About This Job<span class="required_field">*</span></label>
                           <textarea class="form-control" name="about_job" readonly="true">{{$template->about_job}}</textarea>
                           @if($errors->has('about_job'))
                           <span class="error">{{$errors->first('about_job')}}</span>
                           @endif
                        </div>
                     </div>

                      <div class="form-group row @if($errors->has('skill'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="skill">Skills<span class="required_field">*</span></label>
                           <input type="text" name="skill" class="form-control" value="{{implode(',',$template->employerJob->required_skill)}}" readonly="true">
                           @if($errors->has('skill'))
                           <span class="error">{{$errors->first('skill')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('benefit'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="benefit">Benefits<span class="required_field">*</span></label>
                           <textarea class="form-control" name="benefit" readonly="true">{{$template->employerJob->benefits}}</textarea>
                           @if($errors->has('benefit'))
                           <span class="error">{{$errors->first('benefit')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('primary_responsibility'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="primary_responsibility">Primary Responsive<span class="required_field">*</span></label>
                           <textarea class="form-control" name="primary_responsibility" readonly="true">{{$template->employerJob->primary_responsibility}}</textarea>
                           @if($errors->has('primary_responsibility'))
                           <span class="error">{{$errors->first('primary_responsibility')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('special_note'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="special_note">Special Note<span class="required_field">*</span></label>
                           <textarea class="form-control" name="special_note" readonly="true">{{$template->employerJob->special_notes}}</textarea>
                           @if($errors->has('special_note'))
                           <span class="error">{{$errors->first('special_note')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('country_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="country_id">Choose Country<span class="required_field">*</span></label>
                             <select class="form-control" name="country_id" disabled="true">
                              <option value="">select country</option>
                             @if($allCountry)
                                 @foreach($allCountry as $country)
                                    <option value="{{$country->_id}}" @if($template->employerJob->country_id == $country->_id) selected @endif >{{$country->name}}</option>
                                 @endforeach
                              @endif
                           </select>
                           @if($errors->has('country_id'))
                           <span class="error">{{$errors->first('country_id')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('city_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="city_id">Choose City<span class="required_field">*</span></label>
                              <select class="form-control" name="citi_id" disabled="true">
                              <option value="">select city</option>
                              @if($allcity)
                                 @foreach($allcity as $city)
                                    <option value="{{$city->_id}}" @if($template->employerJob->city_id == $city->_id) selected @endif >{{$city->name}}</option>
                                 @endforeach
                              @endif
                              </select>
                           @if($errors->has('city_id'))
                           <span class="error">{{$errors->first('city_id')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('min_exp'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="min_exp">Minimum Experiance<span class="required_field">*</span></label>
                           <input type="text" name="min_exp" class="form-control" value="{{$template->employerJob->minExperiance->name.' '.'Yrs'}}" readonly="true">
                           @if($errors->has('min_exp'))
                           <span class="error">{{$errors->first('min_exp')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('max_exp'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="max_exp">Maximum Experiance<span class="required_field">*</span></label>
                           <input type="text" name="max_exp" class="form-control" value="{{$template->employerJob->maxExperiance->name.' '.'Yrs'}}" readonly="true">
                           @if($errors->has('max_exp'))
                           <span class="error">{{$errors->first('max_exp')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('language_pref'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="language_pref">Language Preference<span class="required_field">*</span></label>
                           <input type="text" name="language_pref" class="form-control" value="{{ implode(',',$template->employerJob->language->pluck('language_name')->toArray())}}" readonly="true">
                           @if($errors->has('language_pref'))
                           <span class="error">{{$errors->first('language_pref')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('salary_type'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="salary_type">Salary<span class="required_field">*</span></label>
                           <select class="form-control" name="salary_type" disabled="true">
                              <option value="1" @if($template->salary_type==1) selected @endif>Hourly</option>
                              <option value="2" @if($template->salary_type==2) selected @endif>Weekly</option>
                              <option value="3" @if($template->salary_type==3) selected @endif>Monthly</option>
                           </select>
                           @if($errors->has('salary_type'))
                           <span class="error">{{$errors->first('salary_type')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('min_salary'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="min_salary">Min Salary<span class="required_field">*</span></label>
                           <input type="text" name="min_salary" class="form-control" value="{{$template->employerJob->min_monthly_salary}}" readonly="true">
                           @if($errors->has('min_salary'))
                           <span class="error">{{$errors->first('min_salary')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('max_salary'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="max_salary">Max Salary<span class="required_field">*</span></label>
                           <input type="text" name="max_salary" class="form-control" value="{{$template->employerJob->max_monthly_salary}}" readonly="true">
                           @if($errors->has('max_salary'))
                           <span class="error">{{$errors->first('max_salary')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('reviewed_flag'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="reviewed_flag">Review Status<span class="required_field">*</span></label>
                           <select class="form-control" name="reviewed_flag">
                              <option value="">select</option>
                              <option value="0" @if($template->reviewed_flag==0 || $template->reviewed_flag=='') selected @endif>Pending</option>
                              <option value="1" @if($template->reviewed_flag==1) selected @endif>Approved</option>
                              <option value="2" @if($template->reviewed_flag==2) selected @endif>Disapproved</option>
                           </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update Template</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection