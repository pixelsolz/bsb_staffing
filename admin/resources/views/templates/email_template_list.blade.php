@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Email Template List

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              {{--<a href="{{route('admin.user-manage.create-employee')}}" class="btn btn-primary">Creat New</a><br/><br/>--}}
              <div class="clearfix"></div>
              <div class="form-group row">
                <form action="{{url('email-template/list')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-4">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by name or email" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                  </div>
                  <div class="col-xs-4">
                  <button type="submit" class="btn btn-info">Search</button>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Template Name</th>
                  <th>CV Received Email</th>
                  <th>subject</th>
                  <th>Job Title</th>
                  <th>Skills</th>
                  <th>Benefit</th>
                  <th>Created By</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($templates as $template)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{$template->template_name }}</td>
                  <td>{{$template->employerJob->received_email}}</td>
                  <td>{{$template->subject}}</td>
                  <td>{{$template->employerJob->job_title}}</td>
                  <td>{{implode(',',$template->employerJob->required_skill)}}</td>
                  <td>{{$template->employerJob->benefits}}</td>
                  <td>{{$template->createdUser->employerUser->first_name}}</td>
                  <td>{{$template->admin_reviewd}}</td>
                  <td>
                   <a href="{{url('email-template/edit', ['id'=>$template->id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="9" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table></div>
              <div>{{ $templates->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
