@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit User Entity
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.userentity-manage.index') }}"><i class="fa fa-dashboard"></i> User Entity List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit User Entity</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User Entity Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.userentity-manage.update',$user_entity->id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('entity_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="entity_name">Entity Name<span class="required_field">*</span></label>
                           <input type="text" name="entity_name" class="form-control" value="{{@$user_entity->entity_name}}">
                           @if($errors->has('entity_name'))
                           <span class="error">{{$errors->first('entity_name')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection