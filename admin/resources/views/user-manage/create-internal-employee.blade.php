@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Internal Employee
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.list.internal-employee') }}"><i class="fa fa-dashboard"></i> Users List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create Internal employee</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"> Internal employee Create Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.user-manage.internal-employee.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('f_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="f_name">First Name<span class="required_field">*</span></label>
                           <input type="text" name="f_name" class="form-control" value="{{old('f_name')}}">
                           @if($errors->has('f_name'))
                           <span class="error">{{$errors->first('f_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('middle_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="middle_name">Middle Name<span class="required_field"></span></label>
                           <input type="text" name="middle_name" class="form-control" value="{{old('middle_name')}}">
                           @if($errors->has('middle_name'))
                           <span class="error">{{$errors->first('middle_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('l_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="l_name">Last Name<span class="required_field">*</span></label>
                           <input type="text" name="l_name" class="form-control" value="{{old('l_name')}}">
                           @if($errors->has('l_name'))
                           <span class="error">{{$errors->first('l_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('user_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="user_name">User Name<span class="required_field">*</span></label>
                           <input type="text" name="user_name" class="form-control" value="{{old('user_name')}}">
                           @if($errors->has('user_name'))
                           <span class="error">{{$errors->first('user_name')}}</span>
                           @endif
                        </div>
                     </div>
                      <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="user_name">Email<span class="required_field">*</span></label>
                           <input type="email" name="email" class="form-control" value="{{old('email')}}">
                           @if($errors->has('email'))
                           <span class="error">{{$errors->first('email')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('phone_no'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="phone_no">Mobile<span class="required_field">*</span></label>
                           <input type="text" name="phone_no" class="form-control" value="{{old('phone_no')}}">
                           @if($errors->has('phone_no'))
                           <span class="error">{{$errors->first('phone_no')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('user_role'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="user_role">Working Segment<span class="required_field">*</span></label>
                        <select class="form-control" name="user_role">
                           <option value="">Select Working Segment</option>
                           <option value="employee">Employee</option>
                           <option value="employer">Employer</option>
                           <option value="college">College/ University/ Institution</option>
                           <option value="agency">Recruitment Agency</option>
                           <option value="franchise">Franchise</option>
                           <option value="trainer">Trainer</option>
                           <!-- <option value="internal_employee">Internal Employee</option> -->
                        </select>
                        @if($errors->has('user_role'))
                           <span class="error">{{$errors->first('user_role')}}</span>
                        @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('office_name'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="office_name">Office Name</label>
                           <input type="text" name="office_name" class="form-control" value="{{old('office_name')}}">
                           @if($errors->has('office_name'))
                           <span class="error">{{$errors->first('office_name')}}</span>
                           @endif
                        </div>
                     </div>


                     <div class="form-group row @if($errors->has('country_id'))has-error @endif">
                      <div class="col-xs-9">
                      <label for="country_id">Country</label>
                      @php $getCityUrl = url('city-ajax'); @endphp
                        <select class="form-control" name="country_id" onchange="getCity(this.value,'{{$getCityUrl}}')">
                           <option value="">select country</option>
                           @if($allCountry)
                              @foreach($allCountry as $country)
                                 <option value="{{$country->id}}">{{$country->name}}</option>
                              @endforeach
                           @endif
                        </select>
                        @if($errors->has('country_id'))
                           <span class="error">{{$errors->first('country_id')}}</span>
                        @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('citi_id'))has-error @endif">
                        <div class="col-xs-9">
                        <label for="citi_id">City</label>
                        <select class="form-control set_city" name="citi_id">
                           <option value="">select</option>
                        </select>
                        @if($errors->has('citi_id'))
                           <span class="error">{{$errors->first('citi_id')}}</span>
                        @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('address'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="address">Address</label>
                           <textarea name="address" class="form-control">{{old('address')}}</textarea>
                           @if($errors->has('address'))
                           <span class="error">{{$errors->first('address')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row">
                        <div class="col-xs-9">
                        <label for="profile_image">Govt. Id</label>
                        <div class="field_wrapper">
                           <div>
                              <input type="file" name="govt_id[]" class="form-control"><a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-circle fa-3x"></i></a>
                           </div>
                        </div>
                        </div>

                     </div>

                     <div class="form-group row @if($errors->has('date_of_joining'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="date_of_joining">Date of Joining<span class="required_field">*</span></label>
                           <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                           <input type="text" name="date_of_joining" class="form-control pull-right datepicker" value="{{old('date_of_joining')}}">
                           </div>
                           @if($errors->has('date_of_joining'))
                           <span class="error">{{$errors->first('date_of_joining')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('designation'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="designation">Designation</label>
                           <input type="text" name="designation" class="form-control" value="{{old('designation')}}">
                           @if($errors->has('designation'))
                           <span class="error">{{$errors->first('designation')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('password'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="password">Password<span class="required_field">*</span></label>
                           <input type="password" name="password" class="form-control" value="">
                           @if($errors->has('password'))
                           <span class="error">{{$errors->first('password')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('password_confirmation'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="password_confirmation">Confirm Password<span class="required_field">*</span></label>
                           <input type="password" name="password_confirmation" class="form-control" value="">
                           @if($errors->has('password_confirmation'))
                           <span class="error">{{$errors->first('password_confirmation')}}</span>
                           @endif
                        </div>
                     </div>

                     @foreach($allRole as $role)
                        @if($role->slug == 'internal_employee')
                        <input type="hidden" class="minimal" name="role_id" value="{{$role->_id}}">
                        @endif
                     @endforeach
                      <input type="hidden" class="" name="status" value=1>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection
