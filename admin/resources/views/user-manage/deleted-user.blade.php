@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Deleted User Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              {{--<a href="{{route('admin.user-manage.create')}}" class="btn btn-primary">Creat New</a><br/><br/>--}}
              <div class="clearfix"></div>
              {{--<div class="form-group row">
                <form action="{{route('admin.user-manage.employee')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-2">
                    @php $getCityUrl = url('city-ajax'); @endphp
                    <select class="form-control" name="search_by_country" onchange="getCity(this.value,'{{$getCityUrl}}')">
                      <option value="">Select Country</option>
                     @foreach($countries as $country)
                      <option value="{{$country->id}}" @if(!empty($request->search_by_country) && $request->search_by_country == $country->id) selected @endif >{{$country->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-xs-2">
                    <select class="form-control set_city" name="search_by_city">
                      <option value="">Select City</option>
                    </select>
                  </div>
                  <div class="col-xs-4">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by name email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                  </div>
                  <div class="col-xs-4">
                  <button type="submit" class="btn btn-info">Search</button>
                  </div>
                </form>
              </div>--}}
            </div>
            <!-- /.box-header -->

            <div class="box-body">
               <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>

                  <th>Email</th>
                  <th>Phone No</th>


                  <th>Role</th>
                  <th>Created Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($deleted_user as $user)
                <tr>
                  <td>{{$a +($page*25)}}</td>

                  <td>{{@$user->email}}</td>
                  <td>{{@$user->phone_no}}</td>
                  <td>{{@$user->roles()->first()->name}}</td>
                  <td>{{@$user->created_at}}</td>
                  <td>{{$user->status ==1? 'Active':'Inactive'}}</td>
                  <td>
                   <a href="{{route('admin.user-manage.restore', ['id'=>$user->id])}}" class="" alt="Restore"><i class="fa fa-undo"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                @php
                  $deleteroute = route('admin.user-manage.delete', ['id'=>$user->id]);
                @endphp
                   {{--<a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove"></a>--}}
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table></div>
              <div>{{ $deleted_user->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
