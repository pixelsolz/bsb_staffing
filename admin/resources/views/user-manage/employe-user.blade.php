@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>
              <form action="{{route('admin.user-manage.employee')}}" method="get">
                <div class="row">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_joined_date" class="form-control pull-right datepicker" placeholder="Start Created Date" value="{{!empty($request->from_joined_date)?$request->from_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_joined_date" class="form-control pull-right datepicker" placeholder="End Created Date" value="{{!empty($request->to_joined_date)?$request->to_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_login_date" class="form-control pull-right datepicker" placeholder="Start Last Active Date" value="{{!empty($request->from_login_date)?$request->from_login_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_login_date" class="form-control pull-right datepicker" placeholder="End Last Active Date" value="{{!empty($request->to_login_date)?$request->to_login_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-xs-2">
                    <div class="form-group">
                      @php $getCityUrl = url('city-ajax'); @endphp
                      <select class="form-control" name="search_by_country" onchange="getCity(this.value,'{{$getCityUrl}}')">
                        <option value="">Select Country</option>
                       @foreach($countries as $country)
                        <option value="{{$country->id}}" @if(!empty($request->search_by_country) && $request->search_by_country == $country->id) selected @endif >{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control set_city" name="search_by_city">
                      <option value="">Select City</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="status">
                      <option value="">Select Status</option>
                      <option value="1" @if(!empty($request->status) && $request->status == 1) selected @endif>Active</option>
                      <option value="0" @if(!empty($request->status) && $request->status == 0) selected @endif>Inactive</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="industry_id">
                      <option value="">Select Industry</option>
                      @if($industries)
                        @foreach($industries as $industry)
                        <option value="{{$industry->_id}}" @if(!empty($request->industry_id) && $request->industry_id == $industry->_id) selected @endif >{{$industry->name}}</option>
                        @endforeach
                      @endif
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="search_by_salary_type">
                        <option value="">Select Salary Type</option>
                        <option value="hourly" @if(!empty($request->search_by_salary_type) && $request->search_by_salary_type == 'hourly') selected @endif >Hourly</option>
                        <option value="weekly" @if(!empty($request->search_by_salary_type) && $request->search_by_salary_type == 'weekly') selected @endif >Weekly</option>
                        <option value="monthly" @if(!empty($request->search_by_salary_type) && $request->search_by_salary_type == 'monthly') selected @endif >Monthly</option>

                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="search_by_currency">
                        <option value="">Select Currency</option>
                       @foreach($countries as $country)
                        <option value="{{$country->id}}" @if(!empty($request->search_by_currency) && $request->search_by_currency == $country->id) selected @endif >{{$country->currency_code}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                   </div>
                  <div class="row ">
                     <div class="col-xs-2">
                    <div class="form-group">
                      <input type="text" class="form-control" name="salary_from" placeholder="Min Salary" value="{{!empty($request->salary_from)?$request->salary_from: '' }}">
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <input type="text" class="form-control" name="salary_to" placeholder="Max Salary" value="{{!empty($request->salary_to)?$request->salary_to: '' }}">
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="gender">
                      <option value="">Select Gender</option>
                      <option value="1" @if(!empty($request->gender) && $request->gender == 1) selected @endif>Male</option>
                      <option value="2" @if(!empty($request->gender) && $request->gender == 2) selected @endif>Female</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="employee_type">
                      <option value="">Select Employee Type</option>
                      <option value='1' @if(!empty($request->employee_type) && $request->employee_type == '1') selected @endif>Full Completed Employee</option>
                      <option value='2' @if(!empty($request->employee_type) && $request->employee_type == '2') selected @endif>Not Full Complete Employee</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by ID, name, email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>
                    @if($request->institute_code)
                    <input type="hidden" name="institute_code" value="{{!empty($request->institute_code)?$request->institute_code: '' }}">
                    @endif
                    <div class="col-xs-12">
                    <button type="submit" class="btn btn-info">Search</button>
                    <a href="{{route('admin.user-manage.employee')}}" class="reset">Reset</a>
                    </div>
                  </div>
              </form>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone No</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Gender</th>
                  <th>Industry</th>
                  <th>Resume</th>
                  <th>Status</th>
                  <th>Applied Jobs</th>
                  <th>Saved Jobs</th>
                  <th>Last Active Date</th>
                  <th>Login Status</th>
                  <th>Created Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @php $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($users as $user)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{ @$user->employeeUser->first_name }} {{ @$user->employeeUser->last_name }}</td>
                  <td>{{@$user->email}}</td>
                  <td>{{@$user->phone_no}}</td>
                  <td>{{@$user->employeeUser->country->name}}</td>
                  <td>{{@$user->employeeUser->city->name}}</td>
                  <td>{{@$user->employeeUser->gender_text}}</td>
                  @php
                    $industry = [];
                    if(@$user->employeePrefTxn->industries){
                      $industry = @$user->employeePrefTxn->industries->pluck('name')->toArray();
                    }
                  @endphp
                  <td>{{@$user->employeeUser->industry->name}}</td>
                  <td>@if(@$user->employeeCvTxn->cv_path)<a href="{{Storage::disk('s3')->url('/upload_files/employee-resumes/' . @$user->employeeCvTxn->cv_path)}}" download target="_blank">View Resume</a>@endif</td>
                  <td>{{$user->status ==1? 'Active':'Inactive'}}</td>
                  <td>@if($user->employeeAppliedJob()->count())<a href="{{url('employee/applied-job/?id='.@$user->id)}}" title="View detail">{{$user->employeeAppliedJob()->count()}}</a>@else {{$user->employeeAppliedJob()->count()}} @endif</td>
                  <td>@if($user->employeeSavedJob()->whereNull('employee_application_id')->count())<a href="{{url('employee/saved-job',@$user->id)}}" title="View detail">{{$user->employeeSavedJob()->whereNull('employee_application_id')->count()}}</a>@else{{$user->employeeSavedJob()->whereNull('employee_application_id')->count()}}@endif</td>

                  <td>{{@$user->last_login ? @$user->last_login : @$user->created_at}}</td>
                  <td>
                    @if(@$user->getLoginStatus($user) == 3)
                    <span><i class="fa fa-circle" style="font-size:18px;color:red"></i></span>
                    @elseif(@$user->getLoginStatus($user) == 2)
                     <span><i class="fa fa-circle" style="font-size:18px;color:yellow"></i></span>
                    @else
                     <span><i class="fa fa-circle" style="font-size:18px;color:green"></i></span>
                    @endif
                  </td>
                  <td>{{@$user->created_at}}</td>
                  <td>
                     <a href="{{route('admin.user-manage.show_employee', ['id'=>$user->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                   <a href="{{route('admin.user-manage.edit-employee', ['id'=>$user->id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                @php
                  $deleteroute = route('admin.user-manage.delete', ['id'=>$user->id]);
                @endphp
                   <a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove"></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table></div>
              <div>{{ $users->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
