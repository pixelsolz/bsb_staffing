@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         View User
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> View User</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User View</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <div class="employee-dtls-wrp employee-dtls-wrp2">
                  <h4 class="emdtls-hding">Profile Details ( User Id : BSBEMY{{@$user->bsb_unique_no}})</h4>
                  <div class="emplyr-img-holder">
                     <img src="{{Storage::disk('s3')->url('/upload_files/profile_picture/' . @$user->employeeUser->profile_picture)}}">
                  </div>
                  <div class="employee-dtls2-wrap">
                     <div class="employee-dtls2-wrapper">
                        <div class="employee-dtls2-holder">
                           <span>First Name :</span>
                           <span>{{@$user->employeeUser->first_name}}</span>
                        </div>
                     </div>
                     <div class="employee-dtls2-wrapper employee-dtls2-wrapper2">
                        <div class="employee-dtls2-holder">
                           <span>Last Name :</span>
                           <span>{{@$user->employeeUser->last_name}}</span>
                        </div>
                     </div>
                     <div class="employee-dtls2-wrapper">
                        <div class="employee-dtls2-holder">
                           <span>Email :</span>
                           <span>{{@$user->email}}</span>
                        </div>
                     </div>
                     <div class="employee-dtls2-wrapper employee-dtls2-wrapper2">
                        <div class="employee-dtls2-holder">
                           <span>Mobile :</span>
                           <span>{{@$user->phone_no}}</span>
                        </div>
                     </div>
                     <div class="employee-dtls2-wrapper">
                        <div class="employee-dtls2-holder">
                           <span>Skype :</span>
                           <span>{{@$user->employeeUser->skype_id}}</span>
                        </div>
                     </div>
                     <div class="employee-dtls2-wrapper employee-dtls2-wrapper2">
                        <div class="employee-dtls2-holder">
                           <span>Place :</span>
                           <span>{{@$user->employeeUser->place}}</span>
                        </div>
                     </div>
                     <div class="employee-dtls2-wrapper">
                        <div class="employee-dtls2-holder">
                           <span>Resume  :</span>
                           <span><a href="{{Storage::disk('s3')->url('/upload_files/employee-resumes/' . @$user->employeeCvTxn->cv_path)}}" download target="_blank">{{@$user->employeeCvTxn->cv_path}}</a></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="employee-dtls-wrp">
                  <h4 class="emdtls-hding">Summary of Your Profile</h4>
                  <div class="emploee-contnt-dtls-wrp">
                     <div class="employee-cntnt-holder">
                        <div class="employee-cntnt1">
                           <span>Job Title :</span>
                           <span>{{@$user->employeeUser->profile_title}}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Total Experience :</span>
                           <span>{{@$user->employeeUser->totExprYr->name.'Yrs'}} {{@$user->employeeUser->totExpMn->name}}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                        
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Current Salary :</span>
                           <span>{{@$user->employeeUser->current_salary}}
                           {{@$user->employeeUser->currency->currency_code}}/{{@$user->employeeUser->current_salary_type}}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Notice Period :</span>
                           <span>{{@$user->employeeUser->noticePeriod->name}}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Profile Summary :</span>
                           <span>{!!@$user->employeeUser->summary!!}</span>
                        </div>
                       
                     </div>
                  </div>
               </div>
               <div class="employee-dtls-wrp">
                  <h4 class="emdtls-hding">Desire Job Details</h4>
                  <div class="emploee-contnt-dtls-wrp">
                     <div class="employee-cntnt-holder">
                        <div class="employee-cntnt1">
                           <span>Prefer Country :</span>
                           @if(@$user->employeePrefTxn->countries)
                           <span>{{@$user->employeePrefTxn->countries->pluck('name')}}</span>
                           @endif
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Prefer City :</span>
                           @if(@$user->employeePrefTxn->cities)
                           <span>{{@$user->employeePrefTxn->cities->pluck('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Industry :</span>
                           @if(@$user->employeePrefTxn->industries)
                           <span>{{@$user->employeePrefTxn->industries->pluck('name')}}</span>
                           @endif
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Department :</span>
                           <span>
                              @if(@$user->employeePrefTxn->departments)
                              {!!@$user->employeePrefTxn->departments->pluck('name')!!}</span>
                              @endif
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Designation :</span>
                           <span>{{@$user->employeePrefTxn->designation}}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Job Type :</span>
                           <span>{{@$user->employeePrefTxn->employment->name}}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Shift Type :</span>
                           @if(@$user->employeePrefTxn->shiftTypes)
                           <span>{{@$user->employeePrefTxn->shiftTypes->pluck('name')}}</span>
                           @endif
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Expected Salary :</span>
                           <span>{{@$user->employeePrefTxn->min_salary}} - {{@$user->employeePrefTxn->max_salary}} {{@$user->employeePrefTxn->currency->currency_code}}/{{@$user->employeePrefTxn->salary_type}}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="employee-dtls-wrp">
                  <h4 class="emdtls-hding">Personal Information</h4>
                  <div class="emploee-contnt-dtls-wrp">
                     <div class="employee-cntnt-holder">
                        <div class="employee-cntnt1">
                           <span>Permanent Address :</span>
                           <span>{!!@$user->employeeUser->permanent_address!!}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Present Address :</span>
                           <span>{!!@$user->employeeUser->present_address!!}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Date of Birth :</span>
                           <span>{{@$user->employeeUser->date_of_birth}}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Nationality :</span>
                           <span>{!!@$user->employeeUser->nationality!!}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Hometown :</span>
                           <span>{{@$user->employeeUser->home_town}}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Living Country :</span>
                           <span>{{@$user->employeeUser->country->name}}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Living City :</span>
                           <span>{{@$user->employeeUser->city->name}}</span>
                        </div>
                        <div class="employee-cntnt1 employee-cntnt2">
                           <span>Marital Status :</span>
                           <span>{{@$user->employeeUser->getMarriedStatusAttribute()}}</span>
                        </div>
                     </div>
                     <div class="employee-cntnt-holder">
                       <div class="employee-cntnt1 employee-cntnt2">
                           <span>Gender :</span>
                           <span>{{@$user->employeeUser->getGenderTextAttribute()}}</span>
                        </div>
                     </div>
                  </div>
               </div>

               <!-- <div class="employee-dtls-wrp employee-dtls-wrp2">
                  <h4 class="emdtls-hding">Lorm Ipsam</h4>
                  <div class="emplyr-img-holder">
                     <img src="http://localhost/job-portal/admin/public/dist/img/user2-160x160.jpg">
                  </div>
                  <div class="employee-dtls2-wrap">
                  <div class="employee-dtls2-wrapper">
                     <div class="employee-dtls2-holder">
                        <span>Name:</span>
                        <span>Rammoy Ghosh</span>
                     </div>
                  </div>
                  <div class="employee-dtls2-wrapper employee-dtls2-wrapper2">
                     <div class="employee-dtls2-holder">
                        <span>Name:</span>
                        <span>Rammoy Ghosh</span>
                     </div>
                  </div>
                  </div>
               </div> -->

            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->

      <!-- START EMPOLOYER DETALIS DIV -->

     


      
      <!-- END EMPOLOYER DETALIS DIV -->
   </section>
   <!-- /.content -->
</div>
@endsection