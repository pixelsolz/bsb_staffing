@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Employer Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>
              <form action="{{route('admin.user-manage.employer')}}" method="get">
                <div class="row">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_joined_date" class="form-control pull-right datepicker" placeholder="Start Created Date" value="{{!empty($request->from_joined_date)?$request->from_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_joined_date" class="form-control pull-right datepicker" placeholder="End Created Date" value="{{!empty($request->to_joined_date)?$request->to_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_login_date" class="form-control pull-right datepicker" placeholder="Start Last Active Date" value="{{!empty($request->from_login_date)?$request->from_login_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_login_date" class="form-control pull-right datepicker" placeholder="End Last Active Date" value="{{!empty($request->to_login_date)?$request->to_login_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>


                </div>
                <div class="row ">
                  <div class="col-xs-2">
                    <div class="form-group">
                      @php $getCityUrl = url('city-ajax'); @endphp
                      <select class="form-control" name="search_by_country" onchange="getCity(this.value,'{{$getCityUrl}}')">
                        <option value="">Select Country</option>
                       @foreach($countries as $country)
                        <option value="{{$country->id}}" @if(!empty($request->search_by_country) && $request->search_by_country == $country->id) selected @endif >{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control set_city" name="search_by_city">
                      <option value="">Select City</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="status">
                      <option value="">Select Status</option>
                      <option value="1" @if(!empty($request->status) && $request->status == 1) selected @endif>Active</option>
                      <option value="0" @if(!empty($request->status) && $request->status == 0) selected @endif>Inactive</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="search_by_industry">
                      <option value="">Select Industry</option>
                      @if($industries)
                        @foreach($industries as $industry)
                        <option value="{{$industry->_id}}" @if(!empty($request->search_by_industry) && $request->search_by_industry == $industry->_id) selected @endif >{{$industry->name}}</option>
                        @endforeach
                      @endif
                    </select>
                    </div>
                  </div>

                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="gender">
                      <option value="">Select Gender</option>
                      <option value='male' @if(!empty($request->gender) && $request->gender == 'male') selected @endif>Male</option>
                      <option value='female' @if(!empty($request->gender) && $request->gender == 'female') selected @endif>Female</option>
                       <option value='custom' @if(!empty($request->gender) && $request->gender == 'custom') selected @endif>Custom</option>
                    </select>
                    </div>
                  </div>

                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="employer_type">
                      <option value="">Select Employer Type</option>
                      <option value='1' @if(!empty($request->employer_type) && $request->employer_type == '1') selected @endif>Full Completed Employer</option>
                      <option value='2' @if(!empty($request->employer_type) && $request->employer_type == '2') selected @endif>Not Full Complete Employer</option>
                    </select>
                    </div>
                  </div>

                </div>
                  <div class="row">
                    <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by ID, name, Company Name, email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>
                    <div class="col-xs-4">
                    <button type="submit" class="btn btn-info">Search</button>
                    <a href="{{route('admin.user-manage.employer')}}" class="reset">Reset</a>
                    </div>
                  </div>
              </form>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Sl No.</th>
                    <th>Company Name</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone No</th>
                    <th>Country</th>
                    <th>City</th>
                    <th>Gender</th>
                    <th>Industry</th>
                    <!-- <th>Address</th> -->
                    <th>Status</th>
                    <th>Posted Job</th>
                    <th>Posted International Job</th>
                    <th>Posted Walking</th>
                    <th>Cv View</th>
                    <th>Cv Download</th>
                    <th>Mass Mail Send</th>
                    <th>Last Active Date</th>
                    <th>Login Status</th>
                    <th>Created Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                  @forelse($users as $user)
                  <tr>

                    <td>{{$a +($page*25)}}</td>
                    <td>{{@$user->employerUser->company_name}}</td>
                    <td>{{$user->isEmployer() ? @$user->employerUser->first_name : @$user->employeeUser->first_name }} {{$user->isEmployer() ? @$user->employerUser->last_name : @$user->employeeUser->last_name }}</td>
                    <td>{{$user->email}}</td>
                    <td>{{@$user->phone_no}}</td>
                    <td>{{@$user->employerUser->country->name}}</td>
                    <td>{{@$user->employerUser->city->name}}</td>
                    <td>{{@$user->employerUser->gender}}</td>
                    <td>{{@$user->employerUser->industry_id ? @$user->employerUser->industry->name : '' }}</td>

                    <!-- <td>{{@$user->employerUser->address}}</td> -->
                    <td>{{$user->status ==1? 'Active':'Inactive'}}</td>
                    <td>@if($user->jobCount()) <a href="{{url('employer/posted-job/?id='.$user->id)}}">{{$user->jobCount()}}</a> @else {{$user->jobCount()}} @endif</td>
                    <td>@if($user->internationalJobCount()) <a href="{{url('employer/international-job/?id='.$user->id)}}">{{$user->internationalJobCount()}}</a> @else {{$user->internationalJobCount()}} @endif</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>

                    <td>{{@$user->last_login}}</td>
                    <td>
                    @if(@$user->getLoginStatus($user) == 3)
                    <span><i class="fa fa-circle" style="font-size:18px;color:red"></i></span>
                    @elseif(@$user->getLoginStatus($user) == 2)
                     <span><i class="fa fa-circle" style="font-size:18px;color:yellow"></i></span>
                    @else
                     <span><i class="fa fa-circle" style="font-size:18px;color:green"></i></span>
                    @endif
                  </td>

                    <td>{{$user->created_at}}</td>
                    <td>
                      <a href="{{route('admin.user-manage.show_employer', ['id'=>$user->id])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                     <a href="{{route('admin.user-manage.edit-employer', ['id'=>$user->id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                  @php
                    $deleteroute = route('admin.user-manage.delete', ['id'=>$user->id]);
                  @endphp
                     <a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove"></a>

                    </td>
                    </tr>
                     @php $a++; @endphp
                 @empty
                  <tr>
                    <td colspan="9" style="text-align: center;">No data available</td>
                  </tr>
                 @endforelse
                  </tbody>

                </table>
              </div>
              <div>{{ $users->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
