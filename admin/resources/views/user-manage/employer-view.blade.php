@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         View User
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> View User</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User View</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <div class="form-wrpr">
                  <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>ID : </label>
                        <span>BSBEMR{{@$user->bsb_unique_no}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name : </label>
                        <span>{{@$user->employerUser->first_name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name : </label>
                        <span>{{@$user->employerUser->last_name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>User Name : </label>
                        <span>{{@$user->user_name}}</span>
                     </div>
                     
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email : </label>
                        <span>{{@$user->email}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile : </label>
                        <span>{{@$user->phone_no}}</span>
                     </div>
                  </div>
               </div>
               <div class="form-wrpr">
                  <div class="row">
                      <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Name : </label>
                        <span>{{@$user->employerUser->company_name}}</span>
                     </div> 
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Country Name : </label>
                        <span>{{@$user->employerUser->country->name}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>City Name : </label>
                        <span>{{@$user->employerUser->city->name}}</span>
                     </div>
                     
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>address : </label>
                        <span>{{@$user->employerUser->address}}</span>
                     </div>
                     
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>zipcode : </label>
                        <span>{{@$user->employerUser->zipcode}}</span>
                     </div>
                      <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>designation : </label>
                        <span>{{@$user->employerUser->designation}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Industry Name : </label>
                        <span>{{@$user->employerUser->industry_id ? @$user->employerUser->industry->name : '' }}</span>
                     </div> 
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Phone no : </label>
                        <span>{{@$user->employerUser->alternate_ph_no}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Type : </label>
                        <span>{{@$user->employerUser->companyType->company_type}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Size : </label>
                        <span>{{@$user->employerUser->companySize->company_size}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Email : </label>
                        <span>{{@$user->employerUser->company_email}}</span>
                     </div>
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Company Website : </label>
                        <span>{{@$user->employerUser->company_website}}</span>
                     </div>
                      <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Tagline : </label>
                        <span>{!!@$user->employerUser->company_tagline!!}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Company Short Desc : </label>
                        <span>{!!@$user->employerUser->company_short_desc!!}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="logo-lbl">Company Logo : </label>
                        <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/employer/logo/' . @$user->employerUser->company_logo)}}"></span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="cmpny-glry">Company Gallery Images : </label>
                        @if(@$user->galleryUser)
                        @foreach($user->galleryUser as $img)
                           <span class="glry-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/employer/gallery_images/' . @$img->name)}}"></span>
                        @endforeach
                        @endif
                        <span>

                        </span>
                     </div>
                     
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Business Licence Type : </label>
                        @if(@$user->employerUser->business_licence_type == 1)
                        <span>Provided by state government/ province government business license</span>
                        @elseif(@$user->employerUser->business_licence_type == 2)
                           <span>Provided by Central Government business license</span>
                        @elseif(@$user->employerUser->business_licence_type == 3)
                           <span>Provided by Federal Government business license</span>
                        @else
                           <span></span>
                        @endif
                        
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Business Licence : </label>
                        <span><a href="{{Storage::disk('s3')->url('/upload_files/employer/business_licence/' . @$user->employerUser->business_licence)}}" target="_blank">{{@$user->employerUser->business_licence}}</a></span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Lat : </label>
                        <span>{{@$user->employerUser->lat}}</span>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Lng : </label>
                        <span>{{@$user->employerUser->lng}}</span>
                     </div>
                  </div>
               </div>

            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection