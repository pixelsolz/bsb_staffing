

<div class="box-body">
   <div class="form-group row @if($errors->has('job_title'))has-error @endif">
      <div class="col-xs-9">
         <label for="job_title">College Name<span class="required_field"></span></label>
         <input type="text" name="job_title" class="form-control" value="{{$job->job_title}}">
         @if($errors->has('job_title'))
         <span class="error">{{$errors->first('job_title')}}</span>
         @endif
      </div>
   </div>
   <div class="form-group row @if($errors->has('job_title'))has-error @endif">
      <div class="col-xs-9">
         <label for="job_title">Website<span class="required_field"></span></label>
         <input type="text" name="job_title" class="form-control" value="{{$job->job_title}}">
         @if($errors->has('job_title'))
         <span class="error">{{$errors->first('job_title')}}</span>
         @endif
      </div>
   </div>
      <div class="form-group row @if($errors->has('job_title'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="country_id">Country<span class="required_field"></span></label>
                           <select name="country_id" class="form-control">
                              <option value="">Select Country</option>
                               @foreach($allCountry as $country)
                              <option value="{{$country->_id}}" @if(!empty($job->country_id) && $job->country_id == $country->_id) selected @endif>{{$country->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('country_id'))
                           <span class="error">{{$errors->first('country_id')}}</span>
                           @endif
                        </div>
   </div>
      <div class="form-group row @if($errors->has('job_title'))has-error @endif">
      <div class="col-xs-9">
         <label for="job_title">City<span class="required_field"></span></label>
                           <select name="city_id" class="form-control">
                             <option value="">Select City</option>
                              @foreach($selectedCity as $city)
                              <option value="{{$city->_id}}" @if(!empty($job->city_id) && $job->city_id == $city->_id) selected @endif>{{$city->name}}</option>
                              @endforeach
                           </select>
         @if($errors->has('job_title'))
         <span class="error">{{$errors->first('job_title')}}</span>
         @endif
      </div>
   </div>
   <div class="form-group row @if($errors->has('job_title'))has-error @endif">
      <div class="col-xs-9">
         <label for="job_title">Postal Address<span class="required_field"></span></label>
         <textarea name="job_title" class="form-control"></textarea>
         @if($errors->has('job_title'))
         <span class="error">{{$errors->first('job_title')}}</span>
         @endif
      </div>
   </div>
   <div class="form-group row @if($errors->has('job_title'))has-error @endif">
      <div class="col-xs-9">
         <label for="job_title">Logo<span class="required_field"></span></label>
          <input type="file" name="job_title" class="form-control" value="{{$job->job_title}}">
         @if($errors->has('job_title'))
         <span class="error">{{$errors->first('job_title')}}</span>
         @endif
      </div>
   </div>
   <div class="form-group row @if($errors->has('job_title'))has-error @endif">
      <div class="col-xs-9">
         <label for="job_title">Course<span class="required_field"></span></label>
                           <select name="loc_cities" class="form-control" multiple="true">
                             <option value="">Select City</option>
                              @foreach($selectedLocationCities as $city)
                              <option value="{{$city->_id}}" @if(!empty($job->loc_cities) && @$job->loc_cities && array_search($city->_id, $job->loc_cities) !== false) selected @endif> {{$city->name}}</option>
                              @endforeach
                           </select>
         @if($errors->has('job_title'))
         <span class="error">{{$errors->first('job_title')}}</span>
         @endif
      </div>
   </div>



</div>

