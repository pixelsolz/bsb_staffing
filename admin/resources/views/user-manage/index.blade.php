@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($user_role_type)}} User Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>
              @php $url = '/user-manage/'.$user_role_type;
                //echo $url;die;
              @endphp
              <form action="{{url($url)}}" method="get">
                <div class="row">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="search_by_date_type">
                        <option value="">Select By Date Type</option>
                        <option value="created_date" @if($request->search_by_date_type && $request->search_by_date_type =='created_date') selected @endif >By Created Date</option>
                        <option value="last_active_date" @if($request->search_by_date_type && $request->search_by_date_type =='last_active_date') selected @endif>By Last Active Date</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_joined_date" class="form-control pull-right datepicker" placeholder="From Date" value="{{!empty($request->from_joined_date)?$request->from_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_joined_date" class="form-control pull-right datepicker" placeholder="To Date" value="{{!empty($request->to_joined_date)?$request->to_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      @php $getCityUrl = url('city-ajax'); @endphp
                      <select class="form-control" name="search_by_country" onchange="getCity(this.value,'{{$getCityUrl}}')">
                        <option value="">Select Country</option>
                       @foreach($countries as $country)
                        <option value="{{$country->id}}" @if(!empty($request->search_by_country) && $request->search_by_country == $country->id) selected @endif >{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control set_city" name="search_by_city">
                      <option value="">Select City</option>
                    </select>
                    </div>
                  </div>

                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="status">
                      <option value="">Select Status</option>
                      <option value="1" @if(!empty($request->status) && $request->status == 1) selected @endif>Active</option>
                      <option value="0" @if(!empty($request->status) && $request->status == 0) selected @endif>Inactive</option>
                    </select>
                    </div>
                  </div>





                </div>
                <div class="row ">
                   @if(@$user_role_type == 'trainer')
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="trainer_type">
                      <option value="">Select Trainer Type</option>

                      <option value="1" @if(!empty($request->trainer_type) && $request->trainer_type == 1) selected @endif>Job Trainer</option>
                      <option value="2" @if(!empty($request->trainer_type) && $request->trainer_type == 2) selected @endif>Visa Trainer</option>

                    </select>
                    </div>
                  </div>
                  @endif
                  @if(@$user_role_type != 'trainer')
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="search_by_cv_type">
                        <option value="">Select By CV Type</option>
                        <option value="by_upload_cv" @if(!empty($request->search_by_cv_type) && $request->search_by_cv_type == 'by_upload_cv') selected @endif>By CV Upload</option>
                        <option value="by_premium_cv" @if(!empty($request->search_by_cv_type) && $request->search_by_cv_type == 'by_premium_cv') selected @endif>By Premium CV</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_cv_date" class="form-control pull-right datepicker" placeholder="From Date" value="{{!empty($request->from_cv_date)?$request->from_cv_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_cv_date" class="form-control pull-right datepicker" placeholder="To Date" value="{{!empty($request->to_cv_date)?$request->to_cv_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  @endif
                   @if(@$user_role_type == 'college')
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="course">
                      <option value="">Select Course</option>
                      @foreach($courses as $course)
                      <option value="{{$course->_id}}" @if(!empty($request->course) && $request->course == $course->_id) selected @endif>{{$course->name}}</option>
                      @endforeach
                    </select>
                    </div>
                  </div>
                  @endif
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="gender">
                      <option value="">Select Gender</option>
                      <option value='male' @if(!empty($request->gender) && $request->gender == 'male') selected @endif>Male</option>
                      <option value='female' @if(!empty($request->gender) && $request->gender == 'female') selected @endif>Female</option>
                       <option value='custom' @if(!empty($request->gender) && $request->gender == 'custom') selected @endif>Custom</option>
                    </select>
                    </div>
                  </div>
                  @if(@$user_role_type != 'college')
                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by ID, name, {{@$user_role_type}} Name ,email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>
                  @endif
                </div>
                  <div class="row ">
                    @if(@$user_role_type == 'college')
                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder="Search by ID, name, {{@$user_role_type}} Name ,email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>
                  @endif
                    <div class="col-xs-12">
                    <button type="submit" class="btn btn-info">Search</button>
                    <a href="{{url($url)}}" class="reset">Reset</a>
                    </div>
                  </div>
              </form>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  @if(@$user_role_type == 'trainer')
                  <th>Trainer Type</th>
                  @elseif(@$user_role_type == 'college')
                  <th>College/ University/ Institution</th>
                  @else
                  <th>Company Name</th>
                  @endif
                  <th>Contact Person Name</th>
                  <th>Email</th>
                  <th>Phone No</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Gender</th>
                  @if(@$user_role_type != 'trainer')
                  <th>Number of cv Upload</th>
                  <th>Number of cv premium</th>
                  @endif
                  @if(@$user_role_type == 'trainer')
                    <th>Training Done</th>
                    <th>Upcoming Training</th>
                  @endif
                   <th>Last Active Date</th>
                   <th>Login Status</th>
                  <th>Created Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($users as $user)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  @if(@$user_role_type == 'trainer')
                    @if($user->trainerUser->trainer_type == 1)
                    <td>Job Trainer</td>
                    @elseif($user->trainerUser->trainer_type == 2)
                    <td>Visa Trainer</td>
                    @else
                    @endif
                  @else
                  <td>{{$user->GetOtherUserComp($user)}}</td>
                  @endif
                  <td>{{$user->getOtherUserName($user)}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{@$user->phone_no}}</td>
                  <td>{{$user->GetOtherUserCountry($user)}}</td>
                  <td>{{@$user->GetOtherUserCity($user)}}</td>
                  <td>{{@$user->GetOtherUserGender($user)}}</td>
                  @if(@$user_role_type != 'trainer')
                  @if(@$user->collegeStudents()->GetDatewiseStudent($cvCountTodate, $cvCountFromdate) > 0)
                  <td>
                    <a href="{{url('user-manage/employee?institute_code='.$user->user_unique_code)}}">{{@$user->collegeStudents()->GetDatewiseStudent($cvCountTodate, $cvCountFromdate)}}</a>
                  </td>
                  @else
                  <td>{{@$user->collegeStudents()->GetDatewiseStudent($cvCountTodate, $cvCountFromdate)}}</td>
                  @endif
                  <td>0</td>
                  @endif
                  @if(@$user_role_type == 'trainer')
                    <td>@if(@$user->trainerSchedule()->trainingDone() > 0) <a href="{{route('admin.user-trainer.schedule',['trainer'=>$user->id, 'type'=>'completed'])}}">{{@$user->trainerSchedule()->trainingDone()}}</a> @else {{@$user->trainerSchedule()->trainingDone()}} @endif</td>
                    <td>@if(@$user->trainerSchedule()->trainingUpcoming() > 0) <a href="{{route('admin.user-trainer.schedule',['trainer'=>$user->id, 'type'=>'upcoming'])}}">{{@$user->trainerSchedule()->trainingUpcoming()}}</a> @else {{@$user->trainerSchedule()->trainingUpcoming()}} @endif</td>
                  @endif
                  <td>{{@$user->last_login}}</td>
                  <td>
                    @if(@$user->getLoginStatus($user) == 3)
                    <span><i class="fa fa-circle" style="font-size:18px;color:red"></i></span>
                    @elseif(@$user->getLoginStatus($user) == 2)
                     <span><i class="fa fa-circle" style="font-size:18px;color:yellow"></i></span>
                    @else
                     <span><i class="fa fa-circle" style="font-size:18px;color:green"></i></span>
                    @endif
                  </td>
                  <td>{{@$user->created_at}}</td>
                  <td>{{$user->status ==1? 'Active':'Inactive'}}</td>
                  <td>
                    <a href="{{route('admin.user-manage.show', ['id'=>$user->id,'user_type'=>$user_role_type])}}" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                   <a href="{{route('admin.user-manage.edit', ['id'=>$user->id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                @php
                  $deleteroute = route('admin.user-manage.delete', ['id'=>$user->id]);
                @endphp
                   <a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove"></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table></div>
              <div>{{ $users->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
