@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Internal Employee Manage

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <a href="{{route('admin.user-manage.create.internal-employee')}}" class="btn btn-primary">Creat New</a><br/><br/>
              <div class="clearfix"></div>
              <form action="{{route('admin.user-manage.list.internal-employee')}}" method="get">
                <div class="row">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="from_joined_date" class="form-control pull-right datepicker" placeholder="Start Date" value="{{!empty($request->from_joined_date)?$request->from_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="text" name="to_joined_date" class="form-control pull-right datepicker" placeholder="End Date" value="{{!empty($request->to_joined_date)?$request->to_joined_date: '' }}" autocomplete="off">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      @php $getCityUrl = url('city-ajax'); @endphp
                      <select class="form-control" name="search_by_country" onchange="getCity(this.value,'{{$getCityUrl}}')">
                        <option value="">Select Country</option>
                       @foreach($countries as $country)
                        <option value="{{$country->id}}" @if(!empty($request->search_by_country) && $request->search_by_country == $country->id) selected @endif >{{$country->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control set_city" name="search_by_city">
                      <option value="">Select City</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                      <select class="form-control" name="status">
                      <option value="">Select Status</option>
                      <option value="1" @if(!empty($request->status) && $request->status == 1) selected @endif>Active</option>
                      <option value="0" @if(!empty($request->status) && $request->status == 0) selected @endif>Inactive</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="referrel_code" placeholder="Search By Referrel code" value="{{!empty($request->referrel_code)?$request->referrel_code: '' }}">
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <input type="text" class="form-control" name="input_search" placeholder=" Search name, email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">
                    </div>
                  </div>
                </div>
                  <div class="row ">
                    <div class="col-xs-12">
                    <button type="submit" class="btn btn-info">Search</button>
                    <a href="{{route('admin.user-manage.list.internal-employee')}}" class="reset">Reset</a>
                    </div>
                  </div>
              </form>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone No</th>
                  <th>Referral Code</th>
                  <th>User Role</th>
                  <th>Referral user count</th>
                  <th>Created Date</th>
                  <th>Termination Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; $page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($users as $user)
                <tr>
                  <td>{{$a +($page*25)}}</td>
                  <td>{{@$user->internalEmployeeUser->first_name}} {{@$user->internalEmployeeUser->last_name}}</td>
                  <td>{{@$user->email}}</td>
                  <td>{{@$user->phone_no}}</td>
                   <td>{{@$user->user_unique_code}}</td>
                  <td>{{@$user->internalEmployeeUser->user_role}}</td>
                  <td>{{@$user->staffReferalEmployes->count() + @$user->staffReferalEmployers->count()}}</td>
                  <td>{{@$user->created_at}}</td>
                  <td>{{@$user->updated_at}}</td>
                  <td>{{@$user->status ==1? 'Active':'Inactive'}}</td>
                  <td>
                    <a href="{{route('admin.user-manage.internal-employee.show', ['id'=>$user->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                   <a href="{{route('admin.user-manage.internal-employee.edit', ['id'=>$user->id])}}" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                @php
                  $deleteroute = route('admin.user-manage.delete', ['id'=>$user->id]);
                @endphp
                   <a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove"></a>
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>
              <div>{{ $users->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
