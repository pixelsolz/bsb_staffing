@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         View Staff
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.list.internal-employee') }}"><i class="fa fa-dashboard"></i> Staff List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> View Staff</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Staff View</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>



                  <div class="form-wrpr">
                     <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Name : </label>
                           <span>{{@$user->internalEmployeeUser->first_name .' '.@$user->internalEmployeeUser->last_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Designation : </label>
                           <span>{{@$user->internalEmployeeUser->designation}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>User Name : </label>
                           <span>{{@$user->user_name}}</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Email : </label>
                           <span>{{@$user->email}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Mobile : </label>
                           <span>{{@$user->phone_no}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Alt Mobile : </label>
                           <span>{{@$user->internalEmployeeUser->alternate_ph_no}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Country : </label>
                           <span>{{@$user->internalEmployeeUser->country->name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>City : </label>
                           <span>{{@$user->internalEmployeeUser->city->name}}</span>
                        </div>
                          <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Date Of Joining : </label>
                           <span>{{@$user->internalEmployeeUser->date_of_joining}}</span>
                        </div>
                          <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Gender : </label>
                           <span>{{@$user->internalEmployeeUser->gender_text}}</span>
                        </div>
                          <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Address : </label>
                           <span>{{@$user->internalEmployeeUser->address}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Zip : </label>
                           <span>{{@$user->internalEmployeeUser->zipcode}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Referal Code : </label>
                           <span>{{@$user->user_unique_code}}</span>
                        </div>
                         <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Profile picture : </label>
                           @if($user->internalEmployeeUser->profile_picture)
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/other_users/' . @$user->internalEmployeeUser->profile_picture)}}"></span>
                           @endif
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Gov. Id : </label>
                           @if(@$user->internaleEployeeGovtids && @$user->internaleEployeeGovtids()->first())
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/internal_employee/govtId/' . @$user->internaleEployeeGovtids()->first()->file_name)}}"></span>
                           @endif
                        </div>

                     </div>


                  </div>
                  <div class="form-wrpr">
                     <div class="row">
                         <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Office Name : </label>
                           <span>{{@$user->internalEmployeeUser->office_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Company Email : </label>
                           <span>{{@$user->internalEmployeeUser->company_email}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Company Logo : </label>
                           @if(@$user->internalEmployeeUser->company_logo)
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/other_users/' . @$user->internalEmployeeUser->company_logo)}}"></span>
                           @endif
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Working Segment : </label>
                           <span>{{@$user->internalEmployeeUser->user_role}}</span>
                        </div>

                     </div>
                  </div>



            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection