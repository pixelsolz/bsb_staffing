@extends('layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Training Date</th>
                  <th>Training From Time</th>
                  <th>Training To Time</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; @endphp
                @forelse($data as $schedule)
                <tr>
                  <td>{{$a}}</td>
                  <td>{{\Carbon\Carbon::parse($schedule->training_actual_timestamp)->format('d/m/Y')}}</td>
                  <td>{{$schedule->from_time}} {{$schedule->from_am_pm}}</td>
                  <td>{{$schedule->to_time}} {{$schedule->to_am_pm}}</td>
                  <td>{{$type}}</td>
                </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="5" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table></div>
              <div>{{ $data->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
