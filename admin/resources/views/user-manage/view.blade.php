@extends('layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         View {{ucfirst($user_role)}} User
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('admin.user-manage.index') }}"><i class="fa fa-dashboard"></i> Users List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> View User</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User View</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               @php //echo "<pre>";print_r($user->GetOtherUserDetails($user));@endphp


               @if($user_role == 'franchise' || $user_role == 'agency')
                  <div class="form-wrpr">
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>ID : </label>
                           <span>{{@$user->getBsbUserNoAttribute()}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->contact_person_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Designation : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->contact_person_designation}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>User Name : </label>
                           <span>{{@$user->user_name}}</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Email : </label>
                           <span>{{@$user->email}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Mobile : </label>
                           <span>{{@$user->phone_no}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Company Certificate : </label>
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/other_users/' . @$user->GetOtherUserDetails($user)->registration_certificate)}}"></span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Contact Person Gov. Id : </label>
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/other_users/' . @$user->GetOtherUserDetails($user)->govt_id)}}"></span>
                        </div>
                     </div>

                  </div>
                  <div class="form-wrpr">
                     <div class="row">
                         <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Company Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->comp_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Job Location Country : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->country->name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>City Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->city->name}}</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Company Address : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->address}}</span>
                        </div>
                     </div>
                  </div>
                  <div class="form-wrpr">
                     <div class="row">
                         <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Name of Account Holder : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->name_account_holder}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Bank Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->bank_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Account Number : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->account_no}}</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>SWIFT CODE : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->swift_code}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>IFSC CODE : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->ifsc_code}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Bank Postal address : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->bank_portal_address}}</span>
                        </div>
                     </div>
                  </div>
               @endif
               @if($user_role == 'college')
                  <div class="form-wrpr">
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>ID : </label>
                           <span>{{@$user->getBsbUserNoAttribute()}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>College Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Website : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->website}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Contact Person Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->contact_person_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Contact Person Designation : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->contact_person_designation}}</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Email : </label>
                           <span>{{@$user->email}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Mobile : </label>
                           <span>{{@$user->phone_no}}</span>
                        </div>
                     </div>
                  </div>
                  <div class="form-wrpr">
                     <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Country : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->country->name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>City Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->city->name}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Address : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->address}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Selected Courses offered : </label>
                           @php
                              $courses = '';
                              if($user->collegeCourses){
                                 $coursesArray = json_decode(json_encode($user->collegeCourses), true);
                                 $coursesNameArr = array_map(function ($ar) {return $ar['name'];}, $coursesArray);
                                 //print_r($coursesNameArr);
                                 $courses = implode(',',$coursesNameArr);
                              }
                           @endphp
                           <span>{{@$courses}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">College Logo : </label>
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/colleges/' . @$user->GetOtherUserDetails($user)->logo)}}"></span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="cmpny-glry">College Gallery Images : </label>
                           @php
                              $gallery_imageArr=[];
                              if($user->GetOtherUserDetails($user)->gallery_images !=''){
                                 $gallery_imageArr = explode(',',@$user->GetOtherUserDetails($user)->gallery_images);
                                 //print_r($gallery_imageArr);
                              }
                           @endphp
                           @if(@$gallery_imageArr)
                           @foreach($gallery_imageArr as $img)
                              <span class="glry-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/colleges/gallery_images/' . @$img)}}"></span>
                           @endforeach
                           @endif
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Total No of Students : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->total_student}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Vacancy for placement per year : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->total_student_place_per_year}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Institute Campus Video(Youtube Link) : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->institute_video}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>About your College/ University/ Institution : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->about_college!!}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Mission : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->mission!!}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Special Note for Employer : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->special_note_employer!!}</span>
                        </div>
                     </div>
                  </div>
                 {{--<div class="form-wrpr">
                     <div class="row">
                         <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Name of Account Holder : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->name_account_holder}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Bank Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->bank_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Account Number : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->account_no}}</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>SWIFT CODE : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->swift_code}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>IFSC CODE : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->ifsc_code}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Bank Postal address : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->bank_postal_address}}</span>
                        </div>
                     </div>
                  </div>--}}
               @endif
               @if($user_role == 'trainer')
                  <div class="form-wrpr">
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>ID : </label>
                           <span>{{@$user->getBsbUserNoAttribute()}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Middle Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->mid_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Family Name : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->family_name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Email : </label>
                           <span>{{@$user->email}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Mobile : </label>
                           <span>{{@$user->phone_no}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Skype : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->skype_id}}</span>
                        </div>
                     </div>
                  </div>
                  <div class="form-wrpr">
                     <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Gender : </label>
                           <span>@if(@$user->GetOtherUserDetails($user)->gender == 1){{'Male'}} @elseif(@$user->GetOtherUserDetails($user)->gender == 2) {{'Female'}} @else {{' '}}@endif</span>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Date Of Birth : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->date_of_birth}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>Country : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->country->name}}</span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label>City : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->city->name}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Address : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->address}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Trainer Type : </label>
                           <span>@if(@$user->GetOtherUserDetails($user)->trainer_type == 1){{'Job Trainer'}} @elseif(@$user->GetOtherUserDetails($user)->trainer_type == 2) {{'Visa Trainer'}} @else {{' '}}@endif</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Industry : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->industry->name}}</span>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Current Designation : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->current_designation}}</span>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Degree : </label>
                           @php @$hgedu_id = @$user->GetOtherUserDetails($user)->highest_education; @endphp
                              @if(@$hgedu_id && is_array(@$hgedu_id))
                           <span>{{implode(',',App\Models\Course::whereIn('_id',@$hgedu_id)->pluck('name')->toArray())}}</span>
                           @endif
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Year Of Experience : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->expreiance->name}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Skill : </label>
                           <span>{{@$user->GetOtherUserDetails($user)->skills}}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>About Profile : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->about_profile!!}</span>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Name Of Account Holder : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->name_account_holder!!}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Bank Name : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->bank_name!!}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Account No.: </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->account_no!!}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Swift Code : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->swift_code!!}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Ifsc Code : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->ifsc_code!!}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label>Bank POstal Address : </label>
                           <span>{!!@$user->GetOtherUserDetails($user)->bank_portal_address!!}</span>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <label class="">Upload CV : </label>
                           <span><a href="{{Storage::disk('s3')->url('/upload_files/trainer-resumes/' . @$user->GetOtherUserDetails($user)->cv_path)}}" download target="_blank">{{@$user->GetOtherUserDetails($user)->cv_path}}</a></span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Photo : </label>
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/profile_picture/' . @$user->GetOtherUserDetails($user)->profile_image)}}"></span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <label class="logo-lbl">Government identity proofs : </label>
                           <span class="logo-img-wrapr"><img src="{{ Storage::disk('s3')->url('/upload_files/other_users/' . @$user->GetOtherUserDetails($user)->identity_proof)}}"></span>
                        </div>
                     </div>
                  </div>
               @endif
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection