<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('public/css/frontend_style.css')}}">
<div class="container">
  <div class="snippetOuter">
    <div class="snippetOutertop">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
          <h4>Current openings</h4>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
          <div class="viewOuter">
            <span>{{count($employer_jobs)}} Jobs</span>
          </div>
        </div>
      </div>
    </div>
    <div class="openingSnippetTable">
      @forelse($employer_jobs as $empr_job)
        <div class="openingSnippetRow">
          <div class="openingSnippetCol">
            <p><span><a href="{{config('app.frontendUrl').'/job-details/job-post/'.$empr_job->_id}}" target="_blank">{{$empr_job->job_title}} Salary: {{$empr_job->min_monthly_salary}} Up to {{$empr_job->max_monthly_salary}} PM for {{$empr_job->city->name}}</a></span></p>
          </div>
          <div class="openingSnippetCol">
            <p>{{@$empr_job->employer->employerUser->zipcode}}</p>
          </div>
          <div class="openingSnippetCol">
            <p>{{@$empr_job->employmentType->name}}</p>
          </div>
        </div>
        @empty

      @endforelse

    </div>
  </div>
</div>