<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});
 */

Route::post('registration', 'api\AuthController@registration');
Route::post('college-registration', 'api\AuthController@collegeRegistration');
Route::post('other-registration', 'api\AuthController@otherRegistration');
Route::post('trainer-registration', 'api\AuthController@trainerRegistration');
Route::post('login', 'api\AuthController@login');
Route::post('forget-password', 'api\AuthController@forgetPassword');
Route::get('reset-token/check/{token}', 'api\AuthController@checkResetToken');
Route::post('user/password_reset', 'api\AuthController@otherUserPasswordreset');
Route::post('email-verification/send', 'api\AuthController@userMailVerificationSend');

Route::get('skill/filter', 'api\HomeController@getSkillFilter');
Route::get('skill-and-job-title/filter', 'api\HomeController@getSkillJobTitleFilter');
//Route::get('email-verification','');

Route::get('autocomplete-search', 'api\HomeController@searchForDropdown');

Route::get('employer/autocomplete-search', 'api\HomeController@searchForDropdownEmployer');

Route::get('search-job', 'api\HomeController@getSearchJob');

Route::get('country-list', 'api\HomeController@getCountryList');

Route::get('job-list-by-country/{country_name}', 'api\HomeController@getJobByCountry');

Route::get('job-by-id/{job_id}', 'api\HomeController@getJobById');

Route::get('international/job-detail/{job_id}', 'api\InternationalJobController@getJobById');

Route::get('common-data', 'api\HomeController@getCommonData');

Route::get('get-city/codewise/{code}', 'api\HomeController@getCityByCode');

Route::get('countries-industries', 'api\HomeController@getCountriesAndIndustries');

Route::get('get-all-cities', 'api\HomeController@getcities');

Route::get('get-home-job-search', 'api\HomeController@searchJobDropdown');

Route::get('logo-branding', 'api\HomeController@getLogoBranding');

Route::get('get-job-by-search', 'api\JobManageController@getJobBySearch');

Route::post('get-job-by-adv-search', 'api\JobManageController@getJobByAdvSearch');

Route::get('employer/get-cv-by-search', 'api\CvManageController@getCvByHomeSearch');
Route::post('employer/cv-search/get-cvs', 'api\CvManageController@getCvListBySearch');

Route::get('employer/get-similar/profile/{empid}', 'api\CvManageController@viewSimilarProfile');
Route::post('employer/get-similar/profile/by-post', 'api\CvManageController@viewSimilarProfileByPostSearch');

Route::get('employer/save-search-keyword', 'api\HomeController@saveSearchKeyword');

Route::post('create-job-alert', 'api\JobManageController@createJobAlert');

Route::get('similar-jobs/{job_id}/{limit}', 'api\JobSearchController@getSimilarJobs');

Route::get('employee/remove/save-job/{id}', 'api\JobSearchController@removeSaveJob');

Route::get('employer/package-list', 'api\PackageManageController@getPackageList');

Route::resource('custom-service', 'api\CustomServiceController');

Route::get('announcement-templates', 'Admin\AnnoucementTemplateController@index');

Route::get('benefit-data', 'Admin\BenefitController@index');

Route::get('footer-job-listing', 'api\HomeController@getFooterJobs');
Route::get('trainer/common-data', 'api\TrainerController@index');
Route::get('employer/carrer-details', 'api\CarrerController@getCarrerByEmployer');

Route::get('employer/job-title/search', 'api\JobManageController@getJobTitleSearch');

Route::get('employee/job-search', 'api\JobSearchController@index');
Route::get('employee/job-search/cat', 'api\JobSearchController@index');
//wordpress plagin api
Route::get('wordpress/get-job-by-employer/{id}', 'api\JobManageController@getJobByEmployeer');

Route::get('get-announcement/template', 'api\BenefitController@getAnnouncementTemplate');
Route::post('announcement-benefit/submit-without-login', 'api\BenefitController@announcementWithOutLogin');
//Route::middleware('jwt.auth')->namespace('api')->group(function () {
Route::group(['namespace' => 'api', 'middleware' => ['jwt.auth', 'check.login.request']], function () {
	Route::get('logout', 'AuthController@logout');
	Route::post('employer-profile-update', 'AuthController@employerProfileUpdate');
	Route::post('employee-profile-update', 'AuthController@employeeProfileUpdate');
	Route::get('employee-dashboard', 'HomeController@getEmployeeDashboardDet');
	Route::post('employer-quick-profile-update', 'AuthController@employerQuickProfileUpdate');
	Route::get('employer-dashboard', 'HomeController@getEmployerDashboardDeta');
	Route::get('employer-posted-job-list', 'JobManageController@index');
	Route::resource('employer/email-template', 'EmailTemplateController');
	Route::post('employer/email-template/deleteall', 'EmailTemplateController@deleteAll');
	Route::get('employer/email-template/common-data', 'EmailTemplateController@getTemplateCommonData');
	Route::post('employer/template-send', 'EmailTemplateController@sendEmailTemplate');

	Route::get('employer-recent-job', 'JobManageController@getEmployeerPosetdJob');
	Route::post('job-check/same-exist', 'JobManageController@checkSameJobExist');

	Route::get('employer/all-template', 'EmailTemplateController@getAllTemplate');
	//Route::post('employer-job-post','JobManageController@store');
	Route::get('employee/get-single-job-data/{id}', 'JobManageController@getSingleJobArray');
	Route::resource('employer-job', 'JobManageController');
	Route::get('employer-job/status-change/{id}', 'JobManageController@jobStatus');
	Route::get('employer-job/published/{id}', 'JobManageController@jobPublished');
	Route::get('employer-job/re-published/{id}', 'JobManageController@jobRePublished');

	Route::post('check-same/walking-exist', 'WalkInInterviewController@checkSameInternationalJobExist');
	Route::resource('walkin-interview', 'WalkInInterviewController');
	Route::get('walkin-interview/status-change/{id}', 'WalkInInterviewController@walkingStatus');

	/* Employee JOB SERACH */

	Route::get('employee/get-job-by-search', 'JobManageController@getJobBySearch');
	Route::get('employee/get-singlejob/{id}/{type}', 'JobManageController@getSingleJobArrayByParam');
	Route::post('employee/get-job-by-adv-search', 'JobManageController@getJobByAdvSearch');
	Route::get('update-user/location', 'ProfileController@updateUserlatLng');

	Route::resource('employee-profile', 'ProfileController');
	Route::post('employee-profile/image-change', 'ProfileController@profileImageChange');
	Route::post('employee-profile/store-formdata', 'ProfileController@storeFormdata');
	Route::post('employee/resume-upload', 'ProfileController@uploadResume');
	Route::post('employee/video-upload', 'ProfileController@uploadVideo');
	Route::post('employee/image-upload', 'ProfileController@uploadPhoto');
	Route::post('employee/update-photo', 'ProfileController@updatePhoto');
	Route::delete('employee/delete-photo/{id}', 'ProfileController@deletePhoto');
	Route::delete('employee/delete-video/{id}', 'ProfileController@deleteVideo');
	Route::post('employee/update-education', 'ProfileController@updateEducation');
	Route::delete('employee/delete-eudcation/{id}', 'ProfileController@deleteEducation');
	Route::post('employee/update-certification', 'ProfileController@updateCertification');
	Route::delete('employee/delete-certification/{id}', 'ProfileController@deleteCertification');
	Route::post('employee/update-language', 'ProfileController@updateLanguage');
	Route::delete('employee/delete-language/{id}', 'ProfileController@deleteLanguage');
	Route::get('employee/like-profile', 'ProfileController@likeProfile');
	Route::get('employee/dis-like-profile', 'ProfileController@disLikeProfile');
	Route::get('employee/profile/cv-download/{emp_id}', 'ProfileController@downloadResume');
	Route::delete('employee/delete-resume/{id}', 'ProfileController@deleteResume');
	Route::get('employee/employee_profile_views', 'ProfileController@getEmployeeProfileView');
	Route::post('get-cv-by-search', 'CvManageController@getCvBySearch');
	Route::post('save-cv-note', 'CvManageController@saveCvNote');
	Route::get('save-cv-history', 'CvManageController@saveCvHistory');
	Route::get('view-cv-history', 'CvManageController@viewCvHistory');
	Route::get('download-resume', 'CvManageController@DownloadResume');
	Route::get('save-profile-view-history', 'CvManageController@saveProfileViewHistory');
	//Route::get('resume/download', 'CvManageController@employeeDownloadResumeLink');
	Route::post('save-reported-candidate', 'CvManageController@saveReportedCandidate');
	Route::get('employee-resume/export', 'CvManageController@employeeCvsExport');

	Route::post('employee/update-employee-history', 'ProfileController@updateEmpHistory');
	Route::delete('employee/delete-employee-history/{id}', 'ProfileController@deleteEmpHistory');

	Route::get('employee/job-search/get-initial-data', 'JobSearchController@getInitialData');
	Route::post('employee/job-search/get-jobs', 'JobSearchController@getJobListBySearch');
	Route::post('employee/job-search/save-job', 'JobSearchController@savedJob');

	Route::resource('employee/job-search', 'JobSearchController');

	Route::get('employee/job-search/multiple-initdata', 'JobSearchController@getInitDataMultipleSearch');
	Route::post('employee/job-search/multiple-jobs', 'JobSearchController@getJobsMultipleSearch');

	Route::get('employee/get-job-alert', 'JobsForController@getjobAlert');
	Route::get('employee/get-applied-jobs', 'JobsForController@getAppliedJobs');
	Route::get('employee/get-saved-jobs', 'JobsForController@getSavedJobs');
	Route::post('employee/saved-job/applied', 'JobsForController@savedJobApplied');
	Route::get('employee/get-local-waliking/jobs', 'JobsForController@getLocalWalkingJobs');
	Route::get('employee/get-inter-waliking/jobs', 'JobsForController@getInternationalWalkingJobs');
	Route::post('employee/store-slot-booking', 'JobsForController@storeSlot');

	Route::get('employee/get-application/status', 'JobsForController@getApplicationStatus');
	Route::get('mail/employee-details', 'MailController@getEmployees');
	Route::get('mail/employer-details', 'MailController@getEmployer');
	Route::resource('mail', 'MailController');
	Route::post('mail/delete', 'MailController@deleteAll');
	Route::post('mail/single-delete', 'MailController@deleteSingle');
	Route::post('mail/restore', 'MailController@restoreMail');
	Route::get('mail/markstar/{id}', 'MailController@markStar');

	Route::post('mail/label/create', 'MailController@createLabel');
	Route::delete('mail/label/delete/{id}', 'MailController@deletelabel');
	Route::post('mail/label/sendto-label', 'MailController@sendToLabel');

	Route::get('employer/application-received', 'JobsForController@getApplicationReceived');
	Route::get('application-status', 'JobsForController@getAllApplicationPhase');
	Route::get('employer/application-job', 'JobsForController@getApplicationJobById');
	Route::PUT('employer/applied-job-update/{id}', 'JobsForController@updateAppliedJob');
	Route::PUT('employer/applied-job-international-update/{id}', 'JobsForController@updateInternationalAppliedJob');

/*	Route::get('employer/cv-view-or-download-package','CvManageController@getCvViewPackage');*/

	Route::get('package-details/{id}', 'PackageManageController@getPackageById');
	Route::post('payment', 'PackageManageController@payment');
	Route::get('payment-history', 'PackageManageController@getPaymentHistory');
	Route::get('employer/check-service', 'PackageManageController@checkService');

	Route::resource('referal', 'ReferalController');

	Route::get('/other-user/home', 'HomeController@otherUserHome');

	Route::get('get-announcement-benefit', 'BenefitController@getAnnouncement');
	Route::post('announcement-benefit/submit', 'BenefitController@annoucementBenefit');

	Route::get('referral-list', 'BenefitController@getReferalBenefit');

	Route::post('create/new-referral', 'BenefitController@referalBenefitSubmit');
	Route::post('referral/message-send', 'BenefitController@referralSendMessage');

	Route::post('student/create', 'AuthController@studentCreate');
	Route::get('student-data', 'HomeController@getStudents');
	//employer folder
	Route::post('folders/get-cvs', 'FolderController@getCVbyPost');
	Route::resource('folders', 'FolderController');
	Route::get('/folder/delete-all', 'FolderController@deleteAll');

	Route::post('save-cv-to-folder', 'FolderController@saveCvFolder');
	Route::get('get-cv-folder', 'FolderController@getCvFolderById');
	Route::get('get-employee/skills', 'FolderController@getEmployeeSkills');

	Route::put('search-history/saved/{id}', 'SearchHistoryController@saveSearch');
	Route::post('search-history/saved-delete', 'SearchHistoryController@savedSearchDelete');
	Route::get('getlast-search', 'SearchHistoryController@getLastSearch');
	Route::resource('search-history', 'SearchHistoryController');

	Route::resource('college-list', 'CollegeListController');
	Route::resource('my-job-alert', 'MyAlertController');

	Route::post('trainer/profile-update', 'TrainerController@profileUpdate');
	Route::post('trainer/profile-image/update', 'TrainerController@profileImageUpdate');
	Route::post('trainer/schedule/create', 'TrainerController@traininScheduleCreate');
	Route::post('trainer/schedule/update', 'TrainerController@trainingScheduleUpdate');
	Route::get('trainer/schedule-get/{month}/{year}', 'TrainerController@getSchedule');
	Route::get('trainer/schedule/list', 'TrainerController@getTrainerSchedules');
	Route::get('trainer/booked/list/{type}', 'TrainerController@getTrainerBookedList');

	Route::get('employee/get-trainer/list/{type}', 'TrainerController@getTrainerList');
	Route::get('employee/get-slots/{month}/{trainer_id}', 'TrainerController@getTrainingSlots');
	Route::get('employee/schedule-book/store', 'TrainerController@scheduleBookStore');
	Route::post('employee/searchby-trainer', 'TrainerController@searchByTrainer');
	//employee carrer page
	Route::get('employer/carrer', 'CarrerController@index');

	Route::post('employer/carrer-page-update', 'CarrerController@carrerPageSave');

	Route::post('agency/profile-update', 'OtherUserController@agencyProfileUpdate');
	Route::post('agency/profile-image/update', 'OtherUserController@agencyProfileImageUpdate');

	Route::post('franchise/profile-update', 'OtherUserController@franchiseProfileUpdate');
	Route::post('franchise/profile-image/update', 'OtherUserController@franchiseProfileImageUpdate');

	Route::post('college/profile-update', 'OtherUserController@collegeProfileUpdate');
	Route::get('employee/college-autocomplete-search', 'OtherUserController@getAutocompleteSearch');
	Route::get('employee/walkin-interview-details', 'JobsForController@getWalkinInterviewById');
	Route::get('employer/application-note-history', 'JobsForController@getApplicationHistory');

	// Internation jobs

	Route::get('employer/international-jobs', 'InternationalJobController@index');
	Route::post('employer/check-international-jobs/exist', 'InternationalJobController@checkSameInternationalJobExist');
	Route::get('employer/international-jobs/{id}', 'InternationalJobController@show');
	Route::post('employer/international-jobs/store', 'InternationalJobController@store');
	Route::put('employer/international-jobs/update/{id}', 'InternationalJobController@update');
	Route::delete('employer/international-jobs/delete/{id}', 'InternationalJobController@destroy');
	Route::get('employer/international-jobs/status-update/{id}', 'InternationalJobController@jobStatus');
	Route::post('employer/international-jobs/applied', 'InternationalJobController@appliedJob');
	Route::post('employer/international-jobs/saved', 'InternationalJobController@savedJob');

	Route::get('employee/international/applied-jobs', 'InternationalJobController@getAppliedJob');
	Route::get('employee/international/saved-jobs', 'InternationalJobController@getSavedJob');
	Route::get('employee/international/applied-saved-job/{id}', 'InternationalJobController@appliedSavedJob');
	Route::delete('employee/international/saved-job-remove/{id}', 'InternationalJobController@removeSavedJob');
	Route::get('international-job/published/{id}', 'InternationalJobController@jobPublished');
	Route::get('international-job/re-published/{id}', 'InternationalJobController@jobRePublished');

});
