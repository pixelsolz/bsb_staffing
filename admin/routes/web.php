<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('check-mongo', 'MongoRawController@checkUser');

Route::get('update-username', 'Admin\CronJobController@updateUserName');

Route::get('check-email/exist', 'Admin\UserManageController@checkEmailexist');

Route::get('check-cron', 'Admin\CronJobController@employerCompletedProfileSendMail');
Route::get('update-international/usertype', 'Admin\InternationalJobController@updateUserType');

Route::get('career/widget/{user_id}', 'api\CarrerController@carrerWidetData');

Route::get('/login', 'Admin\AuthController@showLogin')->name('admin.login');
Route::post('/login', 'Admin\AuthController@storeLogin');
Route::get('reset-password/{token}', 'api\AuthController@resetForm');
Route::post('reset-password', 'api\AuthController@resetPassword');

Route::get('/superadmin-create', 'Admin\AuthController@adminCreate');
Route::get('/insert-any', 'Admin\AuthController@insertAny');
Route::get('resume/download', 'api\CvManageController@employeeDownloadResumeLink');

//Route::resource('announcement-template', 'Admin\AnnoucementTemplateController');

Route::get('/import-execl', 'Admin\ExcelImportExportController@create');
Route::post('/import-excel-save', 'Admin\ExcelImportExportController@importExcel');
Route::get('/export-country', 'Admin\ExcelImportExportController@exportCountry');
Route::get('test-upload-file', 'Admin\CountryController@testUploadFile');
Route::group(['namespace' => 'Admin', 'middleware' => 'admin'], function () {
	Route::get('/logout', 'AuthController@logout');

	Route::get('/', 'DashboardController@index')->name('admin.dasboard');
	Route::get('city-ajax', 'UserManageController@getCityAjax');

	Route::get('/cms-manage', 'CmsManageController@index')->name('admin.cms-manage.index');
	Route::get('/cms-manage/create', 'CmsManageController@create')->name('admin.cms-manage.create');
	Route::post('/cms-manage', 'CmsManageController@store')->name('admin.cms-manage.store');
	Route::get('/cms-manage/{id}', 'CmsManageController@edit')->name('admin.cms-manage.edit');
	Route::put('/cms-manage/{id}', 'CmsManageController@update')->name('admin.cms-manage.update');
	Route::delete('/cms-manage/{id}', 'CmsManageController@destroy')->name('admin.cms-manage.delete');

	Route::get('/user-manage/other-user', 'UserManageController@index')->name('admin.user-manage.index')->middleware('admin.roles:user-list');
	Route::get('/user-manage/franchise', 'UserManageController@index')->name('admin.user-manage.franchise')->middleware('admin.roles:user-list');
	Route::get('/user-manage/agency', 'UserManageController@index')->name('admin.user-manage.agency')->middleware('admin.roles:user-list');
	Route::get('/user-manage/college', 'UserManageController@index')->name('admin.user-manage.college')->middleware('admin.roles:user-list');
	Route::get('/user-manage/trainer', 'UserManageController@index')->name('admin.user-manage.trainer')->middleware('admin.roles:user-list');
	Route::get('/user-manage/employee', 'UserManageController@employeeUser')->name('admin.user-manage.employee');
	Route::get('/user-manage/employer', 'UserManageController@employerUser')->name('admin.user-manage.employer');
	Route::get('/user-manage/create', 'UserManageController@create')->name('admin.user-manage.create')->middleware('admin.roles:user-create');
	Route::get('/user-manage/create/employee', 'UserManageController@createEmployee')->name('admin.user-manage.create-employee');
	Route::get('/user-manage/create/employer', 'UserManageController@createEmployer')->name('admin.user-manage.create-employer');
	Route::get('/user-manage/edit/employee/{id}', 'UserManageController@editEmployee')->name('admin.user-manage.edit-employee');
	Route::get('/user-manage/edit/employer/{id}', 'UserManageController@editEmployer')->name('admin.user-manage.edit-employer');
	Route::post('/user-manage', 'UserManageController@store')->name('admin.user-manage.store')->middleware('admin.roles:user-create');
	Route::get('/user-manage/{id}', 'UserManageController@edit')->name('admin.user-manage.edit')->middleware('admin.roles:user-edit');
	Route::put('/user-manage/{id}', 'UserManageController@update')->name('admin.user-manage.update')->middleware('admin.roles:user-edit');
	Route::delete('/user-manage/{id}', 'UserManageController@destroy')->name('admin.user-manage.delete')->middleware('admin.roles:user-delete');
	Route::get('employee/saved-job/{id}', 'UserManageController@getEmployeeSavedJob');

	Route::get('user-manage/{id}/{user_type}', 'UserManageController@show')->name('admin.user-manage.show');

	Route::get('employer/job/status-change', 'UserManageController@employerJobStatusChange');
	Route::get('/user-manage/show/employer/{id}', 'UserManageController@showEmployer')->name('admin.user-manage.show_employer');
	Route::get('/user-manage/show/employee/{id}', 'UserManageController@showEmployee')->name('admin.user-manage.show_employee');

	Route::get('user/all/deleted-user', 'UserManageController@showDeletedUser')->name('admin.user-manage.deleted-user');

	Route::get('user-manage/restore-user/{id}', 'UserManageController@restoreDeletedUser')->name('admin.user-manage.restore');

	Route::get('user-manage/trainer/schedule/{trainer}/{type}', 'UserManageController@getTrainerSchedule')->name('admin.user-trainer.schedule');

	Route::get('staff/list', 'UserManageController@showAllInternalEmployee')->name('admin.user-manage.list.internal-employee');
	Route::get('staff/create', 'UserManageController@createInternalEmployee')->name('admin.user-manage.create.internal-employee');
	Route::post('staff/store', 'UserManageController@storeInternalEmployee')->name('admin.user-manage.internal-employee.store');
	Route::get('staff/edit/{id}', 'UserManageController@editInternalEmployee')->name('admin.user-manage.internal-employee.edit');
	Route::get('staff/view/{id}', 'UserManageController@showInternalEmployee')->name('admin.user-manage.internal-employee.show');
	Route::put('staff/update/{id}', 'UserManageController@updateInternalEmployee')->name('admin.user-manage.internal-employee.update');
	Route::delete('/staff/govtid/{id}', 'UserManageController@internalEmpdestroy')->name('admin.internal-emp-govtid.delete');

	Route::get('/role', 'RoleController@index')->name('admin.role.index');
	Route::get('/role/create', 'RoleController@create')->name('admin.role.create');
	Route::post('/role', 'RoleController@store')->name('admin.role.store');
	Route::get('/role/{id}', 'RoleController@edit')->name('admin.role.edit');
	Route::put('/role/{id}', 'RoleController@update')->name('admin.role.update');
	Route::delete('/role/{id}', 'RoleController@destroy')->name('admin.role.delete');

	//Benefit Manage

	Route::get('/benefit', 'BenefitController@index')->name('admin.benefit-manage.index');
	Route::get('/benefit/create', 'BenefitController@create')->name('admin.benefit-manage.create');
	Route::post('/benefit', 'BenefitController@store')->name('admin.benefit-manage.store');
	Route::get('/benefit/{id}', 'BenefitController@edit')->name('admin.benefit-manage.edit');
	Route::put('/benefit/{id}', 'BenefitController@update')->name('admin.benefit-manage.update');
	Route::delete('/benefit/{id}', 'BenefitController@destroy')->name('admin.benefit-manage.delete');

	//country
	Route::get('/country-manage', 'CountryController@index')->name('admin.country-manage.index');
	Route::get('/country-manage/create', 'CountryController@create')->name('admin.country-manage.create');
	Route::post('/country-manage', 'CountryController@store')->name('admin.country-manage.store');
	Route::get('/country-manage/{id}', 'CountryController@edit')->name('admin.country-manage.edit');
	Route::put('/country-manage/{id}', 'CountryController@update')->name('admin.country-manage.update');

	//Banner Image
	Route::get('/banner-manage', 'BannerController@index')->name('admin.banner-manage.index');
	Route::get('/banner-manage/create', 'BannerController@create')->name('admin.banner-manage.create');
	Route::post('/banner-manage', 'BannerController@store')->name('admin.banner-manage.store');
	Route::get('/banner-manage/{id}', 'BannerController@edit')->name('admin.banner-manage.edit');
	Route::put('/banner-manage/{id}', 'BannerController@update')->name('admin.banner-manage.update');
	Route::delete('/banner-manage/{id}', 'BannerController@destroy')->name('admin.banner-manage.delete');

	//Service Package
	Route::get('/service-package', 'ServicePackageController@index')->name('admin.service-package.index');
	Route::get('/service-package/create', 'ServicePackageController@create')->name('admin.service-package.create');
	Route::post('/service-package', 'ServicePackageController@store')->name('admin.service-package.store');
	Route::get('/service-package/{id}', 'ServicePackageController@edit')->name('admin.service-package.edit');
	Route::put('/service-package/{id}', 'ServicePackageController@update')->name('admin.service-package.update');

	// City
	Route::get('/city-manage', 'CityController@index')->name('admin.city-manage.index');
	Route::get('/city-manage/create', 'CityController@create')->name('admin.city-manage.create');
	Route::post('/city-store', 'CityController@store')->name('admin.city-manage.store');
	Route::get('/city-manage/edit/{id}', 'CityController@edit')->name('admin.city-manage.edit');
	Route::put('/city-update/{id}', 'CityController@update')->name('admin.city-manage.update');

	// Application Phase
	Route::get('/application-phase', 'ApplicationPhaseController@index')->name('admin.application-phase.index');
	Route::get('/application-phase/create', 'ApplicationPhaseController@create')->name('admin.application-phase.create');
	Route::post('/application-phase-store', 'ApplicationPhaseController@store')->name('admin.application-phase.store');
	Route::get('/application-phase/edit/{id}', 'ApplicationPhaseController@edit')->name('admin.application-phase.edit');
	Route::put('/application-phase/{id}', 'ApplicationPhaseController@update')->name('admin.application-phase.update');

	// Department
	Route::get('/department-manage', 'DepartmentController@index')->name('admin.department-manage.index');
	Route::get('/department-manage/create', 'DepartmentController@create')->name('admin.department-manage.create');
	Route::post('/department-store', 'DepartmentController@store')->name('admin.department-manage.store');
	Route::get('/department-manage/edit/{id}', 'DepartmentController@edit')->name('admin.department-manage.edit');
	Route::put('/department-update/{id}', 'DepartmentController@update')->name('admin.department-manage.update');
	Route::delete('/department-delete/{id}', 'DepartmentController@destroy')->name('admin.department-manage.delete');

	// Course
	Route::get('/course-manage', 'CourseController@index')->name('admin.course-manage.index');
	Route::get('/course-manage/create', 'CourseController@create')->name('admin.course-manage.create');
	Route::post('/course-store', 'CourseController@store')->name('admin.course-manage.store');
	Route::get('/course-manage/edit/{id}', 'CourseController@edit')->name('admin.course-manage.edit');
	Route::put('/course-update/{id}', 'CourseController@update')->name('admin.course-manage.update');
	Route::delete('/course-delete/{id}', 'CourseController@destroy')->name('admin.course-manage.delete');

	// Company Size
	Route::get('/companysize-manage', 'CompanySizeController@index')->name('admin.companysize-manage.index');
	Route::get('/companysize-manage/create', 'CompanySizeController@create')->name('admin.companysize-manage.create');
	Route::post('/companysize-store', 'CompanySizeController@store')->name('admin.companysize-manage.store');
	Route::get('/companysize-manage/edit/{id}', 'CompanySizeController@edit')->name('admin.companysize-manage.edit');
	Route::put('/companysize-update/{id}', 'CompanySizeController@update')->name('admin.companysize-manage.update');
	Route::delete('/companysize-delete/{id}', 'CompanySizeController@destroy')->name('admin.companysize-manage.delete');

	// Company Type
	Route::get('/companytype-manage', 'CompanyTypeController@index')->name('admin.companytype-manage.index');
	Route::get('/companytype-manage/create', 'CompanyTypeController@create')->name('admin.companytype-manage.create');
	Route::post('/companytype-store', 'CompanyTypeController@store')->name('admin.companytype-manage.store');
	Route::get('/companytype-manage/edit/{id}', 'CompanyTypeController@edit')->name('admin.companytype-manage.edit');
	Route::put('/companytype-update/{id}', 'CompanyTypeController@update')->name('admin.companytype-manage.update');
	Route::delete('/companytype-delete/{id}', 'CompanyTypeController@destroy')->name('admin.companytype-manage.delete');

	// Degree
	Route::get('/degree-manage', 'DegreeController@index')->name('admin.degree-manage.index');
	Route::get('/degree-manage/create', 'DegreeController@create')->name('admin.degree-manage.create');
	Route::post('/degree-store', 'DegreeController@store')->name('admin.degree-manage.store');
	Route::get('/degree-manage/edit/{id}', 'DegreeController@edit')->name('admin.degree-manage.edit');
	Route::put('/degree-update/{id}', 'DegreeController@update')->name('admin.degree-manage.update');
	Route::delete('/degree-delete/{id}', 'DegreeController@destroy')->name('admin.degree-manage.delete');

	// Industry
	Route::get('/industry-manage', 'IndustryController@index')->name('admin.industry-manage.index');
	Route::get('/industry-manage/create', 'IndustryController@create')->name('admin.industry-manage.create');
	Route::post('/industry-store', 'IndustryController@store')->name('admin.industry-manage.store');
	Route::get('/industry-manage/edit/{id}', 'IndustryController@edit')->name('admin.industry-manage.edit');
	Route::put('/industry-update/{id}', 'IndustryController@update')->name('admin.industry-manage.update');
	Route::delete('/industry-delete/{id}', 'IndustryController@destroy')->name('admin.industry-manage.delete');

	// Job Sector
	Route::get('/jobsector-manage', 'JobSectorController@index')->name('admin.jobsector-manage.index');
	Route::get('/jobsector-manage/create', 'JobSectorController@create')->name('admin.jobsector-manage.create');
	Route::post('/jobsector-store', 'JobSectorController@store')->name('admin.jobsector-manage.store');
	Route::get('/jobsector-manage/edit/{id}', 'JobSectorController@edit')->name('admin.jobsector-manage.edit');
	Route::put('/jobsector-update/{id}', 'JobSectorController@update')->name('admin.jobsector-manage.update');
	Route::delete('/jobsector-delete/{id}', 'JobSectorController@destroy')->name('admin.jobsector-manage.delete');

	// Language
	Route::get('/language-manage', 'LanguageController@index')->name('admin.language-manage.index');
	Route::get('/language-manage/create', 'LanguageController@create')->name('admin.language-manage.create');
	Route::post('/language-store', 'LanguageController@store')->name('admin.language-manage.store');
	Route::get('/language-manage/edit/{id}', 'LanguageController@edit')->name('admin.language-manage.edit');
	Route::put('/language-update/{id}', 'LanguageController@update')->name('admin.language-manage.update');
	Route::delete('/language-delete/{id}', 'LanguageController@destroy')->name('admin.language-manage.delete');

	//User Entity
	Route::get('/userentity-manage', 'UserEntityController@index')->name('admin.userentity-manage.index');
	Route::get('/userentity-manage/create', 'UserEntityController@create')->name('admin.userentity-manage.create');
	Route::post('/userentity-store', 'UserEntityController@store')->name('admin.userentity-manage.store');
	Route::get('/userentity-manage/edit/{id}', 'UserEntityController@edit')->name('admin.userentity-manage.edit');
	Route::put('/userentity-update/{id}', 'UserEntityController@update')->name('admin.userentity-manage.update');
	Route::delete('/userentity-delete/{id}', 'UserEntityController@destroy')->name('admin.userentity-manage.delete');

	//Other Master
	Route::get('/other-master-manage', 'OtherMasterController@index')->name('admin.other-master-manage.index');
	Route::get('/other-master-manage/create', 'OtherMasterController@create')->name('admin.other-master-manage.create');
	Route::post('/other-master-store', 'OtherMasterController@store')->name('admin.other-master-manage.store');
	Route::get('/other-master-manage/edit/{id}', 'OtherMasterController@edit')->name('admin.other-master-manage.edit');
	Route::put('/other-master-update/{id}', 'OtherMasterController@update')->name('admin.other-master-manage.update');
	Route::delete('/other-master-delete/{id}', 'OtherMasterController@destroy')->name('admin.other-master-manage.delete');

	// Attribute Manage
	Route::get('/attribute-manage', 'AttributeController@index')->name('admin.attribute-manage.index');
	Route::get('/attribute-manage/create', 'AttributeController@create')->name('admin.attribute-manage.create');
	Route::post('/attribute-master-store', 'AttributeController@store')->name('admin.attribute-manage.store');
	Route::get('/attribute-manage/edit/{id}', 'AttributeController@edit')->name('admin.attribute-manage.edit');
	Route::put('/attribute-update/{id}', 'AttributeController@update')->name('admin.attribute-manage.update');
	Route::delete('/attribute-delete/{id}', 'AttributeController@destroy')->name('admin.attribute-manage.delete');

	// Templates

	Route::get('email-template/list', 'TemplateManageController@employerEmailTemplates');
	Route::get('email-template/edit/{id}', 'TemplateManageController@editEmailTemplate');
	Route::put('email-template/update/{id}', 'TemplateManageController@updateEmailTemplate');
	//Payment Configration
	Route::get('/payment-config', 'PaymentConfigController@index')->name('admin.payment-config.index');
	Route::get('payment-config/status-change', 'PaymentConfigController@paymentStatusChange');

	//Experience Master
	Route::get('/experience-manage', 'ExperienceController@index')->name('admin.experience-manage.index');
	Route::get('/experience-manage/create', 'ExperienceController@create')->name('admin.experience-manage.create');
	Route::post('/experience-store', 'ExperienceController@store')->name('admin.experience-manage.store');
	Route::get('/experience-manage/edit/{id}', 'ExperienceController@edit')->name('admin.experience-manage.edit');
	Route::put('/experience-update/{id}', 'ExperienceController@update')->name('admin.experience-manage.update');
	Route::delete('/experience-delete/{id}', 'ExperienceController@destroy')->name('admin.experience-manage.delete');
	//Announcement benifits email template
	Route::get('announcement-template/list-all', 'AnnoucementTemplateController@getAllAnnoucement')->name('admin.announcement-template.list-all');

	Route::get('announcement-template/create', 'AnnoucementTemplateController@create')->name('admin.announcement-template.create');
	Route::post('announcement-template/store', 'AnnoucementTemplateController@store')->name('admin.announcement-template.store');
	Route::get('announcement-template/edit/{id}', 'AnnoucementTemplateController@edit')->name('admin.announcement-template.edit');
	Route::put('announcement-template/update/{id}', 'AnnoucementTemplateController@update')->name('admin.announcement-template.update');

	//job controller
	Route::get('employee/applied-job', 'JobController@getEmployeeAppliedJob')->name('admin.employee.applied-job');
	Route::get('employee/applied-job/show/{id}', 'JobController@employeeAppliedJobview')->name('admin.employee.applied-job.show');
	Route::get('employer/posted-job', 'JobController@employerJobPost')->name('admin.employer.posted-job');
	Route::get('employer/posted-job-edit/{id}', 'JobController@editJob')->name('admin.employer.posted-job-edit');
	Route::put('employer/posted-job-update/{id}', 'JobController@updateJob')->name('admin.employer.posted-job-update');
	Route::get('employer/job-application-received', 'JobController@employerJobApplicationReceived');

	Route::get('employee/applied-international-job', 'InternationalJobController@getEmployeeAppliedInternationalJob')->name('admin.employee.applied-international-job');
	Route::get('employer/posted-international-job-edit/{id}', 'InternationalJobController@editJob')->name('admin.employer.posted-international-job-edit');
	Route::put('employer/posted-international-job-update/{id}', 'InternationalJobController@updateJob')->name('admin.employer.posted-international-job-update');
	Route::get('employer/international-job', 'InternationalJobController@employerInternationalPost')->name('admin.employer.international-posted-job');
	Route::get('employer/international-application-received', 'InternationalJobController@employerInternationalJobApplicationReceived')->name('admin.employer.international-application-recived');
	Route::get('employee/international-applied-job/show/{id}', 'InternationalJobController@employeeAppliedInternationalJobview')->name('admin.employee.applied-internationaljob.show');

	Route::get('employee/walking-applied-job/show/{id}', 'InternationalJobController@employeeAppliedWalkingJobview')->name('admin.employee.applied-walkingjob.show');

	Route::get('international-job/status-change', 'InternationalJobController@internationalJobStatusChange');

	Route::get('international-job/applicant/{type}', 'InternationalJobController@getInternationalJobApplicant');

	Route::get('domestic-job/applicant/{type}', 'InternationalJobController@getDomesicJobApplicant');

	// deleted jobs
	Route::get('deleted/jobs', 'JobController@employerDeletedJobs')->name('admin.deletd.jobs');
	Route::get('deleted/international-job', 'JobController@employerDeletedInternationalJobs')->name('admin.deletd.international-job');
	Route::get('deleted/walking', 'JobController@employerDeletedWalkings')->name('admin.deletd.walking');

	// expired jobs
	Route::get('expired/jobs', 'JobController@expiredJobs')->name('admin.expired.jobs');
	Route::get('expired/international-job', 'JobController@expiredInternationalJobs')->name('admin.expired.international-job');
	Route::get('expired/walking', 'JobController@expiredWalkings')->name('admin.expired.walking');

	// Draft jobs
	Route::get('draft/jobs', 'JobController@draftJobs')->name('admin.draft.jobs');
	Route::get('draft/international-job', 'JobController@draftInternationalJobs')->name('admin.draft.international-job');
	Route::get('draft/walking', 'JobController@draftWalkings')->name('admin.draft.walking');

});
/*Route::get('/', function () {
return view('welcome');
});
 */

/*Route::get('/internal-employee', function () {
echo "Hello Agent";
})->middleware('internal_employee');*/

Route::group(['namespace' => 'Admin', 'middleware' => 'internal_employee'], function () {
	Route::get('internal-dashboard', 'DashboardController@internalEmpDashboard')->name('internal-emp.dashboard');
	Route::get('user-list', 'InternalEmployeeController@index')->name('internal-emp.list');
});