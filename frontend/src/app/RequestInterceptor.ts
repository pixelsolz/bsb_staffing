import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse,HttpHeaders
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {Router, Event, NavigationStart} from '@angular/router';
import {Global} from './global';
import {AuthService} from './services/auth.service';
import {CommonService} from './services/common.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  exceptionLoding: boolean;
  userType: string;
  countApiCall: number=0;
  constructor(private global: Global, private router: Router,
    private authService: AuthService, private loadingBar: LoadingBarService, private commonService: CommonService) {
    this.router.events.subscribe((event : Event)=>{
       if(event instanceof NavigationStart) {
             if(event.url.includes('employer')){
               this.userType = 'employer';
             }else if(event.url.includes('college')){
               this.userType = 'college';
             }else if(event.url.includes('agency')){
               this.userType = 'agency';
             }else if(event.url.includes('franchise')){
               this.userType = 'franchise';
             }else if(event.url.includes('trainer')){
               this.userType = 'trainer';
             }else{
               this.userType = 'employee';
             }
        }
    });
    this.exceptionLoding = false;
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(req.url !='/api/common-data'){
      this.loadingBar.start();
    }
    
    const dupReq = req.url.includes('cv-search') ? req.clone({url: this.global.communityApiUrl +req.url}) : req.clone({url: this.global.apiUrl +req.url});
    return next.handle(dupReq).pipe(tap(evt => {
      if (evt instanceof HttpResponse) {
        if(req.url !='/api/common-data'){
          this.countApiCall +=1;
          if(this.countApiCall ==1){
            this.commonService.callOpenPage('1');
          }
        }
        this.loadingBar.complete();
      }
    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err);
        this.loadingBar.complete();
        if(err.status === 401){
          switch (err.error.error) {
            case "TOKEN_EXPIRED":
              this.handleUnauthorized();
              break;
            case "TOKEN_INVALID":
              this.handleUnauthorized();
              break;
            case "TOKEN_BLACKLISTED":
              this.handleUnauthorized();
              break;   
            case "UNAUTHORIZED_REQUEST":
              // code...
              break;          
            default:
              // code...
              break;
          }
        }
        if (err.error.error === 'token_invalid' || err.error.error === 'token_expired') {

        }
      }
    }));

  }

  handleUnauthorized(){
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('is_employer');

    if(this.userType ==='employee'){
    localStorage.getItem('job_preview')? localStorage.removeItem('job_preview'):'';
    localStorage.getItem('international_job_preview')? localStorage.removeItem('international_job_preview'):'';
    localStorage.getItem('advSearch')? localStorage.removeItem('advSearch'):'';
    localStorage.getItem('search-queries')? localStorage.removeItem('search-queries'):'';
    localStorage.getItem('cvSearch')? localStorage.removeItem('cvSearch'):'';
    localStorage.getItem('walk_in_preview')? localStorage.removeItem('walk_in_preview'):'';
        this.router.navigate(['']);
    }else{
        this.router.navigate([this.userType]);
    }
  }

}