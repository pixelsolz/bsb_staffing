import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
	{path: '', loadChildren:'./employee/employee.module#EmployeeModule'},
	{path:'employer', loadChildren:'./employeer/employeer.module#EmployeerModule'},
	{path:'agency',  loadChildren:'./recruitment-agency/recruitment-agency.module#RecruitmentAgencyModule'},
	{path:'franchise', loadChildren:'./franchise/franchise.module#FranchiseModule'},
	{path:'college', loadChildren:'./college-institute/college-institute.module#CollegeInstituteModule'},
	{path:'trainer', loadChildren:'./trainer/trainer.module#TrainerModule'},
	{path:'legal', loadChildren:'./legal/legal.module#LegalModule'},
	//{path: '**', component: PageNotFoundComponent}
	//{path:'mail', loadChildren:'./email/email.module#EmailModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
