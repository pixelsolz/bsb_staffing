import { Component , OnInit,  Renderer2, ElementRef, Renderer} from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader'; 
declare var $;
import { Router,Event as RouterEvent, ActivatedRoute, NavigationEnd,
 NavigationStart, NavigationCancel, NavigationError } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { CommonService } from './services/common.service';
import {Global} from './global';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend';
  currentUrl:string;
  showheader:boolean;
  isLoaded: boolean =false;
  routerUrl: any;
  openFooter: boolean = false;
  constructor(private ngxService: NgxUiLoaderService, private renderer: Renderer2, private commonService: CommonService,
   private el: ElementRef, private render: Renderer, private router: Router, private loadingBar: LoadingBarService,private global: Global) {
    this.showheader = true;
    /*let test = document.getElementsByTagName('section');
     this.renderer.addClass(test,'in-view');*/

     router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
      if (event instanceof NavigationEnd ) {
        if($('.modal').length){
          $('.modal').click();
        }
        //this.currentUrl = event.url;
        this.routerUrl = event.url;
        if(event.url.includes('career-page')){
          this.showheader= false;
        }else if(event.url.includes('career/widget')){
          this.showheader= false;
        }

        else if(event.url === '/'){
          this.showheader= false;
          //document.querySelector('link[href$="materialize.min.css"]').remove()
        }else{
          this.showheader= true;
          if(!event.url.includes('/dashboard')){
             if(!document.querySelector('link[href$="materialize.min.css"]')){
              this.loadCss('assets/css/materialize.min.css')
            }
          }
        }


      }
    })

     router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
      if(e.url.match(/\/$/)){
       this.renderer.addClass(document.body,'homeMap');
      }else if(e.url =='/employer'){
       this.renderer.addClass(document.body,'homeMap');
      }else if(e.url =='/dashboard'){
       this.renderer.addClass(document.body,'homeMap');
      }else if(e.url.includes('job-home')){
        this.renderer.addClass(document.body,'homeMap');
      } else{
        this.renderer.removeClass(document.body,'homeMap');
      }
    });

    this.commonService.isNotFound.subscribe(data=>{
      if(data=='not-found'){
        this.showheader = false;
      }
    });
  }

  public loadCss(url: string){
    const head = <HTMLDivElement> document.head;
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    head.appendChild(link);
  }

   ngOnInit(){
     this.commonService.showOpenPage.subscribe(value=>{
       if(value =='1'){
         this.isLoaded = true;
       }
       
     });

     this.commonService.openFooterMenu.subscribe(value=>{
       if(value ==1){
         this.openFooter = true;
       }
     });
     
     this.commonService.sendHeaderUrl(window.location.href);
      /*this.commonService.getAll('/api/common-data').subscribe(data=>{
         this.commonService.callCommonData(data);
       });*/

      this.commonService.getAll('/api/countries-industries').subscribe(data=>{
        this.commonService.callCountryData(data);
       });

      this.commonService.getAll('/api/get-all-cities').subscribe(data=>{
        this.commonService.callCityData(data);
       });

     setTimeout (() => {
       this.commonService.getAll('/api/common-data').subscribe(data=>{
         this.commonService.callCommonData(data);
       });
       this.isLoaded = true;
      }, 100);
   }

/*async  ngOnInit(){
    this.global.commonDataArray = await this.callCommonData();
    this.isLoaded = true;
    this.commonService.sendHeaderUrl(window.location.href);
    this.commonService.callCommonData(this.global.commonDataArray);
  }*/

callCommonData() {
  return new Promise((resolve, reject) => {
    $.getJSON({
      url: this.global.apiUrl +`/api/common-data`,
      success: resolve,
      error: reject
    });
  });
}

   navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      //this.ngxService.start();
      this.loadingBar.start();

    }
    if (event instanceof NavigationEnd) {
      //this.ngxService.stop();
      this.loadingBar.complete();
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loadingBar.complete();
    }
    if (event instanceof NavigationError) {
     this.loadingBar.complete();
    }
  }
}
