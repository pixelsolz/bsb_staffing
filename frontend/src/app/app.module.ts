import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FooterComponent } from './includes/footer/footer.component';
import { HeaderComponent } from './includes/header/header.component';
import { NavigationComponent } from './includes/navigation/navigation.component';
//import { DateInputDirective } from './directives/date-input.directive';
import {Global} from './global';
import {MainService} from './services/main.service';
import {CommonService} from './services/common.service';
import {AuthService} from './services/auth.service';
import {MessageService} from './services/message.service';
import {PagerService} from './services/pager.service';
import {LoginComponent} from './auth/login/login.component';
import {RegistrationComponent} from './auth/registration/registration.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthGuard} from './auth.guard';
import {ProfileCheckGuard} from './profile-check.guard';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { InputMultiValueComponent } from './component-directive/input-multi-value/input-multi-value.component';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AgmCoreModule } from '@agm/core';
//import { LanguagePipe } from './filters/language.pipe';




@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    //DateInputDirective,
    
    
   
    InputMultiValueComponent,
    PageNotFoundComponent,
    //LanguagePipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    //NgxUiLoaderModule,
    LoadingBarModule,
    //NgbModule.forRoot()
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8lof8u7i8wENtO9PsabM5XYAId8D7Nbg'
    })
  ],
  providers: [Global, MainService, CommonService, AuthService, MessageService,PagerService,AuthGuard,ProfileCheckGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
