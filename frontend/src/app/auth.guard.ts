import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Global } from './global';
import { CommonService } from './services/common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  previousUrl: string;
  nextUrl: string;
  isEmployer: any;
  useRole: string;
  userDetail: any;
  constructor(private authService: AuthService, private router: Router, private global: Global,
   private commonservice: CommonService) {
    this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((e: any) => {
        localStorage.setItem('previousUrl', this.previousUrl);
        this.previousUrl = e.url;
      });

      this.commonservice.checkEmployer.subscribe((check)=>{
          this.isEmployer =localStorage.getItem('is_employer');
          this.useRole = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).user_role:'';
          this.userDetail = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).user_detail:'';
      });
      
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isUserLoginIn() && this.isEmployer ==1) {
        if(state.url.includes('employer')){
          return true;
        }else{
          this.router.navigate([localStorage.getItem('previousUrl')]);
          return true;
        }
      
    }
    else if(this.authService.isUserLoginIn() && this.isEmployer =='0'){
      if(state.url.includes('employer')){
          return false;
        }else{
          return true;
        }
    }
    else if(this.authService.isUserLoginIn() && this.useRole =='college'){
      if(state.url.includes('college')){
          return true;
        }else{
          return false;
        }
    }
    else if(this.authService.isUserLoginIn() && this.useRole =='agency'){
      if(state.url.includes('agency')){
          return true;
        }else{
          return false;
        }
    }
    else if(this.authService.isUserLoginIn() && this.useRole =='franchise'){
      if(state.url.includes('franchise')){
          return true;
        }else{
          return false;
        }
    }
    else if(this.authService.isUserLoginIn() && this.useRole =='trainer'){
      if(state.url.includes('trainer')){
          return true;
        }else{
          return false;
        }
    }
    else{
      if(state.url =='/'){
        return true;
      }else if(state.url =='/employer'){
        return true;
      }else if(state.url.includes('college')){
        return true;
      }else if(state.url.includes('agency')){
        return true;
      }else if(state.url.includes('franchise')){
        return true;
      }else{
        this.router.navigate(['/']);
        return false;
      }
    }
  }
}
