import { Component, OnInit,  Output, EventEmitter, AfterViewInit, ViewChild, HostListener,  ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import { Meta, Title } from '@angular/platform-browser';
import * as moment from 'moment';

@Component({
  selector: 'app-agent-signup',
  templateUrl: './agent-signup.component.html',
  styleUrls: ['./agent-signup.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('700ms ease-in', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('700ms ease-in', style({ transform: 'translateY(100%)' }))
      ])
    ]),
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    ),
    trigger('fade', [
      state('void', style({ opacity: 0 })),
      transition(':enter, :leave', [
        animate(700)
      ])
    ])

  ]
})
export class AgentSignupComponent implements OnInit {

 @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if($(event.target).hasClass('auto_comp_search')){
    }else{
      this.showDesignation = false;
    }
  }

  registrationForm: FormGroup;
  countries: any=[];
  cities: any=[];
  job_titles: any=[];
  logoImage: string;
  validation_error: any;
  coutryWiseCity: any=[];
  years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() openLoginEvent = new EventEmitter<boolean>();
  showloader:boolean = false;
  prevEighteenYearDate:any;
  showdesignation:boolean =false;
  allJobTitle: any=[]
  showDesignation:boolean = false;
  storeMultiSelectRegistration :any ={
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[]
  };
  countryArray: any=[];
  cityArray: any=[];  
  countryDivSign2:boolean =false;
  cityDivSign2:boolean=false

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private title: Title,private eRef: ElementRef,
    private meta: Meta) { 
    this.prevEighteenYearDate = moment().subtract(18, 'years').calendar()
  }

  ngOnInit() {
    //setTimeout(()=>{this.showdesignation = true},500)

  	this.registrationForm = this.fb.group({
  		comp_name:['', [Validators.required]],
      comp_website:[''],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		registration_certificate: ['', [Validators.required]],
      govt_id:[''],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]], 
      date_of_birth:['', [Validators.required]],
      gender:['', [Validators.required]],		
      name_account_holder:'',     
      bank_name:'',     
      account_no:'',     
      swift_code:'',     
      ifsc_code:'',     
      bank_portal_address:'',
      declaration_user:''    
  	});

    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value.length){
        let country = this.countries.find(obj=>obj._id == value[0]);
        this.commonService.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){ 
            this.coutryWiseCity =  {"":data.data}
          }
        })
      }else{
        this.coutryWiseCity =[]
        this.storeMultiSelectRegistration.city_value.splice(0, 1);
        this.storeMultiSelectRegistration.city_name.splice(0, 1);
        this.registrationForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });        
      }

    });

    /*$(document).on('mousedown','#contact_person_designation',()=>{
      this.showdesignation = true
      if(this.allJobTitle.length !== this.job_titles.length){
          this.allJobTitle.slice(10).forEach(data=>{
            this.job_titles.push(data)
          })
      }
    })*/
     
    this.asyncInit();
    this.title.setTitle('Recruitment Agency Registration|Recruiting Agent Form - bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Are you an Employer or Recruitment Agency? Registered your Recruitment Agency on Bsbstaffing and Fulfill your Employment Requirements. Registered Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'Recruitment Agency Registration' });
  }

  handleClose(){
    this.showdesignation = false
    $('#contact_person_designation').html('')
    this.closeEvent.emit(false)
  }

  handleLogin(){
    this.openLoginEvent.emit(true)
  }


  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.countryArray = {"":data['countries']};
      }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
       this.allJobTitle = data['job_titles'];
       this.job_titles = data['job_titles'].slice(0,10);
      }
    });
  }

  searchAutoComplete(key){
    if(key.target.value){
      this.showDesignation = true
      this.job_titles = this.allJobTitle.filter(obj=>obj.job_title.toLowerCase().startsWith(key.target.value)) 
    }else{
      this.showDesignation = false
    }
   
  }

  autoCompleteSearch(data){
    this.registrationForm.get('contact_person_designation').setValue(data.job_title)
    this.showDesignation = false
  }

  submitForm(){
    this.showloader = true;
    this.registrationForm.get('contact_person_email').setValue(this.registrationForm.get('contact_person_email').value.trim())
    let formValue = this.formDataSet(this.registrationForm);
  	this.commonService.create('/api/other-registration', formValue)
  		.subscribe((data)=>{
        this.showloader = false;
  			if(data.status ==200){
          this.handleClose()
  				this.message.success(data.status_text);
          let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: '<p>You have registered successfully.<br/> After verified from admin you can login to system by email and password.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 15000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  //this.router.navigate(['/agency/login']);
                }
              });
  				
  			}else if(data.status ==422){
          this.validation_error = data.error;
          console.log(this.validation_error);
        }
  		},(error)=>{
        this.showloader = false;
      });
  }

  formDataSet(form){
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
      if(key == 'declaration_user' && !form.get(key).value){
        formData.append(key, '');
      }else if(key == 'country_id'){
          formData.append(key, form.get(key).value[0]);
      }else if(key == 'city_id'){
        formData.append(key, form.get(key).value[0]);
      }else{
       formData.append(key, form.get(key).value);
     }
   });
   formData.append('user_type', 'agency');

   return formData;
 }

 localCertificateUpload(e){
 	this.registrationForm.get('registration_certificate').setValue(e.target.files[0]);
 }


 govtCertificateUpload(e){
 	this.registrationForm.get('govt_id').setValue(e.target.files[0]);
 }

  removeMultiSelectSign2(indx, type){
    switch (type) {
      case "country":
       this.storeMultiSelectRegistration.country_value.splice(indx, 1);
       this.storeMultiSelectRegistration.country_name.splice(indx, 1);
       this.registrationForm.patchValue({
        country_id: this.storeMultiSelectRegistration.country_value
       });

        break;
      case "city":
       this.storeMultiSelectRegistration.city_value.splice(indx, 1);
       this.storeMultiSelectRegistration.city_name.splice(indx, 1);
       this.registrationForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
        break; 
      default:
        // code...
        break;
    }    
  }

  getOnchangeEventSign2(type){
    if(type =='country'){
      this.countryDivSign2 = true;
    }else if(type =='city'){
      this.cityDivSign2 = true;
    }
  }

  getChangeSign2(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='country'){
      this.registrationForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.registrationForm.patchValue({
      city_id: e.storedValue
      });
    }
  }

  checkmultiselectSign2(e){
    if(e.field_name =='country'){
      this.countryDivSign2 = e.status;
    }else if(e.field_name =='city'){
      this.cityDivSign2 = e.status;
    }
  }

}
