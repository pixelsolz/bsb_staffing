import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { LoginComponent } from './login/login.component';
//import { RegistrationComponent } from './registration/registration.component';
//import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { OtherLoginComponent } from './other-login/other-login.component';
import { OtherForgotPasswordComponent } from './other-forgot-password/other-forgot-password.component';
import { OtherResetPasswordComponent } from './other-reset-password/other-reset-password.component';

const routes: Routes = [
	{path: 'login',component:OtherLoginComponent},
	{path: 'forgot-password',component:OtherForgotPasswordComponent},
	{path: 'reset-password',component:OtherResetPasswordComponent},
	//{path: 'registration',component:RegistrationComponent},
	//{path: 'forget-password',component:ForgetPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
