import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';

import { AuthRoutingModule } from './auth-routing.module';
//import { LoginComponent } from './login/login.component';
//import { RegistrationComponent } from './registration/registration.component';
//import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { OtherLoginComponent } from './other-login/other-login.component';
import { OtherForgotPasswordComponent } from './other-forgot-password/other-forgot-password.component';
import { OtherResetPasswordComponent } from './other-reset-password/other-reset-password.component';



@NgModule({
  declarations: [
 // LoginComponent,
  // RegistrationComponent,
    //ForgetPasswordComponent,
 OtherLoginComponent,
 OtherForgotPasswordComponent,
 OtherResetPasswordComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    CoreModule
  ]
})
export class AuthModule { }
