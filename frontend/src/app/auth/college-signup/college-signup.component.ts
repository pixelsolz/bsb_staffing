import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import { Meta, Title } from '@angular/platform-browser';
import * as moment from 'moment';
import * as _ from 'lodash'; 
@Component({
  selector: 'app-college-signup',
  templateUrl: './college-signup.component.html',
  styleUrls: ['./college-signup.component.css']
})
export class CollegeSignupComponent implements OnInit {

  registrationForm: FormGroup;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() openLoginEvent = new EventEmitter<boolean>();
  prevEighteenYearDate:any;
  countries: any=[];
  cities: any=[];
  logoImage: string;
  gallerie: any={
    previews:[],
    images:[]
  };
  gallerie_previews: any=[];
  validation_error: any;
  coursesData: any=[];
  courseDiv: boolean=false;
  noSearchSelect: any={
    course_value:[],
    course_name:[]
  };
  storeMultiSelect: any={
    course_value:[],
    course_name:[]
  };
  coutryWiseCity: any=[];
  collegeUploadLogoImage: any;
  showloader:boolean = false;
  addressLatLng:any={
    lat:'',
    lng:''
  }
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About your College/ University/ Institution',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  specialconfig: any = {
    //height: '200px',
    placeholder: 'Special Note for Employer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  missionconfig: any = {
    //height: '200px',
    placeholder: 'Mission',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  visionconfig: any = {
    //height: '200px',
    placeholder: 'Vision',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  storeMultiSelectRegistration :any ={
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[]
  };
  countryArray: any=[];
  cityArray: any=[];  
  countryDivSign2:boolean =false;
  cityDivSign2:boolean=false
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private title: Title,
    private meta: Meta) { 
     this.prevEighteenYearDate = moment().subtract(18, 'years').calendar()
  }


  ngOnInit() {
    var regex = /(<([^>]+)>)/ig
    var max = new Date().getFullYear(),
    min = max - 70,
    max = max ;
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    }   

    this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });

  	this.registrationForm = this.fb.group({
  		name:['', [Validators.required]],
  		website:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		logo: ['', [Validators.required]],
      courses:[''],
  		total_student:['', [Validators.required]],
  		total_student_place_per_year:['', [Validators.required]],
  		about_college:['',[Validators.required]],
  		special_note_employer:['',[Validators.required]],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]],
      date_of_birth:['', [Validators.required]],
      gender:['', [Validators.required]],  
      name_account_holder:'',     
      bank_name:'',     
      account_no:'',     
      swift_code:'',     
      ifsc_code:'',     
      bank_postal_address:'',
      mission:'',
      vision:'',
      institute_video:'', 		
      declaration_user:'',    
      type_of_listing:'',    
  	});
    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value.length){
        let country = this.countries.find(obj=>obj._id == value[0]);
        this.commonService.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){ 
            this.coutryWiseCity =  {"":data.data}
          }
        })
      }else{
        this.coutryWiseCity =[]
        this.storeMultiSelectRegistration.city_value.splice(0, 1);
        this.storeMultiSelectRegistration.city_name.splice(0, 1);
        this.registrationForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });        
      }

    });

    /*this.registrationForm.get('about_college').valueChanges.subscribe(value =>{
      if(value && value.replace(regex,'').replace(/ /g,'').length > 1000){
        this.message.error('Max character should be within 1000');
        //this.registrationForm.get('about_college').setValue('')
      }
    })*/

    this.asyncInit();
    this.title.setTitle('College, University & Institution Registration Form - bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Are you College, University and Institution? Create your College, University and Institution account on bsbstaffing.com.Registered Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'College/ University/ Institution Registration' });
  }

  handleClose(){
    this.closeEvent.emit(false)
  }

  handleLogin(){
    this.openLoginEvent.emit(true)
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.countryArray = {"":data['countries']};
         this.coursesData = {"":_.uniqBy(data['courses'][""],(e)=>{
              return e.name
            })};
        //this.coursesData = data['courses'];
      }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
    });
  }

  submitForm(){
    this.showloader = true;
    var regex = /(<([^>]+)>)/ig
    if(this.registrationForm.get('institute_video').value){
      if(!this.registrationForm.get('institute_video').value.match('^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+')){
        this.message.error('Please enter proper youtube link');
        this.showloader = false
        return false;
      }
    }
    this.registrationForm.get('contact_person_email').setValue(this.registrationForm.get('contact_person_email').value.trim())
    let formValue = this.formDataSet(this.registrationForm);
  	this.commonService.create('/api/college-registration', formValue)
  		.subscribe((data)=>{  
         this.showloader = false			
        if(data.status ==200){
          this.handleClose()
          this.message.success(data.status_text);
          let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: '<p>You have registered successfully.<br/> After verified from admin you can login to system by email and password.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 15000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  //this.router.navigate(['/college/login']);
                }
              });
          
        }else if(data.status ==422){
          this.validation_error = data.error;
          this.registrationForm.get('courses').setValue(this.noSearchSelect.course_name);
        }
  		},(error)=>{
        this.showloader = false  
      });
  }

  logoUpload(e){
    const file = e.target.files[0];
    this.registrationForm.get('logo').setValue(e.target.files[0]);
    const reader = new FileReader();
      reader.onload = (event:any) => {
        this.collegeUploadLogoImage = event.target.result;
      }
      reader.readAsDataURL(file);
  }
  removelogo(){
    this.registrationForm.get('logo').setValue('');
    this.collegeUploadLogoImage ='';
  }

  galleryUpload(e){
    const file = e.target.files[0];
    this.gallerie.images.push(file);
    const reader = new FileReader();
      reader.onload = (event:any) => {
        this.gallerie.previews.push(event.target.result);
      }
      reader.readAsDataURL(file);
  }

 removeGalleryImage(index){
   this.gallerie.previews.splice(index, 1);
   this.gallerie.images.splice(index, 1);
 }

 formDataSet(form){
   var regex = /(<([^>]+)>)/ig
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
      if(key == 'declaration_user' && !form.get(key).value){
        formData.append(key, '');
      }else if(key == 'about_college'){
        formData.append('about_college_copy', form.get(key).value);
        formData.append(key, form.get(key).value.replace(regex,'').replace(/ /g,''));     
      }
      else if(key == 'special_note_employer'){
        formData.append('special_note_employer_copy', form.get(key).value);
        formData.append(key, form.get(key).value.replace(regex,'').replace(/ /g,''));     
      }
      else if(key == 'mission'){
        formData.append('mission_copy', form.get(key).value);
        formData.append(key, form.get(key).value.replace(regex,'').replace(/ /g,''));     
      }
      else if(key == 'vision'){
        formData.append('vision_copy', form.get(key).value);
        formData.append(key, form.get(key).value.replace(regex,'').replace(/ /g,''));     
      }else if(key == 'country_id'){
          formData.append(key, form.get(key).value[0]);
      }else if(key == 'city_id'){
        formData.append(key, form.get(key).value[0]);
      }
      else{
        if(key == 'address'){
          formData.append(key, form.get(key).value);
          if( form.get(key).value !==''){
            formData.append('lat', this.addressLatLng.lat);
            formData.append('lng', this.addressLatLng.lng);
          }                    
        }
       formData.append(key, form.get(key).value);
     }
   });
   if(this.gallerie.images.length){
       this.gallerie.images.forEach((data, i)=>{
         formData.append('gallery_' + i, data);
       });
       formData.append('gallery_length', this.gallerie.images.length);
   }
   return formData;
 }

  noSearchGetchange(e){
    console.log(e);
     if(e.event){
      let index = this.noSearchSelect.course_value.indexOf(e.value);
      if(index ==-1){
        this.noSearchSelect.course_value.push(e.value);
        this.noSearchSelect.course_name.push(e.name); 
      }
    }else{
      let index = this.noSearchSelect.course_value.indexOf(e.value);
      if(index !=-1){
        this.noSearchSelect.course_value.splice(index,1);
        this.noSearchSelect.course_name.splice(index,1);
      }
    }
    this.registrationForm.patchValue({
      courses: this.noSearchSelect.course_value
      });
  }

  removeMultiSelect(indx, type){
    switch (type) { 
      case "course":
        this.noSearchSelect.course_value.splice(indx,1);
        this.noSearchSelect.course_name.splice(indx,1);
       this.registrationForm.patchValue({
          courses: this.noSearchSelect.course_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  getOnchangeEvent(){
    this.courseDiv = true;
  }
  checkmultiselect(e){
    console.log(e);
    if(e.field_name =='courses'){
      this.courseDiv = e.status;
    }
    
  }
 
 handleAddres =(e)=>{
   this.commonService.getLatLong(e.target.value).then(res=>{
     if(res.status === "OK"){
       console.log(res.results[0]['geometry']['location']['lat'])
       this.addressLatLng.lat = res.results[0]['geometry']['location']['lat']
       this.addressLatLng.lng = res.results[0]['geometry']['location']['lng']
       //console.log(res.results[0]['geometry']['location'])
     }
     
   })
 }

   removeMultiSelectSign2(indx, type){
    switch (type) {
      case "country":
       this.storeMultiSelectRegistration.country_value.splice(indx, 1);
       this.storeMultiSelectRegistration.country_name.splice(indx, 1);
       this.registrationForm.patchValue({
        country_id: this.storeMultiSelectRegistration.country_value
       });

        break;
      case "city":
       this.storeMultiSelectRegistration.city_value.splice(indx, 1);
       this.storeMultiSelectRegistration.city_name.splice(indx, 1);
       this.registrationForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
        break; 
      default:
        // code...
        break;
    }    
  }

  getOnchangeEventSign2(type){
    if(type =='country'){
      this.countryDivSign2 = true;
    }else if(type =='city'){
      this.cityDivSign2 = true;
    }
  }

  getChangeSign2(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='country'){
      this.registrationForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.registrationForm.patchValue({
      city_id: e.storedValue
      });
    }
  }

  checkmultiselectSign2(e){
    if(e.field_name =='country'){
      this.countryDivSign2 = e.status;
    }else if(e.field_name =='city'){
      this.cityDivSign2 = e.status;
    }
  }


}
