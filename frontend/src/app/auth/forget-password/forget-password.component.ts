import { Component, OnInit, Input ,Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $ :any;
import * as _ from 'lodash';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  forgetPasswordForm: FormGroup;
  showloader:boolean;
  isSubmit: boolean;
  @Output() forgotEvent = new EventEmitter<string>();
  @Input() callfromFooter: string
  constructor(private fb:FormBuilder, private global: Global,private authService: AuthService, 
    private router: Router, private message: MessageService) { 

  }

  ngOnInit() {
  	this.forgetPasswordForm = this.fb.group({
  		email:['', [Validators.required, Validators.email]],
      user_type: ''
  	});
  }

  send(){
    this.showloader = true;
    this.checkUrl();
    this.isSubmit = true;
     if(this.forgetPasswordForm.invalid){
      this.showloader = false;
      return;
    }
  	this.authService.create('/api/forget-password', this.forgetPasswordForm.value)
        .subscribe((data)=>{
          this.showloader = false;
          if(data.status ==200){
            this.isSubmit = false;
            $("#loginModal").find('.close').click();
            this.message.success('Password change link has been sent to your email');
          }else if(data.status ==401){
            this.message.error(data.msg);
          }
        }, (error)=>{this.showloader = false;});
  }

  showLogin(){
    this.forgotEvent.emit('1');
  }

  checkUrl(){
      if(window.location.href.includes('employer')){
          this.forgetPasswordForm.get('user_type').setValue('employer');
        }else{
          this.forgetPasswordForm.get('user_type').setValue('employee');
      }

    if(!_.isEmpty(this.callfromFooter)){
      this.forgetPasswordForm.get('user_type').setValue('employee');
    }

  }

}
