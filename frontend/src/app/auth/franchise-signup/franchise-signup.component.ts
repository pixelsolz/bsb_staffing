import { Component, OnInit ,  Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import { Meta, Title } from '@angular/platform-browser';
import * as moment from 'moment';

@Component({
  selector: 'app-franchise-signup',
  templateUrl: './franchise-signup.component.html',
  styleUrls: ['./franchise-signup.component.css']
})
export class FranchiseSignupComponent implements OnInit {

  registrationForm: FormGroup;
  countries: any=[];
  cities: any=[];
  logoImage: string;
  validation_error: any;
  coutryWiseCity: any=[];
  years:any=[];
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() openLoginEvent = new EventEmitter<boolean>();
  showloader:boolean = false;
  prevEighteenYearDate:any;
  storeMultiSelectRegistration :any ={
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[]
  };
  countryArray: any=[];
  cityArray: any=[];  
  countryDivSign2:boolean =false;
  cityDivSign2:boolean=false

  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private title: Title,
    private meta: Meta) {
      this.prevEighteenYearDate = moment().subtract(18, 'years').calendar()
   }

 ngOnInit() {

  	this.registrationForm = this.fb.group({
  		comp_name:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		registration_certificate: ['', [Validators.required]],
      govt_id:['', [Validators.required]],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]],
      date_of_birth:['', [Validators.required]],
      gender:['', [Validators.required]],  
      name_account_holder:[''],     
      bank_name:[''],     
      account_no:[''],     
      swift_code:[''],     
      ifsc_code:[''],     
      bank_portal_address:[''],
      declaration_user:[''] 		
  	});

    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value.length){
        let country = this.countries.find(obj=>obj._id == value[0]);
        this.commonService.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){ 
            this.coutryWiseCity =  {"":data.data}
          }
        })
      }else{
        this.coutryWiseCity =[]
        this.storeMultiSelectRegistration.city_value.splice(0, 1);
        this.storeMultiSelectRegistration.city_name.splice(0, 1);
        this.registrationForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });        
      }

    });

    this.asyncInit();
    this.title.setTitle('Franchise Registration|Franchise Form - bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Create your Franchise account on bsbstaffing.com. Registered Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'Franchise Registration' });
  }

  handleClose(){
    this.closeEvent.emit(false)
  }

  handleLogin(){
    this.openLoginEvent.emit(true)
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.countryArray = {"":data['countries']};
      }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);

    });
  }


   submitForm(){
    this.showloader = true;
    this.validation_error ='';
    this.registrationForm.get('contact_person_email').setValue(this.registrationForm.get('contact_person_email').value.trim())
    let formValue = this.formDataSet(this.registrationForm);
  	this.commonService.create('/api/other-registration', formValue)
  		.subscribe((data)=>{
        this.showloader = false;
  			if(data.status ==200){
          this.handleClose()
          this.message.success(data.status_text);
          let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: '<p>You have registered successfully.<br/> After verified from admin you can login to system by email and password.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 15000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  //this.router.navigate(['/franchise/login']);
                }
              });
          
        }else if(data.status ==422){
          this.validation_error = data.error;
        }
  		},(error)=>{
        this.showloader = false;
      });
  }



 formDataSet(form){
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
      if(key == 'declaration_user' && !form.get(key).value){
        formData.append(key, '');
      }else if(key == 'country_id'){
          formData.append(key, form.get(key).value[0]);
      }else if(key == 'city_id'){
        formData.append(key, form.get(key).value[0]);
      }else{
       formData.append(key, form.get(key).value);
     }
   });
   formData.append('user_type', 'franchise');
   return formData;
 }

 localCertificateUpload(e){
 	this.registrationForm.get('registration_certificate').setValue(e.target.files[0]);
 }


 govtCertificateUpload(e){
 	this.registrationForm.get('govt_id').setValue(e.target.files[0]);
 }


   removeMultiSelectSign2(indx, type){
    switch (type) {
      case "country":
       this.storeMultiSelectRegistration.country_value.splice(indx, 1);
       this.storeMultiSelectRegistration.country_name.splice(indx, 1);
       this.registrationForm.patchValue({
        country_id: this.storeMultiSelectRegistration.country_value
       });

        break;
      case "city":
       this.storeMultiSelectRegistration.city_value.splice(indx, 1);
       this.storeMultiSelectRegistration.city_name.splice(indx, 1);
       this.registrationForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
        break; 
      default:
        // code...
        break;
    }    
  }

  getOnchangeEventSign2(type){
    if(type =='country'){
      this.countryDivSign2 = true;
    }else if(type =='city'){
      this.cityDivSign2 = true;
    }
  }

  getChangeSign2(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='country'){
      this.registrationForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.registrationForm.patchValue({
      city_id: e.storedValue
      });
    }
  }

  checkmultiselectSign2(e){
    if(e.field_name =='country'){
      this.countryDivSign2 = e.status;
    }else if(e.field_name =='city'){
      this.cityDivSign2 = e.status;
    }
  }

}
