import { Component, OnInit,Input ,Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var $: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  register: string;
  showloader: boolean;
  isEmployer: boolean;
  isSubmit: boolean;
  user_type: string;
  showPassword:boolean = false;
  @Input() isApplied;
  @Output() loginEvent = new EventEmitter<string>();
  @Output() footerRegisterEvent = new EventEmitter<string>();
  @Input() callfromFooter: string
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
    this.register = '2';
    this.showloader = false;
    this.isSubmit = false;
    this.router.events.subscribe((event)=>{
      if(event instanceof NavigationStart){
        this.isEmployer = event.url.includes('employer')? true: false;
        if (this.isEmployer) {
          this.user_type = 'employer';
        } else {
          this.user_type = 'employee';
        }
      }
    });
    
   }

  ngOnInit() {
    if(!_.isEmpty(this.callfromFooter)){
      this.user_type = 'employee';
    }

  	this.loginForm = this.fb.group({
      user_type: [''],
  		//email: ['', [Validators.required,Validators.email, this.noWhitespaceValidator]],
      email: ['', [Validators.required]],
      password: ['', Validators.required]
  	});

  }

  ngOnDestroy() {
        //this.isApplied ='';
    }


  showRegistration(){
    this.loginEvent.emit(this.register);
  }

  showForgotPWD(){
     this.loginEvent.emit('3');
  }

  login(){
    //console.log(this.user_type);
  	this.showloader = true;
    this.isSubmit = true;
    if(this.loginForm.invalid){
      this.showloader = false;
      return;
    }
    this.loginForm.get('user_type').setValue(this.user_type);
    this.loginForm.get('email').setValue(this.loginForm.get('email').value.trim());
  	this.authService.userAuthentication('/api/login', this.loginForm.value)
  		.subscribe(data=>{
        this.showloader = false;
        this.isSubmit = false;
        if(data.status == 422){
          //this.message.error(data.error.email);
        }else if(data.status == 500){
          this.message.error(data.status_text);
        }else if(data.status == 401){
          this.message.error(data.status_text);
        }else{
          if(_.isEmpty(this.callfromFooter) && window.location.pathname.includes('employer') &&  !data.user.is_employer){
            this.message.error("Your credentials doesn't matches");
            return false
          }

          if(!_.isEmpty(this.callfromFooter)){
           this.footerRegisterEvent.emit('1');
          }
          this.loginForm.reset();
          $("#loginModal").find('.close').click();
          this.message.success('Successfully login');
          //console.log(data.user);
          localStorage.setItem('token', data.token);
          localStorage.setItem('user', JSON.stringify(data.user));
          localStorage.setItem('is_employer', data.user.is_employer? '1':'0');
          this.commonService.employer_check(1);
          this.commonService.checkCallLogin('login');
          if(data.user.is_employer){
            if(this.isApplied && this.isApplied.service_id){
              
              this.router.navigate(['employer/payment'],{queryParams:{service:this.isApplied.service_id}}); 
              return true;
            }else{
              this.router.navigate(['employer/dashboard']);
            }
            this.router.navigate(['employer/dashboard']);
          }else{
              this.commonService.getAll('/api/employee-profile').subscribe(res=>{
              if (res.status == 200) {
                let loginEmpProfile = res.data;
                if(this.isApplied && this.isApplied.jb_type){
              if(this.isApplied.jb_data.candidate_type =='2' && data.user.user_detail.gender == 2){
                this.message.error('This job only for male apply next job');
                this.router.navigate(['dashboard']);
                return false;
              }
              if(this.isApplied.jb_data.candidate_type =='3' && data.user.user_detail.gender == 1){
                this.message.error('This job only for female apply next job');
                this.router.navigate(['dashboard']);
                return false;
              }

              let emplyeePrefIndustry =  loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
              if(!this.isApplied.jb_data.job_title.includes(data.user.user_detail.profile_title) && !this.isApplied.jb_data.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
                this.message.error('as per your cv/ Resume this job not matching for you apply next job');
                this.router.navigate(['dashboard']);
                return false;
              }

              if(data.user.user_detail.total_experience_yr.value == 0 && this.isApplied.jb_data.min_experiance.value >0){
                this.message.error('as per your cv/ Resume this job not matching for you apply next job');
                this.router.navigate(['dashboard']);
                return false;
              }

              if(data.user.user_detail.total_experience_yr.value < this.isApplied.jb_data.min_experiance.value){
                this.message.error('This job post work experience not matching with your profile  apply next job');
                this.router.navigate(['dashboard']);
                return false;
              }

              if(this.isApplied.jb_data.employement_for.name =='Domestic Jobs' && this.isApplied.jb_data.country_name != data.user.user_detail.country.name){
                this.message.error('This job only for local candidate apply next job');
                this.router.navigate(['dashboard']);
                return false;

              }
            }

            if(this.isApplied && this.isApplied.jb_post_type =='local-walking'){      
              this.router.navigate(['apply-hiring'],{queryParams:{walking_id:this.isApplied.jb_id}});
              return true;
            }else if(this.isApplied && this.isApplied.jb_post_type =='international-walking'){
              this.router.navigate(['apply-hiring-international'],{queryParams:{walking_id:this.isApplied.jb_id}}); 
              return true;
            }else if(this.isApplied && this.isApplied.jb_post_type =='international-job'){
               if(this.isApplied.jb_type =='saved-job'){
                this.commonService.create('/api/employer/international-jobs/saved', {job_id:this.isApplied.jb_id})
                  .subscribe((data)=>{
                    if(data.status ==200){
                      this.message.success(data.status_text);
                      this.router.navigate(['saved-international-jobs']);                      
                      return true;
                    }else if(data.status ==422){
                      this.router.navigate(['saved-international-jobs']);
                      this.message.error(data.status_text);
                    }
                  }, (error)=>{});
              }else{
                this.commonService.create('/api/employer/international-jobs/applied', {job_id:this.isApplied.jb_id})
                  .subscribe((data)=>{
                    if(data.status ==200){
                      this.message.success(data.status_text);
                      this.router.navigate(['applied-international-jobs']);
                      return true;
                    }else if(data.status ==422){
                      this.router.navigate(['applied-international-jobs']);
                      this.message.error(data.status_text);
                    }
                  }, (error)=>{});
              }       
            }else{
              if(this.isApplied && this.isApplied.jb_type =='apply-job'){
                this.commonService.create('/api/employee/job-search', {job_id:this.isApplied.jb_id})
                  .subscribe((data)=>{
                    if(data.status ==200){
                      this.message.success(data.status_text);
                      this.router.navigate(['applied-jobs']);
                      return true;
                    }else if(data.status ==422){
                      this.router.navigate(['applied-jobs']);
                      this.message.error(data.status_text);
                    }
                  }, (error)=>{});
              }else if(this.isApplied && this.isApplied.jb_type =='saved-job'){
                this.commonService.create('/api/employee/job-search/save-job', {job_id:this.isApplied.jb_id})
                  .subscribe((data)=>{
                    if(data.status ==200){
                      this.message.success(data.status_text);
                      this.router.navigate(['saved-jobs']);
                      return true;
                    }else if(data.status ==422){
                      this.router.navigate(['saved-jobs']);
                      this.message.error(data.status_text);
                    }
                  }, (error)=>{});
              }else{
                this.router.navigate(['job-home']);
              }
            }
              }
            });



      



            
            this.router.navigate(['job-home']);
          }          
        }
  		}, error=>{
  			console.log(error);
  		});
  }

  formReset(){
    Object.keys(this.loginForm.controls).forEach(key => { 
        this.loginForm.get(key).setValue('');
   });

  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
}

}
