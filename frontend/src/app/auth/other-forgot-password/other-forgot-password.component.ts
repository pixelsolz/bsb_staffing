import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MessageService } from '../../services/message.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;

@Component({
  selector: 'app-other-forgot-password',
  templateUrl: './other-forgot-password.component.html',
  styleUrls: ['./other-forgot-password.component.css']
})
export class OtherForgotPasswordComponent implements OnInit {

  forgotForm: FormGroup;
  otherUserType: string;
  isSubmit: boolean;
  constructor(private fb: FormBuilder, private message: MessageService, private authService: AuthService,
    private router: Router, private commonService: CommonService) {
	  	this.router.events.subscribe((event) => {
	      if (event instanceof NavigationEnd) {
	        if (event.url.includes('college')) {
	          this.otherUserType = 'college';
	        } else if (event.url.includes('agency')) {
	          this.otherUserType = 'agency';
	        } else if (event.url.includes('franchise')) {
	          this.otherUserType = 'franchise';
	        } else if (event.url.includes('trainer')) {
	          this.otherUserType = 'trainer';
	        }
	        window.scrollTo(0, 0);
	      }
	    });
     }

  ngOnInit() {
  	 this.forgotForm = this.fb.group({
      user_type: [''],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  submitForm() {
    this.isSubmit = true;
    this.forgotForm.get('user_type').setValue(this.otherUserType);
    if(this.forgotForm.invalid){
      return;
    }
    this.authService.userAuthentication('/api/forget-password', this.forgotForm.value)
      .subscribe((data) => {
        this.isSubmit = false;
        if (data.status == 200) {
        	this.forgotForm.reset();
        	let timerInterval
              Swal.fire({
                title: 'Reset Link Sent',
                html: '<p>Reset link has been sent to your email.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 10000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                }
              });        
          } else if (data.status == 401) {
          this.message.error(data.msg);
        }

      }, (error) => { });
  }

}
