import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MessageService } from '../../services/message.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;

@Component({
  selector: 'app-other-home-forgot-password',
  templateUrl: './other-home-forgot-password.component.html',
  styleUrls: ['./other-home-forgot-password.component.css']
})
export class OtherHomeForgotPasswordComponent implements OnInit {

  otherUserType: string;
  forgetPasswordForm: FormGroup;
  showloader:boolean = false;
  isSubmit:boolean = false;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() openLoginEvent = new EventEmitter<boolean>();
  @Input() userType: string;
  constructor(private fb: FormBuilder, private message: MessageService, private authService: AuthService,
    private router: Router, private commonService: CommonService) { }

  ngOnInit() {
  	this.forgetPasswordForm = this.fb.group({
  		email:['', [Validators.required, Validators.email]],
        user_type: ''
  	});
  }

  handleClose(){
  	this.closeEvent.emit(false)
  }

  handleLogin(){
    this.openLoginEvent.emit(true)
  }


  submitForm() {
  	this.isSubmit = true;
    this.showloader = true;
    this.forgetPasswordForm.get('user_type').setValue(this.userType);
    if(this.forgetPasswordForm.invalid){
      this.showloader = false
      return;
    }
    this.authService.userAuthentication('/api/forget-password', this.forgetPasswordForm.value)
      .subscribe((data) => {
        this.showloader = false;
        if (data.status == 200) {
        	this.forgetPasswordForm.reset();
        	this.handleClose();
        	let timerInterval
              Swal.fire({
                title: 'Reset Link Sent',
                html: '<p>Reset link has been sent to your email.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 10000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                }
              });        
          } else if (data.status == 401) {
          this.message.error(data.msg);
        }

      }, (error) => { });
  }

}
