import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MessageService } from '../../services/message.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-other-login',
  templateUrl: './other-login.component.html',
  styleUrls: ['./other-login.component.css']
})
export class OtherLoginComponent implements OnInit {

  loginForm: FormGroup;
  otherUserType: string;
  isSubmit: boolean;
  showPassword:boolean = false;
  constructor(private fb: FormBuilder, private message: MessageService, private authService: AuthService,
    private router: Router, private commonService: CommonService, private title: Title,
    private meta: Meta) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url.includes('college')) {
          this.otherUserType = 'college';
          this.title.setTitle('College, University & Institution Login Form - bsbstaffing.com');
          this.meta.updateTag({ name: 'description', content: 'Are you College, University and Institution? Login your Bsbstaffing College, University & Institution account.Log in Now!' });
          this.meta.updateTag({ name: 'keywords', content: 'College/ University/ Institution Login' });
        } else if (event.url.includes('agency')) {
          this.otherUserType = 'agency';
          this.title.setTitle('Recruitment Agency Login Form| Recruiting Agent Form - bsbstaffing.com');
          this.meta.updateTag({ name: 'description', content: 'Recruitment Agency Login Form. Bsbstaffing is one of the leading manpower recruitment agencies in India. Fulfill your employment requirements on bsbstaffing.com. Login Now!' });
          this.meta.updateTag({ name: 'keywords', content: 'Recruitment Agency Login' });
        } else if (event.url.includes('franchise')) {
          this.otherUserType = 'franchise';
          this.title.setTitle('Franchise Login|Franchise Form - bsbstaffing.com');
          this.meta.updateTag({ name: 'description', content: 'Login your Franchise account on bsbstaffing.com. LoginNow!' });
          this.meta.updateTag({ name: 'keywords', content: 'Franchise Registration' });
        } else if (event.url.includes('trainer')) {
          this.otherUserType = 'trainer';
          this.title.setTitle('TrainerLogin Form | Login Form - bsbstaffing.com');
          this.meta.updateTag({ name: 'description', content: 'Are you Trainer? Login your Trainer account on bsbstaffing.com.Log in Now!' });
          this.meta.updateTag({ name: 'keywords', content: 'TrainerLogin Form' });
        }
        window.scrollTo(0, 0);
      }
    });
    if (localStorage.getItem('token')) {
      let user = JSON.parse(localStorage.getItem('user'));
      if (user.user_role == 'employee') {
        this.router.navigate(['/dashboard']);
      } else if (user.user_role == 'employer') {
        this.router.navigate(['/employer/dashboard']);
      } else {
        if (user.user_role == 'trainer') {
          this.router.navigate(['/' + user.user_role + '/schedule']);
          return ;
        }else{
          this.router.navigate(['/' + user.user_role + '/home']);
          return ;
        }
       
      }

    }

  }

  ngOnInit() {

    this.loginForm = this.fb.group({
      user_type: [''],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  submitForm() {
    this.isSubmit = true;
    this.loginForm.get('user_type').setValue(this.otherUserType);
     if(this.loginForm.invalid){
      return;
    }
    this.authService.userAuthentication('/api/login', this.loginForm.value)
      .subscribe((data) => {
        if (data.status == 200) {
          this.isSubmit = true;
          this.message.success(data.status_text);
          localStorage.setItem('token', data.token);
          localStorage.setItem('user', JSON.stringify(data.user));
          this.commonService.employer_check('data');
          this.commonService.checkCallLogin('login');
          if (this.otherUserType == 'trainer') {
            this.router.navigate([this.otherUserType + '/schedule']);
          } else {
            this.router.navigate([this.otherUserType + '/home']);
          }

          // this.router.navigate([this.otherUserType + '/home']);
        } else if (data.status == 401) {
          this.message.error(data.status_text);
        }

      }, (error) => { });
  }

  changeRoute() {
    this.router.navigate(['/' + this.otherUserType + '/registration']);
  }

}
