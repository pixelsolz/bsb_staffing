import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MessageService } from '../../services/message.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-other-reset-password',
  templateUrl: './other-reset-password.component.html',
  styleUrls: ['./other-reset-password.component.css']
})
export class OtherResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  otherUserType: string;
  constructor(private fb: FormBuilder, private message: MessageService, private authService: AuthService,
    private router: Router, private commonService: CommonService, private activateRoute: ActivatedRoute) {
  		
  		this.router.events.subscribe((event) => {
	      if (event instanceof NavigationEnd) {
	        if (event.url.includes('college')) {
	          this.otherUserType = 'college';
	        } else if (event.url.includes('agency')) {
	          this.otherUserType = 'agency';
	        } else if (event.url.includes('franchise')) {
	          this.otherUserType = 'franchise';
	        } else if (event.url.includes('trainer')) {
	          this.otherUserType = 'trainer';
	        }
	        window.scrollTo(0, 0);
	      }
	    });
	    this.asyncInit();
    }

  ngOnInit() {
  	this.resetForm = this.fb.group({
      user_type: [''],
      email: [''],
      token:'',
      password: ['', [Validators.required]],
      password_confirmation:['', [Validators.required]]
    });
    this.resetForm.patchValue({
    	user_type: this.otherUserType,
    	token: this.activateRoute.snapshot.queryParams['token']? this.activateRoute.snapshot.queryParams['token']:''
    });
  }

  asyncInit(){
  	if(this.activateRoute.snapshot.queryParams['token']){
  		this.commonService.getAll('/api/reset-token/check/' + this.activateRoute.snapshot.queryParams['token'])
  			.subscribe(data=>{
  				if(data.status ==200){
  					this.resetForm.get('email').setValue(data.data.email);
  				}
  			},error=>{});
  	}
  	
  }

  submitForm(event) {
  	event.preventDefault();
  	console.log(this.resetForm.value);
    this.commonService.create('/api/user/password_reset', this.resetForm.value)
      .subscribe((data) => {
        if (data.status == 200) {
       		this.message.success(data.status_text);
           this.router.navigate(['/']);
       		//this.router.navigate([this.otherUserType + '/login']);
          } else if (data.status == 401) {
          this.message.error(data.msg);
          } else if(data.status ==422){
          	if(data.error && data.error.password){
          		data.error.password.forEach((obj)=>{
          			this.message.error(obj);
          		});
          	}
          }

      }, (error) => { });
  }

}
