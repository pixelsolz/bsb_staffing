import { Component, OnInit, Output,Input, EventEmitter, ɵConsole} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import * as moment from 'moment';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],

})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;
  isEmployer: number;
  verification_code:string;
  verificationData:string;
  login:string;
  showloader: boolean;
  isSubmit: boolean;
  countries: any;
  cities: any;
  industries:any;
  uploaded_file_name:any;
  coutryWiseCity: any=[];
  companySizes: any;
  companyTypes: any;
  @Output() registerEvent = new EventEmitter<string>();
  @Output() footerRegisterEvent = new EventEmitter<string>();
  userUniqueValidErr: any;
  years:any=[]
  serverError: any;
  dateBirthValidation: boolean = false;
  prevEighteenYearDate: any;
  showPassword:boolean = false;
  showConfPassword:boolean = false;
  verificationModal: boolean = false;
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[]
  storeMultiSelectRegistration :any ={
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    industries_value:[],
    industries_name:[],
  };
  countryDivSign2:boolean =false;
  cityDivSign2:boolean=false
  industriesDivSign2:boolean=false
  countryArray: any=[];
  cityArray: any=[];  
  industryArray:any=[];
  @Input() callfromFooter: string

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService) {
    this.router.events.subscribe((event)=>{
      if(event instanceof NavigationStart){
        this.isEmployer = event.url.includes('employer')? 1: 2;
        this.setConditionValidation();
      }
    });
   this.isEmployer = window.location.href.includes('employer')? 1: 2;
   this.login ='1';
   this.showloader = false;
   this.isSubmit = false;


  }
  ngOnInit() {
    if(!_.isEmpty(this.callfromFooter)){
      this.isEmployer = 2
    }
    $(document).on('keyup','.verify_input',(event)=>{
        if(event.target.id !== 6){
          let idVal = Number(event.target.id) + 1
          $('#' + idVal).focus()
        }
        
    })

    if(localStorage.getItem('registrationForm')){
      localStorage.removeItem('registrationForm')
    }

    /*if(localStorage.getItem('registrationForm')){
      let formValue = JSON.parse(localStorage.getItem('registrationForm'))
      if(formValue && formValue.isEmployer != this.isEmployer){
        localStorage.removeItem('registrationForm')
      }else{
        this.verificationModal = true
      }
      
    }*/
    
    this.prevEighteenYearDate = moment().subtract(18, 'years').calendar()
    this.registerForm = this.fb.group({
      isEmployer: '',
      first_name:['', [Validators.required, this.noWhitespaceValidator]],
      last_name: ['', [Validators.required, this.noWhitespaceValidator]],
      country_id: ['', []],
      city_id: ['', []],
      designation: ['', []],
      industry_id: ['', []],
      upload_cv:['', []],
      company_name:['', []],
      office_address:['', []],
      company_size:['', []],
      company_type:['', []],
      date_of_birth:['', []],
      gender:['', []],
      email: ['', [Validators.required]],
      mobile: ['', []],
      user_unique_code:[''],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6), this.checkConfirmPassword]],
      
    });
    this.registerForm.get('country_id').valueChanges.subscribe(value=>{
      if(value.length){
        let country = this.countries.find(obj=>obj._id == value[0]);
        this.commonService.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){
            this.coutryWiseCity = {"":data.data}
         }
        })
        //this.coutryWiseCity = {"":this.cities.filter(obj=>obj.country_code == country.code)}       
      }else{
        this.coutryWiseCity =[]
        this.storeMultiSelectRegistration.city_value.splice(0, 1);
        this.storeMultiSelectRegistration.city_name.splice(0, 1);
        this.registerForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
      }

    });

    this.setConditionValidation();
    this.asyncInit();

    var max = new Date().getFullYear(),
    min = max - 70,
    //max = max + 1;
    max = max ;
    //this.years.current_year = new Date().getFullYear();
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    }
    //console.log(this.years)
    
  }

  setConditionValidation(){

    if(this.isEmployer == 1){
      this.registerForm.controls["country_id"].setValidators([Validators.required]);
      this.registerForm.controls["city_id"].setValidators([Validators.required]);
      this.registerForm.controls["designation"].setValidators([Validators.required, this.noWhitespaceValidator, Validators.maxLength(50)]);
      this.registerForm.controls["industry_id"].setValidators([Validators.required]);
      this.registerForm.controls["date_of_birth"].setValidators([Validators.required]);
      this.registerForm.controls["gender"].setValidators([Validators.required]);
      this.registerForm.controls["mobile"].setValidators([Validators.required,Validators.pattern('^(\\+)?(\\d+)$'),Validators.minLength(8),Validators.maxLength(20)]);

      this.registerForm.controls["company_name"].setValidators([Validators.required,this.noWhitespaceValidator]);
      this.registerForm.controls["office_address"].setValidators([Validators.required,this.noWhitespaceValidator]);
      this.registerForm.controls["company_size"].setValidators([Validators.required]);
      this.registerForm.controls["company_type"].setValidators([Validators.required]);
      this.registerForm.controls['upload_cv'].clearValidators();

    }
    if(this.isEmployer == 2){
      this.registerForm.controls["upload_cv"].setValidators([Validators.required]);
      this.registerForm.controls['company_size'].clearValidators();
      this.registerForm.controls['company_type'].clearValidators();
      this.registerForm.controls['office_address'].clearValidators();
      this.registerForm.controls['company_name'].clearValidators();
      this.registerForm.controls["country_id"].clearValidators();
      this.registerForm.controls["city_id"].clearValidators();
      this.registerForm.controls["designation"].clearValidators();
      this.registerForm.controls["industry_id"].clearValidators();
      this.registerForm.controls["date_of_birth"].clearValidators();
      this.registerForm.controls["mobile"].clearValidators();
      this.registerForm.controls["gender"].clearValidators();
    }
    this.registerForm.controls['country_id'].updateValueAndValidity()
    this.registerForm.controls['city_id'].updateValueAndValidity()
    this.registerForm.controls['upload_cv'].updateValueAndValidity()
    this.registerForm.controls['company_name'].updateValueAndValidity()
    this.registerForm.controls['office_address'].updateValueAndValidity()
    this.registerForm.controls['company_size'].updateValueAndValidity()
    this.registerForm.controls['company_type'].updateValueAndValidity()
    this.registerForm.controls["country_id"].updateValueAndValidity();
    this.registerForm.controls["city_id"].updateValueAndValidity();
    this.registerForm.controls["designation"].updateValueAndValidity();
    this.registerForm.controls["industry_id"].updateValueAndValidity();
    this.registerForm.controls["date_of_birth"].updateValueAndValidity();
    this.registerForm.controls["mobile"].updateValueAndValidity();
    this.registerForm.controls["gender"].updateValueAndValidity();

  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.industries = data['all_industries'].filter(obj=>obj.name !==""); 
        this.countryArray = {"":data['countries']};
        this.companySizes = data['companySizes'];
        this.companyTypes = data['companyTypes'];
        this.industryArray =  data['industries'];
      }
    });

  }

  backToRegister(){
    this.verificationModal=false;
    let formValue = localStorage.getItem('registrationForm') ? JSON.parse(localStorage.getItem('registrationForm')):''
    Object.entries(formValue).forEach(obj=>{
      this.registerForm.controls[obj[0]].setValue(obj[1])
    })

  }

  register(){    
    this.showloader = true;
    this.isSubmit = true;
    this.userUniqueValidErr = '';
    if(this.registerForm.invalid){
      this.showloader = false;
      return;
    }

    this.registerForm.controls['email'].setValue(this.registerForm.controls['email'].value.trim())
    this.registerForm.controls['isEmployer'].setValue(this.isEmployer);
    let formObj = this.formDataSet(this.registerForm);

    this.authService.create('/api/email-verification/send', formObj)
      .subscribe((data)=>{
        this.showloader = false;
        this.isSubmit = false;
        if(data.status == 422){
          this.serverError = data.error;
          if(data.error && data.error.email){
            this.message.error(data.error.email);
          }if(data.error && data.error.upload_cv){
            this.message.error(data.error.upload_cv);
          }
          
        }else if(data.status == 421){
          this.userUniqueValidErr = data.error;
        }else if(data.status == 500){

        }else{
            this.verificationData = data.token
            this.registerForm.controls['upload_cv'].setValue(data.cv_path);
            localStorage.setItem('registrationForm',JSON.stringify(this.registerForm.value))
            this.verificationModal = true
            /*let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: '<p>You have registered successfully.<br/> A Verification code has been sent to your email, copy the code and put the next form. </p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 10000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                }
              });*/
        }
      }, (error)=>{
          this.showloader = false;
      });
  }

 resendCode(){
  let formValue = localStorage.getItem('registrationForm') ? JSON.parse(localStorage.getItem('registrationForm')):''
   this.authService.create('/api/email-verification/send', formValue).subscribe(data=>{
     this.verificationData = data.token
     this.message.success('Successfully send');
   });
 }

 submitFormAndCreate(){
   this.showloader = true
   let verifyInput = '';
    $('.verify_input').each(function(){
    verifyInput += $(this).val()
  });

   if(this.verificationData !== verifyInput){
     this.message.error('verification code does not matched!');
     this.showloader = false;
     return
   }
   let formValue  = localStorage.getItem('registrationForm') ? JSON.parse(localStorage.getItem('registrationForm')):''
   this.authService.create('/api/registration', formValue).subscribe((data)=>{
      this.showloader = false;
      this.isSubmit = false;
      this.verificationModal = false
      if(!_.isEmpty(this.callfromFooter)){
          this.footerRegisterEvent.emit('1');
        }
      this.commonService.checkCallLogin('registration');
      this.registerForm.reset();
      $("#loginModal").find('.close').click();
      $('#regisDiv').attr('class','regSection deactiveSection');
      $('#loginDiv').attr('class','loginSection activeSection');
      this.message.success('Successfully registered');
      localStorage.removeItem('registrationForm');
      localStorage.setItem('token', data.token);
      localStorage.setItem('user', JSON.stringify(data.user));
      localStorage.setItem('is_employer', data.user.is_employer? '1':'0');
      this.commonService.employer_check(1);
      this.commonService.checkCallLogin('login');
      if(data.user.is_employer){
        this.router.navigate(['employer/dashboard']);
      }else{
        this.router.navigate(['dashboard']);
      }
   },error=>{})


 }

  showLgin(){
    this.registerEvent.emit(this.login);
  }

  checkConfirmPassword(c: FormControl) {
    if (!c.root && !c.root.get('password')) {
      return null;
    }
    if (c.value && c.value !== c.root.get('password').value) {
      return { error: true };
    }
  }

   noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
}

  uploadCv(resume) {
    const file = resume.files[0];
    const filesize = Math.round((file.size / 1024));
    if(filesize > 5120 ){
      this.message.error('Maximum file size allowed 5 MB');
      return false;
    }
    console.log(file);
    if (file.name.split('.').pop().toLowerCase() == 'pdf' || file.name.split('.').pop().toLowerCase() == 'docx' || file.name.split('.').pop().toLowerCase() == 'doc') {
     this.registerForm.controls['upload_cv'].setValue(file);
     this.uploaded_file_name = file.name.substring(0,34);
    } else {
      this.message.error('Upload file only allow PDF,DOCX or DOC');
    }

  }

  formDataSet(form) {
    let formData = new FormData();
    Object.keys(form.controls).forEach(key => {
      if(key ==='country_id'){
        formData.append(key, form.get(key).value[0]);
      }else if(key ==='city_id'){
         formData.append(key, form.get(key).value[0]);
      }else if(key ==='industry_id'){
         formData.append(key, form.get(key).value[0]);
      }else{
        formData.append(key, form.get(key).value);
      }
      
    });
    return formData;
  }

  removeMultiSelectSign2(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelectRegistration.industries_value.splice(indx, 1);
       this.storeMultiSelectRegistration.industries_name.splice(indx, 1);
       this.registerForm.patchValue({
        industry_id: this.storeMultiSelectRegistration.industries_value
        });
        break;
      case "country":
       this.storeMultiSelectRegistration.country_value.splice(indx, 1);
       this.storeMultiSelectRegistration.country_name.splice(indx, 1);
       this.registerForm.patchValue({
        country_id: this.storeMultiSelectRegistration.country_value
       });

        break;
      case "city":
       this.storeMultiSelectRegistration.city_value.splice(indx, 1);
       this.storeMultiSelectRegistration.city_name.splice(indx, 1);
       this.registerForm.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
        break; 
      default:
        // code...
        break;
    }    
  }

  getOnchangeEventSign2(type){
    if(type =='country'){
      this.countryDivSign2 = true;
    }else if(type =='city'){
      this.cityDivSign2 = true;
    }else if(type=='industries'){
      this.industriesDivSign2 = true;
    }
  }

  getChangeSign2(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='industries'){
      this.registerForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.registerForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.registerForm.patchValue({
      city_id: e.storedValue
      });
    }
  }

  checkmultiselectSign2(e){
    if (e.field_name == 'industries') {
      this.industriesDivSign2 = e.status;
    }else if(e.field_name =='country'){
      this.countryDivSign2 = e.status;
    }else if(e.field_name =='city'){
      this.cityDivSign2 = e.status;
    }
  }


}
