import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

const routes: Routes = [
	{path:'cms', component: AboutUsComponent,
		children:[
			{
				path: 'about-us',
				//path: '**',
				component: AboutUsComponent
			}
		]
		
	},
	{path:'contact-us', component: ContactUsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
