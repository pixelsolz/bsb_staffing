import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-college-home',
  templateUrl: './college-home.component.html',
  styleUrls: ['./college-home.component.css']
})
export class CollegeHomeComponent implements OnInit {

  mySlideImages = ['assets/images/hotelImg1.png','assets/images/hotelImg2.png','assets/images/hotelImg3.png'];
  myCarouselOptions={items: 1, dots: true, nav: true, autoplay:true, loop:true, animateOut: 'fadeOut', margin:0};
  constructor() { }

  ngOnInit() {
  }

}
