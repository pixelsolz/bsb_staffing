import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollegeRegistrationComponent } from './college-registration/college-registration.component';
import { CollegeHomeComponent } from './college-home/college-home.component';
import { AuthGuard } from './../auth.guard';
import { CollegeProfileComponent } from './college-profile/college-profile.component';

const routes: Routes = [
	{path:'registration',component: CollegeRegistrationComponent},
	{path:'', loadChildren:'../auth/auth.module#AuthModule'},
	{path:'',canActivate: [AuthGuard], loadChildren:'../common-uses/common-uses.module#CommonUsesModule'},
	//{path:'home',canActivate: [AuthGuard], component: CollegeHomeComponent},
	{path:'profile',canActivate: [AuthGuard], component: CollegeProfileComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollegeInstituteRoutingModule { }
