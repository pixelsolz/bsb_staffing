import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import {AuthModule} from '../auth/auth.module';
import { OwlModule } from 'ngx-owl-carousel';
import { CommonUsesModule } from '../common-uses/common-uses.module';

import { CollegeInstituteRoutingModule } from './college-institute-routing.module';
import { CollegeRegistrationComponent } from './college-registration/college-registration.component';
import { CollegeHomeComponent } from './college-home/college-home.component';
import { CollegeProfileComponent } from './college-profile/college-profile.component';

@NgModule({
  declarations: [CollegeRegistrationComponent, CollegeHomeComponent, CollegeProfileComponent],
  imports: [
    CommonModule,
    CollegeInstituteRoutingModule,
    CoreModule,
    AuthModule,
    OwlModule,
    CommonUsesModule
  ]
})
export class CollegeInstituteModule { }
