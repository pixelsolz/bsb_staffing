import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-college-profile',
  templateUrl: './college-profile.component.html',
  styleUrls: ['./college-profile.component.css']
})
export class CollegeProfileComponent implements OnInit {
	collegeUpdateForm: FormGroup;
	user:any;
	isEdit: boolean=false;
	countries: any=[];
  cities: any=[];
  logoImage: string;
  logoUploadImage: string;
  gallerie: any={
    previews:[],
    images:[]
  };
  gallerie_previews: any=[];
  validation_error: any;
  coursesData: any=[];
  courseDiv: boolean;
  noSearchSelect: any={
    course_value:[],
    course_name:[]
  };
  
  coutryWiseCity: any=[];
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About your College/ University/ Institution',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  specialconfig: any = {
    //height: '200px',
    placeholder: 'Special Note for Employer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  addressLatLng:any={
    lat:'',
    lng:''
  }

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
  private router: Router, private commonService: CommonService) { 
  
  this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
  console.log(this.user);
  let gallery_image = this.user.user_detail.gallery_images;
  if(gallery_image){
  let gallery_img_arr = gallery_image.split(",");
    //console.log(gallery_img_arr);
    this.gallerie.previews = gallery_img_arr.map((image)=>{
        return this.global.imageUrl+'colleges/gallery_images/'+image;
     });
  }
      //console.log(this.user);
       window.scrollTo(0, 0);

  }

  ngOnInit() {
    var max = new Date().getFullYear(),
    min = max - 70,
    max = max ;
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    } 
  	this.collegeUpdateForm = this.fb.group({
  		name:['', [Validators.required]],
  		website:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		logo: ['', [Validators.required]],
      courses:[''],
  		total_student:['', [Validators.required]],
  		total_student_place_per_year:['', [Validators.required]],
  		about_college:[''],
  		special_note_employer:[''],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]],
      dob_date:['', [Validators.required]],
      dob_month:['', [Validators.required]],
      dob_year:['', [Validators.required]],
      gender:['', [Validators.required]],  
      name_account_holder:'',     
      bank_name:'',     
      account_no:'',     
      swift_code:'',     
      ifsc_code:'',     
      bank_postal_address:'',
      password:'',
      password_confirmation:'', 		
  	});
    this.collegeUpdateForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });
  	this.asyncInit();
  }

  setForm(){
  	this.noSearchSelect.course_value = this.user.user_detail.courses.map(obj=>obj._id);
    this.noSearchSelect.course_name = this.user.user_detail.courses_name;
    if(this.user.user_detail.address){
      this.commonService.getLatLong(this.user.user_detail.address).then(res=>{
         if(res.status === "OK"){
           console.log(res.results[0]['geometry']['location']['lat'])
           this.addressLatLng.lat = res.results[0]['geometry']['location']['lat']
           this.addressLatLng.lng = res.results[0]['geometry']['location']['lng']
         }     
     })
    }
  	this.collegeUpdateForm.patchValue({
  		id: (this.user)? this.user.id:'',
  		name:(this.user && this.user.user_detail)? this.user.user_detail.name:'',
  		website:(this.user && this.user.user_detail)? this.user.user_detail.website:'',
  		country_id:(this.user && this.user.user_detail)? this.user.user_detail.country._id:'',
  		city_id:(this.user && this.user.user_detail)? this.user.user_detail.city._id:'',
  		address:(this.user && this.user.user_detail)? this.user.user_detail.address:'',
  		courses:(this.user && this.user.user_detail)? this.noSearchSelect.course_value:'',
  		total_student:(this.user && this.user.user_detail)? this.user.user_detail.total_student:'',
  		total_student_place_per_year:(this.user && this.user.user_detail)? this.user.user_detail.total_student_place_per_year:'',
  		about_college:(this.user && this.user.user_detail)? this.user.user_detail.about_college:'',
  		special_note_employer:(this.user && this.user.user_detail)? this.user.user_detail.special_note_employer:'',
  		contact_person_name:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_name:'',
      contact_person_lastname:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_lastname:'',
  		contact_person_designation:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_designation:'',
  		contact_person_email:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_email:'',
  		contact_person_phone:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_phone:'',
      dob_date: (this.user && this.user.user_detail.dob_date) ? this.user.user_detail.dob_date :'',
      dob_month:(this.user && this.user.user_detail.dob_month) ? this.user.user_detail.dob_month :'',
      dob_year: (this.user && this.user.user_detail.dob_year) ? this.user.user_detail.dob_year :'',
      gender: (this.user && this.user.user_detail.gender) ? this.user.user_detail.gender :'',
  		name_account_holder:(this.user && this.user.user_detail && this.user.user_detail.name_account_holder !='null')? this.user.user_detail.name_account_holder:'',
  		bank_name:(this.user && this.user.user_detail && this.user.user_detail.bank_name !='null')? this.user.user_detail.bank_name:'',
  		account_no:(this.user && this.user.user_detail && this.user.user_detail.account_no !='null')? this.user.user_detail.account_no:'',
  		swift_code:(this.user && this.user.user_detail && this.user.user_detail.swift_code !='null')? this.user.user_detail.swift_code:'',
  		ifsc_code:(this.user && this.user.user_detail && this.user.user_detail.ifsc_code !='null')? this.user.user_detail.ifsc_code:'',
  		bank_portal_address:(this.user && this.user.user_detail)? this.user.user_detail.bank_portal_address:'',
      logo:(this.user && this.user.user_detail)? this.user.user_detail.logo_url:'',
      logoImage:(this.user && this.user.user_detail)? this.user.user_detail.logo:'',
  	});
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.coursesData = {"":_.uniqBy(data['courses'][""],(e)=>{
              return e.name
            })};
      }
    });
    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
       this.setForm();
      }
    });
  }

  noSearchGetchange(e){
     if(e.event){
      let index = this.noSearchSelect.course_value.indexOf(e.value);
      if(index ==-1){
        this.noSearchSelect.course_value.push(e.value);
        this.noSearchSelect.course_name.push(e.name); 
      }
    }else{
      let index = this.noSearchSelect.course_value.indexOf(e.value);
      if(index !=-1){
        this.noSearchSelect.course_value.splice(index,1);
        this.noSearchSelect.course_name.splice(index,1);
      }
    }
    this.collegeUpdateForm.patchValue({
      courses: this.noSearchSelect.course_value
      });
  }

    removeMultiSelect(indx, type){
    switch (type) { 
      case "course":
        this.noSearchSelect.course_value.splice(indx,1);
        this.noSearchSelect.course_name.splice(indx,1);
       this.collegeUpdateForm.patchValue({
          courses: this.noSearchSelect.course_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

   checknoSearchmultiSelect(e){
    this.courseDiv = e.status;
  }

  logoUpload(e){
    this.logoImage = e.target.files[0].name;
    this.collegeUpdateForm.get('logo').setValue(e.target.files[0]);
    const reader = new FileReader();
      reader.onload = (event:any) => {
        this.logoUploadImage = event.target.result;
      }
      reader.readAsDataURL(e.target.files[0]);
  }

  removePicture(){
    this.collegeUpdateForm.controls['logo'].setValue('');
    this.logoUploadImage ='';
    this.logoImage='';  
  }

  galleryUpload(e){
    const file = e.target.files[0];
    this.gallerie.images.push(file);
    const reader = new FileReader();
      reader.onload = (event:any) => {
        this.gallerie.previews.push(event.target.result);
      }
      reader.readAsDataURL(file);
  }

  removeGalleryImage(index){
   this.gallerie.previews.splice(index, 1);
   this.gallerie.images.splice(index, 1);
  }
  updateForm(){
    //this.collegeUpdateForm.get('courses').setValue(this.noSearchSelect.course_value);
    let formValue = this.formDataSet(this.collegeUpdateForm);
    this.commonService.create('/api/college/profile-update', formValue)
      .subscribe((data)=>{
        if(data.status == 200){
          localStorage.setItem('user', JSON.stringify(data.data));
          this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
          this.message.success('Successfully update profile');
          this.commonService.callProfileUpdate(data.data);
          this.isEdit = false;

        }else if(data.status == 500){
          this.message.error(data.status_text);
        }else if(data.status ==422){
          this.validation_error = data.error;
        }
      },(error)=>{});
  }
  formDataSet(form){
    let formData = new FormData();
    if(this.addressLatLng.lat && this.addressLatLng.lng){
      formData.append('lat', this.addressLatLng.lat);
      formData.append('lng', this.addressLatLng.lng);
    }
    Object.keys(form.controls).forEach(key => { 
       formData.append(key, form.get(key).value);
    });
    if(this.gallerie.images.length){
       this.gallerie.images.forEach((data, i)=>{
         formData.append('gallery_' + i, data);
       });
       formData.append('gallery_length', this.gallerie.images.length);
    }
    return formData;
    }

    getOnchangeEvent(){
      this.courseDiv = true;
    }
    checkmultiselect(e){
      console.log(e);
      if(e.field_name =='courses'){
        this.courseDiv = e.status;
      }
      
    }
}

