import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-college-registration',
  templateUrl: './college-registration.component.html',
  styleUrls: ['./college-registration.component.css']
})
export class CollegeRegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  countries: any=[];
  cities: any=[];
  logoImage: string;
  gallerie: any={
    previews:[],
    images:[]
  };
  gallerie_previews: any=[];
  validation_error: any;
  coursesData: any=[];
  courseDiv: boolean=false;
  noSearchSelect: any={
    course_value:[],
    course_name:[]
  };
  storeMultiSelect: any={
    course_value:[],
    course_name:[]
  };
  coutryWiseCity: any=[];
  collegeUploadLogoImage: any;
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About your College/ University/ Institution',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  specialconfig: any = {
    //height: '200px',
    placeholder: 'Special Note for Employer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  missionconfig: any = {
    //height: '200px',
    placeholder: 'Mission',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  visionconfig: any = {
    //height: '200px',
    placeholder: 'Vision',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
    years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private title: Title,
    private meta: Meta) { 
    //this.courseDiv = false;
  }

  ngOnInit() {
    var max = new Date().getFullYear(),
    min = max - 70,
    max = max ;
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    }   

    this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });

  	this.registrationForm = this.fb.group({
  		name:['', [Validators.required]],
  		website:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		logo: ['', [Validators.required]],
      courses:[''],
  		total_student:['', [Validators.required]],
  		total_student_place_per_year:['', [Validators.required]],
  		about_college:[''],
  		special_note_employer:[''],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]],
      dob_date:['', [Validators.required]],
      dob_month:['', [Validators.required]],
      dob_year:['', [Validators.required]],
      gender:['', [Validators.required]],  
      name_account_holder:'',     
      bank_name:'',     
      account_no:'',     
      swift_code:'',     
      ifsc_code:'',     
      bank_postal_address:'',
      mission:'',
      vision:'',
      institute_video:'', 		
      declaration_user:'',    
      type_of_listing:'',    
  	});
    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });
    this.asyncInit();
    this.title.setTitle('College, University & Institution Registration Form - bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Are you College, University and Institution? Create your College, University and Institution account on bsbstaffing.com.Registered Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'College/ University/ Institution Registration' });
  }

  asyncInit(){
    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
       this.countries = data['countries'];
       this.cities = data['cities'];
       this.coursesData = data['courses'];
      }
    });
  }

  submitForm(){
    if(this.registrationForm.get('institute_video').value){
      if(!this.registrationForm.get('institute_video').value.match('^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+')){
        this.message.error('Please enter proper youtube link');
        //this.emp_video.video_link ='';
        return false;
      }
    }
    //this.registrationForm.get('courses').setValue(this.noSearchSelect.course_value);
    let formValue = this.formDataSet(this.registrationForm);
  	this.commonService.create('/api/college-registration', formValue)
  		.subscribe((data)=>{  			
        if(data.status ==200){
          this.message.success(data.status_text);
          let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: '<p>You have registered successfully.<br/> After verified from admin you can login to system by email and password.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 15000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  this.router.navigate(['/college/login']);
                }
              });
          
        }else if(data.status ==422){
          this.validation_error = data.error;
          this.registrationForm.get('courses').setValue(this.noSearchSelect.course_name);
        }
  		},(error)=>{});
  }

  logoUpload(e){
    const file = e.target.files[0];
    this.registrationForm.get('logo').setValue(e.target.files[0]);
    const reader = new FileReader();
      reader.onload = (event:any) => {
        this.collegeUploadLogoImage = event.target.result;
      }
      reader.readAsDataURL(file);
  }
  removelogo(){
    this.registrationForm.get('logo').setValue('');
    this.collegeUploadLogoImage ='';
  }

  galleryUpload(e){
    const file = e.target.files[0];
    this.gallerie.images.push(file);
    const reader = new FileReader();
      reader.onload = (event:any) => {
        this.gallerie.previews.push(event.target.result);
      }
      reader.readAsDataURL(file);
  }

 removeGalleryImage(index){
   this.gallerie.previews.splice(index, 1);
   this.gallerie.images.splice(index, 1);
 }

 formDataSet(form){
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
       formData.append(key, form.get(key).value);
   });
   if(this.gallerie.images.length){
       this.gallerie.images.forEach((data, i)=>{
         formData.append('gallery_' + i, data);
       });
       formData.append('gallery_length', this.gallerie.images.length);
   }
   return formData;
 }

  noSearchGetchange(e){
    console.log(e);
     if(e.event){
      let index = this.noSearchSelect.course_value.indexOf(e.value);
      if(index ==-1){
        this.noSearchSelect.course_value.push(e.value);
        this.noSearchSelect.course_name.push(e.name); 
      }
    }else{
      let index = this.noSearchSelect.course_value.indexOf(e.value);
      if(index !=-1){
        this.noSearchSelect.course_value.splice(index,1);
        this.noSearchSelect.course_name.splice(index,1);
      }
    }
    this.registrationForm.patchValue({
      courses: this.noSearchSelect.course_value
      });
  }

  removeMultiSelect(indx, type){
    switch (type) { 
      case "course":
        this.noSearchSelect.course_value.splice(indx,1);
        this.noSearchSelect.course_name.splice(indx,1);
       this.registrationForm.patchValue({
          courses: this.noSearchSelect.course_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  getOnchangeEvent(){
    this.courseDiv = true;
  }
  checkmultiselect(e){
    console.log(e);
    if(e.field_name =='courses'){
      this.courseDiv = e.status;
    }
    
  }
 

}
