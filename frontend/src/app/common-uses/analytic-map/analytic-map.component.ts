import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-analytic-map',
  templateUrl: './analytic-map.component.html',
  styleUrls: ['./analytic-map.component.css']
})
export class AnalyticMapComponent implements OnInit {

  constructor( private router: Router, private commonService: CommonService) { 

  }

  ngOnInit() {
    this.commonService.callFooterMenu(1);
  	    this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });
  }

}
