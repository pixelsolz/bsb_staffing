import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-analytic',
  templateUrl: './analytic.component.html',
  styleUrls: ['./analytic.component.css']
})
export class AnalyticComponent implements OnInit {
  
  user_type: string;
  user:any;
  param: string;
  studentData: any=[];
  constructor(private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  	
  	this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
  	this.user_type = this.user? this.user.user_role:'';
  	this.param = this.activeRoute.snapshot.params.name;
  	console.log(this.activeRoute.snapshot.params);

  }

  ngOnInit() {
  	
  	this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((e: any) => {
        window.scrollTo(0, 0); 
      });

      this.asyncInit();
  }

  asyncInit(){
    this.commonService.getAll('/api/student-data?type='+this.param)
        .subscribe((data)=>{
          this.commonService.callFooterMenu(1);
          if(data.status ==200){
            this.studentData = data.data;
            console.log(this.studentData);
          }
        },(error)=>{});
  }

}
