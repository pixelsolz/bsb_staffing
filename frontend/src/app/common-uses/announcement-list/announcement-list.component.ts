import { Component, OnInit,ElementRef,HostListener } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {MessageService} from '../../services/message.service';
import {CommonService} from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var $;

@Component({
  selector: 'app-announcement-list',
  templateUrl: './announcement-list.component.html',
  styleUrls: ['./announcement-list.component.css']
})
export class AnnouncementListComponent implements OnInit {
  @HostListener('document:click', ['$event'])
  clickout(event) {
     if(event.target.placeholder !='undefined' && (event.target.placeholder ==='Add emails' || event.target.placeholder === '+ email')){
        this.showSummary =false;       
     }else if(event.target.name && event.target.name =='show_summary'){
       this.showSummary =false;
     }else if(event.target.className === 'fixed-text_data'|| event.target.className === 'selected-span' || event.target.className === 'dropdown-search' || event.target.className === 'input_text'){
       this.showSummary =false;
     }else{
       if(this.emailData && this.emailData.length >= 1){
         this.showEmails = this.emailData.slice(0,4).map(obj=>obj.value).toString();
         //console.log(this.emailData.slice(0,2).map(obj=>obj.value));
          if((this.remainingCount - this.emailData.length) <0 ){
            this.message.error('You can not send morethan 50 emails');
          }
          this.showSummary =true;
        }else{
          this.showSummary =false;
        }
     }
  }

  termCondAgree :boolean =false;
  announceForm: FormGroup;
  emailData: any;
  templates: any=[];
  emails_data: any=[];
  userData: any={};
  use_role: string;
  announceId: string;
  isSubmit: boolean =false;
  showSummary: boolean =false;
  sendedEmail: any=[];
  remainingCount : number =50;
  existEmailError: any;
  showEmails: string;
  selectedBody:string='';
  constructor(private fb:FormBuilder, private message:MessageService ,private authService: AuthService,
    private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute, 
    private eRef: ElementRef) {
      this.userData = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
      this.use_role = this.userData.user_role;
      //console.log(this.userData.role._id);
      //console.log(this.activeRoute.snapshot.queryParams);
      this.announceId = this.activeRoute.snapshot.queryParams['announce_id'];
    }

  ngOnInit() {
     window.scrollTo(0, 0); 
    
  	this.announceForm = this.fb.group({
  		emails:['',[Validators.required]],
      benefit_id:'',
  		subject_id:['',[Validators.required]],
  		message:['',[Validators.required]],
  		signature:['',[Validators.required]],
      agree:['',[Validators.required]],
      remaining_count: ['']
  	});

    this.announceForm.get('subject_id').valueChanges.subscribe(value=>{
        if(value){
          let temp = this.templates.find(obj=>String(obj._id) == String(value));
          this.selectedBody =temp.temp_content;
          this.announceForm.get('message').setValue(temp.temp_content);
        }
    });

    this.announceForm.get('benefit_id').setValue(this.announceId);
    this.asyncInit();

    this.announceForm.get('agree').valueChanges.subscribe(value=>{
         this.termCondAgree = value;
    });
  }

  asyncInit(){
    this.commonService.getAll("/api/get-announcement-benefit?id=" + this.announceId)
        .subscribe((data)=>{
          this.commonService.callFooterMenu(1);
          if(data.status == 200){
            if(data.data.sendedEmails.length){
              data.data.sendedEmails.map(data=> data.map(email=> this.sendedEmail.push(email)));
              this.remainingCount = this.remainingCount - this.sendedEmail.length;
            }
            this.templates= data.data.templates.filter(obj=>obj.entity_id == this.userData.role._id);
            //this.templates = data.data.templates;
          }
        },(error)=>{});
  }

  entervalue(event){
     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let data = event;
    let lastData = data.slice(-1)[0]; 
    let lastDataValue = lastData ? lastData.value.replace(/ /g,","):'';
    console.log(lastDataValue);
    var checkSpace = /\s/;
    if(lastData && /[,\-]/.test(lastDataValue)){
    let newData = lastDataValue.split(',').map(val=>({value: val, display: val}));  
    this.emailData.splice(data.length -1, 1);
    newData.forEach(obj=>{
      if(obj.value ){
        if(re.test(String(obj.value).toLowerCase())){
        this.emailData =  [...this.emailData, obj];  
        }
        
      }     
    });
    }else if(checkSpace.test(lastData)){
    let newData = lastData.value.split(' ').map(val=>({value: val, display: val}));  
    this.emailData.splice(data.length -1, 1);
    newData.forEach(obj=>{
      this.emailData =  [...this.emailData, obj];  
    });
    }
  }

  submitForm(){
    this.existEmailError ='';
    if(this.emailData != undefined && this.emailData !==''){
     let emails = this.emailData.map(obj=>obj.value);
     let sameEmails = this.sendedEmail.filter(val=>emails.indexOf(val)!== -1);
     if(sameEmails.length){
       this.existEmailError = sameEmails.toString() + ' emails are already submitted'
       return false;
     }
     this.announceForm.get('emails').setValue(emails);
    }
    this.isSubmit = true;
    if(this.announceForm.invalid){
      return false;
    }

    let remain_count = this.remainingCount - this.announceForm.get('emails').value.length;
    if(remain_count < 0){
       this.existEmailError = 'You can not send morethan 50 emails';
       return false;
    }

    this.announceForm.get('emails').value.forEach(email=> this.sendedEmail.push(email));
    this.announceForm.get('remaining_count').setValue(this.remainingCount);
    this.commonService.create('/api/announcement-benefit/submit-without-login', this.announceForm.value)
        .subscribe(data=>{
          if(data.status ==200){
            this.remainingCount = remain_count;
            this.showSummary =false;
            this.isSubmit = false;
            this.resetForm();
            this.emailData = '';
            this.message.success(data.status_text);
            this.existEmailError ='';
            this.selectedBody='';
            //this.router.navigate(['/' + this.use_role + '/home']);
          }
        }, error=>{});
  }

  resetForm(){
    this.announceForm.reset();
    this.announceForm.get('message').setValue('');
    this.announceForm.get('subject_id').setValue('');
    this.announceForm.get('signature').setValue('');
    this.announceForm.get('agree').setValue('');
  }

  /*public findInvalidControls() {
    const invalid = [];
    const controls = this.announceForm.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }
    console.log(invalid);
}*/

}
