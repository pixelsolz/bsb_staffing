import { Component, OnInit, ElementRef,Renderer2} from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ColorEvent } from 'ngx-color';
declare var $: any;

@Component({
  selector: 'app-carrer',
  templateUrl: './carrer.component.html',
  styleUrls: ['./carrer.component.css']
})
export class CarrerComponent implements OnInit {
	user_id:string;
	carrerData:any;
	count_opening_jobs:number;
	employer_jobs:any;
	galleries: any={ files:[], previews:[]};
  currentYear :any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService,private activeRoute: ActivatedRoute,private eRef:ElementRef,private renderer:Renderer2) {
      this.currentYear = new Date().getFullYear();
  		this.user_id = this.activeRoute.snapshot.queryParamMap.get("ec") ? this.activeRoute.snapshot.queryParamMap.get("ec"):'';
  		this.commonService.getAll('/api/employer/carrer-details?ec='+atob(this.user_id))
        .subscribe((data)=>{
            this.commonService.callFooterMenu(1);
            this.carrerData = data.data.carrerdata;
            this.count_opening_jobs = data.data.count_opening_jobs;
            this.employer_jobs = data.data.employer_jobs;
            //console.log(data.data.employer_jobs);
          	this.galleries.previews = this.carrerData.carrer_photos.map((image)=>{
				      return {id:image.id,url:this.global.imageUrl+'career_page/photo/'+image.name,type:'saved'}
				    });
          	
          	//console.log(btoa(this.carrerData.user_id));
          	//console.log(atob(this.carrerData.user_id));
            
        }, (error)=>{});
  		
    }

  ngOnInit() {

  	

  }

}
