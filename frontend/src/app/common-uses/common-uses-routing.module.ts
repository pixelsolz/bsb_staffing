import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ReferalListComponent } from './referal-list/referal-list.component';
import { AnnouncementListComponent } from './announcement-list/announcement-list.component';
import { PaymentComponent } from './payment/payment.component';
import { AnalyticComponent } from './analytic/analytic.component';
import { AnalyticMapComponent } from './analytic-map/analytic-map.component';
import { OtherUserHomeComponent } from './other-user-home/other-user-home.component';
import { AuthGuard } from './../auth.guard';
import { CvUploadComponent } from './cv-upload/cv-upload.component';
import { CarrerComponent } from './carrer/carrer.component';
import { JobsByComponent } from './jobs-by/jobs-by.component';

const routes: Routes = [
	{path:'', component: HomeComponent},
	{path:'referal-list',canActivate: [AuthGuard], component: ReferalListComponent},
	{path:'announcement-list',canActivate: [AuthGuard], component: AnnouncementListComponent},
	{path:'home',canActivate: [AuthGuard], component:OtherUserHomeComponent},
	{path:'analytic/:name',canActivate: [AuthGuard], component: AnalyticComponent},
	{path:'analytic-map',canActivate: [AuthGuard], component: AnalyticMapComponent},
	{path:'cv-upload',canActivate: [AuthGuard], component: CvUploadComponent},
	{path:'payment', canActivate: [AuthGuard],component: PaymentComponent, children:[
		{
			path:'/:id', canActivate: [AuthGuard],component: PaymentComponent
		}
	]},

	{path:'career-page/:name', component: CarrerComponent},
	{path:'jobs-by/:cat', component: JobsByComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommonUsesRoutingModule { }
