import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { CommonUsesRoutingModule } from './common-uses-routing.module';
import { HomeComponent } from './home/home.component';
import { OwlModule } from 'ngx-owl-carousel';
import { ReferalListComponent } from './referal-list/referal-list.component';
import { AnnouncementListComponent } from './announcement-list/announcement-list.component';
import { PaymentComponent } from './payment/payment.component';
import { AnalyticComponent } from './analytic/analytic.component';
import { AnalyticMapComponent } from './analytic-map/analytic-map.component';
import { OtherUserHomeComponent } from './other-user-home/other-user-home.component';
import { CvUploadComponent } from './cv-upload/cv-upload.component';
import { CarrerComponent } from './carrer/carrer.component';
import { JobsByComponent } from './jobs-by/jobs-by.component';

@NgModule({
  declarations: [HomeComponent, ReferalListComponent, AnnouncementListComponent, PaymentComponent, AnalyticComponent, AnalyticMapComponent, OtherUserHomeComponent, CvUploadComponent, CarrerComponent, JobsByComponent],
  imports: [
    CommonModule,
    CommonUsesRoutingModule,
    OwlModule,
    CoreModule
  ]
})
export class CommonUsesModule { }
