import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-cv-upload',
  templateUrl: './cv-upload.component.html',
  styleUrls: ['./cv-upload.component.css']
})
export class CvUploadComponent implements OnInit {

  registrationForm: FormGroup;
  countries: any=[];
  cities: any=[];
  userData: any={};
  allData: any={};
  experiance_years: any=[];
  industriesData:any=[];
  departmentData:any=[];
  use_role: string;
  emplymentFor:any={};
  empType:any;
  degrees:any;
  cv_file: string;
  validation_error:any;
  filterCities: any=[];
  prevEighteenYearDate: any;
  constructor(private fb:FormBuilder,private message:MessageService ,private commonService: CommonService,
  		private router: Router) {
  		this.asyncInit();
      let prevEighteenYear = (new Date().getFullYear()) - 18;
      let datePrev = new Date().getDate();
      let monthPrev = new Date().getMonth();
      this.prevEighteenYearDate= new Date( Number(monthPrev +1) + '-' + datePrev + '-' + prevEighteenYear);
  	 }

  ngOnInit() {
  	this.userData = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
    this.use_role = this.userData.user_role;
  	window.scrollTo(0, 0);
    console.log(this.userData);

  	this.registrationForm = this.fb.group({
  		first_name:['', [Validators.required]],
  		last_name:['', [Validators.required]],
  		gender:['', [Validators.required]],
  		date_of_birth:['', [Validators.required]],
  		nationality:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		permanent_address: ['', [Validators.required]],
  		present_address:[],
  		email:['', [Validators.required]],
  		phone:['', [Validators.required]],
      cv_file:[],
      middle_name:[],
      industry_id:[],
      department_id:[],
      expriance_id:[],
      employment_type:[],	
      degree_id:[],	
    });
    this.registrationForm.get('industry_id').setValue('');
    this.registrationForm.get('department_id').setValue('');
    this.registrationForm.get('expriance_id').setValue('');
    this.registrationForm.get('employment_type').setValue('');
    this.registrationForm.get('degree_id').setValue('');

    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let selectedCountry =this.countries.find(obj=>obj._id == value);
        this.filterCities = this.cities.filter(obj=>obj.country_code == selectedCountry.code);
      }else{
        this.filterCities =[];
      }
    });
  }

  asyncInit(){
    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
        if(data && data.status ==200){
          this.countries = data.countries;
          this.cities = data.cities;
          this.degrees=data.degrees;
          this.experiance_years = data.other_mst.filter((data) => data.entity == 'experiance_year');
          //this.emplymentFor = data.other_mst.filter((data) => data.entity == 'emplyment_for');
          this.empType = data.other_mst.filter((data) => data.entity == 'employement_type');
          this.industriesData = data.all_industries;
          this.departmentData = data.departments;
        }     
    });
  	/*this.commonService.getAll('/api/common-data')
  		.subscribe(data=>{
  		this.countries = data.countries;
      this.cities = data.cities;
      this.degrees=data.degrees;
      this.experiance_years = data.other_mst.filter((data) => data.entity == 'experiance_year');
      //this.emplymentFor = data.other_mst.filter((data) => data.entity == 'emplyment_for');
      this.empType = data.other_mst.filter((data) => data.entity == 'employement_type');
      this.industriesData = data.all_industries;
      this.departmentData = data.departments;
      console.log(this.experiance_years);
  	}, error=>{});*/
  }

  submitForm(){
    let form = this.formDataSet(this.registrationForm);
    this.commonService.create('/api/student/create', form)
        .subscribe((data)=>{
          if(data.status ==200){
             this.message.success(data.status_text);
             //this.registrationForm.reset();
             this.resetForm();
             this.validation_error ={};
             //this.registrationForm.reset();
          }else if(data.status ==422){
            this.validation_error = data.error;
          }
        }, (error)=>{});
  }



  logoUpload(e){
    this.cv_file = e.target.files[0].name;
    console.log(this.cv_file);
    this.registrationForm.get('cv_file').setValue(e.target.files[0]);
  }

   formDataSet(form){
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
       formData.append(key, form.get(key).value);
   });

   return formData;
 }

 resetForm(){
   this.cv_file ='';
   this.registrationForm.patchValue({
      first_name:'',
      last_name:'',
      gender:'',
      date_of_birth:'',
      nationality:'',
      country_id:'',
      city_id:'',
      permanent_address: '',
      present_address:'',
      email:'',
      phone:'',
      cv_file:'',
      middle_name:'',
      industry_id:'',
      department_id:'',
      expriance_id:'',
      employment_type:'', 
      degree_id:'',    
   });


   /*this.registrationForm.get('first_name').setValue('');
   this.registrationForm.get('last_name').setValue('');
   this.registrationForm.get('gender').setValue('');
   this.registrationForm.get('date_of_birth').setValue('');
   this.registrationForm.get('nationality').setValue('');
   this.registrationForm.get('country_id').setValue('');
   this.registrationForm.get('city_id').setValue('');
   this.registrationForm.get('permanent_address').setValue('');
   this.registrationForm.get('present_address').setValue('');
   this.registrationForm.get('email').setValue('');
   this.registrationForm.get('phone').setValue('');*/
 }

}
