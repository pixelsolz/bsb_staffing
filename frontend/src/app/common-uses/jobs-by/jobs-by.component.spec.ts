import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsByComponent } from './jobs-by.component';

describe('JobsByComponent', () => {
  let component: JobsByComponent;
  let fixture: ComponentFixture<JobsByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsByComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
