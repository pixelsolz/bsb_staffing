import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {Global} from '../../global';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
declare var $ :any;

@Component({
  selector: 'app-jobs-by',
  templateUrl: './jobs-by.component.html',
  styleUrls: ['./jobs-by.component.css']
})
export class JobsByComponent implements OnInit {

  @HostListener("window:scroll", ["$event.target"])
  scrollHandler(elem) {
     var hT = $('#add_moreDiv').offset().top,
       hH = $('#add_moreDiv').outerHeight(),
       wH = $(window).height(),
       wS = $(window).scrollTop();
   if (wS >= (hT+hH-wH) && !this.isSearchByAlpha){
       this.loadMoreData();
   }
    if($(window).scrollTop() <= 325){
      window.location.hash ='';
    }
  }

  category: string;
  resultList: any=[];
  categoryType: string;
  searchInput: string='';
  alphaArr:any;
  totalDataCount : number;
  isSearchByAlpha: boolean;
  constructor(private commonService: CommonService, private router: Router,
  	private activeRoute: ActivatedRoute, private global: Global) { 
    this.isSearchByAlpha = false;
  	router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
  		this.category = this.activeRoute.snapshot.paramMap.get("cat");
		  if(!this.isSearchByAlpha){
              this.asyncInit(this.category);
      }
  		
  	});
  }

  ngOnInit() {
	window.scrollTo(0, 0)
  }

  asyncInit(cat){
    console.log(cat);
    let AlphaStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    this.alphaArr = AlphaStr.split('');  
  	this.commonService.getAll('/api/employee/job-search/cat?cat='+ cat+'&search=' + this.searchInput)
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			if(data.status ==200){
  				this.resultList = data.data;
          this.totalDataCount = data.total_count;
  				this.categoryType = this.getType(cat);
				//console.log(data.data);
				
  			}
  			
  		}, (error)=>{});
  }

  searchData(data){
    this.isSearchByAlpha = false;
    let limit = (this.resultList.length + 100);
    let cat = this.category;
    let search = this.searchInput;
    this.commonService.getAll('/api/employee/job-search/cat?cat='+ cat+ '&search='+ search +'&limit=' + limit)
      .subscribe((data)=>{
        if(data.status ==200){
          this.resultList = data.data;
          this.totalDataCount = data.total_count;
          this.categoryType = this.getType(cat);
        
        }
        
      }, (error)=>{}); 
  }

  loadMoreData(){
    if(this.resultList.length < this.totalDataCount){
    let limit = (this.resultList.length + 100);
    let cat = this.category;
    let search = this.searchInput;
    this.commonService.getAll('/api/employee/job-search/cat?cat='+ cat+ '&search='+ search +'&limit=' + limit)
      .subscribe((data)=>{
        if(data.status ==200){
          this.resultList = data.data;
          this.totalDataCount = data.total_count;
          this.categoryType = this.getType(cat);
        
        }
        
      }, (error)=>{}); 
    }  
  }

  dataShowByAlpha(id){
    this.isSearchByAlpha = true;
      let cat = this.category;
      if(this.resultList.length ===this.totalDataCount && this.searchInput ==''){
              window.location.hash = id;
                let pos = window.pageYOffset;
                if (pos > 0) {
                    window.scrollTo(0, pos - 90); // how far to scroll on each step
                } 
      }else{
       this.searchInput = '';
       this.commonService.getAll('/api/employee/job-search/cat?cat='+ cat+ '&searchById='+ id)
      .subscribe((data)=>{
        if(data.status ==200){
          this.resultList = data.data;
          this.totalDataCount = data.total_count;
          this.categoryType = this.getType(cat);  
            setTimeout(()=>{    
                window.location.hash = id;
                let pos = window.pageYOffset;
                if (pos > 0) {
                    window.scrollTo(0, pos - 90); // how far to scroll on each step
                } 
             }, 500);
      
        }       
      }, (error)=>{});        
      }


  }


  getAlphaid(name,i){ 
    let str = name.trim().charAt(0);
    return str;

  }

  getType(cat){
  	let value='';
  	switch (cat) {
  		case "country":
  			value = 'Country';
  			break;
  		case "city":
  			value = 'City';
  			break;
  		case "industry":
  			value = 'Industry';
  			break;
  		case "role":
  			value = 'Job Role';
  			break;
  		default:
  			// code...
  			break;
  	}

  	return value;

  }
  searchdetail(id){
	this.router.navigate(['/search-job/detail'],{queryParams:{cat: this.category, cat_id: id}});
}

  scroleToTop(){
    //window.scroll(0,0);
    $('html, body').animate({
          scrollTop:0
    }, 800, function(){ 
    });
  }
  
}
