import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import {Global} from '../../global';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-other-user-home',
  templateUrl: './other-user-home.component.html',
  styleUrls: ['./other-user-home.component.css']
})
export class OtherUserHomeComponent implements OnInit {

  userData: any={};
  allData: any={};
  use_role: string;
  announcements: any=[];
  referrals: any=[];
  isLoaded:boolean=false;
  //mySlideImages = ['assets/images/hotelImg1.png','assets/images/hotelImg2.png','assets/images/hotelImg3.png'];
  mySlideImages:any = [];
  myCarouselOptions={items: 1, dots: true, nav: false, autoplay:true, loop:true, animateOut: 'fadeOut', margin:0};
  constructor(private commonService: CommonService,private global: Global,private sanitizer: DomSanitizer) { 
  	this.userData = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
    console.log(this.userData);
    let gallery_image = this.userData.user_detail.gallery_images;
    if(gallery_image){
      this.mySlideImages = gallery_image.split(",");
    }
    this.use_role = this.userData.user_role;
  	window.scrollTo(0, 0);
  	this.asyncInit();
  }

  ngOnInit() {
  	
  }

  asyncInit(){
  	this.commonService.getAll('/api/other-user/home')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.allData = data.data;
        this.referrals = data.data.benefit_data.filter(obj=>obj.type ==1);
        this.announcements = data.data.benefit_data.filter(obj=>obj.type ==2);
        //console.log(this.allData);
        this.isLoaded = true;
  		},(error)=>{});
  }

}
