import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
	paymentForm:FormGroup;
	showloader :boolean;
	isSubmit :boolean;
	package_id:any;
	package:any;
  payment_history:any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router,private activeRoute: ActivatedRoute ,private commonService: CommonService) { 
		this.package_id = this.activeRoute.snapshot.queryParamMap.get("service") ? this.activeRoute.snapshot.queryParamMap.get("service"):'';
		console.log(this.package_id);
		  if(this.package_id){
        this.commonService.show('/api/package-details', this.package_id)
            .subscribe((data)=>{
            	console.log(data.package);
            	this.package = data.package;
              this.setFormValue(this.package);
            }, (error)=>{});
        this.paymentHistory();  
      }else{
        this.paymentHistory();
      }
  }

  ngOnInit() {
  	this.paymentForm= this.fb.group({
        payment_date :[''],
        ammount:[''],
        remarks:[''],
        id:[''],
       
    });
    
  }

  setFormValue(data){
  	let today = new Date();
		let dd = today.getDate();
		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();
		let current_date = dd+'/'+mm+'/'+yyyy;
		//console.log(current_date);
  	this.paymentForm.patchValue({
  		payment_date : current_date,
  		ammount : data.fees,
  		id : data._id,
  	});
  }

  submitPay(){
  	this.showloader = true;
  	this.isSubmit = true;
    if(this.paymentForm.invalid){
      this.showloader = false;
      return;
    }
    this.commonService.create('/api/payment', this.paymentForm.value)
        .subscribe((data)=>{
          this.commonService.callFooterMenu(1);
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.message.success('Successfully Payment');
            this.package_id='';
            this.paymentHistory();
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  paymentHistory(){
    this.commonService.getAll('/api/payment-history')
          .subscribe((data)=>{
            this.payment_history = data.payment_history;
          },(error)=>{});
  }
}
