import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-referal-list',
  templateUrl: './referal-list.component.html',
  styleUrls: ['./referal-list.component.css']
})
export class ReferalListComponent implements OnInit {

  referalList: any=[];
  referrals:FormArray;
  referalForm: FormGroup;
  messageForm: FormGroup;
  show_form: boolean;
  is_submitted: boolean;
  referral_id:string;
  emailData: any ={};
  templates: any=[];
  userData: any={};
  isSubmit: boolean =false;
  selectedBody:string='';
  constructor(private fb: FormBuilder, private commonService: CommonService,
   private messageService: MessageService, private activeRoute: ActivatedRoute) {
    this.userData = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
   	console.log(this.userData);
     this.asyncInit();
   	this.show_form = false;
    this.is_submitted = false; 
    this.referral_id = this.activeRoute.snapshot.queryParams['referal_id'];
   }

  ngOnInit() {
    window.scrollTo(0, 0); 
  	this.referalForm = this.fb.group({
       benefit_id:'',
  		 referrals: this.fb.array([this.initReferal()])
  	});

    this.referalForm.get('benefit_id').setValue(this.referral_id);
  	this.messageForm = this.fb.group({
  		message: ['',[Validators.required]],
      subject_id:['',[Validators.required]],
      agree:'',
      signature:''
  	});
    this.messageForm.get('subject_id').valueChanges.subscribe(value=>{
        if(value){
          let temp = this.templates.find(obj=>String(obj._id) == String(value));
          this.selectedBody =temp.temp_content;
          this.messageForm.get('message').setValue(temp.temp_content);
        }
    });
  }

  initReferal(): FormGroup{
    return this.fb.group({
      referal_name: ['',[Validators.required]],
      referal_email: ['',[Validators.required, Validators.email]],
      referal_designation: ['',[Validators.required]],
      referal_company: ['',[Validators.required]],
      referal_company_website: ['']
    });
  }

  addReferalArray(): void {
    this.is_submitted = false;
    this.referrals = this.referalForm.get('referrals') as FormArray;
    this.referrals.push(this.initReferal());
  }

  deleteReferalArray(index: number){
    this.is_submitted =false;
    this.referrals = this.referalForm.get('referrals') as FormArray;
    this.referrals.removeAt(index);
  }

  get formData() { return <FormArray>this.referalForm.get('referrals')['controls']; }

  asyncInit(){
  	this.commonService.getAll('/api/referral-list')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.referalList = data.data;
        this.templates = data.templates.filter(obj=>obj.entity_id == this.userData.role._id);;
        //console.log(data.templates);
  		}, (error)=>{});
  }

  entervalue(value,index){
    console.log(this.emailData[index])
    this.referalForm.get('referrals')['controls'][index].get('referal_email').setValue((this.emailData[index] && this.emailData[index].length) ?this.emailData[index][0].value:"");
    
  }

  submitForm(){
    console.log(this.referalForm.value);
    this.is_submitted =true;
    if(this.is_submitted && this.referalForm.invalid){
      return false;
    }
  	this.commonService.create('/api/create/new-referral', this.referalForm.value)
  		.subscribe((data)=>{
        this.is_submitted = false;
  			if(data.status ==200){
  				this.referalList = data.data;
          this.emailData = {};
  				this.referalForm.reset();
  				this.show_form = false;
  				this.messageService.success(data.status_text);
          if(this.referrals.length >1){
            for(let i=1; i<=this.referrals.length; i++){
              this.deleteReferalArray(i);
            }
          }

  			}
  		}, (error)=>{});
  }

  sendMesage(){
  	if(this.messageForm.get('agree').value ==''){
      this.messageService.error('Please accept the terms & conditions');
      return false;
    }
    this.isSubmit = true;
    if(this.messageForm.invalid){
      return false;
    }
    
    this.commonService.create('/api/referral/message-send', this.messageForm.value)
        .subscribe(data=>{
          if(data.status ==200){
            this.messageService.success(data.status_text);
            this.isSubmit = false;
            this.selectedBody='';
            this.messageForm.reset();
          }else if(data.status ==422){
            this.messageService.warning(data.status_text);
          }
        }, error=>{});
  }



}
