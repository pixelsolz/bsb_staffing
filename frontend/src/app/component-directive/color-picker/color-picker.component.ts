import { Component,OnInit,ElementRef,HostListener,Output,EventEmitter,Input } from '@angular/core';
import { ColorEvent } from 'ngx-color';
declare var $: any;

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class ColorPickerComponent implements OnInit {

  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
      
    if($(event.target).hasClass('saturation-white') || $(event.target).hasClass('color-hue-container') || 
       $(event.target).hasClass('sketch-sliders') || $(event.target).hasClass('alpha-container') || 
       $(event.target).hasClass('swatch') ||  (event.target.name && event.target.name ==this.field_name )){

    }else{
    	this.sendClickEvent(isInside);
    }

    /*if ((!isInside && event.target.name != this.field_name) || !event.target.name ) {
    	//alert();
      //this.sendClickEvent(isInside);
    }*/
  }

  @Input() field_name: string;
  @Input() defaultColor: string;
  @Output() sendDataEvent = new EventEmitter < any > ();
  @Output() checkOutsideEvent = new EventEmitter < any > ();

  constructor(private eRef: ElementRef) { }

  ngOnInit() {
  }

  sendClickEvent(status) {
    this.checkOutsideEvent.emit({
      status: status,
      field_name: this.field_name
    });
  }

  sendData(event){
  	this.sendDataEvent.emit({event:event, field_name: this.field_name});
  }

}
