import {Component,OnInit,ElementRef,HostListener,Output,EventEmitter,Input} from '@angular/core';

@Component({
  selector: 'app-input-multi-value',
  templateUrl: './input-multi-value.component.html',
  styleUrls: ['./input-multi-value.component.css']
})
export class InputMultiValueComponent implements OnInit {


 @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if (!isInside) {
      this.sendClickEvent(isInside);
    }
  }
  constructor(private eRef: ElementRef) { }

  ngOnInit() {
  }

  sendClickEvent(status) {
   /* this.checkOutsideEvent.emit({
      status: status,
      field_name: this.field_name
    });*/
  }
  
}
