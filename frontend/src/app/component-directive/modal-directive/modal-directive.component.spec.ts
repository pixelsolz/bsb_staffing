import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDirectiveComponent } from './modal-directive.component';

describe('ModalDirectiveComponent', () => {
  let component: ModalDirectiveComponent;
  let fixture: ComponentFixture<ModalDirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
