import { Component, OnInit, ElementRef,HostListener,Output,EventEmitter,Input } from '@angular/core';
import * as $ from 'jquery';
import * as M from 'src/assets/js/materialize.min.js';
import { CommonService } from '../../services/common.service';
@Component({
  selector: 'app-modal-directive',
  templateUrl: './modal-directive.component.html',
  styleUrls: ['./modal-directive.component.css']
})
export class ModalDirectiveComponent implements OnInit {

  @Input() modalId:string;
  modalInstance: any;
  constructor(private commonService: CommonService) {
  		this.commonService.checkmateriallize.subscribe(call=>{
  			alert();
  			this.modalOpen();
  		});
   }

  ngOnInit() {
  	
  	document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    let options ={};
    this.modalInstance = M.Modal.init(elems, options);
  	});

  }

  modalOpen(){
    var elems = document.querySelectorAll('.modal');
    let options ={};
    var instance = M.Modal.getInstance(elems);
    instance.open();


  }
  modalClose(){
  	this.modalInstance.close();
  }

}
