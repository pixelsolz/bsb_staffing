import {Component,OnInit,ElementRef,HostListener,Output,EventEmitter,Input} from '@angular/core';
import {CommonService} from '../../services/common.service';
declare var $: any;

@Component({
  selector: 'app-multi-sub-select',
  templateUrl: './multi-sub-select.component.html',
  styleUrls: ['./multi-sub-select.component.css']
})
export class MultiSubSelectComponent implements OnInit {

  storeMultiSelect: any = {
    undergradute: [],
  };

  selectAllModel: any = [];
  checkboxModel: any = [];
  searchData: string;

  dataArray: any = [];
  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if (!isInside && event.target.name != this.field_name) {
      this.sendClickEvent(isInside);
    }
  }

  @Output() sendDataEvent = new EventEmitter < any > ();
  @Output() sendSubDataEvent = new EventEmitter < any > ();
  @Output() checkOutsideEvent = new EventEmitter < any > ();
  @Output() multidataSelect = new EventEmitter < any > ();
  @Input() mainDataArray: any = [];
  @Input() storedValue: any = [];
  @Input() storedName: any = [];
  @Input() storedSubValue: any=[];
  @Input() field_name: string;
  @Input() maxLength: number =0;
  @Input() maxSubLength: number =0;
  constructor(private eRef: ElementRef, private commonService: CommonService) {}

  ngOnInit() {
    this.dataArray = Object.entries(this.mainDataArray);
    this.dataArray.forEach((data, i) => {
      this.selectAllModel[i];
      data[1].forEach(value => {
        this.checkboxModel[value.id];
      });
    });
    if (this.storedValue.length) {
      this.dataArray.forEach((data, i) => {
        data[1].forEach(checkedValue => {
          checkedValue.select = this.storedValue.indexOf(checkedValue._id) != -1 ? true : false;
          if(checkedValue.select && checkedValue.child && checkedValue.child.length){
             checkedValue.child.map(subcheck=> subcheck.select = this.storedSubValue.find(obj=>obj.value == subcheck._id)? true: false );            
          }else if(!checkedValue.select && checkedValue.child && checkedValue.child.length){
             checkedValue.child.map(subcheck=> subcheck.select = false);            
          }

          if(this.maxLength > 0 && this.storedValue.length == this.maxLength){
            checkedValue.isDisabled = (checkedValue.select)?false: true;
          }else{
            checkedValue.isDisabled  = false;
          }

        });
        this.selectAllModel[i] = data[1].every((checkedValue, i) => checkedValue.select);
      });
    }else{
      this.dataArray.forEach((data, i) => {
        data[1].forEach(checkedValue => {
          checkedValue.select = false;
          checkedValue.isDisabled = false;
          if(checkedValue.child){
            checkedValue.child.map(subcheck=> subcheck.select = false);
          }
        });
      });
    }

    this.dataArray[0][1].forEach((data, i)=>{
      if(data && data.child && data.child.length >0){
        data.child.forEach(subcheck=>{
          if(this.maxSubLength > 0 && this.storedSubValue.length == this.maxSubLength){
             subcheck.isDisabled =(subcheck.select)?false: true
          }else{
            subcheck.isDisabled = false
          }
        })
      }
    })
  }

  getChange(ischecked, value, name) {
    this.sendDataEvent.emit({
      event: ischecked,
      value: value,
      name: name,
      storedValue: this.storedValue,
      storedName: this.storedName,
      field_name: this.field_name
    });
    this.dataArray.forEach((data, i) => {
      this.selectAllModel[i] = data[1].every((checkedValue, i) => checkedValue.select);
      if(!ischecked){
        let filterCurrentUnChecked =data[1].find(checkedValue=> checkedValue.name == name);
        if(filterCurrentUnChecked &&  filterCurrentUnChecked.child && filterCurrentUnChecked.child.length){
          filterCurrentUnChecked.child.map(subcheck=>subcheck.select = false);
          filterCurrentUnChecked.child.map(subcheck=>subcheck.isDisabled = false);
        }
      }
      
      // this is for disabled check box 
      data[1].forEach(checkedValue => {
          if(this.maxLength > 0 && this.storedValue.length == this.maxLength){
            checkedValue.isDisabled = (checkedValue.select)?false: true;
          }else{
            checkedValue.isDisabled  = false;
          }
      });      

    });
  }

  getSubChange(ischecked, value, parentValue, parentName, name){

    this.sendSubDataEvent.emit({
      event: ischecked,
      value: value,
      parent_value: parentValue,
      parent_name: parentName,
      storedSubValue: this.storedSubValue,
      field_name: this.field_name
    });	
    this.dataArray[0][1].forEach((data, i)=>{
      if(data.child.length >0){
        data.child.forEach(subcheck=>{
          if(this.maxSubLength > 0 && this.storedSubValue.length == this.maxSubLength){
             subcheck.isDisabled =(subcheck.select)?false: true
          }else{
            subcheck.isDisabled = false
          }
        })
      }
    })
  }


  sendClickEvent(status) {
    this.checkOutsideEvent.emit({
      status: status,
      field_name: this.field_name
    });
  }

  setCheckedAllModel(ischecked, index) {
    let arrayValue = [];
    let arrayName = [];
    if (ischecked) {
      this.dataArray[index][1].forEach(checkedValue => {
        arrayValue.push(Number(checkedValue.id));
        arrayName.push(checkedValue.name);
        checkedValue.select = true;
      });
    } else {
      this.dataArray[index][1].forEach(checkedValue => {
        arrayValue.push(Number(checkedValue.id));
        arrayName.push(checkedValue.name);
        checkedValue.select = false;
      });
    }
    this.multidataSelect.emit({
      event: ischecked,
      arrValue: arrayValue,
      arrName: arrayName,
      storedValue: this.storedValue,
      storedName: this.storedName,
      field_name: this.field_name
    });
  }

  setCheckedAll(event, index) {
    let arrayValue = [];
    if (event.target.checked) {
      this.dataArray[index][1].forEach(checkedValue => {
        arrayValue.push(Number(checkedValue.id));
        checkedValue.select = true;
      });
    } else {
      this.dataArray[index][1].forEach(checkedValue => {
        arrayValue.push(Number(checkedValue.id));
        checkedValue.select = false;
      });
    }
    this.multidataSelect.emit({
      event: event,
      arrValue: arrayValue,
      storedValue: this.storedValue,
      storedName: this.storedName,
      field_name: this.field_name
    });

  }

}
