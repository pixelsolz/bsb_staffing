import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiselectNosearchComponent } from './multiselect-nosearch.component';

describe('MultiselectNosearchComponent', () => {
  let component: MultiselectNosearchComponent;
  let fixture: ComponentFixture<MultiselectNosearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiselectNosearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiselectNosearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
