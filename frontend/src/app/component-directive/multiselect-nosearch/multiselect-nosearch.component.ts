import { Component, OnInit,ElementRef,HostListener,Output,EventEmitter,Input } from '@angular/core';

@Component({
  selector: 'app-multiselect-nosearch',
  templateUrl: './multiselect-nosearch.component.html',
  styleUrls: ['./multiselect-nosearch.component.css']
})
export class MultiselectNosearchComponent implements OnInit {

  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if (!isInside && event.target.name != 'select_multiple_field') {
      this.sendClickEvent(isInside);
     }
   }

  @Output() sendDataEvent = new EventEmitter < any > ();
  @Output() checkOutsideEvent = new EventEmitter < any > ();
  @Input() mainDataArray: any=[];
  @Input() storedValue: any = [];
  @Input() hasSearch: string;
  dataArray: any = [];
  searchData: string;
  constructor(private eRef: ElementRef) { }

  ngOnInit() {
  	this.dataArray = this.mainDataArray;
    this.dataArray.forEach((checkedValue, i)=>{
      if(this.storedValue.indexOf(checkedValue._id)!=-1){
        checkedValue.select =true;
      }else{
        checkedValue.select =false;
      }
    });
  }

  sendClickEvent(status) {
    this.checkOutsideEvent.emit({
      status: status
    });
  }

  getChange(ischecked, value, name) {
    this.sendDataEvent.emit({
      event: ischecked,
      value: value,
      name: name
    });

  }

}
