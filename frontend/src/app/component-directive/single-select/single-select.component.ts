import { Component, OnInit, ElementRef,HostListener,Output,EventEmitter,Input } from '@angular/core';

@Component({
  selector: 'app-single-select',
  templateUrl: './single-select.component.html',
  styleUrls: ['./single-select.component.css']
})
export class SingleSelectComponent implements OnInit {

  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if (!isInside && event.target.name != 'single_select') {
      this.sendClickEvent(isInside);
    }
  }
  searchData: string;
  dataArray: any = [];
  @Output() sendDataEvent = new EventEmitter < any > ();
  @Output() checkOutsideEvent = new EventEmitter < any > ();
  @Input() field_name: string;
  @Input() mainDataArray: any=[];
  constructor(private eRef: ElementRef) { }

  ngOnInit() {
  	this.dataArray = Object.entries(this.mainDataArray);
  }

  sendClickEvent(status) {
    this.checkOutsideEvent.emit({
      status: status,
      field_name: this.field_name
    });
  }

  getChange(ischecked, value, name){
  	this.sendDataEvent.emit({
  	  event: ischecked,
      value: value,
      name: name,
      field_name: this.field_name
  	});
  }


}
