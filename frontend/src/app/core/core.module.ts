import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {RequestInterceptor} from './../RequestInterceptor';
import { MultiSelectComponent } from './../component-directive/multi-select/multi-select.component';
import { SearchPipe } from './../filters/search.pipe';
import { LanguagePipe } from './../filters/language.pipe';
import { SearchCategoryPipe } from './../filters/search-category.pipe';
import { MultiselectNosearchComponent } from './../component-directive/multiselect-nosearch/multiselect-nosearch.component';
import { ModalDirectiveComponent } from './../component-directive/modal-directive/modal-directive.component';
import { MultiSubSelectComponent } from './../component-directive/multi-sub-select/multi-sub-select.component';
import { DateInputDirective } from './../directives/date-input.directive';
import { SingleSelectComponent } from './../component-directive/single-select/single-select.component';
import { ColorPickerComponent } from './../component-directive/color-picker/color-picker.component';
//import { NgxUiLoaderModule } from  'ngx-ui-loader';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule,
  MatInputModule, MatAutocompleteModule, MatRadioModule, MatTabsModule,
  MatSelectModule, MatMenuModule, MatProgressBarModule, MatProgressSpinnerModule,
  MatTableModule, MatPaginatorModule, MatDatepickerModule} from '@angular/material';
import { TagInputModule } from 'ngx-chips';
import { NgxSummernoteModule } from 'ngx-summernote';
//import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { ColorSketchModule } from 'ngx-color/sketch';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import {LoginComponent} from './../auth/login/login.component';
import {RegistrationComponent} from './../auth/registration/registration.component';
import { ForgetPasswordComponent } from './../auth/forget-password/forget-password.component';

import { AgentSignupComponent } from './../auth/agent-signup/agent-signup.component';
import { CollegeSignupComponent } from './../auth/college-signup/college-signup.component';
import { FranchiseSignupComponent } from './../auth/franchise-signup/franchise-signup.component';
import { TrainerSignupComponent } from './../auth/trainer-signup/trainer-signup.component';
import { OtherUserSigninComponent } from './../auth/other-user-signin/other-user-signin.component';
import { OtherHomeForgotPasswordComponent } from './../auth/other-home-forgot-password/other-home-forgot-password.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [MultiSelectComponent,SearchPipe,SearchCategoryPipe, LanguagePipe,
    MultiselectNosearchComponent, ModalDirectiveComponent,DateInputDirective, SingleSelectComponent,
    MultiSubSelectComponent, ColorPickerComponent,LoginComponent,RegistrationComponent, ForgetPasswordComponent,
    AgentSignupComponent,CollegeSignupComponent,FranchiseSignupComponent,TrainerSignupComponent, OtherUserSigninComponent, OtherHomeForgotPasswordComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatTabsModule,
    MatSelectModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule, 
    TagInputModule,
    NgxSummernoteModule,
    ColorSketchModule,
    JwSocialButtonsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD14Wf8FCUI6z6X-17Vw-S0em4K8NxfGhU'
    })
  ],
  exports:[
  	ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatTabsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule,   
    MultiSelectComponent,
    SearchPipe,
    SearchCategoryPipe, 
    LanguagePipe,
    MultiselectNosearchComponent,
    ModalDirectiveComponent,
    DateInputDirective,
    SingleSelectComponent,
    MultiSubSelectComponent,
    ColorPickerComponent,
    TagInputModule,
    NgxSummernoteModule,
    ColorSketchModule,
    JwSocialButtonsModule,
    LoginComponent,
    RegistrationComponent,
    ForgetPasswordComponent,
    AgentSignupComponent,
    CollegeSignupComponent,
    FranchiseSignupComponent,
    TrainerSignupComponent,
    OtherUserSigninComponent,
    OtherHomeForgotPasswordComponent,
    AgmCoreModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true }]
})
export class CoreModule { }
