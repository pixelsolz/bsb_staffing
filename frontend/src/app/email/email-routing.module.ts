import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { InboxListComponent } from './inbox-list/inbox-list.component';
import { SentboxListComponent } from './sentbox-list/sentbox-list.component';
import { MailSidenavComponent } from './mail-sidenav/mail-sidenav.component';

const routes: Routes = [
  { path: 'mail/:type', component: InboxListComponent },
  // {path:'sentbox', component:SentboxListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailRoutingModule { }
