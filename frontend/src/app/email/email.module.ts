import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthModule} from '../auth/auth.module';
import { CommonUsesModule } from '../common-uses/common-uses.module';
import { CmsModule } from '../cms/cms.module';
import { CoreModule } from '../core/core.module';
import { EmailRoutingModule } from './email-routing.module';
import { InboxListComponent } from './inbox-list/inbox-list.component';
import { SentboxListComponent } from './sentbox-list/sentbox-list.component';
import { MailSidenavComponent } from './mail-sidenav/mail-sidenav.component';
import { TemplatePreviewComponent } from './template-preview/template-preview.component';

@NgModule({
  declarations: [InboxListComponent, SentboxListComponent, MailSidenavComponent, TemplatePreviewComponent],
  imports: [
    CommonModule,
    CoreModule,
    AuthModule,
    EmailRoutingModule,
    CommonUsesModule
  ]
})
export class EmailModule { }
