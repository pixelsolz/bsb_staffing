import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { PagerService } from '../../services/pager.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-inbox-list',
  templateUrl: './inbox-list.component.html',
  styleUrls: ['./inbox-list.component.css']
})
export class InboxListComponent implements OnInit , OnDestroy{
  count_data: any;
  resourceUrl: string = '/api/mail';
  active_page: number;
  mails: any = [];
  singlemail: any;
  isEmployee: boolean;
  param: string;
  mailData: any = [];
  user: any;
  showContent: boolean= false;
  selectAll: boolean=false;
  checkModel: any={};
  checkedData: any=[];
  pager: any = {};
  pagedItems: any = [];
  queryParam: any;
  showLabel: boolean=false;
  all_labels: any=[];
  labelModel: any={};
  routeUrl:string;
  searchText: string;
  subscription: any; 
  pageCall: number =0;
  totalCount: number =0;
  currentPageNo:number=0;
  totalPageNo:number=0;
  isLoading:boolean = false;
  baseUrlPath: string;
  constructor(private pageService: PagerService, private router: Router, private commonService: CommonService,
    private global: Global, private activeRoute: ActivatedRoute, private messageService: MessageService) {
    this.baseUrlPath = this.global.baseUrl
        this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
        this.routeUrl = (this.user && this.user.user_role =='employer')? '/employer/mail':'/mail';
        this.subscription = router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e) => {
           this.param = this.activeRoute.snapshot.paramMap.get('type');
           this.queryParam = this.activeRoute.snapshot.queryParams['id']? this.activeRoute.snapshot.queryParams['id']:'';
           if(e.url.includes('mail')){            
              this.totalCount =0;
              this.currentPageNo =0;
              this.totalPageNo =0;            
             this.getData(this.param);   
             this.showContent =false;        
           }

        });
        this.isEmployee = true;
    }

  ngOnInit() {
    this.commonService.emailUpdateCheck.subscribe(data=>{
      this.totalCount =0;
      this.currentPageNo =0;
      this.totalPageNo =0;  
      this.getData(this.param);
           
    });
    this.pageCall +1;
  }

  ngOnDestroy() {
        this.subscription.unsubscribe();
    }

getData(param) {
  let offset = this.currentPageNo * 10;
  this.isLoading = true;
  this.commonService.getAll(this.resourceUrl + '?param=' + param + (this.queryParam? '&label_id=' +this.queryParam:'') + '&offset=' + offset)
  .subscribe((data) => {
    this.isLoading = false
    this.all_labels = data.labels;
    this.count_data = data.data_count;
    this.currentPageNo = data.current_page;
    this.totalCount = data.count;

    //let datacheck = data.data.filter(obj=>obj.mail_receiver && obj.mail_sender)

    this.mails = data.data.map(obj => ({
        _id: obj._id, sender_id: obj.mail_sender_id, user_name: obj.mail_receiver_detail.first_name,mail_id:obj.mail_receiver.email,sender_name:!_.isEmpty(obj.mail_sender_detail.company_name)?obj.mail_sender_detail.company_name: obj.mail_sender_detail.name,sender_email:obj.mail_sender.email,mail_body: obj.mail_body,mail_sender:obj.mail_sender,mail_sender_detail:obj.mail_sender_detail, 
        mail_subject: obj.mail_subject,mail_summary:obj.mail_summary ,is_star:obj.is_star ,attached_file:obj.attached_file, video_links: obj.video_links,created_at:obj.created_at,created_date:(obj.updated_diff != obj.day_diff)?obj.updated_diff:obj.day_diff, children:obj.children,job_questions:(obj.job_questions? obj.job_questions :''),is_resume_trans:(obj.is_resume_trans ?obj.is_resume_trans:'')  ,template :(obj.template_id ?obj.template:'' )
       })
   );
  this.mailData = this.mails;
  }, (error) => {  this.isLoading = false });
 }

  getSearchData(index) {
    this.active_page = index;
    this.commonService.getAll(this.resourceUrl + '?off_set=' + this.active_page)
        .subscribe((data) => {
        this.mails = data.data;
    }, (error) => { });
  }

  getMapData =(data, param)=>{
    return data.map(obj=>obj[param]);
  }


  setPage(page: number) {
    this.currentPageNo = page -1;
    this.getData(this.param);
  }

  mailbodyopen(id) {
    this.singlemail = this.mails.filter((data) => data._id == id);
    this.showContent = true;
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

 checkIsArray =(value)=>{
   return Array.isArray(value)
 }

 setAttachedFileUrl =(url)=>{
   if(url.includes('//assets.bsbstaffing.com')){
     return url;     
   }else if(url.includes('//d3q00ceu4wkbfz.cloudfront.net')){
     return url;
   }else{
     return this.global.AWS_URL +'/' + url;
   }
 }

  toggleAllSelected(value){
    if(value){
      this.mailData.forEach((obj)=>{this.checkModel[obj._id] = true});
    }else{
      this.mailData.forEach((obj)=>{this.checkModel[obj._id] = false});
    }

   this.checkedData =this.mailData.filter(obj=>this.checkModel[obj._id] ==true);
   this.selectAll = (this.checkedData.length ==this.mailData.length)? true:false; 
  }

  selectIndividual(value){
    this.checkedData = this.mailData.filter(obj=>this.checkModel[obj._id] ==true);
    this.selectAll = (this.checkedData.length ==this.mailData.length)? true:false; 
  }

  deleteMultipleEmail(){
    let deleteIds = this.checkedData.map(obj=>obj._id);
    this.commonService.deleteWithPost('/api/mail/delete', deleteIds)
        .subscribe(data=>{
          if(data.status ==200){
              this.messageService.success(data.status_text);
              deleteIds.forEach((value)=>{
                let index = this.mailData.findIndex(obj=>obj._id == value);
                this.mailData.splice(index, 1);
              });
              this.mailData.forEach((obj)=>{this.checkModel[obj._id] = false});
              this.selectAll = false;
              //this.setPage(1);
          }
         
        },error=>{});
  }

  deleteEmail(id){
    this.commonService.deleteWithPost('/api/mail/single-delete',id)
        .subscribe(data=>{
          if(data.status ==200){
              this.messageService.success(data.status_text);
              let index = this.mailData.findIndex(obj=>obj._id == id);
              this.mailData.splice(index, 1);
              this.showContent =false;
              //this.setPage(1);
          }
         
        },error=>{});
  }

  restoreMail(){
    let deletedIds = this.checkedData.map(obj=>obj._id);
        this.commonService.deleteWithPost('/api/mail/restore', deletedIds)
        .subscribe(data=>{
          if(data.status ==200){
              this.messageService.success(data.status_text);
              deletedIds.forEach((value)=>{
                let index = this.mailData.findIndex(obj=>obj._id == value);
                this.mailData.splice(index, 1);
              });
              this.mailData.forEach((obj)=>{this.checkModel[obj._id] = false});
              this.selectAll = false;
              //this.setPage(1);
          }
         
        },error=>{});
  }

  markStar(id){
    this.commonService.getAll('/api/mail/markstar/' + id)
          .subscribe(data=>{
            if(data.status ==200){
                let specificData = this.mailData.find(obj=>obj._id == id);
                specificData.is_star =1;
                this.messageService.success(data.status_text);
            }
          },error=>{});
  }

  callreply(mail){
    this.commonService.callComposeMail({reply_data:mail});
  }

  sendToLabel(event, value){
    let email_ids = this.checkedData.map(obj=>obj._id);
    let form ={mail_ids:email_ids, label_id:value};
    this.commonService.create('/api/mail/label/sendto-label',form)
        .subscribe(data=>{
          if(data.status==200){
            this.checkedData ='';
            email_ids.forEach(value=>{this.checkModel[value]=false;});          
            this.showLabel =false;
            this.labelModel[value]=false;
           // this.setPage(1);
            this.router.navigate(['/'+ this.routeUrl,'label'],{queryParams:{id:value}});
          }
        },error=>{});
  }

 getInboxValue(message: string){
   this.showContent =false; 
 }

 getFileName(name){
  let newName = name.split('/').slice(-1)[0];
  return newName.split('_').length ? newName.split('_').shift().toString(): newName;
 }

}
