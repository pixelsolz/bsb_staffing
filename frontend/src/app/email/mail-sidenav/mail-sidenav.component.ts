import { Component, OnInit ,Input ,Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
declare var $;

@Component({
	selector: 'app-mail-sidenav',
	templateUrl: './mail-sidenav.component.html',
	styleUrls: ['./mail-sidenav.component.css']
})
export class MailSidenavComponent implements OnInit, OnDestroy {
	composeForm: FormGroup;
	labelForm: FormGroup;
	count_data: number;
	resourceUrl: string = '/api/mail';
	showCompose: boolean;
	activeParam: string;
	employeeData: any = [];
	employee_emails: any = [];
	recipientList: any;
	isCompanyEmail: boolean = false;
	routeUrl:string;
	user: any={};
	uploaded: any = {
		attached_file: [],
		attached_image: ''
	  };
	labels: any=[];
	testindex : number =1060;
	emailbodyEditor: any = {
    //height: '200px',
    placeholder: 'Write Here',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
    subscription: any; 
    composeOpenSubscrition :any;
	@Output() inboxClick = new EventEmitter<string>();
	constructor(private fb: FormBuilder, private message: MessageService, private global: Global,
		private authService: AuthService, private router: Router, private commonService: CommonService,
		private activeRoute: ActivatedRoute) {
		this.showCompose = false;
		this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
		this.routeUrl = (this.user && this.user.user_role =='employer')? '/employer/mail':'/mail';
		this.subscription = router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e) => {
			this.activeParam = this.activeRoute.snapshot.paramMap.get('type');

		});
		console.log(this.activeRoute.snapshot.queryParams,'6666')
		this.asyncInit();

		           
	}

	ngOnInit() {
		this.showCompose = false;
		this.commonService.callComposeMail({});
		this.composeForm = this.fb.group({
			reply_id:'',
			mailusrid: '',
			mailbody: ['', [Validators.required]],
			mailsubject: ['', [Validators.required]],
			mailemail: ['', [Validators.required]],
			attached_file: [],
			attached_image: ''
		});

		this.labelForm = this.fb.group({
			label_name:['', [Validators.required]],
			parent_id:''
		});

		 this.composeOpenSubscrition =this.commonService.composeMailOPen.subscribe(data=>{
			if(data.reply_data){
				this.composeForm.get('reply_id').setValue(data.reply_data._id);
				this.composeForm.get('mailsubject').setValue(data.reply_data.mail_subject);
				if(this.user.id == data.reply_data.sender_id){
					this.recipientList = [{ value: data.reply_data.mail_id, display: data.reply_data.mail_id }];
				}else{
					this.recipientList = [{ value: data.reply_data.sender_email, display: data.reply_data.sender_email }];	
				}

				$('#mail_comp_open').click();		
				//this.composeForm.get('mailemail').setValue(data.reply_data.mail_id);
				//this.showCompose = true;
			}

		});
		if(this.activeRoute.snapshot.queryParams && this.activeRoute.snapshot.queryParams['email_now'] &&  this.activeRoute.snapshot.queryParams['email_now'] ==1){
			let sendEmail ='';
			let sendName ='';
			this.isCompanyEmail = true;
			if(this.activeRoute.snapshot.queryParams['email_name']){
				sendEmail = window.atob(this.activeRoute.snapshot.queryParams['email_name'])
			}
			if(this.activeRoute.snapshot.queryParams['comp_name']){
				sendName =  window.atob(this.activeRoute.snapshot.queryParams['comp_name'])
			}	

			if(sendEmail){
				this.recipientList = [{ value: sendEmail, display: sendName }];
			}
			
			$('#mail_comp_open').click();
		}else if(this.activeRoute.snapshot.queryParams && this.activeRoute.snapshot.queryParams['employeeId']){
			this.recipientList = [{ value: window.atob(this.activeRoute.snapshot.queryParams['emp_email']), display: this.activeRoute.snapshot.queryParams['emp_name'] }];
			this.composeForm.get('mailusrid').setValue(this.activeRoute.snapshot.queryParams['employeeId']);
			$('#mail_comp_open').click();
		}else{
			this.isCompanyEmail = false;
		}

	}

	  ngOnDestroy() {
        this.subscription.unsubscribe();
        this.composeOpenSubscrition.unsubscribe();
    }



	asyncInit() {
		let url = (this.user && this.user.user_role =='employer')?'/api/mail/employee-details':'/api/mail/employer-details';
		setTimeout(() => {
		  this.commonService.getAll(url)
			.subscribe(data => {
				this.employeeData = data.data.map((obj) => ({ display: obj.email, value: obj.email }));
				this.labels = data.labels;
			}, error => { });
		}, 2000);
		
	}

	mailStore() {
		let emails = (this.recipientList && this.recipientList.length) ? this.recipientList.map(obj => obj.value):[];
    	if(emails.length){
    		this.composeForm.get('mailemail').setValue(emails)
    	}else{
    		this.message.error('Receipients emails is required');
			return false;
    	}

		if (this.composeForm.invalid) {
			if (this.composeForm.get('mailemail').value === undefined) {
				this.message.error('Email fields is required');
			}

			if (this.composeForm.get('mailbody').value.length < 10) {
				this.message.error('Mail body field is required');
			}
			return false;
		} else {

			    let formData = new FormData();
			    const formValue = this.composeForm.value;
			    formData.append('reply_id',formValue.reply_id ? formValue.reply_id:'');
			    formData.append('mailusrid',formValue.mailusrid ? formValue.mailusrid :'');
			    formData.append('mailbody',formValue.mailbody);
			    formData.append('mailsubject',formValue.mailsubject);
			    formData.append('mailemail',formValue.mailemail);
			    formData.append('attached_image',formValue.attached_image);

			    if(this.activeRoute.snapshot.queryParams['employeeId']){
			    	formData.append('is_resume_trans','1');
			    }
			    if(this.activeRoute.snapshot.queryParams['email_name']){
			    	formData.append('company_email',window.atob(this.activeRoute.snapshot.queryParams['email_name']));
				}
			    this.uploaded.attached_file.forEach(file=>{
				formData.append('attached_file[]', file);
				});
   			
			this.commonService.create(this.resourceUrl, formData)
				.subscribe((data) => {
					if (data.status === 200) {
						this.showCompose = false;
						this.isCompanyEmail = false;
						this.message.success('Successfully send');
						this.composeForm.reset();
						this.uploaded.attached_file.length =0;
						this.uploaded.attached_image ='';
						this.recipientList = '';
						$('#mail_compose_close').click();
						this.commonService.callEmailUpdate('call');
						//this.redirectTo(this.router.url);
						if(this.user.user_role =='employer'){
							this.router.navigate(['employer/mail','inbox'])							
						}else{
							this.router.navigate(['mail','inbox'])
						}
						
					}
					if (data.status === 400) {
						this.message.error(data.status_text);
					}
					if (data.status === 422) {
						this.message.error(data.status_text);
					}
					if (data.status === 500) {
						this.message.error('Please search and select mail from dropdown list.');
					}
					console.log(data);
				}, (error) => {
				});
		}
	}

	redirectTo(uri) {
	    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
	    this.router.navigate([uri]));
	}

	uploadFile(e, type){
		const file = e.target.files[0];
		if(type =='file'){
			this.uploaded.attached_file.push(file);
			//this.uploaded.attached_file = file;
			this.composeForm.get('attached_file').setValue(this.uploaded.attached_file);
		}else{
			this.uploaded.attached_image = '';
			this.composeForm.get('attached_image').setValue(file);	
		}
		
	}

	removeAttachedFile(indx){
		this.uploaded.attached_file.splice(indx,1);
		this.composeForm.get('attached_file').setValue(this.uploaded.attached_file);
	}

  formDataSet() {


    /*Object.keys(this.composeForm.controls).forEach(key => {
    	console.log(key);
    	if(key === 'attached_file'){
    		if(this.uploaded.attached_file.length){
	    		for(var i = 0; i < this.uploaded.attached_file.length; i++){
			      formData.append('attached_file[]', this.uploaded.attached_file[i].file);
			    }    			
    		}			
    	}else{
    		formData.append(key, this.composeForm.get(key).value);
    	}
      
    });*/
    //return formData;
  }

  modalClose(){
  	this.showCompose=false;
  	this.composeForm.get('reply_id').setValue('');
	this.recipientList = '';
	this.commonService.callComposeMail({});
	this.uploaded.attached_file.length =0;
  }

  submitLabel(event){
  	event.preventDefault();
  	if(this.labelForm.get('label_name').value ==''){
  		this.message.error('please enter label name');
  		return false;
  	}
  	this.commonService.create('/api/mail/label/create', this.labelForm.value)
  		.subscribe(data=>{
  			if(data.status ==200){
  				this.labels.push(data.data);
  				this.message.success('Successfully create label');
  				this.labelForm.reset();
  				$('#button_close').trigger("click");
  			}
  		},error=>{});

  }

  deleteLabel(id){
  	let index = this.labels.findIndex(obj=>obj._id == id);
  	this.commonService.delete('/api/mail/label/delete/', id)
  		.subscribe(data=>{
  			this.labels.splice(index, 1);
  			this.message.success('Successfully deleted');
  		},error=>{});
  }
  hideDetails(){
  	this.inboxClick.emit('1');
  }
}
