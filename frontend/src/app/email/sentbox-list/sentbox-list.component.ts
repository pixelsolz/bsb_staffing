import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-sentbox-list',
  templateUrl: './sentbox-list.component.html',
  styleUrls: ['./sentbox-list.component.css']
})
export class SentboxListComponent implements OnInit {

  constructor(private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
