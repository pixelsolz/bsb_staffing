import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute} from '@angular/router';
import {Global} from '../../global';

@Component({
  selector: 'app-template-preview',
  templateUrl: './template-preview.component.html',
  styleUrls: ['./template-preview.component.css']
})
export class TemplatePreviewComponent implements OnInit {

  @Input() previewData: any={};
  @Input() senderData: any={};
  templateData: any={
  	comp_name:'',
  	comp_email:'',
  	employer_name:'',
  	company_type:'',
  	company_address:'',
  	company_size:'',
  	website:'',
  	comp_mobile:'',
  	job_title:'',
  	min_exp: '',
  	max_exp: '',
  	min_salary: '',
  	max_salary: '',
  	job_country:'',
  	job_city: '',
  	about_job:'',
  	benefit:'',
  	currency_type:'',
  	language_pref:'',
  	skill:'',
  	subject:'',
  	template_name:'',
  	responsibility:'',
	salary_type:'',
	special_note:'',
	id:'',

  };

  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[]
  }
  baseUrlPath: string;
  constructor(private global: Global,private commonService: CommonService, private message:MessageService, private router: Router) { 
    this.baseUrlPath = this.global.baseUrl
  	//localStorage.removeItem('template_preview');
  }

  ngOnInit() {
  	console.log(this.senderData, '666')
  	this.asyncInit();
  }

  asyncInit(){
    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data.status ==200){
        this.setDataValue(data);
        this.commonService.getAll(`/api/employee/get-single-job-data/${this.previewData.employer_job._id}`).subscribe(data=>{
        	if(data.status == 200){
	        	this.globalJobSave.jobs = data.data;
	        	this.commonService.callJobsData(this.globalJobSave);
        	}
        });
        
      }
    });
  }

  setDataValue(data){
  	console.log(this.previewData);
 	this.templateData.id = this.previewData.employer_job._id;  
  	this.templateData.comp_name = this.senderData.company_name;
  	this.templateData.comp_email = this.senderData.company_email;
  	this.templateData.employer_name = this.senderData.name;
  	this.templateData.company_type = this.senderData.company_type_details.company_type;
  	this.templateData.company_address = this.senderData.address;
  	this.templateData.company_size = this.senderData.company_size_details.company_size;
  	this.templateData.website = this.senderData.company_website;
  	this.templateData.comp_mobile = this.senderData.user.phone_no;
  	this.templateData.job_title = this.previewData.employer_job.job_title;
  	this.templateData.min_exp = data['allExprience'].find(obj=>obj._id == this.previewData.employer_job.min_experiance).name;
  	this.templateData.max_exp = data['allExprience'].find(obj=>obj._id == this.previewData.employer_job.max_experiance).name;
  	this.templateData.min_salary = this.previewData.employer_job.min_salary;
  	this.templateData.max_salary = this.previewData.employer_job.max_salary;
  	this.templateData.job_country = data['countries'].find(obj=>obj._id == this.previewData.employer_job.country_id).name;
  	this.templateData.job_city = data['cities'].find(obj=>obj._id == this.previewData.employer_job.city_id).name;
  	this.templateData.about_job = this.previewData.employer_job.about_job;
  	this.templateData.benefit = this.previewData.employer_job.benefit;
  	//this.templateData.salary_type = data.countries.filter(obj=>obj.entity =='salary').find(obj=>obj._id ==this.previewData.salary_type).name;
  	this.templateData.language_pref = data['alllanguage'].filter(obj=> this.previewData.employer_job.language_id.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
  	this.templateData.skill = this.previewData.employer_job.skill;
  	this.templateData.subject = this.previewData.employer_job.subject;
  	this.templateData.template_name = this.previewData.employer_job.template_name;
	this.templateData.responsibility = this.previewData.employer_job.primary_responsibility;
	this.templateData.salary_type = this.previewData.employer_job.salary_type;
	this.templateData.special_note = this.previewData.employer_job.special_note;
	this.templateData.currency_type = data.countries.find(obj=>obj._id == this.previewData.employer_job.currency).currency_code;  	
  

  }

}
