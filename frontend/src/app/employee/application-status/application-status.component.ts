import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-application-status',
  templateUrl: './application-status.component.html',
  styleUrls: ['./application-status.component.css']
})
export class ApplicationStatusComponent implements OnInit {

  appliedJobs: any=[];
  constructor(private commonService: CommonService) { }

  ngOnInit() {
  	this.asyncInit();
  }

    asyncInit(){
  	this.commonService.getAll('/api/employee/get-application/status')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.appliedJobs = data.data.applied_jobs;
  		}, (error)=>{});
  }

}
