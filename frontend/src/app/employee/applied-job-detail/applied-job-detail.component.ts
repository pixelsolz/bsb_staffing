import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-applied-job-detail',
  templateUrl: './applied-job-detail.component.html',
  styleUrls: ['./applied-job-detail.component.css']
})
export class AppliedJobDetailComponent implements OnInit {
    previousUrl:any;
	job_id:any;
	job_details:any;
    shareUrl: string;
    jobType: string;
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute, private messageService: MessageService) { 

  	this.job_id = activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';
    this.jobType = activeRoute.snapshot.queryParams['jb_type'] ? activeRoute.snapshot.queryParams['jb_type']: '';
  	this.getData();
    this.shareUrl = window.location.href;
  }

  ngOnInit() {
  	window.scrollTo(0, 0);
  }

  
  getData(){
    if(this.jobType =='international'){
    this.commonService.getAll('/api/international/job-detail/' + this.job_id)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.job_details = data.job;
      }, (error)=>{});  
    }else{
    this.commonService.getAll('/api/job-by-id/' + this.job_id)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.job_details = data.job;
        //this.galleryImages = data.job.employeer.gallery_images;
        console.log(this.job_details);
        //this.companyImages = this.galleryImages.map((image)=> this.imgUrl +'employer/gallery_images/'+ image.name)
      }, (error)=>{});      
    }


  }

}
