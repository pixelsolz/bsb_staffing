import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';
declare var Swal: any;
declare var $ :any;
import * as _ from 'lodash'; 
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if($(event.target).hasClass('auto_comp_search')){
      if($(event.target).attr('id') =='job_title_search'){
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='industry_search'){
        this.show_jb_title_srh = false;
        this.show_city_comp_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='country_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='department_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
      }
    }else{
      this.show_jb_title_srh = false;
      this.show_industry_srh = false;
      this.show_city_comp_srh = false;
      this.adv_show_jb_title_srh = false;
      this.show_department_srh = false;
    }
  }
  
  allData: any={};
  showloader:boolean = false;
  announcements: any=[];
  referrals: any=[];
  advSearchForm: FormGroup;
  myAlertForm: FormGroup;
  signUp2Form: FormGroup;
  submitted : boolean = false;
  storeMultiSelectRegistration :any ={
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    industries_value:[],
    industries_name:[],
  };

  storeMultiSelect :any ={
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    industries_value:[],
    industries_name:[],
    department_name:[],
    department_value:[],
  };
  skill_key: any;
  skillsData: any=[];
  countries: any=[];
  countryArray: any=[];
  cities: any=[];
  cityArray: any=[];
  coutryWiseCity: any=[];
  departments: any=[];
  departmentData: any=[];
  industries: any=[];
  all_industries: any=[];
  countryDiv: boolean=false;
  cityDiv: boolean=false;
  departmentDiv: boolean=false;
  industriesDiv: boolean=false;
  count_profle_strength: number;

  title_job_data: any=[];
  city_country_data: any=[];
  industrty_data: any=[];
  department_data: any=[];
  country_model_array: any=[];
  country_loc_model: any;
  insudtry_model_array: any=[];
  job_title_model_array: any=[];
  department_model_array: any=[];
  advanced_job_title_model_array:any=[];
  job_title_model:any;
  insudtry_model:any;
  department_model: any;

  show_jb_title_srh : boolean= false;
  show_city_comp_srh : boolean= false;
  show_industry_srh : boolean= false;
  adv_show_jb_title_srh: boolean=false;
  show_department_srh: boolean=false;
  searchData = {
    title_comp:{
      id:'',
      name:'',
      table_name:''
    },
    city_country:{
      id:'',
      name:'',
      table_name:''
    },
    industry:{
      id:'',
      name:'',
      table_name:''
    },
    department:{
      id:'',
      name:'',
      table_name:''
    }
  };
  storeMultiSelectSearch :any ={
    industries_value:[],
    industries_name:[],
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    department_value: [],
    department_name: []
  };
  countryDivSearch:boolean =false;
  cityDivSearch: boolean=false;
  industriesDivSearch: boolean=false;
  departmentDivSearch: boolean = false;
  emplymentFor: any=[];
  experiance: any=[]
  all_currency:any=[];
  user_role_id:any;
  citiesData: any=[];
  showValueService: boolean = false;
  milesRange: number =200;
  userProfileData: any;
  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[],
    miles_jobs:[],
    alert_jobs:[]
  }
  loginEmpProfile : any;
  rangeJobs: any=[];
  prevEighteenYearDate: any;
  show_profile_modal:boolean = false;
  countryDivSign2:boolean =false;
  cityDivSign2:boolean=false
  industriesDivSign2:boolean=false
  constructor(private renderer2: Renderer2, private renderer: Renderer, private commonservice: CommonService, private messageservice: MessageService,
     private router: Router, private fb: FormBuilder, private global: Global,  private eRef: ElementRef) {


     }

  ngOnInit() {
    let userDate = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
    this.user_role_id = userDate.role._id;
    this.show_profile_modal = (userDate.user_detail && userDate.user_detail.form_no == 1) ? true: false;
    this.prevEighteenYearDate = moment().subtract(18, 'years').calendar()

    this.myAlertForm = this.fb.group({
      keyword:[''],
      salary_type:[''],
      currency:[''],
      salary: [''],
      experiance:[''],
      countries:[''],
      cities:[''],
      departments:[''],
      industries:[''],
      email_id:[''],
      name_of_alert:['']
    });

    this.advSearchForm= this.fb.group({
      job_title:'',
      employement_for:'',
      country_id:'',
      city_id:'',
      industry_id:'',
      department_id:'',
      min_experiance:'',
      max_experiance:'',
      salary_type:'',
      currency:'',
      min_monthly_salary:'',
      max_monthly_salary:'',
    });

    this.signUp2Form = this.fb.group({
      country_id: ['', [Validators.required]],
      city_id: ['', [Validators.required]],
      designation: ['', [Validators.required, this.noWhitespaceValidator, Validators.maxLength(50)]],
      industry_id: ['', [Validators.required]],
      date_of_birth:['', [Validators.required]],
      mobile: ['', [Validators.required,Validators.pattern('^(\\+)?(\\d+)$'),Validators.minLength(8),Validators.maxLength(20)]],
      user_unique_code:[''],
      gender:['', [Validators.required]],
    });

    this.signUp2Form.get('country_id').valueChanges.subscribe(value=>{
      if(value.length){
        let country = this.countries.find(obj=>obj._id == value[0]);
        //this.coutryWiseCity = {"":this.cities.filter(obj=>obj.country_code == country.code)}
        this.commonservice.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){ 
            this.coutryWiseCity =  {"":data.data}
          }
        })
      }else{
        this.coutryWiseCity =[]
         this.storeMultiSelectRegistration.city_value.splice(0, 1);
       this.storeMultiSelectRegistration.city_name.splice(0, 1);
       this.signUp2Form.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
      }

    });
    this.setCommonData();
    if(!this.show_profile_modal){
      this.callProfileAsync()
    }
  

     this.advSearchForm.get('country_id').valueChanges.subscribe(value=>{
      let countryCode = this.countries.filter(obj=> this.storeMultiSelectSearch.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.citiesData = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
    });
  }


  ngAfterViewInit() {
      if(this.show_profile_modal){
        if(document.querySelector('link[href$="materialize.min.css"]')){
         document.querySelector('link[href$="materialize.min.css"]').remove() 
        }
      
    }else{
      if(!document.querySelector('link[href$="materialize.min.css"]')){
      this.loadCss('assets/css/materialize.min.css')
      }
    }

  }

    get f() { return this.signUp2Form.controls; }

  public loadCss(url: string){
    const body = <HTMLDivElement> document.body;
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    body.appendChild(link);
  }

  callProfileAsync(){
    this.commonservice.getAll('/api/employee-profile').subscribe(res=>{
      if (res.status == 200) {
        this.loginEmpProfile = res.data;
      }
    });

    this.commonservice.getAll('/api/employee-dashboard')
      .subscribe(data=>{
        this.commonservice.callFooterMenu(1);
        if(data.status ==200){
          this.allData = data.data;
          //this.user_role_id = this.allData.profile.user.role._id;
          this.count_profle_strength = data.data.count_profle_strength;
          this.userProfileData = data.data.profile;
          
          this.globalJobSave.jobs = this.allData.recomended_jobs_data;
          this.globalJobSave.alert_jobs = this.allData.alert_jobs_data;
          this.commonservice.callJobsData(this.globalJobSave);
          this.rangeCall()
        }

      }, error=>{});
  }

  gotoDetail(type, id){
    if(type === 'recomended'){
       this.commonservice.callJobsData(this.allData.recomended_jobs_data);
       this.router.navigate(['/job-details','job-post',id], { queryParams: {backUrl:'dashboard'} })
    }else{
        this.commonservice.callJobsData(this.allData.alert_jobs_data);
        this.router.navigate(['/job-details','job-post',id], { queryParams: {backUrl:'dashboard'} })
    }
  }

  setCommonData(){
    this.commonservice.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.countryArray = {"":data['countries']};
        this.all_industries = data['all_industries'].filter(obj=>obj.name !=="");
        this.industries = data['industries'];
      }
    });

    this.commonservice.getCityData.subscribe(data=>{
        if(data && data.status ==200){
           this.cities = data['cities'];
           this.cityArray = {"":data['cities']}; 
        }
    });

    this.commonservice.getCommonData.subscribe(data=>{
       if(data && data.status ==200){
        let otherData = data['other_mst'];
        this.referrals = data['benefit_data'].filter(obj=> String(obj.type) =='1' && obj.entity_id == this.user_role_id);
        this.announcements = data['benefit_data'].filter(obj=> String(obj.type) =='2' && obj.entity_id == this.user_role_id);
        this.emplymentFor = otherData.filter((data) => data.entity == 'emplyment_for');
        this.experiance = data['allExprience'];
        this.departments = data['all_department'];
        this.departmentData = data['all_parent_department'];
        this.all_currency = data['all_currency'];
        this.skillsData = data['skills'].map((obj)=> {return {display: obj.skill, value: obj.skill}});
       }
    });
  }

   alertMessage(text){
    Swal.fire({
      icon: 'error',
      title: '',
      html: `<strong>${text}</strong>`,
    })
   }

    applyJob(job_id, type, index){

      let job = type =='exact_job'? this.allData.alert_jobs_data.find(obj=>obj._id ==job_id) :(type =='range_job' ? this.rangeJobs.find(obj=>obj._id ==job_id) : this.allData.recomended_jobs_data.find(obj=>obj._id ==job_id)) 

      if(job.candidate_type =='2' && this.userProfileData.gender == 2){
        this.alertMessage('This job only for male apply next job');
      return;
    }
    if(job.candidate_type =='3' && this.userProfileData.gender == 1){
      this.alertMessage('This job only for female apply next job');
      return;
    }

    let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
    if(!job.job_title.includes(this.userProfileData.profile_title) && !job.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
      this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
      //this.messageservice.error('as per your cv/ Resume this job not matching for you apply next job');
      return;
    }

    if(this.userProfileData.total_experience_yr_value == 0 && job.min_experiance.value >0){
       this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
      //this.messageservice.error('as per your cv/ Resume this job not matching for you apply next job');
      return;
    }

    if(this.userProfileData.total_experience_yr_value < job.min_experiance.value){
      this.alertMessage('This job post work experience not matching with your profile  apply next job');
      //this.messageservice.error('This job post work experience not matching with your profile  apply next job');
      return;
    }

    if(job.employement_for.name =='Domestic Jobs' && job.country_name != this.userProfileData.country.name){
      this.alertMessage('This job only for local candidate apply next job');
      //this.messageservice.error('This job only for local candidate apply next job');
      return;

    }

  	this.commonservice.create('/api/employee/job-search', {job_id:job_id})
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.messageservice.success(data.status_text);
				this.router.navigate(['/applied-jobs']);
  			}
  		}, (error)=>{});
  }

  getOnchangeEvent(type){
    if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }else if(type=='industries'){
      this.industriesDiv = true;
    }else if(type=='department'){
      this.departmentDiv = true;
    }

  }

  getChange(e){
    console.log(e);
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='country'){
      this.myAlertForm.patchValue({
      countries: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.myAlertForm.patchValue({
      cities: e.storedValue
      });
    }else if(e.field_name =='industries'){
      this.myAlertForm.patchValue({
      industries: e.storedValue
      });
    }else if(e.field_name =='department'){
      this.myAlertForm.patchValue({
      departments: e.storedValue
      });
    }
  }

  checkmultiselect(e){
    if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }else if(e.field_name =='industries'){
      this.industriesDiv = e.status;
    }else if(e.field_name =='department'){
      this.departmentDiv = e.status;
    }
  }

  multiselectAll(e){
    //console.log(e.storedValue);
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });
    }
    if(filed =='country'){
      this.myAlertForm.patchValue({
      countries: e.storedValue
      });
    }else if(filed =='city'){
      this.myAlertForm.patchValue({
      cities: e.storedName
      });
    }else if(filed =='department'){
      this.myAlertForm.patchValue({
      departments: e.storedValue
      });
    }else if(filed =='industries'){
      this.myAlertForm.patchValue({
      industries: e.storedValue
      });
    }

  }


  getOnchangeEventSign2(type){
    if(type =='country'){
      this.countryDivSign2 = true;
    }else if(type =='city'){
      this.cityDivSign2 = true;
    }else if(type=='industries'){
      this.industriesDivSign2 = true;
    }

  }


  getOnchangeEventSearch(type){
    if(type =='country'){
      this.countryDivSearch = true;
    }else if(type =='city'){
      this.cityDivSearch = true;
    }else if(type=='industries'){
      this.industriesDivSearch = true;
    }else if(type=='departments'){
      this.departmentDivSearch = true;
    }

  }
    getChangeSearch(e){
    //console.log(e);
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='industries'){
      this.advSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }else if(e.field_name =='departments'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }


  getChangeSign2(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='industries'){
      this.signUp2Form.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.signUp2Form.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.signUp2Form.patchValue({
      city_id: e.storedValue
      });
    }
  }

    checkmultiselectSearch(e){
    if (e.field_name == 'industries') {
      this.industriesDivSearch = e.status;
    }else if(e.field_name =='country'){
      this.countryDivSearch = e.status;
    }else if(e.field_name =='city'){
      this.cityDivSearch = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDivSearch = e.status;
    }
  }


  checkmultiselectSign2(e){
    if (e.field_name == 'industries') {
      this.industriesDivSign2 = e.status;
    }else if(e.field_name =='country'){
      this.countryDivSign2 = e.status;
    }else if(e.field_name =='city'){
      this.cityDivSign2 = e.status;
    }
  }

  multiselectAllSearch(e){
    //console.log(e.storedValue);
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });
    }
    if(filed =='industries'){
      this.advSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }else if(filed =='departments'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  multiselectAllSign2(e){
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });
    }
    if(filed =='industries'){
      this.signUp2Form.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='country'){
      this.signUp2Form.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.signUp2Form.patchValue({
      city_id: e.storedValue
      });
    }
  }


  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
        break;
      default:
        // code...
        break;
    }
  }

  removeMultiSelectSearch(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelectSearch.industries_value.splice(indx, 1);
       this.storeMultiSelectSearch.industries_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        industry_id: this.storeMultiSelectSearch.industries_value
        });
        break;
      case "country":
       this.storeMultiSelectSearch.country_value.splice(indx, 1);
       this.storeMultiSelectSearch.country_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        country_id: this.storeMultiSelectSearch.country_value
       });
        break;
      case "city":
       this.storeMultiSelectSearch.city_value.splice(indx, 1);
       this.storeMultiSelectSearch.city_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          city_id: this.storeMultiSelectSearch.city_value
       });
        break;
      case "departments":
       this.storeMultiSelectSearch.department_value.splice(indx, 1);
       this.storeMultiSelectSearch.department_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          department_id: this.storeMultiSelectSearch.department_value
       });
        break;  
      default:
        // code...
        break;
    }
  }

  removeMultiSelectSign2(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelectRegistration.industries_value.splice(indx, 1);
       this.storeMultiSelectRegistration.industries_name.splice(indx, 1);
       this.signUp2Form.patchValue({
        industry_id: this.storeMultiSelectRegistration.industries_value
        });
        break;
      case "country":
       this.storeMultiSelectRegistration.country_value.splice(indx, 1);
       this.storeMultiSelectRegistration.country_name.splice(indx, 1);
       this.signUp2Form.patchValue({
        country_id: this.storeMultiSelectRegistration.country_value
       });

        break;
      case "city":
       this.storeMultiSelectRegistration.city_value.splice(indx, 1);
       this.storeMultiSelectRegistration.city_name.splice(indx, 1);
       this.signUp2Form.patchValue({
          city_id: this.storeMultiSelectRegistration.city_value
       });
        break; 
      default:
        // code...
        break;
    }    
  }

  searchAutoComplete(type, key) {
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
    if(type ==='COUNTRY_WISE'){
      if(key.target.value.trim().length ==0){
        this.country_model_array.length =0;
        this.country_loc_model ='';
      }
      if(this.country_loc_model){
        let titleDataArr = this.country_loc_model.split(',');
        let arrFilter = this.country_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.country_model_array = arrFilter;
      }
      usedData = this.country_loc_model;
    }else if(type ==='INDUSTRY_WISE'){
      if(key.target.value.trim().length ==0){
        this.insudtry_model_array.length =0;
        this.insudtry_model ='';
      }
      if(this.insudtry_model){
        let titleDataArr = this.insudtry_model.split(',');
        let arrFilter = this.insudtry_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.insudtry_model_array = arrFilter;
      }
      usedData = this.insudtry_model;
    }else if(type ==='JB_TITLE_WISE'){
      if(key.target.value.trim().length ==0){
        this.job_title_model_array.length =0;
        this.job_title_model ='';
      }
      if(this.job_title_model){
        let titleDataArr = this.job_title_model.split(',');
        let arrFilter = this.job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.job_title_model_array = arrFilter;
      }
      usedData = this.job_title_model;
    }else if(type ==='DEPARTMENT_WISE'){
      if(key.target.value.trim().length ==0){
        this.department_model_array.length =0;
        this.department_model ='';
      }
      if(this.job_title_model){
        let titleDataArr = this.department_model.split(',');
        let arrFilter = this.department_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.department_model_array = arrFilter;
      }
      usedData = this.department_model;
    }
    if(!searchdata){
      this.show_jb_title_srh = false;
      this.show_city_comp_srh = false;
      this.show_industry_srh = false;
      return false;
    }
    this.commonservice.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.show_jb_title_srh = true;
            this.title_job_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'COUNTRY_WISE') {
            this.show_city_comp_srh = true;
            this.city_country_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'INDUSTRY_WISE') {
            this.show_industry_srh = true;
            this.industrty_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }else if (data.type == 'DEPARTMENT_WISE') {
            this.show_department_srh = true;
            this.department_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }
        }


      }, (error) => { });
  }

  autoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.show_jb_title_srh = false;
      this.searchData.title_comp.id = titJob.id;
      if(this.job_title_model_array.length){
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = this.job_title_model_array.toString() + ',';
      }else{
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = titJob.name + ',';
      }
      document.getElementById('job_title_search').focus();
    } else if (type == 'COUNTRY_WISE') {
      const city_count = this.city_country_data.find((data) => data.id == String(id));
      this.show_city_comp_srh = false;
      if(this.country_model_array.length){
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }else{
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = city_count.name + ',';
      }
       document.getElementById('country_search').focus();
      this.searchData.city_country.id = city_count.id;
      //this.searchData.city_country.name = city_count.name;
    } else if (type == 'INDUSTRY_WISE') {
      const industry_count = this.industrty_data.find((data) => data.id == String(id));
      this.show_industry_srh = false;
      if(this.insudtry_model_array.length){
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }else{
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = industry_count.name + ',';
      }
      this.searchData.industry.id = industry_count.id;
    }
    else if (type == 'DEPARTMENT_WISE') {
      const department_count = this.department_data.find((data) => data.id == String(id));
      this.show_department_srh = false;
      if(this.department_model_array.length){
        this.department_model_array =[...this.department_model_array, department_count.name];
        this.department_model = this.department_model_array.toString() + ',';
      }else{
        this.department_model_array =[...this.department_model_array, department_count.name];
        this.department_model = department_count.name + ',';
      }
      this.searchData.industry.id = department_count.id;
    }

  }

    advancedAutoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.adv_show_jb_title_srh = false;
      if(this.advanced_job_title_model_array.length){
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(this.advanced_job_title_model_array.toString() + ',');
      }else{
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(titJob.name + ',');
      }
      document.getElementById('jobTitle').focus();
    }

  }


    advancedSearchAutoComplete(type, key){
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
    if(type ==='JB_TITLE_WISE'){
      if(key.target.value.trim().length ==0){
        this.advanced_job_title_model_array.length =0;
        this.advSearchForm.controls['job_title'].setValue('');
      }
      if(this.advSearchForm.controls['job_title'].value){
        let titleDataArr = this.advSearchForm.controls['job_title'].value.split(',');
        let arrFilter = this.advanced_job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.advanced_job_title_model_array = arrFilter;
      }

       usedData =  this.advSearchForm.controls['job_title'].value;
    }
    if(!searchdata){
      this.adv_show_jb_title_srh = false;
      return false;
    }
    this.commonservice.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.adv_show_jb_title_srh = true;
            this.title_job_data =  _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }
        }


      }, (error) => { });
  }

    searchFilter() {

    let search_title = this.searchData.title_comp.name ? this.searchData.title_comp.name : (this.job_title_model ? this.job_title_model.replace(/,\s*$/, "") : '');
    let search_industry = this.searchData.industry.name ? this.searchData.industry.name : (this.insudtry_model ? this.insudtry_model.replace(/,\s*$/, "") : '');
    let search_city = this.searchData.city_country.name ? this.searchData.city_country.name : (this.country_loc_model ? this.country_loc_model.replace(/,\s*$/, "") : '');
    //let departmentSearch = this.department_model ? this.department_model.replace(/,\s*$/, "") : '';
    let departmentSearch = this.searchData.department.name ? this.searchData.department.name:  (this.department_model ? this.department_model.replace(/,\s*$/, "") : '');
    if (search_title != '' || search_industry != '' || search_city != ''|| departmentSearch != '') {
      //return true;
      this.router.navigate(['/search-job-list'], { queryParams: { 'search_title': search_title, 'search_industry': search_industry, 'search_location': search_city, 'search_department':departmentSearch } });
    } else {
      this.messageservice.warning("Please Enter Value");
      return false;
    }


  }

  searchJobs() {
    this.advSearchForm.controls['country_id'].setValue(this.storeMultiSelectSearch.country_value);
    this.advSearchForm.controls['city_id'].setValue(this.storeMultiSelectSearch.city_value);
    this.advSearchForm.controls['industry_id'].setValue(this.storeMultiSelectSearch.industries_value);
    this.advSearchForm.controls['department_id'].setValue(this.storeMultiSelectSearch.department_value);
    let jbTitleValue = this.advSearchForm.controls['job_title'].value.replace(/,\s*$/, "");
    this.advSearchForm.controls['job_title'].setValue(jbTitleValue.split(','));
    localStorage.setItem('advSearch', JSON.stringify(this.advSearchForm.value));
    this.router.navigate(['/search-job-list'], { queryParams: { src: 'advance_search' } });
  }

  /*  searchJobs(){
    this.router.navigate(['/search-job-list'], {
      queryParams: {
        'job_title': this.advSearchForm.value.job_title ?this.advSearchForm.value.job_title.replace(/,\s*$/, "") : '',
        'country_list': this.storeMultiSelectSearch.country_value ? this.storeMultiSelectSearch.country_value.toString():'',
        'city_list': this.storeMultiSelectSearch.city_value ? this.storeMultiSelectSearch.city_value.toString():'',
        'industry_list': this.storeMultiSelectSearch.industries_value ? this.storeMultiSelectSearch.industries_value.toString():'',
        'department_list': this.storeMultiSelectSearch.department_value ? this.storeMultiSelectSearch.department_value.toString():'',
        'min_exp':this.advSearchForm.value.min_experiance ? this.advSearchForm.value.min_experiance:'',
        'max_exp':this.advSearchForm.value.max_experiance ? this.advSearchForm.value.max_experiance:'',
        'salary_type':this.advSearchForm.value.salary_type ? this.advSearchForm.value.salary_type: '',
        'currency_type':this.advSearchForm.value.currency ? this.advSearchForm.value.currency: '',
        'min_salary':this.advSearchForm.value.min_monthly_salary ? this.advSearchForm.value.min_monthly_salary:'',
        'max_salary':this.advSearchForm.value.max_monthly_salary ? this.advSearchForm.value.max_monthly_salary:''

      } });
  }*/

    modalClose(){
    this.advanced_job_title_model_array.length =0;
    this.advSearchForm.reset();
  }

  rangeCall(){
    if(this.userProfileData && this.userProfileData.present_address){
      this.commonservice.getLatLong(this.userProfileData.present_address).then((res)=>{
          if(res.status == "OK"){
              let country = this.userProfileData.country.name ? this.userProfileData.country.name :res.results[0]['address_components'][8]['long_name'];
              let lat_long = res.results[0]['geometry']['location']
              console.log(lat_long)
              this.commonservice.getAll(`/api/employee/get-job-by-search?user_lat=${lat_long.lat}&user_long=${lat_long.lng}&range=${this.milesRange}&user_country=${country}`)
                  .subscribe((data)=>{
                    console.log(data)
                    if(data.status ===200){
                      this.rangeJobs = data.jobResults;
                      this.globalJobSave.miles_jobs = data.jobResults;
                      this.commonservice.callJobsData(this.globalJobSave);
                    }
                  },(error)=>{})

          }else{
            
          }

        
      })
    }else if(this.userProfileData && this.userProfileData.city){
      this.commonservice.getLatLong(this.userProfileData.city.name).then((res)=>{
            if(res.status == "OK"){
              let country = this.userProfileData.country.name ? this.userProfileData.country.name :res.results[0]['address_components'][8]['long_name'];
              console.log(country)
              let lat_long = res.results[0]['geometry']['location']
              console.log(lat_long)
              this.commonservice.getAll(`/api/employee/get-job-by-search?user_lat=${lat_long.lat}&user_long=${lat_long.lng}&range=${this.milesRange}&user_country=${country}`)
                  .subscribe((data)=>{
                    console.log(data)
                    if(data.status ===200){
                      this.rangeJobs = data.jobResults;
                    }
                  },(error)=>{})

          }else{
            
          }
      })
    }
    
    
   console.log(this.milesRange)
  }

  rangeViewAll(){
    if(this.userProfileData && this.userProfileData.present_address){
      this.commonservice.getLatLong(this.userProfileData.present_address).then((res)=>{
          if(res.status == "OK"){
              let country = res.results[0]['address_components'][8]['long_name'];
              let lat_long = res.results[0]['geometry']['location']
              console.log(lat_long)
              this.router.navigate(['/search-job-list'],{ queryParams: { lat:lat_long.lat, long:lat_long.lng, user_country:country, job_range:this.milesRange } })

          }else{
            
          }

        
      })
    }else if(this.userProfileData && this.userProfileData.city){
      this.commonservice.getLatLong(this.userProfileData.city.name).then((res)=>{
        if(res.status == "OK"){
              let country = res.results[0]['address_components'][8]['long_name'];
              let lat_long = res.results[0]['geometry']['location']
              console.log(lat_long)
              this.router.navigate(['/search-job-list'],{ queryParams: { lat:lat_long.lat, long:lat_long.lng, user_country:country, range:this.milesRange } })

          }else{
            
          }
      })
    }
  }


  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  handleSignUp(){
    this.submitted = true;
    if (this.signUp2Form.invalid) {
      return;
    }
    this.showloader = true
    let formValue ={
      country_id:this.signUp2Form.get('country_id').value[0],
      city_id:this.signUp2Form.get('city_id').value[0],
      designation:this.signUp2Form.get('designation').value,
      industry_id:this.signUp2Form.get('industry_id').value[0],
      date_of_birth:this.signUp2Form.get('date_of_birth').value,
      mobile:this.signUp2Form.get('mobile').value,
      user_unique_code:this.signUp2Form.get('user_unique_code').value,
      gender:this.signUp2Form.get('gender').value
    }

      this.commonservice.create('/api/employee-profile-update', formValue)
          .subscribe((data)=>{
           this.showloader = false
           if(data.status == 200){
          this.messageservice.success(data.status_text);
          localStorage.setItem('user',JSON.stringify(data.user));
          this.show_profile_modal= false;
          let user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
          if(!document.querySelector('link[href$="materialize.min.css"]')){
          this.loadCss('assets/css/materialize.min.css')
          }
          this.callProfileAsync()
          this.commonservice.callProfileUpdate(user);
             } else if(data.status == 500){
          this.messageservice.error(data.status_text);
        }else if(data.status == 422){
          if(Object.entries(data.error).length){
            Object.entries(data.error).forEach(obj=>{
              this.messageservice.error(obj[1]);
            })
          }          
        }else if(data.status == 421){
          this.messageservice.error(data.error);
        }
          },(error)=>{
            this.showloader = false
            console.log(error)
          });
  }



}
