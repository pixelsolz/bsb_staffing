import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ApplicationsComponent } from './applications/applications.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { ProfileComponent } from './profile/profile.component';


import { SearchDetailComponent } from './search-job/search-detail/search-detail.component';
import { SearchCategoryComponent } from './search-job/search-category/search-category.component';
import { SearchJobListingComponent } from './search-job-listing/search-job-listing.component';

import { JobAlertComponent } from './job-for/job-alert/job-alert.component';
import { MyAlertComponent } from './my-alert/my-alert.component';
import { AppliedJobComponent } from './job-for/applied-job/applied-job.component';
import { SavedJobComponent } from './job-for/saved-job/saved-job.component';
import { LocalWalkinComponent } from './job-for/local-walkin/local-walkin.component';
import { InternationalWalkinComponent } from './job-for/international-walkin/international-walkin.component';
import { SlotBookingComponent } from './job-for/slot-booking/slot-booking.component';
import { ApplicationStatusComponent } from './application-status/application-status.component';
import { RecommendAlertComponent } from './job-for/recommend-alert/recommend-alert.component';
import { TrainingGettingJobComponent } from './offer-training/training-getting-job/training-getting-job.component';
import { WorkingVisaTrainingComponent } from './offer-training/working-visa-training/working-visa-training.component';
import { FindJobsComponent } from './find-jobs/find-jobs.component';
import { SingleJobDetailsComponent } from './single-job-details/single-job-details.component';
import { AppliedJobDetailComponent } from './applied-job-detail/applied-job-detail.component';
import { AuthGuard } from './../auth.guard';
import { SearchListComponent } from './search-job/search-list/search-list.component';
import { WalkinDetailsComponent } from './job-for/walkin-details/walkin-details.component';
import { AppliedInternationalJobsComponent } from './job-for/applied-international-jobs/applied-international-jobs.component';
import { SavedInternationalJobsComponent } from './job-for/saved-international-jobs/saved-international-jobs.component';
import { ProfileViewEmployerComponent } from './profile-view-employer/profile-view-employer.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [

	{path:'',canActivate: [AuthGuard], loadChildren:'../common-uses/common-uses.module#CommonUsesModule'},
	{path:'',canActivate: [AuthGuard], loadChildren:'../email/email.module#EmailModule'},
	{path:'', loadChildren:'../auth/auth.module#AuthModule'},
	{path:'', loadChildren:'../cms/cms.module#CmsModule'},
	{path:'dashboard',  canActivate: [AuthGuard], component:DashboardComponent},
	{path:'job-home',  canActivate: [AuthGuard], component:HomeComponent},
	{path: 'application',canActivate: [AuthGuard],component:ApplicationsComponent},
	{path: 'profile',canActivate: [AuthGuard],component:ProfileComponent},
	//{path: 'job-by-country/:name',component:JobByCountryComponent},
	{path: 'job-details/:job_type/:job_id',component:JobDetailsComponent},
	//{path: 'search-job-listing/:params', component:SearchJobListingComponent},
	//{path: 'search-job-listing/:title_id/:industry_id/:city_id', component:SearchJobListingComponent},
	{path:'search-job/:cat',canActivate: [AuthGuard],component:SearchCategoryComponent},	
	{path:'search-job-detail',canActivate: [AuthGuard],component:SearchDetailComponent},
	{path: 'search-job-list',canActivate: [AuthGuard],component:SearchListComponent},

	{path:'my-jobalert',canActivate: [AuthGuard],component:MyAlertComponent},
	{path:'job-alerts',canActivate: [AuthGuard],component:JobAlertComponent},
	{path:'recommend-alert',canActivate: [AuthGuard], component:RecommendAlertComponent},
	{path:'applied-jobs',canActivate: [AuthGuard],component:AppliedJobComponent},
	{path:'saved-jobs',canActivate: [AuthGuard],component:SavedJobComponent},
	{path:'applied-international-jobs',canActivate: [AuthGuard],component:AppliedInternationalJobsComponent},
	{path:'saved-international-jobs',canActivate: [AuthGuard],component:SavedInternationalJobsComponent},
	{path:'apply-hiring',canActivate: [AuthGuard],component:LocalWalkinComponent},
	{path:'apply-hiring-international',canActivate: [AuthGuard],component:InternationalWalkinComponent},
	{path:'slot-booking',canActivate: [AuthGuard],component:SlotBookingComponent},
	{path:'application-status',canActivate: [AuthGuard],component:AppliedJobComponent},

	{path: 'training-getting-job',canActivate: [AuthGuard],component:TrainingGettingJobComponent},
	{path: 'working-visa-training',canActivate: [AuthGuard],component:WorkingVisaTrainingComponent},
	{path: 'single-job-details/:id',canActivate: [AuthGuard],component:SingleJobDetailsComponent},
	{path: 'applied-job-details/:id',canActivate: [AuthGuard],component:AppliedJobDetailComponent},	
	{path: 'find-jobs',canActivate: [AuthGuard],component:FindJobsComponent},
	{path: 'walkin-job-details/:job_type/:id',canActivate: [AuthGuard],component:WalkinDetailsComponent},	
	{path: 'profile-view',canActivate: [AuthGuard],component:ProfileViewEmployerComponent},

	{path: 'search-job-listing', component:SearchJobListingComponent},
	{path: '', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
