import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthModule} from '../auth/auth.module';
import { CommonUsesModule } from '../common-uses/common-uses.module';
import {EmailModule} from '../email/email.module';

import { CmsModule } from '../cms/cms.module';
import { OwlModule } from 'ngx-owl-carousel';
import { LightboxModule } from 'ngx-lightbox';
import { CoreModule } from '../core/core.module';

import { EmployeeRoutingModule } from './employee-routing.module';
import { ApplicationsComponent } from './applications/applications.component';
import { JobByCountryComponent } from './job-by-country/job-by-country.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { SearchJobListingComponent } from './search-job-listing/search-job-listing.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

import { SearchDetailComponent } from './search-job/search-detail/search-detail.component';
import { SearchCategoryComponent } from './search-job/search-category/search-category.component';

import { JobAlertComponent } from './job-for/job-alert/job-alert.component';
import { AppliedJobComponent } from './job-for/applied-job/applied-job.component';
import { SavedJobComponent } from './job-for/saved-job/saved-job.component';
import { LocalWalkinComponent } from './job-for/local-walkin/local-walkin.component';
import { InternationalWalkinComponent } from './job-for/international-walkin/international-walkin.component';
import { SlotBookingComponent } from './job-for/slot-booking/slot-booking.component';
import { ApplicationStatusComponent } from './application-status/application-status.component';
import { TrainingGettingJobComponent } from './offer-training/training-getting-job/training-getting-job.component';
import { WorkingVisaTrainingComponent } from './offer-training/working-visa-training/working-visa-training.component';
import { SingleJobDetailsComponent } from './single-job-details/single-job-details.component';
import { MyAlertComponent } from './my-alert/my-alert.component';
import { RecommendAlertComponent } from './job-for/recommend-alert/recommend-alert.component';
//import { AgmCoreModule } from '@agm/core';
import { FindJobsComponent } from './find-jobs/find-jobs.component';
import { SearchListComponent } from './search-job/search-list/search-list.component';
import { AppliedJobDetailComponent } from './applied-job-detail/applied-job-detail.component';
import { WalkinDetailsComponent } from './job-for/walkin-details/walkin-details.component';
import { AppliedInternationalJobsComponent } from './job-for/applied-international-jobs/applied-international-jobs.component';
import { SavedInternationalJobsComponent } from './job-for/saved-international-jobs/saved-international-jobs.component';
import { ProfileViewEmployerComponent } from './profile-view-employer/profile-view-employer.component';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [ApplicationsComponent,
   JobByCountryComponent, JobDetailsComponent,
   SearchJobListingComponent, DashboardComponent,
   ProfileComponent, SearchDetailComponent, SearchCategoryComponent,
   JobAlertComponent,
   AppliedJobComponent, 
   SavedJobComponent, 
   LocalWalkinComponent, 
   InternationalWalkinComponent, 
   SlotBookingComponent, 
   ApplicationStatusComponent, TrainingGettingJobComponent,
    WorkingVisaTrainingComponent,
    SingleJobDetailsComponent,
    MyAlertComponent,
    RecommendAlertComponent,
    FindJobsComponent,
    SearchListComponent,
    AppliedJobDetailComponent,
    WalkinDetailsComponent,
    AppliedInternationalJobsComponent,
    SavedInternationalJobsComponent,
    ProfileViewEmployerComponent,
    HomeComponent,
    ],
  imports: [
    CommonModule,
    EmailModule,
    EmployeeRoutingModule,
    AuthModule,
    CommonUsesModule,
    CmsModule,
    OwlModule,
    LightboxModule,
    CoreModule
  ]
})
export class EmployeeModule { }
