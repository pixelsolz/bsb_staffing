import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
declare var $ :any;
import * as _ from 'lodash'; 

@Component({
  selector: 'app-find-jobs',
  templateUrl: './find-jobs.component.html',
  styleUrls: ['./find-jobs.component.css']
})
export class FindJobsComponent implements OnInit {
  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if($(event.target).hasClass('auto_comp_search')){
      if($(event.target).attr('id') =='job_title_search'){
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='industry_search'){
        this.show_jb_title_srh = false;
        this.show_city_comp_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='country_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='department_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
      }
    }else{
      this.show_jb_title_srh = false;
      this.show_industry_srh = false;
      this.show_city_comp_srh = false;
      this.adv_show_jb_title_srh = false;
      this.show_department_srh = false;
    }
  }

	advSearchForm: FormGroup;
	industrty_data: any =[];
  department_data: any =[];
	allCountries: any;
	cities:any;
	industries:any;
  departments:any;
	job_roles: any;
	emplymentFor: any;
	experiance: any;
	salaries: any;
	industriesDiv: boolean;
	storeMultiSelect :any ={
  	industries_value:[],
  	industries_name:[],
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    department_value:[],
    department_name:[]
	};

	search_job_title: any;
	search_industry: any;
  search_department: any;
	search_location: any;
	advance_search: any;
	src: any;

	searchJobsResult: any =[];

	show_jb_title_srh: boolean;
	title_job_data: any=[];
	show_city_comp_srh: boolean;
	city_country_data: any =[];
	show_industry_srh: boolean;
  show_department_srh: boolean;
	searchData = {
    title_comp:{
      id:'',
      name:'',
      table_name:''
    },
    city_country:{
      id:'',
      name:'',
      table_name:''
    },
    industry:{
      id:'',
      name:'',
      table_name:''
    },
    department:{
      id:'',
      name:'',
      table_name:''
    }
  };

  job_title_model: any;
  insudtry_model: any;
  department_model: any;
  country_loc_model: any;
  showloader:boolean;
  all_currency:any;
  internationalWalking: any=[];
  nationalWalking: any=[];
  job_title_model_array: any=[];
  insudtry_model_array: any=[];
  country_model_array: any=[]; 
  department_model_array: any=[];
  advanced_job_title_model_array: any=[];
  adv_show_jb_title_srh: boolean=false;
  countryArray: any=[];
  isloggedIn: boolean;
  search_employer_jobs : string;
  isloaded :boolean=false;
  countryDiv: boolean =false;
  cityDiv: boolean=false;
  cityArray: any=[];
  departmentDiv: boolean =false;
  departmentData: any;
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute, private messageService: MessageService,private http: HttpClient,  private eRef: ElementRef) { 

  	this.isloggedIn = localStorage.getItem('token')? true: false;
  	this.asyncInit();
  	this.showloader = false;
    this.industriesDiv = false;
    this.show_city_comp_srh = false;
    //console.log(this.activeRoute.snapshot.queryParamMap.get("search_title"));
    //this.search_job_title = this.activeRoute.snapshot.paramMap.get("title_id") ? this.activeRoute.snapshot.paramMap.get("title_id"):'';
  
    router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
      if(this.activeRoute.snapshot.queryParams){
        localStorage.setItem('search-queries', JSON.stringify(this.activeRoute.snapshot.queryParams));
      }
    	this.search_job_title = this.activeRoute.snapshot.queryParamMap.get("search_title") ? this.activeRoute.snapshot.queryParamMap.get("search_title") :'';
      this.search_industry = this.activeRoute.snapshot.queryParamMap.get("search_industry") ? this.activeRoute.snapshot.queryParamMap.get("search_industry"):'';
      this.search_industry ? this.insudtry_model_array = this.search_industry:'';
      this.search_department = this.activeRoute.snapshot.queryParamMap.get("search_department") ? this.activeRoute.snapshot.queryParamMap.get("search_department"):'';
      this.search_location = this.activeRoute.snapshot.queryParamMap.get("search_city") ? this.activeRoute.snapshot.queryParamMap.get("search_city"):'';
      
      //this.search_location = this.activeRoute.snapshot.queryParamMap.get("search_location") ? this.activeRoute.snapshot.queryParamMap.get("search_location"):'';
      this.search_employer_jobs = this.activeRoute.snapshot.queryParamMap.get("searchemployerjobs") ? this.activeRoute.snapshot.queryParamMap.get("searchemployerjobs"):'';
	    this.src = this.activeRoute.snapshot.queryParamMap.get("src") ? this.activeRoute.snapshot.queryParamMap.get("src"):'';
	    this.advance_search = localStorage.getItem('advSearch')? JSON.parse(localStorage.getItem('advSearch')):'';

      this.job_title_model = this.search_job_title ? this.search_job_title +',':''; 
      this.search_job_title ? this.job_title_model_array = this.search_job_title :'';
      this.insudtry_model = this.search_industry ? this.search_industry +',':''; 
      this.insudtry_model ? this.insudtry_model_array =this.insudtry_model:'';
      this.department_model = this.search_department; 
      this.country_loc_model = this.search_location ? this.search_location +',':'';  
      this.country_loc_model ? this.country_model_array =this.country_loc_model :'';
	    //this.getJobBysearch();
	    
  	});


  }

   ngOnInit() {
    window.scrollTo(0, 0);
  	this.advSearchForm= this.fb.group({
      job_title:'',
      employement_for:'',
      country_id:'',
      city_id:'',
      industry_id:'',
      department_id:'',
      min_experiance:'',
      max_experiance:'',
      salary_type:'',
      currency:'',
      min_monthly_salary:'',
      max_monthly_salary:'',
    });
    
     this.advSearchForm.get('country_id').valueChanges.subscribe(value=>{
      let countryCode = this.allCountries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
    });
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.allCountries = data['countries'];
        this.countryArray = {"":data['countries']};
        this.industries = data['industries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
        this.job_roles = data['job_roles'];
        let otherData = data['other_mst'];
        this.emplymentFor = otherData.filter((data) => data.entity == 'emplyment_for');
        this.experiance = data['allExprience'];
        this.salaries = otherData.filter((data) => data.entity == 'salary');
        this.departments = data['all_parent_department'];
        this.all_currency = data['all_currency'];   
        this.departmentData = data['all_department'];    
      }
    });
  }

  searchAutoComplete(type, key) {
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
    if(type ==='COUNTRY_WISE'){
      if(key.target.value.trim().length ==0){
        this.country_model_array.length =0;
        this.country_loc_model ='';
      }
      if(this.country_loc_model){
        let titleDataArr = this.country_loc_model.split(',');
        let arrFilter = this.country_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.country_model_array = arrFilter;
      } 
      usedData = this.country_loc_model;
    }else if(type ==='INDUSTRY_WISE'){
      if(key.target.value.trim().length ==0){
        this.insudtry_model_array.length =0;
        this.insudtry_model ='';
      } 
      if(this.insudtry_model){
        let titleDataArr = this.insudtry_model.split(',');
        let arrFilter = this.insudtry_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.insudtry_model_array = arrFilter;
      } 
      usedData = this.insudtry_model;
    }else if(type ==='JB_TITLE_WISE'){
      if(key.target.value.trim().length ==0){
        this.job_title_model_array.length =0;
        this.job_title_model ='';
      } 
      if(this.job_title_model){
        let titleDataArr = this.job_title_model.split(',');
        let arrFilter = this.job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.job_title_model_array = arrFilter;
      } 
      usedData = this.job_title_model;
    } else if(type ==='DEPARTMENT_WISE'){
      if(key.target.value.trim().length ==0){
        this.department_model_array.length =0;
        this.department_model ='';
      } 
      if(this.job_title_model){
        let titleDataArr = this.department_model.split(',');
        let arrFilter = this.department_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.department_model_array = arrFilter;
      } 
      usedData = this.department_model;
    }
    if(!searchdata){
      this.show_jb_title_srh = false;
      this.show_city_comp_srh = false;
      this.show_industry_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.show_jb_title_srh = true;
            this.title_job_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'COUNTRY_WISE') {
            this.show_city_comp_srh = true;
            this.city_country_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'INDUSTRY_WISE') {
            this.show_industry_srh = true;
            this.industrty_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }else if (data.type == 'DEPARTMENT_WISE') {
            this.show_department_srh = true;
            this.department_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }
        }


      }, (error) => { });
  }


  autoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.show_jb_title_srh = false;
      this.searchData.title_comp.id = titJob.id;
      if(this.job_title_model_array.length){
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = this.job_title_model_array.toString() + ',';
      }else{
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = titJob.name + ',';
      }
      document.getElementById('job_title_search').focus();
    } else if (type == 'COUNTRY_WISE') {
      const city_count = this.city_country_data.find((data) => data.id == String(id));
      this.show_city_comp_srh = false;
      if(this.country_model_array.length){
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }else{
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = city_count.name + ',';
      }
       document.getElementById('country_search').focus();
      this.searchData.city_country.id = city_count.id;
      //this.searchData.city_country.name = city_count.name;
    } else if (type == 'INDUSTRY_WISE') {
      const industry_count = this.industrty_data.find((data) => data.id == String(id));
      this.show_industry_srh = false;
      if(this.insudtry_model_array.length){
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }else{
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = industry_count.name + ',';
      }
      this.searchData.industry.id = industry_count.id;
      //this.searchData.industry.name = industry_count.name;
    }
    else if (type == 'DEPARTMENT_WISE') {
      const department_count = this.department_data.find((data) => data.id == String(id));
      this.show_department_srh = false;
      if(this.department_model_array.length){
        this.department_model_array =[...this.department_model_array, department_count.name];
        this.department_model = this.department_model_array.toString() + ',';
      }else{
        this.department_model_array =[...this.department_model_array, department_count.name];
        this.department_model = department_count.name + ',';
      }
      this.searchData.industry.id = department_count.id;
    }

  }


  advancedAutoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.adv_show_jb_title_srh = false;
      if(this.advanced_job_title_model_array.length){
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(this.advanced_job_title_model_array.toString() + ',');
      }else{
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(titJob.name + ',');
      }
      document.getElementById('jobTitle').focus();
    } 

  }


  advancedSearchAutoComplete(type, key){
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
  	if(type ==='JB_TITLE_WISE'){
      if(key.target.value.trim().length ==0){
        this.advanced_job_title_model_array.length =0;
        this.advSearchForm.controls['job_title'].setValue('');
      } 
      if(this.advSearchForm.controls['job_title'].value){
        let titleDataArr = this.advSearchForm.controls['job_title'].value.split(',');
        let arrFilter = this.advanced_job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.advanced_job_title_model_array = arrFilter;
      } 

       usedData =  this.advSearchForm.controls['job_title'].value;
  	}
    if(!searchdata){
      this.adv_show_jb_title_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.adv_show_jb_title_srh = true;
            this.title_job_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } 
        }


      }, (error) => { });
  }

  
  getOnchangeEvent(type) {
    if (type == 'industries') {
      this.industriesDiv = true;
    }else if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }else if(type =='departments'){
      this.departmentDiv = true;
    }
  }

  checkmultiselect(e){
    if (e.field_name == 'industries') {
      this.industriesDiv = e.status;
    }else if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDiv = e.status;
    }
  }

  changeSubmitVal(){
    this.advSearchForm.patchValue({
      industry_id:this.storeMultiSelect.industries_value.toString(),
    });
  }

  multiselectAll(e){  
    //console.log(e.storedValue);
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='industries'){
      this.advSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }else if(filed =='departments'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  getChange(e){
    //console.log(e);
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='industries'){
      this.advSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }else if(e.field_name =='departments'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
        break; 
      case "departments":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          department_id: this.storeMultiSelect.department_value
       });
        break;         
      default:
        // code...
        break;
    }
  }

  searchFilter() {

    let search_title = this.searchData.title_comp.name ? this.searchData.title_comp.name : (this.job_title_model ? this.job_title_model.replace(/,\s*$/, "") : '');
    let search_industry = this.searchData.industry.name ? this.searchData.industry.name : (this.insudtry_model ? this.insudtry_model.replace(/,\s*$/, "") : '');
    let search_city = this.searchData.city_country.name ? this.searchData.city_country.name : (this.country_loc_model ? this.country_loc_model.replace(/,\s*$/, "") : '');
    let departmentSearch = this.searchData.department.name ? this.searchData.department.name:  (this.department_model ? this.department_model.replace(/,\s*$/, "") : '');
    //let departmentSearch =  this.department_model ? this.department_model.replace(/,\s*$/, "") : '';
    if (search_title != '' || search_industry != '' || search_city != '' || departmentSearch != '') {
      //this.router.navigate(['/search-job-list'], { queryParams: { 'job_title': search_title, 'search_industry': search_industry, 'search_city': search_city, 'search_department':departmentSearch } });
      this.router.navigate(['/search-job-list'], { queryParams: { 'search_title': search_title, 'search_industry': search_industry, 'search_location': search_city, 'search_department':departmentSearch } });
    } else {
      this.messageService.warning("Please Enter Value");
      return false;
    }


  }

  searchJobs(){
    this.advSearchForm.controls['country_id'].setValue(this.storeMultiSelect.country_value);
    this.advSearchForm.controls['city_id'].setValue(this.storeMultiSelect.city_value);
    this.advSearchForm.controls['industry_id'].setValue(this.storeMultiSelect.industries_value);
    this.advSearchForm.controls['department_id'].setValue(this.storeMultiSelect.department_value);
    let jbTitleValue = this.advSearchForm.controls['job_title'].value.replace(/,\s*$/, "");
    this.advSearchForm.controls['job_title'].setValue(jbTitleValue.split(','));
    localStorage.setItem('advSearch', JSON.stringify(this.advSearchForm.value));
    this.router.navigate(['/search-job-list'], { queryParams: { src: 'advance_search' } });
    
    /*this.router.navigate(['/search-job-list'], { 
      queryParams: { 
        'job_title': this.advSearchForm.value.job_title ?this.advSearchForm.value.job_title.replace(/,\s*$/, "") : '',
        'country_list': this.storeMultiSelect.country_value ? this.storeMultiSelect.country_value.toString():'',
        'city_list': this.storeMultiSelect.city_value ? this.storeMultiSelect.city_value.toString():'',
        'industry_list': this.storeMultiSelect.industries_value ? this.storeMultiSelect.industries_value.toString():'',
        'department_list': this.storeMultiSelect.department_value ? this.storeMultiSelect.department_value.toString():'',
        'min_exp':this.advSearchForm.value.min_experiance ? this.advSearchForm.value.min_experiance:'',
        'max_exp':this.advSearchForm.value.max_experiance ? this.advSearchForm.value.max_experiance:'',
        'salary_type':this.advSearchForm.value.salary_type ? this.advSearchForm.value.salary_type: '',
        'currency_type':this.advSearchForm.value.currency ? this.advSearchForm.value.currency: '',
        'min_salary':this.advSearchForm.value.min_monthly_salary ? this.advSearchForm.value.min_monthly_salary:'',
        'max_salary':this.advSearchForm.value.max_monthly_salary ? this.advSearchForm.value.max_monthly_salary:''

      } });*/
  }


  modalClose(){
    this.advanced_job_title_model_array.length =0;
    this.advSearchForm.reset();
  }



}
