import { Component, OnInit, Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild, HostListener, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Meta, Title } from '@angular/platform-browser';
declare var $ :any;
import * as _ from 'lodash'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

 @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if($(event.target).hasClass('auto_comp_search')){
      if($(event.target).attr('id') =='job_title_search'){
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
      }else if($(event.target).attr('id') =='insudtry_search'){
        this.show_jb_title_srh = false;
        this.show_city_comp_srh = false;
      }else if($(event.target).attr('id') =='country_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
      }
    }else{
      this.show_jb_title_srh = false;
      this.show_industry_srh = false;
      this.show_city_comp_srh = false;
      this.adv_show_jb_title_srh = false;
    }
  }

    /*@HostListener("window:scroll", ["$event.target"])
  scrollHandler(elem) {
     if($(window).scrollTop() >=  $('#add_moreDiv').outerHeight() - window.innerHeight) {
        this.addMoreCountry();
    }
    if($(window).scrollTop() <= 325){
      window.location.hash ='';
    }
  }*/


  showloader: boolean;
  advSearchForm: FormGroup;
  jobTitleSrh: string;
  departmentData: any;
  title_comp_array: any = [];
  show_jb_title_srh: boolean;
  show_city_comp_srh: boolean;
  show_industry_srh: boolean;
  title_job_data: any = [];
  city_country_data: any = [];
  allcountrySort: any=[];
  industrty_data: any = [];
  searchData = {
    title_comp: {
      id: '',
      name: '',
      table_name: ''
    },
    city_country: {
      id: '',
      name: '',
      table_name: ''
    },
    industry: {
      id: '',
      name: '',
      table_name: ''
    }
  };
  count: number;
  currentImage: string;
  countries: any = [];
  allCountries: any;
  cities: any;
  coutryWiseCity: any=[];
  industries: any;
  departments: any;
  job_roles: any;
  emplymentFor: any;
  experiance: any;
  salaries: any;
  all_currency: any;
  industriesDiv: boolean;
  departmentDiv: boolean =false;
  storeMultiSelect: any = {
    industries_value: [],
    industries_name: [],
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    department_name:[],
    department_value:[]
  };
  job_title_model: any;
  job_title_model_array: any=[];
  insudtry_model: any;
  insudtry_model_array: any=[];
  country_loc_model: any ;
  country_model_array: any=[];
  jobs_by_title: any;
  jobs_by_city: any;
  jobs_by_industry: any;
  popular_job_search: any;
  cityDiv: boolean =false;
  countryDiv: boolean =false;
  countryArray: any=[];
  cityArray: any=[];
  advanced_job_title_model_array : any=[];
  adv_show_jb_title_srh: boolean =false;

  emplorerSlideImages: any;
  emplorerSlideOptions: any;
  emplorerCarouselOptions = { items: 1, dots: false, nav: true, autoplay: true, loop: true, margin: 0, responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 5 } } };

  showcountries: any = [];
  countshowcountres: number;
  country_name_by_ip: any;
  alphabets:any = [];
  isloaded: boolean = false;
  previousIdprint:string ='';
  countryIVal:number;
  settedCountryNameArr: any=[];
  @ViewChild('slideData') slideData: ElementRef;
  constructor(private fb: FormBuilder, private message: MessageService, private renderer2: Renderer2, private renderer: Renderer, private router: Router, private commonService: CommonService,
    private global: Global, private authService: AuthService,private http: HttpClient,private activeRoute: ActivatedRoute, private eRef: ElementRef, private title: Title,
    private meta: Meta) {
    this.asyncInit();
    this.show_city_comp_srh = false;
    this.industriesDiv = false;
     this.countryIVal = 0;
    for (let i = 65; i <= 90;i++) {
        this.alphabets.push(String.fromCharCode(i));
    }

  }

  ngOnInit() {
    this.title.setTitle('Job Vacancies | Recruitment | Job Search- bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Search for Job Vacancies across the World at bsbstaffing.com, Global Job Portal. Search and Discover Jobs across Top Companies. Apply Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'Jobs,Job Vacancies' });
    window.scrollTo(0, 0);
    this.advSearchForm = this.fb.group({
      job_title: '',
      employement_for: '',
      country_id: '',
      city_id: '',
      industry_id: '',
      department_id: '',
      min_experiance: '',
      max_experiance: '',
      salary_type: '',
      currency: '',
      min_monthly_salary: '',
      max_monthly_salary: '',
    });

    this.advSearchForm.get('country_id').valueChanges.subscribe(value=>{
      let countryCode = this.countries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
    });

    this.getData();

  }

  asyncInit() {
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.allCountries = data['countries'];
        this.countryArray = {"":data['countries']};
        this.industries = data['industries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
           this.cities = data['cities'];
           this.cityArray = {"":data['cities']}; 
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
        this.commonService.callFooterMenu(1);
        if(data && data.status ==200){
        this.job_roles = data['job_roles'];
        let otherData = data['other_mst'];
        this.emplymentFor = otherData.filter((data) => data.entity == 'emplyment_for');
        this.experiance = data['allExprience'];
        this.salaries = otherData.filter((data) => data.entity == 'salary');
        this.departments = data['all_parent_department'];
        this.all_currency = data['all_currency']; 
        this.departmentData = data['all_department'];
        }
      });


    this.commonService.getSliderImages.subscribe(data=>{
      if(!data){
        this.commonService.getAll('/api/logo-branding')
      .subscribe((data) => {
        this.emplorerSlideImages = data.data;
        this.commonService.callSliderImages(data.data)
        this.isloaded = true;
      }, (error) => { });
      }else{
        this.emplorerSlideImages = data;
        this.isloaded = true;
      }
    }); 

    

    this.emplorerSlideOptions = { items: 3, dots: false, nav: false, autoplay: true, loop: true, margin: 0, responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 5 } } };
  }


  getData() {
    this.showloader = true;
    this.commonService.getAll('/api/country-list')
      .subscribe((data) => {
        this.commonService.callFooterMenu(1);
        this.showloader = false;
        this.countries = data.data;
        this.country_name_by_ip = data.country_name_by_ip;
       let firstcountriesNames= ['United States', 'Canada', 'Australia', 'United Kingdom', 'Germany', 'United Arab Emirates', 'Qatar', 'Singapore', 'China', 'Kuwait', 'Bahrain', 'Saudi Arabia', 'Oman', 'Malaysia', 'Maldives', 'Poland', 'New Zealand']; 
       let firstorderCountries =  this.countries.filter(obj=>firstcountriesNames.indexOf(obj.name) != -1);
       let secondorderCountries =  this.countries.filter(obj=>firstcountriesNames.indexOf(obj.name) == -1).sort((a,b)=>(a.name > b.name)? 1: -1);
       let userCountry = this.country_name_by_ip;

       if(firstorderCountries.find(obj=>obj.name == userCountry)){
           let firstCountry= firstorderCountries.filter(obj=>obj.name == userCountry);
           let firstList =firstorderCountries.filter(obj=> obj.name !=userCountry).map(obj=>{
               let orderNum = firstcountriesNames.indexOf(obj.name);
               return {_id:obj._id, code:obj.code, currency: obj.currency, currency_code:obj.currency_code, 
                 dial_code:obj.dial_code, employee_pref_id:obj.employee_pref_id, image:obj.image, job_count:obj.job_count,
                 jobs: obj.jobs, name:obj.name, updated_at: obj.updated_at, order: Number(orderNum)+1}
           });
          let firstOrder = firstList.sort((a, b) => (a.order > b.order) ? 1 : -1);
          let mergeTwo = firstCountry.concat(firstOrder);
          //let secondOrder =secondorderCountries.sort((a,b)=>(a.name > b.name) ? 1 : -1);
          //let secondOrder =secondorderCountries.sort((a,b)=>(a.job_count > b.job_count) ? 1 : -1);
          let secondOrder =secondorderCountries;
          this.allcountrySort = mergeTwo.concat(secondOrder);

       }else{
         let firstCountry= secondorderCountries.filter(obj=>obj.name == userCountry);
         let firstList =firstorderCountries.map(obj=>{
               let orderNum = firstcountriesNames.indexOf(obj.name);
               return {_id:obj._id, code:obj.code, currency: obj.currency, currency_code:obj.currency_code, 
                 dial_code:obj.dial_code, employee_pref_id:obj.employee_pref_id, image:obj.image, job_count:obj.job_count,
                 jobs: obj.jobs, name:obj.name, updated_at: obj.updated_at, order: Number(orderNum)+1}
           });
         let firstOrder = firstList.sort((a, b) => (a.order > b.order) ? 1 : -1);
         let mergeTwo = firstCountry.concat(firstOrder);
         //let secondOrder =secondorderCountries.filter(obj=>obj.name !=userCountry).sort((a,b)=>(a.name > b.name) ? 1 : -1);
         //let secondOrder =secondorderCountries.filter(obj=>obj.name !=userCountry).sort((a,b)=>(a.job_count > b.job_count) ? 1 : -1);
         let secondOrder =secondorderCountries.filter(obj=>obj.name !=userCountry);
         this.allcountrySort = mergeTwo.concat(secondOrder);
;
       }
       	this.showcountries = this.allcountrySort
        /*this.countshowcountres = 6;
        this.allcountrySort.forEach((obj, i) => {
          if (i < 6) {
            this.showcountries.push(obj);
          }
        });*/

               setTimeout(()=>{    
               	console.log($("#" + this.activeRoute.snapshot.queryParams.alpha))
                $('html,body').animate({
                    scrollTop: $("#" + this.activeRoute.snapshot.queryParams.alpha).offset().top -50
                  },
                    'slow');
               },500);
      }, (error) => { });

    this.commonService.getAll('/api/footer-job-listing')
      .subscribe((data) => {
        this.showloader = false;
        this.jobs_by_title = data.data.jobs_by_title;
        this.jobs_by_city = data.data.jobs_by_city;
        this.jobs_by_industry = data.data.jobs_by_industry;
        this.popular_job_search = data.data.popular_job_search;

      }, (error) => { });


  }

  addMoreCountry() {
    this.showloader = true;
    this.countshowcountres = this.countshowcountres + 6;
    let limit = 6 + this.showcountries.length;
    this.showcountries.length = 0;
    this.allcountrySort.forEach((obj, i) => {
      if (i < limit) {
        this.showcountries.push(obj);
      }
    });
    this.showloader = false;
  }

  allCountryShow(id){
    this.showloader = true;
     setTimeout(()=>{    
                $('html,body').animate({
                    scrollTop: $("#" + id).offset().top -50
                  },
                    'slow');
                this.showloader = false;
               },500);
    /*this.showcountries.length =0;
    this.allcountrySort.forEach((obj, i) => {      
        this.showcountries.push(obj);     
    });
    setTimeout(()=>{    
    window.location.hash = id;
    let pos = window.pageYOffset;
    if (pos > 0) {
        window.scrollTo(0, pos - 90);
    } 
    this.showloader = false;
 }, 500);*/
    
  }

  countryAlphaid(country_name,i){ 
    if(country_name.trim() && i >18){
     let str = country_name.trim().charAt(0);
     return str;
/*    if(this.settedCountryNameArr.indexOf(str) === -1){
      this.settedCountryNameArr.push(str);
      return str ;
    }else{
      this.settedCountryNameArr.push(str);
      return '';
    } */ 
 
    }else{
      return '';
    }


    

    // this.countryIVal = i;
    // //console.log(i);
    // if(i >= this.countryIVal){
    //   console.log('tset');
    // }
   
    // let firstcountriesNames= ['United States', 'Canada', 'Australia', 'United Kingdom', 'Germany', 'United Arab Emirates', 'Qatar', 'Singapore', 'China', 'Kuwait', 'Bahrain', 'Saudi Arabia', 'Oman', 'Malaysia', 'Maldives', 'Poland', 'New Zealand'];
    // if(firstcountriesNames.find(obj=>obj == country_name)){
    //   //console.log(country_name);
    // }else if(this.country_name_by_ip == country_name){

    // }else{
    //   let name = country_name.replace(/\s/g, '');
    //   //console.log(name);
    //   let res = name.charAt(0);
    //   if(res){
    //     if(res == this.previousIdprint){
    //       return country_name.replace(/\s/g, '')+'_'+i;
    //     }else{
    //       this.previousIdprint= res;
    //       return res;
          
    //     }
    //   }
    // }
  }

  searchAutoComplete(type, key) {
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
    if(type ==='COUNTRY_WISE'){
      /*if(key.which == 188){
        this.country_model_array =[...this.country_model_array, typedKeyArray[typedKeyArray.length -1]];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }*/
      if(key.target.value.trim().length ==0){
        this.country_model_array.length =0;
        this.country_loc_model ='';
      }
      if(this.country_loc_model){
        let titleDataArr = this.country_loc_model.split(',');
        let arrFilter = this.country_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.country_model_array = arrFilter;
      } 
      usedData = this.country_loc_model;
    }else if(type ==='INDUSTRY_WISE'){
      /*if(key.which == 188){
        this.insudtry_model_array =[...this.insudtry_model_array, typedKeyArray[typedKeyArray.length -1]];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }*/
      if(key.target.value.trim().length ==0){
        this.insudtry_model_array.length =0;
        this.insudtry_model ='';
      } 
      if(this.insudtry_model){
        let titleDataArr = this.insudtry_model.split(',');
        let arrFilter = this.insudtry_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.insudtry_model_array = arrFilter;
      } 
      usedData = this.insudtry_model;
    }else if(type ==='JB_TITLE_WISE'){
      /*if(key.which == 188){
        this.job_title_model_array =[...this.job_title_model_array, typedKeyArray[typedKeyArray.length -1]];
        this.job_title_model = this.job_title_model_array.toString() + ',';
      }*/
      if(key.target.value.trim().length ==0){
        this.job_title_model_array.length =0;
        this.job_title_model ='';
      } 
      if(this.job_title_model){
        let titleDataArr = this.job_title_model.split(',');
        let arrFilter = this.job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.job_title_model_array = arrFilter;
      } 
      usedData = this.job_title_model;
    }
    if(!searchdata){
      this.show_jb_title_srh = false;
      this.show_city_comp_srh = false;
      this.show_industry_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.show_jb_title_srh = true;
            this.title_job_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'COUNTRY_WISE') {
            this.show_city_comp_srh = true;
            this.city_country_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'INDUSTRY_WISE') {
            this.show_industry_srh = true;
            this.industrty_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }
        }


      }, (error) => { });
  }

  autoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.show_jb_title_srh = false;
      this.searchData.title_comp.id = titJob.id;
      if(this.job_title_model_array.length){
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = this.job_title_model_array.toString() + ',';
      }else{
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = titJob.name + ',';
      }
      document.getElementById('job_title_search').focus();
    } else if (type == 'COUNTRY_WISE') {
      const city_count = this.city_country_data.find((data) => data.id == String(id));
      this.show_city_comp_srh = false;
      if(this.country_model_array.length){
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }else{
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = city_count.name + ',';
      }
       document.getElementById('country_search').focus();
      this.searchData.city_country.id = city_count.id;
      //this.searchData.city_country.name = city_count.name;
    } else if (type == 'INDUSTRY_WISE') {
      const industry_count = this.industrty_data.find((data) => data.id == String(id));
      this.show_industry_srh = false;
      if(this.insudtry_model_array.length){
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }else{
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = industry_count.name + ',';
      }
      this.searchData.industry.id = industry_count.id;
      //this.searchData.industry.name = industry_count.name;
    }

  }

    advancedAutoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.adv_show_jb_title_srh = false;
      if(this.advanced_job_title_model_array.length){
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(this.advanced_job_title_model_array.toString() + ',');
      }else{
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(titJob.name + ',');
      }
      document.getElementById('jobTitle').focus();
    } 

  }

  advancedSearchAutoComplete(type, key){
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
  if(type ==='JB_TITLE_WISE'){

      if(key.target.value.trim().length ==0){
        this.advanced_job_title_model_array.length =0;
        this.advSearchForm.controls['job_title'].setValue('');
      } 
      if(this.advSearchForm.controls['job_title'].value){
        let titleDataArr = this.advSearchForm.controls['job_title'].value.split(',');
        let arrFilter = this.advanced_job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.advanced_job_title_model_array = arrFilter;
      } 

       usedData =  this.advSearchForm.controls['job_title'].value;
    }
    if(!searchdata){
      this.adv_show_jb_title_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.adv_show_jb_title_srh = true;
            this.title_job_data = data.data;
          } 
        }


      }, (error) => { });
    }


  searchFilter() {

    let search_title = this.searchData.title_comp.name ? this.searchData.title_comp.name : (this.job_title_model ? this.job_title_model.replace(/,\s*$/, "") : '');
    let search_industry = this.searchData.industry.name ? this.searchData.industry.name : (this.insudtry_model ? this.insudtry_model.replace(/,\s*$/, "") : '');
    let search_city = this.searchData.city_country.name ? this.searchData.city_country.name : (this.country_loc_model ? this.country_loc_model.replace(/,\s*$/, "") : '');
    //let params = search_title + '-' + (search_industry ? search_industry + '-':'') + (search_city ? search_city + '-':'');
    //this.router.navigate(['/search-job-listing',search_title,search_industry,search_city]);
    if (search_title != '' || search_industry != '' || search_city != '') {
      //return true;
      this.router.navigate(['/search-job-list'], { queryParams: { search_title: search_title, 'search_industry': search_industry, 'search_location': search_city } });
    } else {
      this.message.warning("Please Enter Value");
      return false;
    }

    if (search_title != '') {

      this.commonService.getAll('/api/employer/save-search-keyword?keyword=' + search_title)
        .subscribe((data) => {
          if (data.status == 200) {

          }
        }, (error) => {

        });

    }

  }


  searchJobs() {
    this.advSearchForm.controls['country_id'].setValue(this.storeMultiSelect.country_value);
    this.advSearchForm.controls['city_id'].setValue(this.storeMultiSelect.city_value);
    this.advSearchForm.controls['industry_id'].setValue(this.storeMultiSelect.industries_value);
    this.advSearchForm.controls['department_id'].setValue(this.storeMultiSelect.department_value);
    let jbTitleValue = this.advSearchForm.controls['job_title'].value.replace(/,\s*$/, "");
    this.advSearchForm.controls['job_title'].setValue(jbTitleValue.split(','));
    localStorage.setItem('advSearch', JSON.stringify(this.advSearchForm.value));
    this.router.navigate(['/search-job-list'], { queryParams: { src: 'advance_search' } });
  }

  getOnchangeEvent(type) {
    if (type == 'industries') {
      this.industriesDiv = true;
    }else if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }else if(type =='department'){
      this.departmentDiv = true;
    }
  }

  checkmultiselect(e) {
    if (e.field_name == 'industries') {
      this.industriesDiv = e.status;
    }else if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDiv = e.status;
    }
  }

  changeSubmitVal() {
    this.advSearchForm.patchValue({
      industry_id: this.storeMultiSelect.industries_value.toString(),
    });
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
      case "department":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          department_id: this.storeMultiSelect.department_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  multiselectAll(e) {
    //console.log(e.storedValue);
    let filed = e.field_name;
    if (e.event) {
      e.arrValue.forEach((value, i) => {
        let index = e.storedValue.indexOf(value);
        if (index == -1) {
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }
      });
    } else {
      e.arrValue.forEach(value => {
        let index = e.storedValue.indexOf(value);
        if (index != -1) {
          e.storedValue.splice(index, 1);
          e.storedName.splice(index, 1);
        }
      });
    }
    if (filed == 'industries') {
      this.advSearchForm.patchValue({
        industry_id: e.storedValue
      });
    }
    else if(filed =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
     else if(filed =='department'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  getChange(e) {
    if (e.event) {
      let index = e.storedValue.indexOf(e.value);
      if (index == -1) {
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    } else {
      let index = e.storedValue.indexOf(e.value);
      if (index != -1) {
        e.storedValue.splice(index, 1);
        e.storedName.splice(index, 1);
      }
    }
    if (e.field_name == 'industries') {
      this.advSearchForm.patchValue({
        industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }else if(e.field_name =='department'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  scroleToTop(){
    //window.scroll(0,0);
    $('html, body').animate({
          scrollTop:0
    }, 800, function(){ 
    });
  }

}