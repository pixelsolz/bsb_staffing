import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobByCountryComponent } from './job-by-country.component';

describe('JobByCountryComponent', () => {
  let component: JobByCountryComponent;
  let fixture: ComponentFixture<JobByCountryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobByCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobByCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
