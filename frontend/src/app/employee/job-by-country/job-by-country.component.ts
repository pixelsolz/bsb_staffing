import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { ActivatedRoute } from "@angular/router";
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-job-by-country',
  templateUrl: './job-by-country.component.html',
  styleUrls: ['./job-by-country.component.css']
})
export class JobByCountryComponent implements OnInit {
	countryName :any;
	countryJobs: any =[];
	constructor(private global: Global,private route: ActivatedRoute,private commonService: CommonService) {
  	this.countryName = this.route.snapshot.params.name;

   }

  ngOnInit() {

  	this.getData();
    //console.log(this.countryName);

  }

  getData(){
    this.commonService.getAll('/api/job-list-by-country/' + this.countryName)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.countryJobs = data.countryJobs;
        console.log(data);
      }, (error)=>{});
  }

  addMoreJob(){
    let limit = 12 + this.countryJobs.length;
    //console.log(limit);
    this.commonService.getAll('/api/job-list-by-country/'+this.countryName+'?limit=' + Number(limit))
      .subscribe((data)=>{
        this.countryJobs = data.countryJobs;
        //console.log(data);
      }, (error)=>{});
  }

}
