import { Component, OnInit, OnDestroy } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';
import {Global} from '../../global';
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
declare var Swal: any;
import * as _ from 'lodash';
declare var $;

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent implements OnInit, OnDestroy {
  logingUser = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
  loginEmpProfile: any;
  shareUrl:string;
	allImage : any;
  job_id :string;
  job_details:any={};
  jobPostType:string;
  galleryImages:any=[];
  companyImages:any=[];
  imgUrl :any;
  isloggedIn: boolean;
  job_type: string;
  isloading: boolean =false;
  prevQueryParams: any;
	isloaded:boolean=false;
  showloader:boolean;
  emplorerSlideImages = ['assets/images/clientLogo1.png','assets/images/clientLogo2.png','assets/images/clientLogo3.png','assets/images/clientLogo4.png','assets/images/clientLogo5.png'];
  //myCarouselImages = [1,2,3,4,5,6].map((i)=> `https://picsum.photos/640/480?image=${i}`);
  emplorerSlideOptions={items: 5, dots: false, nav: false, autoplay:true, loop:true, margin:0,responsive:{0:{items:2},600:{items:3},1000:{items:5}}};
  emplorerCarouselOptions={items: 1, dots: false, nav: true, autoplay:true, loop:true, margin:0,responsive:{0:{items:2},600:{items:3},1000:{items:5}}};
  jobsData: any=[];
  currentDataIndex: number;
  job_post_type: string;
  email_for_alert: string;
  similar_jobs:any;
  count_similar_job_show:number;
  similar_job_count:number;
  postGraduateData: any=[];
  underGraduateData: any=[];
  advSearch: any;
  lat: any;
  lng: any;
  isdashboardUrl:any;
  isApplied: boolean = false;
  isSaved: boolean = false;
  questionAnswers:any={};
    benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Answer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };

  constructor(private _lightbox: Lightbox,private global: Global,private route: ActivatedRoute,
             private commonService: CommonService, private messageService: MessageService, private router: Router,private fb:FormBuilder) { 
  this.shareUrl = this.global.baseUrl + 'job-details/' +this.route.snapshot.params.job_type + '/' + this.route.snapshot.params.job_id; 
  this.job_id = this.route.snapshot.params.job_id;
  this.job_type = this.route.snapshot.params.job_type;
  this.imgUrl = global.imageUrl;
  this.isloggedIn = localStorage.getItem('token')? true: false;
  this.prevQueryParams = localStorage.getItem('search-queries') ? JSON.parse(localStorage.getItem('search-queries')) :'';
  this.advSearch = localStorage.getItem('advSearch') ? JSON.parse(localStorage.getItem('advSearch')) :'';
  this.isdashboardUrl = this.route.snapshot.queryParams && this.route.snapshot.queryParams['backUrl'] =='dashboard'? true: false;

    if(this.route.snapshot.queryParams && this.route.snapshot.queryParams['backUrl']){
      if(this.route.snapshot.queryParams['backUrl'] =='applied-jobs' || this.route.snapshot.queryParams['backUrl'] =='applied-international-jobs'){
        this.isApplied = true;
        this.isSaved = true;
      }else if(this.route.snapshot.queryParams['backUrl'] =='saved-jobs' || this.route.snapshot.queryParams['backUrl'] =='saved-international-jobs'){
         this.isSaved = true;
      }
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0); 
    let searchqueries = localStorage.getItem('search-queries') ? JSON.parse(localStorage.getItem('search-queries')) :'';

        this.commonService.getCommonData.subscribe(data=>{
          if(data && data.status ==200){
          this.underGraduateData=data['under_graduate_deg'];
          this.postGraduateData=data['post_graduate_deg']     
          }
        });
         this.asyncInit();
        this.commonService.getSliderImages.subscribe(data=>{
        if(!data){
          this.commonService.getAll('/api/logo-branding')
        .subscribe((data) => {
          this.emplorerSlideImages = data.data;
          this.commonService.callSliderImages(data.data)
          this.isloaded = true;
        }, (error) => { });
        }else{
          this.emplorerSlideImages = data;
          this.isloaded = true;
        }
      }); 

        this.emplorerSlideOptions = {items: 3, dots: false, nav: false, autoplay:true, loop:true, margin:0,responsive:{0:{items:2},600:{items:3},1000:{items:5}}};
    
    if(this.job_type =='job-post'){
      this.similarJobs(this.job_id);
    }


  }

    ngOnDestroy() {
        localStorage.removeItem('storeJobData');
    }

  asyncInit(){    
    this.isloading = true;
    if(this.isloggedIn){
      this.commonService.getAll('/api/employee-profile').subscribe(res=>{
        if (res.status == 200) {
          this.loginEmpProfile = res.data;
        }
      });
    }
    
    this.commonService.getJobsData.subscribe(data=>{
      let jobSearchData =[];
      this.commonService.callFooterMenu(1);
       if(this.job_type =='local-walking'){
         jobSearchData = data.nationalWalking.filter(obj=>obj.walkin_location_type ==1);
       }else if(this.job_type =='international-walking'){
         jobSearchData = data.internationWalking.filter(obj=>obj.walkin_location_type ==2);
       }else if(this.job_type =='international-job'){
         jobSearchData = data.internationJobs;
       }else if(this.job_type =='job-alert'){
          jobSearchData = data.alert_jobs;
       }else if(this.job_type =='range-job'){
         jobSearchData = data.miles_jobs;
       }
       else{
         jobSearchData = data.jobs;
       }

       console.log(jobSearchData, '999994')
       if(jobSearchData && jobSearchData.length > 0){
         localStorage.setItem('storeJobData', JSON.stringify(jobSearchData));
       }
       
       this.jobsData  = JSON.parse(localStorage.getItem('storeJobData'));
       this.job_details =!_.isEmpty(this.jobsData)? this.jobsData.find(obj=>obj._id == this.job_id):'';
       if(!_.isEmpty(this.job_details)){
         this.galleryImages = this.job_details && this.job_details.employer_gallery ? this.job_details.employer_gallery :[];
         this.lat = this.job_details ? this.job_details.employer_lat :'';
         this.lng = this.job_details ? this.job_details.employer_lng:'';
         this.currentDataIndex = this.jobsData.findIndex(obj=>obj._id == this.job_id);
         this.isloading = false;
       }else{
         this.commonService.getAll(`/api/employee/get-singlejob/${this.job_id}/${this.route.snapshot.params.job_type}`)
             .subscribe(res=>{
               if(res.status ==200){
                 this.job_details = res.data[0];
                 this.jobsData = res.data;
                  this.galleryImages = this.job_details && this.job_details.employer_gallery ? this.job_details.employer_gallery :[];
                   this.lat = this.job_details ? this.job_details.employer_lat :'';
                   this.lng = this.job_details ? this.job_details.employer_lng:'';
                   this.currentDataIndex = this.jobsData.findIndex(obj=>obj._id == this.job_id);
                   this.isloading = false;
               }
               this.isloading = false;
             })

       }

        //this.isloading = false;

    })

  }

  getPrevData(){
    let index = this.currentDataIndex -1;
    if(index >=0){
      this.job_details =this.jobsData[index];
      //this.galleryImages = this.job_details.employeer.gallery_images;
      this.galleryImages = this.job_details.employer_gallery;
      this.currentDataIndex = index;
      //console.log(this.job_details);
      this.similarJobs(this.job_details._id);
    } 
  }

  getNextData(){
    let index = this.currentDataIndex +1;
    if(index <= this.jobsData.length){
      this.job_details =this.jobsData[index];
      //this.galleryImages = this.job_details.employeer.gallery_images;
      this.galleryImages = this.job_details.employer_gallery;
      this.currentDataIndex = index;
      this.similarJobs(this.job_details._id);
    }
  }

  open(index: number){
  	//console.log(index);
  	this._lightbox.open(this.allImage, index);
  }

  close(){
  	this._lightbox.close();
  }

  applyJob(job_id, job_type, jb_post_type, jb_data){
    this.jobPostType = jb_post_type;
    if(this.isloggedIn && job_type =='apply-job'){
      if(jb_data.condition_type ==1){
        if(jb_data.conditions.gender && this.job_details.candidate_type =='2' && this.logingUser.user_detail.gender == 2){
          this.alertMessage('This job only for male apply next job');
          return;
        }
        if(jb_data.conditions.gender && this.job_details.candidate_type =='3' && this.logingUser.user_detail.gender == 1){
          this.alertMessage('This job only for female apply next job');
          return;
        }

        let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
        if(jb_data.conditions.industry &&  !this.job_details.job_title.includes(this.logingUser.user_detail.profile_title) && !this.job_details.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(jb_data.conditions.expeiance && this.loginEmpProfile.profile.total_experience_yr_value == 0 && this.job_details.min_experiance.value >0){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(jb_data.conditions.expeiance && this.job_details.min_experiance && this.loginEmpProfile.profile.total_experience_yr_value < this.job_details.min_experiance.value){
          this.alertMessage('This job post work experience not matching with your profile  apply next job');
          return;
        }

        if(jb_data.conditions.location && this.job_details.employement_for.name =='Domestic Jobs' && this.job_details.country_name != this.loginEmpProfile.profile.country.name){
          this.alertMessage('This job only for local candidate apply next job');
          return;
        }

        if(jb_data.conditions.salary){
          if(jb_data.min_monthly_salary && !jb_data.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary < jb_data.min_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(!jb_data.min_monthly_salary && jb_data.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > jb_data.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(jb_data.min_monthly_salary && jb_data.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > jb_data.min_monthly_salary && this.loginEmpProfile.employee_pref.min_salary < jb_data.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }
        }
        if(jb_data.conditions.education && jb_data.qualifications_name.indexOf('Any Course') == -1){
           let qualificationIds = jb_data.qualifications
           let empQualificationIds = !_.isEmpty(this.loginEmpProfile.employee_education) ? this.loginEmpProfile.employee_education.map(obj=>obj.degree_id) :[]
           if(empQualificationIds.length === 0){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           }else if(!_.isEmpty(qualificationIds) && _.size(qualificationIds.filter(obj=> empQualificationIds.indexOf(obj)!= -1)) ===0 ){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           } 
        }
      }else if(jb_data.condition_type ==3){
        $('#question_modal_open').click();
        return
      }

    }

    if(!this.isloggedIn){
      this.commonService.callApplyCheckLogin({jb_id: job_id, jb_type: job_type,jb_post_type:jb_post_type, jb_data: jb_data});
    }else{
      if(jb_post_type =='local-walking'){
        this.router.navigate(['apply-hiring'],{queryParams:{walking_id:job_id}});
                
      }else if(jb_post_type =='international-walking'){
        this.router.navigate(['apply-hiring-international'],{queryParams:{walking_id:job_id}}); 
      }
      else if(jb_post_type =='international-job'){
         if(job_type =='saved-job'){
          this.commonService.create('/api/employer/international-jobs/saved', {job_id:job_id})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
        }else{
          this.commonService.create('/api/employer/international-jobs/applied', {job_id:job_id})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
                this.backPreviuosUrl();
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
        }       
      }
      else{
        if(job_type =='saved-job'){
          this.commonService.create('/api/employee/job-search/save-job', {job_id:job_id})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
        }else{
          this.commonService.create('/api/employee/job-search', {job_id:job_id})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
                this.backPreviuosUrl();
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
        }

      }

    }   
  }

  applyQuestionCondWise(job_id,jb_post_type){
    let questtions = Object.entries(this.questionAnswers)
    let finalQuestionsAns = [];
    if(questtions.length !== this.job_details.conditions.questions.length){
        this.messageService.error('Please give answer all questions');
        return
    }
    questtions.forEach((question, index)=>{
        finalQuestionsAns = [...finalQuestionsAns,{'qustion':question[0].split('_')[1],'answer':question[1]}]        
    })

      if(jb_post_type =='local-walking'){
          this.router.navigate(['apply-hiring'],{queryParams:{walking_id:job_id,job_questions:JSON.stringify(finalQuestionsAns)}});
                
      }else if(jb_post_type =='international-walking'){
          this.router.navigate(['apply-hiring-international'],{queryParams:{walking_id:job_id,job_questions:JSON.stringify(finalQuestionsAns)}}); 
      }else if(jb_post_type =='international-job'){
          this.commonService.create('/api/employer/international-jobs/applied', {job_id:job_id,job_questions:Object.assign({}, finalQuestionsAns)})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
                this.backPreviuosUrl();
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
      }else{
          this.commonService.create('/api/employee/job-search', {job_id:job_id,job_questions:Object.assign({}, finalQuestionsAns)})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
                this.backPreviuosUrl();
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});        
      }
  }

  submitCreateAlert(job_title,job_location){
    
    if(this.email_for_alert != ''){
      this.commonService.create('/api/create-job-alert', {email : this.email_for_alert,job_title:job_title,job_location:job_location})
          .subscribe((data)=>{
            if(data.status ==200){
                this.messageService.success("Successfull Create Alert");
                this.email_for_alert ='';
              }
          }, (error)=>{
            this.showloader = false;
          });
    }
  }

  similarJobs(job_id){
    this.count_similar_job_show =4;
    this.commonService.getAll('/api/similar-jobs/' + job_id+'/'+this.count_similar_job_show)
      .subscribe((data)=>{
        this.similar_jobs = data.similar_jobs;
        this.similar_job_count = data.similar_job_count;
        
        //console.log(this.similar_job_count);
        
      }, (error)=>{});
  }

  addMoreSimilarJobs(job_id){
    this.count_similar_job_show =this.count_similar_job_show + 4;
    this.commonService.getAll('/api/similar-jobs/' + job_id+'/'+this.count_similar_job_show)
      .subscribe((data)=>{
        this.similar_jobs = data.similar_jobs;
        
        //console.log(this.similar_jobs);
        
      }, (error)=>{});
  }

  getDegreeDetail(qualification, stream, type){
    let returnData='';
    if(type =='under-graduate'){
      let data = this.underGraduateData.filter(obj=>qualification.find(ob=> ob == obj._id));
      data.map(degree=>{
        if(degree.child && degree.child.length > 0){      
          let hasSub = degree.child.filter(obj=>stream.find(ob=>ob ==  obj._id));
          if(hasSub){
            returnData = degree.name + '(' + hasSub.map(obj=>obj.name) +')';
          }else{
            returnData = degree.name;
          }
        } else{
          returnData = degree.name
        }
      });
    }else if(type== 'post-graduate'){
       let data = this.postGraduateData.filter(obj=>qualification.find(ob=> ob == obj._id));
        data.map(degree=>{
          if(degree.child && degree.child.length > 0){      
            let hasSub = degree.child.filter(obj=>stream.find(ob=>ob ==  obj._id));
            if(hasSub){
              returnData = degree.name + '(' + hasSub.map(obj=>obj.name) +')';
            }else{
              returnData = degree.name;
            }
          } else{
            returnData = degree.name
          }
        });
    }
    return returnData;
  }

  getSpaceValue(data){
    let value ='';
    if(Array.isArray(data)){
      value = data.toString().replace(/,/g, ', ');
    }else{
      return  data.replace(/,/g, ', ');
    }

  }

  backPreviuosUrl(){
    //alert(this.route.snapshot.queryParams['backUrl'])
    if(this.route.snapshot.queryParams && this.route.snapshot.queryParams['backUrl']){
      this.router.navigate([`/${this.route.snapshot.queryParams['backUrl']}`]);
    }else{
      this.router.navigate(['/search-job-list'], { queryParams: this.prevQueryParams });
      
    }
  }

  alertMessage(text){
    Swal.fire({
      icon: 'error',
      title: '',
      html: `<strong>${text}</strong>`,
    })
   }

   checkQuestionMark(data){
       if(data.slice(-15).includes('?')){
         return data
       }else{
         return data + ' ?'
       }
   }


}
