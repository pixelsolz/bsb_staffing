import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';

@Component({
  selector: 'app-applied-international-jobs',
  templateUrl: './applied-international-jobs.component.html',
  styleUrls: ['./applied-international-jobs.component.css']
})
export class AppliedInternationalJobsComponent implements OnInit {

  appliedJobs : any=[];
  applicationPhases: any=[];
  showPhase: boolean;
  showPhaseIndex: number;
    globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[],
    miles_jobs:[],
    alert_jobs:[]
  }
  total_count: number =0;
  total_pages: number=0;
  current_page: number=1

  constructor(private commonService: CommonService, private messageservice: MessageService) {
    this.showPhase = false;
    this.showPhaseIndex =0;
  }


  ngOnInit() {
    window.scrollTo(0, 0);
    this.asyncInit();
  }

    asyncInit(){
    this.commonService.getAll('/api/employee/international/applied-jobs')
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.appliedJobs = data.data.applied_jobs;
        this.applicationPhases = data.data.application_phases;
        this.total_count = data.data.total_count;
        this.total_pages = Math.ceil(data.data.total_count/10);
        this.globalJobSave.internationJobs = this.appliedJobs.map(obj=>obj.job);
        this.commonService.callJobsData(this.globalJobSave);
        console.log(this.appliedJobs);
      }, (error)=>{});
  }

  loadMore(){
    this.current_page+=1;
    this.commonService.getAll(`/api/employee/international/applied-jobs?skip=${this.current_page}`)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.appliedJobs = [...this.appliedJobs,...data.data.applied_jobs];
        this.total_count = data.data.total_count;
        this.total_pages = Math.ceil(data.data.total_count/6);
         this.globalJobSave.internationJobs = this.appliedJobs.map(obj=>obj.job);
        this.commonService.callJobsData(this.globalJobSave);
      }, (error)=>{});    

  }

  setPhase(index){
    if(this.showPhaseIndex === index){
      this.showPhase =!this.showPhase;
    }else{
      if(this.showPhase ==false){
        this.showPhaseIndex = index;
        this.showPhase = true;
      }else{
      this.showPhaseIndex = index;        
      }

    }
  }


}
