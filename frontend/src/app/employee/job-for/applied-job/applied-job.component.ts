import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-applied-job',
  templateUrl: './applied-job.component.html',
  styleUrls: ['./applied-job.component.css']
})
export class AppliedJobComponent implements OnInit {

  appliedJobs : any=[];
  applicationPhases: any=[];
  showPhase: boolean;
  showPhaseIndex: number;
  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[],
    miles_jobs:[],
    alert_jobs:[]
  }
  title: string ='Applied Jobs';
  total_count: number =0;
  total_pages: number=0;
  current_page: number=1
  constructor(private commonService: CommonService, private messageservice: MessageService,private router: Router,private route: ActivatedRoute,) {
  	this.showPhase = false;
  	this.showPhaseIndex =0;

    if(this.router.url.includes('application-status')){
      this.title = 'Application Status'
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  	this.asyncInit();
  }

  asyncInit(){
  	this.commonService.getAll('/api/employee/get-applied-jobs')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.appliedJobs = data.data.applied_jobs;
        this.total_count = data.data.total_count;
        this.total_pages = Math.ceil(data.data.total_count/10);
  			this.applicationPhases = data.data.application_phases;
        this.globalJobSave.jobs = this.appliedJobs.map(obj=>obj.job);
        this.commonService.callJobsData(this.globalJobSave);
  		}, (error)=>{});
  }

  setPhase(event,index){
    event.preventDefault();
    if(this.showPhaseIndex === index){
      this.showPhase =!this.showPhase;
    }else{
      if(this.showPhase ==false){
        this.showPhaseIndex = index;
        this.showPhase = true;
      }else{
      this.showPhaseIndex = index;        
      }

    }
  }

  loadMore(){
    this.current_page+=1;
    this.commonService.getAll(`/api/employee/get-applied-jobs?skip=${this.current_page}`)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.appliedJobs = [...this.appliedJobs,...data.data.applied_jobs];
        this.total_count = data.data.total_count;
        this.total_pages = Math.ceil(data.data.total_count/10);
        this.applicationPhases = data.data.application_phases;
        this.globalJobSave.jobs = this.appliedJobs.map(obj=>obj.job);
        this.commonService.callJobsData(this.globalJobSave);
      }, (error)=>{});    

  }

}
