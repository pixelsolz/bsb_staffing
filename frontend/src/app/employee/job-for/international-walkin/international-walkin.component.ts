import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute} from '@angular/router';
import {trigger,state,style,animate,transition} from '@angular/animations';

@Component({
  selector: 'app-international-walkin',
  templateUrl: './international-walkin.component.html',
  styleUrls: ['./international-walkin.component.css'],
      animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(100%)'}),
        animate('700ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('700ms ease-in', style({transform: 'translateY(100%)'}))
      ])
    ]),
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    ),
    trigger('fade',[
      state('void', style({opacity: 0})),
      transition(':enter, :leave', [
        animate(700)
      ])
    ])

  ]
})
export class InternationalWalkinComponent implements OnInit {

  internationalwalking_data: any=[];
  bookedSlot: any=[];
  showSlot: boolean;
  setIndex: number;
  hideDiv: boolean;
  dateBookedSlot: any=[];
  bookedDateActive: number;
  slot_book_form: any=[];
  active_date: string;
  selected_slot_time: string;
  total_internationalwalkin:number;
  count_international_walkin_show:number;

  constructor(private commonService: CommonService, private messageservice: MessageService, private activeRoute: ActivatedRoute) { 
  	this.showSlot = false;
  	this.bookedDateActive =0;
  }

  ngOnInit() {
    window.scrollTo(0, 0); 
  	this.asyncInit();
  }

  asyncInit(){
    let url = '';
    if(this.activeRoute.snapshot.queryParams['walking_id']){
      let walking_id = this.activeRoute.snapshot.queryParams['walking_id'];
      url = '/api/employee/get-inter-waliking/jobs?walking_id=' + walking_id;
    }else{
      url = '/api/employee/get-inter-waliking/jobs';
    }
  	this.commonService.getAll(url)
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.internationalwalking_data = data.data;
        this.total_internationalwalkin = data.total_internationalwalkin;
        this.count_international_walkin_show =12;
  			this.bookedSlot = data.booked_slot;
  		}, (error)=>{});
  }

  checkSlotBooked(id){
  	let checkedData = this.internationalwalking_data.find(data=>data._id == id);
    console.log(checkedData);
  	let booked = this.bookedSlot.filter(data=>{return data.interview_id == id && data.booked_date == checkedData.interview_date.split(',')[0]});
  	this.dateBookedSlot = booked.map(obj=>obj.booked_time);	
  	this.active_date = checkedData.interview_date.split(',')[0];
  	this.getActiveSlot(id, checkedData.interview_date.split(',')[0]);
  }

  changeSlotBooked(id, date){
  	let booked = this.bookedSlot.filter(data=>{return data.interview_id == id && data.booked_date == date});
  	this.dateBookedSlot = booked.map(obj=>obj.booked_time);	
  	this.active_date = date;
  	this.getActiveSlot(id, date);
  }
  
  setSlotBook(id, time){
  	if(this.slot_book_form.length > 0 ){
  		let index = this.slot_book_form.findIndex(obj=>obj.interview_id ==id);
  			if(index != -1){
  				let data ={interview_id:id, booked_date:this.active_date,booked_time:time};
  				this.slot_book_form[index] = data;
  			}else{
  				let data ={interview_id:id, booked_date:this.active_date,booked_time:time};
  				this.slot_book_form.push(data);	
  			}  		
  	}else if(this.slot_book_form.length ==0){
  		let data ={interview_id:id, booked_date:this.active_date,booked_time:time};
  		this.slot_book_form.push(data);
  	}

  }

  getActiveSlot(id, date){
  	this.selected_slot_time ='';
  	let data = this.slot_book_form.find(obj=>obj.interview_id == id && obj.booked_date ==date);
  	if(data){
  		this.selected_slot_time = data.booked_time;
  	}	
  }

  submitSelectedSlot(id){
  	let data = this.slot_book_form.find(obj=>obj.interview_id == id );
  	if(!data){
  		this.messageservice.error('Please select slot');
  		return false;
  	}
  	this.commonService.create('/api/employee/store-slot-booking', data)
  		.subscribe((data)=>{
  			if(data.status ==200){
  				let index = this.internationalwalking_data.findIndex(obj=>obj.id ==id);
  				this.internationalwalking_data.splice(index,1);
  				this.messageservice.success(data.status_text);
  			}
  		},(error)=>{});
  }

  addMoreInternationalWalking(){
    let limit = 12 + this.internationalwalking_data.length;
    this.commonService.getAll('/api/employee/get-inter-waliking/jobs?limit=' + limit)
      .subscribe((data)=>{
        this.internationalwalking_data = data.data;
        this.bookedSlot = data.booked_slot;
        this.count_international_walkin_show = this.count_international_walkin_show + 12;
        //console.log(this.localwalking_data);
      }, (error)=>{});
  }

}
