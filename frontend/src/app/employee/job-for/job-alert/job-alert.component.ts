import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-job-alert',
  templateUrl: './job-alert.component.html',
  styleUrls: ['./job-alert.component.css']
})
export class JobAlertComponent implements OnInit {

  exactJobs: any=[];
  likeJobs: any=[];
  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[],
    miles_jobs:[],
    alert_jobs:[]
  }
  constructor(private commonService: CommonService, private messageservice: MessageService) { }

  ngOnInit() {
    window.scrollTo(0, 0); 
  	this.asyncInit();
  }

  asyncInit(){
  	this.commonService.getAll('/api/employee/get-job-alert?type=my-alert')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.exactJobs = data.data.job_data;
        this.globalJobSave.jobs = this.exactJobs;
        this.commonService.callJobsData(this.globalJobSave);
  			//this.likeJobs = data.data.like_job_data;
  		}, (error)=>{});
  }

  saveJob(job_id, type, index){
  	this.commonService.create('/api/employee/job-search/save-job', {job_id:job_id})
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.messageservice.success(data.status_text);
          if(type =='exact_job'){
            this.exactJobs.splice(index,1);
          }else if(type =='like_job'){
            this.likeJobs.splice(index, 1);
          }     				
  			}
  		}, (error)=>{});
  }

  applyJob(job_id, type, index){
  	this.commonService.create('/api/employee/job-search', {job_id:job_id})
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.messageservice.success(data.status_text); 		
  				if(type =='exact_job'){
  					this.exactJobs.splice(index,1);
  				}else if(type =='like_job'){
  					this.likeJobs.splice(index, 1);
  				}		
  			}else if(data.status ==422){
          this.messageservice.error(data.status_text);
        }
  		}, (error)=>{});
  }

}
