import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router} from '@angular/router';
declare var Swal: any;
declare var $;
import * as _ from 'lodash';

@Component({
  selector: 'app-recommend-alert',
  templateUrl: './recommend-alert.component.html',
  styleUrls: ['./recommend-alert.component.css']
})
export class RecommendAlertComponent implements OnInit {
  logingUser = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
  loginEmpProfile:any
  exactJobs: any=[];
  likeJobs: any=[];
  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[],
    miles_jobs:[],
    alert_jobs:[]
  }
  selected_job: any={}
  selected_type:string;
  selected_index: number;
  questionAnswers:any={};
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Answer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };

  constructor(private commonService: CommonService, private messageservice: MessageService) { }

  ngOnInit() {
    window.scrollTo(0, 0); 
  	this.asyncInit();
  }

   asyncInit(){
      this.commonService.getAll('/api/employee-profile').subscribe(res=>{
        if (res.status == 200) {
          this.loginEmpProfile = res.data;
        }
      });

  	this.commonService.getAll('/api/employee/get-job-alert?type=recommend')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.exactJobs = data.data.job_data;
        this.globalJobSave.jobs = this.exactJobs;
        this.commonService.callJobsData(this.globalJobSave);
  			//this.likeJobs = data.data.like_job_data;
  		}, (error)=>{});
  }

  saveJob(job_id, type, index){

  	this.commonService.create('/api/employee/job-search/save-job', {job_id:job_id})
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.messageservice.success(data.status_text);
          if(type =='exact_job'){
            this.exactJobs.splice(index,1);
          }else if(type =='like_job'){
            this.likeJobs.splice(index, 1);
          }     				
  			}
  		}, (error)=>{});
  }

  applyJob(job_id, type, index, job){
      this.selected_job = job;
      this.selected_type = type;
      this.selected_index = index;
      if(job.condition_type ==1){
        if(job.conditions.gender && job.candidate_type =='2' && this.logingUser.user_detail.gender == 2){
          this.alertMessage('This job only for male apply next job');
          return;
        }
        if(job.conditions.gender && job.candidate_type =='3' && this.logingUser.user_detail.gender == 1){
          this.alertMessage('This job only for female apply next job');
          return;
        }

        let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
        if(job.conditions.industry &&  !job.job_title.includes(this.logingUser.user_detail.profile_title) && !job.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && this.loginEmpProfile.profile.total_experience_yr_value == 0 && job.min_experiance.value >0){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && job.min_experiance && this.loginEmpProfile.profile.total_experience_yr_value < job.min_experiance.value){
          this.alertMessage('This job post work experience not matching with your profile  apply next job');
          return;
        }

        if(job.conditions.location && job.employement_for.name =='Domestic Jobs' && job.country_name != this.loginEmpProfile.profile.country.name){
          this.alertMessage('This job only for local candidate apply next job');
          return;
        }

        if(job.conditions.salary){
          if(job.min_monthly_salary && !job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary < job.min_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(!job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.min_monthly_salary && this.loginEmpProfile.employee_pref.min_salary < job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }
        }
        if(job.conditions.education  && job.qualifications_name.indexOf('Any Course') == -1){
           let qualificationIds = job.qualifications
           let empQualificationIds = !_.isEmpty(this.loginEmpProfile.employee_education) ? this.loginEmpProfile.employee_education.map(obj=>obj.degree_id) :[]
           if(empQualificationIds.length === 0){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           }else if(!_.isEmpty(qualificationIds) && _.size(qualificationIds.filter(obj=> empQualificationIds.indexOf(obj)!= -1)) ===0 ){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           } 
        }
      }else if(job.condition_type ==3){
        $('#question_modal_open').click();
        return
      }

  	this.commonService.create('/api/employee/job-search', {job_id:job_id})
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.messageservice.success(data.status_text); 		
  				if(type =='exact_job'){
  					this.exactJobs.splice(index,1);
  				}else if(type =='like_job'){
  					this.likeJobs.splice(index, 1);
  				}		
  			}else if(data.status ==422){
          this.messageservice.error(data.status_text);
        }
  		}, (error)=>{});
  }

  applyQuestionCondWise(job_id){
    let questtions = Object.entries(this.questionAnswers)
    let finalQuestionsAns = [];
    if(questtions.length !== this.selected_job.conditions.questions.length){
        this.messageservice.error('Please give answer all questions');
        return
    }
    questtions.forEach((question, index)=>{
        finalQuestionsAns = [...finalQuestionsAns,{'qustion':question[0].split('_')[1],'answer':question[1]}]        
    })

    this.commonService.create('/api/employee/job-search', {job_id:job_id,job_questions:Object.assign({}, finalQuestionsAns)})
      .subscribe((data)=>{
        if(data.status ==200){
          this.messageservice.success(data.status_text);     
          if(this.selected_type =='exact_job'){
            this.exactJobs.splice(this.selected_index,1);
          }else if(this.selected_type =='like_job'){
            this.likeJobs.splice(this.selected_index, 1);
          }    
        }else if(data.status ==422){
          this.messageservice.error(data.status_text);
        }
      }, (error)=>{});

  }



  alertMessage(text){
    Swal.fire({
      icon: 'error',
      title: '',
      html: `<strong>${text}</strong>`,
    })
   }

   checkQuestionMark(data){
       if(data.slice(-15).includes('?')){
         return data
       }else{
         return data + ' ?'
       }
   }


}
