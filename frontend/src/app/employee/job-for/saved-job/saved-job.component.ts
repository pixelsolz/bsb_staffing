import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router} from '@angular/router';
declare var Swal: any;
declare var $;
import * as _ from 'lodash';

@Component({
  selector: 'app-saved-job',
  templateUrl: './saved-job.component.html',
  styleUrls: ['./saved-job.component.css']
})
export class SavedJobComponent implements OnInit {
  logingUser = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
  loginEmpProfile:any
  savedJobs: any=[];
  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[],
    miles_jobs:[],
    alert_jobs:[]
  }
  selected_job: any={}
  selected_save_id:string;
  questionAnswers:any={};
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Answer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  constructor(private commonService: CommonService, private messageservice: MessageService, private router: Router) { 

  }

  ngOnInit() {
    window.scrollTo(0, 0); 
  	this.asyncInit();
  }

  asyncInit(){
    this.commonService.getAll('/api/employee-profile').subscribe(res=>{
        if (res.status == 200) {
          this.loginEmpProfile = res.data;
        }
      });
  	this.commonService.getAll('/api/employee/get-saved-jobs')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.savedJobs = data.data.saved_jobs.length > 0? data.data.saved_jobs.filter(obj=>{if(obj.job)return obj }):[];
        this.globalJobSave.jobs = this.savedJobs.map(obj=>obj.job);
        this.commonService.callJobsData(this.globalJobSave);
  		}, (error)=>{});
  }

  appliedJob(job_id, saved_id, job){
    this.selected_job = job;
    this.selected_save_id = saved_id;
      if(job.condition_type ==1){
        if(job.conditions.gender && job.candidate_type =='2' && this.logingUser.user_detail.gender == 2){
          this.alertMessage('This job only for male apply next job');
          return;
        }
        if(job.conditions.gender && job.candidate_type =='3' && this.logingUser.user_detail.gender == 1){
          this.alertMessage('This job only for female apply next job');
          return;
        }

        let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
        if(job.conditions.industry &&  !job.job_title.includes(this.logingUser.user_detail.profile_title) && !job.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && this.loginEmpProfile.profile.total_experience_yr_value == 0 && job.min_experiance.value >0){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && job.min_experiance && this.loginEmpProfile.profile.total_experience_yr_value < job.min_experiance.value){
          this.alertMessage('This job post work experience not matching with your profile  apply next job');
          return;
        }

        if(job.conditions.location && job.employement_for.name =='Domestic Jobs' && job.country_name != this.loginEmpProfile.profile.country.name){
          this.alertMessage('This job only for local candidate apply next job');
          return;
        }

        if(job.conditions.salary){
          if(job.min_monthly_salary && !job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary < job.min_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(!job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.min_monthly_salary && this.loginEmpProfile.employee_pref.min_salary < job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }
        }
        if(job.conditions.education && job.qualifications_name.indexOf('Any Course') == -1){
           let qualificationIds = job.qualifications
           let empQualificationIds = !_.isEmpty(this.loginEmpProfile.employee_education) ? this.loginEmpProfile.employee_education.map(obj=>obj.degree_id) :[]
           if(empQualificationIds.length === 0){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           }else if(!_.isEmpty(qualificationIds) && _.size(qualificationIds.filter(obj=> empQualificationIds.indexOf(obj)!= -1)) ===0 ){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           } 
        }
      }else if(job.condition_type ==3){
        $('#question_modal_open').click();
        return
      }


  	this.commonService.create('/api/employee/saved-job/applied', {job_id:job_id, saved_id: saved_id})
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.messageservice.success(data.status_text);
  				this.router.navigate(['applied-jobs']);
  			}
  		}, (error)=>{});
  }

  applyQuestionCondWise(job_id){
    let questtions = Object.entries(this.questionAnswers)
    let finalQuestionsAns = [];
    if(questtions.length !== this.selected_job.conditions.questions.length){
        this.messageservice.error('Please give answer all questions');
        return
    }
    questtions.forEach((question, index)=>{
        finalQuestionsAns = [...finalQuestionsAns,{'qustion':question[0].split('_')[1],'answer':question[1]}]        
    })

    this.commonService.create('/api/employee/saved-job/applied', {job_id:job_id, saved_id: this.selected_save_id,job_questions:Object.assign({}, finalQuestionsAns)})
      .subscribe((data)=>{
        if(data.status ==200){
          this.messageservice.success(data.status_text);
          this.router.navigate(['applied-jobs']);
        }
      }, (error)=>{});

  }

  removeSaveJob(job_id, savedId){
    this.commonService.getAll('/api/employee/remove/save-job/' + job_id)
        .subscribe(data=>{
          if(data.status ==200){
            let index= this.savedJobs.findIndex(obj=>obj._id == savedId);
            this.savedJobs.splice(index,1);
            this.messageservice.success('Successfully unsaved job');
          }
        },error=>{});
  }

  alertMessage(text){
    Swal.fire({
      icon: 'error',
      title: '',
      html: `<strong>${text}</strong>`,
    })
   }

   checkQuestionMark(data){
       if(data.slice(-15).includes('?')){
         return data
       }else{
         return data + ' ?'
       }
   }

}
