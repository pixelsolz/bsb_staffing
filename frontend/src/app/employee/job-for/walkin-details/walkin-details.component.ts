import { Component, OnInit } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';
import {Global} from '../../../global';
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from '../../../services/common.service';
import { MessageService } from '../../../services/message.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {trigger,state,style,animate,transition} from '@angular/animations';
declare var $;
declare var Swal: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-walkin-details',
  templateUrl: './walkin-details.component.html',
  styleUrls: ['./walkin-details.component.css']
})
export class WalkinDetailsComponent implements OnInit {
  logingUser = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
  loginEmpProfile: any;
  shareUrl:string;
	allImage : any;
  job_id :string;
  job_details:any;
  galleryImages:any=[];
  companyImages:any=[];
  imgUrl :any;
  isloggedIn: boolean;
  job_type: string;
  isloading: boolean =false;
	isloaded:boolean=false;
  showloader:boolean;
  job_post_type: string;
  showSlot: boolean;
  hideDiv: boolean;
  bookedDateActive: number;
  dateBookedSlot: any=[];
  selected_slot_time: string;
  slot_book_form: any=[];
  bookedSlot: any=[];
  localwalking_data: any=[];
  active_date: string;
  user:any;
  applied:boolean=false;
  questionAnswers:any={};    
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Answer',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  constructor(private _lightbox: Lightbox,private global: Global,private route: ActivatedRoute,private commonService: CommonService, private messageService: MessageService, private router: Router,private fb:FormBuilder) { 
	  this.shareUrl = this.global.baseUrl + 'walkin-job-details/' +this.route.snapshot.params.job_type + '/' + this.route.snapshot.params.id;
    this.job_id = this.route.snapshot.params.id;
	  if(this.route.snapshot.params.job_type == 'local'){
	  	this.job_type = 'Local';
	  }else{
	  	this.job_type = 'International';
	  }
	  //this.job_type = this.route.snapshot.params.job_type;

	  this.imgUrl = global.imageUrl;
	  this.showSlot = false;
	  this.bookedDateActive =0;
	  this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
  }

  ngOnInit() {
    window.scrollTo(0, 0); 
      this.commonService.getAll('/api/employee-profile').subscribe(res=>{
        if (res.status == 200) {
          this.loginEmpProfile = res.data;
        }
      });
    
  	this.commonService.getAll('/api/employee/walkin-interview-details?interview_type='+this.job_type+'&id='+this.job_id)
     .subscribe((data)=>{ 
     	this.commonService.callFooterMenu(1);
     	this.localwalking_data = data.walking_interviews;
      this.job_details = data.walking_interviews[0];
      this.bookedSlot = data.booked_slot;
      let booked = this.bookedSlot.filter(data=>{return data.interview_id == this.job_id && data.employee._id == this.user.id});   
      if(booked.length){
      	this.applied = true;
      }

     },(error)=>{});
  }

  checkSlotBooked(id){

      if(this.job_details.condition_type ==1){
        if(this.job_details.conditions.gender && this.job_details.candidate_type =='2' && this.logingUser.user_detail.gender == 2){
          this.alertMessage('This job only for male apply next job');
          return;
        }
        if(this.job_details.conditions.gender && this.job_details.candidate_type =='3' && this.logingUser.user_detail.gender == 1){
          this.alertMessage('This job only for female apply next job');
          return;
        }

        let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
        if(this.job_details.conditions.industry &&  !this.job_details.job_title.includes(this.logingUser.user_detail.profile_title) && !this.job_details.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(this.job_details.conditions.expeiance && this.loginEmpProfile.profile.total_experience_yr_value == 0 && this.job_details.min_experiance.value >0){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(this.job_details.conditions.expeiance && this.job_details.min_experiance && this.loginEmpProfile.profile.total_experience_yr_value < this.job_details.min_experiance.value){
          this.alertMessage('This job post work experience not matching with your profile  apply next job');
          return;
        }

        if(this.job_details.conditions.location && this.job_details.employement_for.name =='Domestic Jobs' && this.job_details.country_name != this.loginEmpProfile.profile.country.name){
          this.alertMessage('This job only for local candidate apply next job');
          return;
        }

        if(this.job_details.conditions.salary){
          if(this.job_details.min_monthly_salary && !this.job_details.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary < this.job_details.min_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(!this.job_details.min_monthly_salary && this.job_details.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > this.job_details.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(this.job_details.min_monthly_salary && this.job_details.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > this.job_details.min_monthly_salary && this.loginEmpProfile.employee_pref.min_salary < this.job_details.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }
        }
        if(this.job_details.conditions.education && this.job_details.qualifications_name.indexOf('Any Course') == -1){
           let qualificationIds = this.job_details.qualifications
           let empQualificationIds = !_.isEmpty(this.loginEmpProfile.employee_education) ? this.loginEmpProfile.employee_education.map(obj=>obj.degree_id) :[]
           if(empQualificationIds.length === 0){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           }else if(!_.isEmpty(qualificationIds) && _.size(qualificationIds.filter(obj=> empQualificationIds.indexOf(obj)!= -1)) ===0 ){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           } 
        }
      }else if(this.job_details.condition_type ==3){
        $('#question_modal_open').click();
        return
      }

  	$('#booked_time_slot').click();
  	let checkedData = this.localwalking_data.find(data=>data._id == id);
  	let booked = this.bookedSlot.filter(data=>{return data.interview_id == id && data.booked_date == checkedData.interview_date.split(',')[0]});
    this.dateBookedSlot = booked.map(obj=>obj.booked_time);	
  	this.active_date = checkedData.interview_date.split(',')[0];
  	this.getActiveSlot(id, checkedData.interview_date.split(',')[0]);
  }

 applyQuestionCondWise(id){
   let questtions = Object.entries(this.questionAnswers)
    if(questtions.length !== this.job_details.conditions.questions.length){
        this.messageService.error('Please give answer all questions');
        return
    }
    $('#question_modal_close').click();
    $('#booked_time_slot').click();
    let checkedData = this.localwalking_data.find(data=>data._id == id);
    let booked = this.bookedSlot.filter(data=>{return data.interview_id == id && data.booked_date == checkedData.interview_date.split(',')[0]});
    this.dateBookedSlot = booked.map(obj=>obj.booked_time);  
    this.active_date = checkedData.interview_date.split(',')[0];
    this.getActiveSlot(id, checkedData.interview_date.split(',')[0]);  
 }

  changeSlotBooked(id, date){
  	let booked = this.bookedSlot.filter(data=>{return data.interview_id == id && data.booked_date == date});
  	this.dateBookedSlot = booked.map(obj=>obj.booked_time);	
  	this.active_date = date;
  	this.getActiveSlot(id, date);
  }

  setSlotBook(id, time){
  	if(this.slot_book_form.length > 0 ){
  		let index = this.slot_book_form.findIndex(obj=>obj.interview_id ==id);
  			if(index != -1){
  				let data ={interview_id:id, booked_date:this.active_date,booked_time:time};
  				this.slot_book_form[index] = data;
  			}else{
  				let data ={interview_id:id, booked_date:this.active_date,booked_time:time};
  				this.slot_book_form.push(data);	
  			}  		
  	}else if(this.slot_book_form.length ==0){
  		let data ={interview_id:id, booked_date:this.active_date,booked_time:time};
  		this.slot_book_form.push(data);
  	}

  }

  getActiveSlot(id, date){
  	this.selected_slot_time ='';
  	let data = this.slot_book_form.find(obj=>obj.interview_id == id && obj.booked_date ==date);
  	if(data){
  		this.selected_slot_time = data.booked_time;
  	}	
  }

  submitSelectedSlot(id){
  	let data = this.slot_book_form.find(obj=>obj.interview_id == id );
  	if(!data){
  		this.messageService.error('Please select slot');
  		return false;
  	}
   let questtions = Object.entries(this.questionAnswers)
   let finalQuestionsAns = [];
    questtions.forEach((question, index)=>{
        finalQuestionsAns = [...finalQuestionsAns,{'qustion':question[0].split('_')[1],'answer':question[1]}]        
    })

  	this.commonService.create('/api/employee/store-slot-booking', {data:data, job_questions:Object.assign({}, finalQuestionsAns) })
  		.subscribe((data)=>{
  			if(data.status ==200){
  				$('#close_booked_time_slot_model').click();
  				
  				let index = this.localwalking_data.findIndex(obj=>obj.id ==id);
  				//this.localwalking_data.splice(index,1);
  				this.messageService.success(data.status_text);
  				this.applied = true;
  			}
  		},(error)=>{});
  }

  alertMessage(text){
    Swal.fire({
      icon: 'error',
      title: '',
      html: `<strong>${text}</strong>`,
    })
   }

   checkQuestionMark(data){
       if(data.slice(-15).includes('?')){
         return data
       }else{
         return data + ' ?'
       }
   }
  

}
