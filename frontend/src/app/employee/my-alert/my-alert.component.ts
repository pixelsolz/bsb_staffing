import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var Swal: any;
declare var $: any;
@Component({
  selector: 'app-my-alert',
  templateUrl: './my-alert.component.html',
  styleUrls: ['./my-alert.component.css']
})
export class MyAlertComponent implements OnInit {

  myAlertForm: FormGroup;
  storeMultiSelect: any = {
    country_value: [],
    country_name: [],
    city_value: [],
    city_name: [],
    industries_value: [],
    industries_name: [],
    department_name: [],
    department_value: [],
  };
  skill_key: any=[];
  skillsData: any = [];
  countries: any = [];
  countryArray: any = [];
  cities: any = [];
  cityArray: any = [];
  departments: any = [];
  industries: any = [];
  countryDiv: boolean = false;
  cityDiv: boolean = false;
  departmentDiv: boolean = false;
  industriesDiv: boolean = false;
  myAlertData: any = [];
  currencyData: any = [];
  validation_errors: any;
  coutryWiseCity: any=[];
  constructor(private fb: FormBuilder, private commonService: CommonService,
    private messageService: MessageService, private activeRoute: ActivatedRoute) {
    this.asyncInit();
    window.scrollTo(0, 0);
  }

  ngOnInit() {
    this.myAlertForm = this.fb.group({
      id: '',
      keyword: [''],
      salary_type: [''],
      currency: [''],
      salary: [''],
      //experiance:[''],
      country_id: [''],
      city_id: [''],
      department_id: [''],
      industry_id: [''],
      email_id: [''],
      name_of_alert: ['']
    });

    this.myAlertForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        this.storeMultiSelect.city_name = [];
        this.storeMultiSelect.city_value = [];
        let countryCode = this.countries.find(obj=>obj._id == value);
        this.cityArray = {"":this.cities.filter(obj=>obj.country_code == countryCode.code)};
      }else{
        this.cityArray=[];
      }

    });
  }

  asyncInit() {
    this.commonService.getAll('/api/my-job-alert')
      .subscribe(data => {
        this.commonService.callFooterMenu(1);
        this.myAlertData = data.data.alerts;
        this.countries = data.data.countries;
        this.countryArray = data.data.countrie_array;
        this.cities = data.data.cities;
        //this.cityArray = data.data.cities_array;
        this.industries = data.data.industries;
        this.departments = data.data.all_department;
        this.currencyData = data.data.countries;
        this.skillsData = data.data.skills.map((obj) => { return { display: obj.skill, value: obj.skill } });
      }, error => { });
  }

  getOnchangeEvent(type) {
    if (type == 'country') {
      this.countryDiv = true;
    } else if (type == 'city') {
      this.cityDiv = true;
    } else if (type == 'industries') {
      this.industriesDiv = true;
    } else if (type == 'department') {
      this.departmentDiv = true;
    }

  }

  getChange(e) {
    console.log(e);
    if (e.event) {
      let index = e.storedValue.indexOf(e.value);
      if (index == -1) {
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    } else {
      let index = e.storedValue.indexOf(e.value);
      if (index != -1) {
        e.storedValue.splice(index, 1);
        e.storedName.splice(index, 1);
      }
    }
    if (e.field_name == 'country') {
      this.myAlertForm.patchValue({
        country_id: e.storedValue
      });
    } else if (e.field_name == 'city') {
      this.myAlertForm.patchValue({
        city_id: e.storedValue
      });
    } else if (e.field_name == 'industries') {
      this.myAlertForm.patchValue({
        industry_id: e.storedValue
      });
    } else if (e.field_name == 'department') {
      this.myAlertForm.patchValue({
        department_id: e.storedValue
      });
    }
  }

  checkmultiselect(e) {
    if (e.field_name == 'country') {
      this.countryDiv = e.status;
    } else if (e.field_name == 'city') {
      this.cityDiv = e.status;
    } else if (e.field_name == 'industries') {
      this.industriesDiv = e.status;
    } else if (e.field_name == 'department') {
      this.departmentDiv = e.status;
    }
  }

  multiselectAll(e) {
    //console.log(e.storedValue);
    let filed = e.field_name;
    if (e.event) {
      e.arrValue.forEach((value, i) => {
        let index = e.storedValue.indexOf(value);
        if (index == -1) {
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }
      });
    } else {
      e.arrValue.forEach(value => {
        let index = e.storedValue.indexOf(value);
        if (index != -1) {
          e.storedValue.splice(index, 1);
          e.storedName.splice(index, 1);
        }
      });
    }
    if (filed == 'country') {
      this.myAlertForm.patchValue({
        country_id: e.storedValue
      });
    } else if (filed == 'city') {
      this.myAlertForm.patchValue({
        city_id: e.storedValue
      });
    } else if (filed == 'department') {
      this.myAlertForm.patchValue({
        department_id: e.storedValue
      });
    } else if (filed == 'industries') {
      this.myAlertForm.patchValue({
        industry_id: e.storedValue
      });
    }

  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.myAlertForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "department":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.myAlertForm.patchValue({
        department_id: this.storeMultiSelect.department_value
        });
        break;
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.myAlertForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.myAlertForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
        break;     
      default:
        // code...
        break;
    }
  }


  createAlert() {
    //this.changeSubmitVal();
    this.validation_errors = '';
    let skills = this.skill_key.length?this.skill_key.map(obj => obj.value):'';
    skills ? this.myAlertForm.get('keyword').setValue(skills) : '';
    this.commonService.create('/api/my-job-alert', this.myAlertForm.value)
      .subscribe(data => {
        if (data.status == 200) {
          this.messageService.success(data.status_text);
          this.myAlertData.push(data.data);
          this.formReset();
        }
        if (data.status == 422) {
          console.log(data.error);
          this.setValue();
          this.validation_errors = data.error;
        }
      }, error => { });
  }

  showAlert(id) {
    $('html, body').animate({
      scrollTop: $('#scroll_set').offset().top
    }, 500);

    let data = this.myAlertData.find(obj => obj._id == id);

    this.storeMultiSelect.industries_value = data.industry_id;
    this.storeMultiSelect.industries_name = data.industry_name;
    this.storeMultiSelect.department_value = data.department_id;
    this.storeMultiSelect.department_name = data.department_name;
    this.storeMultiSelect.country_value = data.country_id;
    this.storeMultiSelect.country_name = data.country_name;
    this.skill_key = data.keyword.map((obj) => { return { value: obj, display: obj } });

    this.myAlertForm.patchValue({
      id: id,
      keyword: data.keyword,
      salary_type: data.salary_type,
      currency: data.currency,
      salary: data.salary,
      //experiance:data.experiance,
      country_id: data.country_id,
      city_id: data.city_id,
      department_id: data.department_id,
      industry_id: data.industry_id,
      email_id: data.email_id,
      name_of_alert: data.name_of_alert,
    });

    this.storeMultiSelect.city_value = data.city_id;
    this.storeMultiSelect.city_name = data.city_name;
    this.myAlertForm.controls['city_id'].setValue(this.storeMultiSelect.city_value);
  }

  updateAlert() {
    this.validation_errors = '';
    this.changeSubmitVal();
    let skills = this.skill_key.map(obj => obj.value);
    skills ? this.myAlertForm.get('keyword').setValue(skills) : '';
    let index = this.myAlertData.findIndex(obj => obj._id == this.myAlertForm.value.id);
    this.commonService.update('/api/my-job-alert', this.myAlertForm.value.id, this.myAlertForm.value)
      .subscribe(data => {
        if (data.status == 200) {
          this.myAlertData[index] = data.data;
          this.formReset();
          this.messageService.success(data.status_text);
        } if (data.status == 422) {
          this.setValue();
          this.validation_errors = data.error;
        }
      }, error => { });
  }

  deleteAlert(id) {
    let index = this.myAlertData.findIndex(obj => obj._id == this.myAlertForm.value.id);
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonService.delete('/api/my-job-alert', id)
          .subscribe((data) => {
            if (data.status == 200) {
              this.myAlertData.splice(index, 1);
              Swal.fire(
                'Deleted!',
                data.status_text,
                'success'
              )
            }
          }, (error) => { });

      }

    });

    this.commonService.delete('/api/my-job-alert', id)
      .subscribe(data => {
        if (data.status == 200) {
          this.myAlertData.splice(index, 1);
          this.messageService.success(data.status_text);
        }
      }, error => { });
  }


  changeSubmitVal() {
    this.myAlertForm.patchValue({
      //country_id: this.storeMultiSelect.country_value,
      city_id: this.storeMultiSelect.city_value,
      department_id: this.storeMultiSelect.department_value,
      industry_id: this.storeMultiSelect.industries_value,
    });
  }

  setValue() {
    this.myAlertForm.patchValue({
      //country_id: this.storeMultiSelect.country_value,
      city_id: this.storeMultiSelect.city_value,
      department_id: this.storeMultiSelect.department_value,
      industry_id: this.storeMultiSelect.industries_value,
    });
  }

  formReset() {
    this.skill_key = '';
    this.myAlertForm.patchValue({
      id: '',
      keyword: '',
      salary_type: '',
      currency: '',
      salary: '',
      country_id: '',
      city_id: '',
      department_id: '',
      industry_id: '',
      email_id: '',
      name_of_alert: ''
    });
    this.storeMultiSelect.country_value = [];
    this.storeMultiSelect.country_name = [];
    this.storeMultiSelect.city_value = [];
    this.storeMultiSelect.city_name = [];
    this.storeMultiSelect.industries_value = [];
    this.storeMultiSelect.industries_name = [];
    this.storeMultiSelect.department_name = [];
    this.storeMultiSelect.department_value = [];

  }



}
