import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var Swal: any;

@Component({
  selector: 'app-training-getting-job',
  templateUrl: './training-getting-job.component.html',
  styleUrls: ['./training-getting-job.component.css'],
    animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('700ms ease-in', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('700ms ease-in', style({ transform: 'translateY(100%)' }))
      ])
    ]),

  ]
})
export class TrainingGettingJobComponent implements OnInit {

  trainers: any=[];
  schedules: any=[];
  slots: any=[];
  showslot: boolean=false;
  currentMonth:any;
  activeSchedule: number;
  activeSlotIndex:number;
  selectedTrainer:any;
  selectdSlot:any;
  countries: any=[];
  industries: any =[];
  searchData:any={
  	countryModel:'',
  	indsutryModel:''
  };
  totDays = ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  isClickSearch: boolean = false;
  constructor(private fb: FormBuilder, private commonService: CommonService,
    private messageService: MessageService, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
  	const d = new Date();
  	this.currentMonth = d.getMonth();
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries =  data['countries'];
        this.industries = data['all_industries'];
      }
    });

  	this.asyncInit();

  }

  asyncInit(){
  	this.commonService.getAll('/api/employee/get-trainer/list/1')
  		.subscribe(data=>{
        this.commonService.callFooterMenu(1);
  			this.trainers = data.data;
  			console.log(data);
  		},error=>{});
  }

  searchByTrainer(event){
    this.isClickSearch = true;
  	event.preventDefault();
  	let form={searchby_country:this.searchData.countryModel,searchby_industry:this.searchData.indsutryModel,type:'1'};
  	this.commonService.getDataWithPost('/api/employee/searchby-trainer', form)
  		.subscribe(data=>{
  			this.trainers = data.data;
  		},error=>{});
  }

  openSlot(trainer){
  	this.selectedTrainer = trainer;
  	this.callSlot(this.currentMonth, trainer.id);
  	this.showslot = true;
  }

  previousMonth(month) { 	
    this.currentMonth = month - 1;
    this.callSlot(this.currentMonth, this.selectedTrainer.id);
  }

  nextMonth(month) {
    this.currentMonth = month + 1;
    this.callSlot(this.currentMonth, this.selectedTrainer.id);
  }

  callSlot(month, trainer_id){
  	this.commonService.getAll('/api/employee/get-slots/'+ month + '/'+ trainer_id)
  		.subscribe(data=>{
  			this.schedules = Object.entries(data.data);
  			if(this.schedules.length){
  			this.slots = this.schedules[0][1];
        this.activeSlotIndex =-1;
        this.selectdSlot='';
  			console.log(this.slots);
  			this.activeSchedule =0;
  			}else{
  				this.slots = [];
  			}

  		},error=>{});
  	
  }

  changeSchedule(index){
  	this.activeSchedule = index;
    this.activeSlotIndex =-1;
    this.selectdSlot='';
  	this.slots = this.schedules[index][1];
  }

  selectSlot(slot){
  	this.selectdSlot = slot;
  }

  getdayName(date){
  	let crYear =  new Date().getFullYear();
  	var baseDate = new Date(Date.UTC(crYear, this.currentMonth, date));
  	return this.totDays[baseDate.getDay()];
  }

  bookSlot(slot_id){
  	this.commonService.getAll('/api/employee/schedule-book/store?schedule_id=' + slot_id)
  		.subscribe(data=>{
  			console.log(data);
  		},error=>{});
  }

}
