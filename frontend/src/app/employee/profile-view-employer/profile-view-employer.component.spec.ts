import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileViewEmployerComponent } from './profile-view-employer.component';

describe('ProfileViewEmployerComponent', () => {
  let component: ProfileViewEmployerComponent;
  let fixture: ComponentFixture<ProfileViewEmployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileViewEmployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileViewEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
