import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-profile-view-employer',
  templateUrl: './profile-view-employer.component.html',
  styleUrls: ['./profile-view-employer.component.css']
})
export class ProfileViewEmployerComponent implements OnInit {
 profile_views:any=[];
 total_count:number;
 search_key:any={
   limit:3,
   offset:0,
   from_date:'',
   to_date:''
 }

  constructor(private commonService: CommonService, private messageservice: MessageService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  	this.asyncInit();
  }
  asyncInit(){
  	this.commonService.getAll('/api/employee/employee_profile_views')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.profile_views = data.employer_profiles;
        this.total_count = data.total_count
  		}, (error)=>{});
  }

  filterByDate(event){
    event.preventDefault();
    this.filterData()
  }

  filterData(){
    var queryString = '';
    Object.entries(this.search_key).forEach((data, index)=>{
      if(index == 0){
        queryString += `?${data[0]}=${data[1]}`
      }else{
        queryString += `&${data[0]}=${data[1]}`
      }
      
    })
    if(this.search_key.from_date && this.search_key.to_date ==''){
      this.messageservice.error('please select to date');
      return false;
    }else if(this.search_key.to_date && this.search_key.from_date ==''){
      this.messageservice.error('please select from date');
      return false;
    }
    this.commonService.getAll(`/api/employee/employee_profile_views${queryString}`)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.profile_views = data.employer_profiles;
        this.total_count = data.total_count
      }, (error)=>{});
  }

  filterDataConcat(){
        var queryString = '';
    Object.entries(this.search_key).forEach((data, index)=>{
      if(index == 0){
        queryString += `?${data[0]}=${data[1]}`
      }else{
        queryString += `&${data[0]}=${data[1]}`
      }
      
    })
    if(this.search_key.from_date && this.search_key.to_date ==''){
      this.messageservice.error('please select to date');
      return false;
    }else if(this.search_key.to_date && this.search_key.from_date ==''){
      this.messageservice.error('please select from date');
      return false;
    }
    this.commonService.getAll(`/api/employee/employee_profile_views${queryString}`)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.profile_views = this.profile_views.concat(data.employer_profiles);
        this.total_count = data.total_count
      }, (error)=>{});
  }

  clearFilter(){
    this.search_key.limit =3
    this.search_key.offset =0
    this.search_key.from_date =''
    this.search_key.to_date =''
    this.filterData()
  }

  viewMore(){
    if(this.profile_views.length < this.total_count){
      this.search_key.limit = 20
      this.search_key.offset = this.search_key.offset + 20
      this.filterDataConcat()
    }
    
  }

  getBase64Encode(value){
    return value ? window.btoa(value) :''
  }


}
