import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var $;
declare var swal: any;
import * as _ from 'lodash'; 

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('700ms ease-in', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('700ms ease-in', style({ transform: 'translateY(100%)' }))
      ])
    ]),
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    ),
    trigger('fade', [
      state('void', style({ opacity: 0 })),
      transition(':enter, :leave', [
        animate(700)
      ])
    ])

  ]
})
export class ProfileComponent implements OnInit {
  userPhotos = ['assets/images/clientLogo1.png', 'assets/images/clientLogo2.png', 'assets/images/clientLogo3.png', 'assets/images/clientLogo4.png', 'assets/images/clientLogo5.png'];
  //myCarouselImages = [1,2,3,4,5,6].map((i)=> `https://picsum.photos/640/480?image=${i}`);

  //showElement:boolean;
  showQuickLink:boolean =false;
  showElement: any = [];
  is_profile_update: boolean;
  is_profile_inner: boolean;
  is_desired_job: boolean;
  is_summary_your_pfile: boolean;
  is_emp_history: boolean;
  is_education: boolean;
  is_key_skill: boolean;
  is_it_skill: boolean;
  is_certification: boolean;
  underGraduateDiv: boolean;
  is_edit_lang: boolean;
  profileForm: FormGroup;
  personal_infoForm: FormGroup;
  desiredJobForm: FormGroup;
  summaryForm: FormGroup;
  employeementHisForm: FormGroup;
  educationQualifyForm: FormGroup;
  keyForm: FormGroup;
  certificationForm: FormGroup;
  editLangForm: FormGroup;
  show_all_his: boolean =false;
  prefCountryDiv: boolean;
  prefCityDiv: boolean;
  industriesDiv: boolean;
  departmentDiv: boolean;
  allExperiance; any=[];
  under_graduates: any=[];
  post_under_graduates: any=[];
  storeMultiSelect: any = {
    prefCountry_value: [],
    prefCountry_name: [],
    prefCity_value: [],
    prefCity_name: [],
    industries_value: [],
    industries_name: [],
    department_name: [],
    department_value: [],
    undergradute_value:[],
    undergraduate_name:[],
    undergraduate_sub_value:[]
  };
  show_sub_under: any=[];

  logoFileName: string;
  instituteLogoFileName: string;
  certInstituteLogoname: string;
  emp_his_data: any=[];
  formData: any = {
    profileForm: '',
    personal_infoForm: '',
    desiredJobForm: '',
    summaryForm: '',
    employeementHisForm: '',
    educationQualifyForm: '',
    keyForm: '',
    certificationForm: '',
    editLangForm: ''
  };

  emp_video: any = {
    description: '',
    video_link: ''
  };

  emp_photo: any = {
    id: '',
    description: '',
    photo: '',
    photoUrl: ''
  };

  industry_div: boolean;
  department_div: boolean;
  upload_link: string;
  departments: any = [];
  industryOfComp: any = {
    value: '',
    name: ''
  };

  countries: any = [];
  cities: any = [];
  countryArray:any = [];
  cityArray :any= [];
  employee_certification: any = [];
  employee_education: any = [];
  employee_language: any = [];
  employee_pref: any;
  employee_skill: any = [];
  employment_history: any = [];
  industires: any = [];
  job_roles: any = [];
  profileData: any = [];
  languages: any = [];
  employeeVideos: any = [];
  formDataValue: any;
  otherMaster_data: any = [];
  currency_data: any = [];
  salary_data: any = [];
  experiance_data: any = [];
  experiance_years: any = [];
  experiance_months: any = [];
  employement_type_data: any = [];
  notice_period_data: any = [];
  passoutYearArray: any = [];
  shiftTypes: any = [];
  shiftModel: any = [];
  industriesData: any = [];
  departmentData: any = [];
  skillKey: any;
  skill_mst_data: any = [];
  addOnBlur: boolean = true;
  degrees: any = [];
  employee_photos: any = [];
  isSubmit: boolean = false;
  isLoaded: boolean = false;
  userPhotoOptions: any;
  userPhotoCarouselOptions: any;
  count_profle_strength:number;
  employer_profile_like_count:number;
  coutryWiseCity: any=[];
  galleries: any={ files:[], previews:[]};
  current_date : any;
  showPendingList: boolean =false;
  prevEighteenYearDate: any;
  emp_his_ind: number;
  profile_summary: any = {
    //height: '200px',
    placeholder: 'Profile Summary',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  profile_job: any = {
    //height: '200px',
    placeholder: 'Few Word about Your Job Profile',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  about_certification: any = {
    //height: '200px',
    placeholder: 'Few words about Certification',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  @ViewChild('videoModalClose') videoModalClose: ElementRef;
  @ViewChild('photoModalClose') photoModalClose: ElementRef;
  constructor(private el: ElementRef, private fb: FormBuilder, private commonservice: CommonService,
    private global: Global, private messageservice: MessageService, private sanitizer: DomSanitizer, private router: Router) {
    this.is_profile_update = false;
    //this.showElement =true;
    this.setShowElement();
    this.is_profile_inner = false;
    this.is_desired_job = false;
    this.industry_div = false;
    this.department_div = false;
    this.owlCoursolCall();
    window.scrollTo(0, 0);
    const today = new Date();
    const date = today.getDate();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.current_date = year+'-'+month+'-'+date;

     let prevEighteenYear = (new Date().getFullYear()) - 18;
      let datePrev = new Date().getDate();
      let monthPrev = new Date().getMonth();
      this.prevEighteenYearDate= new Date( Number(monthPrev +1) + '-' + datePrev + '-' + prevEighteenYear);
    //console.log();
    let that = this;
      $(document).on("click",".pending_action h4",function() {
      that.showPendingList = !that.showPendingList;
      });

      $(document).on("click",".pending_view",function() {
      that.showPendingList = !that.showPendingList;
      });
  }


  ngOnInit() {
    this.profileForm = this.fb.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      place: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone_no: ['', [Validators.required, Validators.pattern('^(\\+)?(\\d+)$'),Validators.minLength(8),Validators.maxLength(20)]],
      skype_id: ['']
    });

    this.personal_infoForm = this.fb.group({
      date_of_birth: ['', [Validators.required]],
      nationality: ['', [Validators.required]],
      home_town: ['', [Validators.required]],
      country_id: ['', [Validators.required]],
      city_id: ['', [Validators.required]],
      maritual_status: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      permanent_address: ['', [Validators.required]],
      same_permanent: '',
      present_address: ['', [Validators.required]],
    });

    this.personal_infoForm.controls['same_permanent'].valueChanges.subscribe((value) => {
      if (value) {
        let p_add = this.personal_infoForm.controls['permanent_address'].value;
        this.personal_infoForm.controls['present_address'].setValue(p_add);
      } else {
        this.personal_infoForm.controls['present_address'].setValue('');
      }
    });

   this.personal_infoForm.controls['country_id'].valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
        //this.jobPostForm.controls['currency'].setValue(value);
      }

    });

    this.desiredJobForm = this.fb.group({
      // job_location:['', [Validators.required]],
      country_id: ['', [Validators.required]],
      city_id: ['', [Validators.required]],
      industry_id: ['', [Validators.required]],
      department_id: [''],
      // job_role_id: ['', [Validators.required]],
      shift_type: ['', [Validators.required]],
      job_type: ['', [Validators.required]],
      currency_type: ['', [Validators.required]],
      salary_type: ['', [Validators.required]],
      min_salary: ['', [Validators.required]],
      max_salary: ['', [Validators.required, this.checkMinMaxSal]],
      designation:[]
    });

    this.desiredJobForm.controls['country_id'].valueChanges.subscribe(value=>{
      if(value){
       let countryCode = this.countries.filter(obj=> this.storeMultiSelect.prefCountry_value.indexOf(obj._id) != -1).map(obj=>obj.code);
       this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
      }

    });

    this.summaryForm = this.fb.group({
      profile_title: ['', [Validators.required]],
      total_experience_yr: ['', [Validators.required]],
      total_experience_mn: ['', [Validators.required]],
      notice_period: ['', [Validators.required]],
      current_salary_currency:['', [Validators.required]],
      current_salary: ['', [Validators.required]],
      current_salary_type:['', [Validators.required]],
      summary: ['', [Validators.required]]
    });


    this.employeementHisForm = this.fb.group({
      id: '',
      job_title: ['', [Validators.required]],
      company_logo: [''],
      company_name: ['', [Validators.required]],
      company_website: [''],
      industry_of_company: ['', [Validators.required]],
      department: [''],
      is_present_company:[''],
      from_date: ['', [Validators.required, this.checkFromDate]],
      to_date: ['', [Validators.required, this.checkToDate]],
      about_job_profile:['']
    });

    this.employeementHisForm.controls['is_present_company'].setValue('');
    this.employeementHisForm.controls['is_present_company'].valueChanges.subscribe(value=>{
      if(value){
        this.employeementHisForm.controls['to_date'].clearValidators();
        this.employeementHisForm.controls['to_date'].updateValueAndValidity();
      }else{
        this.employeementHisForm.controls['to_date'].setValidators([Validators.required, this.checkToDate]);
        this.employeementHisForm.controls['to_date'].updateValueAndValidity();
      }
    });

    this.educationQualifyForm = this.fb.group({
      id: '',
      degree_id: ['', [Validators.required]],
      degree_parent:[''],
      course_name: [''],
      specialization: [''],
      name_of_institute: ['', [Validators.required]],
      institute_logo: [''],
      institute_website: [''],
      course_type: ['', [Validators.required]],
      passout_year: ['', [Validators.required]]

    });
    this.educationQualifyForm.get('degree_id').setValue('');

    this.keyForm = this.fb.group({
      title: ['', [Validators.required]]
    });

    this.certificationForm = this.fb.group({
      id: '',
      course_name: ['', [Validators.required]],
      institute_name: ['', [Validators.required]],
      details: [''],
      certification_date: ['', [Validators.required]],
      about_certification: [''],
      institute_logo: ['']
    });

    this.editLangForm = this.fb.group({
      id: '',
      lang_id: ['', [Validators.required]],
      proficiency: ['', [Validators.required]]
    });

    this.editLangForm.get('lang_id').setValue('');
    this.editLangForm.get('proficiency').setValue('');

    this.setCommonData();
    this.getData();

  }

  divHide(form_name) {
    setTimeout(() => {
      this.showElement[form_name] = false;
    }, 700);
  }

  scroll(id) {   
    let div:any = document.getElementById(id);
    const header_heigth = $('header').innerHeight();
      $('html, body').animate({
        scrollTop: $('#' + id).offset().top - header_heigth
      }, 1000);

  }

  scrollMobile(id) {   
    let div:any = document.getElementById(id);
    const header_heigth = $('header').innerHeight();
    const addHeigth = id =='resumediv'? 0: 350;
      $('html, body').animate({
        scrollTop: $('#' + id).offset().top - (header_heigth + addHeigth)
      }, 1000);

  }

  owlCoursolCall() {
    this.userPhotoOptions = { items: 3, dots: false, nav: true, autoplay: false, loop: false, margin: 10, responsive: { 0: { items: 1 }, 600: { items: 2 }, 1000: { items: 3 } } };
    this.userPhotoCarouselOptions = { items: 1, dots: false, nav: true, autoplay: true, loop: true, margin: 0, responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 5 } } };
  }

    setCommonData(){
      this.commonservice.getCountryData.subscribe(data=>{
        if(data && data.status ==200){
          this.countries = data['countries'];
          this.countryArray = {"":data['countries']};
          this.industires = data['industries'];
          this.industriesData = data['all_industries'];
        }
      });

      this.commonservice.getCityData.subscribe(data=>{
          if(data && data.status ==200){
             this.cities = data['cities'];
          }
      });      
      this.commonservice.getCommonData.subscribe(data=>{
        if(data && data.status ==200){
          this.job_roles = data['job_roles'];
          this.departments = data['all_department'];
          this.languages = data['alllanguage'];
          this.otherMaster_data = data['other_mst'];
          this.salary_data = data['other_mst'].filter((data) => data.entity == 'salary');
          this.experiance_years = data['other_mst'].filter((data) => data.entity == 'experiance_year');
          this.experiance_months = data['other_mst'].filter((data) => data.entity == 'experiance_month');
          this.employement_type_data = data['other_mst'].filter((data) => data.entity == 'employement_type');
          this.notice_period_data = data['other_mst'].filter((data) => data.entity == 'notice_period');
          this.currency_data = data['all_currency'];
          this.departmentData = data['departments'];
          this.under_graduates = _.uniqBy(data['courses'][""],(e)=>{
              return e.name
          });
          this.post_under_graduates = {"":_.uniqBy(data['courses'][""],(e)=>{
              return e.name
            })};
          this.skill_mst_data = _.uniqBy(data['skills'],(e)=>{
              return e.skill
            }).map((obj) => { return { display: obj.skill, value: obj.skill } });
          this.degrees = data['degrees'];
          this.shiftTypes = data['other_mst'].filter((data) => data.entity == 'shift_type'); 
          this.allExperiance = data['allExprience'];  
        }
      });
  }

  getData() {
    this.commonservice.getAll('/api/employee-profile')

      .subscribe((data) => {
        this.commonservice.callFooterMenu(1);
        if (data.status == 200) {           
          this.isLoaded = true;
          this.profileData = data.data.profile;
          this.employee_certification = data.data.employee_certification;
          this.employee_education = data.data.employee_education;
          this.employee_language = data.data.employee_language;
          this.employee_pref = data.data.employee_pref;
          this.employee_skill = _.uniqBy(data.data.employee_skill,(e)=>{
              return e.title
            });
          this.emp_his_data = data.data.employment_history;
          this.employment_history =  this.emp_his_data.length > 2  ?this.emp_his_data.slice(0,2): this.emp_his_data;    
          this.employee_photos = data.data.employee_photos;
          this.count_profle_strength = data.data.count_profle_strength;
          this.employer_profile_like_count = data.data.employer_profile_like_count;
          var start_year = new Date().getFullYear();
          for (var i = start_year; i > start_year - 70; i--) {
            this.passoutYearArray.push(i);
          }

          this.employeeVideos = data.data.employee_videos.map(video => {
            return {
              id: video._id,
              description: video.description,
              video_link: this.sanitizer.bypassSecurityTrustResourceUrl(video.video_link.replace("watch?v=", "embed/"))
            };
          });
          if (this.employee_skill.length) {
            this.skillKey = this.employee_skill.map((obj) => {
              return { value: obj.title, display: obj.title };

            });
          }

        }
      }, (error) => { });
  }

  saveForm(form_name) {
    this.isSubmit = true;
    event.preventDefault();
    switch (form_name) {
      case "profileForm":
        if (this.profileForm.invalid) {
          return false;
        }
        this.showElement['profileForm'] = true;
        this.is_profile_update = false;
        this.formData.profileForm = this.profileForm.value;
        break;
      case "personal_infoForm":
        if (this.personal_infoForm.invalid) {
          return false;
        }
        this.showElement['personal_infoForm'] = true;
        this.is_profile_inner = false;
        this.formData.personal_infoForm = this.personal_infoForm.value;
        break;
      case "desiredJobForm":
        if (this.desiredJobForm.invalid) {
          return false;
        }
        this.showElement['desiredJobForm'] = true;
        this.is_desired_job = false;
        this.desiredJobForm.get('country_id').setValue(this.storeMultiSelect.prefCountry_value);
        this.desiredJobForm.get('city_id').setValue(this.storeMultiSelect.prefCity_value);
        this.desiredJobForm.get('industry_id').setValue(this.storeMultiSelect.industries_value);
        this.desiredJobForm.get('department_id').setValue(this.storeMultiSelect.department_value);
        this.formData.desiredJobForm = this.desiredJobForm.value;
        break;
      case "summaryForm":
        if (this.summaryForm.invalid) {
          return false;
        }
        this.is_summary_your_pfile = false;
        this.summaryForm.controls['current_salary'].setValue(this.summaryForm.value.current_salary ? Number(this.summaryForm.value.current_salary):'');
        this.formData.summaryForm = this.summaryForm.value;
        break;
      case "employeementHisForm":
        break;
      case "educationQualifyForm":
        //this.formData.educationQualifyForm = this.formDataSet(this.employeementHisForm);
        break;
      case "keyForm":
        let skills = this.skillKey.map(obj => obj.value);
        this.keyForm.get('title').setValue(skills);
        if (this.keyForm.invalid) {
          return false;
        }
        this.is_key_skill = false;

        this.formData.keyForm = this.keyForm.value;
        break;
      case "certificationForm":
        if (this.certificationForm.invalid) {
          return false;
        }
        this.is_certification = false;
        this.formData.certificationForm = this.certificationForm.value;
        break;
      case "editLangForm":
        if (this.editLangForm.invalid) {
          return false;
        }
        this.is_edit_lang = false;
        this.formData.editLangForm = this.editLangForm.value;
        break;
      default:
        // code...
        break;
    }

    //console.log(this.formData);

    this.commonservice.create('/api/employee-profile', this.formData)
      .subscribe(data => {
        if (data.status == 200) {
          this.isSubmit = false;
          this.resetFormData();
          this.resetForm();
          this.profileData = data.data.profile;
          //this.employee_certification = data.data.employee_certification;
          //this.employee_education = data.data.employee_education;
          this.employee_language = data.data.employee_language;
          this.employee_pref = data.data.employee_pref;
          this.employee_skill = data.data.employee_skill;
          this.emp_his_data = data.data.employment_history;
          this.employment_history = this.show_all_his ? this.emp_his_data : this.emp_his_data.slice(0,2);
          this.messageservice.success(data.status_text);
          if(form_name =='certificationForm'){
            $('#addcerModal').find('.close').click();
            this.redirectTo(this.router.url);
          }
        }
      }, error => {

      });

  }

  saveCertificateForm(){
    if(this.certificationForm.invalid){
       return false;
    }
    this.is_certification = false; 
    let data =  this.formDataSet(this.certificationForm, 'certification_form');
    this.commonservice.create('/api/employee-profile/store-formdata', data)
      .subscribe(data => {
        if (data.status == 200) {
          this.isSubmit = false;
          this.messageservice.success(data.status_text);
            $('#addcerModal').find('.close').click();
            this.redirectTo(this.router.url);
        }
      }, error => { });
  }


  saveFormDataForm(type) {
    event.preventDefault();
    this.isSubmit = true;
    if (type == 'employeementHisForm' && this.employeementHisForm.invalid) {
      return false;
    } else if (type == 'education' && this.educationQualifyForm.invalid) {
      return false;
    }
    this.is_emp_history = false;
    this.is_education = false;      
    let data = (type == 'employeementHisForm') ? this.formDataSet(this.employeementHisForm, 'employeement_history') :
      this.formDataSet(this.educationQualifyForm, 'education');
    this.commonservice.create('/api/employee-profile/store-formdata', data)
      .subscribe(data => {
        if (data.status == 200) {
          this.isSubmit = false;
          //this.showElement[] = true;
          if (type == 'employeementHisForm') {
            this.redirectTo(this.router.url);
            //this.emp_his_data.push(data.data);
            //this.employment_history.push(data.data);
            $("#addexpModal").find('.close').click();
          } else if (type == 'education') {
            this.employee_education.push(data.data);
            $("#addeduModal").find('.close').click();
            this.redirectTo(this.router.url);
          }
          this.resetForm();
          this.messageservice.success(data.status_text);
        }
      }, error => { });
  }

  getChange(e) {
    if (e.field_name == 'industries') {
      this.industryOfComp.value = e.value;
      this.industryOfComp.name = e.name;
      this.employeementHisForm.controls['industry_of_company'].setValue(e.name);
    }
      else if (e.field_name == 'qualification') {
      this.industryOfComp.value = e.value;
      this.industryOfComp.name = e.name;
      this.employeementHisForm.controls['industry_of_company'].setValue(e.name);
    }
    //this.industry_div =
  }

  checkOutSide(e) {
    this.industry_div = e.status;
  }

  setFormValue(form) {
    //this.resetForm();
    if (form == 'profileForm') {
      this.profileForm.patchValue({
        first_name: this.profileData.first_name,
        last_name: this.profileData.last_name,
        place: this.profileData.place,
        email: this.profileData.user.email,
        phone_no: this.profileData.user.phone_no,
        skype_id: this.profileData.skype_id
      });

    }
    else if (form == 'personal_infoForm') {
      this.personal_infoForm.patchValue({
        date_of_birth: this.profileData.date_of_birth,
        nationality: this.profileData.nationality,
        home_town: this.profileData.home_town,
        country_id: this.profileData.country_id ? String(this.profileData.country_id) : '',
        city_id: this.profileData.city_id ? String(this.profileData.city_id) : '',
        maritual_status: this.profileData.maritual_status ? String(this.profileData.maritual_status) : '',
        permanent_address: this.profileData.permanent_address,
        same_permanent: this.profileData.same_permanent,
        present_address: this.profileData.present_address,
        gender: this.profileData.gender ? String(this.profileData.gender) : ''
      });
    }
    else if (form == 'desiredJobForm') {
      this.storeMultiSelect.prefCountry_value = this.employee_pref.countries? this.employee_pref.countries.map(obj => obj._id):[];
      this.storeMultiSelect.prefCountry_name = this.employee_pref.countries?this.employee_pref.countries.map(obj => obj.name):[];
      this.storeMultiSelect.prefCity_value = this.employee_pref.cities ? this.employee_pref.cities.map(obj => obj._id):[];
      this.storeMultiSelect.prefCity_name = this.employee_pref.cities ? this.employee_pref.cities.map(obj => obj.name):[];
      this.storeMultiSelect.industries_value = this.employee_pref.industries ? this.employee_pref.industries.map(obj => obj._id):[];
      this.storeMultiSelect.industries_name = this.employee_pref.industries ? this.employee_pref.industries.map(obj => obj.name):[];
      this.storeMultiSelect.department_value = this.employee_pref.departments ? this.employee_pref.departments.map(obj => obj._id):[];
      this.storeMultiSelect.department_name = this.employee_pref.departments ? this.employee_pref.departments.map(obj => obj.name):[];

      this.desiredJobForm.patchValue({
        job_location: this.employee_pref ? this.employee_pref.job_location : '',
        country_id: this.employee_pref ? this.employee_pref.countries.map(obj => obj.name) : '',
        city_id: this.employee_pref ? this.employee_pref.cities.map(obj => obj.name) : '',
        industry_id: this.employee_pref ? this.employee_pref.industries.map(obj => obj.name) : '',
        department_id: this.employee_pref ? this.employee_pref.departments.map(obj => obj.name) : '',
        job_role_id: this.employee_pref ? this.employee_pref.department_id : '',
        shift_type: this.employee_pref ? this.employee_pref.shift_types.map(obj => obj._id) : '',
        job_type: this.employee_pref ? this.employee_pref.job_type : '',
        currency_type: this.employee_pref ? this.employee_pref.currency_type : '',
        salary_type: this.employee_pref ? this.employee_pref.salary_type : '',
        min_salary: this.employee_pref ? this.employee_pref.min_salary : '',
        max_salary: this.employee_pref ? this.employee_pref.max_salary : '',
        designation: this.employee_pref ? this.employee_pref.designation : ''
      });
    }
    else if (form == 'summaryForm') {
      this.summaryForm.patchValue({
        profile_title: this.profileData ? this.profileData.profile_title : '',
        total_experience_yr: (this.profileData && this.profileData.total_experience_yr) ? this.profileData.total_experience_yr._id : '',
        total_experience_mn: (this.profileData && this.profileData.total_experience_mn) ? this.profileData.total_experience_mn._id : '',
        notice_period: (this.profileData && this.profileData.notice_period) ? this.profileData.notice_period : '',
        current_salary_currency:(this.profileData && this.profileData.current_salary_currency)?this.profileData.current_salary_currency._id:'',
        current_salary: (this.profileData && this.profileData.current_salary) ? this.profileData.current_salary : '',
        current_salary_type: this.profileData && this.profileData.current_salary_type ? this.profileData.current_salary_type:'',
        summary: this.profileData ? this.profileData.summary : '',
      });
    }

  }

  uploadLogo(e, logo) {
    let file = e.target.files[0];
    const file_extence = file.name.split('.').pop().toLowerCase();
    //console.log(file_extence);
    const filesize = Math.round((file.size / 1024));
    if(filesize > 2000 ){
      this.messageservice.error('Maximum file size allowed 2 MB');
      return false;
    }
    //const formData = new FormData();
    if(file_extence == 'jpg' || file_extence == 'png' || file_extence == 'jpeg'){
      if(logo =='emp_his_comp_logo') {
        this.logoFileName = file.name;
        this.employeementHisForm.get('company_logo').setValue(file);
      }
      else if (logo == 'edu_ins_logo') {
        this.instituteLogoFileName = file.name;
        this.educationQualifyForm.get('institute_logo').setValue(file);
      }else if(logo == 'cert_ins_logo'){
        this.certInstituteLogoname = file.name;
        this.certificationForm.get('institute_logo').setValue(file);
      }
    }else{
      this.messageservice.error('Upload file only allow jpg,png or jpeg!');
    }

  }

  imageChangeEvent(e) {
    //const file = e.target.files[0];
    //this.emp_photo.photo = file;
   // const reader = new FileReader();
    // reader.onload = (event: any) => {
    //   this.emp_photo.photoUrl = event.target.result;
    // }
    // reader.readAsDataURL(file);
    if(this.galleries.previews.length > 4){
      this.messageservice.error('Cannot Upload morethan 5 image!');
      return false;
    }
    const file = e.target.files[0];
    const file_extence = file.name.split('.').pop().toLowerCase();
    console.log(file_extence);
    const filesize = Math.round((file.size / 1024));
    if(filesize > 2000 ){
      this.messageservice.error('Maximum file size allowed 2 MB');
      return false;
    }
    if(file_extence == 'jpg' || file_extence == 'png' || file_extence == 'jpeg'){
      const reader = new FileReader();
      let rand_id = Math.random();
      reader.onload = (event:any) => {
        this.galleries.previews.push({'url':event.target.result, 'type':'upload','id':rand_id});
      }
      reader.readAsDataURL(file);
      this.galleries.files.push({id:rand_id,'file':file});
    //console.log(this.galleries);
      
    }else{
      this.messageservice.error('Upload file only allow jpg,png or jpeg!');
    }

  }

  removeFile(id, type){
    //console.log(id);
    let index = this.galleries.previews.findIndex(preview=>preview.id ==id);
    this.galleries.previews.splice(index,1);
    if(type =='upload'){
      let file_index = this.galleries.files.findIndex(file=>file.id ==id);
      this.galleries.files.splice(file_index,1);
    }
  }

  uploadPhoto() {

    let form = new FormData();
    form.append('description', this.emp_photo.description);
    //form.append('photo', this.emp_photo.photo);
    for(var i = 0; i < this.galleries.files.length; i++){
      form.append('photo[]',this.galleries.files[i].file);
      console.log(this.galleries.files[i].file);
    }
    //console.log(form);
    this.commonservice.create('/api/employee/image-upload', form)
      .subscribe((data) => {
        if (data.status == 200) {
          this.photoModalClose.nativeElement.click();
          this.emp_photo.description = '';
          this.emp_photo.photo = '';
          this.emp_photo.photoUrl = '';
          this.employee_photos.push(data.data);
          this.messageservice.success(data.status_text);
          this.redirectTo(this.router.url);
        } else if (data.status == 422) {
          if (data.error && data.error.photo) {
            this.messageservice.error(data.error.photo[0]);
          } if (data.error && data.error.description) {
            this.messageservice.error(data.error.description[0]);
          }

        }
      }, (error) => { });

  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate([uri]));
  }

  editPhoto(id) {
    let data = this.employee_photos.find(obj => obj._id == id);
    this.emp_photo.id = id;
    this.emp_photo.description = data.description;
    this.emp_photo.photoUrl = data.photo_image;
  }

  updatePhoto() {
    let updateForm: any = '';
    if (this.emp_photo.photo) {
      let form = new FormData();
      form.append('id', this.emp_photo.id);
      form.append('description', this.emp_photo.description);
      form.append('photo', this.emp_photo.photo);
      updateForm = form;
    } else {
      updateForm = this.emp_photo;
    }

    this.commonservice.create('/api/employee/update-photo', updateForm)
      .subscribe(data => {
        if (data.status == 200) {
          this.emp_photo.id = '';
          this.emp_photo.description = '';
          this.emp_photo.photo = '';
          this.emp_photo.photoUrl = '';
          this.photoModalClose.nativeElement.click();
          this.redirectTo(this.router.url);
        } else if (data.status == 422) {
          if (data.error && data.error.photo) {
            this.messageservice.error(data.error.photo[0]);
          } if (data.error && data.error.description) {
            this.messageservice.error(data.error.description[0]);
          }
        }
      }, error => { });
  }

  deletePhoto(id) {
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-photo', id)
          .subscribe(response => {
            if (response.status === 200) {
              let index = this.employee_photos.findIndex(obj => obj._id == id);
              this.employee_photos.splice(index, 1);
              this.messageservice.success(response.status_text);
              this.redirectTo(this.router.url);
              this.owlCoursolCall();
            }
          });
      }
    });

  }

  createAlbum() {

  }


  uploadResume(e) {
    let file = e.target.files[0];
    //console.log(file);
    if (file.name.split('.').pop().toLowerCase() == 'pdf' || file.name.split('.').pop().toLowerCase() == 'docx' || file.name.split('.').pop().toLowerCase() == 'doc') {
      const formData = new FormData();
      formData.append('employee_resume', file);
      this.commonservice.create('/api/employee/resume-upload', formData)
        .subscribe((data) => {
          if (data.status == 200) {
            this.profileData = data.profile;
            this.count_profle_strength = this.profileData.profile_strength;
            this.profileData.user.cv = data.cv;
            this.profileData.user.cv_path = data.cv_path;
            this.messageservice.success(data.status_text);
          }
        }, (error) => { });
    } else {
      this.messageservice.error('Upload file only allow PDF,DOCX or DOC');
    }

  }

  getFileExtention(file) {
    return file.split('.').pop();
  }
  deleteCv(id){
    //console.log(id);
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-resume', id)
          .subscribe(response => {
            if (response.status === 200) {
              //this.profileData.user.cv = data.cv;
              this.profileData.user.cv_path = '';
              this.messageservice.success(response.status_text);
            }
          });
      }
    });
  }

  uploadVideo() {
    if(!this.emp_video.video_link.match('^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+')){
      this.messageservice.error('Please enter proper youtube link');
      this.emp_video.video_link ='';
      return false;
    }
    this.commonservice.create('/api/employee/video-upload', this.emp_video)
      .subscribe((data) => {
        if (data.status == 200) {
          let new_data = { id: data.data._id, description: data.data.description, video_link: this.sanitizer.bypassSecurityTrustResourceUrl(data.data.video_link.replace("watch?v=", "embed/")) };
          this.employeeVideos.push(new_data);
          this.messageservice.success(data.status_text);
          this.emp_video.description ='';
          this.emp_video.video_link ='';
          this.videoModalClose.nativeElement.click();
        }
      }, (error) => { });

  }

  removeVideo(id) {
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-video', id)
          .subscribe(response => {
            if (response.status === 200) {
              let index = this.employeeVideos.findIndex(obj => obj.id == id);
              this.employeeVideos.splice(index, 1);
              this.messageservice.success(response.status_text);
              this.owlCoursolCall();
            }
          });
      }
    });
  }

  formDataSet(form, type) {
    let formData = new FormData();
    formData.append('form_type', type);
    Object.keys(form.controls).forEach(key => {
      formData.append(key, form.get(key).value);
    });
    return formData;
  }

  changeProfileImage(e) {
    let file = e.target.files[0];
    const formData = new FormData();
    formData.append('profile_image', file);
    this.commonservice.create('/api/employee-profile/image-change', formData)
      .subscribe((data) => {
        if (data.status == 200) {
          this.profileData = data.data;
           localStorage.setItem('user', JSON.stringify(data.user));
           this.commonservice.callProfileUpdate(data.user);
          this.messageservice.success(data.status_text);
        }
      }, (error) => { });

  }

  showEducationEdit(id) {
    this.addEducationReset();
    let data = this.employee_education.find(edu => edu.id == id);
    this.educationQualifyForm.patchValue({
      id: data.id,
      degree_id: data.course_degree_id ?data.course_degree_id :data.degree_id,
      degree_parent: data.degree_parent,
      course_name: data.course_name,
      specialization: data.specialization,
      name_of_institute: data.name_of_institute,
      institute_logo: data.institute_logo,
      institute_website: data.institute_website,
      course_type: String(data.course_type),
      passout_year: data.passout_year

    });

     this.storeMultiSelect.undergradute_value = [data.degree_id];
     this.storeMultiSelect.undergraduate_name = data.course_degree_id? [data.course_degree_data.name]: [data.degree_name]
    /*if(data.degree_parent){
      if(Number(data.degree_data.parent_id) == 0){
        this.storeMultiSelect.undergradute_value = [data.degree_parent];
        this.storeMultiSelect.undergraduate_name = [data.degree_parent_name]
      }else{
        this.storeMultiSelect.undergradute_value = [data.degree_parent];
        this.storeMultiSelect.undergraduate_name = [data.degree_parent_name]
        this.storeMultiSelect.undergraduate_sub_value.push({'parent_name' :data.degree_parent_name, 'value': data.degree_id})

      }
    }*/

    
  }

  addEducationReset(){
    this.educationQualifyForm.patchValue({
      id: '',
      degree_id: '',
      course_name: '',
      specialization: '',
      name_of_institute: '',
      institute_logo: '',
      institute_website: '',
      course_type: '',
      passout_year: ''

    });
    this.storeMultiSelect.undergradute_value =[];
    this.storeMultiSelect.undergraduate_name =[];
    this.storeMultiSelect.undergraduate_sub_value =[];   
  }

  educationUpdate(event) {
     event.preventDefault();
    if (this.educationQualifyForm.invalid) {
      return false;
    }
    this.is_education = false;
    let dataIndex = this.employee_education.findIndex(edu => edu.id == this.educationQualifyForm.get('id').value);
    let formValue = this.formDataSet(this.educationQualifyForm,'emp_education');
    this.commonservice.create('/api/employee/update-education', formValue)
      .subscribe(data => {
        if (data.status == 200) {
          this.employee_education[dataIndex] = data.data;
          this.messageservice.success(data.status_text);
          this.resetForm();
          $("#addeduModal").find('.close').click();
          this.redirectTo(this.router.url);
        }

      }, error => { });
  }

  educationDelete(id) {
    const index = this.employee_education.findIndex((obj => obj.id === id));
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-eudcation', id)
          .subscribe(response => {
            if (response.status === 200) {
              this.redirectTo(this.router.url);
              this.employee_education.splice(index, 1);
              this.messageservice.success(response.status_text);
            }
          });
      }
    });
  }

  showCertification(id) {
    this.addCertificationReset();
    let data = this.employee_certification.find(certificate => certificate._id == id);
    this.certificationForm.patchValue({
      id: data._id,
      course_name: data.course_name,
      institute_name: data.institute_name,
      details: data.details,
      certification_date: data.certification_date,
      about_certification: data.about_certification,
      institute_logo:data.institute_logo
    });
  }

  addCertificationReset(){
    this.certificationForm.patchValue({
      id: '',
      course_name: '',
      institute_name: '',
      details: '',
      certification_date:'',
      about_certification:'',
    });
  }

  certificationEdit() {
    event.preventDefault();
    if (this.certificationForm.invalid) {
      return false;
    }
    this.is_certification = false;
    let dataIndex = this.employee_certification.findIndex(certificate => certificate._id == this.certificationForm.get('id').value);
    let data =  this.formDataSet(this.certificationForm, 'certification_form');
    this.commonservice.create('/api/employee/update-certification', data)
      .subscribe(data => {
        if (data.status == 200) {
          this.employee_certification[dataIndex] = data.data;
          this.messageservice.success(data.status_text);
          this.resetForm();
          $('#addcerModal').find('.close').click();
          this.redirectTo(this.router.url);
        }

      }, error => { });
  }

  certificationDelete(id) {
    const index = this.employee_certification.findIndex((obj => obj.id === id));
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-certification', id)
          .subscribe(response => {
            if (response.status === 200) {
              this.employee_certification.splice(index, 1);
              this.messageservice.success(response.status_text);
              this.redirectTo(this.router.url);
            }
          });
      }
    });
  }


  showLanguages(id) {
    let data = this.employee_language.find(obj => obj._id == id);
    //console.log(data);
    this.editLangForm.patchValue({
      id: data._id,
      lang_id: data.lang_id,
      proficiency: data.proficiency,
    });
  }

  languageEdit() {
    event.preventDefault();
    if (this.editLangForm.invalid) {
      return false;
    }
    this.is_edit_lang = false;
    let dataIndex = this.employee_language.findIndex(obj => obj._id == this.editLangForm.get('id').value);
    this.commonservice.create('/api/employee/update-language', this.editLangForm.value)
      .subscribe(data => {
        if (data.status == 200) {
          this.employee_language[dataIndex] = data.data;
          this.messageservice.success(data.status_text);
          this.resetForm();
          this.editLangForm.reset();
        }

      }, error => { });
  }

  LanguageDelete(id) {
    const index = this.employee_language.findIndex((obj => obj.id === id));
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-language', id)
          .subscribe(response => {
            if (response.status === 200) {
              this.employee_language.splice(index, 1);
              this.messageservice.success(response.status_text);
            }
          });
      }
    });
  }


  showEmpHistory(id) {
    let data = this.employment_history.find(history => history.id == id);
    this.employeementHisForm.patchValue({
      id: data.id,
      job_title: data.job_title,
      company_logo: data.company_logo,
      company_name: data.company_name,
      company_website: data.company_website,
      industry_of_company: data.industry_of_company,
      department: data.department._id,
      from_date: data.from_date,
      to_date: data.to_date,
      is_present_company: data.is_present_company,
      about_job_profile:data.about_job_profile
    });
  }

  empHistoryEdit() {
    let dataIndex = this.employment_history.findIndex(history => history.id == this.employeementHisForm.get('id').value);
    let formValue = this.formDataSet(this.employeementHisForm, 'employeement_history');
    this.commonservice.create('/api/employee/update-employee-history', formValue)
      .subscribe(data => {
        if (data.status == 200) {
          this.employment_history[dataIndex] = data.data;
          this.messageservice.success(data.status_text);
          this.resetForm();
          this.employeementHisForm.reset();
        }

      }, error => { });
  }

  empHistoryDelete(id) {
    const index = this.employment_history.findIndex((obj => obj.id === id));
    swal({
      title: 'Are you sure?',
      text: 'You want to delete this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employee/delete-employee-history', id)
          .subscribe(response => {
            if (response.status === 200) {
              this.employment_history.splice(index, 1);
              this.messageservice.success(response.status_text);
            }
          });
      }
    });
  }

  resetForm() {
    this.logoFileName ='';
    this.instituteLogoFileName ='';
    this.profileForm.patchValue({
      first_name: '',
      last_name: '',
      permanent_address: '',
      email: '',
      phone_no: '',
      skype_id: '',
    });

    this.personal_infoForm.patchValue({
      date_of_birth: '',
      nationality: '',
      home_town: '',
      country_id: '',
      city_id: '',
      maritual_status: '',
      gender: '',
      permanent_address: '',
      same_permanent: '',
      present_address: '',
    });

    this.desiredJobForm.patchValue({
      job_location: '',
      country_id: '',
      city_id: '',
      industry_id: '',
      department_id: '',
      job_role_id: '',
      shift_type: '',
      job_type: '',
      currency_type: '',
      salary_type: '',
      min_salary: '',
      max_salary: '',
      designation:'',
    });

    this.summaryForm.patchValue({
      profile_title: '',
      total_experience: '',
      notice_period: '',
      current_salary: '',
      summary: '',
    });

    this.employeementHisForm.patchValue({
      id: '',
      job_title: '',
      company_logo: '',
      company_name: '',
      company_website: '',
      industry_of_company: '',
      department: '',
      from_date: '',
      to_date: '',
      about_job_profile:'',
    });

    this.educationQualifyForm.patchValue({
      id: '',
      degree_id: '',
      course_name: '',
      specialization: '',
      name_of_institute: '',
      institute_logo: '',
      institute_website: '',
      course_type: '',
      passout_year: ''
    });

    this.certificationForm.patchValue({
      id: '',
      course_name: '',
      institute_name: '',
      details: '',
      certification_date: '',
      about_certification: ''
    });

    this.editLangForm.patchValue({
      lang_id: '',
      proficiency: ''
    });


  }

  getChangeSelect(e) {
    //console.log(e);
    if (e.event) {
      let index = e.storedValue.indexOf(e.value);
      if (index == -1) {
        e.storedValue.push(e.value);
        e.storedName.push(e.name);
      }
    } else {
      let index = e.storedValue.indexOf(e.value);
      if (index != -1) {
        e.storedValue.splice(index, 1);
        e.storedName.splice(index, 1);
      }
    }
    if (e.field_name == 'prefCountry') {
      this.desiredJobForm.patchValue({
        country_id: e.storedValue
      });
    } else if (e.field_name == 'prefCity') {
      this.desiredJobForm.patchValue({
        city_id: e.storedValue
      });
    } else if (e.field_name == 'industry') {
      this.desiredJobForm.patchValue({
        industry_id: e.storedValue
      });
    } else if (e.field_name == 'department') {
      //console.log(e.storedName);
      this.desiredJobForm.patchValue({
        department_id: e.storedValue
      });
    }
  }

  getOnchangeEvent(type) {
    if (type == 'prefCountry') {
      this.prefCountryDiv = true;
    } else if (type == 'prefCity') {
      this.prefCityDiv = true;
    } else if (type == 'industry') {
      this.industriesDiv = true;
    } else if (type == 'department') {
      this.departmentDiv = true;
    }else if(type =='undergraduate'){
      this.underGraduateDiv = true;
    }

  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.desiredJobForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "prefCountry":
       this.storeMultiSelect.prefCountry_value.splice(indx, 1);
       this.storeMultiSelect.prefCountry_name.splice(indx, 1);
       this.desiredJobForm.patchValue({
        country_id: this.storeMultiSelect.prefCountry_value
       });
        break;  
      case "prefCity":
       this.storeMultiSelect.prefCity_value.splice(indx, 1);
       this.storeMultiSelect.prefCity_name.splice(indx, 1);
       this.desiredJobForm.patchValue({
          city_id: this.storeMultiSelect.prefCity_value
       });
        break;  
      case "department":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.desiredJobForm.patchValue({
          department_id: this.storeMultiSelect.department_value
       });
        break;    
      default:
        // code...
        break;
    }
  }


  checkmultiselect(e) {
    if (e.field_name == 'prefCountry') {
      this.prefCountryDiv = e.status;
    } else if (e.field_name == 'prefCity') {
      this.prefCityDiv = e.status;
    } else if (e.field_name == 'industry') {
      this.industriesDiv = e.status;
    } else if (e.field_name == 'department') {
      this.departmentDiv = e.status;
    }
    else if (e.field_name == 'qualification') {
      this.underGraduateDiv = e.status;
    }
  }

  multiselectAll(e) {
    //console.log(e.storedValue);
    let filed = e.field_name;
    if (e.event) {
      e.arrValue.forEach((value, i) => {
        let index = e.storedValue.indexOf(value);
        if (index == -1) {
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }
      });
    } else {
      e.arrValue.forEach(value => {
        let index = e.storedValue.indexOf(value);
        if (index != -1) {
          e.storedValue.splice(index, 1);
          e.storedName.splice(index, 1);
        }
      });
    }
    if (filed == 'prefCountry') {
      this.desiredJobForm.patchValue({
        country_id: e.storedName
      });
    } else if (filed == 'prefCity') {
      this.desiredJobForm.patchValue({
        city_id: e.storedName
      });
    } else if (filed == 'industry') {
      this.desiredJobForm.patchValue({
        industry_id: e.storedName
      });
    } else if (filed == 'department') {
      this.desiredJobForm.patchValue({
        department_id: e.storedName
      });
    }

  }

  shiftModelEvent(event) {
    if (event.target.checked) {
      this.shiftModel.push(event.target.value);
    } else {
      let index = this.shiftModel.indexOf(event.target.value);
      this.shiftModel.splice(index, 1);
    }
    this.desiredJobForm.get('shift_type').setValue(this.shiftModel);
  }

  resetFormData() {
    this.formData.profileForm = '';
    this.formData.personal_infoForm = '';
    this.formData.desiredJobForm = '';
    this.formData.summaryForm = '';
    this.formData.employeementHisForm = '';
    this.formData.educationQualifyForm = '';
    this.formData.keyForm = '';
    this.formData.certificationForm = '';
    this.formData.editLangForm = '';
  }

  setShowElement() {
    this.showElement['profileForm'] = true;
    this.showElement['personal_infoForm'] = true;
    this.showElement['desiredJobForm'] = true;
  }


  checkFromDate(c: FormControl) {
    if (!c.root && !c.root.get('to_date')) {
      return null;
    }

    if (c.value &&  Number(new Date(c.value))  >  Number(new Date(c.root.get('to_date').value)) ) {
      return { error: true };
    }
  }

  checkToDate(c: FormControl) {
    if (!c.root && !c.root.get('from_date')) {
      return null;
    }
    if (c.value && Number(new Date(c.value)) < Number(new Date(c.root.get('from_date').value)) ) {
      return { error: true };
    }
  }
  

  checkMinMaxSal(c: FormControl) {
    if (!c.root && !c.root.get('min_salary')) {
      return null;
    }
    if (c.value && c.value < c.root.get('min_salary').value) {
      return { error_min_max: true };
    }
  }

  showAllEmployeementHis(){
    this.show_all_his = true;
    this.employment_history = this.emp_his_data;
  }

  getSubData(parent,type){
   if(type =='under-graduate'){
    let subData = this.storeMultiSelect.undergraduate_sub_value.filter(obj=>obj.parent_name == parent);
    return subData.length ? subData.length: '';
    }

  }

  getChangeQualification(e){
    let totalDeg = this.post_under_graduates[""];
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1); 
        this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));      
      }
    }
    if(this.storeMultiSelect.undergradute_value.length > 0){
      this.educationQualifyForm.get('degree_parent').setValue(this.storeMultiSelect.undergradute_value[0])
    }else{
      this.educationQualifyForm.get('degree_parent').setValue('')
    }
    if(this.storeMultiSelect.undergraduate_sub_value.length > 0){
      this.educationQualifyForm.get('degree_id').setValue(this.storeMultiSelect.undergraduate_sub_value[0]['value'])
    }else{
      this.educationQualifyForm.get('degree_id').setValue('')
    }
    
    if(this.educationQualifyForm.get('degree_parent').value && totalDeg.find(obj=>obj._id == this.educationQualifyForm.get('degree_parent').value)){
      this.educationQualifyForm.get('degree_id').setValue(this.educationQualifyForm.get('degree_parent').value)
    }

  }

  getSubChange(e){
    let event : any= e;
    if (e.event) {
      let index = e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index == -1) {
        e.storedSubValue.push({'parent_name' :e.parent_name, 'value': e.value});
      }
    } else {
      let index =  e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index != -1) {
        e.storedSubValue.splice(index, 1);
      }
    }  
    if(this.storeMultiSelect.undergraduate_sub_value.length > 0){
      this.educationQualifyForm.get('degree_id').setValue(this.storeMultiSelect.undergraduate_sub_value[0]['value'])
    }else{
      this.educationQualifyForm.get('degree_id').setValue('')
    }
  } 

  removeQualification(indx){
      let nameUnd = this.storeMultiSelect.undergraduate_name[indx];
      this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(nameUnd));         

      this.storeMultiSelect.undergradute_value.splice(indx, 1);
      this.storeMultiSelect.undergraduate_name.splice(indx, 1);

      this.educationQualifyForm.get('degree_parent').setValue('')
      this.educationQualifyForm.get('degree_id').setValue('')

  }

}
