import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchJobListingComponent } from './search-job-listing.component';

describe('SearchJobListingComponent', () => {
  let component: SearchJobListingComponent;
  let fixture: ComponentFixture<SearchJobListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchJobListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchJobListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
