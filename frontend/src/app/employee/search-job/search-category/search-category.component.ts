import { Component, OnInit,HostListener, OnDestroy } from '@angular/core';
import { PlatformLocation } from '@angular/common'
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {Global} from '../../../global';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
declare var $ :any;


@Component({
  selector: 'app-search-category',
  templateUrl: './search-category.component.html',
  styleUrls: ['./search-category.component.css']
})
export class SearchCategoryComponent implements OnInit, OnDestroy {

	@HostListener("window:scroll", ["$event.target"])
	scrollHandler(elem) {
	   var hT = $('#add_moreDiv').offset().top,
		 hH = $('#add_moreDiv').outerHeight(),
		 wH = $(window).height(),
		 wS = $(window).scrollTop();
	 if (wS >= (hT+hH-wH) && !this.isSearchByAlpha){
     if(this.category !=='country'){
       this.loadMoreData();
     }
		 
	 }
    if($(window).scrollTop() <= 200){
      window.location.hash ='';
      this.isSearchByAlpha = false;
    }

	}
  category: string;
  resultList: any=[];
  categoryType: string;
  searchInput: string='';
  alphaArr:any;
  isSearchByAlpha: boolean;
  totalDataCount : number;
  subscription:any;
  constructor(private commonService: CommonService, private router: Router, private location: PlatformLocation,
  	private activeRoute: ActivatedRoute, private global: Global) {
	this.isSearchByAlpha = false;		  
  	this.subscription = router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
		  this.category = this.activeRoute.snapshot.paramMap.get("cat");
  		if(!this.isSearchByAlpha && e.url.includes('search-job')){
			this.asyncInit(this.category);
		}
  		
  	});


   }

  ngOnInit() {
	window.scrollTo(0, 0)
   }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

  asyncInit(cat){
	let AlphaStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    this.alphaArr = AlphaStr.split('');    
  	this.commonService.getAll('/api/employee/job-search?cat='+ cat+'&search=' + this.searchInput + '&limit=200')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			if(data.status ==200){
				  this.resultList = data.data;
				  this.totalDataCount = data.total_count;
				  this.categoryType = this.getType(cat);
              if(this.activeRoute.snapshot.queryParams && this.activeRoute.snapshot.queryParams.alpha){              
               setTimeout(()=>{    
                $('html,body').animate({
                    scrollTop: $("#" + this.activeRoute.snapshot.queryParams.alpha).offset().top -50
                  },
                    'slow');
               },500);
            }
  			}
  			
  		}, (error)=>{});
  }

  getType(cat){
  	let value='';
  	switch (cat) {
  		case "country":
  			value = 'Country';
  			break;
  		case "city":
  			value = 'City';
  			break;
  		case "industry":
  			value = 'Industry';
  			break;
  		case "role":
  			value = 'Job Role';
			  break;
		case "department":
			value = 'Department';
			break;		  
  		default:
  			// code...
  			break;
  	}

  	return value;

  }


  searchdetail(id){
  	this.router.navigate(['/search-job/detail'],{queryParams:{cat: this.category, cat_id: id}});
  }

  getAlphaid(name,i){ 
    let str = name.trim().charAt(0);
    return str;

  }



  dataShowByAlpha(id){
    let pos = window.pageYOffset;
    let setTop = pos - 90

    this.isSearchByAlpha = true;
      let cat = this.category;
      if(this.resultList.length ===this.totalDataCount && this.searchInput ==''){
        $('html,body').animate({
        scrollTop: $("#" + id).offset().top -50
      },'slow');

      }else{
       this.searchInput = '';
       this.commonService.getAll('/api/employee/job-search?cat='+ cat+ '&searchById='+ id)
      .subscribe((data)=>{
        if(data.status ==200){
          this.resultList = data.data;
          this.totalDataCount = data.total_count;
          this.categoryType = this.getType(cat);  
            setTimeout(()=>{    
              $('html,body').animate({
                scrollTop: $("#" + id).offset().top -50
              },'slow');
             }, 500);
      
        }       
      }, (error)=>{});        
      }


  }

  loadMoreData(){
    if(this.resultList.length < this.totalDataCount){
    let limit = (this.resultList.length + 100);
    let cat = this.category;
    let search = this.searchInput;
    this.commonService.getAll('/api/employee/job-search?cat='+ cat+ '&search='+ search +'&limit=' + limit)
      .subscribe((data)=>{
        if(data.status ==200){
          this.resultList = data.data;
          this.totalDataCount = data.total_count;
          this.categoryType = this.getType(cat);
        
        }
        
      }, (error)=>{}); 
    }  
  }

  searchData(data){
    this.isSearchByAlpha = false;
    let limit = (this.resultList.length + 100);
    let cat = this.category;
    let search = this.searchInput;
    this.commonService.getAll('/api/employee/job-search?cat='+ cat+ '&search='+ search +'&limit=' + limit)
      .subscribe((data)=>{
        if(data.status ==200){
          this.resultList = data.data;
          this.totalDataCount = data.total_count;
          this.categoryType = this.getType(cat);
        
        }
        
      }, (error)=>{}); 
  }

    scroleToTop(){
    //window.scroll(0,0);
    $('html, body').animate({
          scrollTop:0
    }, 800, function(){ 
    });
  }

  getCategoryReplace(category){
    if(category == 'department'){
      return 'search_department';
    }else if(category == 'country'){
      return 'search_location';
    }else if(category == 'city'){
      return 'search_location';
    }else if(category == 'industry'){
      return 'search_industry';
    }
  }

  goToSearchJobList(category, cat){
    if(category == 'department'){
      return this.router.navigate(['/search-job-list'],{queryParams:{'search_department': cat.name}}); 
    }else if(category == 'country'){
      return this.router.navigate(['/search-job-list'],{queryParams:{'search_location': cat.name}});
    }else if(category == 'city'){
      return this.router.navigate(['/search-job-list'],{queryParams:{'search_location': cat.name}});
    }else if(category == 'industry'){
      return this.router.navigate(['/search-job-list'],{queryParams:{'search_industry': cat.name}});
    }
  }

}
