import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { CommonService } from '../../../services/common.service';
import {AuthService} from '../../../services/auth.service';
import {MessageService} from '../../../services/message.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-search-detail',
  templateUrl: './search-detail.component.html',
  styleUrls: ['./search-detail.component.css']
})
export class SearchDetailComponent implements OnInit {

  routeParms: any;
  job_data: any=[];
  countries: any=[];
  cities: any=[];
  industries: any=[];
  job_roles: any=[];
  countryModel: any={};
  cityModel: any={};
  industryModel: any={};
  jobRoleModel: any={};
  experiance_data: any=[];
  salary_data: any=[];
  currency_data: any=[];
  departments: any=[];
  departmentModel: any={};
  isLoading: boolean = false
  searchForm: any={
  	cat: '',
  	cat_id:'',
  	country_list:[],
  	city_list:[],
  	industry_list:[],
  	role_list:[],
    department_list:[],
  	experince:{
  		min_exp:'',
  		max_exp:''
  	},
  	salary:{
  		currency:'',
  		min_salary:'',
  		max_salary:''
  	},
    pageLimit:10,
    pageOffset:0,
  };
  total_job_count:number;
  shareUrl: string;
  others_job:[]
  otherJobCount:Number;

  constructor(private route: ActivatedRoute, private commonService: CommonService, private messageservice: MessageService,private authService: AuthService) {
  	 window.scrollTo(0, 0);
     this.route.queryParams.subscribe(params=>{
  	 	this.routeParms = params;
  	 	//console.log(params.cat);
  	 	this.searchForm.cat = params.cat;
  	 	this.searchForm.cat_id = params.cat_id;
      this.shareUrl = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + '/single-job-details/';
      this.asyncInit(params.cat, params.cat_id);
  	 });

  }

  ngOnInit() {
    this.commonService.getCommonData.subscribe(data=>{
      if(data && data.status ==200){
        this.currency_data = data['all_currency'];
        this.experiance_data =data['allExprience'];
      }
    });
	}

  asyncInit(cat, cat_id){
     this.commonService.getDataWithPost('/api/employee/job-search/multiple-jobs', this.searchForm)
  	//this.commonService.getAll('/api/employee/job-search/get-initial-data?cat='+cat + '&cat_id=' + cat_id)
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.job_data = data.data;
  			this.countries = data.countries;
  			this.cities = data.cities;
  			this.industries = data.industries;
        this.departments = data.departments;
  			this.job_roles = data.roles;
        //this.experiance_data = data.other_master.filter(data=> data.entity =='experience');
        //this.currency_data = data.other_master.filter(data=> data.entity =='currency');
        this.salary_data = data.other_master.filter(data=> data.entity =='salary');
        //this.total_job_count = data.total_job_count;
        this.others_job = data.totOtherJobCountArr;
        let interCount =  data.totOtherJobCountArr.length > 0 ?data.totOtherJobCountArr.map(obj=>obj.international_job_count).reduce((a,b)=> a+b) :0;
        let walkingCount = data.totOtherJobCountArr.length > 0 ? data.totOtherJobCountArr.map(obj=>obj.walking_job_count).reduce((a,b)=> a+b):0;
        if(data.countries.length > 0){
          this.total_job_count = data.countries.map(obj =>obj.job_count).reduce((a,b)=> a+b);
        }else{
          this.total_job_count = 0;
        }
        this.otherJobCount = Number(interCount) + Number(walkingCount);
        this.initialSetSearch(cat, cat_id);
  		}, (error)=>{});
  }

  getsearchData(){
  	//this.commonService.getDataWithPost('/api/employee/job-search/get-jobs', this.searchForm)
    this.commonService.getDataWithPost('/api/employee/job-search/multiple-jobs', this.searchForm)
  		.subscribe((data)=>{
        if(data.status ==200){
          this.job_data = data.data;
          this.countries = data.countries;
          this.cities = data.cities;
          this.industries = data.industries;
          this.departments = data.departments;
          //this.total_job_count = data.total_job_count;
          this.others_job = data.totOtherJobCountArr;
          if(data.countries.length > 0){
            this.total_job_count = data.countries.map(obj =>obj.job_count).reduce((a,b)=> a+b);
          }else{
            this.total_job_count = 0;
          }
        }
  		}, (error)=>{});
  }

    loadMoreData(){
    //this.job_data.length >= 10 || 

    if((this.job_data.length < this.total_job_count) && !this.isLoading){
      this.searchForm.pageOffset = this.job_data.length;
      this.isLoading = true;
      this.commonService.getDataWithPost('/api/employee/job-search/multiple-jobs', this.searchForm)
      .subscribe((data)=>{
        if(data.status ==200){
          this.job_data = this.job_data.concat(data.data);
          this.countries = data.countries;
          this.cities = data.cities;
          this.industries = data.industries;
          this.departments = data.departments;
          this.others_job = data.totOtherJobCountArr;
          if(data.countries.length > 0){
            this.total_job_count = data.countries.map(obj =>obj.job_count).reduce((a,b)=> a+b);
          }else{
            this.total_job_count = 0;
          }
          this.isLoading = false
        }
      }, (error)=>{});
    }
    
  }

  initialSetSearch(cat, cat_id){
      if(cat=='country'){
      this.countryModel[cat_id] =true;  
      let count_list = Object.entries(this.countryModel);
      let countArr =[];
      count_list.forEach((value, i)=>{if(value[1] ==true){countArr.push(value[0]);}});

      this.searchForm.country_list = countArr;
    }else if(cat=='city'){
      this.cityModel[cat_id] = true;
      let city_list = Object.entries(this.cityModel);
      let cityArr =[];
      city_list.forEach((value, i)=>{if(value[1] ==true){cityArr.push(value[0]);}});
      this.searchForm.city_list = cityArr;
    }else if(cat=='industry'){
      this.industryModel[cat_id] = true;
      let indus_list = Object.entries(this.industryModel);
      let indusArr =[];
      indus_list.forEach((value, i)=>{if(value[1] ==true){indusArr.push(value[0]);}});
      this.searchForm.industry_list = indusArr;
    }else if(cat=='department'){
      this.departmentModel[cat_id] =true;
      let department_list = Object.entries(this.departmentModel);
      let departmentArr =[];
      department_list.forEach((value, i)=>{if(value[1] ==true){departmentArr.push(value[0]);}});
      this.searchForm.department_list = departmentArr;
    }else if(cat=='role'){
      this.jobRoleModel[cat_id] =true;
      let role_list = Object.entries(this.jobRoleModel);
      let roleArr =[];
      role_list.forEach((value, i)=>{if(value[1] ==true){roleArr.push(value[0]);}});
      this.searchForm.role_list = roleArr;
    }
  }

  setsearchValue(event, type){
  	if(type=='country'){
  		let count_list = Object.entries(this.countryModel);
  		let countArr =[];
  		count_list.forEach((value, i)=>{if(value[1] ==true){countArr.push(value[0]);}});
  		this.searchForm.country_list = countArr;
  	}else if(type=='city'){
  		let city_list = Object.entries(this.cityModel);
  		let cityArr =[];
  		city_list.forEach((value, i)=>{if(value[1] ==true){cityArr.push(value[0]);}});
  		this.searchForm.city_list = cityArr;
  	}else if(type=='industry'){
  		let indus_list = Object.entries(this.industryModel);
  		let indusArr =[];
  		indus_list.forEach((value, i)=>{if(value[1] ==true){indusArr.push(value[0]);}});
  		this.searchForm.industry_list = indusArr;
  	}else if(type=='department'){
      let department_list = Object.entries(this.departmentModel);
      let departmentArr =[];
      department_list.forEach((value, i)=>{if(value[1] ==true){departmentArr.push(value[0]);}});
      this.searchForm.department_list = departmentArr;
    } else if(type=='role'){
  		let role_list = Object.entries(this.jobRoleModel);
  		let roleArr =[];
  		role_list.forEach((value, i)=>{if(value[1] ==true){roleArr.push(value[0]);}});
  		this.searchForm.role_list = roleArr;
  	}

    this.getsearchData();
    

  }

  toggleAllSelect(event, param){
    if(event.target.checked){
      switch (param) {
        case "country":
          this.countries.map(country=> this.countryModel[country.id]=true);
          break;
        case "city":
          this.cities.map(city=> this.cityModel[city.id]=true);
          break;
        case "industry":
          this.industries.map(industry=> this.industryModel[industry.id]=true);
          break;
        case "role":
          this.job_roles.map(role=> this.jobRoleModel[role.id]=true);
          break;
        default:
          // code...
          break;
      }
    }else{
      switch (param) {
        case "country":
          this.countries.map(country=> this.countryModel[country.id]=false);
          break;
        case "city":
          this.cities.map(city=> this.cityModel[city.id]=false);
          break;
        case "industry":
          this.industries.map(industry=> this.industryModel[industry.id]=false);
          break;
        case "role":
          this.job_roles.map(role=> this.jobRoleModel[role.id]=false);
          break;
        default:
          // code...
          break;
      }
    }
  }

  getInterNationalJobCount =(country)=>{
    const dataCount =  this.others_job.find(obj=>obj['country_name'] == country);
     return dataCount? Number(dataCount['international_job_count']):0
  }

  getWalkingJobCount =(country)=>{
    const dataCount =  this.others_job.find(obj=>obj['country_name'] == country);
    return dataCount? Number(dataCount['walking_job_count']):0
  }

  getCountJob =(total_job_count,otherJobCount)=>{
    return Number(total_job_count) + Number(otherJobCount);
  }


  applyJob(id, index){
    this.commonService.create('/api/employee/job-search', {job_id:id})
        .subscribe((data)=>{
          if(data.status ==200){
            this.messageservice.success(data.status_text);
            this.job_data.splice(index,1);
          }else if(data.status ==422){
            this.messageservice.error(data.status_text);
          }
        }, (error)=>{});
  }

  saveJob(id, index){
      this.commonService.create('/api/employee/job-search/save-job', {job_id:id})
        .subscribe((data)=>{
          if(data.status ==200){
            this.messageservice.success(data.status_text);
            let indexdata = this.job_data.findIndex(obj=>obj._id == id);
            this.job_data[indexdata].is_saved =1;
            //this.job_data.splice(index,1);
          }
        }, (error)=>{});
  }

  removeSaveJob(id){
    this.commonService.getAll('/api/employee/remove/save-job/' + id)
        .subscribe(data=>{
          if(data.status ==200){
            let indexdata = this.job_data.findIndex(obj=>obj._id == id);
            this.job_data[indexdata].is_saved =0;
          }
        },error=>{});
  }

}
