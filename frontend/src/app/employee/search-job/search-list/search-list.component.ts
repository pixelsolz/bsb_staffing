import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Meta, Title } from '@angular/platform-browser';
declare var $ :any;
declare var Swal: any;
import * as _ from 'lodash'; 

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit, OnDestroy {
 @ViewChild('job_listing_wrapper') jobListingWraper:any;
 @HostListener('document:click', ['$event'])
  
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if($(event.target).hasClass('auto_comp_search')){
      if($(event.target).attr('id') =='job_title_search'){
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='industry_search'){
        this.show_jb_title_srh = false;
        this.show_city_comp_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='country_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
        this.show_department_srh = false;
      }else if($(event.target).attr('id') =='department_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
      }
    }else{
      this.show_jb_title_srh = false;
      this.show_industry_srh = false;
      this.show_city_comp_srh = false;
      this.adv_show_jb_title_srh = false;
      this.show_department_srh = false;
    }
  }

  logingUser = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')) :'';
  advSearchForm: FormGroup;
  industrty_data: any =[];
  department_data: any =[];
  allCountries: any;
  all_industries: any;
  cities:any;
  industries:any;
  departments:any;
  job_roles: any;
  emplymentFor: any;
  experiance: any;
  salaries: any;
  tot_job_count: any;
  industriesDiv: boolean;
  storeMultiSelect :any ={
    industries_value:[],
    industries_name:[],
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
    department_value:[],
    department_name:[]
  };

  search_job_title: any;
  search_industry: any;
  search_department: any;
  search_location: any;
  advance_search: any;
  src: any;

  searchJobsResult: any =[];

  show_jb_title_srh: boolean;
  title_job_data: any=[];
  show_city_comp_srh: boolean;
  city_country_data: any =[];
  show_industry_srh: boolean;
  show_department_srh: boolean;
  searchData = {
    title_comp:{
      id:'',
      name:'',
      table_name:''
    },
    city_country:{
      id:'',
      name:'',
      table_name:''
    },
    industry:{
      id:'',
      name:'',
      table_name:''
    },
    department:{
      id:'',
      name:'',
      table_name:''
    }
  };

  advCallCount =0;

  job_title_model: any;
  insudtry_model: any;
  department_model: any;
  country_loc_model: any;
  showloader:boolean;
  all_currency:any;
  internationalWalking: any=[];
  nationalWalking: any=[];
  internationalJobs: any=[];
  job_title_model_array: any=[];
  insudtry_model_array: any=[];
  country_model_array: any=[]; 
  department_model_array: any=[];
  advanced_job_title_model_array: any=[];
  adv_show_jb_title_srh: boolean=false;
  countryArray: any=[];
  jobResults_count: number;
  international_jobs_count: number;
  walking_int_count: number;
  walking_loc_count: number;

  isloggedIn: boolean;
  search_employer_jobs : string;
  isloaded :boolean=false;
  count_job_show :number;
  international_count_job_show : number;
  count_national_walkin_show :number;
  count_international_walkin_show :number;
  countryDiv: boolean =false;
  cityDiv: boolean=false;
  cityArray: any=[];
  departmentDiv: boolean= false;
  departmentData: any;
  all_departments: any=[];
  apiCall: number =0;
  //last_item_id:string;
  maxExpData: any=[];
  before_load_more_last_index:number = -1;
  subscription: any; 
  globalJobSave:any={
    jobs:[],
    internationJobs:[],
    internationWalking:[],
    nationalWalking:[]
  }
  not_fond_text: string;
  loginEmpProfile:any;

  selected_job: any={}
  selected_job_type:string;
  questionAnswers:any={};
  selectedindex: number;
  
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute, private messageService: MessageService,private http: HttpClient, private eRef: ElementRef, private title: Title,private meta: Meta) { 
    this.isloggedIn = localStorage.getItem('token')? true: false;
    this.asyncInit();
    this.showloader = false;
    this.industriesDiv = false;
    this.show_city_comp_srh = false;
    this.subscription = router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
      if(e.url.includes('search-job-list')){   
      //this.isloaded = false;   
      this.getJobBysearch();
      }
    });

    //this.getJobBysearch();
    
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.advSearchForm= this.fb.group({
      job_title:'',
      employement_for:'',
      country_id:'',
      city_id:'',
      industry_id:'',
      department_id:'',
      min_experiance:'',
      max_experiance:'',
      salary_type:'',
      currency:'',
      min_monthly_salary:'',
      max_monthly_salary:'',
    });
    
     this.advSearchForm.get('country_id').valueChanges.subscribe(value=>{
      let countryCode = this.allCountries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
    });

     
    /*this.advSearchForm.get('min_experiance').valueChanges.subscribe(value=>{
      let dataValue  = this.experiance.find(obj=>obj._id == value);
      this.maxExpData = this.experiance.filter(obj=> Number(dataValue.value) < Number(obj.value));
    });*/
  }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        localStorage.removeItem('advSearch')
    }


  resetAdvanceSearch(){
    this.advSearchForm.get('job_title').setValue('');
    this.advSearchForm.get('employement_for').setValue('')
    this.advSearchForm.get('country_id').setValue('');
    this.advSearchForm.get('city_id').setValue('');
    this.advSearchForm.get('industry_id').setValue('');
    this.advSearchForm.get('department_id').setValue('')
    this.advSearchForm.get('min_experiance').setValue('');
    this.advSearchForm.get('max_experiance').setValue('');  
    this.advSearchForm.get('salary_type').setValue('');
    this.advSearchForm.get('currency').setValue('')
    this.advSearchForm.get('min_monthly_salary').setValue('');
    this.advSearchForm.get('max_monthly_salary').setValue(''); 
    this.storeMultiSelect.industries_value.length =0;
    this.storeMultiSelect.industries_name.length =0;
    this.storeMultiSelect.country_value.length =0;
    this.storeMultiSelect.country_name.length =0;
    this.storeMultiSelect.city_value.length =0;
    this.storeMultiSelect.city_name.length =0;
    this.storeMultiSelect.department_value.length =0;
    this.storeMultiSelect.department_name.length =0;
  }


    setAndCheckParams(){
      if(this.activeRoute.snapshot.queryParams){
        localStorage.setItem('search-queries', JSON.stringify(this.activeRoute.snapshot.queryParams));
      }
      this.search_job_title = this.activeRoute.snapshot.queryParamMap.get("search_title") ? this.activeRoute.snapshot.queryParamMap.get("search_title") :'';
      this.search_industry = this.activeRoute.snapshot.queryParamMap.get("search_industry") ? this.activeRoute.snapshot.queryParamMap.get("search_industry"):'';
      this.search_industry ? this.insudtry_model_array = this.search_industry:'';
      this.search_department = this.activeRoute.snapshot.queryParamMap.get("search_department") ? this.activeRoute.snapshot.queryParamMap.get("search_department"):'';   
      
      this.search_location = this.activeRoute.snapshot.queryParamMap.get("search_location") ? this.activeRoute.snapshot.queryParamMap.get("search_location"):'';
      this.search_employer_jobs = this.activeRoute.snapshot.queryParamMap.get("searchemployerjobs") ? this.activeRoute.snapshot.queryParamMap.get("searchemployerjobs"):'';
      this.src = this.activeRoute.snapshot.queryParamMap.get("src") ? this.activeRoute.snapshot.queryParamMap.get("src"):'';
      this.advance_search = localStorage.getItem('advSearch')? JSON.parse(localStorage.getItem('advSearch')):'';

      this.job_title_model = this.search_job_title ? this.search_job_title +',':''; 
      this.search_job_title ? this.job_title_model_array = this.search_job_title.split(',') :[];
      this.insudtry_model = this.search_industry ? this.search_industry +',':''; 
      this.insudtry_model ? this.insudtry_model_array =this.insudtry_model.split(','):[];
      this.department_model = this.search_department; 
      this.country_loc_model = this.search_location ? this.search_location +',':'';  
      this.country_loc_model ? this.country_model_array = this.search_location.split(',') :[];
  }

  asyncInit(){
      this.commonService.getAll('/api/employee-profile').subscribe(res=>{
        if (res.status == 200) {
          this.loginEmpProfile = res.data;
        }
      });

      this.commonService.getCountryData.subscribe(data=>{
        if(data && data.status ==200){
          this.allCountries = data['countries'];
          this.countryArray = {"":data['countries']};
          this.industries = data['industries'];
          this.all_industries = data['all_industries'];
        }
      });

      this.commonService.getCityData.subscribe(data=>{
          if(data && data.status ==200){
            this.cities = data['cities'];
          }
      });

        this.commonService.getCommonData.subscribe(data=>{
          this.commonService.callFooterMenu(1);
          if(data && data.status ==200){
            this.job_roles = data['job_roles'];
            let otherData = data['other_mst'];
            this.emplymentFor = otherData.filter((data) => data.entity == 'emplyment_for');
            this.experiance = data['allExprience'];
            this.salaries = otherData.filter((data) => data.entity == 'salary');
            this.departments = data['all_parent_department'];
            this.departmentData = data['all_department'];
             this.all_departments = data['departments'];
            this.all_currency = data['all_currency'];      
          }

          if(this.activeRoute.snapshot.queryParamMap.get("search_location") && this.allCountries !=undefined ){
           let seodata=[];
            seodata = this.allCountries.filter((data)=>data.name ==this.activeRoute.snapshot.queryParamMap.get("search_location"));
            if(seodata.length > 0){
              this.title.setTitle(seodata[0].seo_title ? seodata[0].seo_title : 'Job Vacancies | Recruitment | Job Search- bsbstaffing.com');
             this.meta.updateTag({ name: 'description', content: seodata[0].seo_description ? seodata[0].seo_description : 'Search for Job Vacancies across the World at bsbstaffing.com, Global Job Portal. Search and Discover Jobs across Top Companies. Apply Now!'});
             this.meta.updateTag({ name: 'keywords', content: seodata[0].seo_keywords ? seodata[0].seo_keywords :'Jobs,Job Vacancies' });
            } else {
              seodata = this.cities.filter((data)=>data.name ==this.activeRoute.snapshot.queryParamMap.get("search_location"));
             this.title.setTitle(seodata[0].seo_title ? seodata[0].seo_title : 'Job Vacancies | Recruitment | Job Search- bsbstaffing.com');
             this.meta.updateTag({ name: 'description', content: seodata[0].seo_description ? seodata[0].seo_description : 'Search for Job Vacancies across the World at bsbstaffing.com, Global Job Portal. Search and Discover Jobs across Top Companies. Apply Now!'});
             this.meta.updateTag({ name: 'keywords', content: seodata[0].seo_keywords ? seodata[0].seo_keywords :'Jobs,Job Vacancies' });
            }
         }else if(this.activeRoute.snapshot.queryParamMap.get("search_industry") && this.all_industries !=undefined){
           //console.log(this.all_industries);
            let seodata=[];
            seodata = this.all_industries.filter((data)=>data.name ==this.activeRoute.snapshot.queryParamMap.get("search_industry"));
            this.title.setTitle(seodata[0].seo_title ? seodata[0].seo_title : 'Job Vacancies | Recruitment | Job Search- bsbstaffing.com');
            this.meta.updateTag({ name: 'description', content: seodata[0].seo_description ? seodata[0].seo_description : 'Search for Job Vacancies across the World at bsbstaffing.com, Global Job Portal. Search and Discover Jobs across Top Companies. Apply Now!'});
            this.meta.updateTag({ name: 'keywords', content: seodata[0].seo_keywords ? seodata[0].seo_keywords :'Jobs,Job Vacancies' });
         }
        });

       
 
  }

    getJobBysearch(){
    if(this.apiCall ==0){
      this.apiCall =1;
      this.setAndCheckParams();
      this.showloader = true;
      if(this.src == 'advance_search' && this.advance_search != ''){
        let data={formData:this.advance_search};
       this.commonService.create('/api/employee/get-job-by-adv-search',data)
       .subscribe((data)=>{         
         this.searchJobsResult = data.jobResults;
        this.internationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==2);
        this.nationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==1);
        this.internationalJobs = data.international_jobs;
        this.globalJobSave.jobs = this.searchJobsResult;
        this.globalJobSave.internationJobs = this.internationalJobs;
        this.globalJobSave.internationWalking = this.internationalWalking;
        this.globalJobSave.nationalWalking = this.nationalWalking;
        this.commonService.callJobsData(this.globalJobSave);

        this.count_job_show =12;
        this.count_national_walkin_show =12;
        this.count_international_walkin_show =12;
        this.international_count_job_show = 12;
        this.showloader = false;
        this.tot_job_count = data.job_count;
        this.jobResults_count = data.jobResults_count;
        this.international_jobs_count = data.international_jobs_count;
        this.walking_int_count = data.inter_walking_count;
        this.walking_loc_count = data.local_walking_count;
        this.isloaded = true;
        this.notFoundText(this.searchJobsResult, this.internationalJobs)
       },(error)=>{});
     } else { 
       var url ='';
       if(this.activeRoute.snapshot.queryParams && this.activeRoute.snapshot.queryParams['job_range']){
         url = '/api/employee/get-job-by-search?user_country='+this.activeRoute.snapshot.queryParams.user_country + '&job_range='+ this.activeRoute.snapshot.queryParams['job_range'] +'&userId=' + this.logingUser.id;
       }else{
         url = '/api/employee/get-job-by-search?search_job_title='+this.search_job_title+'&search_industry='+this.search_industry+'&search_department='+this.search_department+'&search_location='+this.search_location+'&search_employer_jobs='+this.search_employer_jobs + '&userId=' + this.logingUser.id
       }       
      this.commonService.getAll(url).subscribe((data)=>{
        this.searchJobsResult = data.jobResults;
        this.internationalJobs = data.international_jobs;
        this.internationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==2);
        this.nationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==1);
        this.globalJobSave.jobs = this.searchJobsResult;
        this.globalJobSave.internationJobs = this.internationalJobs;
        this.globalJobSave.internationWalking = this.internationalWalking;
        this.globalJobSave.nationalWalking = this.nationalWalking;
        this.commonService.callJobsData(this.globalJobSave);
        this.count_job_show =12;
        this.count_national_walkin_show =12;
        this.count_international_walkin_show =12;
        this.international_count_job_show = 12;
        this.showloader = false;
        this.tot_job_count = data.job_count;
        this.jobResults_count = data.jobResults_count;
        this.international_jobs_count = data.international_jobs_count;
        this.walking_int_count = data.inter_walking_count;
        this.walking_loc_count = data.local_walking_count;
        this.isloaded = true;
        this.notFoundText(this.searchJobsResult, this.internationalJobs)
       },(error)=>{});
     }
     setTimeout (() => {
         this.apiCall =0;
      }, 2000);
    }

  }

  notFoundText =(data, interjobs)=>{
    if(data.length == 0 && interjobs.length == 0){
      if(this.job_title_model !=''){
        this.not_fond_text = 'As per this job title/Keyword no job now we have, hope soon will be available. Best of luck for your next Search.';
        return
      }
      if(this.insudtry_model !=''){
         this.not_fond_text = 'As per this Industry no job now we have, hope soon will be available. Best of luck for your next Search.';
        return
      }
      if(this.department_model!=''){
          this.not_fond_text = 'As per this Department no job now we have, hope soon will be available. Best of luck for your next Search.';
        return       
      }
      if(this.country_loc_model!=''){
        this.not_fond_text = 'As per this country/city no job now we have, hope soon will be available. Best of luck for your next Search.';
        return 
      }
    }
  }

  getScrollingElement(): Element {
    return document.scrollingElement || document.documentElement;
  }

  addMoreSearchJob(){
    //var last_arrIndex = this.searchJobsResult[this.searchJobsResult.length - 1];
    //this.last_item_id = last_arrIndex._id;
    this.before_load_more_last_index = [...this.searchJobsResult].length - 1;
    this.showloader = true;
    let limit = 12 + this.searchJobsResult.length;
    let offset =  this.searchJobsResult.length;
      if(this.src == 'advance_search' && this.advance_search != ''){
        let data={limit:limit, skip:offset,formData:this.advance_search,job_type:'job-post'};
     this.commonService.create('/api/employee/get-job-by-adv-search',data)
     .subscribe((data)=>{
       this.showloader = false;
      if(data.jobResults && data.jobResults.length){
         data.jobResults.forEach(value=>{this.searchJobsResult.push(value)});
         this.commonService.callJobsData(this.globalJobSave);
         if(this.jobListingWraper)
         {
           let previousLastElement = this.jobListingWraper.nativeElement.querySelectorAll('.job_listing_item');
           try {previousLastElement = previousLastElement[this.before_load_more_last_index];} catch(e) {}
           //console.log(previousLastElement.offsetTop);
           setTimeout(() => {
             window.scrollTo(0, previousLastElement.offsetTop - 75);
           }, 400);

         }
      } 
      this.count_job_show = this.count_job_show + 12;
     },(error)=>{});
   } else {
     this.commonService.getAll('/api/employee/get-job-by-search?search_job_title='+this.search_job_title+'&search_industry='+this.search_industry+'&search_department='+this.search_department+'&search_location='+this.search_location+'&limit=' + Number(limit) + '&skip=' + offset + '&job_type=job-post')
     .subscribe((data)=>{
       this.showloader = false;
       if(data.jobResults && data.jobResults.length){
         data.jobResults.forEach(value=>{this.searchJobsResult.push(value)});
         this.commonService.callJobsData(this.globalJobSave);
         if(this.jobListingWraper)
         {
           let previousLastElement = this.jobListingWraper.nativeElement.querySelectorAll('.job_listing_item');
           try {previousLastElement = previousLastElement[this.before_load_more_last_index];} catch(e) {}
           //console.log(previousLastElement.offsetTop);
           setTimeout(() => {
             window.scrollTo(0, previousLastElement.offsetTop - 75);
           }, 400);

         }
         
       }       
      this.count_job_show = this.count_job_show + 12;
     },(error)=>{});
   }
  }

  addMoreSearchInternational(){
    this.showloader = true;
    let limit = 12 + this.internationalJobs.length;
    let offset =  this.internationalJobs.length;
      if(this.src == 'advance_search' && this.advance_search != ''){
        let data={limit:limit, skip:offset, formData:this.advance_search, job_type:'international-job'};
     this.commonService.create('/api/employee/get-job-by-adv-search',data)
     .subscribe((data)=>{
       this.showloader = false;
       if(data.international_jobs && data.international_jobs.length){
         data.international_jobs.forEach(value=>{this.internationalJobs = value});
         this.commonService.callJobsData(this.globalJobSave);
       }      
      this.international_count_job_show = this.international_count_job_show + 12;
     },(error)=>{});
   } else {
     this.commonService.getAll('/api/employee/get-job-by-search?search_job_title='+this.search_job_title+'&search_industry='+this.search_industry+'&search_department='+this.search_department+'&search_location='+this.search_location+'&limit=' + Number(limit) + '&skip=' + offset + '&job_type=international-job')
     .subscribe((data)=>{
       this.showloader = false;
       if(data.international_jobs && data.international_jobs.length){
         data.international_jobs.forEach(value=>{this.internationalJobs = value});
         this.commonService.callJobsData(this.globalJobSave);
       } 
      this.international_count_job_show = this.international_count_job_show + 12;
     },(error)=>{});
   }   
  }

  addMoreSearchLocalWalking(){
    this.showloader = true;
    let limit = 12 + this.nationalWalking.length;
    let offset =  this.nationalWalking.length;
      if(this.src == 'advance_search' && this.advance_search != ''){
        let data={limit:limit, skip:offset,formData:this.advance_search, job_type:'local-walking'};
     this.commonService.create('/api/employee/get-job-by-adv-search',data)
     .subscribe((data)=>{
       this.showloader = false;
       if(data.walking_interviews && data.walking_interviews.length){
         data.walking_interviews.forEach(value=>{this.nationalWalking.push(value)});
         this.commonService.callJobsData(this.globalJobSave);
       }
       //this.nationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==1);
       this.count_national_walkin_show = this.count_national_walkin_show + 12;
     },(error)=>{});
   } else {
     this.commonService.getAll('/api/employee/get-job-by-search?search_job_title='+this.search_job_title+'&search_industry='+this.search_industry+'&search_department='+this.search_department+'&search_location='+this.search_location+'&limit=' + Number(limit) + '&skip=' + offset + '&job_type=local-walking')
     .subscribe((data)=>{
       this.showloader = false;
        if(data.walking_interviews && data.walking_interviews.length){
         data.walking_interviews.forEach(value=>{this.nationalWalking.push(value)});
         this.commonService.callJobsData(this.globalJobSave);
        }
       //this.nationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==1);
       this.count_national_walkin_show =this.count_national_walkin_show +12;
     },(error)=>{});
   }
  }

    addMoreSearchInternationalWalking(){
    this.showloader = true;
    let limit = 12 + this.internationalWalking.length;
    let offset =  this.internationalWalking.length;
      if(this.src == 'advance_search' && this.advance_search != ''){
        let data={limit:limit, skip:offset,formData:this.advance_search, job_type:'international-walking'};
     this.commonService.create('/api/employee/get-job-by-adv-search',data)
     .subscribe((data)=>{
       this.showloader = false;
       if(data.walking_interviews && data.walking_interviews.length){
         data.walking_interviews.forEach(value=>{this.internationalWalking.push(value)});
         this.commonService.callJobsData(this.globalJobSave);
       }
       //this.internationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==2);
       this.count_international_walkin_show =this.count_international_walkin_show+12;
     },(error)=>{});
   } else {
     this.commonService.getAll('/api/employee/get-job-by-search?search_job_title='+this.search_job_title+'&search_industry='+this.search_industry+'&search_department='+this.search_department+'&search_location='+this.search_location+'&limit=' + Number(limit)+ '&skip=' + offset + '&job_type=local-walking')
     .subscribe((data)=>{
       this.showloader = false;
       if(data.walking_interviews && data.walking_interviews.length){
         data.walking_interviews.forEach(value=>{this.internationalWalking.push(value)});
         this.commonService.callJobsData(this.globalJobSave);
       }
       //this.internationalWalking = data.walking_interviews.filter((obj)=>obj.walkin_location_type ==2);
       this.count_international_walkin_show =this.count_international_walkin_show+12;
     },(error)=>{});
   }
  }

  searchAutoComplete(type, key) {
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
    if(type ==='COUNTRY_WISE'){
      if(key.target.value.trim().length ==0){
        this.country_model_array.length =0;
        this.country_loc_model ='';
      }
      if(this.country_loc_model){
        let titleDataArr = this.country_loc_model.split(',');
        let arrFilter = this.country_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.country_model_array = arrFilter;
      } 
      usedData = this.country_loc_model;
    }else if(type ==='INDUSTRY_WISE'){
      if(key.target.value.trim().length ==0){
        this.insudtry_model_array.length =0;
        this.insudtry_model ='';
      } 
      if(this.insudtry_model){
        let titleDataArr = this.insudtry_model.split(',');
        let arrFilter = this.insudtry_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.insudtry_model_array = arrFilter;
      } 
      usedData = this.insudtry_model;
    }else if(type ==='JB_TITLE_WISE'){
      if(key.target.value.trim().length ==0){
        this.job_title_model_array.length =0;
        this.job_title_model ='';
      } 
      if(this.job_title_model){
        let titleDataArr = this.job_title_model.split(',');
        let arrFilter = this.job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.job_title_model_array = arrFilter;
      } 
      usedData = this.job_title_model;
    }
    if(!searchdata){
      this.show_jb_title_srh = false;
      this.show_city_comp_srh = false;
      this.show_industry_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.show_jb_title_srh = true;
            this.title_job_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'COUNTRY_WISE') {
            this.show_city_comp_srh = true;
            this.city_country_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } else if (data.type == 'INDUSTRY_WISE') {
            this.show_industry_srh = true;
            this.industrty_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          }
        }


      }, (error) => { });
  }

   autoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.show_jb_title_srh = false;
      this.searchData.title_comp.id = titJob.id;
      if(this.job_title_model_array.length){
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = this.job_title_model_array.toString() + ',';
      }else{
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = titJob.name + ',';
      }
      document.getElementById('job_title_search').focus();
    } else if (type == 'COUNTRY_WISE') {
      const city_count = this.city_country_data.find((data) => data.id == String(id));
      this.show_city_comp_srh = false;
      if(this.country_model_array.length){
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }else{
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = city_count.name + ',';
      }
       document.getElementById('country_search').focus();
      this.searchData.city_country.id = city_count.id;
      //this.searchData.city_country.name = city_count.name;
    } else if (type == 'INDUSTRY_WISE') {
      const industry_count = this.industrty_data.find((data) => data.id == String(id));
      this.show_industry_srh = false;
      if(this.insudtry_model_array.length){
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }else{
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = industry_count.name + ',';
      }
      this.searchData.industry.id = industry_count.id;
      //this.searchData.industry.name = industry_count.name;
    }

  }

  advancedAutoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.adv_show_jb_title_srh = false;
      if(this.advanced_job_title_model_array.length){
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(this.advanced_job_title_model_array.toString() + ',');
      }else{
        this.advanced_job_title_model_array =[...this.advanced_job_title_model_array, titJob.name];
        this.advSearchForm.controls['job_title'].setValue(titJob.name + ',');
      }
      document.getElementById('jobTitle').focus();
    } 

  }

   advancedSearchAutoComplete(type, key){
    let typedKeyArray = key.target.value.trim().split(',');
    let searchdata = typedKeyArray.pop();
    let usedData = '';
  if(type ==='JB_TITLE_WISE'){
      if(key.target.value.trim().length ==0){
        this.advanced_job_title_model_array.length =0;
        this.advSearchForm.controls['job_title'].setValue('');
      } 
      if(this.advSearchForm.controls['job_title'].value){
        let titleDataArr = this.advSearchForm.controls['job_title'].value.split(',');
        let arrFilter = this.advanced_job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.advanced_job_title_model_array = arrFilter;
      } 

       usedData =  this.advSearchForm.controls['job_title'].value;
    }
    if(!searchdata){
      this.adv_show_jb_title_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.adv_show_jb_title_srh = true;
            this.title_job_data = _.uniqBy(data.data,(e)=>{
              return e.name
            });
          } 
        }


      }, (error) => { });
    }

  searchFilter(){

     this.show_jb_title_srh = false;
     this.show_city_comp_srh = false;
     this.show_industry_srh = false;
    this.show_department_srh = false;
   
    let search_title = this.job_title_model?this.job_title_model.replace(/,\s*$/, "") : '';
    let search_industry = this.insudtry_model?this.insudtry_model.replace(/,\s*$/, "") : '';
    let search_department = this.department_model?this.department_model.replace(/,\s*$/, "") : '';
    let search_city = this.country_loc_model?this.country_loc_model.replace(/,\s*$/, "") : '';
    localStorage.removeItem('advSearch');
    this.resetAdvanceSearch();
    this.router.navigate(['/search-job-list'], { queryParams: { search_title: search_title, 'search_industry': search_industry,'search_department':search_department, 'search_location':search_city } });
    //this.getJobBysearch();
  }


  searchJobs(){
    this.advCallCount += 1;
    this.advSearchForm.controls['country_id'].setValue(this.storeMultiSelect.country_value);
    this.advSearchForm.controls['city_id'].setValue(this.storeMultiSelect.city_value);
    this.advSearchForm.controls['industry_id'].setValue(this.storeMultiSelect.industries_value);
    this.advSearchForm.controls['department_id'].setValue(this.storeMultiSelect.department_value);
    let commaExist = this.advSearchForm.controls['job_title'].value.slice(-1);
    let jbTitleValue = (commaExist == ',')? this.advSearchForm.controls['job_title'].value.replace(/,\s*$/, ""):this.advSearchForm.controls['job_title'].value;
    this.advSearchForm.controls['job_title'].setValue((Array.isArray(jbTitleValue)? jbTitleValue : jbTitleValue.split(',')) );
    localStorage.setItem('advSearch',JSON.stringify(this.advSearchForm.value));
    this.advance_search = localStorage.getItem('advSearch')? JSON.parse(localStorage.getItem('advSearch')):'';
    this.router.navigate(['/search-job-list'],{queryParams:{src:'advance_search',call:this.advCallCount}});
    //this.getJobBysearch();
  }


  getOnchangeEvent(type) {
    if (type == 'industries') {
      this.industriesDiv = true;
    }else if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }
    else if(type =='departments'){
      this.departmentDiv = true;
    }
  }

  checkmultiselect(e){
    if (e.field_name == 'industries') {
      this.industriesDiv = e.status;
    }else if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDiv = e.status;
    }
  }

  changeSubmitVal(){
    this.advSearchForm.patchValue({
      industry_id:this.storeMultiSelect.industries_value.toString(),
    });
  }

  multiselectAll(e){  
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='industries'){
      this.advSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
    else if(filed =='departments'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='industries'){
      this.advSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.advSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.advSearchForm.patchValue({
      city_id: e.storedValue
      });
    }else if(e.field_name =='departments'){
      this.advSearchForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

    removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.advSearchForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
        break; 
      case "departments":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.advSearchForm.patchValue({
          department_id: this.storeMultiSelect.department_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

    applyJob(id, index){
    let job = this.searchJobsResult.find(obj=>obj._id == id)
    this.selected_job = job;
    this.selected_job_type = 'jobs'
    this.selectedindex = index
      if(job.condition_type ==1){
        if(job.conditions.gender && job.candidate_type =='2' && this.logingUser.user_detail.gender == 2){
          this.alertMessage('This job only for male apply next job');
          return;
        }
        if(job.conditions.gender && job.candidate_type =='3' && this.logingUser.user_detail.gender == 1){
          this.alertMessage('This job only for female apply next job');
          return;
        }

        let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
        if(job.conditions.industry &&  !job.job_title.includes(this.logingUser.user_detail.profile_title) && !job.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && this.loginEmpProfile.profile.total_experience_yr_value == 0 && job.min_experiance.value >0){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && job.min_experiance && this.loginEmpProfile.profile.total_experience_yr_value < job.min_experiance.value){
          this.alertMessage('This job post work experience not matching with your profile  apply next job');
          return;
        }

        if(job.conditions.location && job.employement_for.name =='Domestic Jobs' && job.country_name != this.loginEmpProfile.profile.country.name){
          this.alertMessage('This job only for local candidate apply next job');
          return;
        }

        if(job.conditions.salary){
          if(job.min_monthly_salary && !job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary < job.min_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(!job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.min_monthly_salary && this.loginEmpProfile.employee_pref.min_salary < job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }
        }
        if(job.conditions.education && job.qualifications_name.indexOf('Any Course') == -1){
           let qualificationIds = job.qualifications
           let empQualificationIds = !_.isEmpty(this.loginEmpProfile.employee_education) ? this.loginEmpProfile.employee_education.map(obj=>obj.degree_id) :[]
           if(empQualificationIds.length === 0){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           }else if(!_.isEmpty(qualificationIds) && _.size(qualificationIds.filter(obj=> empQualificationIds.indexOf(obj)!= -1)) ===0 ){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           } 
        }
      }else if(job.condition_type ==3){
        $('#question_modal_open').click();
        return
      }
    
      this.commonService.create('/api/employee/job-search', {job_id:id})
        .subscribe((data)=>{
          if(data.status ==200){
            this.messageService.success(data.status_text);
            this.searchJobsResult.splice(index,1);
          }else if(data.status ==422){
              this.messageService.error(data.status_text);
          }
        }, (error)=>{});
  }

  saveJob(id, index){
      this.commonService.create('/api/employee/job-search/save-job', {job_id:id})
        .subscribe((data)=>{
          if(data.status ==200){
            this.messageService.success(data.status_text);
            //let indexdata = this.job_data.findIndex(obj=>obj._id == id);
            //this.job_data[indexdata].is_saved =1;
            //this.job_data.splice(index,1);
          }
        }, (error)=>{});
  }

  removeSaveJob(id){
    this.commonService.getAll('/api/employee/remove/save-job/' + id)
        .subscribe(data=>{
          if(data.status ==200){
            //let indexdata = this.job_data.findIndex(obj=>obj._id == id);
            //this.job_data[indexdata].is_saved =0;
          }
        },error=>{});
  }

  applyInternationalJob(job_id, job_type, index){
    
    let job = this.internationalJobs.find(obj=>obj._id == job_id)
    this.selected_job = job;
    this.selected_job_type = 'international'
    this.selectedindex = index
    if(job_type =='apply-job'){
      if(job.condition_type ==1){
        if(job.conditions.gender && job.candidate_type =='2' && this.logingUser.user_detail.gender == 2){
          this.alertMessage('This job only for male apply next job');
          return;
        }
        if(job.conditions.gender && job.candidate_type =='3' && this.logingUser.user_detail.gender == 1){
          this.alertMessage('This job only for female apply next job');
          return;
        }

        let emplyeePrefIndustry =  this.loginEmpProfile.employee_pref.industries.map(obj=>obj._id); 
        if(job.conditions.industry &&  !job.job_title.includes(this.logingUser.user_detail.profile_title) && !job.industry_ids.some(obj=> emplyeePrefIndustry.indexOf(obj)!= -1)){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && this.loginEmpProfile.profile.total_experience_yr_value == 0 && job.min_experiance.value >0){
          this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
          return;
        }

        if(job.conditions.expeiance && job.min_experiance && this.loginEmpProfile.profile.total_experience_yr_value < job.min_experiance.value){
          this.alertMessage('This job post work experience not matching with your profile  apply next job');
          return;
        }

        if(job.conditions.location && job.employement_for.name =='Domestic Jobs' && job.country_name != this.loginEmpProfile.profile.country.name){
          this.alertMessage('This job only for local candidate apply next job');
          return;
        }

        if(job.conditions.salary){
          if(job.min_monthly_salary && !job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary < job.min_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(!job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }else if(job.min_monthly_salary && job.max_monthly_salary && this.loginEmpProfile.employee_pref.max_salary > job.min_monthly_salary && this.loginEmpProfile.employee_pref.min_salary < job.max_monthly_salary){
            this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
            return;
          }
        }
        if(job.conditions.education){
           let qualificationIds = [...job.min_qualification_stream,...job.max_qualification_stream]
           let empQualificationIds = !_.isEmpty(this.loginEmpProfile.employee_education) ? this.loginEmpProfile.employee_education.map(obj=>obj.degree_id) :[]
           if(empQualificationIds.length === 0){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           }else if(!_.isEmpty(qualificationIds) && _.size(qualificationIds.filter(obj=> empQualificationIds.indexOf(obj)!= -1)) ===0 ){
             this.alertMessage('As per your cv/ Resume this job not matching for you apply next job');
             return;
           } 
        }
      }else if(job.condition_type ==3){
        $('#question_modal_open').click();
        return
      }
    }

         if(job_type =='saved-job'){
          this.commonService.create('/api/employer/international-jobs/saved', {job_id:job_id})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
        }else{
          this.commonService.create('/api/employer/international-jobs/applied', {job_id:job_id})
            .subscribe((data)=>{
              if(data.status ==200){
                this.messageService.success(data.status_text);
                this.internationalJobs.splice(index,1);
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
        } 
  }

  applyQuestionCondWise(job_id,jb_post_type){
    let questtions = Object.entries(this.questionAnswers)
    let finalQuestionsAns = [];
    if(questtions.length !== this.selected_job.conditions.questions.length){
        this.messageService.error('Please give answer all questions');
        return
    }
    questtions.forEach((question, index)=>{
        finalQuestionsAns = [...finalQuestionsAns,{'qustion':question[0].split('_')[1],'answer':question[1]}]        
    });

    if(jb_post_type =='international'){
          this.commonService.create('/api/employer/international-jobs/applied', {job_id:job_id,job_questions:Object.assign({}, finalQuestionsAns)})
            .subscribe((data)=>{
              if(data.status ==200){
                $('#question_modal_close').click();
                this.messageService.success(data.status_text);
                this.internationalJobs.splice(this.selectedindex,1);
              }else if(data.status ==422){
                this.messageService.error(data.status_text);
              }
            }, (error)=>{});
      }else{
          this.commonService.create('/api/employee/job-search', {job_id:job_id,job_questions:Object.assign({}, finalQuestionsAns)})
            .subscribe((data)=>{
              if(data.status ==200){
                 $('#question_modal_close').click();
                this.messageService.success(data.status_text);
                this.searchJobsResult.splice(this.selectedindex,1);
              }else if(data.status ==422){
                  this.messageService.error(data.status_text);
              }
            }, (error)=>{});        
      }
  }

  applyWalking(job_id, job_type){
    let job = this.searchJobsResult.find(obj=>obj._id == job_id)

      if(job_type =='local-walking'){
        this.router.navigate(['walkin-job-details','local',job_id]); 
        //this.router.navigate(['apply-hiring'],{queryParams:{walking_id:job_id}});
                
      }else if(job_type =='international-walking'){
        this.router.navigate(['walkin-job-details','international',job_id]); 
      }
  }



    advancedJobOpen(){
      this.advance_search = localStorage.getItem('advSearch')? JSON.parse(localStorage.getItem('advSearch')):'';
      
      if(this.advance_search){
        this.storeMultiSelect.industries_value = this.advance_search.industry_id ? this.industries[""].filter(obj=> this.advance_search.industry_id.indexOf(obj._id) !== -1).map(obj=>obj._id):[];
        this.storeMultiSelect.industries_name = this.advance_search.industry_id ?  this.industries[""].filter(obj=> this.advance_search.industry_id.indexOf(obj._id) !== -1).map(obj=>obj.name):[];
        this.storeMultiSelect.country_value = this.advance_search.country_id ? this.allCountries.filter(obj=> this.advance_search.country_id.indexOf(obj._id) !== -1).map(obj=>obj._id):[];
        this.storeMultiSelect.country_name = this.advance_search.country_id ?  this.allCountries.filter(obj=> this.advance_search.country_id.indexOf(obj._id) !== -1).map(obj=>obj.name):[];
        this.storeMultiSelect.city_value = this.advance_search.city_id ? this.cities.filter(obj=>this.advance_search.city_id.indexOf(obj._id) !== -1).map(obj=>obj._id):[];
        this.storeMultiSelect.city_name = this.advance_search.city_id ? this.cities.filter(obj=>this.advance_search.city_id.indexOf(obj._id) !== -1).map(obj=>obj.name):[];
        this.storeMultiSelect.department_value = this.advance_search.department_id ? this.advance_search.department_id:[];
        this.storeMultiSelect.department_name = this.advance_search.department_id ?  this.all_departments.filter(obj=>this.advance_search.department_id.find(ob=> ob==obj._id)).map(obj=>obj.name):[];

        this.advanced_job_title_model_array = this.advance_search.job_title ? this.advance_search.job_title: [];

         this.advSearchForm.patchValue({
          job_title:this.advance_search.job_title ? this.advance_search.job_title.toString() + ',':'',
          employement_for:this.advance_search.employement_for,
          country_id:this.storeMultiSelect.country_name,
          city_id:this.storeMultiSelect.city_name,
          industry_id:this.storeMultiSelect.industries_name,
          department_id:this.storeMultiSelect.department_value,
          min_experiance:this.advance_search.min_experiance,
          max_experiance:this.advance_search.max_experiance,
          salary_type:this.advance_search.salary_type,
          currency:this.advance_search.currency,
          min_monthly_salary:this.advance_search.min_monthly_salary,
          max_monthly_salary:this.advance_search.max_monthly_salary,
        });       
      }

  }

  alertMessage(text){
    Swal.fire({
      icon: 'error',
      title: '',
      html: `<strong>${text}</strong>`,
    })
   }

  getSpaceValue(data){
    let value ='';
    if(Array.isArray(data)){
      value = data.toString().replace(/,/g, ', ');
    }else{
      return  data.replace(/,/g, ', ');
    }

  }

}
