import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-single-job-details',
  templateUrl: './single-job-details.component.html',
  styleUrls: ['./single-job-details.component.css']
})
export class SingleJobDetailsComponent implements OnInit {
	previousUrl:any;
	job_id:any;
	job_details:any;
  shareUrl: string;
  jobType: string;
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute, private messageService: MessageService) { 
  	
  	this.job_id = activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';
  	this.jobType = activeRoute.snapshot.queryParams['jb_type'] ? activeRoute.snapshot.queryParams['jb_type']: '';
    this.getData();
    this.shareUrl = window.location.href;
  }

  ngOnInit() {
  	window.scrollTo(0, 0);
  }

  getData(){
     if(this.jobType =='international'){
    this.commonService.getAll('/api/international/job-detail/' + this.job_id)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.job_details = data.job;
      }, (error)=>{});  
    }else{
    this.commonService.getAll('/api/job-by-id/' + this.job_id)
      .subscribe((data)=>{
        this.job_details = data.job;
        //this.companyImages = this.galleryImages.map((image)=> this.imgUrl +'employer/gallery_images/'+ image.name)
      }, (error)=>{});
    }

  }

   applyJob(id){
     if(this.jobType =='international'){
      this.commonService.getAll('/api/employee/international/applied-saved-job/' + id)
        .subscribe((data)=>{
          if(data.status ==200){
            this.messageService.success('Successfully applied');
            this.router.navigate(['applied-international-jobs']);
          }else if(data.status ==422){
              this.messageService.error(data.status_text);
          }
        }, (error)=>{});
     }else{
      this.commonService.create('/api/employee/job-search', {job_id:id})
          .subscribe((data)=>{
            if(data.status ==200){
              this.messageService.success(data.status_text);
              //this.job_data.splice(index,1);
            }else if(data.status ==422){
              this.messageService.error(data.status_text);
            }
          }, (error)=>{});       
     }

  }

  saveJob(id){
      this.commonService.create('/api/employee/job-search/save-job', {job_id:id})
        .subscribe((data)=>{
          if(data.status ==200){
            this.messageService.success(data.status_text);
            //this.job_data.splice(index,1);
          }
        }, (error)=>{});
  }

  removeSaveJob(id){
    if(this.jobType =='international'){
    this.commonService.delete('/api/employee/remove/save-job/', id)
        .subscribe(data=>{
          if(data.status ==200){
            this.messageService.success('Successfully unsaved job');
            this.router.navigate(['saved-international-jobs']);
          }
        },error=>{});
    }else{
    this.commonService.getAll('/api/employee/remove/save-job/' + id)
        .subscribe(data=>{
          if(data.status ==200){
             this.messageService.success(data.status_text);
          }
        },error=>{});      
    }

  }
}
