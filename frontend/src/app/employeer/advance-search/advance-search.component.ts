import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { filter, map, catchError } from 'rxjs/operators';
import * as _ from 'lodash'; 
import * as moment from 'moment'; 

@Component({
  selector: 'app-advance-search',
  templateUrl: './advance-search.component.html',
  styleUrls: ['./advance-search.component.css']
})
export class AdvanceSearchComponent implements OnInit {
	advanceSearchForm:FormGroup;
	showloader:boolean;
	any_two_job_title: any;
	all_two_job_title: any;
	exclude_job_title: any;
	profile_type: any;
	countries: any;
	cities:any;
	industries:any;
	job_roles: any;
	emplymentFor: any;
	experiance: any;
	empType: any;
	noOfVacancies: any;
	salaries: any;
	free_posted_job: any;
	industryList = [];
	selectedItems = [];
	dropdownSettings = {};
	underGraduateDiv: boolean;
	postGraduateDiv: boolean;
	industriesDiv: boolean;
	laguageDiv:boolean;
  departmentDiv:boolean;
	storeMultiSelect :any ={
  	undergradute_value:[],
  	undergraduate_name:[],
    undergraduate_sub_value: [],
  	postgraduate_value:[],
  	postgraduate_name:[],
    postgraduate_sub_value: [],
  	industries_value:[],
  	industries_name:[],
    department_name:[],
    department_value:[],
    country_value:[],
    country_name:[],
    city_value:[],
    city_name:[],
	};
  show_sub: any=[];
  show_sub_under: any=[];
	post_graduates: any=[];
	under_graduates: any=[];
	industry_id: any;
  languages: any=[];
  departments: any=[];
  noSearchSelect: any={
    lang_value:[],
    lang_name:[]
  };
  other_mst_data : any;
  salary_type_val :string;
  skillsData: any=[];
  anySkillKey: any;
  mustSkillKey: any;
  excludeSkillKey: any;
  countryArray: any=[];
  countryDiv:boolean=false;
  cityArray: any=[];
  cityDiv: boolean=false;
  currencyTypes: any=[];
  all_currency: any=[];
  queryParam: string;
  searchSavedData: any={};
  allIndustries: any=[];
  departData: any=[];
  under_graduate_deg: any=[];
  post_graduate_deg: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService,  private activeRoute:ActivatedRoute,private http: HttpClient) { 

    //this.departmentDiv = false;
  }

  ngOnInit() {
  	    this.advanceSearchForm= this.fb.group({
        id:'',
        any_keyword :[''],
        must_keyword:[''],
        exclude_keyword:[''],
        profile_type:[''],
        industry_id:[''],
        department_id:[''],
        country_id:[''],
        city_id:[''],
        min_experiance:[''],
        max_experiance:[''],
        salary_type:[''],
        currency:[''],
        min_monthly_salary:[''],
        max_monthly_salary:[''],
        max_qualification:[''],
        min_qualification:[''],
        min_qualification_stream: [],
        max_qualification_stream: [],
        education_type:[''],
        min_age:[''],
        max_age:[''],
        language_pref: [''],
        active_in: [''],
        sort_by: [''],
        premium: [''],
        women_only: [''],
        profile_view: [''],

    });

    this.advanceSearchForm.get('profile_type').setValue('profile_title_or_key_skills');   
    this.advanceSearchForm.get('education_type').setValue('underGraduate_or_postGraduate');  
  	this.asyncInit();
    this.advanceSearchForm.controls['country_id'].valueChanges.subscribe(value=>{
      let countryCode = this.countries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
      this.storeMultiSelect.city_value = this.cityArray[""].filter(obj=>this.storeMultiSelect.city_value.indexOf(obj._id) != -1).map(obj=>obj._id);
      this.storeMultiSelect.city_name = this.cityArray[""].filter(obj=>this.storeMultiSelect.city_name.indexOf(obj.name) != -1).map(obj=>obj.name);
      this.advanceSearchForm.patchValue({city_id: this.storeMultiSelect.city_name});
    });
  }

    asyncInit(){
        this.commonService.getCountryData.subscribe(data=>{
          if(data && data.status ==200){
            this.countries = data['countries'];
            this.countryArray ={"":data['countries']};
            this.industries = data['industries'];
            this.allIndustries = data['all_industries'];
            this.under_graduates = {"":_.uniqBy(data['courses'][""],(e)=>{
              return e.name
            })};
            this.under_graduate_deg = _.uniqBy(data['courses'][""],(e)=>{
              return e.name
            });
          }
        });

        this.commonService.getCityData.subscribe(data=>{
            if(data && data.status ==200){
              this.cities = data['cities'];
              this.cityArray =[];

            }
        });

        this.commonService.getCommonData.subscribe(data=>{
          if(data && data.status ==200){
             this.commonService.callFooterMenu(1);              
              this.job_roles = data['job_roles'];
              this.free_posted_job = data['free_posted_job'];
              this.other_mst_data = data['other_mst'];
              let otherData = data['other_mst'];
              this.emplymentFor = otherData.filter((data)=>data.entity =='emplyment_for');
              this.experiance =  data['allExprience'];
              this.empType = otherData.filter((data)=>data.entity =='employement_type');
              this.noOfVacancies = otherData.filter((data)=>data.entity =='no_of_vacancies');
              this.skillsData = data['skills'].map((obj)=> {return {display: obj.skill, value: obj.skill}});
              this.salaries = otherData.filter((data)=>data.entity =='salary');
              this.all_currency = data['all_currency'];
              this.post_graduates = data['post_graduate'];
              //this.under_graduates = data['under_graduate'];
              this.post_graduate_deg = data['post_graduate_deg'];
              //this.under_graduate_deg = data['under_graduate_deg'];
              this.languages = data['alllanguage'];
              this.departments = data['all_department'];
              //this.departData = this.departments ? Object.entries(this.departments)[0][1]:[];
              this.getParamData();
          }
        });
        
  }
  public requestAutocompleteSkill = (text: string): Observable<Response> => {
   const url = `/api/skill-and-job-title/filter?q=${text}`;
   return this.http.get<any>(url).pipe(map(data => _.uniqBy(data.data,(e)=>{
              return e.value
            })));
  };

  getParamData(){
    if(this.activeRoute.snapshot.queryParams['modify']){
      this.queryParam = this.activeRoute.snapshot.queryParams['modify'];
      this.searchSavedData = localStorage.getItem('cvSearch') ? JSON.parse(localStorage.getItem('cvSearch')):'';
      console.log(this.searchSavedData);
      this.setFormValue();
    }
  }

  setFormValue(){
    let countryNames = this.searchSavedData.country_id.length ?this.countries.filter(obj=>this.searchSavedData.country_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let citiesName = this.searchSavedData.city_id.length ? this.cities.filter(obj=>this.searchSavedData.city_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let indusrtyName = this.searchSavedData.industry_id.length ? this.allIndustries.filter(obj=>this.searchSavedData.industry_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let depName = this.searchSavedData.department_id.length ? this.departData.filter(obj=>this.searchSavedData.department_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let languageName = this.searchSavedData.language_pref.length ? this.languages.filter(obj=>this.searchSavedData.language_pref.indexOf(obj._id) != -1).map(obj=>obj.language_name):[];
    let undergradName = this.searchSavedData.min_qualification.length ? this.under_graduate_deg.filter(obj=>this.searchSavedData.min_qualification.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let postgradName = this.searchSavedData.max_qualification.length ? this.post_graduate_deg.filter(obj=>this.searchSavedData.max_qualification.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    this.noSearchSelect.lang_value = !_.isEmpty(this.searchSavedData.language_pref)?this.searchSavedData.language_pref:[] ;
    this.noSearchSelect.lang_name = !_.isEmpty(languageName) ? languageName :[];
    this.storeMultiSelect.country_value = !_.isEmpty(this.searchSavedData.country_id) ?this.searchSavedData.country_id:[];
    this.storeMultiSelect.country_name = !_.isEmpty(countryNames)?countryNames:[];
    this.storeMultiSelect.city_value = !_.isEmpty(this.searchSavedData.city_id)?this.searchSavedData.city_id:[];
    this.storeMultiSelect.city_name = !_.isEmpty(citiesName)?citiesName:[];
    this.storeMultiSelect.industries_value = !_.isEmpty(this.searchSavedData.industry_id) ?this.searchSavedData.industry_id:[];
    this.storeMultiSelect.industries_name = !_.isEmpty(indusrtyName)?indusrtyName:[];
    this.storeMultiSelect.department_value = !_.isEmpty(this.searchSavedData.department_id) ?this.searchSavedData.department_id:[];
    this.storeMultiSelect.department_name = !_.isEmpty(depName)?depName:[];
    this.storeMultiSelect.undergradute_value = !_.isEmpty(this.searchSavedData.min_qualification)?this.searchSavedData.min_qualification:[],
    this.storeMultiSelect.undergraduate_name= !_.isEmpty(undergradName) ?undergradName:[],
    this.storeMultiSelect.postgraduate_value= !_.isEmpty(this.searchSavedData.max_qualification)?this.searchSavedData.max_qualification:[],
    this.storeMultiSelect.postgraduate_name= !_.isEmpty(postgradName)?postgradName:[],
    this.storeMultiSelect.undergraduate_sub_value = !_.isEmpty(this.searchSavedData.min_qualification_stream)?this.searchSavedData.min_qualification_stream:[];
    this.storeMultiSelect.postgraduate_sub_value = !_.isEmpty(this.searchSavedData.max_qualification_stream)?this.searchSavedData.max_qualification_stream:[];

    this.anySkillKey = this.searchSavedData.any_keyword.map((obj)=> {return {display: obj, value: obj}});
    this.mustSkillKey = this.searchSavedData.must_keyword.map((obj)=> {return {display: obj, value: obj}});
    this.excludeSkillKey =this.searchSavedData.exclude_keyword? this.searchSavedData.exclude_keyword.map((obj)=> {return {display: obj, value: obj}}):'';

    let countryCode = this.countries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
    if(countryCode){
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
    }
        
    this.advanceSearchForm.patchValue({
        any_keyword :this.searchSavedData.any_keyword,
        must_keyword:this.searchSavedData.must_keyword,
        exclude_keyword:this.searchSavedData.exclude_keyword,
        profile_type:this.searchSavedData.profile_type,
        industry_id:this.storeMultiSelect.industries_value,
        department_id:this.storeMultiSelect.department_value,
        country_id:this.storeMultiSelect.country_value,
        city_id:this.storeMultiSelect.city_value,
        min_experiance:this.searchSavedData.min_experiance,
        max_experiance:this.searchSavedData.max_experiance,
        salary_type:this.searchSavedData.salary_type,
        currency:this.searchSavedData.currency,
        min_monthly_salary:this.searchSavedData.min_monthly_salary,
        max_monthly_salary:this.searchSavedData.max_monthly_salary,
        max_qualification:this.storeMultiSelect.postgraduate_value,
        min_qualification:this.storeMultiSelect.undergradute_value,
        min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value,
        max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value,
        education_type:this.searchSavedData.education_type,
        min_age:this.searchSavedData.min_age,
        max_age:this.searchSavedData.max_age,
        language_pref: this.noSearchSelect.lang_value,
        active_in: this.searchSavedData.active_in,
        sort_by: this.searchSavedData.sort_by,
        premium: this.searchSavedData.premium,
        women_only: this.searchSavedData.women_only,
        profile_view: this.searchSavedData.profile_view,
    });
  }

  getOnchangeEvent(type){
    if(type =='undergraduate'){
      this.underGraduateDiv = true;
    }else if(type =='postgraduate'){
      this.postGraduateDiv = true;
    }else if(type =='industries'){
      this.industriesDiv = true;
    }else if(type=='language'){
      this.laguageDiv = true;
    }else if(type=='department'){
      this.departmentDiv = true;
    }else if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }
    
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
        if (e.field_name == 'max_qualification') {
         this.storeMultiSelect.postgraduate_sub_value = this.storeMultiSelect.postgraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));
         this.advanceSearchForm.patchValue({
          max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value
         });
        }else if(e.field_name == 'min_qualification'){
         this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));         
          this.advanceSearchForm.patchValue({
            min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value
          });
        }
      }
    }
    if(e.field_name =='min_qualification'){
      this.advanceSearchForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(e.field_name =='max_qualification'){
      this.advanceSearchForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(e.field_name =='industries'){
      this.advanceSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='departments'){
      this.advanceSearchForm.patchValue({
      department_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.advanceSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.advanceSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
  }

  getSubChange(e){
    let event : any= e;
    if (e.event) {
      let index = e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index == -1) {
        e.storedSubValue.push({'parent_name' :e.parent_name, 'value': e.value});
      }
    } else {
      let index =  e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index != -1) {
        e.storedSubValue.splice(index, 1);
      }
    }  
    if (e.field_name == 'min_qualification') {
      this.advanceSearchForm.patchValue({
        min_qualification_stream: e.storedSubValue
      });
    } else if (e.field_name == 'max_qualification') {
      this.advanceSearchForm.patchValue({
        max_qualification_stream: e.storedSubValue
      });
    }
  }    

  getSubData(parent,type){
    if(type =='post-graduate'){
    let subData = this.storeMultiSelect.postgraduate_sub_value.filter(obj=>obj.parent_name == parent);
    return subData.length ? subData.length: '';
    }else if(type =='under-graduate'){
    let subData = this.storeMultiSelect.undergraduate_sub_value.filter(obj=>obj.parent_name == parent);
    return subData.length ? subData.length: '';
    }

  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
       case "department":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
        department_id: this.storeMultiSelect.department_name
        });
        break;
       case "undergraduate":
       let nameUnd = this.storeMultiSelect.undergraduate_name[indx];
        this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(nameUnd));         
        this.advanceSearchForm.patchValue({
            min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value
        });
       this.storeMultiSelect.undergradute_value.splice(indx, 1);
       this.storeMultiSelect.undergraduate_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
        min_qualification: this.storeMultiSelect.undergradute_value
        });
        break;  
       case "postgraduate":
       let namePost = this.storeMultiSelect.postgraduate_name[indx];
         this.storeMultiSelect.postgraduate_sub_value = this.storeMultiSelect.postgraduate_sub_value.filter(obj=> obj.parent_name !== String(namePost));
         this.advanceSearchForm.patchValue({
          max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value
         });
       this.storeMultiSelect.postgraduate_value.splice(indx, 1);
       this.storeMultiSelect.postgraduate_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
        max_qualification: this.storeMultiSelect.postgraduate_value
        });
        break;      
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
      case "language":
       this.noSearchSelect.lang_value.splice(indx, 1);
       this.noSearchSelect.lang_name.splice(indx, 1);
       this.advanceSearchForm.patchValue({
          language_pref: this.noSearchSelect.lang_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  multiselectAll(e){  
    console.log(e.storedValue);
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='min_qualification'){
      this.advanceSearchForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(filed =='max_qualification'){
      this.advanceSearchForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(filed =='industries'){
      this.advanceSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='departments'){
      this.advanceSearchForm.patchValue({
      department_id: e.storedValue
      });
    }else if(filed =='country'){
      this.advanceSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.advanceSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
    
  }

  checkmultiselect(e){
    if(e.field_name =='min_qualification'){
      this.underGraduateDiv = e.status;
    }else if(e.field_name =='max_qualification'){
      this.postGraduateDiv = e.status;
    }else if(e.field_name =='industries'){
      this.industriesDiv = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDiv = e.status;
    }else if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }
  }

  changeSubmitVal(){
    this.advanceSearchForm.patchValue({
      industry_id:this.storeMultiSelect.industries_value,
      min_qualification:this.storeMultiSelect.undergradute_value,
      max_qualification:this.storeMultiSelect.postgraduate_value,
      language_pref:this.noSearchSelect.lang_value,
      department_id:this.storeMultiSelect.department_value,
      country_id:this.storeMultiSelect.country_value,
      city_id: this.storeMultiSelect.city_value
    });
  }

  noSearchGetchange(e){
    //console.log(e);
     if(e.event){
      let index = this.noSearchSelect.lang_value.indexOf(e.value);
      if(index ==-1){
        this.noSearchSelect.lang_value.push(e.value);
        this.noSearchSelect.lang_name.push(e.name); 
      }
    }else{
      let index = this.noSearchSelect.lang_value.indexOf(e.value);
      if(index !=-1){
        this.noSearchSelect.lang_value.splice(index,1);
        this.noSearchSelect.lang_name.splice(index,1);
      }
    }
    this.advanceSearchForm.patchValue({
      language_pref: this.noSearchSelect.lang_value
      });
  }

  checknoSearchmultiSelect(e){
    this.laguageDiv = e.status;
  }

  salaryChange(e){
    this.salary_type_val = e.target.value;
    let otherData =this.other_mst_data; 
    if(e.target.value == 'hourly'){
      this.salaries = otherData.filter((data)=>data.entity =='hourly_salary');
    }else if(e.target.value == 'weekly'){
      this.salaries = otherData.filter((data)=>data.entity =='weekly_salary');
    }else if(e.target.value == 'monthly'){
      this.salaries = otherData.filter((data)=>data.entity =='monthly_salary');
    }
  }

  submitSearch(){

    let anyskillVal = this.anySkillKey? this.anySkillKey.map(obj=>obj.value.toLowerCase()):'';
    let mustskillVal = this.mustSkillKey ? this.mustSkillKey.map(obj=>obj.value.toLowerCase()):'';
    let excludeskillVal = this.excludeSkillKey ? this.excludeSkillKey.map(obj=>obj.value.toLowerCase()):'';
    this.advanceSearchForm.get('any_keyword').setValue(anyskillVal);
    this.advanceSearchForm.get('must_keyword').setValue(mustskillVal);
    this.advanceSearchForm.get('exclude_keyword').setValue(excludeskillVal);

    if(this.advanceSearchForm.get('any_keyword').value ==''){
      this.message.error('Please select any keyword field');
      return false;
    }
    if(this.advanceSearchForm.get('must_keyword').value ==''){
      this.message.error('Please select must keyword field');
      return false;
    }
    //this.changeSubmitVal();

    localStorage.setItem('cvSearch',JSON.stringify(this.advanceSearchForm.value));
    this.commonService.create('/api/search-history', {search_key:this.advanceSearchForm.get('any_keyword').value,
      search_data:this.advanceSearchForm.value,search_type:'advance','is_modified':(this.queryParam?1:0)})
        .subscribe((data)=>{

        }, (error)=>{});
    this.router.navigate(['/employer/resumes'],{queryParams:{src:'cvSearch',prevsearch:'advance-search'}});
  }

  getAddDates(value){
      return moment().subtract(value, 'd').format('YYYY-MM-DD');
  }



}
