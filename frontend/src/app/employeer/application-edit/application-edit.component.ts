import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-application-edit',
  templateUrl: './application-edit.component.html',
  styleUrls: ['./application-edit.component.css']
})
export class ApplicationEditComponent implements OnInit {
	jobApplicationEditForm:FormGroup;
  walkinApplicationEditForm:FormGroup;
  internationalJobEditForm: FormGroup;
  showloader:boolean;
  application_id :any;
  application_phases :any;
  applied_job_data : any;
  applied_walk_in : any;
  applied_international_job: any;
  isSubmit: boolean;
  job_type: string;
  isLoaded: boolean = false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router,private activeRoute: ActivatedRoute ,private commonService: CommonService) { 
  	this.asyncInit();
  	this.isSubmit = false;
  	this.application_id = this.activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';
    this.job_type = this.activeRoute.snapshot.paramMap.get("type") ? this.activeRoute.snapshot.paramMap.get("type"):'';
      if(this.application_id){
          this.commonService.getAll('/api/employer/application-job?application_id='+this.application_id+'&job_type='+this.job_type)
              .subscribe((data)=>{
                this.isLoaded = true;
                this.applied_job_data = data.data.applied_list_job[0];
                this.applied_walk_in = data.data.applied_walk_in[0];
                this.applied_international_job = data.data.applied_international_job[0];
                this.setJobFormValue(this.applied_job_data);
                this.setWalkinFormValue(this.applied_walk_in);
                this.setInternationalJobFormValue(this.applied_international_job);
              }, (error)=>{});
        }

  }

  ngOnInit() {
  	this.jobApplicationEditForm= this.fb.group({
  		application_id :'',
      job_id: '',
      employee_name: '',
      application_status: ['',[Validators.required]],
      employee_email: '',
      special_note: ['',[Validators.required]],
  	});
    this.walkinApplicationEditForm= this.fb.group({
      application_id :'',
      walk_in_id: '',
      employee_name: '',
     // application_status: ['',[Validators.required]],
      employee_email: '',
      booked_date: '',
      booked_time: '',
      special_note: ['',[Validators.required]],
    });
    this.internationalJobEditForm= this.fb.group({
      application_id :'',
      job_id: '',
      employee_name: '',
      application_status: ['',[Validators.required]],
      employee_email: '',
      special_note: ['',[Validators.required]],
    });  
  }
  asyncInit(){
  	this.commonService.getAll('/api/application-status')
  		.subscribe((data)=>{
         this.commonService.callFooterMenu(1);
  				//console.log(data.data.application_phases);
  				this.application_phases = data.data.application_phases;
  		});
  }
  setJobFormValue(data){
    this.jobApplicationEditForm.patchValue({
        application_id : (data ? data.bsb_jb_app_id :''),
        job_id: (data ? data.job.bsb_jb_trans_id:''),
        employee_name: data?data.employee_profile.user_detail.name:'',
        application_status: data? data.applied_status._id:'',
        employee_email: data ? data.employee_profile.email:'',
        //special_note: data.employement_for,
    });
  }

  setInternationalJobFormValue(data){
    this.internationalJobEditForm.patchValue({
        application_id : (data ? data.bsb_jb_app_id :''),
        job_id: (data ? data.job.bsb_jb_trans_id:''),
        employee_name: data?data.employee_profile.user_detail.name:'',
        application_status: data? data.applied_status._id:'',
        employee_email: data ? data.employee_profile.email:'',
        //special_note: data.employement_for,
    });
  }

  setWalkinFormValue(data){
    this.walkinApplicationEditForm.patchValue({
        application_id : (data ? data.bsb_jb_app_id :''),
        walk_in_id: (data && data.interview)? data.interview.bsb_jb_trans_id : '',
        employee_name: data ? data.employee_profile.user_detail.name : '',
        //application_status: data ? data.applied_status._id : '',
        employee_email: data ? data.employee_profile.email : '',
        booked_date: data ? data.booked_date : '',
        booked_time: data ? data.booked_time : '',
        //special_note: data.employement_for,
    });
  }



  updateForm(){
  	 this.showloader = true;
  	 this.isSubmit = true;
    if(this.jobApplicationEditForm.invalid){
      this.showloader = false;
      return;
    }
  	  this.commonService.update('/api/employer/applied-job-update', this.application_id,this.jobApplicationEditForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          if(data.status === 200){
            this.message.success(data.status_text);
            this.router.navigate(['/employer/application-recieved']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  updateInternationalForm(){
     this.showloader = true;
     this.isSubmit = true;
    if(this.internationalJobEditForm.invalid){
      this.showloader = false;
      return;
    }
      this.commonService.update('/api/employer/applied-job-international-update', this.application_id,this.internationalJobEditForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          if(data.status === 200){
            this.message.success(data.status_text);
            this.router.navigate(['/employer/application-recieved']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

}
