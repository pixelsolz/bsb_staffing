import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-application-recieved',
  templateUrl: './application-recieved.component.html',
  styleUrls: ['./application-recieved.component.css']
})
export class ApplicationRecievedComponent implements OnInit {
	showloader:boolean;
	applied_list_jobs : any;
	store_applied_list_jobs : any;
  store_applied_walk_in_list : any;
  store_applied_interntional_jobs_list:any;
  applied_walk_in_list : any;
  applied_interntional_jobs_list:any;
	posted_jobs : any[];
	active_page: number=0;
  count_data: number;
  count_data_walk_in: number;
  count_international_job: number;
  current_selected_option:string='free_job_post';
  application_history:any={};
  set_employee_id: any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { 

  }

  ngOnInit() {

  	this.asyncInit();
  }

  asyncInit(){
  	this.showloader = true;
  	this.commonService.getAll('/api/employer/application-received?off_set='+this.active_page)
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.showloader = false;
  			this.applied_list_jobs = data.data.applied_list_jobs;
  			this.count_data = data.data.applied_jobs_count;
  			this.store_applied_list_jobs = data.data.applied_list_jobs;
  			this.posted_jobs = data.data.posted_jobs;
  			console.log(data.data.applied_list_jobs);
  		});

  }
  applicationType(e){
  	this.showloader = true;
    this.current_selected_option = e.target.value;
  	//console.log(e.target.value);
  	//if(e.target.value == 'free_job_post'){
  		this.commonService.getAll('/api/employer/application-received?off_set='+this.active_page+'&job_type='+e.target.value)
  		.subscribe((data)=>{
  			this.showloader = false;
  			this.applied_list_jobs = data.data.applied_list_jobs;
  			this.count_data = data.data.applied_jobs_count;
  			this.store_applied_list_jobs = data.data.applied_list_jobs;
  			this.posted_jobs = data.data.posted_jobs;

        this.store_applied_interntional_jobs_list = data.data.applied_interntional_jobs_list;
        this.applied_interntional_jobs_list =  data.data.applied_interntional_jobs_list;
        this.count_international_job = data.data.applied_interntional_count;

        this.applied_walk_in_list = data.data.applied_walk_in_list;
        this.store_applied_walk_in_list = data.data.applied_walk_in_list;
        this.count_data_walk_in = data.data.applied_walk_in_count;
  			//console.log(this.applied_walk_in_list);
  		});
  	//}
  	
  }

  jobByTitle(job_id){
    console.log(this.current_selected_option);
  	this.showloader = true;
  	if(job_id !=''){
      if(this.current_selected_option == 'free_job_post' || this.current_selected_option == 'premium_job_post'){
  	  	let data = this.store_applied_list_jobs.filter((job)=>job.job_id == job_id);
  	  	this.applied_list_jobs = data;
        //console.log(data);
        this.count_data = data.length;
      }
      if(this.current_selected_option == 'local_walk_in' || this.current_selected_option == 'international_walk_in'){
        let data = this.store_applied_walk_in_list.filter((job)=>job.job_id == job_id);
        this.applied_walk_in_list = data;
        this.count_data_walk_in =data.length;
      }
      if(this.current_selected_option == 'international_free_job' || this.current_selected_option == 'international_premium_job'){
        let data = this.store_applied_interntional_jobs_list.filter(job =>job.job._id == job_id);
        this.applied_interntional_jobs_list = data;
        this.count_international_job =data.length;
      }
	  	this.showloader = false;
  	}else{
  		this.applied_list_jobs = this.store_applied_list_jobs;
  		this.showloader = false;
  	}
  }

  pagination(){
    let tot_number = (Number(this.count_data) /10);
    var items: number[] = [];
  for(var i = 1; i <= tot_number; i++){
     items.push(i);
  }
  return items;
  }

  paginationCall(index){
    this.active_page = index;
    this.asyncInit();
  
  }

  specialNoteHistory(type,application_id){
    console.log(type);
    console.log(application_id);
      this.showloader = true;
       this.commonService.getAll('/api/employer/application-note-history?type='+type+'&application_id='+application_id)
        .subscribe((data)=>{
          this.showloader =false;
          if(data.status === 200){
            this.application_history = data.allpication_history;
            console.log(this.application_history);
            //this.getResumeBySearch();
            //this.router.navigate(['/employer/job-posted-premium-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  likeProfile(employee_id){
    this.commonService.getAll('/api/employee/like-profile?id='+employee_id)
     .subscribe((data)=>{
       if(data.status === 200){
            this.message.success(data.status_text);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }else if(data.status == 422){
            this.message.error(data.status_text);
          }
     },(error)=>{});

  }
}
