import { Component, OnInit, HostListener, ElementRef,Renderer2} from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ColorEvent } from 'ngx-color';
declare var $: any;
@Component({
  selector: 'app-career-page',
  templateUrl: './career-page.component.html',
  styleUrls: ['./career-page.component.css']
})
export class CareerPageComponent implements OnInit {
	carrerForm:FormGroup;
	companyUploadLogoImage:string='';
	showHeaderColor:boolean = false;
	showBackgroundColor:boolean = false;
	showFooterColor:boolean = false;
	showTextColor:boolean = false;
	field_name:string;
	colorPattern = "^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$";
	galleries: any={ files:[], previews:[]};
	carrerData:any;
	submitted : boolean = false;
	imgErrmsg:string;
	count_opening_jobs:number;
	employer_jobs:any;
	desktopVersion:boolean=false;
	mobileVersion:boolean=false;
	tabletVersion:boolean=false;
	headerSection:boolean=false;
	company_name:string;
	carrer_url:string;
  remove_image_id: any=[];
  isDark: boolean =false;
  employer_id: any;
  widgetUrl: string;
  about_company:string;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService,private activeRoute: ActivatedRoute,private eRef:ElementRef,private renderer:Renderer2) {
  	this.renderer.removeClass(document.body, 'noScroll');
    this.widgetUrl = this.global.baseUrl;
  		this.commonService.getAll('/api/employer/carrer')
        .subscribe((data)=>{
           this.commonService.callFooterMenu(1);
            this.carrerData = data.data.carrerdata;
            this.count_opening_jobs = data.data.count_opening_jobs;
            this.employer_jobs = data.data.employer_jobs;

            //console.log(data.data.employer_jobs);
            if(this.carrerData && this.carrerData.id){
            	this.setFormValue();
            	this.company_name = this.carrerData.company_name;
            	this.galleries.previews = this.carrerData.carrer_photos.map((image)=>{
					      return {id:image._id,url:this.global.imageUrl+'career_page/photo/'+image.name,type:'saved'}
					    });

            	//this.carrer_url = location.origin+'/career-page/'+this.carrerData.company_name+'?ec='+btoa(this.carrerData.user_id);
              this.carrer_url = location.origin+'/career-page/'+this.carrerData.company_name+'?ec='+btoa(this.carrerData.user_id);
            	//console.log(btoa(this.carrerData.user_id));
            	//console.log(atob(this.carrerData.user_id));
            }  
        }, (error)=>{});

        //console.log(location.origin);

    }

  ngOnInit() {
    this.employer_id = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).id:'';
  	//console.log(location.origin);
  	this.carrerForm= this.fb.group({
        id:'',
        company_logo :['', [Validators.required]],
        header_color :['',[Validators.required,Validators.pattern(this.colorPattern)]],
        footer_color :['',[Validators.required,Validators.pattern(this.colorPattern)]],
        background_color :['',[Validators.required,Validators.pattern(this.colorPattern)]],
        text_color :['',[Validators.required,Validators.pattern(this.colorPattern)]],
        page_headline :['', [Validators.required]],
        about_company :['', [Validators.required]],
        contact_info :['', [Validators.required]],

    });
  	//console.log(this.companyUploadLogoImage);
  	this.carrerForm.controls.header_color.setValue('#fff');
  	this.carrerForm.controls.background_color.setValue('#c0c4e6');
  	this.carrerForm.controls.footer_color.setValue('#15171e');
  	this.carrerForm.controls.text_color.setValue('#0f1d79');
    //let about_company = '';
    this.carrerForm.controls.about_company.valueChanges.subscribe(val=>{
      //console.log(val);
      this.about_company = val.replace(/\n/g, '<br/>');
      //this.carrerForm.controls.about_company.setValue(val.replace(/\r?\n/g, '<br />'));
    });
    //console.log(this.about_company);

  	
  }

  setFormValue(){
  	
  	this.carrerForm.patchValue({
	      id:this.carrerData.id,
	    	company_logo:this.carrerData.company_logo,
	    	header_color:this.carrerData.header_color,
	    	footer_color:this.carrerData.footer_color,
	    	background_color:this.carrerData.background_color,
	    	text_color:this.carrerData.text_color,
	    	page_headline:this.carrerData.page_headline,
        //about_company: 'hi I am sourav \n I just want to know. \n that is .',
	    	about_company:this.carrerData.about_company.replace(/<br\s*[\/]?>/gi, "\n"),
	    	contact_info:this.carrerData.contact_info,
	      
	    });
  }

  saveCarrer(){

  	this.submitted = true;
    if (this.carrerForm.invalid) {
        return;
    }
    const formData = new FormData();
    const formValue = this.carrerForm.value;
        console.log(formValue.about_company);
    formData.append('id',formValue.id);
    formData.append('company_logo',formValue.company_logo);
    formData.append('header_color',formValue.header_color);
    formData.append('footer_color',formValue.footer_color);
    formData.append('background_color',formValue.background_color);
    formData.append('text_color',formValue.text_color);
    formData.append('page_headline',formValue.page_headline);
    formData.append('about_company',formValue.about_company.replace(/\n/g, '<br/>'));
    formData.append('contact_info',formValue.contact_info);
    formData.append('remove_img_ids',this.remove_image_id);
    for(var i = 0; i < this.galleries.files.length; i++){
      formData.append('images_files[]',this.galleries.files[i].file);
    }
    //console.log(formData);
    this.commonService.create('/api/employer/carrer-page-update', formData)
  		.subscribe((data)=>{
        if(data.status == 200){
          this.message.success(data.status_text);
          this.carrerData = data.data.carrerdata;
          this.galleries.previews = this.carrerData.carrer_photos.map((image)=>{
					      return {id:image.id,url:this.global.imageUrl+'career_page/photo/'+image.name,type:'saved'}
					    });
			    this.remove_image_id.length =0;
        }else if(data.status == 500){
          this.message.error(data.status_text);
        }
  		},(error)=>{});
  }

  companyLogo(logo){
  	this.imgErrmsg='';
  	const file = logo.files[0];
  	var mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.imgErrmsg = "Only images are supported.";
      return;
    }
  	console.log(file);
    this.carrerForm.controls['company_logo'].setValue(file);
     const reader = new FileReader();
    reader.onload = (event:any) => {
      this.companyUploadLogoImage = event.target.result;
    }
    reader.readAsDataURL(file);
  }
  logoReplace(logo){
  	this.imgErrmsg='';
  	const file = logo.files[0];
  	var mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.imgErrmsg = "Only images are supported.";
      return;
    }
    this.carrerForm.controls['company_logo'].setValue(file);
     const reader = new FileReader();
    reader.onload = (event:any) => {
      this.companyUploadLogoImage = event.target.result;
    }
    reader.readAsDataURL(file);
  }
  removeCompanyLogo(){
    this.carrerForm.controls['company_logo'].setValue('');
    this.companyUploadLogoImage ='';
  }

  selectColor(event) {
    console.log(event);
  	//console.log($event.color.hex);
  	//this.field_name = type;
  	if(event.field_name == 'header_color'){
  		this.carrerForm.controls.header_color.setValue(event.event.color.hex);
  	}else if(event.field_name == 'background_color'){
  		this.carrerForm.controls.background_color.setValue(event.event.color.hex);
  	}else if(event.field_name == 'footer_color'){
  		this.carrerForm.controls.footer_color.setValue(event.event.color.hex);
  	}else if(event.field_name == 'text_color'){
  		this.carrerForm.controls.text_color.setValue(event.event.color.hex);
  	}
 
  }

  checkOutSideEvent(event){
    if(event.field_name =='background_color'){
      this.showBackgroundColor = false;
    }else if(event.field_name =='footer_color'){
      this.showFooterColor = false;
    }else if(event.field_name =='header_color'){
      this.showHeaderColor = false;
    }else if(event.field_name =='text_color'){
      this.showTextColor = false;
    }
   
  }

  showColorPicker(type){
  	console.log(type);
  		this.field_name = type;
  	if(type == 'header_color'){
  		
  		this.showHeaderColor=true;
  		this.showBackgroundColor=false;
  		this.showFooterColor=false;
  		this.showTextColor=false;
  	}else if(type == 'background_color'){
  		
  		this.showBackgroundColor=true;
  		this.showHeaderColor=false;
  		this.showFooterColor=false;
  		this.showTextColor=false;
  	}else if(type == 'footer_color'){
  		
  		this.showFooterColor=true;
  		this.showTextColor=false;
  		this.showBackgroundColor=false;
  		this.showHeaderColor=false;
  	}else if(type == 'text_color'){
  	
  		this.showTextColor=true;
	  	this.showHeaderColor=false;
  		this.showBackgroundColor=false;
  		this.showFooterColor=false;
  	}else{
  		this.showTextColor=false;
	  	this.showHeaderColor=false;
  		this.showBackgroundColor=false;
  		this.showFooterColor=false;
  	}
  }

  processFile(imageInput: any){
    console.log(this.galleries.previews.length);
    if(this.galleries.previews.length > 4){
      this.message.error('Cannot Upload morethan 5 image!');
      return false;
    }
    const file = imageInput.files[0];
    const file_extence = file.name.split('.').pop().toLowerCase();
    //console.log(file_extence);
    const filesize = Math.round((file.size / 1024));
    const reader = new FileReader();
    let rand_id = Math.random();
     if(filesize > 2000 ){
      this.message.error('Maximum file size allowed 2 MB');
      return false;
    }
    if(file_extence == 'jpg' || file_extence == 'png' || file_extence == 'jpeg'){

      reader.onload = (event:any) => {
        this.galleries.previews.push({'url':event.target.result, 'type':'upload','id':rand_id});
      }
      reader.readAsDataURL(file);
      this.galleries.files.push({id:rand_id,'file':file});
      //console.log(this.galleries);
    }else{
      this.message.error('Upload file only allow jpg,png or jpeg!');
    }
  }

  removeFile(id, type){
    let index = this.galleries.previews.findIndex(preview=>preview.id ==id);
    this.galleries.previews.splice(index,1);
    if(type =='upload'){
      let file_index = this.galleries.files.findIndex(file=>file.id ==id);
      this.galleries.files.splice(file_index,1);
    }else{
      this.remove_image_id.push(id);
    }
  }

	viewVersion(version){
		if(version == 'desktop'){
			this.desktopVersion = !this.desktopVersion;
			this.headerSection = !this.headerSection;
			this.renderer.addClass(document.body, 'noScroll');
			//this.renderer.removeClass(document.body, 'noScroll');
		}else if(version == 'mobile'){
			this.mobileVersion = !this.mobileVersion;
		}else if(version == 'tablet'){
			this.tabletVersion = !this.tabletVersion;
			this.headerSection = !this.headerSection;
			this.renderer.addClass(document.body, 'noScroll');
		}

	}  

	closeVersion(version){
		this.desktopVersion = false;
		this.tabletVersion = false;
		this.headerSection = false;
		this.renderer.removeClass(document.body, 'noScroll');
	}

  copyUrl(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.message.success('copied!');
  }


	copyUrl12(){
    let inputElement:any = document.getElementById('copy_snippet');
		inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.message.success('copied!');
	}

}
