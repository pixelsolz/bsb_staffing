import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-carrer-widget',
  templateUrl: './carrer-widget.component.html',
  styleUrls: ['./carrer-widget.component.css']
})
export class CarrerWidgetComponent implements OnInit {

  employerId: any;
  employer_jobs: any=[];
  constructor( private router: Router, private commonService: CommonService,private activeRoute: ActivatedRoute) { 
  	this.employerId = this.activeRoute.snapshot.params.empr_id;

  	console.log(this.activeRoute.snapshot.params);
  }

  ngOnInit() {
  	this.commonService.getAll('/api/career/widget/' + this.employerId)
  		.subscribe(data=>{
  			this.employer_jobs = data.data;
  		},error=>{});
  }

}
