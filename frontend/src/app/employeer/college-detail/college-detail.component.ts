import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Global} from '../../global';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-college-detail',
  templateUrl: './college-detail.component.html',
  styleUrls: ['./college-detail.component.css']
})
export class CollegeDetailComponent implements OnInit {

  //mySlideImages = ['assets/images/hotelImg1.png','assets/images/hotelImg2.png','assets/images/hotelImg3.png'];
  mySlideImages:any = [];
  myCarouselOptions={items: 1, dots: true, nav: false, autoplay:true, loop:true, animateOut: 'fadeOut', margin:0};
  collegeId: string;
  collegeData: any={};
  institute_video:any;
  isLoaded: boolean = false
  sanitizeOutSite: any;
  constructor(private commonService: CommonService, private activeRoute: ActivatedRoute,private global: Global,private sanitizer: DomSanitizer) { 
  	window.scrollTo(0, 0);
  	this.collegeId = this.activeRoute.snapshot.params['id'];
    this.sanitizeOutSite = this.sanitizer
  }

  ngOnInit() {
  	this.asyncInit();
  }

  asyncInit(){
  	this.commonService.show('/api/college-list',this.collegeId)
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.collegeData = data.data;
        
        if(!this.collegeData.user.user_location){
           this.commonService.getLatLong(this.collegeData.address).then(res=>{
             if(res.status === "OK"){
               let lat = res.results[0]['geometry']['location']['lat']
               let lng = res.results[0]['geometry']['location']['lng']
               //this.collegeData.user.user_location = {"type":'Point','coordinates':[lat, lng]}
               this.commonService.getAll(`/api/update-user/location?user_id=${this.collegeData.user._id}&lat=${lat}&lng=${lng}`).subscribe(resp=>{
                 if(resp.status ===200){
                   window.location.reload();
                 }
               })

               //console.log(res.results[0]['geometry']['location'])
             }
     
           })
        }
        //this.institute_video = this.collegeData.institute_video.replace("watch?v=", "embed/");
        if(this.collegeData.institute_video){
          this.institute_video = this.sanitizer.bypassSecurityTrustResourceUrl(this.collegeData.institute_video.replace("watch?v=", "embed/"));
        }
        console.log(this.collegeData);
        let gallery_image = this.collegeData.gallery_images;
        if(gallery_image){
          this.mySlideImages = gallery_image.split(",");
        }
  		},(error)=>{});
  }

  checkId(){
    setTimeout(()=> {this.isLoaded = true},7000)
    return 'iGmap'
  }

}
