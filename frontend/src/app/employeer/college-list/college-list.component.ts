import { Component, OnInit,HostListener,ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-college-list',
  templateUrl: './college-list.component.html',
  styleUrls: ['./college-list.component.css']
})
export class CollegeListComponent implements OnInit {

@HostListener('document:click', ['$event'])
  clickout(event) {
    //const isInside = this.eRef.nativeElement.contains(event.target);
	//console.log(event.target.id);
	if(event.target.id === 'institute_name'){
		this.show_country_srh = false;
		this.show_city_srh = false;
		this.show_course_srh = false;
	}else if(event.target.id === 'location_by_country'){
		this.show_clg_name_srh = false;
		//this.show_country_srh = false;
		this.show_city_srh = false;
		this.show_course_srh = false;
	}else if(event.target.id === 'location_by_city'){
		this.show_clg_name_srh = false;
		this.show_country_srh = false;
		//this.show_city_srh = false;
		this.show_course_srh = false;
	}else if(event.target.id === 'course_offered'){
		this.show_clg_name_srh = false;
		this.show_country_srh = false;
		this.show_city_srh = false;
		//this.show_course_srh = false;
	}else{
		this.show_clg_name_srh = false;
		this.show_country_srh = false;
		this.show_city_srh = false;
		this.show_course_srh = false;
	}
  }

  collegeSearchForm: FormGroup;
  collegeList: any=[];
  college_data: any=[];
  country_data: any=[];
  city_data: any=[];
  course_data: any=[];
  queryString: any;
  countshowcollege: number;
  totalData:number
  show_clg_name_srh:boolean =false;
  show_country_srh:boolean =false;
  show_city_srh:boolean =false;
  show_course_srh:boolean =false;
  clg_institute_model:any;
  country_model:any;
  city_model:any;
  course_model:any;
  searchData = {
    institute_name: {
      id: '',
      name: '',
      table_name: ''
	},
	location_by_country: {
		id: '',
		name: '',
		table_name: ''
	  },
    location_by_city: {
      id: '',
      name: '',
      table_name: ''
    },
    course_offered: {
      id: '',
      name: '',
      table_name: ''
    }
  };
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,
    private router: Router, private commonService: CommonService,private eRef:ElementRef) { }

  ngOnInit() {
  	this.collegeSearchForm = this.fb.group({
  		institute_name:[''],
  		location_by_country:[''],
  		location_by_city:[''],
  		course_offered: ['']
  	});

  	this.asyncInit();
  }

  asyncInit(){
  	this.commonService.getAll('/api/college-list')
  		.subscribe(data=>{
        this.commonService.callFooterMenu(1);
  			if(data.status ==200){
          this.totalData = data.count
				  this.collegeList = data.data;
				  console.log(this.collegeList);
          		this.countshowcollege = 8;
  			}
  		}, error=>{});
  }

  addMoreCollege(){
    this.countshowcollege = this.countshowcollege + 8;
  	let limit = 8 + this.collegeList.length;
  	let urlData = (this.queryString) ? this.queryString + '&limit=' + limit : '?limit='+ limit;
  	this.commonService.getAll('/api/college-list' + urlData)
  		.subscribe(data=>{
  			if(data.status ==200){
  				this.collegeList = data.data;
  			}
  		},error=>{});
  }

  searchCollege(){
    if(this.collegeSearchForm.value.institute_name ==undefined && this.collegeSearchForm.value.location_by_country ==undefined &&
      this.collegeSearchForm.value.location_by_city ==undefined && this.collegeSearchForm.value.course_offered ==undefined){
      this.message.error('Please fill up atleast one field');
      return false;
    }
	  let institute_name = this.searchData.institute_name.name;
	  let location_by_country = this.searchData.location_by_country.id;
	  let location_by_city = this.searchData.location_by_city.id;
	  let course_offered = this.searchData.course_offered.id;
	  let formValues =  Object.entries(this.collegeSearchForm.value).filter(obj=> !_.isEmpty(obj[1]) )
    let qString ='';
    if(formValues.length >0){
      formValues.forEach((value, index)=>{
        if(index == 0){
          if(value[0] =='location_by_country'){
            qString += `?${value[0]}=${this.searchData.location_by_country.id}`
          }else if(value[0] =='location_by_city'){
             qString += `?${value[0]}=${this.searchData.location_by_city.id}`
          }else if(value[0] =='course_offered'){
             qString += `?${value[0]}=${this.searchData.course_offered.id}`
          }
          else{
              qString += `?${value[0]}=${value[1]}`
          }
          
        }else{
          if(value[0] =='location_by_country'){
            qString += `&${value[0]}=${this.searchData.location_by_country.id}`
          }else if(value[0] =='location_by_city'){
             qString += `&${value[0]}=${this.searchData.location_by_city.id}`
          }else if(value[0] =='course_offered'){
             qString += `&${value[0]}=${this.searchData.course_offered.id}`
          }else{
              qString += `&${value[0]}=${value[1]}`
          }
          
        }
      })
      this.commonService.getAll('/api/college-list' + qString)
      .subscribe(data=>{
        if(data.status ==200){
          this.collegeList = data.data;
        }
      },error=>{});

    }else{
       this.commonService.getAll('/api/college-list')
      .subscribe(data=>{
        if(data.status ==200){
          this.collegeList = data.data;
        }
      },error=>{});

    }



  }

  searchAutoComplete(type, key){
	//console.log(key.target.value);
	let searchdata = key.target.value;
	this.commonService.getAll('/api/employee/college-autocomplete-search?type=' + type + '&keyword=' + searchdata)
      .subscribe((data) => {
		  
        if (data.status == 200) {
          if (data.type == 'CLG_INSTITUTE_NAME') {
			this.searchData.institute_name.name = '';
            this.show_clg_name_srh = true;
            this.college_data = data.data;
          } else if (data.type == 'COUNTRY_WISE') {
			this.searchData.location_by_country.id = '';
            this.show_country_srh = true;
            this.country_data = data.data;
          } else if (data.type == 'CITY_WISE') {
			this.searchData.location_by_city.id = '';
            this.show_city_srh = true;
            this.city_data = data.data;
		  }
		  else if (data.type == 'COURSE_WISE') {
			this.searchData.course_offered.id = '';
            this.show_course_srh = true;
            this.course_data = data.data;
          }
        }
      }, (error) => { });
  }
  selectCompleteSearch(id,name,type){
	if(type ==='CLG_INSTITUTE_NAME'){
		this.clg_institute_model = name;
		this.searchData.institute_name.name = name;
		this.show_clg_name_srh = false;

	}else if(type === 'COUNTRY_WISE'){
		this.country_model = name;
		this.searchData.location_by_country.id = id;
		this.show_country_srh = false;
	}else if(type === 'CITY_WISE'){
		this.city_model = name;
		this.searchData.location_by_city.id = id;
		this.show_city_srh = false;
	}else if(type === 'COURSE_WISE'){
		this.course_model = name;
		this.searchData.course_offered.id = id;
		this.show_course_srh = false;
	}


  }

}
