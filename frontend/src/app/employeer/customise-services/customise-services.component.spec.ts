import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomiseServicesComponent } from './customise-services.component';

describe('CustomiseServicesComponent', () => {
  let component: CustomiseServicesComponent;
  let fixture: ComponentFixture<CustomiseServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomiseServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomiseServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
