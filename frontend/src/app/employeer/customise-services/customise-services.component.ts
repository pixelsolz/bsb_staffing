import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-customise-services',
  templateUrl: './customise-services.component.html',
  styleUrls: ['./customise-services.component.css']
})
export class CustomiseServicesComponent implements OnInit {
	customServiceForm:FormGroup;
  showloader:boolean;
  submitted : boolean = false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router,private activeRoute: ActivatedRoute ,private commonService: CommonService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  	this.customServiceForm = this.fb.group({
  		name :['',[Validators.required]],
  		designation :['',[Validators.required]],
  		email :['',[Validators.required,Validators.email]],
  		phone_no :['',[Validators.required]],
  		skype_id :[''],
  		service_type :['',[Validators.required]],
  		company_name :[''],
  		company_website :[''],
  		queries :['',[Validators.required]],
  	});
  }

  // convenience getter for easy access to form fields
  get f() { return this.customServiceForm.controls; }
  submitCustomService(){
  	//console.log(this.customServiceForm.value);
  	this.showloader = true;
    this.submitted = true;
    if(this.customServiceForm.invalid){
      this.showloader = false;
      return;
    }
  	//console.log(this.customServiceForm.value);
  	this.commonService.create('/api/custom-service', this.customServiceForm.value)
  		.subscribe((data)=>{
	  		this.showloader =false;
	          this.submitted = false;
	          if(data.status === 200){
	            this.message.success('Successfully send your query');
	            this.customServiceForm.reset();
	          }else if(data.status ==500){
	            this.message.error(data.stats_text);
	          }
	        }, (error)=>{
	          this.showloader = false;
	        });
  }

}
