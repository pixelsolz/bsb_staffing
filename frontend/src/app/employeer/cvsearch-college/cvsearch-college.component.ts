import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var Swal: any;
declare var $;

@Component({
  selector: 'app-cvsearch-college',
  templateUrl: './cvsearch-college.component.html',
  styleUrls: ['./cvsearch-college.component.css']
})
export class CvsearchCollegeComponent implements OnInit {
  noteSaveForm : FormGroup;
  reportCandidateForm: FormGroup;
  saveToFolderForm: FormGroup;
  institute_id: string;
  countries: any=[];
  cities: any=[];
  industries: any=[];
  searchForm: any={
    country_list:[],
    city_list:[],
    industry_list:[],
    candidate_active:'',
    show_profile:''
  };
  countryModel: any={};
  cityModel: any={};
  industryModel: any={};
  courseModel: any={};
  showloader: boolean =false;
  searchcvResult: any=[];
  selectedAll: any;
  set_employee_id:string;
  notedata:any;
  showphonwArr: any=[];
  isSubmit: boolean =false;
  employee_view_history:any=[];
  searchFormCall: boolean=false;
  total_cv_count : any;
  courses: any=[];
  checkedList:any;
  countrySerach: String ='';
  citySearch: String ='';
  courseOffer: String ='';

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,
    private router: Router, private commonService: CommonService, private activateRoute: ActivatedRoute) { 
  	this.institute_id = this.activateRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.reportCandidateForm=this.fb.group({
      id:[''],
      employee_id:[''],
      report_candidate_option:['', [Validators.required]],
      incorrect_email_id:[''],
      incorrect_mobile_no:[''],
      mobile_not_reachable:[''],
      incorrect_profile_or_resume:[''],
      profile_not_updated:[''],
      
    });

    this.saveToFolderForm=this.fb.group({
      id:[''],
      folder_save:[''],
      folder_name:[''],
      folder_id:[''],
      employee_ids:[''],
    });

    this.noteSaveForm=this.fb.group({
      id:[''],
      employee_id:[''],
      note : ['', [Validators.required]],
    })

    this.asyncInit();
  }

  asyncInit(){
  	this.commonService.create('/api/get-cv-by-search',{institute_id: this.institute_id})
  		.subscribe(data=>{
        this.commonService.callFooterMenu(1);
	      this.countries = data.countries;
	      this.cities = data.cities;
	      this.industries = data.industries;
	      this.searchcvResult = data.cvResults;
        this.total_cv_count = data.total_cv_count;
        this.courses = data.courses;
  		}, error=>{});
  }

  getsearchData(){
    this.showloader = true;
    let data={institute_id: this.institute_id, additional:this.searchForm};
      this.commonService.create('/api/get-cv-by-search',data)
     .subscribe((data)=>{
      this.searchcvResult = data.cvResults;
      this.showloader = false;
      this.searchFormCall =false;
      this.total_cv_count = data.total_cv_count;
      this.cities = data.cities;
      this.countries = data.countries;
       //console.log(this.searchcvResult);
     },(error)=>{});
  }

  checkIfAllSelected() {
    this.selectedAll = this.searchcvResult.every(function(item:any) {
        return item.selected == true;
      })
    //this.getCheckedItemList();
  }

  viewProfile(employee_id){
    console.log(employee_id);
    this.router.navigate(['/employer/profile-view/'+employee_id]);
  }

  setCvDetail(employee_id){
    let data = this.searchcvResult.find(obj=>obj.id ==employee_id);
    this.notedata = data.note.note;
    this.noteSaveForm.get('note').setValue(this.notedata);
    this.noteSaveForm.get('id').setValue(data.note._id);
  }

  showMobile(employee_id,i){
    this.showphonwArr[i] = true;
    this.commonService.getAll('/api/save-cv-history?employee_id='+employee_id)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

    viewHistory(employee_id){
       this.commonService.getAll('/api/view-cv-history?employee_id='+employee_id)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.employee_view_history = data.data;
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  reportCandidate(employee_id){

      $('#report_candidate_model').click();
      this.reportCandidateForm.get('employee_id').setValue(employee_id);
    }

  candidateReportSave(){
      this.commonService.create('/api/save-reported-candidate',this.reportCandidateForm.value)
     .subscribe((data)=>{
       if(data.status === 200){
            $("#close_reported_candidate_modal").click();
            this.message.success('Successfully Reported Candidate');
            this.reportCandidateForm.reset();
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
     },(error)=>{});
    }

  cvSaveFolder(){
    this.isSubmit =true;
    if(this.isSubmit && this.saveToFolderForm.invalid){
      
      return ;
    }
    this.commonService.create('/api/save-cv-to-folder', this.saveToFolderForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            $("#close_folder_save_modal").click();
            this.message.success('Successfully Save To Folder');
            this.saveToFolderForm.reset();
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  setsearchValue(event, type){
    this.searchFormCall =true;
    if(type=='country'){
      let count_list = Object.entries(this.countryModel);
      let countArr =[];
      count_list.forEach((value, i)=>{if(value[1] ==true){countArr.push(value[0]);}});
      console.log(countArr);
      this.searchForm.country_list = countArr;
    }else if(type=='city'){
      let city_list = Object.entries(this.cityModel);
      let cityArr =[];
      city_list.forEach((value, i)=>{if(value[1] ==true){cityArr.push(value[0]);}});
      this.searchForm.city_list = cityArr;
    }else if(type=='industry'){
      let indus_list = Object.entries(this.industryModel);
      let indusArr =[];
      indus_list.forEach((value, i)=>{if(value[1] ==true){indusArr.push(value[0]);}});
      this.searchForm.industry_list = indusArr;
    }else if(type=='course'){
      let course_list = Object.entries(this.courseModel);
      let courseArr =[];
      course_list.forEach((value, i)=>{if(value[1] ==true){courseArr.push(value[0]);}});
      this.searchForm.course_list = courseArr;
    }
    this.getsearchData();
  }

  sendMail(){
    this.checkedList = [];
    for (var i = 0; i < this.searchcvResult.length; i++) {
      if(this.searchcvResult[i].selected)
      this.checkedList.push(this.searchcvResult[i].id);
    }

     if(this.checkedList.length > 0){
       localStorage.setItem('mailUids',JSON.stringify(this.checkedList));
       this.router.navigate(['/employer/send-mail']);
       
      }else{
        Swal.fire({
          //title: 'Are you sure?',
          text: "Please select atleast one!",
          type: 'warning',
        });
      }
    }

  saveToFolder(){

    this.checkedList = [];
    for (var i = 0; i < this.searchcvResult.length; i++) {
      if(this.searchcvResult[i].selected)
      this.checkedList.push(this.searchcvResult[i].id);
    }
    
      if(this.checkedList.length > 0){
        $('#save_folder_model').click();
        this.saveToFolderForm.get('employee_ids').setValue(this.checkedList);
        
      }else{
        Swal.fire({
          //title: 'Are you sure?',
          text: "Please select atleast one!",
          type: 'warning',
        });
      }
  }

  downloadResume(){
    this.checkedList = [];
    for (var i = 0; i < this.searchcvResult.length; i++) {
      if(this.searchcvResult[i].selected)
      this.checkedList.push(this.searchcvResult[i].id);
    }

    if(this.checkedList.length > 0){
        this.commonService.getAll('/api/download-resume?employee_ids='+this.checkedList)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            const blob = new Blob([data.url], {
              type: 'application/zip'
            });
            const url = window.URL.createObjectURL(blob);
            window.open(url);

          }else if(data.status ==500){
            this.message.error(data.status_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
       
      }else{
        Swal.fire({
          //title: 'Are you sure?',
          text: "Please select atleast one!",
          type: 'warning',
        });
      }

  }

  selectAll() {
    for (var i = 0; i < this.searchcvResult.length; i++) {
      this.searchcvResult[i].selected = this.selectedAll;
    }
    //this.getCheckedItemList();
  }
}
