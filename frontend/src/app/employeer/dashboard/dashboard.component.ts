import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';
declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
	profileUpdateForm: FormGroup;
	countries: any;
  cities:any;
  selectedCities: any=[];
  industries:any;
  companySizes:any;
  companyTypes:any;
  galleries: any={ files:[], previews:[]};
  remove_image_id: any=[];
  profileuploadImage: string;
  companyUploadLogoImage: string;
  submitted : boolean = false;
  user: any;
  announcements: any=[];
  referrals: any=[];
  recent_jobs: any=[];
  recent_jobs_count: number;
  new_cv_received: number;
  total_closed_job: number;
  total_cv_received: number;
  total_open_job: number;
  cv_reviewed: number;
  premium_job_post_service: any;
  cv_download_view_service: any;
  mass_mail_service_service: any;
  international_walk_in_interview_service: any;
  carrer_url:string;
  carrer_company_name:string;
  uploadDocumentName: string;
  isLicenseAgree: boolean =false;
  coutryWiseCity: any;
  trigerData: any;
  isloggedIn: boolean;
  showEmployer: boolean;
  userUniqueValidErr: any = '';
  openProfileModal:boolean = false;
  constructor(private commonservice: CommonService,private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router) { 
  	this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
    //console.log(this.user);
    this.isloggedIn = localStorage.getItem('token') ? true : false;
  	this.asyncInit();
  }

  ngOnInit() {

    
  	if(this.user.profile_update == 0){
      if(document.querySelector('link[href$="materialize.min.css"]')){
      document.querySelector('link[href$="materialize.min.css"]').remove()
      }
      this.openProfileModal = true
	  	this.commonservice.checkProfileUpdate.subscribe(data=>{
	  		$('#profile_check_modal').click();
	  	});
  	}else{
      if(!document.querySelector('link[href$="materialize.min.css"]')){
        this.loadCss('assets/css/materialize.min.css')
      }
    }

  	this.profileUpdateForm =this.fb.group({
      id:'',
  		company_name:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		designation:['',[Validators.required]],
  		industry_id:['',[Validators.required]],
  		address:['', [Validators.required]],
  		zipcode:['', [Validators.required]],
  		alternate_ph_no:['', [Validators.required, Validators.pattern('^(\\+)?(\\d+)$'),Validators.minLength(8),Validators.maxLength(20)]],
  		company_type:['', [Validators.required]],
  		company_size:['', [Validators.required]],
  		company_logo:['', [Validators.required]],
  		company_tagline:['', [Validators.required, Validators.maxLength(200)]],
  		company_short_desc:['', [Validators.required, Validators.maxLength(1000)]],
      company_website :['', [Validators.required]],
      company_email :['', [Validators.required,Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      business_licence_type: [''],
      business_licence :[''],
      license_agree: '',
      user_unique_code: [''],
      
  	});

    this.profileUpdateForm.controls['country_id'].valueChanges.subscribe(value=>{
      if(this.countries){
      let countryCode =  this.countries.find(obj=>obj._id === value) ? this.countries.find(obj=>obj._id === value).code :'';
      this.selectedCities = this.cities.filter(obj=>obj.country_code == countryCode);        
      }

    });
    this.profileUpdateForm.get('license_agree').valueChanges.subscribe(value=>{
    this.isLicenseAgree = value;
  });

    this.profileUpdateForm.patchValue({
      id:this.user.id,
      name:this.user.user_detail.name,
      first_name:this.user.user_detail.first_name,
      last_name:this.user.user_detail.last_name,
      email:this.user.email,
      phone:this.user.phone_no,
      password:'',
      conf_password:'',
      company_name:this.user.user_detail.company_name,
      country_id:!this.user.user_detail.country_id?'': this.user.user_detail.country_id,
      city_id:!this.user.user_detail.city_id?'': this.user.user_detail.city_id,
      address:this.user.user_detail.address,
      zipcode:this.user.user_detail.zipcode,
      alternate_ph_no:this.user.user_detail.alternate_ph_no,
      company_type:!this.user.user_detail.company_type ? '':this.user.user_detail.company_type,
      company_size:!this.user.user_detail.company_size ?'':this.user.user_detail.company_size,
      company_logo:this.user.user_detail.company_logo,
      company_tagline:this.user.user_detail.company_tagline,
      company_short_desc:this.user.user_detail.company_short_desc,
      nearby_place:this.user.user_detail.nearby_place,
      element:this.user.user_detail.element,
      designation:this.user.user_detail.designation,
      industry_id:!this.user.user_detail.industry?'': this.user.user_detail.industry,
      profile_picture:this.user.user_detail.profile_picture,
      company_website:(this.user.user_detail.company_website && this.user.user_detail.company_website != 'null') ? this.user.user_detail.company_website:'',
      company_email:(this.user.user_detail.company_email && this.user.user_detail.company_email != 'null') ? this.user.user_detail.company_email:'',
      video_link: this.user.video_link ?this.user.video_link.name:'',
      business_licence: this.user.user_detail.business_licence ?this.user.user_detail.business_licence:'',
      business_licence_type:  this.user.user_detail.business_licence_type ? this.user.user_detail.business_licence_type:'',
      license_agree:  this.user.user_detail.license_agree ? this.user.user_detail.license_agree:''
      //video_link: this.user.video_link
    });
    this.profileUpdateForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.commonservice.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){
            this.coutryWiseCity = data.data
         }
        })
        //this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });
  }

  ngOnDestroy() {

    }


  public loadCss(url: string){
    const body = <HTMLDivElement> document.body;
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    body.appendChild(link);
  }

  asyncInit(){
    this.commonservice.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.industries = data['all_industries'].filter(obj=>obj.name !==""); 
        this.companySizes = data['companySizes'];
        this.companyTypes = data['companyTypes'];
        if(this.user.user_detail.country_id){
          let country = this.countries.find(obj=>obj._id == this.user.user_detail.country_id);
          this.commonservice.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
          if(data.status == 200){ 
            this.coutryWiseCity =  data.data

            }
          })     
        }        
      }
    });

      this.commonservice.getCityData.subscribe(data=>{
        if(data && data.status ==200){
           this.cities = data['cities'];
        }
      });

        this.commonservice.getCommonData.subscribe(data=>{
          if(data && data.status ==200){
              this.referrals = data['benefit_data'].filter(obj=>obj.type ==1 && obj.entity_id == this.user.role._id);
              this.announcements = data['benefit_data'].filter(obj=>obj.type ==2 && obj.entity_id == this.user.role._id);

          }
        });

        this.commonservice.getAll('/api/employer-dashboard').subscribe((data)=>{
          this.commonservice.callFooterMenu(1);
          this.recent_jobs = data.data.recent_jobs;
          this.recent_jobs_count = data.data.recent_jobs_count;
          this.new_cv_received = data.data.new_cv_received;
          this.total_closed_job = data.data.total_closed_job;
          this.total_cv_received = data.data.total_cv_received;
          this.total_open_job = data.data.total_open_job;
          this.cv_reviewed = data.data.cv_reviewed;
          this.premium_job_post_service = data.data.premium_job_post_service[0];
          this.cv_download_view_service = data.data.cv_download_view_service[0];
          this.mass_mail_service_service = data.data.mass_mail_service_service[0];
          this.international_walk_in_interview_service = data.data.international_walk_in_interview_service[0];
          let carrerData =  data.data.carrerdata;
          this.carrer_company_name = carrerData.company_name;
          this.carrer_url = location.origin+'/career-page/'+carrerData.company_name+'?ec='+btoa(carrerData.user_id);
          console.log(this.carrer_url);
        },(error)=>{});
  }

  companyLogo(logo){
    const file = logo.files[0];
    const filesize = Math.round((file.size / 1024));
    const filetype = file.type;
    if(filesize > 2000 ){
      //alert("file size lese then 2 MB");
      this.message.error('Maximum file size allowed 2 MB');
    }else if(filetype != 'image/png'){
      //alert("Image must be png");
      this.message.error('File type must be png');
    }else{
      this.profileUpdateForm.controls['company_logo'].setValue(file);
       const reader = new FileReader();
      reader.onload = (event:any) => {
        this.companyUploadLogoImage = event.target.result;
      }
      reader.readAsDataURL(file);
    }
    // console.log(Math.round((file.size / 1024)));
    // console.log('size', file.size);
    // console.log('type', file.type);  

  }

  removePicture(type){
    if(type =='profile_picture'){
      this.profileUpdateForm.controls['profile_picture'].setValue('');
      this.profileuploadImage ='';
    }else if(type=="company_logo"){
      this.profileUpdateForm.controls['company_logo'].setValue('');
      this.profileuploadImage ='';
    }
  }
  processFile(imageInput: any){
    console.log(this.galleries.previews.length);
    if(this.galleries.previews.length > 3){
      this.message.error('Cannot Upload morethan 4 image!');
      return false;
    }
    const file = imageInput.files[0];
    const reader = new FileReader();
    let rand_id = Math.random();
    reader.onload = (event:any) => {
      this.galleries.previews.push({'url':event.target.result, 'type':'upload','id':rand_id});
    }
    reader.readAsDataURL(file);
    this.galleries.files.push({id:rand_id,'file':file});
    console.log(this.galleries);
  }

  removeFile(id, type){
    let index = this.galleries.previews.findIndex(preview=>preview.id ==id);
    this.galleries.previews.splice(index,1);
    if(type =='upload'){
      let file_index = this.galleries.files.findIndex(file=>file.id ==id);
      this.galleries.files.splice(file_index,1);
    }else{
      this.remove_image_id.push(id);
    }
  }
  get f() { return this.profileUpdateForm.controls; }

  updateProfile(){
    this.submitted = true;
    if (this.profileUpdateForm.invalid) {
            return;
        }
    if(!this.isLicenseAgree){
      this.message.error('Please check declaration checked box');
      return false;
    }    
    const formData = new FormData();
    const formValue = this.profileUpdateForm.value;
    formData.append('id',formValue.id);
    formData.append('company_name',formValue.company_name);
    formData.append('country_id',formValue.country_id);
    formData.append('city_id',formValue.city_id);
    formData.append('address',formValue.address);
    formData.append('zipcode',formValue.zipcode);
    formData.append('alternate_ph_no',formValue.alternate_ph_no);
    formData.append('company_type',formValue.company_type);
    formData.append('company_size',formValue.company_size);
    formData.append('company_logo',formValue.company_logo);
    formData.append('company_tagline',formValue.company_tagline);
    formData.append('company_short_desc',formValue.company_short_desc);
    formData.append('designation',formValue.designation);
    formData.append('industry_id',formValue.industry_id);
    formData.append('remove_img_ids',this.remove_image_id);
    formData.append('business_licence',formValue.business_licence);
    formData.append('business_licence_type',formValue.business_licence_type);
    formData.append('license_agree',formValue.license_agree);
    formData.append('user_unique_code',formValue.user_unique_code);
    formData.append('company_website',formValue.company_website);
    formData.append('company_email',formValue.company_email);
    
    for(var i = 0; i < this.galleries.files.length; i++){
      formData.append('images_files[]',this.galleries.files[i].file);
    }

  	this.commonservice.create('/api/employer-quick-profile-update', formData)
  		.subscribe((data)=>{
        if(data.status == 200){
          this.openProfileModal = false;
          this.loadCss('assets/css/materialize.min.css')
          this.message.success(data.status_text);
          localStorage.setItem('user',JSON.stringify(data.user));
          this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
          this.commonservice.employer_check('update');
          $('#close_modal').click();
          this.remove_image_id.length =0;
        }else if(data.status == 500){
          this.message.error(data.status_text);
        }else if(data.status == 421){
          this.userUniqueValidErr = data.error;
        }else if(data.status == 422){
          if(Object.entries(data.error).length){
            Object.entries(data.error).forEach(obj=>{
              this.message.error(obj[1]);
            })
          }
          
        }
  		},(error)=>{});
  }

  copyUrl(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.message.success('copied!');
  }

    uploadeBusinessLicense(license){
    const file = license.files[0];
    const file_extence = file.name.split('.').pop().toLowerCase();
    this.uploadDocumentName = file.name;
    const filesize = Math.round((file.size / 1024));
    const filetype = file.type;
    if(filesize > 2000 ){
      this.message.error('Maximum file size allowed 2 MB');
      return false;
    }

     if(file_extence == 'jpg' || file_extence == 'png' || file_extence == 'jpeg' || file_extence == 'pdf' || file_extence == 'docx' || file_extence == 'doc'){
       this.profileUpdateForm.controls['business_licence'].setValue(file);
     } else {
      
      this.message.error('Upload file only allow jpg,png,jpeg,pdf,docx or doc!');
    }

  }

  logOut(){
     this.authService.logOut('/api/logout').subscribe((data)=>{
       if(data.status ==200){
         this.openProfileModal = false
         $('#close_modal').click();
         localStorage.removeItem('token');
         localStorage.removeItem('user');
         localStorage.removeItem('is_employer');
         this.trigerData ='';
         this.isloggedIn = false;
         this.showEmployer = false;
          localStorage.getItem('job_preview')? localStorage.removeItem('job_preview'):'';
          localStorage.getItem('international_job_preview')? localStorage.removeItem('international_job_preview'):'';
          localStorage.getItem('advSearch')? localStorage.removeItem('advSearch'):'';
          localStorage.getItem('search-queries')? localStorage.removeItem('search-queries'):'';
          localStorage.getItem('cvSearch')? localStorage.removeItem('cvSearch'):'';
          localStorage.getItem('walk_in_preview')? localStorage.removeItem('walk_in_preview'):'';
          //this.router.navigate(['']);
          //this.router.navigate([''])
           window.location.href ='/employer'
         /* .then(() => {
            window.location.reload();
          });*/
         

       }
     });
   }

}
