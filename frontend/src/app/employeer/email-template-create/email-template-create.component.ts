import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute} from '@angular/router';
let expdata =[];
@Component({
  selector: 'app-email-template-create',
  templateUrl: './email-template-create.component.html',
  styleUrls: ['./email-template-create.component.css']
})
export class EmailTemplateCreateComponent implements OnInit {
  emailForm:FormGroup;
  previousEmail:any={};
  previousTemplates:any=[];
  resourceUrl: string='/api/employer/email-template';
  language_pref_div:boolean;
  countries: any=[];
  cities: any=[];
  coutryWiseCity: any=[];
  experianceAray :any=[];
  langPrefArray: any=[];
  currencyArray: any=[];
  salaryArray: any=[];
  allEmailTemplate: any=[];
  template_id:any='';
  isSubmit: boolean;
  langPrefStore: any={
    value:[],
    name:[]
  };
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  skillData:any= [];
  skill_key:any;
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About This Job',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Benefits',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  responsconfig: any = {
    //height: '200px',
    placeholder: 'Primary Responsibility',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  noteconfig: any = {
    //height: '200px',
    placeholder: 'Special Note',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  constructor(private fb:FormBuilder, private message:MessageService,
    private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.template_id = this.activeRoute.snapshot.paramMap.get("id")?this.activeRoute.snapshot.paramMap.get("id"):'';
    this.language_pref_div = false;
    this.isSubmit = false;
    this.getData();
    console.log(this.template_id);
  }

  ngOnInit() {
  	this.emailForm = this.fb.group({
  		id:this.template_id,
  		template_name:['', [Validators.required]],
  		//cv_received_email:['', [Validators.required, Validators.pattern(this.emailPattern)]],
  		subject:['', [Validators.required]],
  		about_job:['', [Validators.required]],
  		skill:['', [Validators.required]],
  		benefit:['', [Validators.required]],
  		primary_responsibility:['', [Validators.required]],
  		special_note:['', [Validators.required]],
      job_title:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		min_exp:['', [Validators.required]],
  		max_exp:['', [Validators.required, this.checkMinMaxExp]],
  		language_pref:['', [Validators.required]],
  		salary_type:['', [Validators.required]],
  		currency_type:['', [Validators.required]],
  		min_salary:['', [Validators.required]],
  		max_salary:['', [Validators.required,this.checkMinMaxSal]],
  		previous_id:['', []],

  	});
    this.emailForm.controls.salary_type.setValue('monthly');
    this.emailForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
      this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      this.emailForm.controls['currency_type'].setValue(value);
      }
      
    });

     if(localStorage.getItem('template_preview')){
      let prevData = JSON.parse(localStorage.getItem('template_preview'));
      //console.log(prevData);
      this.setFormValue(prevData);
    }
  }



  getData(){

    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
        this.langPrefArray = data['alllanguage'];
        this.experianceAray = data['allExprience'];
        this.salaryArray = data['other_mst'].filter((salary)=>salary.entity ==='salary');
        this.currencyArray =  data['all_currency'];   
        this.skillData = data['skills'].map((obj) => { return { display: obj.skill, value: obj.skill } });
        expdata = this.experianceAray;
      }
    });
    if(this.template_id){
    	this.commonService.getAll(this.resourceUrl+'/'+this.template_id)
    		.subscribe((data)=>{
    			if(data.status ==200){
            this.skillData = data.data.employer_job.required_skill.map((obj) => { return { display: obj, value: obj } });
            this.allEmailTemplate = data.allEmailTemplate;
            this.setEditForm(data.data);
          }
    	},(error)=>{

    	});
    }else{
      this.commonService.getAll(this.resourceUrl)
        .subscribe((data)=>{
          if(data.status ==200){
            this.previousTemplates = data.data;
            this.allEmailTemplate = data.allEmailTemplate;
            if(localStorage.getItem('template_preview')){
              let prevData = JSON.parse(localStorage.getItem('template_preview'));
              this.langPrefStore.value = prevData.language_pref;
              this.langPrefStore.name = this.langPrefArray.filter(obj=> prevData.language_pref.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
              this.emailForm.get('language_pref').setValue(this.langPrefStore.value);
              let country = this.countries.find(obj=>obj._id == prevData.country_id);
              this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
            }
          }
      },(error)=>{

      });
    }
  }

  templateOnChange(template_id){
    //console.log(template_id);
    if(template_id == 'new'){
      this.resetForm();
      this.router.navigate(['employer/email-template/create']);
    }else{
      let data = this.allEmailTemplate.find((template) => template._id == template_id);
      this.setEditForm(data);
    }
    //console.log(data);
  }

  // clearForm() {
  //   this.emailForm.reset();
  // }

  saveForm(){

    this.emailForm.patchValue({
      language_pref:this.langPrefStore.value
    });

    let skills = this.skill_key.map(obj => obj.value);
    skills ? this.emailForm.get('skill').setValue(skills) : '';

    this.isSubmit =true;
    if(this.emailForm.invalid){
      return ;
    }

  	this.commonService.create(this.resourceUrl, this.emailForm.value)
  		.subscribe((data)=>{
  			this.isSubmit = false;
  			if(data.status ==200){
  				this.previousTemplates.push(data);
          		this.message.success(data.status_text);
  			}else if(data.status ==422){
          
  			}else{
  				this.message.error(data.status_text);
  			}
  			this.router.navigate(['employer/email-template']);
  		}, (error)=>{

  		});
  	}

  	showForm(){
	  	this.commonService.show(this.resourceUrl, this.emailForm.value.id)
	  		.subscribe((data)=>{
	  			this.setFormValue(data);
	  		}, (error)=>{});
  	}

  updateForm(){
    this.emailForm.patchValue({
      language_pref:this.langPrefStore.value
    });

    let skills = this.skill_key.map(obj => obj.value);
    skills ? this.emailForm.get('skill').setValue(skills) : '';

    this.isSubmit =true;
    if(this.emailForm.invalid){
      return ;
    }
  	this.commonService.update(this.resourceUrl, this.emailForm.value.id,this.emailForm.value)
  		.subscribe((data)=>{
  			if(data.status ==200){
          this.message.success('Successfully updated');
  				this.router.navigate(['employer/email-template']);
  			}
        
  		}, (error)=>{});
  }

  setFormValue(data){
    this.skill_key = data.skill.map((obj) => { return { value: obj, display: obj } });
    this.langPrefStore.value = data.language_pref;
  	this.emailForm.patchValue({
		id:data.id,
		template_name:data.template_name,
		//cv_received_email:data.cv_received_email,
		subject:data.subject,
		about_job:data.about_job,
		skill:data.skill,
		benefit:data.benefit,
		primary_responsibility:data.primary_responsibility,
		special_note:data.special_note,
		country_id:data.country_id,
		city_id:data.city_id,
    job_title:data.job_title,
		min_exp:data.min_exp,
		max_exp:data.max_exp,
		salary_type:data.salary_type,
		currency_type:data.currency_type,
		min_salary:data.min_salary,
		max_salary:data.max_salary,
	});

  }

  setEditForm(data){
      this.skill_key = data.employer_job.required_skill.map((obj) => { return { value: obj, display: obj } });
      this.langPrefStore.value = data.employer_job.language_id;
      this.langPrefStore.name =  this.langPrefArray.filter(obj=> data.employer_job.language_id.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
      this.emailForm.patchValue({
      //id:data._id,
      template_name:data.template_name,
      //cv_received_email:data.employer_job.received_email,
      subject:data.subject,
      about_job:data.about_job,
      skill:data.employer_job.skill,
      benefit:data.employer_job.benefits,
      primary_responsibility:data.employer_job.primary_responsibility,
      special_note:data.employer_job.special_notes,
      country_id:data.employer_job.country_id,
      city_id:data.employer_job.city_id,
      job_title:data.employer_job.job_title,
      min_exp:data.employer_job.min_experiance,
      max_exp:data.employer_job.max_experiance,  
      language_pref:this.langPrefStore.value,
      salary_type:data.employer_job.salary_type,
      currency_type:data.employer_job.currency,
      min_salary:data.employer_job.min_monthly_salary,
      max_salary:data.employer_job.max_monthly_salary,
    });
  }

  showDropdown(){
    this.language_pref_div = true;
  }

  dropDownToggle(e){
    //console.log(e);
    this.language_pref_div = e.status;
  }

  getDropDownData(e){
    if(e.event){
      let index = this.langPrefStore.value.indexOf(e.value);
      if(index  ==-1){
        this.langPrefStore.value.push(e.value);
        this.langPrefStore.name.push(e.name);
      }
    }else{
      let index = this.langPrefStore.value.indexOf(e.value);
      if(index !=-1){
        this.langPrefStore.value.splice(index,1);
        this.langPrefStore.name.splice(index,1);
      }
    }
    this.emailForm.patchValue({
      language_pref: this.langPrefStore.value
    });
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "language":
       this.langPrefStore.value.splice(indx, 1);
       this.langPrefStore.name.splice(indx, 1);
       this.emailForm.patchValue({
        language_pref: this.langPrefStore.value
        });
        break;   
      default:
        // code...
        break;
    }
  }

  modalCall(){
  	this.commonService.callMaterializeModal('call');
  }

  templatOnChange(template_id){
  	let data = this.previousTemplates.find((jb)=>jb.id ==template_id);
    this.setFormValue(data);
  }

  templatePreview() {
    //console.log(this.skill_key);
    let skills = this.skill_key&&this.skill_key.map(obj => obj.value);
    skills ? this.emailForm.get('skill').setValue(skills) : '';

    this.isSubmit =true;
    if(this.emailForm.invalid){
      return ;
    }
    this.emailForm.patchValue({
      language_pref:this.langPrefStore.value
    });

    localStorage.setItem('template_preview',JSON.stringify(this.emailForm.value)); 
    this.router.navigate(['/employer/email-template/preview']);
  }

  resetForm(){
    this.emailForm.reset();
    this.langPrefStore.value.length =0;
    this.langPrefStore.name.length =0;
    this.skill_key ='';
    this.emailForm.patchValue({
      country_id:'',
      city_id:'',
      min_exp:'',
      max_exp:'',
      currency_type:''
    });
    this.emailForm.controls.salary_type.setValue('monthly');
  }

  checkMinMaxSal(c: FormControl) {
    if (!c.root && !c.root.get('min_salary')) {
      return null;
    }
    if (c.value && c.value < c.root.get('min_salary').value) {
      return { error_min_max: true };
    }

  }

  checkMinMaxExp(c: FormControl) {
    if (!c.root && !c.root.get('min_exp')) {
      return null;
    }
    if(c.value){
      console.log(c.value);
      let minExpValueOrder = expdata.find(obj=>obj._id == c.root.get('min_exp').value);
      let maxExpValueOrder = expdata.find(obj=>obj._id == c.value);
      if(maxExpValueOrder && maxExpValueOrder.order < minExpValueOrder.order){
        return { error_min_max_exp: true };
      }
    }
  }

}
