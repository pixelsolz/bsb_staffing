import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute} from '@angular/router';
import {Global} from '../../global';

@Component({
  selector: 'app-email-template-preview',
  templateUrl: './email-template-preview.component.html',
  styleUrls: ['./email-template-preview.component.css']
})
export class EmailTemplatePreviewComponent implements OnInit {
  previewData: any;
  userData: any;
  templateData: any={
  	comp_name:'',
  	comp_email:'',
  	employer_name:'',
  	company_type:'',
  	company_address:'',
  	company_size:'',
  	website:'',
  	comp_mobile:'',
  	job_title:'',
  	min_exp: '',
  	max_exp: '',
  	min_salary: '',
  	max_salary: '',
  	job_country:'',
  	job_city: '',
  	about_job:'',
  	benefit:'',
  	currency_type:'',
  	language_pref:'',
  	skill:'',
  	subject:'',
  	template_name:'',
  	responsibility:'',
	salary_type:'',
	special_note:'',
	id:'',

  };
  baseUrlPath:string;

  constructor(private global: Global,private commonService: CommonService, private message:MessageService, private router: Router) { 
  	this.previewData = JSON.parse(localStorage.getItem('template_preview'));
	  this.userData = JSON.parse(localStorage.getItem('user'));
     this.baseUrlPath = this.global.baseUrl
  	//localStorage.removeItem('template_preview');
  }

  ngOnInit() {
  	this.asyncInit();
  }

  asyncInit(){
    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data.status ==200){
        this.setDataValue(data);
      }
    });
  	// this.commonService.getAll('/api/employer/email-template/common-data')
  	// 	.subscribe(data=>{
  	// 		//console.log(data);
  	// 		this.setDataValue(data);
  	// 	},error=>{});
  }

  setDataValue(data){
	  this.templateData.id = this.previewData.id;  
  	this.templateData.comp_name = this.userData.user_detail.company_name;
  	this.templateData.comp_email = this.userData.user_detail.company_email;
  	this.templateData.employer_name = this.userData.user_detail.name;
  	this.templateData.company_type = this.userData.user_detail.company_type_details.company_type;
  	this.templateData.company_address = this.userData.user_detail.address;
  	this.templateData.company_size = this.userData.user_detail.company_size_details.company_size;
  	this.templateData.website = this.userData.user_detail.company_website;
  	this.templateData.comp_mobile = this.userData.phone_no;
  	this.templateData.job_title = this.previewData.job_title;
  	this.templateData.min_exp = data['allExprience'].find(obj=>obj._id == this.previewData.min_exp).name;
  	this.templateData.max_exp = data['allExprience'].find(obj=>obj._id == this.previewData.max_exp).name;
  	this.templateData.min_salary = this.previewData.min_salary;
  	this.templateData.max_salary = this.previewData.max_salary;
  	this.templateData.job_country = data['countries'].find(obj=>obj._id == this.previewData.country_id).name;
  	this.templateData.job_city = data['cities'].find(obj=>obj._id == this.previewData.city_id).name;
  	this.templateData.about_job = this.previewData.about_job;
  	this.templateData.benefit = this.previewData.benefit;
  	//this.templateData.salary_type = data.countries.filter(obj=>obj.entity =='salary').find(obj=>obj._id ==this.previewData.salary_type).name;
  	this.templateData.language_pref = data['alllanguage'].filter(obj=> this.previewData.language_pref.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
  	this.templateData.skill = this.previewData.skill;
  	this.templateData.subject = this.previewData.subject;
  	this.templateData.template_name = this.previewData.template_name;
	this.templateData.responsibility = this.previewData.primary_responsibility;
	this.templateData.salary_type = this.previewData.salary_type;
	this.templateData.special_note = this.previewData.special_note;
	this.templateData.currency_type = data.countries.find(obj=>obj._id == this.previewData.currency_type).currency_code;
  }

  saveForm(){
  	this.commonService.create('/api/employer/email-template', this.previewData)
  		.subscribe((data)=>{
  			if(data.status ==200){
          		this.message.success(data.status_text);
          		localStorage.removeItem('template_preview');
          		this.router.navigate(['employer/email-template']);
  			}else{
          if(data.error =='exist'){
            this.message.error('Same template already exist');
          }else{
            this.message.error(data.status_text);
          }
  				
  			}
  			
  		}, (error)=>{

  		});
  }
  updateForm(){
   
    //console.log(this.emailForm.value.id);
  	this.commonService.update('/api/employer/email-template', this.previewData.id,this.previewData)
  		.subscribe((data)=>{
  			if(data.status ==200){
				this.message.success('Successfully updated');
				localStorage.removeItem('template_preview');
  				this.router.navigate(['employer/email-template']);
  			}
        
  		}, (error)=>{});
  }


  editTemplate(){
  	this.router.navigate(['employer/email-template/create']);
  }

}
