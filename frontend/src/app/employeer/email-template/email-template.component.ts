import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';
declare var Swal: any;
@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.css']
})
export class EmailTemplateComponent implements OnInit {
  count_data:number;
  active_page: number;
  alltemplates: any=[];
  selectedids:any=[];
  resourceUrl: string='/api/employer/email-template';
  constructor(private fb:FormBuilder, private message:MessageService,
    private router: Router, private commonService: CommonService) { 
    this.count_data =0;
    this.active_page =0;
    localStorage.removeItem('template_preview');
  }

  ngOnInit() {
  	this.getData();
  }

  getData(){
  	this.commonService.getAll(this.resourceUrl+'?off_set='+this.active_page)
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.count_data = data.data_count;
  			this.alltemplates = data.data;
        this.pagination();
  		}, (error)=>{});
  }

  getSearchData(index){
    this.active_page =index;
    this.commonService.getAll(this.resourceUrl+'?off_set='+this.active_page)
      .subscribe((data)=>{
        //this.count_data = data.data_count;
        this.alltemplates = data.data;
        this.pagination();
      }, (error)=>{});
  }

  pagination(){
    let tot_number =  (Number(this.count_data) /25);
    var items: number[] = [];
    for(var i = 1; i <= tot_number; i++){
       items.push(i);
    }
    return items;
  }

  paginationCall(index){
    this.active_page = index;
    this.getSearchData(index);
  }

  editData(event, id, index){
    console.log(id);
  }

  deleteData(event, id, index){
    console.log(id);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonService.delete(this.resourceUrl, id)
          .subscribe((data)=>{
          if(data.status ==200){   
             this.alltemplates.splice(index,1);          
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
           }
          },(error)=>{});

      }
          
    });   
  }

  deleteAll(){
    this.selectedids = this.alltemplates.filter(_ => _.selected);
    var items: number[] = [];
    this.selectedids.forEach((val, i)=>{
      items.push(val.id); 
    });
    this.commonService.create(this.resourceUrl+'/deleteall', items)
      .subscribe((data)=>{
          this.selectedids.forEach((val, i)=>{
            let index = this.alltemplates.findIndex(obj =>obj.id == val.id);
            this.alltemplates.splice(index,1); 
          });
      }, (error)=>{

      });
  }

  toggleCheck(){

  }

}
