import { Component, OnInit, Renderer2, Renderer} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router ,ActivatedRoute, NavigationEnd} from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

declare var Swal: any;
declare var $;

@Component({
  selector: 'app-employee-profile-view',
  templateUrl: './employee-profile-view.component.html',
  styleUrls: ['./employee-profile-view.component.css']
})
export class EmployeeProfileViewComponent implements OnInit {
  userPhotoOptions: any;
  userPhotoCarouselOptions: any;
	employee_id:string;
	showloader :boolean;
	employee_details:any;
	cvLink :string;
	isLoading: boolean= false;
  similar_profiles: any=[];
  employee_photos: any=[];
  employeeVideos: any=[];
  employment_history: any=[];
  employee_education: any=[];
  employee_skill: any=[];
  employee_certification: any=[];
  employee_language: any=[];
  emp_his_data: any;
  show_all_his: boolean =false;
  read_more_value:string ='';
	
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute,private message:MessageService,private sanitizer: DomSanitizer) { 

    this.owlCoursolCall();
  	this.employee_id = this.activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';
  	//console.log(this.employee_id);
  	this.isLoading = true;
  	if(this.employee_id){
          this.commonService.show('/api/employee-profile', this.employee_id)
              .subscribe((data)=>{
                 this.commonService.callFooterMenu(1);
              		this.isLoading =false;
                	this.employee_details = data.data;
                	//console.log(this.employee_details);
                	this.cvLink = data.data.cv;
                  this.similar_profiles = data.similar_profile;
                  this.employee_photos=data.employee_photos;
                  this.emp_his_data=data.employment_history;
                  this.employment_history =  this.emp_his_data.length > 2  ?this.emp_his_data.slice(0,2): this.emp_his_data;
                  this.employee_education = data.employee_education;
                  this.employee_skill = data.employee_skill;
                  this.employee_certification = data.employee_certification;
                  this.employee_language = data.employee_language;
                	//this.cvLink = 'https://file-examples.com/wp-content/uploads/2017/02/file-sample_100kB.docx';
                	//console.log(this.employee_details);
                  this.employeeVideos = data.employee_videos.map(video => {
                    return {
                      id: video._id,
                      description: video.description,
                      video_link: this.sanitizer.bypassSecurityTrustResourceUrl(video.video_link.replace("watch?v=", "embed/"))
                    };
                  });
                this.owlCoursolCall();
              }, (error)=>{

              });
        }

  }

  owlCoursolCall() {
    this.userPhotoOptions = { items: 3, dots: false, nav: true, autoplay: false, loop: false, margin: 10, responsive: { 0: { items: 1 }, 600: { items: 2 }, 1000: { items: 3 } } };
    this.userPhotoCarouselOptions = { items: 1, dots: false, nav: true, autoplay: true, loop: true, margin: 0, responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 5 } } };
  }

  ngOnInit() {

  }

  likeProfile(employee_id){
  	this.commonService.getAll('/api/employee/like-profile?id='+employee_id)
     .subscribe((data)=>{
       if(data.status === 200){
            this.message.success(data.status_text);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }else if(data.status == 422){
            this.message.error(data.status_text);
          }
     },(error)=>{});

  }

  downloadResume(){
    this.commonService.getAll('/api/employee/profile/cv-download/' + this.employee_id)
        .subscribe(data=>{

        },error=>{});
  }

  showAllEmployeementHis(){
    this.show_all_his = true;
    this.employment_history = this.emp_his_data;
  }

  readMore(read_more){

    $('#read_more_model').click();
    this.read_more_value = read_more;
      //this.reportCandidateForm.get('employee_id').setValue(employee_id);
    }

}
