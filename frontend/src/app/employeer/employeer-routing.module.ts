import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobPostedComponent } from './job-posted/job-posted.component';
import { JobPostedListComponent } from './job-posted-list/job-posted-list.component';
import { ApplicationRecievedComponent } from './application-recieved/application-recieved.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './../auth.guard';
import {ProfileCheckGuard} from './../profile-check.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SimpleSearchComponent } from './simple-search/simple-search.component';
import { AdvanceSearchComponent } from './advance-search/advance-search.component';
import { PeopleSearchComponent } from './people-search/people-search.component';
import { JobPostPremiumComponent } from './job-post-premium/job-post-premium.component';
import { JobPostPremiumListComponent } from './job-post-premium-list/job-post-premium-list.component';
import { WalkInInterviewLocalComponent } from './walk-in-interview-local/walk-in-interview-local.component';
import { WalkInInterviewLocalListComponent } from './walk-in-interview-local-list/walk-in-interview-local-list.component';
import { WalkInInterviewInternationalComponent } from './walk-in-interview-international/walk-in-interview-international.component';
import { WalkInInterviewInternationalListComponent } from './walk-in-interview-international-list/walk-in-interview-international-list.component';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { EmailTemplateCreateComponent } from './email-template-create/email-template-create.component';
import { ResumesComponent } from './resumes/resumes.component';
import { ApplicationEditComponent } from './application-edit/application-edit.component';
import { CustomiseServicesComponent } from './customise-services/customise-services.component';
import { ResumesWithoutLoginComponent } from './resumes-without-login/resumes-without-login.component';
import { ManageFolderComponent } from './manage-folder/manage-folder.component';
import { FolderViewComponent } from './folder-view/folder-view.component';
import { EmployeeProfileViewComponent } from './employee-profile-view/employee-profile-view.component';
import { SearchHistoryComponent } from './search-history/search-history.component';
import { SendMailUsingTemplateComponent } from './send-mail-using-template/send-mail-using-template.component';
import { CollegeListComponent } from './college-list/college-list.component';
import { RecentPostedJobComponent } from './recent-posted-job/recent-posted-job.component';
import { CollegeDetailComponent } from './college-detail/college-detail.component';
import { CvsearchCollegeComponent } from './cvsearch-college/cvsearch-college.component';
import { MailboxComponent } from './mailbox/mailbox.component';
import { ServiceComponent } from './service/service.component';
import { CareerPageComponent } from './career-page/career-page.component';
import { EmailTemplatePreviewComponent } from './email-template-preview/email-template-preview.component';
import { ViewSimilarProfileComponent } from './view-similar-profile/view-similar-profile.component';
import { JobPostPreviewComponent } from './job-post-preview/job-post-preview.component';
import { WalkInInterviewPreviewComponent } from './walk-in-interview-preview/walk-in-interview-preview.component';
import { SendMailPreviewComponent } from './send-mail-preview/send-mail-preview.component';
import { CarrerWidgetComponent } from './carrer-widget/carrer-widget.component';
import { FreeInternationalJobComponent } from './free-international-job/free-international-job.component';
import { FreeInternationalJobListComponent } from './free-international-job-list/free-international-job-list.component';
import { PremiumInternationalJobComponent } from './premium-international-job/premium-international-job.component';
import { PremiumInternationalJobListComponent } from './premium-international-job-list/premium-international-job-list.component';
import { InternationalJobPreviewComponent } from './international-job-preview/international-job-preview.component';
//import { CareerPageViewComponent } from './career-page-view/career-page-view.component';
import { ServiceMoreDetailsComponent } from './service-more-details/service-more-details.component';
const routes: Routes = [
	//{path:'', loadChildren:'../common-uses/common-uses.module#CommonUsesModule'},
	{path:'', canActivate: [AuthGuard], component:HomeComponent},
    {path:'forgotpassword', component: HomeComponent},
	{path:'profile', canActivate: [AuthGuard],component:ProfileComponent},
	//{path:'', loadChildren:'../common-uses/common-uses.module#CommonUsesModule'},
	//{path:'', loadChildren:'../auth/auth.module#AuthModule'},
	{path:'', loadChildren:'../cms/cms.module#CmsModule'},
	{path:'', loadChildren:'../email/email.module#EmailModule'},
	{path:'dashboard', canActivate: [AuthGuard, ProfileCheckGuard], component:DashboardComponent},
	{path:'job-posted', canActivate: [AuthGuard, ProfileCheckGuard], component:JobPostedComponent},
	{path:'job-posted-edit/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:JobPostedComponent},
	{path:'job-posted-list', canActivate: [AuthGuard, ProfileCheckGuard], component:JobPostedListComponent},
	{path:'job-posted-premium', canActivate: [AuthGuard, ProfileCheckGuard], component:JobPostPremiumComponent},
	{path:'job-posted-premium-edit/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:JobPostPremiumComponent},
	{path:'job-posted-premium-list', canActivate: [AuthGuard, ProfileCheckGuard], component:JobPostPremiumListComponent},
	{path:'application-recieved', canActivate: [AuthGuard, ProfileCheckGuard], component:ApplicationRecievedComponent},
	{path:'application-edit/:type/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:ApplicationEditComponent},
	{path:'simple-search', canActivate: [AuthGuard], component:SimpleSearchComponent},
	{path:'advance-search', canActivate: [AuthGuard], component:AdvanceSearchComponent},
	{path:'people-search', canActivate: [AuthGuard], component:PeopleSearchComponent},
	{path:'walk-in-interview-local', canActivate: [AuthGuard, ProfileCheckGuard], component:WalkInInterviewLocalComponent},
	{path:'walk-in-interview-local-edit/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:WalkInInterviewLocalComponent},
	{path:'walk-in-interview-local-list', canActivate: [AuthGuard, ProfileCheckGuard], component:WalkInInterviewLocalListComponent},
	{path:'walk-in-interview-international', canActivate: [AuthGuard, ProfileCheckGuard], component:WalkInInterviewInternationalComponent},
	{path:'walk-in-interview-international-edit/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:WalkInInterviewInternationalComponent},
	{path:'walk-in-interview-international-list', canActivate: [AuthGuard, ProfileCheckGuard], component:WalkInInterviewInternationalListComponent},
	{path:'email-template/create',canActivate: [AuthGuard],component:EmailTemplateCreateComponent},
	{path:'email-template/edit/:id',canActivate: [AuthGuard],component:EmailTemplateCreateComponent},
	{path:'email-template',canActivate: [AuthGuard],component:EmailTemplateComponent},
	{path:'email-template/preview',canActivate: [AuthGuard], component: EmailTemplatePreviewComponent},
	{path:'resumes', canActivate: [AuthGuard], component:ResumesComponent},
	{path:'customise-services', component:CustomiseServicesComponent},
	{path:'resumes-list', component:ResumesWithoutLoginComponent},
	{path:'folders-list', canActivate: [AuthGuard],component:ManageFolderComponent},
	{path:'folders-view/:id', canActivate: [AuthGuard],component:FolderViewComponent},
	{path:'profile-view/:id', canActivate: [AuthGuard],component:EmployeeProfileViewComponent},
	{path:'send-mail', canActivate: [AuthGuard],component:SendMailUsingTemplateComponent},
	{path:'search-history',canActivate: [AuthGuard], component:SearchHistoryComponent},
	{path:'college-list',canActivate: [AuthGuard], component:CollegeListComponent},
	{path:'recent-posted-job',canActivate: [AuthGuard], component:RecentPostedJobComponent},
	{path:'college-detail/:id', canActivate: [AuthGuard], component:CollegeDetailComponent},
	{path:'cvsearch-college/:id', canActivate: [AuthGuard], component:CvsearchCollegeComponent},
	{path:'service', canActivate: [AuthGuard], component:ServiceComponent},
	{path:'career', canActivate: [AuthGuard], component:CareerPageComponent},
	{path: 'view/similar-profile/:emp_id', canActivate: [AuthGuard], component:ViewSimilarProfileComponent},
	{path: 'job-post/preview', canActivate: [AuthGuard], component:JobPostPreviewComponent},
	{path: 'walk-in-interview/preview', canActivate: [AuthGuard], component:WalkInInterviewPreviewComponent},
	{path: 'email/preview', canActivate: [AuthGuard], component:SendMailPreviewComponent},
	{path:'career/widget/:empr_id', component:CarrerWidgetComponent},

	{path:'free-international-job/create', canActivate: [AuthGuard, ProfileCheckGuard], component:FreeInternationalJobComponent},
	{path:'free-international-job/edit/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:FreeInternationalJobComponent},
	{path:'free-international/list', canActivate: [AuthGuard, ProfileCheckGuard], component:FreeInternationalJobListComponent},
	{path:'premium-international-job/create', canActivate: [AuthGuard, ProfileCheckGuard], component:PremiumInternationalJobComponent},
	{path:'premium-international-job/edit/:id', canActivate: [AuthGuard, ProfileCheckGuard], component:PremiumInternationalJobComponent},
	{path:'premium-international-job/list', canActivate: [AuthGuard, ProfileCheckGuard], component:PremiumInternationalJobListComponent},
	{path: 'international/job-post/preview', canActivate: [AuthGuard], component:InternationalJobPreviewComponent},

	{path: 'service/more-details', component:ServiceMoreDetailsComponent},
	//{path:'career-page', component:CareerPageViewComponent},



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeerRoutingModule { }
