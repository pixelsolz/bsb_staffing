import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthModule} from '../auth/auth.module';
import { CommonUsesModule } from '../common-uses/common-uses.module';
import { CmsModule } from '../cms/cms.module';
import { CoreModule } from '../core/core.module';
import { OwlModule } from 'ngx-owl-carousel';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { EmployeerRoutingModule } from './employeer-routing.module';
import { JobPostedComponent } from './job-posted/job-posted.component';
import { ApplicationRecievedComponent } from './application-recieved/application-recieved.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { JobPostedListComponent } from './job-posted-list/job-posted-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SimpleSearchComponent } from './simple-search/simple-search.component';
import { AdvanceSearchComponent } from './advance-search/advance-search.component';
import { PeopleSearchComponent } from './people-search/people-search.component';
import { JobPostPremiumComponent } from './job-post-premium/job-post-premium.component';
import { JobPostPremiumListComponent } from './job-post-premium-list/job-post-premium-list.component';
import { WalkInInterviewLocalComponent } from './walk-in-interview-local/walk-in-interview-local.component';
import { WalkInInterviewLocalListComponent } from './walk-in-interview-local-list/walk-in-interview-local-list.component';
import { WalkInInterviewInternationalComponent } from './walk-in-interview-international/walk-in-interview-international.component';
import { WalkInInterviewInternationalListComponent } from './walk-in-interview-international-list/walk-in-interview-international-list.component';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { EmailTemplateCreateComponent } from './email-template-create/email-template-create.component';
import { ResumesComponent } from './resumes/resumes.component';
import { ApplicationEditComponent } from './application-edit/application-edit.component';
import { CustomiseServicesComponent } from './customise-services/customise-services.component';
import { ResumesWithoutLoginComponent } from './resumes-without-login/resumes-without-login.component';
import { ManageFolderComponent } from './manage-folder/manage-folder.component';
import { FolderViewComponent } from './folder-view/folder-view.component';
import { EmployeeProfileViewComponent } from './employee-profile-view/employee-profile-view.component';

import { SearchHistoryComponent } from './search-history/search-history.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SendMailUsingTemplateComponent } from './send-mail-using-template/send-mail-using-template.component';
import { CollegeListComponent } from './college-list/college-list.component';
import { RecentPostedJobComponent } from './recent-posted-job/recent-posted-job.component';
import { CollegeDetailComponent } from './college-detail/college-detail.component';
import { CvsearchCollegeComponent } from './cvsearch-college/cvsearch-college.component';
import { MailboxComponent } from './mailbox/mailbox.component';
import { ServiceComponent } from './service/service.component';
import { CareerPageComponent } from './career-page/career-page.component';
import { EmailTemplatePreviewComponent } from './email-template-preview/email-template-preview.component';
import { ViewSimilarProfileComponent } from './view-similar-profile/view-similar-profile.component';
import { JobPostPreviewComponent } from './job-post-preview/job-post-preview.component';
import { WalkInInterviewPreviewComponent } from './walk-in-interview-preview/walk-in-interview-preview.component';
import { SendMailPreviewComponent } from './send-mail-preview/send-mail-preview.component';
import { CarrerWidgetComponent } from './carrer-widget/carrer-widget.component';
import { FreeInternationalJobComponent } from './free-international-job/free-international-job.component';
import { FreeInternationalJobListComponent } from './free-international-job-list/free-international-job-list.component';
import { PremiumInternationalJobComponent } from './premium-international-job/premium-international-job.component';
import { PremiumInternationalJobListComponent } from './premium-international-job-list/premium-international-job-list.component';
import { InternationalJobPreviewComponent } from './international-job-preview/international-job-preview.component';
import { ServiceMoreDetailsComponent } from './service-more-details/service-more-details.component';
//import { CareerPageViewComponent } from './career-page-view/career-page-view.component';

@NgModule({
  declarations: [JobPostedComponent, ApplicationRecievedComponent, ProfileComponent, HomeComponent, JobPostedListComponent, DashboardComponent, SimpleSearchComponent, AdvanceSearchComponent, PeopleSearchComponent, JobPostPremiumComponent, JobPostPremiumListComponent, WalkInInterviewLocalComponent, WalkInInterviewLocalListComponent, WalkInInterviewInternationalComponent, WalkInInterviewInternationalListComponent, EmailTemplateComponent, EmailTemplateCreateComponent,ResumesComponent, ApplicationEditComponent, CustomiseServicesComponent, ResumesWithoutLoginComponent, ManageFolderComponent, FolderViewComponent, EmployeeProfileViewComponent, SearchHistoryComponent, SendMailUsingTemplateComponent, CollegeListComponent, CollegeDetailComponent, CvsearchCollegeComponent,RecentPostedJobComponent, MailboxComponent, ServiceComponent, CareerPageComponent, EmailTemplatePreviewComponent, ViewSimilarProfileComponent, JobPostPreviewComponent, WalkInInterviewPreviewComponent, SendMailPreviewComponent, CarrerWidgetComponent, FreeInternationalJobComponent, FreeInternationalJobListComponent, PremiumInternationalJobComponent, PremiumInternationalJobListComponent, InternationalJobPreviewComponent, ServiceMoreDetailsComponent],
  imports: [
    CommonModule,
    EmployeerRoutingModule,
    AuthModule,
    CommonUsesModule,
    CmsModule,
    CoreModule,
    OwlModule,
    PdfViewerModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class EmployeerModule { }
