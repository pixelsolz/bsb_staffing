import { Component, OnInit, Renderer2, Renderer, ElementRef} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router ,ActivatedRoute, NavigationEnd, NavigationStart} from '@angular/router';
declare var Swal: any;
declare var $;
import { filter } from 'rxjs/operators';
import { PagerService } from '../../services/pager.service';
import * as _ from 'lodash';
import * as moment from 'moment'; 
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-folder-view',
  templateUrl: './folder-view.component.html',
  styleUrls: ['./folder-view.component.css']
})
export class FolderViewComponent implements OnInit {
  noteSaveForm : FormGroup;
  saveToFolderForm : FormGroup;
  reportCandidateForm : FormGroup;
  showloader : boolean;
  all_currency: any=[];
  all_countries:any=[];
  all_cities: any=[];
  cv_search : any;
  searchcvResult : any=[];
  total_cv_count : any;
  countries: any=[];
  cities: any=[];
  industries: any=[];
  job_roles: any=[];
  experiances: any=[];
  salaries: any=[];
  currencies: any=[];
  countryModel: any={};
  cityModel: any={};
  industryModel: any={};
  jobRoleModel: any={};
  departmentModel: any={};
  otherFilterModel: any={};
  degreeModel: any={};
  searchForm: any={
    country_list:[],
    city_list:[],
    industry_list:[],
    department_list:[],
    degree_list:[],
    other_filters:[],
    experince:{
      min_exp:'',
      max_exp:''
    },
    salary:{
      currency:'',
      min_salary:'',
      max_salary:''
    },
    candidate_active:'',
    show_profile:''
  };
  searchFormCall: boolean=false;
  isSubmit:boolean;
  set_employee_id:string;
  notedata:any;
  showPhone: Boolean = true;
  show_phone_index :number;
  showphonwArr: any=[];
  employee_view_history:any=[];
  selectedAll: any;
  checkedList:any;
  createFolderDiv:boolean =false;
  selectFolderDiv:boolean = true;
  reportAbuseOptionDiv:boolean = false;
  employer_folders:any;
  previousSearchUrl: any;
  lastSearchData: any={};
  activeQueryParams: any;
  emp_edu_string:any;
  departments: any=[];
  degrees: any=[];
  cv_download_view_check_active_package:number;
  mass_mail_service_check_active_package:number;
  pager: any = {};
  pagedItems: any = [];

  countrySerach: String ='';
  citySearch: String ='';
  industrySearch: String='';
  departmentSearch: String='';
  degreeSearch: String='';
  folderId: any;
  employeeIds: any=[];
  pageData: any={
    current_page:1,
    limit:10,
  };

  constructor(private loadingBar: LoadingBarService, private pageService: PagerService,private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute,private message:MessageService, private el: ElementRef) { 
    this.showloader = false;
    this.isSubmit = false;
    this.folderId = this.activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';
    this.activeQueryParams = this.activeRoute.snapshot.queryParams;
    this.previousSearchUrl = this.activeRoute.snapshot.queryParams['prevsearch'];
    this.cv_search = {folder_id:this.folderId,search_type:'folder_wise'};

    this.commonService.getAll('/api/employer/check-service?type=cv_download_view')
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        this.cv_download_view_check_active_package = data.check_active_service_count;
        /*if(this.cv_download_view_check_active_package ==0){
          this.router.navigate(['employer/service'], { queryParams: { type:'premium_job_post' } });
        }*/
        
      }, (error)=>{});

    this.commonService.getAll('/api/employer/check-service?type=mass_mail_service')
      .subscribe((data)=>{
        this.mass_mail_service_check_active_package = data.check_active_service_count;
        /*if(this.cv_download_view_check_active_package ==0){
          this.router.navigate(['employer/service'], { queryParams: { type:'premium_job_post' } });
        }*/
        
      }, (error)=>{});  


    }


  ngOnInit() {
    this.noteSaveForm=this.fb.group({
      id:[''],
      employee_id:[''],
      note : ['', [Validators.required]],
    })

     this.saveToFolderForm=this.fb.group({
      id:[''],
      folder_save:[''],
      folder_name:[''],
      folder_id:[''],
      employee_ids:[''],
    });

     this.reportCandidateForm=this.fb.group({
      id:[''],
      employee_id:[''],
      report_candidate_option:['', [Validators.required]],
      incorrect_email_id:[''],
      incorrect_mobile_no:[''],
      mobile_not_reachable:[''],
      incorrect_profile_or_resume:[''],
      profile_not_updated:[''],
      
    });
    this.getResumeBySearch();
    this.getLastSearchData(); 
    this.formControlValueChanged();
     /*if(this.employer_folders && this.employer_folders.length > 0){
       this.saveToFolderForm.get('folder_save').setValue('existing_folder');
     }else{
       this.saveToFolderForm.get('folder_save').setValue('create_new_folder');
     }*/
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.all_countries = data['countries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
          this.all_cities = data['cities']
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      if(data && data.status ==200){        
        this.all_currency = data['all_currency'];
        this.experiances =data['allExprience'];
      }
    });
     
  }

  setPage(page: number) {
    if (page < 1 ) {
      return;
    }else if(page > Math.ceil(this.total_cv_count/this.pageData.limit)){
      return;
    }

    this.pageData.current_page = page
    let data={};
    if(this.searchFormCall){
     data={employeeIds:this.employeeIds, additional:this.searchForm, paginate:{limit:this.pageData.limit, skip: ((page-1) * this.pageData.limit)}};
    }else{
     data={employeeIds:this.employeeIds, paginate:{limit:this.pageData.limit, skip: ((page-1) * this.pageData.limit)}};
    }
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-employee`,data).then(res=>{
      this.searchcvResult = res.data;
    }).catch(err=>{});

  }

  formControlValueChanged(){
    const folderNameControl = this.saveToFolderForm.get('folder_name');
    const folderIdControl = this.saveToFolderForm.get('folder_id');
    this.saveToFolderForm.get('folder_save').valueChanges
      .subscribe(folderSave => {
        if(folderSave === 'existing_folder'){
          folderIdControl.setValidators([Validators.required]);
          folderNameControl.setValidators(null);
        } else if(folderSave === 'create_new_folder'){
          folderNameControl.setValidators([Validators.required]);
          folderIdControl.setValidators(null);
        }

        folderIdControl.updateValueAndValidity();
        folderNameControl.updateValueAndValidity();
      });
  }

  getResumeBySearch(){
    this.showloader = true;
    
    this.commonService.getAll('/api/get-cv-folder?folder_id=' + this.folderId).subscribe(data=>{
      if(data.folder_data){
        let empIds = data.folder_data.map(obj=>obj.employee_id)

        if(empIds.length > 1){
          empIds.forEach(obj=>{
            obj.forEach(objValue=>{
              this.employeeIds.push(objValue)
            })
          })
        }else{
          this.employeeIds = empIds[0]
        }
    let form_data={employeeIds:this.employeeIds}; 
    this.loadingBar.start();
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-employee`,form_data).then(res=>{
      this.searchcvResult = res.data;
      //this.total_cv_count = res.data.length
      this.pagedItems = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-employee-count`,form_data).then(res=>{      
      this.total_cv_count = res.data[0].count
      this.loadingBar.complete();
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-country`,form_data).then(res=>{
      this.countries = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-city`,form_data).then(res=>{
      this.cities = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-department`,form_data).then(res=>{
      this.departments = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-industry`,form_data).then(res=>{
      this.industries = res.data;
    }).catch(err=>{});

      }
    });
     /*this.commonService.create('/api/get-cv-by-search',data)
     .subscribe((data)=>{
      this.countries = data.countries;
      this.cities = data.cities;
      this.industries = data.industries;
      this.departments = data.departments;
      this.degrees = data.degrees;
      this.job_roles = data.roles; 
       this.searchcvResult = data.cvResults;
      this.total_cv_count = data.total_cv_count;
      this.employer_folders = data.employer_folders;
      let otherData = data.other_mst;
      this.salaries = otherData.filter((data)=>data.entity =='salary');
      this.currencies = otherData.filter((data)=>data.entity =='currency');
      //this.searchForm.salary.currency = this.currencies.find(obj=>obj.name =='USD')._id;
       this.showloader = false;
      this.searchcvResult.map((obj, i)=> this.showphonwArr[i]= false);
      this.searchcvResult.forEach((obj)=>{
        obj.employee_degrees = obj.employee_education.map(l_obj=>l_obj.degree_name);
      });
     this.initialSetSearch();
     this.setPage(1);
     console.log(this.pagedItems);
     },(error)=>{});*/
  }

  initialSetSearch(){
    if(this.cv_search.country_id && this.cv_search.country_id.length){
        this.cv_search.country_id.forEach((co_id, index)=>{
          this.countryModel[co_id] = true;
        });
    }
    if(this.cv_search.city_id && this.cv_search.city_id.length){
      this.cv_search.city_id.forEach((city_id, index)=>{
          this.cityModel[city_id] = true;
        });
    }    
    if(this.cv_search.industry_id && this.cv_search.industry_id.length){
      this.cv_search.industry_id.forEach((ind_id, index)=>{
          this.industryModel[ind_id] = true;
        });
    }
    if(this.cv_search.department_id && this.cv_search.department_id.length){
      this.cv_search.department_id.forEach((dep_id, index)=>{
          this.departmentModel[dep_id] = true;
        });
    }
    if(this.cv_search.max_qualification && this.cv_search.max_qualification.length){
      this.cv_search.max_qualification.forEach((deg_id, index)=>{
          this.degreeModel[deg_id] = true;
        });
    }  
    if(this.cv_search.min_qualification && this.cv_search.min_qualification.length){
      this.cv_search.min_qualification.forEach((deg_id, index)=>{
          this.degreeModel[deg_id] = true;
        });
    }   
    if(this.cv_search.min_experiance){
      this.searchForm.experince.min_exp = this.cv_search.min_experiance;
    }
    if(this.cv_search.max_experiance){
      this.searchForm.experince.max_exp = this.cv_search.max_experiance;
    }

    if(this.employer_folders && this.employer_folders.length > 0){
       this.saveToFolderForm.get('folder_save').setValue('existing_folder');
       this.createFolderDiv =false;
       this.selectFolderDiv = true;
     }else{
       this.saveToFolderForm.get('folder_save').setValue('create_new_folder');
       this.createFolderDiv =true;
      this.selectFolderDiv = false;
     }
  }

  toggleAllSelect(event, param){
    console.log(param);
    if(event.target.checked){
      switch (param) {
        case "country":
          this.countries.map(country=> this.countryModel[country.id]=true);
          break;
        case "city":
          this.cities.map(city=> this.cityModel[city.id]=true);
          break;
        case "industry":
          this.industries.map(industry=> this.industryModel[industry.id]=true);
          break;
        case "role":
          this.job_roles.map(role=> this.jobRoleModel[role.id]=true);
          break;
        default:
          // code...
          break;
      }
    }else{
      switch (param) {
        case "country":
          this.countries.map(country=> this.countryModel[country.id]=false);
          break;
        case "city":
          this.cities.map(city=> this.cityModel[city.id]=false);
          break;
        case "industry":
          this.industries.map(industry=> this.industryModel[industry.id]=false);
          break;
        case "role":
          this.job_roles.map(role=> this.jobRoleModel[role.id]=false);
          break;
        default:
          // code...
          break;
      }
    }
  }

  setsearchValue(event, type){
    this.searchFormCall =true;
    if(type=='country'){
      let count_list = Object.entries(this.countryModel);
      let countArr =[];
      count_list.forEach((value, i)=>{if(value[1] ==true){countArr.push(value[0]);}});
      console.log(countArr);
      this.searchForm.country_list = countArr;
    }else if(type=='city'){
      let city_list = Object.entries(this.cityModel);
      let cityArr =[];
      city_list.forEach((value, i)=>{if(value[1] ==true){cityArr.push(value[0]);}});
      this.searchForm.city_list = cityArr;
    }else if(type=='industry'){
      console.log(this.industryModel);
      let indus_list = Object.entries(this.industryModel);
      let indusArr =[];
      indus_list.forEach((value, i)=>{if(value[1] ==true){indusArr.push(value[0]);}});
      this.searchForm.industry_list = indusArr;
    }else if(type=='department'){
      let dep_list = Object.entries(this.departmentModel);
      let depArr =[];
      dep_list.forEach((value, i)=>{if(value[1] ==true){depArr.push(value[0]);}});
      this.searchForm.department_list = depArr;
    }else if(type=='degree'){
      let deg_list = Object.entries(this.degreeModel);
      let degArr =[];
      deg_list.forEach((value, i)=>{if(value[1] ==true){degArr.push(value[0]);}});
      this.searchForm.degree_list = degArr;
    }
    else if(type=='other_filter'){
      let other_list = Object.entries(this.otherFilterModel);
      let otherArr =[];
      other_list.forEach((value, i)=>{if(value[1] ==true){otherArr.push(value[0]);}});
      this.searchForm.other_filters = otherArr;
    }
    this.getsearchData();
    //console.log(this.searchForm);
  }

  getsearchData(){
    this.showloader = true;
    let data={};
    if(this.searchFormCall){
     data={additional:this.searchForm,employeeIds:this.employeeIds};
    }else{
     data={employeeIds:this.employeeIds};
    }
    this.loadingBar.start();
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-employee`,data).then(res=>{
      this.searchcvResult = res.data;
      //this.total_cv_count = res.data.length
      this.pagedItems = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-employee-count`,data).then(res=>{      
      this.total_cv_count = res.data[0].count
      this.loadingBar.complete();
    }).catch(err=>{});

    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-country`,data).then(res=>{
      this.countries = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-city`,data).then(res=>{
      this.cities = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-department`,data).then(res=>{
      this.departments = res.data;
    }).catch(err=>{});
    this.commonService.getCVSearchResult(`${this.global.communityApiUrl}api/cv-search/search-by-industry`,data).then(res=>{
      this.industries = res.data;
    }).catch(err=>{});
  }

  submitNote(){
    this.isSubmit =true;
    this.noteSaveForm.get('employee_id').setValue(this.set_employee_id);
    console.log(this.set_employee_id);
    if(this.isSubmit && this.noteSaveForm.invalid){

      return ;
    }
     this.commonService.create('/api/save-cv-note', this.noteSaveForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.message.success('Successfully Add Note');
            //this.clearForm();
            this.set_employee_id = '';
            this.getResumeBySearch();
            //this.router.navigate(['/employer/job-posted-premium-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });

  }

  setCvDetail(employee_id){
    let data = this.searchcvResult.find(obj=>obj.id ==employee_id);
    this.notedata = data.note.note;
    this.noteSaveForm.get('note').setValue(this.notedata);
    this.noteSaveForm.get('id').setValue(data.note._id);
  }

  showMobile(employee_id,i){
    if(this.cv_download_view_check_active_package !=0){
      this.showphonwArr[i] = true;
      /*this.showPhone = this.showPhone ? false : true;
      this.show_phone_index = i;*/
      //console.log(employee_id);
      this.commonService.getAll('/api/save-cv-history?employee_id='+employee_id)
          .subscribe((data)=>{
            this.showloader =false;
            this.isSubmit = false;
            if(data.status === 200){
              //this.getResumeBySearch();
              //this.router.navigate(['/employer/job-posted-premium-list']);
            }else if(data.status ==500){
              this.message.error(data.status_text);
            }else if(data.status ==422){
               this.showphonwArr[i] = false;
              this.message.error(data.status_text);
            }
          }, (error)=>{
            this.showloader = false;
          });
        }else{
          this.message.error('Please buy cv download and view package');
        }
  }

  viewHistory(employee_id){
       this.commonService.getAll('/api/view-cv-history?employee_id='+employee_id)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.employee_view_history = data.data;
            //this.getResumeBySearch();
            //this.router.navigate(['/employer/job-posted-premium-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  selectAll() {
    for (var i = 0; i < this.pagedItems.length; i++) {
      this.pagedItems[i].selected = this.selectedAll;
    }
    //this.getCheckedItemList();
  }
  checkIfAllSelected() {
    this.selectedAll = this.pagedItems.every(function(item:any) {
        return item.selected == true;
      })
    //this.getCheckedItemList();
  }

  saveToFolder(){

    this.checkedList = [];
    for (var i = 0; i < this.pagedItems.length; i++) {
      if(this.pagedItems[i].selected)
      this.checkedList.push(this.pagedItems[i].id);
    }
    
      if(this.checkedList.length > 0){
        //console.log(this.checkedList);
        $('#save_folder_model').click();
        this.saveToFolderForm.get('employee_ids').setValue(this.checkedList);
        
      }else{
        Swal.fire({
          //title: 'Are you sure?',
          text: "Please select atleast one!",
          type: 'warning',
        });
      }
  }

  folderOption(e){
    if(e.target.value=='existing_folder'){
      this.createFolderDiv =false;
      this.selectFolderDiv = true;
    }else if(e.target.value=='create_new_folder'){
      this.createFolderDiv =true;
      this.selectFolderDiv = false;
    }else{
      this.createFolderDiv =false;
      this.selectFolderDiv = true;
    }
  }

  cvSaveFolder(){
    this.isSubmit =true;
    if(this.isSubmit && this.saveToFolderForm.invalid){
      
      return ;
    }
    this.commonService.create('/api/save-cv-to-folder', this.saveToFolderForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            $("#close_folder_save_modal").click();
            this.message.success('Successfully Save To Folder');
            this.saveToFolderForm.reset();
            this.initialSetSearch();

            //this.clearForm();
            //this.router.navigate(['/employer/job-posted-premium-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  downloadExcel(){
    let selectedData = this.pagedItems.filter(obj=>obj.selected);
    if(selectedData.length){
      let emp_ids = selectedData.map(obj=>obj.id);
      const exportForm = this.renderer2.createElement('FORM');
      exportForm.setAttribute('method', 'get');
      exportForm.setAttribute('id', 'download_form');
      exportForm.setAttribute(
        'action',
        this.global.apiUrl + '/api/employee-resume/export'
      );
      const token = this.renderer2.createElement('INPUT');
      token.setAttribute('name', 'token');
      token.setAttribute('value', localStorage.getItem('token'));
      exportForm.appendChild(token);
      const firstInput =this.renderer2.createElement('INPUT');
      firstInput.setAttribute('name', 'emp_id');
      firstInput.setAttribute('type', 'hidden');
      firstInput.setAttribute('value', emp_ids);
      exportForm.appendChild(firstInput);
      this.renderer2.appendChild(this.el.nativeElement, exportForm);
      exportForm.submit();
    }else{
      this.message.error('Please checked employee');
    }
    
  }

  downloadResume(){
    let selectedData = this.pagedItems.filter(obj=>obj.selected);
    if(selectedData.length){
      let emp_ids = selectedData.map(obj=>obj.id);
      const downloadResumeForm = this.renderer2.createElement('FORM');
      downloadResumeForm.setAttribute('method', 'get');
      downloadResumeForm.setAttribute('id', 'download_resume_form');
      downloadResumeForm.setAttribute(
        'action',
        this.global.apiUrl + '/api/download-resume'
      );
      const token1 = this.renderer2.createElement('INPUT');
      token1.setAttribute('name', 'token');
      token1.setAttribute('value', localStorage.getItem('token'));
      downloadResumeForm.appendChild(token1);
      const firstInput1 =this.renderer2.createElement('INPUT');
      firstInput1.setAttribute('name', 'employee_ids');
      firstInput1.setAttribute('type', 'hidden');
      firstInput1.setAttribute('value', emp_ids);
      downloadResumeForm.appendChild(firstInput1);
      this.renderer2.appendChild(this.el.nativeElement, downloadResumeForm);
      downloadResumeForm.submit();
    }else{
      this.message.error('Please checked employee');
    }


  }

  viewProfile(employee_id){
    //console.log(employee_id);
    if(this.cv_download_view_check_active_package !=0){
      this.router.navigate(['/employer/profile-view/'+employee_id]);
    }else{
      this.message.error('Please buy cv download and view package');
    }
  }


  searchShow(){
      this.router.navigate(['/employer/'+ this.previousSearchUrl], { queryParams: { modify: 'modify' }} );
    }

  saveSearch(){
    Swal.fire({
    title: 'Enter save text',
    input: 'text',
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Save',
    showLoaderOnConfirm: true,
    preConfirm: (savetext) => {
      return this.commonService.update('/api/search-history/saved',this.lastSearchData._id, {save_text:savetext})
          .subscribe(data=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              this.router.navigate(['/employer/search-history'],{queryParams:{search_type:'saved'}});
            }
            console.log(data);
          },error=>{});

    },
    allowOutsideClick: () => !Swal.isLoading()
  });

  }

  getLastSearchData(){
    if(this.activeQueryParams['search_id']){
      this.commonService.show('/api/search-history', this.activeQueryParams['search_id'])
          .subscribe(data=>{
            this.lastSearchData = data.data;
          },error=>{});
    }else{
          this.commonService.getAll('/api/getlast-search')
        .subscribe(data=>{
          if(data.status ==200){
            this.lastSearchData = data.data;
          }
        }, error=>{});
    }

  }

  sendMail(){
    console.log(this.mass_mail_service_check_active_package);
    this.checkedList = [];
    for (var i = 0; i < this.pagedItems.length; i++) {
      if(this.pagedItems[i].selected)
      this.checkedList.push(this.pagedItems[i]._id);
    }

     if(this.checkedList.length > 0){
       if(this.mass_mail_service_check_active_package !=0){
         localStorage.setItem('mailUids',JSON.stringify(this.checkedList));
         this.router.navigate(['/employer/send-mail']);
       }else{
         //$('#report_candidate_model').click();
         $("#mail_serice_activated_modal").click();
       }
       
       
      }else{
        Swal.fire({
          //title: 'Are you sure?',
          text: "Please select atleast one!",
          type: 'warning',
        });
      }
    }

    reportOption(e){
    if(e.target.value=='delete_everywhere'){
      this.reportAbuseOptionDiv =false;
    }else if(e.target.value=='report_abuse'){
      this.reportAbuseOptionDiv =true;
    }else{
      this.reportAbuseOptionDiv =false;
    }
  }

    reportCandidate(employee_id){

      $('#report_candidate_model').click();
      this.reportCandidateForm.get('employee_id').setValue(employee_id);
    }

    candidateReportSave(){
      //console.log(this.reportCandidateForm.value);
      this.commonService.create('/api/save-reported-candidate',this.reportCandidateForm.value)
     .subscribe((data)=>{
       if(data.status === 200){
            $("#close_reported_candidate_modal").click();
            this.message.success('Successfully Reported Candidate');
            this.reportCandidateForm.reset();
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
     //console.log(this.searchcvResult);
     },(error)=>{});
    }

    likeProfile(employee_id){
    this.commonService.getAll('/api/employee/like-profile?id='+employee_id)
     .subscribe((data)=>{
       if(data.status === 200){
             //this.message.success('Successfully Like Profile');
             let index = this.searchcvResult.findIndex(obj=>obj.id ==employee_id);
             this.searchcvResult[index].like_profile =1;
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
     },(error)=>{});
  }

  disLikeProfile(employee_id){
    this.commonService.getAll('/api/employee/dis-like-profile?id='+employee_id)
     .subscribe((data)=>{
       if(data.status === 200){
             //this.message.success('Successfully Like Profile');
             let index = this.searchcvResult.findIndex(obj=>obj.id ==employee_id);
             this.searchcvResult[index].like_profile =0;
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
     },(error)=>{});
  }

  viewSimilarProfile(empId){
      this.commonService.getAll('/api/employer/get-similar/profile/' + empId)
          .subscribe(data=>{
            if(data.status ==200){
              
            }
          },error=>{});
  }

    getCountriesList(arrays){
    if(!_.isEmpty(arrays)){
      let countries = this.all_countries.filter(obj=> arrays.indexOf(obj._id) != -1);
     return !_.isEmpty(countries)? countries.map(obj=>obj.name):'';
    }
    return '';
    
  }


  getCitiesList(arrays){
    if(!_.isEmpty(arrays)){
      let cities = this.all_cities.filter(obj=> arrays.indexOf(obj._id) != -1);
      return !_.isEmpty(cities)? cities.map(obj=>obj.name):'';
    }
    return '';
  }

  getCity(city){
    let cities = this.all_cities.find(obj=> String(obj._id) == String(city));
    return !_.isEmpty(cities)? cities.name:'';
  }

  getAddDates(value){
      return moment().subtract(value, 'd').format('YYYY-MM-DD');
  }

  getDateFormat(value){
    return moment(value).format("Do MMM YYYY");
  }

  getPageCount(total, limit){
    return total ? Math.ceil(total/limit):''
  }

}
