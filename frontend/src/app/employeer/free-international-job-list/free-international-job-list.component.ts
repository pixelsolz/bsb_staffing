import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { ActivatedRoute } from "@angular/router";
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
declare var Swal: any;
import {PagerService} from '../../services/pager.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-free-international-job-list',
  templateUrl: './free-international-job-list.component.html',
  styleUrls: ['./free-international-job-list.component.css']
})
export class FreeInternationalJobListComponent implements OnInit {

	job_lists: any =[];
  active_page: number;
  count_data: number;
  pager: any = {};
  pagedItems: any[];
  total_pages: number=0;
  current_page: number=1
  totalPagesArr: any=[];
  constructor(private global: Global,private route: ActivatedRoute,private commonService: CommonService,
      private authService: AuthService, private messageService: MessageService, private Pagerservice: PagerService) { 
    localStorage.removeItem('job_preview');
  }

  ngOnInit() {
  	this.getData();
  }

  showRepublish(exp_date, is_republished, is_published){
    let expDate = exp_date.split('/');
   if(is_published ==0){
     return false;
   }
    if(new Date() > new Date(expDate[1]+ '-'+ expDate[0] + '-'+ expDate[2]) ){
          
      return is_republished && is_republished ==1 ? false : true;
    }
    return false;

  } 

  getData(){
    this.commonService.getAll('/api/employer/international-jobs?job_type='+ 1)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
       if(data.status == 200){
          this.job_lists = data.data;
          this.count_data = data.tot_jobs;
          this.total_pages = Math.ceil(data.tot_jobs/10)
          this.totalPagesArr = _.range(Math.ceil(data.tot_jobs/10)).map(obj=>obj +1);
          this.job_lists.forEach((job, i)=>{
            job.select = (job.job_status ==1? true: false);
            job.republish_show = this.showRepublish(job.job_ageing, job.is_republished, job.is_published)
          });
        }
      }, (error)=>{});
  }

  setPage(page: number) {
    this.current_page = page;
    /*if((this.current_page -2) >= 1 && (this.current_page +2) <=this.total_pages ){
      this.totalPagesArr = [this.current_page -1, this.current_page -2, this.current_page, this.current_page +1, this.current_page +2]
    }else if(this.current_page ==1 ){
      this.totalPagesArr = [1,2,3,4,5]
    }else if(this.current_page ==this.total_pages){
      this.totalPagesArr = [this.total_pages -4,this.total_pages -3,this.total_pages -2,this.total_pages -1,this.total_pages]
    }else if(this.current_page ==1 ){
      this.totalPagesArr = [1,2,3,4,5]
    }else if(this.current_page ==1 ){
      this.totalPagesArr = [1,2,3,4,5]
    }*/

    this.commonService.getAll(`/api/employer/international-jobs?job_type=${1}&off_set=${page}`)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
       if(data.status == 200){
          this.job_lists = data.data;
          this.job_lists.forEach((job, i)=>{
            job.select = (job.job_status ==1? true: false);
            job.republish_show = this.showRepublish(job.job_ageing, job.is_republished, job.is_published)
          });
        }
      }, (error)=>{});
  }

  removeJob(id, index){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonService.delete('/api/employer/international-jobs/delete', id)
          .subscribe((data)=>{
          if(data.status ==200){   
             this.job_lists.splice(index,1);         
            Swal.fire(
              'Deleted!',
              'Job has been deleted successfully.',
              'success'
            )
           }
          },(error)=>{});

      }
          
    });   
  }

  publishedJob(id, index){
     this.commonService.getAll('/api/international-job/published/' + id)
           .subscribe(data=>{
             if(data.status ==200){
               this.job_lists[index]['is_published'] =1;
               this.job_lists[index]['published_date'] = data.data.published_date;
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }
  republishedJob(id,index){
    this.commonService.getAll('/api/international-job/re-published/' + id)
           .subscribe(data=>{
             if(data.status ==200){
               this.job_lists[index]['job_ageing'] = data.data.job_ageing;
               this.job_lists[index]['republished_date'] = data.data.republished_date;
               this.job_lists[index]['republish_show'] = false;
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }

  pagination(){
    let tot_number = Number(this.count_data) >1? (Number(this.count_data) /10):1;
    var items: number[] = [];
  for(var i = 1; i <= tot_number; i++){
     items.push(i);
  }
  return items;
  }

  paginationCall(page){

  
  }

  setStatus(event, id){
    let status = event ? 1:0;
       this.commonService.getAll('/api/employer/international-jobs/status-update/' + id + '?status=' + status)
           .subscribe(data=>{
             if(data.status ==200){
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }

}
