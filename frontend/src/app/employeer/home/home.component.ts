import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild,HostListener, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var $ :any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {

  @HostListener('document:click', ['$event'])
  clickout(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if($(event.target).hasClass('auto_comp_search')){
      if($(event.target).attr('id') =='job_title_search'){
        this.show_industry_srh = false;
        this.show_city_comp_srh = false;
      }else if($(event.target).attr('id') =='insudtry_search'){
        this.show_jb_title_srh = false;
        this.show_city_comp_srh = false;
      }else if($(event.target).attr('id') =='country_search'){
        this.show_jb_title_srh = false;
        this.show_industry_srh = false;
      }
    }else{
      this.show_jb_title_srh = false;
      this.show_industry_srh = false;
      this.show_city_comp_srh = false;
      this.adv_show_jb_title_srh = false;
    }
  }


  employerHomeSlideImages = ['assets/images/slide1.jpg','assets/images/slide2.jpg','assets/images/slide3.jpg'];
  employerHomeSlideOptions={items: 1, dots: true, nav: false, autoplay:true, loop:true, animateOut: 'fadeOut', margin:0};
  employerHomeCarouselOptions={items: 3, dots: true, nav: true, autoplay:true, loop:true, animateOut: 'fadeOut', margin:0};

  emplorerSlideImages : any;
  //myCarouselImages = [1,2,3,4,5,6].map((i)=> `https://picsum.photos/640/480?image=${i}`);
  emplorerSlideOptions:any;
  emplorerCarouselOptions = { items: 1, dots: false, nav: true, autoplay: true, loop: true, margin: 0, responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 5 } } };
  showloader:boolean;
  advSearchForm: FormGroup;
  jobTitleSrh:string;
  title_comp_array: any=[];
  show_jb_title_srh: boolean;
  show_city_comp_srh: boolean;
  show_industry_srh: boolean;
  title_job_data: any=[];
  city_country_data: any =[];
  industrty_data: any =[];
  searchData = {
    title_comp:{
      id:'',
      name:'',
      table_name:''
    },
    city_country:{
      id:'',
      name:'',
      table_name:''
    },
    industry:{
      id:'',
      name:'',
      table_name:''
    }
  };
  count: number;
  currentImage:string;
  countries: any =[];
  allCountries: any;
  cities:any;
  industries:any;
  job_roles: any;
  emplymentFor: any;
  experiance: any;
  salaries: any;
  industriesDiv: boolean;
  storeMultiSelect :any ={
    industries_value:[],
    industries_name:[]
  };
  job_title_model: string;
  insudtry_model: string;
  country_loc_model: string;
  premium_job_post_packages :any;
  mass_mail_service_packages :any;
  cv_download_view_packages :any;
  international_walk_in_packages :any;
  isloggedIn: boolean;
  adv_show_jb_title_srh: boolean =false;
  country_model_array: any=[];
  insudtry_model_array: any=[];
  job_title_model_array: any=[];
  isloaded: boolean = false;

  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private message:MessageService,private eRef: ElementRef) {
    this.isloggedIn = localStorage.getItem('token')? true: false;
 
    if(this.router.url.includes('forgotpassword')){
      this.commonService.callLoginPopup('login')
    }
   }

  ngOnInit() {
    window.scrollTo(0, 0);
    if(document.querySelector('link[href$="materialize.min.css"]')){
      document.querySelector('link[href$="materialize.min.css"]').remove()
    }
    

    this.commonService.getAll('/api/employer/package-list')
        .subscribe((data)=>{
            this.commonService.callFooterMenu(1);
            this.premium_job_post_packages = data.packages.filter((obj)=>obj.service_type == 'premium_job_post');
            this.mass_mail_service_packages = data.packages.filter((obj)=>obj.service_type == 'mass_mail_service');
            this.cv_download_view_packages = data.packages.filter((obj)=>obj.service_type == 'cv_download_view');
            this.international_walk_in_packages = data.packages.filter((obj)=>obj.service_type == 'international_walk_in_interview');
            //console.log(this.international_walk_in_packages);  
            //this.packages = data.packages;
        }, (error)=>{});

        this.commonService.getAll('/api/logo-branding')
      .subscribe((data) => {
        this.emplorerSlideImages = data.data;
        this.isloaded = true;
      }, (error) => { });

    this.emplorerSlideOptions = { items: 3, dots: false, nav: false, autoplay: true, loop: true, margin: 0, responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 5 } } };

  }

  ngOnDestroy(){
    this.loadCss('assets/css/materialize.min.css');

  }

  public loadCss(url: string){
    const body = <HTMLDivElement> document.body;
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    body.appendChild(link);
  }

  /*searchAutoComplete(type, key){
    let keyData = key.target.value;
    //console.log(keyData);
    this.commonService.getAll('/api/employer-autocomplete-search?type='+type+'&keyword='+keyData)
      .subscribe((data)=>{
            if(data.status==200){
              
            }

    },(error)=>{});
  }*/

  searchAutoComplete(type, key){
   let typedKeyArray = key.target.value.trim().split(',');
   let searchdata = typedKeyArray.pop();
   let usedData = '';
    if(type ==='COUNTRY_WISE'){
      /*if(key.which == 188){
        this.country_model_array =[...this.country_model_array, typedKeyArray[typedKeyArray.length -1]];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }*/
      if(key.target.value.trim().length ==0){
        this.country_model_array.length =0;
        this.country_loc_model ='';
      }
      if(this.country_loc_model){
        let titleDataArr = this.country_loc_model.split(',');
        let arrFilter = this.country_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.country_model_array = arrFilter;
      } 
      usedData = this.country_loc_model;
    }else if(type ==='INDUSTRY_WISE'){
     /* if(key.which == 188){
        this.insudtry_model_array =[...this.insudtry_model_array, typedKeyArray[typedKeyArray.length -1]];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }*/
      if(key.target.value.trim().length ==0){
        this.insudtry_model_array.length =0;
        this.insudtry_model ='';
      } 
      if(this.insudtry_model){
        let titleDataArr = this.insudtry_model.split(',');
        let arrFilter = this.insudtry_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.insudtry_model_array = arrFilter;
      } 
      usedData = this.insudtry_model;
    }else if(type ==='JB_TITLE_WISE'){
      /*if(key.which == 188){
        alert();
        this.job_title_model_array =[...this.job_title_model_array, typedKeyArray[typedKeyArray.length -1]];
        this.job_title_model = this.job_title_model_array.toString() + ',';
        console.log(this.job_title_model );
      }*/
      if(key.target.value.trim().length ==0){
        this.job_title_model_array.length =0;
        this.job_title_model ='';
      }
      if(this.job_title_model){
        let titleDataArr = this.job_title_model.split(',');
        let arrFilter = this.job_title_model_array.filter(value=> titleDataArr.indexOf(value) != -1);
        this.job_title_model_array = arrFilter;
      } 
      usedData = this.job_title_model;      
    }
    if(!searchdata){
      this.show_jb_title_srh = false;
      this.show_city_comp_srh = false;
      this.show_industry_srh = false;
      return false;
    }
    this.commonService.getAll('/api/autocomplete-search?type=' + type + '&keyword=' + searchdata + '&used=' + usedData)
      .subscribe((data) => {
        if (data.status == 200) {
          if (data.type == 'JB_TITLE_WISE') {
            this.show_jb_title_srh = true;
            this.title_job_data = data.data;
          } else if (data.type == 'COUNTRY_WISE') {
            this.show_city_comp_srh = true;
            this.city_country_data = data.data;
          } else if (data.type == 'INDUSTRY_WISE') {
            this.show_industry_srh = true;
            this.industrty_data = data.data;
          }
        }


      }, (error) => { });
  }

  autoCompleteSearch(id, type) {
    if (type == 'JB_TITLE_WISE') {
      const titJob = this.title_job_data.find((data) => data.id == String(id));
      this.show_jb_title_srh = false;
      this.searchData.title_comp.id = titJob.id;
      if(this.job_title_model_array.length){
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = this.job_title_model_array.toString() + ',';
      }else{
        this.job_title_model_array =[...this.job_title_model_array, titJob.name];
        this.job_title_model = titJob.name + ',';
      }
      document.getElementById('job_title_search').focus();
    } else if (type == 'COUNTRY_WISE') {
      const city_count = this.city_country_data.find((data) => data.id == String(id));
      this.show_city_comp_srh = false;
      if(this.country_model_array.length){
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = this.country_model_array.toString() + ',';
      }else{
        this.country_model_array =[...this.country_model_array, city_count.name];
        this.country_loc_model = city_count.name + ',';
      }
       document.getElementById('country_search').focus();
      this.searchData.city_country.id = city_count.id;
      //this.searchData.city_country.name = city_count.name;
    } else if (type == 'INDUSTRY_WISE') {
      const industry_count = this.industrty_data.find((data) => data.id == String(id));
      this.show_industry_srh = false;
      if(this.insudtry_model_array.length){
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = this.insudtry_model_array.toString() + ',';
      }else{
        this.insudtry_model_array =[...this.insudtry_model_array, industry_count.name];
        this.insudtry_model = industry_count.name + ',';
      }
      this.searchData.industry.id = industry_count.id;
      //this.searchData.industry.name = industry_count.name;
    }

  }

  searchFilter(){
    //alert(this.searchData.title_comp.id);
    let search_title = this.job_title_model?this.job_title_model:'';
    let search_industry = this.insudtry_model?this.insudtry_model:'';
    let search_city = this.country_loc_model?this.country_loc_model:'';
    //let params = search_title + '-' + (search_industry ? search_industry + '-':'') + (search_city ? search_city + '-':'');
    //this.router.navigate(['/search-job-listing',search_title,search_industry,search_city]);
    if(search_title !='' || search_industry !='' || search_city !='' ){
      this.commonService.callLoginPopup('login')
      //return true;
      //this.router.navigate(['employer/resumes-list'], { queryParams: { search_title: search_title, 'search_industry': search_industry, 'search_city':search_city } });
    } else {
      this.message.warning("Please Enter Value");
      return false;
    }
    
  }

  buyNow(service_id){
    if(!this.isloggedIn){
      //alert("test");
      this.commonService.callLoginPopup({service_id: service_id});
    }else{
      alert('test');
    }
  }

  scroleToTop(){
    //window.scroll(0,0);
    $('html, body').animate({
          scrollTop:0
    }, 800, function(){ 
    });
  }




}
