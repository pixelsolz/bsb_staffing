import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobPostPremiumListComponent } from './job-post-premium-list.component';

describe('JobPostPremiumListComponent', () => {
  let component: JobPostPremiumListComponent;
  let fixture: ComponentFixture<JobPostPremiumListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPostPremiumListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPostPremiumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
