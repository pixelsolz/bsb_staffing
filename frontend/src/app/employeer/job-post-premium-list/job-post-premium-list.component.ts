import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { ActivatedRoute } from "@angular/router";
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
declare var Swal: any;
import {PagerService} from '../../services/pager.service';

@Component({
  selector: 'app-job-post-premium-list',
  templateUrl: './job-post-premium-list.component.html',
  styleUrls: ['./job-post-premium-list.component.css']
})
export class JobPostPremiumListComponent implements OnInit {
	job_lists: any =[];
  active_page: number;
  count_data: number;
  pager: any = {};
  pagedItems: any[];
  constructor(private global: Global,private route: ActivatedRoute,private commonService: CommonService,
      private authService: AuthService, private messageService: MessageService, private Pagerservice: PagerService) { 
    localStorage.removeItem('job_preview');
  }

  ngOnInit() {
  	this.getData();
  }

  showRepublish(exp_date, is_republished, is_published){
    let expDate = exp_date.split('/');
   if(is_published ==0){
     return false;
   }
    if(new Date() > new Date(expDate[1]+ '-'+ expDate[0] + '-'+ expDate[2]) ){
          
      return is_republished && is_republished ==1 ? false : true;
    }
    return false;

  } 

  getData(){
    this.commonService.getAll('/api/employer-job?job_type='+ 2)
      .subscribe((data)=>{
       this.commonService.callFooterMenu(1);
       if(data.status == 200){
          this.job_lists = data.data;
          this.count_data = data.tot_jobs;
          this.setPage(1);
          this.job_lists.forEach((job, i)=>{
            job.select = (job.job_status ==1? true: false);
            job.republish_show = this.showRepublish(job.job_ageing, job.is_republished, job.is_published)
          });
        }
      }, (error)=>{});
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    // get pager object from service
    this.pager = this.Pagerservice.getPager(this.job_lists.length, page);
    // get current page of items
    this.pagedItems = this.job_lists.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  removeJob(id, index){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonService.delete('/api/employer-job', id)
          .subscribe((data)=>{
          if(data.status ==200){   
             this.job_lists.splice(index,1);  
             this.setPage(1);        
            Swal.fire(
              'Deleted!',
              'Job has been deleted successfully.',
              'success'
            )
           }
          },(error)=>{});

      }
          
    });   
  }

  publishedJob(id, index){
     this.commonService.getAll('/api/employer-job/published/' + id)
           .subscribe(data=>{
             if(data.status ==200){
               this.pagedItems[index]['is_published'] =1;
               this.pagedItems[index]['published_date'] = data.data.published_date;
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }
  republishedJob(id,index){
    this.commonService.getAll('/api/employer-job/re-published/' + id)
           .subscribe(data=>{
             if(data.status ==200){
               this.pagedItems[index]['job_ageing'] = data.data.job_ageing;
               this.pagedItems[index]['republished_date'] = data.data.republished_date;
               this.pagedItems[index]['republish_show'] = false;
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }

  pagination(){
    let tot_number = Number(this.count_data) >1? (Number(this.count_data) /10):1;
    var items: number[] = [];
  for(var i = 1; i <= tot_number; i++){
     items.push(i);
  }
  return items;
  }

  paginationCall(index){
    this.active_page = index;
    this.getData();
  
  }

  setStatus(event, id){
    let status = event ? 1:0;
       this.commonService.getAll('/api/employer-job/status-change/' + id + '?status=' + status)
           .subscribe(data=>{
             if(data.status ==200){
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }

}
