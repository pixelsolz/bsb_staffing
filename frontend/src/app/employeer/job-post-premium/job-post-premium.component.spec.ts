import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobPostPremiumComponent } from './job-post-premium.component';

describe('JobPostPremiumComponent', () => {
  let component: JobPostPremiumComponent;
  let fixture: ComponentFixture<JobPostPremiumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPostPremiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPostPremiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
