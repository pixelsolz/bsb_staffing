import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
declare var $ :any;
import * as _ from 'lodash'; 

  let expdata =[];

@Component({
  selector: 'app-job-post-premium',
  templateUrl: './job-post-premium.component.html',
  styleUrls: ['./job-post-premium.component.css']
})
export class JobPostPremiumComponent implements OnInit {
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(event.target.id && event.target.id =='jobTitle'){
    }else{
      this.show_jb_title_srh = false;
    }
  }
  @HostListener("document:mousemove", ["$event"])
  onMouseMove(e) {
    $('.mouseTrail').css({'top': this.setTrail(e) , 'left':e.pageX+10, 'width':'auto', 'height':'auto'});
  }

	jobPostForm:FormGroup;
  conditionQuestionForm:FormGroup;
  questions:FormArray;
	showloader:boolean;
  submitted: boolean;
	countries: any;
	cities:any;
  coutryWiseCity: any=[];
	industries:any;
	job_roles: any;
	emplymentFor: any;
	experiance: any;
	empType: any;
	noOfVacancies: any;
	salaries: any;
	premium_posted_job: any;
	industryList = [];
	selectedItems = [];
  departments: any=[];
	dropdownSettings = {};
	underGraduateDiv: boolean;
	postGraduateDiv: boolean;
	industriesDiv: boolean;
	laguageDiv:boolean;
  departmentDiv:boolean;
	storeMultiSelect :any ={
		undergradute_value:[],
		undergraduate_name:[],
    undergraduate_sub_value: [],
		postgraduate_value:[],
		postgraduate_name:[],
    postgraduate_sub_value: [],
		industries_value:[],
		industries_name:[],
    department_name:[],
    department_value:[],
	};
	post_graduates: any=[];
	under_graduates: any=[];
  show_sub: any=[];
  show_sub_under: any=[];
  noSearchSelect: any={
    lang_value:[],
    lang_name:[]
  };
  languages: any=[];
  isSubmit: boolean;
  job_id: any='';
  job_data:any={};
  salary_type_val:string;
  other_mst_data:any;
  all_currency:any;
  skill_key: any;
  skillData: any=[];
  addOnBlur: boolean=true;
  check_active_package:number;
  job_titles: any=[];
  show_jb_title_srh: boolean=false;
  post_graduate_deg: any = [];
  under_graduate_deg: any = [];
  all_departments: any = [];
  relevant_conditions: any={
    expeiance:false,
    industry: false,
    age: false,
    location: false,
    gender: false,
    salary: false,
    education: false
  };
  alljob_conditions: any={
    all:true
  }


  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About This Job',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Benefit',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  noteconfig: any = {
    //height: '200px',
    placeholder: 'Special Notes',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  responsconfig: any = {
    //height: '200px',
    placeholder: 'Primary Responsibility',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };

    questionconfig: any = {
    //height: '200px',
    placeholder: 'Question',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
  };
  constructor(private renderer2: Renderer2, private renderer: Renderer,private fb:FormBuilder, private message:MessageService ,private authService: AuthService,
    private router: Router,private activeRoute: ActivatedRoute, private commonService: CommonService, private http: HttpClient,  private eRef: ElementRef) { 
  this.showloader =false;
	this.underGraduateDiv = false;
	this.postGraduateDiv = false;
	this.laguageDiv = false;
	this.industriesDiv = false;
  this.departmentDiv = false;
  this.isSubmit = false;
  this.job_id = this.activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';

          this.commonService.getAll('/api/employer/check-service?type=premium_job_post')
              .subscribe((data)=>{
                this.check_active_package = data.check_active_service_count;
                if(this.check_active_package ==0){
                  this.router.navigate(['employer/service'], { queryParams: { type:'premium_job_post' } });
                }
              }, (error)=>{});

  }

  ngOnInit() {
    this.conditionQuestionForm = this.fb.group({
       questions: this.fb.array([this.initReferal()])
    });

  	this.jobPostForm= this.fb.group({
        id:this.job_id,
        job_title :['', [Validators.required,Validators.maxLength(50)]],
        country_id:['', [Validators.required]],
        city_id:['', [Validators.required]],
        about_this_job:['', [Validators.required]],
        industry_id:['', [Validators.required]],
        department_id:['',],
        //job_role_id:['', [Validators.required]],
        employement_for:['', [Validators.required]],
        required_skill:['', [Validators.required]],
        benefits:['', [Validators.required]],
        min_experiance:['', [Validators.required]],
        max_experiance:['', [Validators.required, this.checkMinMaxExp]],
        //job_desc:['', [Validators.required]],
        min_qualification: ['', [Validators.required]],
        max_qualification:[],
        min_qualification_stream: [],
        max_qualification_stream: [],
        graduate_and_or_post_graduate:[''],
        primary_responsibility:['', [Validators.required]],
        employment_type:['', [Validators.required]],
        language_pref: ['', [Validators.required]],
        number_of_vacancies:['', [Validators.required]],
        min_monthly_salary:['', [Validators.required]],
        max_monthly_salary:['', [Validators.required, this.checkMinMaxSal]],
        special_notes:[''],
      //  job_ageing:['', [Validators.required]],
        //received_email: ['', [Validators.required]],
        currency: ['', [Validators.required]],
        salary_type: ['', [Validators.required]],
        job_type:[],
        candidate_type:['', [Validators.required]],
        job_lat:[''],
        job_long:[''],
        condition_type:[2],
        conditions:[]

    });

    this.jobPostForm.controls.job_type.setValue(2);
    this.jobPostForm.controls.candidate_type.setValue(1);
    this.jobPostForm.controls.salary_type.setValue('monthly');
    this.jobPostForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
      let country = this.countries.find(obj=>obj._id == value);
      this.commonService.getAll(`/api/get-city/codewise/${country.code}`).subscribe(data=>{
        if(data.status == 200){ this.coutryWiseCity =  data.data}
      })
      this.jobPostForm.controls['currency'].setValue(value); 
      }

    });

    this.jobPostForm.get('city_id').valueChanges.subscribe(value=>{
      if(value){
        let city  = this.cities.find(obj=>obj._id == value);
        this.commonService.getLatLong(city.name).then(response=>{
          if(response.status == "OK"){
              let lat_long = response.results[0]['geometry']['location']
              this.jobPostForm.controls.job_lat.setValue(lat_long.lat)
              this.jobPostForm.controls.job_long.setValue(lat_long.lng)
          }else{
            this.jobPostForm.controls.job_lat.setValue('')
            this.jobPostForm.controls.job_long.setValue('')
          }
        })
      }else{
        this.jobPostForm.controls.job_lat.setValue('')
        this.jobPostForm.controls.job_long.setValue('')
      }
    })

    /*this.jobPostForm.get('candidate_type').valueChanges.subscribe(value=>{
      if(value){
        this.jobPostForm.get('candidate_type').setValue(1)
      }else{
        this.jobPostForm.get('candidate_type').setValue(0)
      }
    })*/

      this.asyncInit();
  }

  initReferal(): FormGroup{
    return this.fb.group({
      question_name: ['',[Validators.required]],
    });
  }

  setTrail =(e)=>{
    var is_chrome = /chrome/i.test( navigator.userAgent );
    if(is_chrome){
      let pageHeight = $('.registrationForm').outerHeight();
      var top = e.pageY;
      if(top >= pageHeight){
        top = pageHeight;
      }else if(e.pageY < 1000){
        top = e.pageY;
      }else if(e.pageY > 1000 && e.pageY < 1500){
        top = e.pageY + 500;
      }
      else if(e.pageY > 1500 && e.pageY < 2000){
        top = e.pageY + 1000;
      }else{
        top = e.pageY + 1200;
      }
      return top      
    }else{
      return e.pageY +10
    }

  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.industries = data['industries'];
        let courses = _.uniqBy(data['courses'][""],(e)=>{
          return e.name
        });
        let anyCourse = courses.find(obj=> obj.name.toLowerCase().replace(/ +/g, "") == 'anycourse');
        let newcourse = courses.filter(obj=>obj._id != anyCourse._id);
        newcourse.unshift(anyCourse)
        this.under_graduates = {"":newcourse};
        this.under_graduate_deg = _.uniqBy(data['courses'][""],(e)=>{
          return e.name
        });
      }
    });
    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
       this.commonService.callFooterMenu(1);
       if(data && data.status ==200){
        this.job_roles = data['job_roles'];
        this.other_mst_data = data['other_mst'];
        let otherData = data['other_mst'];
        this.emplymentFor = otherData.filter((data) => data.entity == 'emplyment_for');
        this.experiance = data['allExprience'];
        expdata = this.experiance;
        this.empType = otherData.filter((data) => data.entity == 'employement_type');
        this.noOfVacancies = otherData.filter((data) => data.entity == 'no_of_vacancies');
        this.all_currency = data['all_currency'];
        this.skillData = data['skills'].map((obj) => { return { display: obj.skill, value: obj.skill } });
        this.post_graduates = data['post_graduate'];
       // this.under_graduates = data['under_graduate'];
        this.post_graduate_deg = data['post_graduate_deg'];
        //this.under_graduate_deg = data['under_graduate_deg'];
        this.languages = data['alllanguage'];
        this.departments = data['all_department'];
        this.all_departments = data['departments'];   
        if(localStorage.getItem('job_preview')){
          let prevData = JSON.parse(localStorage.getItem('job_preview'));
          this.setPreviewFormValue(prevData);
        }
        if(this.job_id){
          this.commonService.show('/api/employer-job', this.job_id)
            .subscribe((data)=>{
              this.job_data = data.data;
              this.setFormValue(this.job_data);
            }, (error)=>{});
          }      
       }
    });

        this.commonService.getAll('/api/employer-job?job_type='+ 2)
            .subscribe((data)=>{
              if(data.status == 200){
                this.premium_posted_job = data.data;
              }
              
            }, (error)=>{});

        
  }

  searchAutoComplete(key){
    if(key.trim().length){
    this.commonService.getAll('/api/employer/job-title/search?q=' + key)
        .subscribe(data=>{
          if(data.status ==200){
            this.job_titles = data.data;
            this.show_jb_title_srh = true;
          }
        },error=>{});
    }else{
      this.show_jb_title_srh = false;
    }
  }

    autoCompleteSearch(data) {
      this.show_jb_title_srh = false;
      this.jobPostForm.get('job_title').setValue(data.job_title);

  }
  
 /* submitPost(){
    this.showloader = true;
    this.isSubmit =true;
    let skills= this.skill_key.map(obj=>obj.value);
    skills ? this.jobPostForm.get('required_skill').setValue(skills):'';
    if(this.isSubmit && this.jobPostForm.invalid){
      return ;
    }
    this.changeSubmitVal();
    console.log(this.jobPostForm.value);
    this.commonService.create('/api/employer-job', this.jobPostForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.message.success('Successfully job posted');
            this.clearForm();
            this.router.navigate(['/employer/job-posted-premium-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }*/

  jobPreview(){
    this.showloader = true;
    this.isSubmit = true;
    var regex = /(<([^>]+)>)/ig
    let skills = this.skill_key.map(obj => obj.value);
    skills ? this.jobPostForm.get('required_skill').setValue(skills) : '';
    if (this.jobPostForm.invalid) {
      this.showloader = false;
      return;
    }

      if(this.checkIfEmailInString(this.jobPostForm.get('about_this_job').value)){
        this.message.error('Do not write email on about this job field');
        return false;
      }
      else if(this.checkIfPhoneNoInString(this.jobPostForm.get('about_this_job').value)){
        this.message.error('Do not write phone no on about this job field');
        return false;
      }
      else if(this.checkIfUrlInString(this.jobPostForm.get('about_this_job').value)){
        this.message.error('Do not write website url on about this job field');
        return false;
      }
      else if(this.checkIfUrlInString(this.jobPostForm.get('about_this_job').value)){
        this.message.error('Do not write website url on about this job field');
        return false;
      }
       else if(this.checkIfEmailInString(this.jobPostForm.get('benefits').value)){
        this.message.error('Do not write email on benefits field');
        return false;
      }
       else if(this.checkIfPhoneNoInString(this.jobPostForm.get('benefits').value)){
        this.message.error('Do not write phone no on benefits field');
        return false;
      }
       else if(this.checkIfUrlInString(this.jobPostForm.get('benefits').value)){
        this.message.error('Do not write website url on benefits field');
        return false;
      }
      else if(this.checkIfUrlInString(this.jobPostForm.get('benefits').value)){
        this.message.error('Do not write website url on benefits field');
        return false;
      }
      else if(this.checkIfEmailInString(this.jobPostForm.get('primary_responsibility').value)){
        this.message.error('Do not write email on primary responsibility');
        return false;
      }
       else if(this.checkIfPhoneNoInString(this.jobPostForm.get('primary_responsibility').value)){
        this.message.error('Do not write phone no on primary responsibility');
        return false;
      }
       else if(this.checkIfUrlInString(this.jobPostForm.get('primary_responsibility').value)){
        this.message.error('Do not write website url on primary responsibility');
        return false;
      }
      else if(this.jobPostForm.get('primary_responsibility').value.includes('http:')){
        this.message.error('Do not write website url on primary responsibility');
        return false;
      }
      else if(this.checkIfEmailInString(this.jobPostForm.get('special_notes').value)){
        this.message.error('Do not write email on special notes');
        return false;
      }
       else if(this.checkIfPhoneNoInString(this.jobPostForm.get('special_notes').value)){
        this.message.error('Do not write phone no on special notes');
        return false;
      }
       else if(this.checkIfUrlInString(this.jobPostForm.get('special_notes').value)){
        this.message.error('Do not write website url on special notes');
        return false;
      }
      else if(this.jobPostForm.get('special_notes').value.includes('http:')){
        this.message.error('Do not write website url on special notes');
        return false;
      }

      if(this.jobPostForm.get('about_this_job').value.replace(regex,'') == '' ){
        this.message.error('about this job field required');
        return false;
      }

      if(this.jobPostForm.get('benefits').value.replace(regex,'') == '' ){
        this.message.error('benefits field required');
        return false;
      }
      if(this.jobPostForm.get('primary_responsibility').value.replace(regex,'') == '' ){
        this.message.error('primary responsibility field required');
        return false;
      }

      if(this.jobPostForm.get('condition_type').value ===1){
        this.jobPostForm.get('conditions').setValue(this.relevant_conditions) 
      }else if(this.jobPostForm.get('condition_type').value ===2){
        this.jobPostForm.get('conditions').setValue(this.alljob_conditions)
      }else{
        this.jobPostForm.get('conditions').setValue(this.conditionQuestionForm.value)
      }

      this.commonService.getDataWithPost(`/api/job-check/same-exist`,
        {job_title:this.jobPostForm.get('job_title').value,city_id:this.jobPostForm.get('city_id').value}).subscribe(data=>{
        if(data.is_exist !=1){
          localStorage.setItem('job_preview',JSON.stringify(this.jobPostForm.value)); 
          this.router.navigate(['/employer/job-post/preview']);          
        }else{
          this.message.error('Same job title and city is already exist');
        }
      });  


  }

   checkIfEmailInString(text) { 
    var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
  }

  checkIfPhoneNoInString(text) { 
    var re = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/img;
    return re.test(text);
  }

  checkIfUrlInString(text){
    var re = new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?");
    return re.test(text);
  }


  public requestAutocompleteSkill = (text: string): Observable<Response> => {
   const url = `/api/skill/filter?q=${text}`;
   return this.http.get<any>(url).pipe(map(data =>  _.uniqBy(data.data,(e)=>{
              return e.value
            })));
  };

  clearForm() {
    this.jobPostForm.reset();
    this.jobPostForm.controls['job_title'].setValue('');
    this.jobPostForm.controls['country_id'].setValue("");
    this.jobPostForm.controls['city_id'].setValue("");
    this.jobPostForm.controls['about_this_job'].setValue("");
    this.jobPostForm.controls['industry_id'].setValue("");
    //this.jobPostForm.controls['received_email'].setValue("");
    this.jobPostForm.controls['employement_for'].setValue("");
    this.jobPostForm.controls['required_skill'].setValue("");
    this.jobPostForm.controls['benefits'].setValue("");
    this.jobPostForm.controls['min_experiance'].setValue("");
    this.jobPostForm.controls['max_experiance'].setValue("");
    this.jobPostForm.controls['min_qualification'].setValue("");
    this.jobPostForm.controls['max_qualification'].setValue("");
    this.jobPostForm.controls['min_qualification_stream'].setValue("");
    this.jobPostForm.controls['max_qualification_stream'].setValue("");
    this.jobPostForm.controls['primary_responsibility'].setValue("");
    this.jobPostForm.controls['employment_type'].setValue("");
    this.jobPostForm.controls['number_of_vacancies'].setValue("");
    this.jobPostForm.controls['language_pref'].setValue("");
    this.jobPostForm.controls['currency'].setValue("");
    this.jobPostForm.controls['min_monthly_salary'].setValue("");
    this.jobPostForm.controls['max_monthly_salary'].setValue("");
    this.jobPostForm.controls['special_notes'].setValue("");
    this.jobPostForm.controls['graduate_and_or_post_graduate'].setValue('');
    this.jobPostForm.controls['candidate_type'].setValue('');
    this.jobPostForm.controls['condition_type'].setValue(2);
    this.storeMultiSelect.industries_value.length =0;
    this.storeMultiSelect.industries_name.length =0;
    this.storeMultiSelect.department_value.length =0;
    this.storeMultiSelect.department_name.length =0;
    this.storeMultiSelect.undergradute_value.length =0;
    this.storeMultiSelect.undergraduate_name.length =0;
    this.storeMultiSelect.postgraduate_value.length =0;
    this.storeMultiSelect.postgraduate_name.length =0;
    this.noSearchSelect.lang_value.length =0;
    this.noSearchSelect.lang_name.length =0;
    this.skill_key = '';
    this.storeMultiSelect.postgraduate_sub_value.length =0;
    this.storeMultiSelect.undergraduate_sub_value.length =0;
  }

   changeSubmitVal(){
    this.jobPostForm.patchValue({
      industry_id:this.storeMultiSelect.industries_value,
      min_qualification:this.storeMultiSelect.undergradute_value,
      max_qualification:this.storeMultiSelect.postgraduate_value,
      language_pref:this.noSearchSelect.lang_value,
      department_id:this.storeMultiSelect.department_value   
    });
  }


  getOnchangeEvent(type){
    if(type =='undergraduate'){
      this.underGraduateDiv = true;
    }else if(type =='postgraduate'){
      this.postGraduateDiv = true;
    }else if(type =='industries'){
      this.industriesDiv = true;
    }else if(type=='language'){
      this.laguageDiv = true;
    }else if(type=='department'){
      this.departmentDiv = true;
    }
    
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
        if (e.field_name == 'max_qualification') {
         this.storeMultiSelect.postgraduate_sub_value = this.storeMultiSelect.postgraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));
         this.jobPostForm.patchValue({
          max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value
         });
        }else if(e.field_name == 'min_qualification'){
         this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));         
          this.jobPostForm.patchValue({
            min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value
          });
        }
      }
    }
    if(e.field_name =='min_qualification'){
      this.jobPostForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(e.field_name =='max_qualification'){
      this.jobPostForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(e.field_name =='industries'){
      this.jobPostForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='departments'){
      this.jobPostForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  getSubChange(e){
    let event : any= e;
    if (e.event) {
      let index = e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index == -1) {
        e.storedSubValue.push({'parent_name' :e.parent_name, 'value': e.value});
      }
    } else {
      let index =  e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index != -1) {
        e.storedSubValue.splice(index, 1);
      }
    }  
    if (e.field_name == 'min_qualification') {
      this.jobPostForm.patchValue({
        min_qualification_stream: e.storedSubValue
      });
    } else if (e.field_name == 'max_qualification') {
      this.jobPostForm.patchValue({
        max_qualification_stream: e.storedSubValue
      });
    }
  }    

  getSubData(parent,type){
    if(type =='post-graduate'){
      if(this.storeMultiSelect.postgraduate_sub_value){
        let subData = this.storeMultiSelect.postgraduate_sub_value.filter(obj=>obj.parent_name == parent);
        return subData.length ? subData.length: '';        
      }else{
        return '';
      }

    }else if(type =='under-graduate'){
      if(this.storeMultiSelect.undergraduate_sub_value){
        let subData = this.storeMultiSelect.undergraduate_sub_value.filter(obj=>obj.parent_name == parent);
        return subData.length ? subData.length: '';        
      }else{
        return '';
      }
    }

  }

  noSearchGetchange(e){
     if(e.event){
      let index = this.noSearchSelect.lang_value.indexOf(e.value);
      if(index ==-1){
        this.noSearchSelect.lang_value.push(e.value);
        this.noSearchSelect.lang_name.push(e.name); 
      }
    }else{
      let index = this.noSearchSelect.lang_value.indexOf(e.value);
      if(index !=-1){
        this.noSearchSelect.lang_value.splice(index,1);
        this.noSearchSelect.lang_name.splice(index,1);
      }
    }
    this.jobPostForm.patchValue({
      language_pref: this.noSearchSelect.lang_value
      });
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.jobPostForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "department":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.jobPostForm.patchValue({
        department_id: this.storeMultiSelect.department_value
       });
        break;  
      case "undergraduate":
      let nameUnd = this.storeMultiSelect.undergraduate_name[indx];
        this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(nameUnd));         
          this.jobPostForm.patchValue({
            min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value
          });
       this.storeMultiSelect.undergradute_value.splice(indx, 1);
       this.storeMultiSelect.undergraduate_name.splice(indx, 1);
       this.jobPostForm.patchValue({
          min_qualification: this.storeMultiSelect.undergradute_value
       });
        break; 
      case "postgraduate":
      let namePost = this.storeMultiSelect.postgraduate_name[indx];
         this.storeMultiSelect.postgraduate_sub_value = this.storeMultiSelect.postgraduate_sub_value.filter(obj=> obj.parent_name !== String(namePost));
         this.jobPostForm.patchValue({
          max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value
         });
       this.storeMultiSelect.postgraduate_value.splice(indx, 1);
       this.storeMultiSelect.postgraduate_name.splice(indx, 1);
       this.jobPostForm.patchValue({
        max_qualification: this.storeMultiSelect.postgraduate_value
       });
        break; 
      case "language":
       this.noSearchSelect.lang_value.splice(indx, 1);
       this.noSearchSelect.lang_name.splice(indx, 1);
       this.jobPostForm.patchValue({
        language_pref: this.noSearchSelect.lang_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  checknoSearchmultiSelect(e){
    this.laguageDiv = e.status;
  }


  multiselectAll(e){  
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='min_qualification'){
      this.jobPostForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(filed =='max_qualification'){
      this.jobPostForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(filed =='industries'){
      this.jobPostForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='departments'){
      this.jobPostForm.patchValue({
      department_id: e.storedValue
      });
    }
    
  }

  checkmultiselect(e){
    if(e.field_name =='min_qualification'){
      this.underGraduateDiv = e.status;
    }else if(e.field_name =='max_qualification'){
      this.postGraduateDiv = e.status;
    }else if(e.field_name =='industries'){
      this.industriesDiv = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDiv = e.status;
    }
  }

    jobOnChange(jb_id){
    if(jb_id=='1'){
      this.router.navigate(['/employer/job-posted-premium']);
      this.clearForm();
      this.jobPostForm.controls.job_type.setValue(2);
      this.jobPostForm.controls.salary_type.setValue('monthly');
    }else{
      let data = this.premium_posted_job.find((jb)=>jb._id ==jb_id);
      this.setFormValue(data);
    }
    
  }

  setFormValue(data){
    this.storeMultiSelect.industries_value = data.industry_ids;
    this.storeMultiSelect.industries_name = data.industry_name;
    this.storeMultiSelect.department_value = data.department_ids;
    this.storeMultiSelect.department_name = data.department_name;
    if(!_.isEmpty(data.qualifications)){
      this.storeMultiSelect.undergradute_value = data.qualifications;
      this.storeMultiSelect.undergraduate_name = data.qualifications_name;
    }else{
      this.storeMultiSelect.undergradute_value = [];
      this.storeMultiSelect.undergraduate_name = [];      
    }
    //this.storeMultiSelect.postgraduate_value = data.max_qualification;
    //this.storeMultiSelect.postgraduate_name = data.max_qualification_name;
    //this.storeMultiSelect.undergraduate_sub_value = data.min_qualification_stream ?this.under_graduate_deg.filter(obj=>data.min_qualification_stream.find(value=> value== obj._id)).map(obj=>({'parent_name' :obj.parent_name, 'value': obj._id})):[];
    //this.storeMultiSelect.postgraduate_sub_value = data.max_qualification_stream ?this.post_graduate_deg.filter(obj=>data.max_qualification_stream.find(value=> value== obj._id)).map(obj=>({'parent_name' :obj.parent_name, 'value': obj._id})):[];
    this.noSearchSelect.lang_value = data.language_pref_id;
    this.noSearchSelect.lang_name = data.language_pref_name;
    this.skill_key = data.required_skill.map((obj)=>{ return {value:obj, display:obj}});

    if(data.condition_type === 1){
      this.relevant_conditions.expeiance = data.conditions.expeiance;
      this.relevant_conditions.industry = data.conditions.industry;
      this.relevant_conditions.age = data.conditions.age;
      this.relevant_conditions.location = data.conditions.location;
      this.relevant_conditions.gender = data.conditions.gender;
      this.relevant_conditions.salary = data.conditions.salary;
      this.relevant_conditions.education = data.conditions.education;
    }else if(data.condition_type === 2){
      this.alljob_conditions.all = data.conditions.all
    }else if(data.condition_type === 3){
        this.questions = <FormArray> this.conditionQuestionForm.get('questions')
        data.conditions.questions.forEach((ques,i)=>{
            this.questions.controls[i].get('question_name').setValue(ques.question_name);
            if((data.conditions.questions.length -i) !=1){
              this.questions.push(this.initReferal());
            }
        })
        
    }
    
    this.jobPostForm.patchValue({
        job_title : data.job_title,
        country_id: data.country._id,
        city_id: data.city._id,
        about_this_job: data.about_this_job,
        industry_id: this.storeMultiSelect.industries_value,
        department_id: this.storeMultiSelect.department_value,
        employement_for: data.employement_for_id,
        required_skill: data.required_skill,
        benefits: data.benefits,
        min_experiance: data.min_experiance_id,
        max_experiance: data.max_experiance_id,
        min_qualification: this.storeMultiSelect.undergradute_value,
        max_qualification: this.storeMultiSelect.postgraduate_value,
        min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value,
        max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value,
        graduate_and_or_post_graduate: String(data.graduate_and_or_post_graduate),
        primary_responsibility: data.primary_responsibility,
        employment_type: data.employment_type_id,
        language_pref: this.noSearchSelect.lang_value,
        number_of_vacancies: data.number_of_vacancies,
        min_monthly_salary: data.min_monthly_salary,
        max_monthly_salary: data.max_monthly_salary,
        special_notes: data.special_notes,
        //received_email: data.received_email,
        currency: data.currency,
        salary_type: data.salary_type,
        candidate_type:data.candidate_type,
        condition_type:data.condition_type ? data.condition_type:2
    });
  }

  setPreviewFormValue(data){
    this.storeMultiSelect.industries_value = data.industry_id;
    this.storeMultiSelect.industries_name = this.industries[""].filter(obj=> data.industry_id.indexOf(obj._id)!= -1).map(obj=>obj.name);
    this.storeMultiSelect.department_value = data.department_id;
    this.storeMultiSelect.department_name = this.all_departments.filter(obj=> data.department_id.find(ob=> ob==obj._id)).map(obj=>obj.name);
    this.storeMultiSelect.undergradute_value = data.min_qualification;
    this.storeMultiSelect.undergraduate_name = this.under_graduate_deg.filter(obj=> data.min_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
    //this.storeMultiSelect.undergraduate_sub_value = data.min_qualification_stream ?data.min_qualification_stream:[];
    //this.storeMultiSelect.postgraduate_value = data.max_qualification;
    //this.storeMultiSelect.postgraduate_name = this.post_graduate_deg.filter(obj=> data.max_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
    //this.storeMultiSelect.postgraduate_sub_value = data.max_qualification_stream ?data.max_qualification_stream:[];
    this.noSearchSelect.lang_value = data.language_pref;
    this.noSearchSelect.lang_name = this.languages.filter(obj=> data.language_pref.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
    this.skill_key = data.required_skill.map((obj) => { return { value: obj, display: obj } });
    
    this.jobPostForm.patchValue({
      job_title: data.job_title,
      country_id: data.country_id,
      city_id: data.city_id,
      about_this_job: data.about_this_job,
      industry_id: this.storeMultiSelect.industries_value,
      department_id: this.storeMultiSelect.department_value,
      employement_for: data.employement_for,
      required_skill: data.required_skill,
      benefits: data.benefits,
      min_experiance: data.min_experiance,
      max_experiance: data.max_experiance,
      min_qualification: this.storeMultiSelect.undergradute_value,
      max_qualification: this.storeMultiSelect.postgraduate_value,
      min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value,
      max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value,
      graduate_and_or_post_graduate: String(data.graduate_and_or_post_graduate),
      primary_responsibility: data.primary_responsibility,
      employment_type: data.employment_type,
      language_pref: this.noSearchSelect.lang_value,
      number_of_vacancies: data.number_of_vacancies,
      min_monthly_salary: data.min_monthly_salary,
      max_monthly_salary: data.max_monthly_salary,
      special_notes: data.special_notes,
      //received_email: data.received_email,
      currency: data.currency,
      salary_type: data.salary_type,
      candidate_type:data.candidate_type,
    });
  }

  updateForm(){
    this.showloader = true;
    this.isSubmit = true;
    if(this.jobPostForm.invalid){
      this.showloader = false;
      return;
    }
    this.commonService.update('/api/employer-job', this.job_id,this.jobPostForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.message.success('Successfully update job');
            this.clearForm();
            this.router.navigate(['/employer/job-posted-premium-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }

  checkMinMaxSal(c: FormControl) {
    if (!c.root && !c.root.get('min_monthly_salary')) {
      return null;
    }
    if (c.value && c.value < c.root.get('min_monthly_salary').value) {
      return { error_min_max: true };
    }
  }


  checkMinMaxExp(c: FormControl) {
    if (!c.root && !c.root.get('min_experiance')) {
      return null;
    }
    if(c.value){
      let minExpValueOrder = expdata.find(obj=>obj._id == c.root.get('min_experiance').value);
      let maxExpValueOrder = expdata.find(obj=>obj._id == c.value);
      if(!_.isEmpty(maxExpValueOrder) && !_.isEmpty(minExpValueOrder) && maxExpValueOrder.order < minExpValueOrder.order){
        return { error_min_max_exp: true };
      }
    }
  }

  setConditionType(type){
    this.jobPostForm.get('condition_type').setValue(type)
  }

  addQuestionArray(): void {
    //this.is_submitted = false;
    this.questions = this.conditionQuestionForm.get('questions') as FormArray;
    this.questions.push(this.initReferal());
  }

  deleteQuestionArray(index: number){
    //this.is_submitted =false;
    this.questions = this.conditionQuestionForm.get('questions') as FormArray;
    this.questions.removeAt(index);
  }

  get questionFormData() { return <FormArray>this.conditionQuestionForm.get('questions')['controls']; }

  setConditionValue(value){
      console.log(value)
  }

}
