import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobPostPreviewComponent } from './job-post-preview.component';

describe('JobPostPreviewComponent', () => {
  let component: JobPostPreviewComponent;
  let fixture: ComponentFixture<JobPostPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPostPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPostPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
