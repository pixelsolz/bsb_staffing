import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute} from '@angular/router';
import * as _ from 'lodash'; 
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-job-post-preview',
  templateUrl: './job-post-preview.component.html',
  styleUrls: ['./job-post-preview.component.css']
})
export class JobPostPreviewComponent implements OnInit {
  previewData: any;
  userData: any;
  templateData: any={
  	job_title:'',
  	about_this_job:'',
  	benefits:'',
  	currency:'',
  	employement_for:'',
  	employment_type:'',
  	max_experiance:'',
  	min_experiance:'',
  	max_monthly_salary:'',
  	min_monthly_salary:'',
  	number_of_vacancies:'',
  	primary_responsibility:'',
  	required_skill:'',
  	salary_type:'',
  	special_notes:'',
  	language_pref:'',
  	min_qualification:'',
  	max_qualification:'',
  	job_country:'',
  	job_city:'',
  	
  };

  countries: any;
  cities:any;
  degrees:any;

  constructor(private commonService: CommonService,private loadingBar: LoadingBarService, private message:MessageService, private router: Router, private activeRoute: ActivatedRoute,) { 
  	this.previewData = JSON.parse(localStorage.getItem('job_preview'));
  	this.userData = JSON.parse(localStorage.getItem('user'));

  }

  ngOnInit() {
  	this.asyncInit();
  }
  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.degrees =_.uniqBy(data['courses'][""],(e)=>{
          return e.name
        });
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
        this.setDataValue(data);
      }
    });

  }

  setDataValue(data){
  	let otherData = data.other_mst;
  	this.templateData.job_title = this.previewData.job_title;
  	this.templateData.about_this_job = this.previewData.about_this_job;
  	this.templateData.benefits = this.previewData.benefits;
  	this.templateData.job_city = this.cities.find(obj=>obj._id == this.previewData.city_id).name;;
  	this.templateData.job_country = this.countries.find(obj=>obj._id == this.previewData.country_id).name;
  	this.templateData.currency = this.countries.find(obj=>obj._id == this.previewData.currency).currency_code;
  	this.templateData.employement_for = otherData.find(obj=>obj._id == this.previewData.employement_for).name;
  	this.templateData.employment_type = otherData.find(obj=>obj._id == this.previewData.employment_type).name;
  	this.templateData.max_experiance = data.allExprience.find(obj=>obj._id == this.previewData.max_experiance).name;
  	this.templateData.min_experiance = data.allExprience.find(obj=>obj._id == this.previewData.min_experiance).name;
  	this.templateData.max_monthly_salary = this.previewData.max_monthly_salary;
  	this.templateData.min_monthly_salary = this.previewData.min_monthly_salary;
  	this.templateData.number_of_vacancies = this.previewData.number_of_vacancies;
  	this.templateData.primary_responsibility = this.previewData.primary_responsibility;
  	this.templateData.required_skill = this.previewData.required_skill;
  	this.templateData.salary_type = this.previewData.salary_type;
  	this.templateData.special_notes = this.previewData.special_notes;
    this.templateData.candidate_type = this.previewData.candidate_type;
  	this.templateData.language_pref = data.alllanguage.filter(obj=> this.previewData.language_pref.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
  	this.templateData.min_qualification = this.degrees.filter(obj=> this.previewData.min_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;

   /* this.templateData.min_qualification =this.templateData.min_qualification.map(name=> {
      if(this.previewData.min_qualification_stream){
        let hassub =this.previewData.min_qualification_stream.filter(obj=> obj.parent_name == name);
        if(hassub && hassub.length >0){
          return name + '(' + data.under_graduate_deg.filter(obj=> hassub.find(ob=> ob.value==obj._id)).map(obj=>obj.name) + ')';
        }else{
          return name;
        }        
      }else{
        return name;
      }
    });*/
    if(!_.isEmpty(this.previewData.max_qualification)){
    this.templateData.max_qualification = data.post_graduate_deg.filter(obj=> this.previewData.max_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
    this.templateData.max_qualification =this.templateData.max_qualification.map(name=> {
      if(this.previewData.max_qualification_stream){
      let hassub =this.previewData.max_qualification_stream.filter(obj=> obj.parent_name == name);
        if(hassub && hassub.length >0){
          return name + '(' + data.post_graduate_deg.filter(obj=> hassub.find(ob=> ob.value==obj._id)).map(obj=>obj.name) + ')';
        }else{
          return name;
        }         
      }else{
        return name;
      }
    });      
    }


  }
  editJob(){
    if(this.previewData.job_type == 1){
      if(this.previewData.id){
         this.router.navigate(['employer/job-posted-edit', this.previewData.id]);
      }else{
        this.router.navigate(['employer/job-posted']);
      }
    }else if(this.previewData.job_type == 2){
      if(this.previewData.id){
        this.router.navigate(['employer/job-posted-premium-edit', this.previewData.id]);
      }else{
        this.router.navigate(['employer/job-posted-premium']);
      }
    }
  }
  submitPost(){
    if(this.previewData.min_qualification_stream && this.previewData.min_qualification_stream.length){
      this.previewData.min_qualification_stream.forEach(obj=>{
        this.previewData.min_qualification.push(obj.value);
      });      
    }
    if(this.previewData.max_qualification_stream && this.previewData.max_qualification_stream.length){
      this.previewData.max_qualification_stream.forEach(obj=>{
        this.previewData.max_qualification.push(obj.value);
      });      
    }
    this.previewData.job_status=1;
    if(this.previewData.id){
      this.commonService.update('/api/employer-job', this.previewData.id, this.previewData)
      .subscribe((data) => {
        if (data.status === 200) {
          this.message.success('Successfully update job');
          if(this.previewData.job_type == 1){
            if(this.activeRoute.snapshot.queryParams['from']){
              this.router.navigate(['/employer/recent-posted-job']);
            }else{
              this.router.navigate(['/employer/job-posted-list']);
            }
            
          }
          if(this.previewData.job_type == 2){
            if(this.activeRoute.snapshot.queryParams['from']){
              this.router.navigate(['/employer/recent-posted-job']);
            }else{
            this.router.navigate(['/employer/job-posted-premium-list']);
            }
          }
          localStorage.removeItem('job_preview');
        } else if (data.status == 500) {
          this.message.error(data.stats_text);
        }
      }, (error) => {
        //this.showloader = false;
      });
    }else{
      this.previewData.is_published=1;
    	this.commonService.create('/api/employer-job', this.previewData)
        .subscribe((data) => {
           /*if (data.status === 200) {         	
            this.message.success('Successfully job posted');
            if(this.previewData.job_type == 1){
              this.router.navigate(['/employer/job-posted-list']);
            }
            if(this.previewData.job_type == 2){
              this.router.navigate(['/employer/job-posted-premium-list']);
            }
            localStorage.removeItem('job_preview');
          } else if (data.status == 500) {
            this.message.error(data.stats_text);
          }*/
        }, (error) => {
        });
          setTimeout(()=>{
            this.loadingBar.complete();
            this.message.success('Successfully job posted');
            if(this.previewData.job_type == 1){
              this.router.navigate(['/employer/job-posted-list']);
            }
            if(this.previewData.job_type == 2){
              this.router.navigate(['/employer/job-posted-premium-list']);
            }
            localStorage.removeItem('job_preview');            
          },5000)


    }
  }

  postLater(){
    if(this.previewData.min_qualification_stream && this.previewData.min_qualification_stream.length){
      this.previewData.min_qualification_stream.forEach(obj=>{
        this.previewData.min_qualification.push(obj.value);
      });      
    }
    if(this.previewData.max_qualification_stream && this.previewData.max_qualification_stream.length){
      this.previewData.max_qualification_stream.forEach(obj=>{
        this.previewData.max_qualification.push(obj.value);
      });      
    }
    this.previewData.job_status=0;
    if(this.previewData.id){
      this.commonService.update('/api/employer-job', this.previewData.id, this.previewData)
      .subscribe((data) => {
        if (data.status === 200) {
          this.message.success('Successfully update job');
          //this.clearForm();
          if(this.previewData.job_type == 1){
            if(this.activeRoute.snapshot.queryParams['from']){
              this.router.navigate(['/employer/recent-posted-job']);
            }
            else{
              this.router.navigate(['/employer/job-posted-list']);
            }
          }
          if(this.previewData.job_type == 2){
            if(this.activeRoute.snapshot.queryParams['from']){
              this.router.navigate(['/employer/recent-posted-job']);
            }else{
             this.router.navigate(['/employer/job-posted-premium-list']);             
            }

          }
          localStorage.removeItem('job_preview');
        } else if (data.status == 500) {
          this.message.error(data.stats_text);
        }
      }, (error) => {
        //this.showloader = false;
      });
    }else{
      this.previewData.is_published=0;
      this.commonService.create('/api/employer-job', this.previewData)
      .subscribe((data) => {
        //this.showloader = false;
        //this.isSubmit = false;
        if (data.status === 200) {
          
          this.message.success('Successfully job posted');
          //this.clearForm();
          if(this.previewData.job_type == 1){
            this.router.navigate(['/employer/job-posted-list']);
          }
          if(this.previewData.job_type == 2){
            this.router.navigate(['/employer/job-posted-premium-list']);
          }
          localStorage.removeItem('job_preview');
        } else if (data.status == 500) {
          this.message.error(data.stats_text);
        }
      }, (error) => {
        //this.showloader = false;
      });
    }
  }

  getSpaceValue(data){
    let value ='';
    if(Array.isArray(data)){
      value = data.toString().replace(/,/g, ', ');
    }else{
      return  data.replace(/,/g, ', ');
    }

  }

}
