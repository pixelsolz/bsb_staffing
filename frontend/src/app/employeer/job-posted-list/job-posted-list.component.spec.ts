import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobPostedListComponent } from './job-posted-list.component';

describe('JobPostedListComponent', () => {
  let component: JobPostedListComponent;
  let fixture: ComponentFixture<JobPostedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPostedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPostedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
