import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-manage-folder',
  templateUrl: './manage-folder.component.html',
  styleUrls: ['./manage-folder.component.css']
})
export class ManageFolderComponent implements OnInit {
	floder_list:any;
	count_data:number;
	selectedAll: any;
	checkedList:any;
  constructor(private global: Global,private route: ActivatedRoute,private commonService: CommonService,
              private authService: AuthService, private router: Router, private messageService: MessageService) {

    this.commonService.updateFolderData.subscribe(data=>{
      this.getData();
    });
  }

  ngOnInit() {
  	this.getData();
  }

  getData(){
  	this.commonService.getAll('/api/folders')
      .subscribe((data)=>{
         this.commonService.callFooterMenu(1);
        if(data.status == 200){
          this.floder_list = data.data;
          this.count_data = data.total_folder;
          
          console.log(this.floder_list);
        }
        
      }, (error)=>{});
  }

  removeFolder(id, index){
  	 Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonService.delete('/api/folders', id)
          .subscribe((data)=>{
          if(data.status ==200){   
             this.floder_list.splice(index,1);          
            Swal.fire(
              'Deleted!',
              'Your folder has been deleted.',
              'success'
            )
           }
          },(error)=>{});

      }
          
    }); 
  }

  selectAll() {
    for (var i = 0; i < this.floder_list.length; i++) {
      this.floder_list[i].selected = this.selectedAll;
    }
    //this.getCheckedItemList();
  }
  checkIfAllSelected() {
    this.selectedAll = this.floder_list.every(function(item:any) {
        return item.selected == true;
      })
    //this.getCheckedItemList();
  }

  removeAllFolder(){
    this.checkedList = [];
    for (var i = 0; i < this.floder_list.length; i++) {
      if(this.floder_list[i].selected)
      this.checkedList.push(this.floder_list[i]._id);
    }
    //this.checkedList = JSON.stringify(this.checkedList);
    console.log(this.checkedList);
    	if(this.checkedList.length > 0){
    		Swal.fire({
		      title: 'Are you sure?',
		      text: "You won't be able to revert this!",
		      type: 'warning',
		      showCancelButton: true,
		      confirmButtonColor: '#3085d6',
		      cancelButtonColor: '#d33',
		      confirmButtonText: 'Yes, delete it!'
		    }).then((result) => {
		      if (result.value) {
		        this.commonService.getAll('/api/folder/delete-all?ids='+this.checkedList)
		          .subscribe((data)=>{
		          if(data.status ==200){   
		              this.floder_list = data.data;
          				this.count_data = data.total_folder;          
		            Swal.fire(
		              'Deleted!',
		              'Your file has been deleted.',
		              'success'
		            )
		           }
		          },(error)=>{});

		      }
		          
		    }); 
    	}else{
    		Swal.fire({
		      //title: 'Are you sure?',
		      text: "Please select some folder(s) you want to delete.",
		      type: 'warning',
		    });
    	}
  }
}
