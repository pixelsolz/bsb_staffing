import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-people-search',
  templateUrl: './people-search.component.html',
  styleUrls: ['./people-search.component.css']
})
export class PeopleSearchComponent implements OnInit {
	peopleSearchForm : FormGroup;
	countries: any;
	cities:any;
  storeMultiSelect :any ={
      country_value:[],
      country_name:[],
      city_value:[],
      city_name:[],
    };
    countryArray: any=[];
    countryDiv:boolean=false;
    cityArray: any=[];
    cityDiv: boolean=false;
    queryParam: string;
    searchSavedData: any={};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, private activeRoute:ActivatedRoute) {     

    }

  ngOnInit() {
  	this.peopleSearchForm=this.fb.group({
  		search_candidate : [''],
  		search_by : [''],
  		country_id :[''],
  		city_id :[''],
  		
  	})
  	this.asyncInit();
    this.peopleSearchForm.controls['search_by'].setValue('search_by_name');
    this.peopleSearchForm.controls['country_id'].valueChanges.subscribe(value=>{
      let countryCode = this.countries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
      this.storeMultiSelect.city_value = this.cityArray[""].filter(obj=>this.storeMultiSelect.city_value.indexOf(obj._id) != -1).map(obj=>obj._id);
      this.storeMultiSelect.city_name = this.cityArray[""].filter(obj=>this.storeMultiSelect.city_name.indexOf(obj.name) != -1).map(obj=>obj.name);
      this.peopleSearchForm.patchValue({city_id: this.storeMultiSelect.city_value});
    });

  }

  getParamData(){
        if(this.activeRoute.snapshot.queryParams['modify']){
        this.queryParam = this.activeRoute.snapshot.queryParams['modify'];
        this.searchSavedData = localStorage.getItem('cvSearch') ? JSON.parse(localStorage.getItem('cvSearch')):'';
        this.setFormValue();
      }
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
          this.countries = data['countries'];
          this.countryArray = {"":data['countries']};
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
            this.cities = data['cities'];
            this.cityArray = [];
        }
    });

      this.commonService.getCommonData.subscribe(data=>{
         this.commonService.callFooterMenu(1);
          if(data && data.status ==200){
              this.getParamData();
          }
        });    
  }

  setFormValue(){
    let countryNames = this.searchSavedData.country_id.length ?this.countries.filter(obj=>this.searchSavedData.country_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let citiesName = this.searchSavedData.city_id.length ? this.cities.filter(obj=>this.searchSavedData.city_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    this.storeMultiSelect.country_value = this.searchSavedData.country_id;
    this.storeMultiSelect.country_name = countryNames;
    this.storeMultiSelect.city_value = this.searchSavedData.city_id;
    this.storeMultiSelect.city_name = citiesName;
    this.peopleSearchForm.patchValue({
      search_candidate :this.searchSavedData.search_candidate,
      search_by : this.searchSavedData.search_by,
      country_id :this.storeMultiSelect.country_value,
      city_id :this.storeMultiSelect.city_value,
    });
  }

  getOnchangeEvent(type){
   if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }   
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
   if(e.field_name =='country'){
      this.peopleSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.peopleSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.peopleSearchForm.patchValue({
        department_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.peopleSearchForm.patchValue({
          min_qualification: this.storeMultiSelect.city_value
       });
        break;     
      default:
        // code...
        break;
    }
  }


  multiselectAll(e){  
    console.log(e.storedValue);
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
   if(filed =='country'){
      this.peopleSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.peopleSearchForm.patchValue({
      city_id: e.storedValue
      });
    }   
  }

  checkmultiselect(e){
    if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }
  }

  changeSubmitVal(){
    this.peopleSearchForm.patchValue({
      country_id:this.storeMultiSelect.country_value,
      city_id: this.storeMultiSelect.city_value
    });
  }

  searchCv(){
    if(this.peopleSearchForm.get('search_candidate').value ==''){
      this.message.error('Please put search candiadte');
      return false;
    }
    this.changeSubmitVal();
    localStorage.setItem('cvSearch',JSON.stringify(this.peopleSearchForm.value));
    this.commonService.create('/api/search-history', {search_key:this.peopleSearchForm.get('search_candidate').value,
      search_data:this.peopleSearchForm.value,search_type:'people','is_modified':(this.queryParam?1:0)})
        .subscribe((data)=>{

        }, (error)=>{});
    this.router.navigate(['/employer/resumes'],{queryParams:{src:'cvSearch', prevsearch:'people-search'}});
  }



}
