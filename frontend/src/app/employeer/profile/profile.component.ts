import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  isEdit: boolean = false;
  isLicenseAgree: boolean =false;
  profileForm: FormGroup;
  submitted : boolean = false;
  user: any;
  passwordChange: boolean;
  profileDetails: boolean;
  editProfileDetails: boolean;
  countries: any;
  cities:any;
  coutryWiseCity: any;
  industries:any;
  companySizes:any;
  companyTypes:any;
  galleries: any={ files:[], previews:[]};
  remove_image_id: any=[];
  profileuploadImage: string;
  companyUploadLogoImage: string;
  companyvideoUrl:any;
  uploadDocumentName: string;
  taglineconfig: any = {
    //height: '200px',
    placeholder: 'Company Tag Line',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'Company Overview',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  nearByconfig: any = {
    //height: '200px',
    placeholder: 'Near By Place(Lorem ipsum dolor sit amet)',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  elementconfig: any = {
    //height: '200px',
    placeholder: 'Element',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService,private sanitizer: DomSanitizer) {
    this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
    this.galleries.previews = this.user.gallery_images.map((image)=>{
      return {id:image._id,url:this.global.imageUrl+'employer/gallery_images/'+image.name,type:'saved'}
    });
    this.companyvideoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.user.video_link ? this.user.video_link.name.replace("watch?v=", "embed/") :'');

    //console.log(this.user);
    this.passwordChange = false;
    this.profileDetails = true;
    this.editProfileDetails = false;
    this.asyncInit();
  }

  ngOnInit() {
  	this.profileForm =this.fb.group({
      id:'',
  		first_name:['', [Validators.required]],
      last_name:['', [Validators.required]],
  		email:['', [Validators.required,Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
  		phone:['', [Validators.required,Validators.pattern('^(\\+)?(\\d+)$'),Validators.minLength(8),Validators.maxLength(20)]],
  		password:'',
  		conf_password:'',
      dob_date:['', [Validators.required]],
      dob_month:['', [Validators.required]],
      dob_year:['', [Validators.required]],
      gender:['', [Validators.required]],
  		company_name:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address:['', [Validators.required]],
  		zipcode:['', [Validators.required]],
  		company_type:['', [Validators.required]],
  		company_size:['', [Validators.required]],
  		company_logo:['', [Validators.required]],
  		company_tagline:['', [Validators.required, Validators.maxLength(200)]],
  		company_short_desc:['', [Validators.required, Validators.maxLength(1000)]],
  		nearby_place:[''],
  		element:[''],
      passwordCheck:'',
      alternate_ph_no:['', [Validators.required,Validators.pattern('^(\\+)?(\\d+)$'),Validators.minLength(8),Validators.maxLength(20)]],
      designation:['',[Validators.required]],
      industry_id:['',[Validators.required]],
      profile_picture :'',
      company_website :['', [Validators.required]],
      company_email :['', [Validators.required,Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      business_licence_type: [''],
      business_licence :[''],
      license_agree: '',
      video_link:['',[Validators.pattern('^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+')]],
  	});
    this.profileForm.controls['license_agree'].setValue('');
    this.profileForm.patchValue({
      id:this.user.id,
      name:this.user.user_detail.name,
      first_name:this.user.user_detail.first_name,
      last_name:this.user.user_detail.last_name,
      email:this.user.email,
      phone:this.user.phone_no,
      password:'',
      conf_password:'',
      company_name:this.user.user_detail.company_name,
      country_id:!this.user.user_detail.country_id?'': this.user.user_detail.country_id,
      city_id:!this.user.user_detail.city_id?'': this.user.user_detail.city_id,
      address:this.user.user_detail.address,
      zipcode:this.user.user_detail.zipcode,
      alternate_ph_no:this.user.user_detail.alternate_ph_no,
      company_type:!this.user.user_detail.company_type ? '':this.user.user_detail.company_type,
      company_size:!this.user.user_detail.company_size ?'':this.user.user_detail.company_size,
      company_logo:this.user.user_detail.company_logo,
      company_tagline:this.user.user_detail.company_tagline,
      company_short_desc:this.user.user_detail.company_short_desc,
      nearby_place:this.user.user_detail.nearby_place,
      element:this.user.user_detail.element,
      designation:this.user.user_detail.designation,
      industry_id:!this.user.user_detail.industry?'': this.user.user_detail.industry,
      profile_picture:this.user.user_detail.profile_picture,
      company_website:(this.user.user_detail.company_website && this.user.user_detail.company_website != 'null') ? this.user.user_detail.company_website:'',
      company_email:(this.user.user_detail.company_email && this.user.user_detail.company_email != 'null') ? this.user.user_detail.company_email:'',
      video_link: this.user.video_link ?this.user.video_link.name:'',
      business_licence: this.user.user_detail.business_licence ?this.user.user_detail.business_licence:'',
      business_licence_type:  this.user.user_detail.business_licence_type ? this.user.user_detail.business_licence_type:'',
      license_agree:  this.user.user_detail.license_agree ? this.user.user_detail.license_agree:'',
      dob_date: this.user.user_detail.dob_date ? this.user.user_detail.dob_date :'',
      dob_month: this.user.user_detail.dob_month ? this.user.user_detail.dob_month :'',
      dob_year: this.user.user_detail.dob_year ? this.user.user_detail.dob_year :'',
      gender: this.user.user_detail.gender ? this.user.user_detail.gender :''
      //video_link: this.user.video_link
    });
    this.isLicenseAgree = this.user.user_detail.license_agree ? this.user.user_detail.license_agree:'';
    this.profileForm.get('passwordCheck').valueChanges.subscribe(value=>{
      this.passwordChange = value;
    });

    this.uploadDocumentName =this.user.user_detail.business_licence ? this.user.user_detail.business_licence: '';

    this.profileForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });

this.profileForm.get('license_agree').valueChanges.subscribe(value=>{
    this.isLicenseAgree = value;
  
});

var max = new Date().getFullYear(),
    min = max - 70,
    //max = max + 1;
    max = max ;
    //this.years.current_year = new Date().getFullYear();
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    }

  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.industries = data['all_industries'];
        this.companySizes = data['companySizes'];
        this.companyTypes = data['companyTypes'];

      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        if(this.user.user_detail.country_id){
         let country = this.countries.find(obj=>obj._id == this.user.user_detail.country_id);
         this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);       
        }
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      if(data && data.status ==200){
        this.commonService.callFooterMenu(1);
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.profileForm.controls; }

  updateProfile(){
    this.submitted = true;
    if (this.profileForm.invalid) {
        return;
    }
    if(!this.isLicenseAgree){
      this.message.error('Please check declaration checked box');
      return false;
    }
    const formData = new FormData();
    const formValue = this.profileForm.value;
    formData.append('id',formValue.id);
    formData.append('first_name',formValue.first_name);
    formData.append('last_name',formValue.last_name);
    formData.append('email',formValue.email);
    formData.append('phone_no',formValue.phone);
    formData.append('dob_date',formValue.dob_date);
    formData.append('dob_month',formValue.dob_month);
    formData.append('dob_year',formValue.dob_year);
    formData.append('gender',formValue.gender);
    formData.append('password',formValue.password);
    formData.append('company_name',formValue.company_name);
    formData.append('country_id',formValue.country_id);
    formData.append('city_id',formValue.city_id);
    formData.append('address',formValue.address);
    formData.append('zipcode',formValue.zipcode);
    formData.append('alternate_ph_no',formValue.alternate_ph_no);
    formData.append('company_type',formValue.company_type);
    formData.append('company_size',formValue.company_size);
    formData.append('company_logo',formValue.company_logo);
    formData.append('profile_picture',formValue.profile_picture);
    formData.append('company_tagline',formValue.company_tagline);
    formData.append('company_short_desc',formValue.company_short_desc);
    formData.append('nearby_place',formValue.nearby_place);
    formData.append('element',formValue.element);
    formData.append('designation',formValue.designation);
    formData.append('industry_id',formValue.industry_id);
    formData.append('company_website',formValue.company_website);
    formData.append('company_email',formValue.company_email);
    formData.append('remove_img_ids',this.remove_image_id);
    formData.append('video_link',formValue.video_link);
    formData.append('business_licence',formValue.business_licence);
    formData.append('business_licence_type',formValue.business_licence_type);
    formData.append('license_agree',formValue.license_agree);

    for(var i = 0; i < this.galleries.files.length; i++){
      formData.append('images_files[]',this.galleries.files[i].file);
    }

  	this.commonService.create('/api/employer-profile-update', formData)
  		.subscribe((data)=>{
        if(data.status == 200){
          this.message.success(data.status_text);
          localStorage.setItem('user',JSON.stringify(data.user));
          this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
          this.companyvideoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.user.video_link ? this.user.video_link.name.replace("watch?v=", "embed/") :'');
          this.passwordChange = false;
          this.profileDetails = true;
          this.editProfileDetails = false;
          this.commonService.callProfileUpdate(this.user);
          this.profileForm.patchValue({
            password:'',
            conf_password:'',
            passwordCheck:''
          });
          this.remove_image_id.length =0;
          //this.router.navigate(['employer/profile']);
        }else if(data.status == 500){
          this.message.error(data.status_text);
        }else if(data.status == 422){
          if(Object.entries(data.error).length){
            Object.entries(data.error).forEach(obj=>{
              this.message.error(obj[1]);
            })
          }
          
        }
  		},(error)=>{});
  }

  companyLogo(logo){
    const file = logo.files[0];
    const filesize = Math.round((file.size / 1024));
    const filetype = file.type;
    if(filesize > 2000 ){
      //alert("file size lese then 2 MB");
      this.message.error('Maximum file size allowed 2 MB');
    }else if(filetype != 'image/png'){
      //alert("Image must be png");
      this.message.error('File type must be png');
    }else{
      this.profileForm.controls['company_logo'].setValue(file);
       const reader = new FileReader();
      reader.onload = (event:any) => {
        this.companyUploadLogoImage = event.target.result;
      }
      reader.readAsDataURL(file);
    }

  }
  uploadeBusinessLicense(license){
    const file = license.files[0];
    const file_extence = file.name.split('.').pop().toLowerCase();
    this.uploadDocumentName = file.name;
    const filesize = Math.round((file.size / 1024));
    const filetype = file.type;
    if(filesize > 2000 ){
      //alert("file size lese then 2 MB");
      this.message.error('Maximum file size allowed 2 MB');
      return false;
    }

     if(file_extence == 'jpg' || file_extence == 'png' || file_extence == 'jpeg' || file_extence == 'pdf' || file_extence == 'docx' || file_extence == 'doc'){
       this.profileForm.controls['business_licence'].setValue(file);
        //  const reader = new FileReader();
        // reader.onload = (event:any) => {
        //   this.companyUploadLogoImage = event.target.result;
        // }
        // reader.readAsDataURL(file);
     } else {
      
      this.message.error('Upload file only allow jpg,png,jpeg,pdf,docx or doc!');
    }

  }
  profilePicture(picture){
    const file = picture.files[0];
      this.profileForm.controls['profile_picture'].setValue(file);
      const reader = new FileReader();
      reader.onload = (event:any) => {
        this.profileuploadImage = event.target.result;
      }
      reader.readAsDataURL(file);
  }

  removePicture(type){
    if(type =='profile_picture'){
      this.profileForm.controls['profile_picture'].setValue('');
      this.profileuploadImage ='';
    }else if(type=="company_logo"){
      this.profileForm.controls['company_logo'].setValue('');
      this.profileuploadImage ='';
    }
  }

  processFile(imageInput: any){
    if(this.galleries.previews.length > 3){
      this.message.error('Cannot Upload morethan 4 image!');
      return false;
    }
    const file = imageInput.files[0];
    const reader = new FileReader();
    let rand_id = Math.random();
    reader.onload = (event:any) => {
      this.galleries.previews.push({'url':event.target.result, 'type':'upload','id':rand_id});
    }
    reader.readAsDataURL(file);
    this.galleries.files.push({id:rand_id,'file':file});
  }

  removeFile(id, type){
    console.log(id);
    let index = this.galleries.previews.findIndex(preview=>preview.id ==id);
    this.galleries.previews.splice(index,1);
    if(type =='upload'){
      let file_index = this.galleries.files.findIndex(file=>file.id ==id);
      this.galleries.files.splice(file_index,1);
    }else{
      this.remove_image_id.push(id);
    }
  }

  editProfile(){
    //alert("test");
    //console.log("editFrom");
    /*this.profileDetails = false;
    this.editProfileDetails = true;*/
    this.profileDetails = this.profileDetails ? false : true;
  }
  showProfileDetails(){
    this.profileDetails = true;
    //this.editProfileDetails = false;
  }

  resetForm(){

  }



}
