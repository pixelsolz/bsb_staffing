import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentPostedJobComponent } from './recent-posted-job.component';

describe('RecentPostedJobComponent', () => {
  let component: RecentPostedJobComponent;
  let fixture: ComponentFixture<RecentPostedJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentPostedJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentPostedJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
