import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';
import {PagerService} from '../../services/pager.service';
declare var Swal: any;
declare var $;

@Component({
  selector: 'app-recent-posted-job',
  templateUrl: './recent-posted-job.component.html',
  styleUrls: ['./recent-posted-job.component.css']
})
export class RecentPostedJobComponent implements OnInit {
	showloader:boolean;
	recent_jobs:any[];
	recent_jobs_count:number;
  pager: any = {};
  pagedItems: any=[];
  active_page: number;
  count_data: number;

  constructor(private commonservice: CommonService,private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router,  private Pagerservice: PagerService) { 
  	this.showloader =false;
  	this.asyncInit();

  }

  ngOnInit() {
  	window.scrollTo(0, 0);
  }

  asyncInit(){
  	 this.commonservice.getAll('/api/employer-recent-job').subscribe((data)=>{     
      this.commonservice.callFooterMenu(1);    
          if(data.data.length){
            this.recent_jobs = data.data.map(obj=>(
              {_id: obj._id, job_title: obj.job_title, created_at: obj.created_at, 
                country:obj.country, applied_job_count: obj.applied_job_count, job_type: obj.job_type,
                select:(obj.job_status ==1? true: false), job_views:obj.job_views,
                url: (obj.job_type =='free'? '/employer/job-posted-edit':'/employer/job-posted-premium-edit')}))
                this.setPage(1);
          }
          this.recent_jobs_count = data.tot_jobs;
        },(error)=>{});
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    // get pager object from service
    this.pager = this.Pagerservice.getPager(this.recent_jobs.length, page);
    // get current page of items
    this.pagedItems = this.recent_jobs.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }


  deleteJob(id, index){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonservice.delete('/api/employer-job', id)
          .subscribe((data)=>{
          if(data.status ==200){   
             this.recent_jobs.splice(index,1);   
             this.setPage(1);       
            Swal.fire(
              'Deleted!',
              'Job has been deleted successfully.',
              'success'
            )
           }
          },(error)=>{});

      }
          
    });
  }

    setStatus(event, id){
    let status = event ? 1:0;
       this.commonservice.getAll('/api/employer-job/status-change/' + id + '?status=' + status)
           .subscribe(data=>{
             if(data.status ==200){
               this.message.success(data.status_text);
             }
           }, error=>{});
  }

}
