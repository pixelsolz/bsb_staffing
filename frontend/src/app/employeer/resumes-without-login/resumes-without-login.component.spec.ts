import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumesWithoutLoginComponent } from './resumes-without-login.component';

describe('ResumesWithoutLoginComponent', () => {
  let component: ResumesWithoutLoginComponent;
  let fixture: ComponentFixture<ResumesWithoutLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumesWithoutLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumesWithoutLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
