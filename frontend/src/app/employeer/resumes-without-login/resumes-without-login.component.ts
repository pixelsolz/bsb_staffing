import { Component, OnInit, Renderer2, Renderer} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router ,ActivatedRoute, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-resumes-without-login',
  templateUrl: './resumes-without-login.component.html',
  styleUrls: ['./resumes-without-login.component.css']
})
export class ResumesWithoutLoginComponent implements OnInit {
	showloader : boolean;
	cv_search : any;
	searchcvResult : any;
  total_cv_count : any;
  countries: any=[];
  cities: any=[];
  industries: any=[];
  job_roles: any=[];
  experiances: any=[];
  salaries: any=[];

  countryModel: any={};
  cityModel: any={};
  industryModel: any={};
  jobRoleModel: any={};
  searchForm: any={
    cat: '',
    cat_id:'',
    country_list:[],
    city_list:[],
    industry_list:[],
    role_list:[],
    experince:{
      min_exp:'',
      max_exp:''
    },
    salary:{
      currency:'',
      min_salary:'',
      max_salary:''
    }
  };
  search_title : string;
  search_industry : string;
  search_city : string;
  
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute) { 
  	this.showloader = false;

    this.search_title = this.activeRoute.snapshot.queryParamMap.get("search_title") ? this.activeRoute.snapshot.queryParamMap.get("search_title"):'';
    this.search_industry = this.activeRoute.snapshot.queryParamMap.get("search_industry") ? this.activeRoute.snapshot.queryParamMap.get("search_industry"):'';
    this.search_city = this.activeRoute.snapshot.queryParamMap.get("search_city") ? this.activeRoute.snapshot.queryParamMap.get("search_city"):'';
    console.log(this.search_industry);
    this.getCvBySearch();
  }

  ngOnInit() {

  }

  getCvBySearch(){
    this.showloader = true;
    this.commonService.getAll('/api/employer/get-cv-by-search?search_title='+this.search_title+'&search_industry='+this.search_industry+'&search_location='+this.search_city)
      .subscribe((data)=>{
      this.commonService.callFooterMenu(1);
      this.countries = data.countries;
      this.cities = data.cities;
      this.industries = data.industries;
      this.job_roles = data.roles; 
      this.searchcvResult = data.cvResults;
      this.total_cv_count = data.total_cv_count;
      let otherData = data.other_mst;
      this.experiances =  otherData.filter((data)=>data.entity =='experience');
      this.salaries = otherData.filter((data)=>data.entity =='salary');
      this.showloader = false;
      console.log(this.searchcvResult);
      },(error)=>{});
  }

  setsearchValue(event, type){
    if(type=='country'){
      let count_list = Object.entries(this.countryModel);
      let countArr =[];
      count_list.forEach((value, i)=>{if(value[1] ==true){countArr.push(value[0]);}});
      console.log(countArr);
      this.searchForm.country_list = countArr;
    }else if(type=='city'){
      let city_list = Object.entries(this.cityModel);
      let cityArr =[];
      city_list.forEach((value, i)=>{if(value[1] ==true){cityArr.push(value[0]);}});
      this.searchForm.city_list = cityArr;
    }else if(type=='industry'){
      let indus_list = Object.entries(this.industryModel);
      let indusArr =[];
      indus_list.forEach((value, i)=>{if(value[1] ==true){indusArr.push(value[0]);}});
      this.searchForm.industry_list = indusArr;
    }else if(type=='role'){
      let role_list = Object.entries(this.jobRoleModel);
      let roleArr =[];
      role_list.forEach((value, i)=>{if(value[1] ==true){roleArr.push(value[0]);}});
      this.searchForm.role_list = roleArr;
    }
    this.getsearchData();
  }

  getsearchData(){
    this.commonService.getDataWithPost('/api/employer/cv-search/get-cvs', this.searchForm)
      .subscribe((data)=>{
        if(data.status ==200){
           this.searchcvResult = data.cvResults;
           this.total_cv_count = data.total_cv_count;
        }
      }, (error)=>{});
  }

}
