import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import {PagerService} from '../../services/pager.service';
declare var Swal: any;
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-search-history',
  templateUrl: './search-history.component.html',
  styleUrls: ['./search-history.component.css']
})
export class SearchHistoryComponent implements OnInit {

  searchData: any=[];
  savedData: any=[];
  routeParam: string;
  checkedModel: any={};
  checkedModelSearch: any={};
  isAllchecked: boolean=false;
  isAllcheckedSearch: boolean=false;
  checkedData: any=[];
  checkedDataSearch: any=[];
  pager: any = {};
  pagedItems: any[];
  pager1: any = {};
  pagedItems1: any[];
  search_key:any={
    from_date:'',
    to_date:'',
    type:''
  }
  constructor(private commonService: CommonService, private route: Router, private activeRoute: ActivatedRoute,
  		private messageService: MessageService, private Pagerservice: PagerService) {

  	  route.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
        this.routeParam = this.activeRoute.snapshot.queryParams['search_type'];
        this.asyncInit();
      });
   }

  ngOnInit() {

  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    // get pager object from service
    this.pager = this.Pagerservice.getPager(this.searchData.length, page);
    // get current page of items
    this.pagedItems = this.searchData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setPage1(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    // get pager object from service
    this.pager1 = this.Pagerservice.getPager(this.savedData.length, page);
    // get current page of items
    this.pagedItems1 = this.savedData.slice(this.pager1.startIndex, this.pager1.endIndex + 1);
  }

  asyncInit(){
  	let url = this.routeParam =='saved'? '/api/search-history?search_type=saved':'/api/search-history';
  	this.commonService.getAll(url)
  		.subscribe(data=>{
        this.commonService.callFooterMenu(1);
  			if(this.routeParam =='saved'){
  				this.savedData = data.data;
  				this.savedData.forEach((obj,i)=>{
  					this.checkedModel[obj._id] = false;
  				});
          this.setPage1(1);
  			}else{
  				this.searchData = data.data;
          this.setPage(1);
  			}
  			
  		}, error=>{});	
  }

  filterSearch(){
    let queryString ='';
    if(this.routeParam =='saved'){
      Object.entries(this.search_key).filter(obj=> !_.isEmpty(obj[1])).forEach((data, index) => {
        queryString += `&${data[0]}=${data[1]}`
    })
    }else{
      Object.entries(this.search_key).filter(obj=> !_.isEmpty(obj[1])).forEach((data, index) => {
      if (index === 0) {
        queryString += `?${data[0]}=${data[1]}`
      } else {
        queryString += `&${data[0]}=${data[1]}`
      }

      })
    }

    if(!_.isEmpty(this.search_key.from_date) && _.isEmpty(this.search_key.to_date)){
      this.messageService.error('Please select to date');
      return
    }else if(!_.isEmpty(this.search_key.to_date) && _.isEmpty(this.search_key.from_date)){
      this.messageService.error('Please select from date');
      return
    }
    
    let url = this.routeParam =='saved'? `/api/search-history?search_type=saved${queryString}`:`/api/search-history${queryString}`;
    this.commonService.getAll(url)
      .subscribe(data=>{
        this.commonService.callFooterMenu(1);
        if(this.routeParam =='saved'){
          this.savedData = data.data;
          this.savedData.forEach((obj,i)=>{
            this.checkedModel[obj._id] = false;
          });
          this.setPage1(1);
        }else{
          this.searchData = data.data;
          this.setPage(1);
        }
        
      }, error=>{});  
  }

  clearSearch(){
    this.search_key.from_date=''
    this.search_key.to_date=''
    this.search_key.type=''
    
    let url = this.routeParam =='saved'? `/api/search-history?search_type=saved`:`/api/search-history`;
    this.commonService.getAll(url)
      .subscribe(data=>{
        this.commonService.callFooterMenu(1);
        if(this.routeParam =='saved'){
          this.savedData = data.data;
          this.savedData.forEach((obj,i)=>{
            this.checkedModel[obj._id] = false;
          });
          this.setPage1(1);
        }else{
          this.searchData = data.data;
          this.setPage(1);
        }
        
      }, error=>{});     
  }

  saveSearch(id,search_key){
    console.log(search_key);
  	Swal.fire({
	  title: 'Enter save text',
	  input: 'text',
    inputValue: search_key.toString(),
	  inputAttributes: {
	    autocapitalize: 'off'
	  },
    inputValidator: (value) => {
    return !value && 'Please enter value!'
    },
	  showCancelButton: true,
	  confirmButtonText: 'Save',
	  showLoaderOnConfirm: true,
	  preConfirm: (savetext) => {
	  	return this.commonService.update('/api/search-history/saved',id, {save_text:savetext})
		  		.subscribe(data=>{
		  			if(data.status ==200){
		  				this.messageService.success(data.status_text);
		  				let index = this.searchData.findIndex(obj=>obj._id == data.data._id);
		  				this.searchData.splice(index, 1);
		  			}
            this.setPage(this.pager.currentPage); 
		  			console.log(data);
		  		},error=>{});

	  },
	  allowOutsideClick: () => !Swal.isLoading()
	});

  }

  deleteSaved(){
  	let deleteIds = this.checkedData.map(obj=>obj._id);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
      this.commonService.create('/api/search-history/saved-delete',{ids: deleteIds})
        .subscribe(data=>{
          if(data.status ==200){
            this.checkedData.forEach((obj_data, i)=>{
              let index = this.savedData.findIndex(obj=>obj._id ==obj_data._id);
              this.savedData.splice(index, 1);
            });
            this.checkedData.length =0;
            this.isAllchecked = false;
            this.messageService.success(data.status_text);
            this.setPage1(this.pager1.currentPage);
          }
        }, error=>{});

      }
          
    });


  }

  deleteSearch(){
    let deleteIds = this.checkedDataSearch.map(obj=>obj._id);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
      this.commonService.create('/api/search-history/saved-delete',{ids: deleteIds})
        .subscribe(data=>{
          if(data.status ==200){
            console.log(this.pager, '5666')
            this.checkedDataSearch.forEach((obj_data, i)=>{
              let index = this.searchData.findIndex(obj=>obj._id ==obj_data._id);
              this.searchData.splice(index, 1);
            });
            this.checkedDataSearch.length =0;
            this.isAllcheckedSearch = false;
            this.setPage(this.pager.currentPage);           
            this.messageService.success(data.status_text);
          }
        }, error=>{});

      }
          
    });

  }

  setChecked(value){
  	this.checkedData =this.savedData.filter(obj=>this.checkedModel[obj._id] ==true);
  	this.isAllchecked = (this.checkedData.length ==this.savedData.length)? true:false; 
  }

  setCheckedSearch(value){
    this.checkedDataSearch =this.searchData.filter(obj=>this.checkedModelSearch[obj._id] ==true);
    this.isAllcheckedSearch = (this.checkedDataSearch.length ==this.searchData.length)? true:false; 
  }

  toggleChecked(value){
  	if(value){
  		this.savedData.forEach((obj,i)=>{this.checkedModel[obj._id]= true});
  	}else{
  		this.savedData.forEach((obj,i)=>{this.checkedModel[obj._id]= false});
  	}
  	this.checkedData =this.savedData.filter(obj=>this.checkedModel[obj._id] ==true);
  	this.isAllchecked = (this.checkedData.length ==this.savedData.length)? true:false; 
  }

   toggleCheckedSearch(value){
    if(value){
      this.searchData.forEach((obj,i)=>{this.checkedModelSearch[obj._id]= true});
    }else{
      this.searchData.forEach((obj,i)=>{this.checkedModelSearch[obj._id]= false});
    }
    this.checkedDataSearch =this.searchData.filter(obj=>this.checkedModelSearch[obj._id] ==true);
    this.isAllcheckedSearch = (this.checkedDataSearch.length ==this.searchData.length)? true:false; 
  }

  searchShow(data){
  	localStorage.setItem('cvSearch',JSON.stringify(data));
  }

}
