import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMailPreviewComponent } from './send-mail-preview.component';

describe('SendMailPreviewComponent', () => {
  let component: SendMailPreviewComponent;
  let fixture: ComponentFixture<SendMailPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMailPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMailPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
