import { Component, OnInit, Renderer2, Renderer} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router ,ActivatedRoute, NavigationEnd} from '@angular/router';
declare var Swal: any;
declare var $;
let expdata =[];
@Component({
  selector: 'app-send-mail-using-template',
  templateUrl: './send-mail-using-template.component.html',
  styleUrls: ['./send-mail-using-template.component.css']
})
export class SendMailUsingTemplateComponent implements OnInit {
	employee_ids:any;
	emailForm:FormGroup;
  previousEmail:any={};
  previousTemplates:any=[];
  //resourceUrl: string='/api/employer/email-template';
  language_pref_div:boolean;
  countries: any=[];
  cities: any=[];
  experianceAray :any=[];
  langPrefArray: any=[];
  currencyArray: any=[];
  salaryArray: any=[];
  template_id:any;
  isSubmit: boolean;
  langPrefStore: any={
    value:[],
    name:[]
  };
  other_mst_data:any;
  all_template:any;
  selectedtemplate: any;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  skillData:any= [];
  skill_key:any;
  coutryWiseCity: any=[];
  template_name_field : boolean;
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About This Job',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Benefits',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  responsconfig: any = {
    //height: '200px',
    placeholder: 'Primary Responsibility',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  noteconfig: any = {
    //height: '200px',
    placeholder: 'Special Note',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
	constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private activeRoute: ActivatedRoute,private message:MessageService) {
		this.asyncInit();
		this.language_pref_div = false;
    this.isSubmit = false;
		this.employee_ids = localStorage.getItem('mailUids')? JSON.parse(localStorage.getItem('mailUids')):'';
		console.log(this.employee_ids);
		
	}

  ngOnInit() {
  	this.emailForm = this.fb.group({
  		id:'',
  		template_name:['', []],
  		//cv_received_email:['', [Validators.required, Validators.pattern(this.emailPattern)]],
  		subject:['', [Validators.required]],
  		about_job:['', [Validators.required]],
  		skill:['', [Validators.required]],
  		benefit:['', [Validators.required]],
  		primary_responsibility:['', [Validators.required]],
  		special_note:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
      job_title:['', [Validators.required]],
  		min_exp:['', [Validators.required]],
  		max_exp:['', [Validators.required, this.checkMinMaxExp]],
  		language_pref:['', [Validators.required]],
  		salary_type:['', [Validators.required]],
  		currency_type:['', [Validators.required]],
  		min_salary:['', [Validators.required]],
  		max_salary:['', [Validators.required, this.checkMinMaxSal]],
  		previous_id:['', []],
      save_email_template:['', []],

  	});
    this.emailForm.controls.salary_type.setValue('monthly');
    this.emailForm.controls.save_email_template.setValue(true);
    this.emailForm.controls["template_name"].setValidators([Validators.required]);
    this.template_name_field = true;
    this.emailForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
      this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      this.emailForm.controls['currency_type'].setValue(value);
      }
    });
    this.emailForm.get('save_email_template').valueChanges.subscribe(value=>{
         if(value == true){
           this.template_name_field = true;
           this.emailForm.controls["template_name"].setValidators([Validators.required]);

         }
         if(value == false){
           this.emailForm.controls["template_name"].clearValidators();
           this.emailForm.get('template_name').updateValueAndValidity();
           this.template_name_field = false;
         }
    });
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });


  this.commonService.getCommonData.subscribe(data=>{
    this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
        this.langPrefArray = data['alllanguage'];
        this.experianceAray = data['allExprience'];
        this.salaryArray = data['other_mst'].filter((salary)=>salary.entity ==='salary');
        this.currencyArray =  data['all_currency'];   
        this.skillData = data['skills'].map((obj) => { return { display: obj.skill, value: obj.skill } });
        expdata = this.experianceAray;
      }
    });


      this.commonService.getAll('/api/employer/all-template')
	      .subscribe((data)=>{
	        if(data.status == 200){
	          this.all_template = data.data;
	          console.log(this.all_template);
	        }
	        
	      }, (error)=>{});
        
  }

  showDropdown(){
    this.language_pref_div = true;
  }

  dropDownToggle(e){
    console.log(e);
    this.language_pref_div = e.status;
  }

  getDropDownData(e){
    if(e.event){
      let index = this.langPrefStore.value.indexOf(e.value);
      if(index  ==-1){
        this.langPrefStore.value.push(e.value);
        this.langPrefStore.name.push(e.name);
      }
    }else{
      let index = this.langPrefStore.value.indexOf(e.value);
      if(index !=-1){
        this.langPrefStore.value.splice(index,1);
        this.langPrefStore.name.splice(index,1);
      }
    }
    this.emailForm.patchValue({
      language_pref: this.langPrefStore.name
    });
  }


  templateOnChange(template_id){
    if(template_id){
      let data = this.all_template.find((temp)=>temp._id ==template_id);
      this.selectedtemplate = data;
      this.template_name_field = false;
      this.emailForm.controls.save_email_template.setValue(false);
      this.emailForm.controls["template_name"].clearValidators();
      this.emailForm.get('template_name').updateValueAndValidity();
      this.setFormValue(data);
    }else{
      this.resetForm();
    }
    
    //console.log(data);
  }

  resetForm(){
    this.emailForm.reset();
    this.langPrefStore.value.length =0;
    this.langPrefStore.name.length =0;
    this.skill_key ='';
    this.emailForm.patchValue({
      country_id:'',
      city_id:'',
      min_exp:'',
      max_exp:'',
      currency_type:''
    });
    this.emailForm.controls.salary_type.setValue('monthly');
    this.template_name_field = true;
      this.emailForm.controls.save_email_template.setValue(true);
  }

  setFormValue(data){
    //console.log(data);
  	this.langPrefStore.value = data.employer_job.language_id;
    this.langPrefStore.name =  this.langPrefArray.filter(obj=> data.employer_job.language_id.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
    this.skill_key = data.employer_job.required_skill.map((obj) => { return { value: obj, display: obj } });
  	this.emailForm.patchValue({
  		job_title : data.employer_job.job_title,
  		//cv_received_email:data.employer_job.received_email,
  		subject:data.subject,
  		about_job:data.about_job,
  		skill:data.employer_job.required_skill,
  		benefit:data.employer_job.benefits,
  		primary_responsibility:data.employer_job.primary_responsibility,
  		special_note:data.employer_job.special_notes,
  		country_id:data.employer_job.country_id,
  		city_id:data.employer_job.city_id,
  		min_exp:data.employer_job.min_experiance,
  		max_exp:data.employer_job.max_experiance,
  		language_pref:this.langPrefStore.name,
  		salary_type:data.employer_job.salary_type,
  		currency_type:data.employer_job.currency,
  		min_salary:data.employer_job.min_monthly_salary,
  		max_salary:data.employer_job.max_monthly_salary,
  	});
  }

  templatePreview(){
    
  }

  sendMail(){
    this.emailForm.patchValue({
      language_pref:this.langPrefStore.value
    });
    let skills = this.skill_key&&this.skill_key.map(obj => obj.value);
    skills ? this.emailForm.get('skill').setValue(skills) : '';
    
    this.isSubmit = true;
    if(this.emailForm.invalid){
      return ;
    }

    //alert();
     if(this.selectedtemplate){
       let formData = {employee_id: JSON.parse(localStorage.getItem('mailUids')), job_id: this.selectedtemplate.employer_job._id};
      this.commonService.create('/api/employer/template-send',formData)
           .subscribe(data=>{
             if(data.status ==200){
               this.message.success(data.status_text);
               localStorage.removeItem('mailUids');
               this.router.navigate(['/employer/dashboard']);
             }
           },error=>{});
     }else{
       this.message.error('Please select template');
     }
  }

  checkMinMaxSal(c: FormControl) {
    if (!c.root && !c.root.get('min_salary')) {
      return null;
    }
    if (c.value && c.value < c.root.get('min_salary').value) {
      return { error_min_max: true };
    }

  }

  checkMinMaxExp(c: FormControl) {
    if (!c.root && !c.root.get('min_exp')) {
      return null;
    }
    if(c.value){
      let minExpValueOrder = expdata.find(obj=>obj._id == c.root.get('min_exp').value);
      let maxExpValueOrder = expdata.find(obj=>obj._id == c.value);
      if(maxExpValueOrder && maxExpValueOrder.order < minExpValueOrder.order){
        return { error_min_max_exp: true };
      }
    }
  }

  

}
