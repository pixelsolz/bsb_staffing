import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceMoreDetailsComponent } from './service-more-details.component';

describe('ServiceMoreDetailsComponent', () => {
  let component: ServiceMoreDetailsComponent;
  let fixture: ComponentFixture<ServiceMoreDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceMoreDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceMoreDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
