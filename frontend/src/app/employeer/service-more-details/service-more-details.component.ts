import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var $;
declare var swal: any;
@Component({
  selector: 'app-service-more-details',
  templateUrl: './service-more-details.component.html',
  styleUrls: ['./service-more-details.component.css']
})
export class ServiceMoreDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
  	let id :any;
  	this.route.fragment.subscribe((fragment: string) => {
  		id=fragment;
        //console.log("My hash fragment is here => ", fragment)
    })
    //console.log(id);
    const header_heigth = $('header').innerHeight();
      $('html, body').animate({
        scrollTop: $('#' + id).offset().top - header_heigth
      }, 1000);
  }

}
