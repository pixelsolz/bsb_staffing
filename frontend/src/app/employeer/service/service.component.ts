import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  service_type: string;	
  packages:any;
  page_title:string;
  constructor(private fb:FormBuilder,private renderer2: Renderer2, private renderer: Renderer,private router: Router, private commonService: CommonService,
    private global: Global,private message:MessageService,private activeRoute:ActivatedRoute) { 

  	this.service_type = this.activeRoute.snapshot.queryParamMap.get("type") ? this.activeRoute.snapshot.queryParamMap.get("type"):'';
  	//console.log(this.service_type);
  }

  ngOnInit() {
  	window.scrollTo(0, 0);
    this.commonService.getAll('/api/employer/package-list')
        .subscribe((data)=>{
          this.commonService.callFooterMenu(1);
            if(this.service_type == 'cv_download_view'){
            	this.packages = data.packages.filter((obj)=>obj.service_type == 'cv_download_view');
            	this.page_title = 'CV Download / View';
            }else if(this.service_type == 'premium_job_post'){
            	this.packages = data.packages.filter((obj)=>obj.service_type == 'premium_job_post');
            	this.page_title = 'Premium Job Post';
            }else if(this.service_type == 'mass_mail_service'){
            	this.packages = data.packages.filter((obj)=>obj.service_type == 'mass_mail_service');
            	this.page_title = 'Mass Mail Services';
            }else if(this.service_type == 'international_walk_in_interview'){
            	this.packages = data.packages.filter((obj)=>obj.service_type == 'international_walk_in_interview');
            	this.page_title = 'International Walk-In';
            }else{
            	this.packages = '';
            	this.page_title = '';
            }
            
        }, (error)=>{});
  }

}
