import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { filter, map, catchError } from 'rxjs/operators';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-simple-search',
  templateUrl: './simple-search.component.html',
  styleUrls: ['./simple-search.component.css']
})
export class SimpleSearchComponent implements OnInit {
	simpleSearchForm : FormGroup;
	showloader:boolean;
	underGraduateDiv: boolean;
	postGraduateDiv: boolean;
	industriesDiv: boolean;
	laguageDiv:boolean;
	countries: any;
	cities:any;
	industries:any;
	job_roles: any;
	emplymentFor: any;
	experiance: any;
	empType: any;
  skillsData: any=[];
  addOnBlur: boolean=true;
  allIndustries: any=[];
  skill_key: any;
	storeMultiSelect :any ={
	    undergradute_value:[],
	    undergraduate_name:[],
	    postgraduate_value:[],
	    postgraduate_name:[],
	    industries_value:[],
	    industries_name:[],
      country_value:[],
      country_name:[],
      city_value:[],
      city_name:[],
  	};
    countryArray: any=[];
    countryDiv:boolean=false;
    cityArray: any=[];
    cityDiv: boolean=false;
    queryParam: string;
    searchSavedData: any={};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, private activeRoute:ActivatedRoute,private http: HttpClient) { 
  	this.showloader =false;
	  this.underGraduateDiv = false;
	  this.postGraduateDiv = false;
	  this.laguageDiv = false;
	  this.industriesDiv = false;

  }

  ngOnInit() {
  	this.simpleSearchForm=this.fb.group({
  		key_skills :[''],
  		industry_id :[''],
  		country_id :[''],
  		city_id :[''],
  		min_experiance :[''],
  		max_experiance :[''],
      premium :[''],
      women_only :[''],
  	})
  	this.asyncInit();
    this.simpleSearchForm.controls['country_id'].valueChanges.subscribe(value=>{
      let countryCode = this.countries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
      this.storeMultiSelect.city_value = this.cityArray[""].filter(obj=>this.storeMultiSelect.city_value.indexOf(obj._id) != -1).map(obj=>obj._id);
      this.storeMultiSelect.city_name = this.cityArray[""].filter(obj=>this.storeMultiSelect.city_name.indexOf(obj.name) != -1).map(obj=>obj.name);
      this.simpleSearchForm.patchValue({city_id: this.storeMultiSelect.city_name});
    });

  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.countryArray = {"":data['countries']};
        this.industries = data['industries'];
        this.allIndustries = data['all_industries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
          this.cities = data['cities'];
          this.cityArray = [];
        }
    });


     this.commonService.getCommonData.subscribe(data=>{
       this.commonService.callFooterMenu(1);
          if(data && data.status ==200){
            this.commonService.callFooterMenu(1);
            let otherData = data['other_mst'];
            this.emplymentFor = otherData.filter((data)=>data.entity =='emplyment_for');
            //this.experiance =  otherData.filter((data)=>data.entity =='experiance_year');
            this.experiance  =data['allExprience'];
            this.empType = otherData.filter((data)=>data.entity =='employement_type');
            this.skillsData = data['skills'].map((obj)=> {return {display: obj.skill, value: obj.skill}});
            this.getParamData();
          }
        });
        
  }

  public requestAutocompleteSkill = (text: string): Observable<Response> => {
   const url = `/api/skill-and-job-title/filter?q=${text}`;
   return this.http.get<any>(url).pipe(map(data => _.uniqBy(data.data,(e)=>{
              return e.value
            })));
  };

    getParamData(){
        if(this.activeRoute.snapshot.queryParams['modify']){
        this.queryParam = this.activeRoute.snapshot.queryParams['modify'];
        this.searchSavedData = localStorage.getItem('cvSearch') ? JSON.parse(localStorage.getItem('cvSearch')):'';
        this.setFormValue();
      }
  }

  setFormValue(){
    let countryNames = this.searchSavedData.country_id.length ?this.countries.filter(obj=>this.searchSavedData.country_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let citiesName = this.searchSavedData.city_id.length ? this.cities.filter(obj=>this.searchSavedData.city_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    let indusrtyName = this.searchSavedData.industry_id.length ? this.allIndustries.filter(obj=>this.searchSavedData.industry_id.indexOf(obj._id) != -1).map(obj=>obj.name):[];
    this.storeMultiSelect.country_value = !_.isEmpty(this.searchSavedData.country_id)?this.searchSavedData.country_id:[];
    this.storeMultiSelect.country_name = !_.isEmpty(countryNames)?countryNames:[];
    this.storeMultiSelect.city_value = !_.isEmpty(this.searchSavedData.city_id)?this.searchSavedData.city_id:[];
    this.storeMultiSelect.city_name = !_.isEmpty(citiesName)?citiesName:[];
    this.storeMultiSelect.industries_value = !_.isEmpty(this.searchSavedData.industry_id)?this.searchSavedData.industry_id:[],
    this.storeMultiSelect.industries_name = !_.isEmpty(indusrtyName)?indusrtyName:[],
    this.skill_key = this.searchSavedData.key_skills.map((obj)=> {return {display: obj, value: obj}});
    let countryCode = this.countries.filter(obj=> this.storeMultiSelect.country_value.indexOf(obj._id) != -1).map(obj=>obj.code);
    if(countryCode){
      this.cityArray = {"":this.cities.filter(obj=> countryCode.indexOf(obj.country_code) !=-1)};
    }
    

    this.simpleSearchForm.patchValue({
      key_skills :this.searchSavedData.key_skills,
      industry_id :this.storeMultiSelect.industries_value,
      country_id :this.storeMultiSelect.country_value,
      city_id :this.storeMultiSelect.city_value,
      min_experiance :this.searchSavedData.min_experiance,
      max_experiance :this.searchSavedData.max_experiance,
      premium :this.searchSavedData.premium,
      women_only :this.searchSavedData.women_only,
    });
  }

  searchCv(){
    if(!this.skill_key){
      this.message.error('Please put keyword');
      return false;
    }
    this.changeSubmitVal();
    let skillVal = this.skill_key.map(obj=>obj.value.toLowerCase());
    console.log(skillVal);
    this.simpleSearchForm.get('key_skills').setValue(skillVal);
    this.commonService.create('/api/search-history', {search_key:this.simpleSearchForm.get('key_skills').value,
      search_data:this.simpleSearchForm.value,search_type:'simple','is_modified':(this.queryParam?1:0)})
        .subscribe((data)=>{

        }, (error)=>{});
    localStorage.setItem('cvSearch',JSON.stringify(this.simpleSearchForm.value));
    this.router.navigate(['/employer/resumes'],{queryParams:{src:'cvSearch', prevsearch:'simple-search'}});
  }
  getOnchangeEvent(type){
    if(type =='undergraduate'){
      this.underGraduateDiv = true;
    }else if(type =='postgraduate'){
      this.postGraduateDiv = true;
    }else if(type =='industries'){
      this.industriesDiv = true;
    }else if(type =='country'){
      this.countryDiv = true;
    }else if(type =='city'){
      this.cityDiv = true;
    }
    
  }

  getChange(e){
    console.log(e);
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
      }
    }
    if(e.field_name =='min_qualification'){
      this.simpleSearchForm.patchValue({
      min_qualification: e.storedName
      });
    }else if(e.field_name =='max_qualification'){
      this.simpleSearchForm.patchValue({
      max_qualification: e.storedName
      });
    }else if(e.field_name =='industries'){
      this.simpleSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='country'){
      this.simpleSearchForm.patchValue({
      country_id: e.storedValue
      });
    }else if(e.field_name =='city'){
      this.simpleSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
  }


  multiselectAll(e){  
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='min_qualification'){
      this.simpleSearchForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(filed =='max_qualification'){
      this.simpleSearchForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(filed =='industries'){
      this.simpleSearchForm.patchValue({
      industry_id: e.storedValue
      });
    }
    else if(filed =='country'){
      this.simpleSearchForm.patchValue({
      country_id: e.storedValue
      });
    }
    else if(filed =='city'){
      this.simpleSearchForm.patchValue({
      city_id: e.storedValue
      });
    }
    
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.simpleSearchForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "country":
       this.storeMultiSelect.country_value.splice(indx, 1);
       this.storeMultiSelect.country_name.splice(indx, 1);
       this.simpleSearchForm.patchValue({
        country_id: this.storeMultiSelect.country_value
       });
        break;  
      case "city":
       this.storeMultiSelect.city_value.splice(indx, 1);
       this.storeMultiSelect.city_name.splice(indx, 1);
       this.simpleSearchForm.patchValue({
          city_id: this.storeMultiSelect.city_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  checkmultiselect(e){
    console.log(e);
    if(e.field_name =='min_qualification'){
      this.underGraduateDiv = e.status;
    }else if(e.field_name =='max_qualification'){
      this.postGraduateDiv = e.status;
    }else if(e.field_name =='industries'){
      this.industriesDiv = e.status;
    }else if(e.field_name =='country'){
      this.countryDiv = e.status;
    }else if(e.field_name =='city'){
      this.cityDiv = e.status;
    }
  }

  changeSubmitVal(){
    this.simpleSearchForm.patchValue({
      industry_id:this.storeMultiSelect.industries_value,
      min_qualification:this.storeMultiSelect.undergradute_value.toString(),
      max_qualification:this.storeMultiSelect.postgraduate_value.toString(),
      country_id:this.storeMultiSelect.country_value,
      city_id: this.storeMultiSelect.city_value
      //language_pref:this.noSearchSelect.lang_value.toString()
    });
  }

}
