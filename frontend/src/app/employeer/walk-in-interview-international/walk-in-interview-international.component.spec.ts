import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalkInInterviewInternationalComponent } from './walk-in-interview-international.component';

describe('WalkInInterviewInternationalComponent', () => {
  let component: WalkInInterviewInternationalComponent;
  let fixture: ComponentFixture<WalkInInterviewInternationalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalkInInterviewInternationalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalkInInterviewInternationalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
