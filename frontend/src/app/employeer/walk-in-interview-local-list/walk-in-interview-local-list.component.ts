import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
declare var Swal: any;
import {PagerService} from '../../services/pager.service';

@Component({
  selector: 'app-walk-in-interview-local-list',
  templateUrl: './walk-in-interview-local-list.component.html',
  styleUrls: ['./walk-in-interview-local-list.component.css']
})
export class WalkInInterviewLocalListComponent implements OnInit {
  walkin_lists: any =[];
  active_page: number;
  count_data: number;
  pager: any = {};
  pagedItems: any[];
  constructor(private global: Global,private route: ActivatedRoute,private commonService: CommonService,
              private authService: AuthService, private router: Router, private messageService: MessageService,
              private Pagerservice: PagerService) { 

    localStorage.removeItem('walk_in_preview');
  }

  ngOnInit() {
  	this.getData();
  }

  getData(){
  	this.commonService.getAll('/api/walkin-interview?walkin_location_type='+ 1)
      .subscribe((data)=>{
        this.commonService.callFooterMenu(1);
        if(data.status == 200){
          this.walkin_lists = data.data;
          console.log(this.walkin_lists);
          this.count_data = data.total_walkin;
          this.walkin_lists.forEach((walkin, i)=>{
            walkin.select = (walkin.status ==1? true: false);
          });
          this.setPage(1);
        }
        
      }, (error)=>{});
  }

  removeWalkin(id, index){
  	Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.commonService.delete('/api/walkin-interview', id)
          .subscribe((data)=>{
          if(data.status ==200){   
             this.walkin_lists.splice(index,1); 
             this.setPage(1);         
            Swal.fire(
              'Deleted!',
              'Walkin has been deleted successfully.',
              'success'
            )
           }
          },(error)=>{});

      }
          
    }); 
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    // get pager object from service
    this.pager = this.Pagerservice.getPager(this.walkin_lists.length, page);
    // get current page of items
    this.pagedItems = this.walkin_lists.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }


    setStatus(event, id){
    let status = event ? 1:0;
       this.commonService.getAll('/api/walkin-interview/status-change/' + id + '?status=' + status)
           .subscribe(data=>{
             if(data.status ==200){
               this.messageService.success(data.status_text);
             }
           }, error=>{});
  }

}
