import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalkInInterviewLocalComponent } from './walk-in-interview-local.component';

describe('WalkInInterviewLocalComponent', () => {
  let component: WalkInInterviewLocalComponent;
  let fixture: ComponentFixture<WalkInInterviewLocalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalkInInterviewLocalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalkInInterviewLocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
