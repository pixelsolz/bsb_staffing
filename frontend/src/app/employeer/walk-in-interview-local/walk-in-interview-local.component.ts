import { Component, OnInit,Renderer2, ElementRef, Renderer, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import {CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router,ActivatedRoute } from '@angular/router';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
declare var $ :any;
import * as _ from 'lodash'; 
  let expdata =[];

@Component({
  selector: 'app-walk-in-interview-local',
  templateUrl: './walk-in-interview-local.component.html',
  styleUrls: ['./walk-in-interview-local.component.css']
})
export class WalkInInterviewLocalComponent implements OnInit {
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(event.target.id && event.target.id =='jobTitle'){
    }else{
      this.show_jb_title_srh = false;
    }
  }
  @HostListener("document:mousemove", ["$event"])
  onMouseMove(e) {
    $('.mouseTrail').css({'top': this.setTrail(e) , 'left':e.pageX+10, 'width':'auto', 'height':'auto'});
  }
	walkInInterviewForm:FormGroup;
  conditionQuestionForm:FormGroup;
  questions:FormArray;
	showloader:boolean;
	countries: any;
	cities:any;
  user_cities:any;
	industries:any;
	//job_roles: any;
	emplymentFor: any;
	experiance: any;
	empType: any;
	noOfVacancies: any;
	salaries: any;
  interview_types: any;
	all_posted_walkin: any;
	industryList = [];
	selectedItems = [];
  departments: any=[];
	dropdownSettings = {};
	underGraduateDiv: boolean;
	postGraduateDiv: boolean;
	industriesDiv: boolean;
	laguageDiv:boolean;
  departmentDiv:boolean;
	storeMultiSelect :any ={
		undergradute_value:[],
		undergraduate_name:[],
    undergraduate_sub_value: [],
		postgraduate_value:[],
		postgraduate_name:[],
    postgraduate_sub_value: [],
		industries_value:[],
		industries_name:[],
    department_name:[],
    department_value:[]
	};
  show_sub: any=[];
  show_sub_under: any=[];
  noSearchSelect: any={
    lang_value:[],
    lang_name:[]
  };
	post_graduates: any=[];
	under_graduates: any=[];
  post_graduate_deg: any=[];
  under_graduate_deg: any=[];
  all_departments: any=[];
  languages: any=[];
  isSubmit: boolean;
  walkin_id:any='';
  walkin_data:any={};
  all_currency:any;
  loggedInEmployer: any={};
  job_titles: any =[];
  coutryWiseCity: any =[];
  show_jb_title_srh: boolean =false;

  relevant_conditions: any={
    expeiance:false,
    industry: false,
    age: false,
    location: false,
    gender: false,
    salary: false,
    education: false
  };
  alljob_conditions: any={
    all:true
  }


  aboutconfig: any = {
    //height: '200px',
    placeholder: 'About This Job',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  benefitsconfig: any = {
    //height: '200px',
    placeholder: 'Benefits',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  responsconfig: any = {
    //height: '200px',
    placeholder: 'Primary Responsibility',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  noteconfig: any = {
    //height: '200px',
    placeholder: 'Special Note',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  questionconfig: any = {
    //height: '200px',
    placeholder: 'Question',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],
  };

  constructor(private renderer2: Renderer2, private renderer: Renderer,private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router,private activeRoute: ActivatedRoute, private commonService: CommonService,  private eRef: ElementRef) { 
		this.showloader =false;
		this.underGraduateDiv = false;
		this.postGraduateDiv = false;
		this.laguageDiv = false;
		this.industriesDiv = false;
    this.isSubmit = false;
    this.departmentDiv = false;
    this.walkin_id = this.activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id"):'';
  	this.loggedInEmployer = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
    }

  ngOnInit() {
    this.conditionQuestionForm = this.fb.group({
       questions: this.fb.array([this.initReferal()])
    });

  	this.walkInInterviewForm= this.fb.group({
        id:this.walkin_id,
        job_title :['',[Validators.required,Validators.maxLength(50)]],
        country_id:[''],
        city_id :['',[Validators.required]],
        industry_id :['',[Validators.required]],
        department_id:[''],
        employement_for :['',[Validators.required]],
        about_this_job :['',[Validators.required]],
        position_offered :[''],
        number_of_vacancies :['',[Validators.required]],
        employment_type :['',[Validators.required]],
        min_experiance :['',[Validators.required]],
        max_experiance :['',[Validators.required, this.checkMinMaxExp]],
        min_qualification :['',[Validators.required]],
        max_qualification :[''],
        min_qualification_stream: [],
        max_qualification_stream: [],
        salary_type :['',[Validators.required]],
        currency :['',[Validators.required]],
        min_monthly_salary :['',[Validators.required]],
        max_monthly_salary :['',[Validators.required, this.checkMinMaxSal]],
        language_pref: ['',[Validators.required]],
        edu_and_or: [''],
        interview_name: ['',[Validators.required]],
        interview_type_id: ['',[Validators.required]],
        interview_date: ['',[Validators.required]],
        interview_start_time: ['',[Validators.required]],
        interview_end_time: ['',[Validators.required, this.checkEndDate]],
        interview_time_slot: ['',[Validators.required]],
        interview_vanue_name: ['',[Validators.required]],
        interview_location_country: [],
        interview_location_city: ['',[Validators.required]],
        interview_venue_address: ['',[Validators.required]],
        walkin_location_type: [],
        benefits: ['',[Validators.required]],
        primary_responsibility: [],
        special_notes: [],
        candidate_type:['',[Validators.required]],
        job_lat:[''],
        job_long:[''],
        condition_type:[2],
        conditions:[]

    });
    if(this.loggedInEmployer && this.loggedInEmployer.user_detail.country.id){
      this.walkInInterviewForm.get('country_id').setValue(this.loggedInEmployer.user_detail.country.id);
      this.walkInInterviewForm.get('interview_location_country').setValue(this.loggedInEmployer.user_detail.country.id);
    }
    
    this.walkInInterviewForm.controls.walkin_location_type.setValue(1);
    this.walkInInterviewForm.controls.candidate_type.setValue(1);
    this.walkInInterviewForm.controls.salary_type.setValue('monthly');
    this.walkInInterviewForm.controls.interview_location_country.setValue(this.loggedInEmployer.user_detail.country._id);
      this.walkInInterviewForm.get('interview_location_country').valueChanges.subscribe(value=>{
        if(value){
          let country = this.countries.find(obj=>obj._id == value);
          this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
        }
      });

    this.walkInInterviewForm.get('city_id').valueChanges.subscribe(value=>{
      if(value){
        let city  = this.user_cities.find(obj=>obj._id == value);
        this.commonService.getLatLong(city.name).then(response=>{
          if(response.status == "OK"){
              let lat_long = response.results[0]['geometry']['location']
              this.walkInInterviewForm.controls.job_lat.setValue(lat_long.lat)
              this.walkInInterviewForm.controls.job_long.setValue(lat_long.lng)
          }else{
            this.walkInInterviewForm.controls.job_lat.setValue('')
            this.walkInInterviewForm.controls.job_long.setValue('')
          }
        })
      }else{
        this.walkInInterviewForm.controls.job_lat.setValue('')
        this.walkInInterviewForm.controls.job_long.setValue('')
      }
    })

    /*this.walkInInterviewForm.get('candidate_type').valueChanges.subscribe(value=>{
      if(value){
        this.walkInInterviewForm.get('candidate_type').setValue(1)
      }else{
        this.walkInInterviewForm.get('candidate_type').setValue(0)
      }
    })*/

    this.asyncInit();
  }

  initReferal(): FormGroup{
    return this.fb.group({
      question_name: ['',[Validators.required]],
    });
  }

  setTrail =(e)=>{
    var is_chrome = /chrome/i.test( navigator.userAgent );
    if(is_chrome){
      let pageHeight = $('.registrationForm').outerHeight();
      var top = e.pageY;
      if(top >= pageHeight){
        top = pageHeight;
      }else if(e.pageY < 1000){
        top = e.pageY;
      }else if(e.pageY > 1000 && e.pageY < 1500){
        top = e.pageY + 500;
      }
      else if(e.pageY > 1500 && e.pageY < 2000){
        top = e.pageY + 1000;
      }else if(e.pageY > 2000 && e.pageY < 2200){
        top = e.pageY + 1200;
      }else{
        top = e.pageY + 1500;
      }
      return top      
    }else{
      return e.pageY +10
    }

  }

  asyncInit(){
        this.commonService.getCountryData.subscribe(data=>{
          if(data && data.status ==200){
            this.countries = data['countries'];
            this.industries = data['industries'];
            let courses = _.uniqBy(data['courses'][""],(e)=>{
              return e.name
            });
            let anyCourse = courses.find(obj=> obj.name.toLowerCase().replace(/ +/g, "") == 'anycourse');
            let newcourse = courses.filter(obj=>obj._id != anyCourse._id);
            newcourse.unshift(anyCourse)
            this.under_graduates = {"":newcourse};
            this.under_graduate_deg = _.uniqBy(data['courses'][""],(e)=>{
              return e.name
            });            
          }
        });

        this.commonService.getCityData.subscribe(data=>{
            if(data && data.status ==200){
              this.cities = data['cities'];
              this.user_cities = this.loggedInEmployer.user_detail.country ?data['cities'].filter(obj=>obj.country_code == this.loggedInEmployer.user_detail.country.code):data['cities']; 
            }
        });

        this.commonService.getCommonData.subscribe(data=>{
          this.commonService.callFooterMenu(1);
            if(data && data.status ==200){
                        
              let otherData = data['other_mst'];
              this.emplymentFor = otherData.filter((data)=>data.entity =='emplyment_for');
              this.experiance =  data['allExprience'];
              expdata = this.experiance;
              this.empType = otherData.filter((data)=>data.entity =='employement_type');
              this.noOfVacancies = otherData.filter((data)=>data.entity =='no_of_vacancies');
              this.salaries = otherData.filter((data)=>data.entity =='salary');
              this.interview_types = otherData.filter((data)=>data.entity =='interview_type');
              this.all_currency = data['all_currency'];
              this.post_graduates = data['post_graduate'];
              //this.under_graduates = data['under_graduate'];
              this.post_graduate_deg = data['post_graduate_deg'];
              //this.under_graduate_deg = data['under_graduate_deg'];
              this.languages = data['alllanguage'];
              this.departments = data['all_department'];
              this.all_departments = data['departments'];
              let curency = this.countries.find(obj=>obj.code === this.loggedInEmployer.user_detail.country.code); 
              this.walkInInterviewForm.controls['currency'].setValue(curency._id); 
              if(localStorage.getItem('walk_in_preview')){
                let prevData = JSON.parse(localStorage.getItem('walk_in_preview'));
                this.setPreviewFormValue(prevData);
              }
              
              if(this.walkin_id){
                this.commonService.show('/api/walkin-interview', this.walkin_id)
                    .subscribe((data)=>{
                      this.walkin_data = data.data;
                      this.setFormValue(this.walkin_data);
                    }, (error)=>{});
              }    
            }
          });

        this.commonService.getAll('/api/walkin-interview?walkin_location_type='+ 1)
            .subscribe((data)=>{
              if(data.status == 200){
                this.all_posted_walkin = data.data;
                //console.log(this.all_posted_walkin);
              }
              
            }, (error)=>{});

        
  }

    searchAutoComplete(key){
      if(key.trim().length){
        this.commonService.getAll('/api/employer/job-title/search?q=' + key)
        .subscribe(data=>{
          if(data.status ==200){
            this.job_titles = data.data;
            this.show_jb_title_srh = true;
          }
        },error=>{});
      }else{
      this.show_jb_title_srh = false;
    }
  }

    autoCompleteSearch(data) {
      this.show_jb_title_srh = false;
      this.walkInInterviewForm.get('job_title').setValue(data.job_title);

  }

  jobPreview(){
    this.showloader = true;
    this.isSubmit = true;
    //alert('asdasd');
    var regex = /(<([^>]+)>)/ig  
    if(this.walkInInterviewForm.invalid){

    /*const invalid = [];
    const controls = this.walkInInterviewForm.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }
    console.log(invalid);*/

      this.showloader = false;
      return;
    }

      if(this.checkIfEmailInString(this.walkInInterviewForm.get('about_this_job').value)){
        this.message.error('Do not write email on about this job field');
        return false;
      }
      else if(this.checkIfPhoneNoInString(this.walkInInterviewForm.get('about_this_job').value)){
        this.message.error('Do not write phone no on about this job field');
        return false;
      }
      else if(this.checkIfUrlInString(this.walkInInterviewForm.get('about_this_job').value)){
        this.message.error('Do not write website url on about this job field');
        return false;
      }
      else if(this.walkInInterviewForm.get('about_this_job').value.includes('http:')){
        this.message.error('Do not write website url on about this job field');
        return false;
      }
       else if(this.checkIfEmailInString(this.walkInInterviewForm.get('benefits').value)){
        this.message.error('Do not write email on benefits field');
        return false;
      }
       else if(this.checkIfPhoneNoInString(this.walkInInterviewForm.get('benefits').value)){
        this.message.error('Do not write phone no on benefits field');
        return false;
      }
       else if(this.checkIfUrlInString(this.walkInInterviewForm.get('benefits').value)){
        this.message.error('Do not write website url on benefits field');
        return false;
      }
      else if(this.walkInInterviewForm.get('benefits').value.includes('http:')){
        this.message.error('Do not write website url on benefits field');
        return false;
      }
      else if(this.checkIfEmailInString(this.walkInInterviewForm.get('primary_responsibility').value)){
        this.message.error('Do not write email on primary responsibility');
        return false;
      }
       else if(this.checkIfPhoneNoInString(this.walkInInterviewForm.get('primary_responsibility').value)){
        this.message.error('Do not write phone no on primary responsibility');
        return false;
      }
       else if(this.checkIfUrlInString(this.walkInInterviewForm.get('primary_responsibility').value)){
        this.message.error('Do not write website url on primary responsibility');
        return false;
      }
      else if(this.walkInInterviewForm.get('primary_responsibility').value.includes('http:')){
        this.message.error('Do not write website url on primary responsibility');
        return false;
      }
      else if(this.checkIfEmailInString(this.walkInInterviewForm.get('special_notes').value)){
        this.message.error('Do not write email on special notes');
        return false;
      }
       else if(this.checkIfPhoneNoInString(this.walkInInterviewForm.get('special_notes').value)){
        this.message.error('Do not write phone no on special notes');
        return false;
      }
       else if(this.checkIfUrlInString(this.walkInInterviewForm.get('special_notes').value)){
        this.message.error('Do not write website url on special notes');
        return false;
      }
      else if(this.walkInInterviewForm.get('special_notes').value.includes('http:')){
        this.message.error('Do not write website url on special notes');
        return false;
      }

      if(this.walkInInterviewForm.get('about_this_job').value.replace(regex,'') == '' ){
        this.message.error('about this job field required');
        return false;
      }

      if(this.walkInInterviewForm.get('benefits').value.replace(regex,'') == '' ){
        this.message.error('benefits field required');
        return false;
      }
      if(this.walkInInterviewForm.get('primary_responsibility').value.replace(regex,'') == '' ){
        this.message.error('primary responsibility field required');
        return false;
      }

      if(this.walkInInterviewForm.get('condition_type').value ===1){
        this.walkInInterviewForm.get('conditions').setValue(this.relevant_conditions) 
      }else if(this.walkInInterviewForm.get('condition_type').value ===2){
        this.walkInInterviewForm.get('conditions').setValue(this.alljob_conditions)
      }else{
        this.walkInInterviewForm.get('conditions').setValue(this.conditionQuestionForm.value)
      }

      this.commonService.getDataWithPost(`/api/check-same/walking-exist`,{
        job_title:this.walkInInterviewForm.get('job_title').value, city_id: this.walkInInterviewForm.get('city_id').value}
        ).subscribe(data=>{
        if(data.is_exist !=1){
          localStorage.setItem('walk_in_preview',JSON.stringify(this.walkInInterviewForm.value)); 
          this.router.navigate(['/employer/walk-in-interview/preview']);       
        }else{
          this.message.error('Same job title and city is already exist');
        }
      })
      

  }

    checkIfEmailInString(text) { 
    var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
  }

  checkIfPhoneNoInString(text) { 
    var re = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/img;
    return re.test(text);
  }

  checkIfUrlInString(text){
    var re = new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?");
    return re.test(text);
  }

  clearForm() {
    this.walkInInterviewForm.reset();
    this.walkInInterviewForm.controls['job_title'].setValue("");
    this.walkInInterviewForm.controls['city_id'].setValue("");
    this.walkInInterviewForm.controls['industry_id'].setValue("");
    this.walkInInterviewForm.controls['employement_for'].setValue("");
    this.walkInInterviewForm.controls['about_this_job'].setValue("");
    this.walkInInterviewForm.controls['position_offered'].setValue("");
    this.walkInInterviewForm.controls['number_of_vacancies'].setValue("");
    this.walkInInterviewForm.controls['employment_type'].setValue("");
    this.walkInInterviewForm.controls['min_experiance'].setValue("");
    this.walkInInterviewForm.controls['max_experiance'].setValue("");
    this.walkInInterviewForm.controls['min_qualification'].setValue("");
    this.walkInInterviewForm.controls['max_qualification'].setValue("");
    this.walkInInterviewForm.controls['min_qualification_stream'].setValue("");
    this.walkInInterviewForm.controls['max_qualification_stream'].setValue("");
    this.walkInInterviewForm.controls['salary_type'].setValue("");
    this.walkInInterviewForm.controls['currency'].setValue("");
    this.walkInInterviewForm.controls['min_monthly_salary'].setValue("");
    this.walkInInterviewForm.controls['max_monthly_salary'].setValue("");
    this.walkInInterviewForm.controls['language_pref'].setValue("");
    this.walkInInterviewForm.controls['edu_and_or'].setValue("");
    this.walkInInterviewForm.controls['interview_name'].setValue("");
    this.walkInInterviewForm.controls['interview_type_id'].setValue("");
    this.walkInInterviewForm.controls['interview_date'].setValue("");
    this.walkInInterviewForm.controls['interview_start_time'].setValue("");
    this.walkInInterviewForm.controls['interview_end_time'].setValue("");
    this.walkInInterviewForm.controls['interview_time_slot'].setValue("");
    this.walkInInterviewForm.controls['interview_vanue_name'].setValue("");
    this.walkInInterviewForm.controls['interview_location_country'].setValue("");
    this.walkInInterviewForm.controls['interview_location_city'].setValue("");
    this.walkInInterviewForm.controls['interview_venue_address'].setValue("");
    this.walkInInterviewForm.controls['candidate_type'].setValue('');
    this.walkInInterviewForm.controls['condition_type'].setValue(2);
    this.storeMultiSelect.industries_value.length =0;
    this.storeMultiSelect.industries_name.length =0;
    this.storeMultiSelect.department_value.length =0;
    this.storeMultiSelect.department_name.length =0;
    this.storeMultiSelect.undergradute_value.length =0;
    this.storeMultiSelect.undergraduate_name.length =0;
    this.storeMultiSelect.postgraduate_value.length =0;
    this.storeMultiSelect.postgraduate_name.length =0;
    this.noSearchSelect.lang_value.length =0;
    this.noSearchSelect.lang_name.length =0;
    this.storeMultiSelect.postgraduate_sub_value.length =0;
    this.storeMultiSelect.undergraduate_sub_value.length =0;
  }

  changeSubmitVal(){
    this.walkInInterviewForm.patchValue({
      industry_id:this.storeMultiSelect.industries_value,
      min_qualification:this.storeMultiSelect.undergradute_value,
      max_qualification:this.storeMultiSelect.postgraduate_value,
      language_pref:this.noSearchSelect.lang_value,
      department_id:this.storeMultiSelect.department_value 
    });
  }

  onItemSelect(item: any) {
    //console.log(item);

  }
  onSelectAll(items: any) {
    //console.log(items);
  }

  getOnchangeEvent(type){
    if(type =='undergraduate'){
      this.underGraduateDiv = true;
    }else if(type =='postgraduate'){
      this.postGraduateDiv = true;
    }else if(type =='industries'){
      this.industriesDiv = true;
    }else if(type=='language'){
      this.laguageDiv = true;
    }else if(type=='department'){
      this.departmentDiv = true;
    }
    
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
        if (e.field_name == 'max_qualification') {
         this.storeMultiSelect.postgraduate_sub_value = this.storeMultiSelect.postgraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));
         this.walkInInterviewForm.patchValue({
          max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value
         });
        }else if(e.field_name == 'min_qualification'){
         this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(e.name));         
          this.walkInInterviewForm.patchValue({
            min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value
          });
        }
      }
    }
    if(e.field_name =='min_qualification'){
      this.walkInInterviewForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(e.field_name =='max_qualification'){
      this.walkInInterviewForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(e.field_name =='industries'){
      this.walkInInterviewForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(e.field_name =='departments'){
      this.walkInInterviewForm.patchValue({
      department_id: e.storedValue
      });
    }
  }

  getSubChange(e){
    let event : any= e;
    if (e.event) {
      let index = e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index == -1) {
        e.storedSubValue.push({'parent_name' :e.parent_name, 'value': e.value});
      }
    } else {
      let index =  e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index != -1) {
        e.storedSubValue.splice(index, 1);
      }
    }  
    if (e.field_name == 'min_qualification') {
      this.walkInInterviewForm.patchValue({
        min_qualification_stream: e.storedSubValue
      });
    } else if (e.field_name == 'max_qualification') {
      this.walkInInterviewForm.patchValue({
        max_qualification_stream: e.storedSubValue
      });
    }
  }    

  getSubData(parent,type){
    if(type =='post-graduate'){
      if(this.storeMultiSelect.postgraduate_sub_value){
        let subData = this.storeMultiSelect.postgraduate_sub_value.filter(obj=>obj.parent_name == parent);
        return subData.length ? subData.length: '';        
      }else{
        return '';
      }

    }else if(type =='under-graduate'){
      if(this.storeMultiSelect.undergraduate_sub_value){
        let subData = this.storeMultiSelect.undergraduate_sub_value.filter(obj=>obj.parent_name == parent);
        return subData.length ? subData.length: '';        
      }else{
        return '';
      }
    }

  }

  noSearchGetchange(e){
    //console.log(e);
     if(e.event){
      let index = this.noSearchSelect.lang_value.indexOf(e.value);
      if(index ==-1){
        this.noSearchSelect.lang_value.push(e.value);
        this.noSearchSelect.lang_name.push(e.name); 
      }
    }else{
      let index = this.noSearchSelect.lang_value.indexOf(e.value);
      if(index !=-1){
        this.noSearchSelect.lang_value.splice(index,1);
        this.noSearchSelect.lang_name.splice(index,1);
      }
    }
    this.walkInInterviewForm.patchValue({
      language_pref: this.noSearchSelect.lang_value
      });
  }
  checknoSearchmultiSelect(e){
    this.laguageDiv = e.status;
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "industry":
       this.storeMultiSelect.industries_value.splice(indx, 1);
       this.storeMultiSelect.industries_name.splice(indx, 1);
       this.walkInInterviewForm.patchValue({
        industry_id: this.storeMultiSelect.industries_value
        });
        break;
      case "department":
       this.storeMultiSelect.department_value.splice(indx, 1);
       this.storeMultiSelect.department_name.splice(indx, 1);
       this.walkInInterviewForm.patchValue({
        department_id: this.storeMultiSelect.department_value
       });
        break;  
      case "undergraduate":
      let nameUnd = this.storeMultiSelect.undergraduate_name[indx];
        this.storeMultiSelect.undergraduate_sub_value = this.storeMultiSelect.undergraduate_sub_value.filter(obj=> obj.parent_name !== String(nameUnd));         
        this.walkInInterviewForm.patchValue({
            min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value
        });
       this.storeMultiSelect.undergradute_value.splice(indx, 1);
       this.storeMultiSelect.undergraduate_name.splice(indx, 1);
       this.walkInInterviewForm.patchValue({
          min_qualification: this.storeMultiSelect.undergradute_value
       });
        break; 
      case "postgraduate":
      let namePost = this.storeMultiSelect.postgraduate_name[indx];
         this.storeMultiSelect.postgraduate_sub_value = this.storeMultiSelect.postgraduate_sub_value.filter(obj=> obj.parent_name !== String(namePost));
         this.walkInInterviewForm.patchValue({
          max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value
         });
       this.storeMultiSelect.postgraduate_value.splice(indx, 1);
       this.storeMultiSelect.postgraduate_name.splice(indx, 1);
       this.walkInInterviewForm.patchValue({
        max_qualification: this.storeMultiSelect.postgraduate_value
       });
        break; 
      case "language":
       this.noSearchSelect.lang_value.splice(indx, 1);
       this.noSearchSelect.lang_name.splice(indx, 1);
       this.walkInInterviewForm.patchValue({
        language_pref: this.noSearchSelect.lang_value
       });
        break;     
      default:
        // code...
        break;
    }
  }

  multiselectAll(e){  
    //console.log(e.storedValue);
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='min_qualification'){
      this.walkInInterviewForm.patchValue({
      min_qualification: e.storedValue
      });
    }else if(filed =='max_qualification'){
      this.walkInInterviewForm.patchValue({
      max_qualification: e.storedValue
      });
    }else if(filed =='industries'){
      this.walkInInterviewForm.patchValue({
      industry_id: e.storedValue
      });
    }else if(filed =='departments'){
      this.walkInInterviewForm.patchValue({
      department_id: e.storedValue
      });
    }
    
  }

  checkmultiselect(e){
    if(e.field_name =='min_qualification'){
      this.underGraduateDiv = e.status;
    }else if(e.field_name =='max_qualification'){
      this.postGraduateDiv = e.status;
    }else if(e.field_name =='industries'){
      this.industriesDiv = e.status;
    }else if(e.field_name =='departments'){
      this.departmentDiv = e.status;
    }
  }

  walkinOnChange(jb_id){
    if(jb_id=='1'){
      this.router.navigate(['/employer/walk-in-interview-local']);
      this.clearForm();
      this.walkInInterviewForm.controls.walkin_location_type.setValue(1);
      this.walkInInterviewForm.controls.salary_type.setValue('monthly');
    }else{
      let data = this.all_posted_walkin.find((jb)=>jb._id ==jb_id);
      //console.log(data);
      this.setFormValue(data);
    }
    if(this.loggedInEmployer && this.loggedInEmployer.user_detail.country.id){
      this.walkInInterviewForm.get('country_id').setValue(this.loggedInEmployer.user_detail.country.id);
      this.walkInInterviewForm.get('interview_location_country').setValue(this.loggedInEmployer.user_detail.country.id);
    }
  }

  setFormValue(data){
    this.storeMultiSelect.industries_value = data.industry_ids;
    this.storeMultiSelect.industries_name = data.industry_name;
    this.storeMultiSelect.department_value = data.department_ids;
    this.storeMultiSelect.department_name = data.department_name;
    if(!_.isEmpty(data.qualifications)){
      this.storeMultiSelect.undergradute_value = data.qualifications;
      this.storeMultiSelect.undergraduate_name = data.qualifications_name;
    }
    //this.storeMultiSelect.postgraduate_value = data.max_qualification;
    //this.storeMultiSelect.postgraduate_name = data.max_qualification_name;
    //this.storeMultiSelect.undergraduate_sub_value = data.min_qualification_stream ? this.under_graduate_deg.filter(obj=>data.min_qualification_stream.find(value=> value== obj._id)).map(obj=>({'parent_name' :obj.parent_name, 'value': obj._id})):[];
    //this.storeMultiSelect.postgraduate_sub_value = data.max_qualification_stream ? this.post_graduate_deg.filter(obj=>data.max_qualification_stream.find(value=> value== obj._id)).map(obj=>({'parent_name' :obj.parent_name, 'value': obj._id})):[];
    this.noSearchSelect.lang_value = data.language_pref_id;
    this.noSearchSelect.lang_name = data.language_pref_name;

    if(data.condition_type === 1){
      this.relevant_conditions.expeiance = data.conditions.expeiance;
      this.relevant_conditions.industry = data.conditions.industry;
      this.relevant_conditions.age = data.conditions.age;
      this.relevant_conditions.location = data.conditions.location;
      this.relevant_conditions.gender = data.conditions.gender;
      this.relevant_conditions.salary = data.conditions.salary;
      this.relevant_conditions.education = data.conditions.education;
    }else if(data.condition_type === 2){
      this.alljob_conditions.all = data.conditions.all
    }else if(data.condition_type === 3){
        this.questions = <FormArray> this.conditionQuestionForm.get('questions')
        data.conditions.questions.forEach((ques,i)=>{
            this.questions.controls[i].get('question_name').setValue(ques.question_name);
            if((data.conditions.questions.length -i) !=1){
              this.questions.push(this.initReferal());
            }
        })
        
    }

    this.walkInInterviewForm.patchValue({
        job_title : data.job_title,
        city_id: data.city_id,
        about_this_job: data.about_this_job,
        industry_id: data.industry_ids,
        department_id: data.department_ids,
        employement_for: data.employement_for._id,
        position_offered: data.position_offered,
        //required_skill: data.required_skill,
        min_experiance: data.min_experiance._id,
        max_experiance: data.max_experiance._id,
        min_qualification: data.min_qualification,
        max_qualification: data.max_qualification,
        min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value,
        max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value,
        //graduate_and_or_post_graduate: data.graduate_and_or_post_graduate,
        employment_type: data.employment_type._id,
        language_pref: data.language_pref_id,
        number_of_vacancies: data.number_of_vacancies,
        min_monthly_salary: data.min_monthly_salary,
        max_monthly_salary: data.max_monthly_salary,
        special_notes: data.special_notes,
        //received_email: data.received_email,
        currency: data.currency,
        salary_type: data.salary_type,
        interview_name: data.interview_name,
        interview_type_id: data.interview_type_id,
        interview_date: data.interview_date,
        interview_start_time: data.interview_start_time,
        interview_end_time: data.interview_end_time,
        interview_time_slot: data.interview_time_slot,
        interview_vanue_name: data.interview_vanue_name,
        interview_location_country: data.interview_location_country,
        interview_location_city: data.interview_location_city._id,
        interview_venue_address: data.interview_venue_address,
        edu_and_or: String(data.edu_and_or),
        benefits: data.benefits,
        primary_responsibility: data.primary_responsibility,
        candidate_type:data.candidate_type,
        condition_type:data.condition_type ? data.condition_type:2
        
    });
  }

  setPreviewFormValue(data){
    //console.log(this.post_graduates);
    this.storeMultiSelect.industries_value = data.industry_id;
    this.storeMultiSelect.industries_name = this.industries[""].filter(obj=> data.industry_id.indexOf(obj._id)!= -1).map(obj=>obj.name);
    //console.log(this.storeMultiSelect.industries_name);
    this.storeMultiSelect.department_value = data.department_id;
    this.storeMultiSelect.department_name = this.all_departments.filter(obj=> data.department_id.find(ob=> ob==obj._id)).map(obj=>obj.name);
    //console.log(this.storeMultiSelect.department_name);
    this.storeMultiSelect.undergradute_value = data.min_qualification;
    this.storeMultiSelect.undergraduate_name = this.under_graduate_deg.filter(obj=> data.min_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
    this.storeMultiSelect.undergraduate_sub_value = data.min_qualification_stream ? data.min_qualification_stream:[];
    this.storeMultiSelect.postgraduate_value = data.max_qualification;
    this.storeMultiSelect.postgraduate_name = this.post_graduate_deg.filter(obj=> data.max_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
    this.storeMultiSelect.postgraduate_sub_value = data.max_qualification_stream ? data.max_qualification_stream:[];
    this.noSearchSelect.lang_value = data.language_pref;
    this.noSearchSelect.lang_name = this.languages.filter(obj=> data.language_pref.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
    //console.log(this.noSearchSelect.lang_name)
    //this.skill_key = data.required_skill.map((obj) => { return { value: obj, display: obj } });
    this.walkInInterviewForm.patchValue({
      job_title: data.job_title,
      //country_id: data.country_id,
      city_id: data.city_id,
      about_this_job: data.about_this_job,
      industry_id: this.storeMultiSelect.industries_value,
      department_id: this.storeMultiSelect.department_value,
      employement_for: data.employement_for,
      //required_skill: data.required_skill,
      benefits: data.benefits,
      min_experiance: data.min_experiance,
      max_experiance: data.max_experiance,
      min_qualification: this.storeMultiSelect.undergradute_value,
      max_qualification: this.storeMultiSelect.postgraduate_value,
      min_qualification_stream: this.storeMultiSelect.undergraduate_sub_value,
      max_qualification_stream: this.storeMultiSelect.postgraduate_sub_value,
      graduate_and_or_post_graduate: String(data.graduate_and_or_post_graduate),
      primary_responsibility: data.primary_responsibility,
      employment_type: data.employment_type,
      language_pref: this.noSearchSelect.lang_name,
      number_of_vacancies: data.number_of_vacancies,
      min_monthly_salary: data.min_monthly_salary,
      max_monthly_salary: data.max_monthly_salary,
      special_notes: data.special_notes,
      //received_email: data.received_email,
      currency: data.currency,
      salary_type: data.salary_type,
      interview_name: data.interview_name,
      interview_type_id: data.interview_type_id,
      interview_date: data.interview_date,
      interview_start_time: data.interview_start_time,
      interview_end_time: data.interview_end_time,
      interview_time_slot: data.interview_time_slot,
      interview_vanue_name: data.interview_vanue_name,
      interview_location_country: data.interview_location_country,
      interview_location_city: data.interview_location_city,
      interview_venue_address: data.interview_venue_address,
      candidate_type:data.candidate_type,
    });
  }

  /*updateForm(){
    this.showloader = true;
    this.isSubmit = true;
    if(this.walkInInterviewForm.invalid){
      this.showloader = false;
      return;
    }
    this.changeSubmitVal();
    this.commonService.update('/api/walkin-interview', this.walkin_id,this.walkInInterviewForm.value)
        .subscribe((data)=>{
          this.showloader =false;
          this.isSubmit = false;
          if(data.status === 200){
            this.message.success('Successfully update');
            this.clearForm();
            this.router.navigate(['/employer/walk-in-interview-local-list']);
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          this.showloader = false;
        });
  }*/

  checkMinMaxSal(c: FormControl) {
    if (!c.root && !c.root.get('min_monthly_salary')) {
      return null;
    }
    if (c.value && c.value < c.root.get('min_monthly_salary').value) {
      return { error_min_max: true };
    }
  }


  checkMinMaxExp(c: FormControl) {
    if (!c.root && !c.root.get('min_experiance')) {
      return null;
    }
    if(c.value){
      let minExpValueOrder = expdata.find(obj=>obj._id == c.root.get('min_experiance').value);
      let maxExpValueOrder = expdata.find(obj=>obj._id == c.value);
      if(!_.isEmpty(maxExpValueOrder) && !_.isEmpty(minExpValueOrder) && maxExpValueOrder.order < minExpValueOrder.order){
        return { error_min_max_exp: true };
      }
    }
  }

  checkEndDate(c: FormControl) {
    let Year = new Date().getFullYear();
    let Month = new Date().getMonth();
    let Day = new Date().getDate();
    if (!c.root && !c.root.get('interview_start_time')) {
      return null;
    }

    if (c.value &&  c.root.get('interview_start_time').value) {
      let startTime = new Date(Month + '-' + Day + '-' + Year + ' '+ c.root.get('interview_start_time').value).getTime();
      let endTime =  new Date(Month + '-' + Day + '-' + Year + ' '+ c.value).getTime();
      if(endTime <  startTime){
        return { error_date: true };
      }
      
    }
  }

  setConditionType(type){
    this.walkInInterviewForm.controls.condition_type.setValue(type)
  }

  addQuestionArray(): void {
    //this.is_submitted = false;
    this.questions = this.conditionQuestionForm.get('questions') as FormArray;
    this.questions.push(this.initReferal());
  }

  deleteQuestionArray(index: number){
    //this.is_submitted =false;
    this.questions = this.conditionQuestionForm.get('questions') as FormArray;
    this.questions.removeAt(index);
  }

  get questionFormData() { return <FormArray>this.conditionQuestionForm.get('questions')['controls']; }

}
