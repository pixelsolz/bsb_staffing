import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalkInInterviewPreviewComponent } from './walk-in-interview-preview.component';

describe('WalkInInterviewPreviewComponent', () => {
  let component: WalkInInterviewPreviewComponent;
  let fixture: ComponentFixture<WalkInInterviewPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalkInInterviewPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalkInInterviewPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
