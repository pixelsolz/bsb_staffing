import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute} from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-walk-in-interview-preview',
  templateUrl: './walk-in-interview-preview.component.html',
  styleUrls: ['./walk-in-interview-preview.component.css']
})
export class WalkInInterviewPreviewComponent implements OnInit {
  previewData: any;
  userData: any;
  walkInData: any={
  	job_title:'',
  	about_this_job:'',
  	benefits:'',
  	currency:'',
  	employement_for:'',
  	employment_type:'',
  	max_experiance:'',
  	min_experiance:'',
  	max_monthly_salary:'',
  	min_monthly_salary:'',
  	number_of_vacancies:'',
  	primary_responsibility:'',
  	required_skill:'',
  	salary_type:'',
  	special_notes:'',
  	language_pref:'',
  	min_qualification:'',
  	max_qualification:'',
  	job_country:'',
  	job_city:'',
  	interview_name: '',
    interview_type_id: '',
    interview_date: '',
    interview_start_time: '',
    interview_end_time: '',
    interview_time_slot: '',
    interview_vanue_name: '',
    interview_location_country: '',
    interview_location_city: '',
    interview_venue_address: '',
  	
  };

  countries: any;
  cities:any;
  degrees:any;
  
  constructor(private commonService: CommonService, private message:MessageService, private router: Router) { 
  	this.previewData = JSON.parse(localStorage.getItem('walk_in_preview'));
  	this.userData = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit() {
  	this.asyncInit();
  }
  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.degrees =_.uniqBy(data['courses'][""],(e)=>{
          return e.name
        });
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
        this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
        this.setDataValue(data);
      }
    });
  }

  setDataValue(data){
  	let otherData = data.other_mst;
  	this.walkInData.job_title = this.previewData.job_title;
  	this.walkInData.about_this_job = this.previewData.about_this_job;
  	this.walkInData.benefits = this.previewData.benefits;
  	this.walkInData.job_city = this.cities.find(obj=>obj._id == this.previewData.city_id).name;;
  	//this.walkInData.job_country = data.countries.find(obj=>obj._id == this.previewData.country_id).name;
  	this.walkInData.currency = this.countries.find(obj=>obj._id == this.previewData.currency).currency_code;
  	this.walkInData.employement_for = otherData.find(obj=>obj._id == this.previewData.employement_for).name;
  	this.walkInData.employment_type = otherData.find(obj=>obj._id == this.previewData.employment_type).name;
  	this.walkInData.max_experiance = data.allExprience.find(obj=>obj._id == this.previewData.max_experiance).name;
  	this.walkInData.min_experiance = data.allExprience.find(obj=>obj._id == this.previewData.min_experiance).name;
  	this.walkInData.max_monthly_salary = this.previewData.max_monthly_salary;
  	this.walkInData.min_monthly_salary = this.previewData.min_monthly_salary;
  	this.walkInData.number_of_vacancies = this.previewData.number_of_vacancies;
  	this.walkInData.primary_responsibility = this.previewData.primary_responsibility;
  	this.walkInData.required_skill = this.previewData.required_skill;
  	this.walkInData.salary_type = this.previewData.salary_type;
  	this.walkInData.special_notes = this.previewData.special_notes;
    this.walkInData.candidate_type = this.previewData.candidate_type;
  	this.walkInData.language_pref = data.alllanguage.filter(obj=> this.previewData.language_pref.find(ob=> ob==obj._id)).map(obj=>obj.language_name) ;
  	this.walkInData.min_qualification = this.degrees.filter(obj=> this.previewData.min_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
  	/*this.walkInData.min_qualification = this.walkInData.min_qualification.map(name=> {
      if(this.previewData.min_qualification_stream){
        let hassub =this.previewData.min_qualification_stream.filter(obj=> obj.parent_name == name);
        if(hassub && hassub.length >0){
          return name + '(' + data.under_graduate_deg.filter(obj=> hassub.find(ob=> ob.value==obj._id)).map(obj=>obj.name) + ')';
        }else{
          return name;
        }        
      }else{
        return name;
      }
    });*/
    /*this.walkInData.max_qualification = data.post_graduate_deg.filter(obj=> this.previewData.max_qualification.find(ob=> ob==obj._id)).map(obj=>obj.name) ;
    this.walkInData.max_qualification = this.walkInData.max_qualification.map(name=> {
      if(this.previewData.max_qualification_stream){
      let hassub =this.previewData.max_qualification_stream.filter(obj=> obj.parent_name == name);
        if(hassub && hassub.length >0){
          return name + '(' + data.post_graduate_deg.filter(obj=> hassub.find(ob=> ob.value==obj._id)).map(obj=>obj.name) + ')';
        }else{
          return name;
        }         
      }else{
        return name;
      }
    });*/

		this.walkInData.interview_name = this.previewData.interview_name;
    this.walkInData.interview_type_id = otherData.find(obj=>obj._id == this.previewData.interview_type_id).name;
    this.walkInData.interview_date = this.previewData.interview_date;
    this.walkInData.interview_start_time = this.previewData.interview_start_time;
    this.walkInData.interview_end_time = this.previewData.interview_end_time;
    this.walkInData.interview_time_slot = this.previewData.interview_time_slot;
    this.walkInData.interview_vanue_name = this.previewData.interview_vanue_name;
    this.walkInData.interview_location_country = this.previewData.interview_location_country ? this.countries.find(obj=>obj._id == this.previewData.interview_location_country).name:'';
    this.walkInData.interview_location_city = this.cities.find(obj=>obj._id == this.previewData.interview_location_city).name;
    this.walkInData.interview_venue_address = this.previewData.interview_venue_address;

  }

  editJob(){
    if(this.previewData.walkin_location_type == 1){
      if(this.previewData.id){
        this.router.navigate(['employer/walk-in-interview-local-edit', this.previewData.id]);
      }else{
        this.router.navigate(['employer/walk-in-interview-local']);
      }
    }else if(this.previewData.walkin_location_type == 2){
      if(this.previewData.id){
        this.router.navigate(['employer/walk-in-interview-international-edit', this.previewData.id]);
      }else{
        this.router.navigate(['employer/walk-in-interview-international']);
      }
    }
  }

  submitPost(){
    if(this.previewData.min_qualification_stream && this.previewData.min_qualification_stream.length){
      this.previewData.min_qualification_stream.forEach(obj=>{
        this.previewData.min_qualification.push(obj.value);
      });      
    }
    if(this.previewData.max_qualification_stream && this.previewData.max_qualification_stream.length){
      this.previewData.max_qualification_stream.forEach(obj=>{
        this.previewData.max_qualification.push(obj.value);
      });      
    }
  	this.previewData.status = 1;
  	if(this.previewData.id){
  		this.commonService.update('/api/walkin-interview', this.previewData.id,this.previewData)
        .subscribe((data)=>{
          
          if(data.status === 200){
            this.message.success('Successfully update');
            if(this.previewData.walkin_location_type == 1){
             this.router.navigate(['/employer/walk-in-interview-local-list']);
	          }
	          if(this.previewData.walkin_location_type == 2){
	            this.router.navigate(['/employer/walk-in-interview-international-list']);
	          }
            localStorage.removeItem('walk_in_preview');
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          
        });
  	}else{
  	this.commonService.create('/api/walkin-interview', this.previewData)
        .subscribe((data)=>{
          if(data.status === 200){
            this.message.success('Successfully save walkin interview');
            if(this.previewData.walkin_location_type == 1){
             this.router.navigate(['/employer/walk-in-interview-local-list']);
	          }
	          if(this.previewData.walkin_location_type == 2){
	            this.router.navigate(['/employer/walk-in-interview-international-list']);
	          }
            localStorage.removeItem('walk_in_preview');
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }

        }, (error)=>{
          
        });
    }
  }

  postLater(){
    if(this.previewData.min_qualification_stream && this.previewData.min_qualification_stream.length){
      this.previewData.min_qualification_stream.forEach(obj=>{
        this.previewData.min_qualification.push(obj.value);
      });      
    }
    if(this.previewData.max_qualification_stream && this.previewData.max_qualification_stream.length){
      this.previewData.max_qualification_stream.forEach(obj=>{
        this.previewData.max_qualification.push(obj.value);
      });      
    }
  	this.previewData.status = 0;
  	if(this.previewData.id){
  		this.commonService.update('/api/walkin-interview', this.previewData.id,this.previewData)
        .subscribe((data)=>{
          
          if(data.status === 200){
            this.message.success('Successfully update');
            
           if(this.previewData.walkin_location_type == 1){
             this.router.navigate(['/employer/walk-in-interview-local-list']);
	          }
	          if(this.previewData.walkin_location_type == 2){
	            this.router.navigate(['/employer/walk-in-interview-international-list']);
	          }
            localStorage.removeItem('walk_in_preview');
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          
        });
  	}else{
  		this.commonService.create('/api/walkin-interview', this.previewData)
        .subscribe((data)=>{
          /*this.showloader =false;
          this.isSubmit = false;*/
          if(data.status === 200){
            this.message.success('Successfully save walkin interview');
            //this.clearForm();
            if(this.previewData.walkin_location_type == 1){
             this.router.navigate(['/employer/walk-in-interview-local-list']);
	          }
	          if(this.previewData.walkin_location_type == 2){
	            this.router.navigate(['/employer/walk-in-interview-international-list']);
	          }
            localStorage.removeItem('walk_in_preview');
          }else if(data.status ==500){
            this.message.error(data.stats_text);
          }
        }, (error)=>{
          //this.showloader = false;
        });
    }
  }

  getSpaceValue(data){
    let value ='';
    if(Array.isArray(data)){
      value = data.toString().replace(/,/g, ', ');
    }else{
      return  data.replace(/,/g, ', ');
    }

  }

}
