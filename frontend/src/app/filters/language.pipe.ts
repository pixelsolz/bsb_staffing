import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'language'
})
export class LanguagePipe implements PipeTransform {

  transform(value: any, args?: any): any {
        if(!value)return null;
        if(!args)return value;
        return value.filter(obj=>obj.language_name.toLowerCase().startsWith(args));

  }

}
