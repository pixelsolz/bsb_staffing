import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchCategory'
})
export class SearchCategoryPipe implements PipeTransform {

  transform(value: any, args?: any): any {
     if(!value)return null;
     if(!args)return value;
     return value.filter(obj=>obj.name.toLowerCase().startsWith(args));
  }

}
