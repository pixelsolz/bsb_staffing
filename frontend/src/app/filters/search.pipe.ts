import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value1: any, args?: any): any {
        if(!value1)return null;
        if(!args)return value1;

        args = args.toLowerCase();
        let value = JSON.parse(JSON.stringify(value1));
         //console.log(value[1]);
        //let totalVal = value[0][1];
        if(value.length)
        {
          if(value[0][1])
          {
             value[0][1] = value[0][1].filter((val)=>{

              if(val.name)
              {
                return val.name.toLowerCase().startsWith(args);
              }
              return false
          });
          }
        }
        
       
       return value;
        
    }
  /*transform(value, keys: string, term: string): any {
  	console.log(keys);

   if (!term) { return value;
    }
    return (value || []).filter((item) => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
  }*/

}
