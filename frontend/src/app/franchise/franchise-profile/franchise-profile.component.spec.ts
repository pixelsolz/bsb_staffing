import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseProfileComponent } from './franchise-profile.component';

describe('FranchiseProfileComponent', () => {
  let component: FranchiseProfileComponent;
  let fixture: ComponentFixture<FranchiseProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranchiseProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
