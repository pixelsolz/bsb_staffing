import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-franchise-profile',
  templateUrl: './franchise-profile.component.html',
  styleUrls: ['./franchise-profile.component.css']
})
export class FranchiseProfileComponent implements OnInit {

  user:any;
	isEdit: boolean=false;
	franchiseProfileUpdateForm :FormGroup;
	countries:any;
	cities:any;
	validation_error: any;
	password_checked: boolean=false;
  coutryWiseCity: any=[];
  years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];  
  
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
  		this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
      //console.log(this.user);
       window.scrollTo(0, 0);
    }

  ngOnInit() {
    var max = new Date().getFullYear(),
    min = max - 70,
    max = max ;
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    } 

  	this.franchiseProfileUpdateForm = this.fb.group({
  		id:'',
  		comp_name:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		registry_certificate: ['', [Validators.required]],
      govt_id:[''],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]], 
      dob_date:['', [Validators.required]],
      dob_month:['', [Validators.required]],
      dob_year:['', [Validators.required]],
      gender:['', [Validators.required]],  		
      name_account_holder:'',     
      bank_name:'',     
      account_no:'',     
      swift_code:'',     
      ifsc_code:'',     
      bank_portal_address:'',
      password:'',
      password_confirmation:'',    
  	});

    this.franchiseProfileUpdateForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });

    this.asyncInit();
  	
  }

  setForm(){
  	this.franchiseProfileUpdateForm.patchValue({
  		id: (this.user)? this.user.id:'',
  		comp_name:(this.user && this.user.user_detail)? this.user.user_detail.name:'',
  		country_id:(this.user && this.user.user_detail)? this.user.user_detail.country._id:'',
  		city_id:(this.user && this.user.user_detail && this.user.user_detail.city)? this.user.user_detail.city._id:'',
  		address:(this.user && this.user.user_detail)? this.user.user_detail.address:'',
  		registry_certificate:(this.user && this.user.user_detail)? this.user.user_detail.registry_certificate:'',
  		govt_id:(this.user && this.user.user_detail)? this.user.user_detail.govt_id:'',
  		contact_person_name:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_name:'',
      contact_person_lastname:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_lastname:'',
  		contact_person_designation:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_designation:'',
  		contact_person_email:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_email:'',
  		contact_person_phone:(this.user && this.user.user_detail)? this.user.user_detail.contact_person_phone:'',
      dob_date: (this.user && this.user.user_detail.dob_date) ? this.user.user_detail.dob_date :'',
      dob_month:(this.user && this.user.user_detail.dob_month) ? this.user.user_detail.dob_month :'',
      dob_year: (this.user && this.user.user_detail.dob_year) ? this.user.user_detail.dob_year :'',
      gender: (this.user && this.user.user_detail.gender) ? this.user.user_detail.gender :'',
  		name_account_holder:(this.user && this.user.user_detail)? this.user.user_detail.name_account_holder:'',
  		bank_name:(this.user && this.user.user_detail)? this.user.user_detail.bank_name:'',
  		account_no:(this.user && this.user.user_detail)? this.user.user_detail.account_no:'',
  		swift_code:(this.user && this.user.user_detail)? this.user.user_detail.swift_code:'',
  		ifsc_code:(this.user && this.user.user_detail)? this.user.user_detail.ifsc_code:'',
  		bank_portal_address:(this.user && this.user.user_detail)? this.user.user_detail.bank_portal_address:'',
  	});
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
       this.countries = data['countries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
         this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
       this.setForm();
      }
    });
  }

  updateProfile(){
    this.validation_error ='';
    
    let formValue = this.formDataSet(this.franchiseProfileUpdateForm);
    console.log(formValue);
    this.commonService.create('/api/franchise/profile-update', formValue)
        .subscribe(data=>{
          if(data.status ==200){
            localStorage.setItem('user', JSON.stringify(data.data));
            this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
            this.commonService.callProfileUpdate(data.data);
            this.message.success('Successfully update profile');
            this.isEdit = false;
          }else if(data.status == 401){
          	this.message.error(data.status_text);
        	}else if(data.status ==422){
          	this.validation_error = data.error;
        	}
        },error=>{});
  }

  formDataSet(form){

   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
   		   console.log(form.get(key).value);
       formData.append(key, form.get(key).value);
   });

   return formData;
 }

 localCertificateUpload(e){
 	this.franchiseProfileUpdateForm.get('registry_certificate').setValue(e.target.files[0]);
 	//this.franchiseProfileUpdateForm.controls['registry_certificate'].setValue(e.target.files[0]);
 }


 govtCertificateUpload(e){
 	this.franchiseProfileUpdateForm.get('govt_id').setValue(e.target.files[0]);
 }

 updateImage(event){
    let file = event.target.files[0];
    let formData = new FormData();
    formData.append('profile_image',file);
    this.commonService.create('/api/franchise/profile-image/update' , formData)
        .subscribe(data=>{
          if(data.status ==200){
            localStorage.setItem('user', JSON.stringify(data.data));
            this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
            this.message.success('Successfully update image');
          }
        },error=>{});
  }


}
