import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var $: any;
declare var Swal: any;
import {MessageService} from '../../services/message.service';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-franchise-registration',
  templateUrl: './franchise-registration.component.html',
  styleUrls: ['./franchise-registration.component.css']
})
export class FranchiseRegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  countries: any=[];
  cities: any=[];
  logoImage: string;
  validation_error: any;
  coutryWiseCity: any=[];
  years:any=[];
  months:any = [{"id": 0, "value":"Jan"},{"id": 1, "value":"Feb"},{"id": 2, "value":"Mar"},{"id": 3, "value":"Apr"},{"id": 4, "value":"May"},{"id": 5, "value":"Jun"},{"id": 6, "value":"Jul"},{"id": 7, "value":"Aug"},{"id": 8, "value":"Sep"},{"id": 9, "value":"Oct"},{"id": 10, "value":"Nov"},{"id": 11, "value":"Dec"},];
  dates:any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private title: Title,
    private meta: Meta) {

   }

  ngOnInit() {
    var max = new Date().getFullYear(),
    min = max - 70,
    max = max ;
    for(var i=min; i<=max; i++){
    this.years.push({"id":i});
    }
    for(var i= 1; i<=31;i++){
      this.dates.push({"id":i});
    } 
  	    this.router.events.subscribe((evt) => {
           /* if (!(evt instanceof NavigationEnd)) {
                return;
            }*/
            window.scrollTo(0, 0)
        });

  	this.registrationForm = this.fb.group({
  		comp_name:['', [Validators.required]],
  		country_id:['', [Validators.required]],
  		city_id:['', [Validators.required]],
  		address: ['', [Validators.required]],
  		registration_certificate: ['', [Validators.required]],
      govt_id:['', [Validators.required]],
  		contact_person_name:['', [Validators.required]],
      contact_person_lastname:['', [Validators.required]],
  		contact_person_designation:['', [Validators.required]],
  		contact_person_email:['', [Validators.required]],
  		contact_person_phone:['', [Validators.required]],
      dob_date:['', [Validators.required]],
      dob_month:['', [Validators.required]],
      dob_year:['', [Validators.required]],
      gender:['', [Validators.required]],  
      name_account_holder:[''],     
      bank_name:[''],     
      account_no:[''],     
      swift_code:[''],     
      ifsc_code:[''],     
      bank_portal_address:[''],
      declaration_user:[''] 		
  	});

    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });
    this.asyncInit();
    this.title.setTitle('Franchise Registration|Franchise Form - bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Create your Franchise account on bsbstaffing.com. Registered Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'Franchise Registration' });
  }

  asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
       this.countries = data['countries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
         this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
    });
  }


   submitForm(){
    this.validation_error ='';
    let formValue = this.formDataSet(this.registrationForm);
  	this.commonService.create('/api/other-registration', formValue)
  		.subscribe((data)=>{
  			        if(data.status ==200){
          this.message.success(data.status_text);
          let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: '<p>You have registered successfully.<br/> After verified from admin you can login to system by email and password.</p>'+
                    'The window will close within <strong></strong> seconds.',
                timer: 15000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  this.router.navigate(['/franchise/login']);
                }
              });
          
        }else if(data.status ==422){
          this.validation_error = data.error;
        }
  		},(error)=>{});
  }



 formDataSet(form){
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
       formData.append(key, form.get(key).value);
   });
   formData.append('user_type', 'franchise');
   return formData;
 }

 localCertificateUpload(e){
 	this.registrationForm.get('registration_certificate').setValue(e.target.files[0]);
 }


 govtCertificateUpload(e){
 	this.registrationForm.get('govt_id').setValue(e.target.files[0]);
 }

}
