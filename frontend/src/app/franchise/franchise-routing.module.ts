import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { FranchiseRegistrationComponent } from './franchise-registration/franchise-registration.component';
import { FranchiseHomeComponent } from './franchise-home/franchise-home.component';
import { FranchiseProfileComponent } from './franchise-profile/franchise-profile.component';

const routes: Routes = [
	{path:'registration',component: FranchiseRegistrationComponent},
	{path:'', loadChildren:'../auth/auth.module#AuthModule'},
	{path:'',canActivate: [AuthGuard], loadChildren:'../common-uses/common-uses.module#CommonUsesModule'},
	//{path:'home',canActivate: [AuthGuard], component: FranchiseHomeComponent},
	{path:'profile',canActivate: [AuthGuard],component:FranchiseProfileComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FranchiseRoutingModule { }
