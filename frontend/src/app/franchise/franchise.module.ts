import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import {AuthModule} from '../auth/auth.module';
import { CommonUsesModule } from '../common-uses/common-uses.module';

import { FranchiseRoutingModule } from './franchise-routing.module';
import { FranchiseRegistrationComponent } from './franchise-registration/franchise-registration.component';
import { FranchiseHomeComponent } from './franchise-home/franchise-home.component';
import { FranchiseProfileComponent } from './franchise-profile/franchise-profile.component';

@NgModule({
  declarations: [FranchiseRegistrationComponent, FranchiseHomeComponent, FranchiseProfileComponent],
  imports: [
    CommonModule,
    FranchiseRoutingModule,
    CoreModule,
    AuthModule,
    CommonUsesModule
  ]
})
export class FranchiseModule { }
