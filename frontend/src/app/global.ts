import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class Global {
	commonDataArray: any;
    baseUrl = 'https://bsbstaffing.com/' //'http://localhost:4200' //'http://65.0.143.21/'; // ;
    apiUrl = 'https://www.bsbstaffing.com/admin'; //'http://localhost/root/job-portal/admin'; //'http://65.0.143.21:8085' //
    communityUrl= 'http://65.0.143.21:8085/' //'http://localhost:3000' //
    communityApiUrl= 'https://community.bsbstaffing.com'; //'http://localhost:8000' //'http://65.0.143.21:8000' // 'http://65.0.143.21:8000'
    imageUrl='https://d3q00ceu4wkbfz.cloudfront.net/upload_files/'
    AWS_URL='http://d3q00ceu4wkbfz.cloudfront.net'

}