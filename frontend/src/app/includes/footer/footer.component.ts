import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { CommonService } from '../../services/common.service';
import {Global} from '../../global'
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  isloggedIn: boolean;
  showheader:boolean=false;
  jobs_by_title: any;
  jobs_by_city: any;
  jobs_by_industry: any;
  popular_job_search: any;
  showEmployer:boolean;
  showEmplyee: boolean;
  user: any;
  mobile_menu: boolean = false;
  islegal:boolean;
  currentYear :any;
  otherUserModalType:string='';
  otherUserSignUpOpen:boolean= false;
  otherUserSignInOpen:boolean= false;
  otherUserForgot:boolean = false;
  isEmloyeeHome:boolean =false
  openSeekerRegistration:boolean = false
  showLogin:boolean =false;
  showRegistration:boolean =false;
  showForgot:boolean=false;
  isForget: boolean = false;
  modalText: string='sign up'
  isregister: boolean = false;
  constructor(private router: Router,private commonService: CommonService,private global: Global) {
    this.currentYear = new Date().getFullYear();
    if(router.url.includes('job-home')){
      this.isEmloyeeHome = true
    }else{
      this.isEmloyeeHome = false
    }

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if(event.url.includes('job-home')){
          this.isEmloyeeHome = true
        }else{
          this.isEmloyeeHome = false
        }
        if(event.url.includes('career-page')){
          //alert("dfg");
          this.showheader= true;
        }
        this.showEmployer = event.url.includes('employer') ? true : false;
        this.islegal = event.url.includes('legal') ? true : false;
        //console.log(this.showEmployer);
      }
    });
    this.commonService.checklogin.subscribe(data => {
      this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
      this.isloggedIn = localStorage.getItem('token') ? true : false;
      this.showEmplyee = (this.user && this.user.user_role == 'employee') ? true : false;
      // this.showLoginModal();
    });

    this.commonService.getAccessMobileMenu.subscribe(data=>{
      this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
      if(this.user && this.user.user_role == 'employer'){
        this.showEmployer = true;
        this.showEmplyee = false;
      }else if(this.user && this.user.user_role == 'employee'){
        this.showEmplyee = true;
        this.showEmployer = false;
      }
    });
  	
  }

  ngOnInit() {
    //alert();
    /*this.commonService.getMasterData(`${this.global.communityApiUrl}/api/cv-search/get/cities/by-count`).then(res=>{
      if(res.status ===200){
         this.jobs_by_city = res.data;
      }

    })*/
  	    this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            this.showEmployer = evt.url.includes('employer') ? true : false;
            this.isloggedIn = localStorage.getItem('token')? true: false;

        });
        this.commonService.getAll('/api/footer-job-listing')
      .subscribe((data) => {
        //this.showloader = false;
        this.jobs_by_title = data.data.jobs_by_title;
        this.jobs_by_city = data.data.citieswithCount;

        this.jobs_by_industry = data.data.jobs_by_industry;
        this.popular_job_search = data.data.popular_job_search;
        //console.log(this.jobs_by_city);

      }, (error) => { });

      this.commonService.getHeaderUrl.subscribe(data=>{
       this.islegal = data.includes('legal') ? true : false;
      });
  }

  menuClose(){
    //alert("ttttt");
    //this.mobile_menu = false;
  }

  checkActiveUrl(value) {
    if (this.router.url.includes('search-job/country') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('search-job/city') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('search-job/role') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('search-job/industry') && value == 'search-job') {
      return true;
    }else if (this.router.url.includes('find-jobs') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('job-alerts') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('recommend-alert') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('applied-jobs') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('saved-jobs') && value == 'job-for-you') {
      return true;
    }else if (this.router.url.includes('applied-international-jobs') && value == 'job-for-you') {
      return true;
    }else if (this.router.url.includes('saved-international-jobs') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('apply-hiring') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('apply-hiring-international') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('training-getting-job') && value == 'we-offer-training') {
      return true;
    } else if (this.router.url.includes('working-visa-training') && value == 'we-offer-training') {
      return true;
    } else if (this.router.url.includes('employer/simple-search') && value == 'employeer-cv-access') {
      return true;
    } else if (this.router.url.includes('employer/advance-search') && value == 'employeer-cv-access') {
      return true;
    } else if (this.router.url.includes('employer/people-search') && value == 'employeer-cv-access') {
      return true;
    } else if (this.router.url.includes('employer/job-posted') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/job-posted-list') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/job-posted-premium') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/job-posted-premium-list') && value == 'employeer-job_post') {
      return true;
    } 
     else if (this.router.url.includes('employer/free-international-job') && value == 'employeer-job_post') {
      return true;
    }  else if (this.router.url.includes('employer/premium-international-job') && value == 'employeer-job_post') {
      return true;
    }
    else if (this.router.url.includes('employer/walk-in-interview-local') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/walk-in-interview-local-list') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/walk-in-interview-international') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/walk-in-interview-international-list') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/email-template/create') && value == 'employeer-email-service') {
      return true;
    } else if (this.router.url.includes('employer/email-template') && value == 'employeer-email-service') {
      return true;
    } else if (this.router.url.includes('employer/mail') && value == 'employeer-email-service') {
      return true;
    } else if (this.router.url.includes('employer/folders-list') && value == 'employeer-folder-manage') {
      return true;
    } else if (this.router.url.includes('employer/resumes') && value == 'employeer-folder-manage') {
      return true;
    }else if (this.router.url.includes('mail/inbox') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('mail/sentbox') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('mail/starred') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('mail/delete') && value == 'mail') {
      return true;
    }
  }

    receivedOtherSignInClose($event){
      this.otherUserModalType = '';
      this.otherUserSignInOpen = $event
    }

    receivedOtherSignUpOPen($event){
      this.otherUserSignUpOpen = $event
      this.otherUserSignInOpen = false
      this.otherUserForgot = false
    }

     receivedOtherSignUpClose($event){
      this.otherUserModalType = '';
      this.otherUserSignUpOpen = $event
    }

    receivedOtherSignInOPen($event){
      this.otherUserSignUpOpen = false
      this.otherUserForgot = false
      this.otherUserSignInOpen = $event
    }

    receviedOpenForgotPassword($event){
       this.otherUserSignInOpen = false
       this.otherUserSignUpOpen = false
       this.otherUserForgot = $event
    }

    recievedForgotClose($event){
      this.otherUserModalType = '';
      this.otherUserForgot = $event
    }

    handleCloseModal(){
      this.openSeekerRegistration = false
      this.showLogin = false;
      this.showForgot = false;
      this.showRegistration = false;
    }

   footerEventCall($event) {
       if(this.openSeekerRegistration){
        this.openSeekerRegistration = false
        this.showLogin = false;
        this.showForgot = false;
        this.showRegistration = false;
       }

  }

  checkIsRegister($event) {
    if ($event == '3') {
      this.isForget = true;
      this.showForgot = true;
      this.showLogin = false;
      this.showRegistration = false
      this.modalText = 'forgot password'

    }else if($event == '1'){
      this.showForgot = false;
      this.showLogin = true;
      this.showRegistration = false
      this.modalText = 'sign in'
    } else {
      this.modalText = 'sign up'
      this.showRegistration = true;
      this.isregister = ($event == '2') ? true : false;
      this.showForgot = false;
      this.showLogin = false;
    }

  }

}
