import { Component, OnInit, ViewChild,HostListener,ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CommonService } from '../../services/common.service';
import { AuthService } from '../../services/auth.service';
declare var $: any;
import { Global } from '../../global';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],

})
export class HeaderComponent implements OnInit {

  createFolderForm: FormGroup;
  showEmployer: boolean;
  showEmplyee: boolean;
  closeResult: string;
  isregister: boolean;
  isloggedIn: boolean;
  user: any;
  trigerData: any;
  otherUser: boolean;
  showloader: boolean;
  isSubmit: boolean;
  validationError: any;
  isForget: boolean = false;
  mobile_menu: boolean = false;
  showheader:boolean =false;
  showLogoutMenu:boolean =false;
  showMobProfMenu: boolean =false;
  showlegal: boolean =false;
  openModal:boolean=false;
  showLogin:boolean =false;
  showRegistration:boolean =false;
  showForgot:boolean=false;
  modalText: string='sign in'
  offLogin: boolean = false;

  @ViewChild('loginModal') loginModal;
  @HostListener('document:click', ['$event'])
  onClick(event) {
    const isInside = this.eRef.nativeElement.contains(event.target);
    if(isInside && ((event.target.nodeName==='IMG' && event.target.id==='menu_open')  || (event.target.nodeName==='A' && event.target.id==='user_a'))){
      this.showLogoutMenu = !this.showLogoutMenu;
    } else {
      this.showLogoutMenu = false;
    }
    if(event.target.className =='mobile_profile_menu'){
      this.showMobProfMenu = true;
    }else{
      this.showMobProfMenu = false;
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private modalService: NgbModal,
    private commonService: CommonService, private authService: AuthService, private global: Global, private fb: FormBuilder, private message: MessageService,private eRef: ElementRef,) {
    this.showEmployer = false;
    this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
    this.isloggedIn = localStorage.getItem('token') ? true : false;
     if(window.location.pathname.includes('privacy-policy') || window.location.pathname.includes('user-agreement') || window.location.pathname.includes('cookie-policy') 
          || window.location.pathname.includes('terms-conditions') ){
            this.offLogin = true
        }else{
          this.offLogin = false
        }
    this.router.events.subscribe((event) => {     
      if (event instanceof NavigationStart) {
        this.showlegal = false;
        this.showEmployer = event.url.includes('employer') ? true : false;


        if (event.url.includes('college') || event.url.includes('agency') || event.url.includes('franchise') || event.url.includes('trainer')) {
          this.otherUser = true;
          this.showEmployer = false;
          this.showEmplyee = false;
          if(this.user && this.user.user_role == 'employer'){
            this.otherUser = false;
            this.showEmployer = true;
          }
         
        } else {
          this.otherUser = false;          
        }
        if(event.url.includes('career-page')){
          this.showheader= true;
        }
         
        if(event.url.includes('legal')){
          this.showlegal= true;
          this.otherUser = false;
          this.showEmployer = false;
          this.showEmplyee = false;
          //this.showheader = true;
        } 
        if(this.user && this.user.user_role == 'employee'){
            this.showEmplyee = event.url.includes('legal') ? false: true;
        }

      }
    });
    if(this.user && (this.user.user_role =='franchise' || this.user.user_role =='college' || this.user.user_role =='agency' || this.user.user_role =='trainer')){
      this.otherUser = true;
    }
    this.isregister = false;
    this.isloggedIn = localStorage.getItem('token') ? true : false;
    // this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';

    this.commonService.checklogin.subscribe(data => {
      this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
      this.isloggedIn = localStorage.getItem('token') ? true : false;
      this.showEmplyee = (this.user && this.user.user_role == 'employee') ? true : false;
       if(this.router.url ==='/dashboard'){
            setTimeout(()=>{
              this.showEmployer = false;
            },500);
            
          }
      this.handleCloseModal();
    });

    this.commonService.applyCheckLogin.subscribe(data => {
      this.trigerData = data;
      $('#job_seeker_modal').trigger('click');
        
      $('#job_seeker_CALL').trigger('click');
    });


    this.commonService.loginPopup.subscribe(data=>{

      if(this.router.url.includes('forgotpassword')){
        $('.loginSection').removeClass('activeSection');
        $('.loginSection').addClass('deactiveSection');
        $('.forgotSection').addClass('activeSection');
        
      }
        this.trigerData = data;
        $('#employer_login').trigger('click');
    });

    if(this.router.url.includes('legal')){
      this.showlegal= true;
          this.otherUser = false;
          this.showEmployer = false;
          this.showEmplyee = false;
          //this.showheader = true;
    }
  }

  ngOnInit() {
    //console.log(this.activatedRoute.snapshot.params)
    this.commonService.getHeaderUrl.subscribe(data=>{
       this.showEmployer = data.includes('employer') ? true : false;
    });
    this.commonService.getHeaderUrl.subscribe(data=>{
       this.showlegal = data.includes('legal') ? true : false;
    });

    this.commonService.checkProfileUpdate.subscribe(data=>{
      this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : '';
    });
    /*this.commonService.checklogin.subscribe(data=>{
      if(data =='registration'){
        this.isregister =false;
      }
    });*/
    this.createFolderForm = this.fb.group({
      folder_name: [],
    });


  }

  ngAfterViewInit() {
    //this.checkIsRegister('3')
  }

  /*checkIsRegister($event) {
    if ($event == '3') {
      this.isForget = true;
    } else {
      this.isregister = ($event == '2') ? true : false;
      this.isForget = false;
    }

  }*/

    checkIsRegister($event) {
    if ($event == '3') {
      this.isForget = true;
      this.showForgot = true;
      this.showLogin = false;
      this.modalText = 'forgot password'

    }else if($event == '1'){
      this.showForgot = false;
      this.showLogin = true;
      this.showRegistration = false
      this.modalText = 'sign in'
    } else {
      this.modalText = 'sign up'
      this.showRegistration = true;
      this.isregister = ($event == '2') ? true : false;
      this.showForgot = false;
      this.showLogin = false;
    }

  }



  open(content, size) {
    this.modalService.open(content, { size: size, backdrop: false }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

   logOut(){
     this.authService.logOut('/api/logout').subscribe((data)=>{
       if(data.status ==200){
         localStorage.removeItem('token');
         localStorage.removeItem('user');
         localStorage.removeItem('is_employer');
         this.trigerData ='';
         this.isloggedIn = localStorage.getItem('token')? true: false;
         if(this.showEmployer){
            localStorage.getItem('job_preview')? localStorage.removeItem('job_preview'):'';
            localStorage.getItem('international_job_preview')? localStorage.removeItem('international_job_preview'):'';
            localStorage.getItem('advSearch')? localStorage.removeItem('advSearch'):'';
            localStorage.getItem('search-queries')? localStorage.removeItem('search-queries'):'';
            localStorage.getItem('cvSearch')? localStorage.removeItem('cvSearch'):'';
            localStorage.getItem('walk_in_preview')? localStorage.removeItem('walk_in_preview'):'';
           this.router.navigate(['employer']);
         }else{
           location.href= this.global.baseUrl;
           //this.router.navigate(['']);
         }
       }
     });
   }

   userSwitch(url){

     if(this.authService.isUserLoginIn()){
       this.logOut();
     }
     
     this.router.navigate([url]);
   }


  showLoginModal() {
    document.getElementById('loginModal').click();
    //this.loginModal.nativeElement.className = 'modal fade signinModal show';
  }

  submitFolder() {
    this.showloader = true;
    this.isSubmit = true;
    if (this.createFolderForm.invalid) {
      this.showloader = false;
      return;
    }

    this.commonService.create('/api/folders', this.createFolderForm.value)
      .subscribe((data) => {
        this.showloader = false;
        this.isSubmit = false;
        if (data.status === 200) {
          this.message.success('Successfully Folder Create');
          this.commonService.callFolderData('');
          this.clearForm();
          $("#folder_model").trigger('click');
          this.router.navigate(['/employer/folders-list']);
        } else if (data.status == 500) {
          this.message.error(data.stats_text);
        } else if (data.status == 422) {
          this.validationError = data.error;
          //console.log(this.validationError);
        }
      }, (error) => {
        this.showloader = false;
      });

  }

  clearForm() {
    this.createFolderForm.reset();
    this.createFolderForm.controls['folder_name'].setValue('');
  }

  checkActiveUrl(value) {
    if (this.router.url.includes('search-job/country') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('search-job/city') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('search-job/role') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('search-job/industry') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('find-jobs') && value == 'search-job') {
      return true;
    } else if (this.router.url.includes('job-alerts') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('recommend-alert') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('applied-jobs') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('saved-jobs') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('applied-international-jobs') && value == 'job-for-you') {
      return true;
    }else if (this.router.url.includes('saved-international-jobs') && value == 'job-for-you') {
      return true;
    }else if (this.router.url.includes('apply-hiring') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('apply-hiring-international') && value == 'job-for-you') {
      return true;
    } else if (this.router.url.includes('training-getting-job') && value == 'we-offer-training') {
      return true;
    } else if (this.router.url.includes('working-visa-training') && value == 'we-offer-training') {
      return true;
    } else if (this.router.url.includes('employer/simple-search') && value == 'employeer-cv-access') {
      return true;
    } else if (this.router.url.includes('employer/advance-search') && value == 'employeer-cv-access') {
      return true;
    } else if (this.router.url.includes('employer/people-search') && value == 'employeer-cv-access') {
      return true;
    } else if (this.router.url.includes('employer/job-posted') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/job-posted-list') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/job-posted-premium') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/job-posted-premium-list') && value == 'employeer-job_post') {
      return true;
    } 
     else if (this.router.url.includes('employer/free-international-job') && value == 'employeer-job_post') {
      return true;
    }else if (this.router.url.includes('employer/free-international/list') && value == 'employeer-job_post') {
      return true;
    }  else if (this.router.url.includes('employer/premium-international-job') && value == 'employeer-job_post') {
      return true;
    } 
    else if (this.router.url.includes('employer/walk-in-interview-local') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/walk-in-interview-local-list') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/walk-in-interview-international') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/walk-in-interview-international-list') && value == 'employeer-job_post') {
      return true;
    } else if (this.router.url.includes('employer/email-template/create') && value == 'employeer-email-service') {
      return true;
    } else if (this.router.url.includes('employer/email-template') && value == 'employeer-email-service') {
      return true;
    }else if (this.router.url.includes('/employer/mail') && value == 'employeer-email-service') {
      return true;
    } else if (this.router.url.includes('employer/folders-list') && value == 'employeer-folder-manage') {
      return true;
    } else if (this.router.url.includes('employer/resumes') && value == 'employeer-folder-manage') {
      return true;
    }else if (this.router.url.includes('mail/inbox') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('mail/sentbox') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('mail/starred') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('mail/delete') && value == 'mail') {
      return true;
    }else if (this.router.url.includes('employer/payment') && value == 'admin-data') {
      return true;
    }else if (this.router.url.includes('employer/recent-posted-job') && value == 'admin-data') {
      return true;
    }
  }

  clickEvent(){
    //alert('hgh');
    this.mobile_menu = !this.mobile_menu;
    this.commonService.callMobileMenu('check');
  }

  menuClose(){
    this.mobile_menu = false;
  }

   handleCloseModal(){
    this.openModal = false;
    this.showLogin = false;
    this.showForgot = false;
    this.showRegistration = false;
  }

}
