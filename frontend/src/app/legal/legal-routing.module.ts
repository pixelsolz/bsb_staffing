import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';

import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { UserAgreementComponent } from './user-agreement/user-agreement.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

const routes: Routes = [
	{path:'privacy-policy',component: PrivacyPolicyComponent},
	{path:'cookie-policy',component: CookiePolicyComponent},
	{path:'user-agreement',component: UserAgreementComponent},
	{path:'terms-conditions',component: TermsConditionsComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LegalRoutingModule { }
