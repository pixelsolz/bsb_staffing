import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var $;
declare var swal: any;
import { CommonService } from '../../services/common.service';
@Component({
  selector: 'app-user-agreement',
  templateUrl: './user-agreement.component.html',
  styleUrls: ['./user-agreement.component.css']
})
export class UserAgreementComponent implements OnInit {

  constructor( private commonService: CommonService) { 
    this.commonService.callFooterMenu(1);
  }

  ngOnInit() {
  }
  scroll(id) {   
    let div:any = document.getElementById(id);
    const header_heigth = $('header').innerHeight();
      $('html, body').animate({
        scrollTop: $('#' + id).offset().top - header_heigth
      }, 1000);

  }
  scroleToTop(){
  	//window.scroll(0,0);
  	$('html, body').animate({
	        scrollTop:0
	  }, 800, function(){ 
	  });
  }

}
