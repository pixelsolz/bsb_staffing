import { TestBed, async, inject } from '@angular/core/testing';

import { ProfileCheckGuard } from './profile-check.guard';

describe('ProfileCheckGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileCheckGuard]
    });
  });

  it('should ...', inject([ProfileCheckGuard], (guard: ProfileCheckGuard) => {
    expect(guard).toBeTruthy();
  }));
});
