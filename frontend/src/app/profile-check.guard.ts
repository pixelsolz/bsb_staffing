import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Global } from './global';
import { CommonService } from './services/common.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileCheckGuard implements CanActivate {
	previousUrl: string;
  nextUrl: string;
  isEmployer: any;
  user:any;
  constructor(private authService: AuthService, private router: Router, private global: Global,
   private commonservice: CommonService) {
   	
   	this.commonservice.checkEmployer.subscribe((check)=>{
          this.isEmployer =localStorage.getItem('is_employer');
          this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
     });
      
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    //return true;
    //console.log(this.user.profile_update);
    if (this.authService.isUserLoginIn() && this.isEmployer ==1 && this.user.profile_update == 0) {
        if(state.url.includes('employer')){
        	console.log(state.url);
        	if(state.url !='/employer/dashboard'){
        		this.router.navigate(['/employer/dashboard']);
        	}
        	
        	this.commonservice.callProfileUpdate('call');
        	//console.log(this.user.profile_update);
          return true;
        }else {
        	
        	return true;
        }
    }else {
    	return true;
    }
  }
}
