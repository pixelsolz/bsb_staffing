import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgencyHomeComponent } from './agency-home/agency-home.component';
import { AgencyRegistrationComponent } from './agency-registration/agency-registration.component';
import { AuthGuard } from './../auth.guard';
import { AgencyProfileComponent } from './agency-profile/agency-profile.component';

const routes: Routes = [
	{path:'registration',component: AgencyRegistrationComponent},
	{path:'', loadChildren:'../auth/auth.module#AuthModule'},
	{path:'',canActivate: [AuthGuard], loadChildren:'../common-uses/common-uses.module#CommonUsesModule'},
	{path:'profile',canActivate: [AuthGuard],component:AgencyProfileComponent},
	//{path:'home',canActivate: [AuthGuard], component: AgencyHomeComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecruitmentAgencyRoutingModule { }
