import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import {AuthModule} from '../auth/auth.module';
import { CommonUsesModule } from '../common-uses/common-uses.module';

import { RecruitmentAgencyRoutingModule } from './recruitment-agency-routing.module';
import { AgencyHomeComponent } from './agency-home/agency-home.component';
import { AgencyRegistrationComponent } from './agency-registration/agency-registration.component';
import { AgencyProfileComponent } from './agency-profile/agency-profile.component';

@NgModule({
  declarations: [AgencyHomeComponent, AgencyRegistrationComponent, AgencyProfileComponent],
  imports: [
    CommonModule,
    RecruitmentAgencyRoutingModule,
    CoreModule,
    AuthModule,
    CommonUsesModule
  ]
})
export class RecruitmentAgencyModule { }
