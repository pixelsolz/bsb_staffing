import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from './main.service';
import {Global} from './../global';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends MainService {

  constructor(http: HttpClient, private httpClient: HttpClient, private global: Global) {
    super(http);
  }

   userAuthentication(url, data): Observable<any> {
    return this.httpClient.post(url, data, this.getHttpOptions())
    .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  logOut(url) : Observable<any>{
    return this.httpClient.get(url, this.getHttpOptions());
  }

  isUserLoginIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

}
