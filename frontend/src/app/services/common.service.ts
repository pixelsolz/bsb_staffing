import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from './main.service';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService extends MainService{

   private callLoginModal = new BehaviorSubject<string>('login');

   private employerCheck = new BehaviorSubject<string>('0');

   private materiallizeModal = new BehaviorSubject<string>('call');

   private searchReload = new BehaviorSubject<string>('reload');

   private profileUpdate = new BehaviorSubject<string>('call');

   private checkApplyLogin = new BehaviorSubject<string>('login');

   private callLoginPopupModel = new BehaviorSubject<string>('login');

   private emailUpdate = new BehaviorSubject<string>('call');

   private composeMail = new BehaviorSubject<any>('call');

   private checkCommonData = new BehaviorSubject<any>('call');

   private checkCountryData = new BehaviorSubject<any>('call');

   private checkCityData = new BehaviorSubject<any>('call');

   private checkNotFound = new BehaviorSubject<any>('call');

   private checkHeaderUrl = new BehaviorSubject<any>('call');

   private mobileMenuOPen =  new BehaviorSubject<any>('call');

   private pageOPen = new BehaviorSubject<any>('');

   private updateFolder = new BehaviorSubject<any>('call');

   private footerOpen = new BehaviorSubject<any>('');

   private checkJobsData  = new BehaviorSubject<any>('call');

   private employerSliderImages = new BehaviorSubject<any>('');

   checklogin = this.callLoginModal.asObservable();

   checkEmployer = this.employerCheck.asObservable();

   checkmateriallize = this.materiallizeModal.asObservable();

   checkReload = this.searchReload.asObservable();

   checkProfileUpdate = this.profileUpdate.asObservable();

   applyCheckLogin = this.checkApplyLogin.asObservable();

   loginPopup = this.callLoginPopupModel.asObservable();

   emailUpdateCheck = this.emailUpdate.asObservable();

   composeMailOPen = this.composeMail.asObservable();

   getCommonData = this.checkCommonData.asObservable();

   getCountryData = this.checkCountryData.asObservable();

   getCityData = this.checkCityData.asObservable();

   isNotFound = this.checkNotFound.asObservable();

   getHeaderUrl = this.checkHeaderUrl.asObservable();

   getAccessMobileMenu = this.mobileMenuOPen.asObservable();

   showOpenPage = this.pageOPen.asObservable();

   updateFolderData = this.updateFolder.asObservable();

   openFooterMenu = this.footerOpen.asObservable();

   getJobsData = this.checkJobsData.asObservable();


   getSliderImages = this.employerSliderImages.asObservable();

   constructor(http: HttpClient) {
    super(http);
  }

  checkCallLogin(check){
  	this.callLoginModal.next(check);
  }

  employer_check(check){
  	this.employerCheck.next(check);
  }

  callMaterializeModal(call){
  	this.materiallizeModal.next(call);
  }

  callReload(call){
    this.searchReload.next(call);
  }

  callProfileUpdate(call){
    this.profileUpdate.next(call);
  }

  callApplyCheckLogin(call){
    this.checkApplyLogin.next(call);
  }
  callLoginPopup(call){
    this.callLoginPopupModel.next(call);
  }

  callEmailUpdate(call){
    this.emailUpdate.next(call);
  }

  callComposeMail(call){
    this.composeMail.next(call);
  }

  callCommonData(call){
    this.checkCommonData.next(call);
  }

  callCountryData(call){
    this.checkCountryData.next(call);
  }

  callCityData(call){
    this.checkCityData.next(call);
  }


  callNotFound(call){
    this.checkNotFound.next(call);
  }

  sendHeaderUrl(call){
    this.checkHeaderUrl.next(call);
  }

 callMobileMenu(call){
   this.mobileMenuOPen.next(call);
 }

 callOpenPage(call){
   this.pageOPen.next(call);
 }

 callFolderData(call){
   this.updateFolder.next(call);
 }

 callFooterMenu(call){
   this.footerOpen.next(call);
 }

 callJobsData(call){
   this.checkJobsData.next(call);
 }

 callSliderImages(call){
   this.employerSliderImages.next(call);
 }

 getLatLong = async(address) =>{
    try{
        let url = `https://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=en&address=${address}&key=AIzaSyDzub4OkqZIbyJb5mrLTMkhW2RD4gk0ZG4`
        let response = await fetch(url);
        if (response.ok) { 
            let json = await response.json();
            return json
          } else {
              return response.status
          }
    }catch(err){

    }
 }

 getCVSearchResult =(url, form)=>{
  return fetch(`${url}`, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(form),
    }).then(response => response.json());
 
 }

 getMasterData =(url)=>{
   return fetch(`${url}`, {
      method: 'GET', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(response => response.json());
 }

}
