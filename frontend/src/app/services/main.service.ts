import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, of, throwError } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MainService {
 validationError: any = [];

  constructor(private http: HttpClient) { }

   getAll(url): Observable<any> {
    return this.http.get(url, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  create(url, resource): Observable<any> {
    return this.http.post(url, resource, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  getDataWithPost(url, resource): Observable<any>{
     return this.http.post(url, resource, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  show(url, id): Observable<any> {
    return this.http.get(url + '/' + id, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  update(url, id, data): Observable<any> {
    return this.http.put(url + '/' + id, data, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  updateWithPatch(url, id, data): Observable<any> {
    return this.http.patch(url + '/' + id, data, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  delete(url, id): Observable<any> {
    return this.http.delete(url + '/' + id, this.getHttpOptions());
  }

  deleteWithPost(url, resource): Observable<any> {
    return this.http.post(url, resource, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  getToken(): string {
    return localStorage.getItem('token');
  }


  getNodeApiPost(url, resource): Observable<any> {
    return this.http.post(url, resource, this.getNodeHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

  getHttpOptions() {
    const httpOption = {
      headers: new HttpHeaders({'Accept': 'application/json', 'X-Requested-With': 'XMLHttpRequest','Authorization': 'Bearer ' + this.getToken()})
      //headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest', 'Authorization': 'Bearer ' + this.getToken()})
    };
    return httpOption;
  }

  getNodeHttpOptions(){
    const httpOption = {
      headers: new HttpHeaders({'Accept': 'application/json'})
      //headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest', 'Authorization': 'Bearer ' + this.getToken()})
    };
    return httpOption;   
  }

   /*getHttpOptions() {
    const httpOption = {
      headers: new HttpHeaders({'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest','Authorization': 'Bearer ' + this.getToken()})
      //headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest', 'Authorization': 'Bearer ' + this.getToken()})
    };
    return httpOption;
  }*/

    _errorHandler(error: HttpErrorResponse) {
    // console.log(error.error.error);
    if (error.status === 422) {
      this.validationError = error.error;
    }
    console.log(this.validationError);
    console.log(error.message);
    return throwError(error || 'some error');
  }
}
