import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-trainer-booking-list',
  templateUrl: './trainer-booking-list.component.html',
  styleUrls: ['./trainer-booking-list.component.css']
})
export class TrainerBookingListComponent implements OnInit {

 bookingType: string;
 allBookedData: any=[];
 currentDate: any;
  constructor(private commonService: CommonService) { 
    this.bookingType = 'current';
  }

  ngOnInit() {
  	this.asyncInit();
  }

  asyncInit(){
  	this.commonService.getAll('/api/trainer/booked/list/current')
  		.subscribe(data=>{
        this.commonService.callFooterMenu(1);
          this.allBookedData = data.data;
  		},error=>{});
  }

  handleType(type){
    this.commonService.getAll('/api/trainer/booked/list/'+ type)
      .subscribe(data=>{
          this.allBookedData = data.data;
      },error=>{});
  }

}
