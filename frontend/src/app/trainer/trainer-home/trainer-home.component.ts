import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import {Global} from '../../global';

@Component({
  selector: 'app-trainer-home',
  templateUrl: './trainer-home.component.html',
  styleUrls: ['./trainer-home.component.css']
})
export class TrainerHomeComponent implements OnInit {

  
  userData: any={};
  allData: any={};
  use_role: string;
  announcements: any=[];
  referrals: any=[];
  isLoaded:boolean=false;
  constructor(private commonService: CommonService,private global: Global) { 
  	this.userData = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
    console.log(this.userData);
    this.use_role = this.userData.user_role;
  	window.scrollTo(0, 0);
  	this.asyncInit();
  }

  ngOnInit() {
  }
  asyncInit(){
  	this.commonService.getAll('/api/other-user/home')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.allData = data.data;
        this.referrals = data.data.benefit_data.filter(obj=>obj.type ==1);
        this.announcements = data.data.benefit_data.filter(obj=>obj.type ==2);
        //console.log(this.allData);
        this.isLoaded = true;
  		},(error)=>{});
  }
}
