import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.component.html',
  styleUrls: ['./trainer-profile.component.css']
})
export class TrainerProfileComponent implements OnInit {

  isEdit: boolean=false;
  profileForm: FormGroup;
  submitted : boolean = false;
  user: any;
  countries:any=[];
  cities: any=[];
  degress: any=[];
  show_sub_under: any=[];
  degreeData: any;
  industries: any=[];
  skill_data: any=[];
  skill_key: any;
  expYears: any=[];
  educationDiv: boolean=false;
  validation_errors: any;
  password_checked: boolean=false;
  coutryWiseCity: any=[];
  storeMultiSelect: any={
    education_value:[],
    education_name:[],
    education_sub_value:[]
  };
  trainerQualifications :any =[];
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'Few Words About Your Profile',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
  		this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
      this.trainerQualifications = this.user.user_detail.education.map(obj=>{
        if(obj.child && obj.child.length){
        let hasChild =  obj.child.filter(value=> this.user.user_detail.education_stream.indexOf(value._id) != -1).map(val=>(val.name));
          if(hasChild){
             return obj.name + '(' + hasChild + ')';
          }else{
             return obj.name;
          }
        }else{
          return obj.name;
        }
      });

    }

  ngOnInit() {
  	this.profileForm =this.fb.group({
      id:'',
  	  name:['', [Validators.required]],
      mid_name:['', [Validators.required]],
      family_name:[''],
  	  email:['', [Validators.required,Validators.email]],
  	  phone_no:['', [Validators.required]],
      password:'',
      password_confirmation:'',
  	  skype_id:[''],
  	  gender:[''],
  	  date_of_birth:[''],
  	  country_id:['', [Validators.required]],
  	  city_id:['', [Validators.required]],
  	  address:['', [Validators.required]],
      industry_id: ['', [Validators.required]],
  	  current_designation:[''],
  	  highest_education:[''],
  	  year_of_exp:[''],
  	  skills:[''],
  	  about_profile:[''],
  	  cv_path:[''],	  
      profile_image :'',
      identity_proof :'',
      name_account_holder :'',
      bank_name:'',
      account_no:[],
      swift_code:[''],
      ifsc_code:[''],
      bank_portal_address:['']
  	});

    this.profileForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });
    this.asyncInit();
  }

   asyncInit(){
    this.commonService.getCountryData.subscribe(data=>{
      if(data && data.status ==200){
        this.countries = data['countries'];
        this.industries = data['all_industries'];
      }
    });

    this.commonService.getCityData.subscribe(data=>{
        if(data && data.status ==200){
          this.cities = data['cities'];
        }
    });

    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
          this.degress = data['degrees'];
          let underGraduate = data['under_graduate'];
          let postGraduate = data['post_graduate'];
          this.degreeData = data['under_graduate'];
          this.expYears = data['other_mst'].filter(obj=>obj.entity =='experiance_year');
          this.skill_data = data['skills'].map((obj)=> {return {display: obj.skill, value: obj.skill}});
          this.setForm();
      }
    });

  }

  setForm(){

    let trainerSkills = (this.user && this.user.user_detail)? this.user.user_detail.skills.split(','):[];
    if(trainerSkills.length){
              this.skill_key = trainerSkills.map((value)=>{
                return {value: value, display:value};
              });
            }
    this.storeMultiSelect.education_value = this.user.user_detail.education_id;
    this.storeMultiSelect.education_name = this.user.user_detail.education_name;
    let subValue =[];
    this.user.user_detail.education.map(obj=>{
        if(obj.child && obj.child.length){
        let hasChild =  obj.child.filter(value=> this.user.user_detail.education_stream.indexOf(value._id) != -1);
          if(hasChild){
            //console.log(hasChild);
            return hasChild.map(val=>( subValue.push({'parent_name' :val.parent_name, 'value': val._id})));
             
          }
        }
      });
    this.storeMultiSelect.education_sub_value = subValue;
    this.profileForm.patchValue({
      id: (this.user && this.user.user_detail)? this.user.user_detail._id:'',
      name:(this.user && this.user.user_detail)? this.user.user_detail.name:'',
      mid_name:(this.user && this.user.user_detail)? (this.user.user_detail.mid_name !='null'?this.user.user_detail.mid_name:''):'',
      family_name:(this.user && this.user.user_detail)? (this.user.user_detail.family_name !='null'? this.user.user_detail.family_name:''):'',
      email:(this.user)? this.user.email:'',
      phone_no:(this.user)? this.user.phone_no:'',
      skype_id:(this.user && this.user.user_detail)? this.user.user_detail.skype_id:'',
      gender:(this.user && this.user.user_detail)? this.user.user_detail.gender:'',
      date_of_birth:(this.user && this.user.user_detail)? this.user.user_detail.date_of_birth:'',
      country_id:(this.user && this.user.user_detail)? this.user.user_detail.country._id:'',
      city_id:(this.user && this.user.user_detail)? this.user.user_detail.city._id:'',
      industry_id:(this.user && this.user.user_detail)? this.user.user_detail.industry._id:'',
      address:(this.user && this.user.user_detail)? this.user.user_detail.address:'',
      current_designation:(this.user && this.user.user_detail)? this.user.user_detail.current_designation:'',
      //highest_education:(this.user && this.user.user_detail)? this.user.user_detail.highest_education:'',
      year_of_exp:(this.user && this.user.user_detail)? this.user.user_detail.year_of_exp:'',
      skills:(this.user && this.user.user_detail)? this.user.user_detail.skills:'',
      about_profile:(this.user && this.user.user_detail)? this.user.user_detail.about_profile:'',
      cv_path:(this.user && this.user.user_detail)? this.user.user_detail.cv_path:'',    
      identity_proof :(this.user && this.user.user_detail)? this.user.user_detail.identity_proof:'',
      name_account_holder :(this.user && this.user.user_detail)? this.user.user_detail.name_account_holder:'',
      bank_name:(this.user && this.user.user_detail)? this.user.user_detail.bank_name:'',
      account_no:(this.user && this.user.user_detail)? this.user.user_detail.account_no:'',
      swift_code:(this.user && this.user.user_detail)? this.user.user_detail.swift_code:'',
      ifsc_code:(this.user && this.user.user_detail)? this.user.user_detail.ifsc_code:'',
      bank_portal_address:(this.user && this.user.user_detail)? this.user.user_detail.bank_portal_address:'',
    });
  }

  updateImage(event){
    let file = event.target.files[0];
    let formData = new FormData();
    formData.append('profile_image',file);
    this.commonService.create('/api/trainer/profile-image/update' , formData)
        .subscribe(data=>{
          if(data.status ==200){
            localStorage.setItem('user', JSON.stringify(data.data));
            this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
            this.commonService.callProfileUpdate(data.data);
            this.message.success('Successfully update image');
          }
        },error=>{});
  }

  updateProfile(){
    this.validation_errors ='';
    let heighstEdu = [];
     if(this.storeMultiSelect.education_value){
       this.storeMultiSelect.education_value.forEach(value=>{
         heighstEdu =[...heighstEdu, value];
       });
       
     }
    if(this.storeMultiSelect.education_sub_value && this.storeMultiSelect.education_sub_value.length){
      this.storeMultiSelect.education_sub_value.forEach(obj=>{
        heighstEdu.push(obj.value);
      });      
    }
    this.profileForm.controls['highest_education'].setValue(heighstEdu);
    let formValue = this.changeSubmitValue();
    this.commonService.create('/api/trainer/profile-update', formValue)
        .subscribe(data=>{
          if(data.status ==200){
            localStorage.setItem('user', JSON.stringify(data.data));
            this.user = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';
            this.message.success('Successfully update image');
            this.isEdit = false;
          }else if(data.status == 422){
            this.validation_errors = data.error;
          }
          else if(data.status == 401){
          this.message.error(data.status_text);
        }
        },error=>{});
  }

  changeSubmitValue(){
    let formData = new FormData();
    let skills = this.skill_key ?this.skill_key.map(obj=>obj.value):'';
    this.profileForm.get('skills').setValue(skills);
    Object.keys(this.profileForm.controls).forEach(key => { 
      formData.append(key, this.profileForm.get(key).value);
    });
    return formData;
  }

  eventUpload(event, param){
    let file = event.target.files[0];
    console.log(file);
    if(param =='cv'){
       this.profileForm.get('cv_path').setValue(file); 
    }else if(param =='photo'){
      this.profileForm.get('profile_image').setValue(file); 
    }else if(param =='id_card'){
      this.profileForm.get('identity_proof').setValue(file); 
    }
  }

   getFileExtention(file){
     if(file && file !=''){
       return file.split('.').pop();
     }
    
  }

    getOnchangeEvent(type){
    if(type =='education'){
      this.educationDiv = true;
    }
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
        if (e.field_name == 'education') {
         this.storeMultiSelect.education_sub_value = this.storeMultiSelect.education_sub_value.filter(obj=> obj.parent_name !== String(e.name));
         /*this.registrationForm.patchValue({
          highest_education_stream: this.storeMultiSelect.education_sub_value
         });*/
        }
      }
    }
    if(e.field_name =='education'){
      this.profileForm.patchValue({
      highest_education: e.storedValue
      });
    }
    console.log(this.storeMultiSelect);
  }

    getSubChange(e){
    let event : any= e;
    if (e.event) {
      let index = e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index == -1) {
        e.storedSubValue.push({'parent_name' :e.parent_name, 'value': e.value});
      }
    } else {
      let index =  e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index != -1) {
        e.storedSubValue.splice(index, 1);
      }
    }  
    if (e.field_name == 'education') {
     /* this.registrationForm.patchValue({
        highest_education_stream: e.storedSubValue
      });*/
    }
  } 

  checkmultiselect(e){
    if(e.field_name =='education'){
      this.educationDiv = e.status;
    }

  }

  multiselectAll(e){  
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='education'){
      this.profileForm.patchValue({
      highest_education: e.storedValue
      });
    }
    
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "education":
      let nameUnd = this.storeMultiSelect.education_name[indx];
        this.storeMultiSelect.education_sub_value = this.storeMultiSelect.education_sub_value.filter(obj=> obj.parent_name !== String(nameUnd));         
         /* this.registrationForm.patchValue({
            highest_education_stream: this.storeMultiSelect.education_sub_value
          });*/
       this.storeMultiSelect.education_value.splice(indx, 1);
       this.storeMultiSelect.education_name.splice(indx, 1);
       this.profileForm.patchValue({
          highest_education: this.storeMultiSelect.education_value
       });
        break;   
      default:
        // code...
        break;
    }
  }

  getSubData(parent,type){
    if(type =='education'){
      if(this.storeMultiSelect.education_sub_value){
        let subData = this.storeMultiSelect.education_sub_value.filter(obj=>obj.parent_name == parent);
        return subData.length ? subData.length: '';        
      }else{
        return '';
      }

    }

  }


}
