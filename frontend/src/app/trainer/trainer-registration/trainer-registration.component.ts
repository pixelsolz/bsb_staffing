import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
declare var $: any;
declare var Swal: any;

@Component({
  selector: 'app-trainer-registration',
  templateUrl: './trainer-registration.component.html',
  styleUrls: ['./trainer-registration.component.css']
})
export class TrainerRegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  countries: any = [];
  cities: any = [];
  degress: any = [];
  show_sub_under: any=[];
  degreeData: any;
  industries: any = [];
  skill_data: any = [];
  skill_key: any;
  expYears: any = [];
  prevEighteenYearDate: any;
  exprectedDayModel: any = [];
  validation_errors: any = [];
  coutryWiseCity: any=[];
  trainer_cv:any;
  trainer_profile_image:any;
  trainer_identity_proof:any;
  storeMultiSelect: any={
    education_value:[],
    education_name:[],
    education_sub_value:[]
  };
  educationDiv: boolean =false;
  aboutconfig: any = {
    //height: '200px',
    placeholder: 'Few Words About Your Profile',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      //['insert', ['table', 'picture', 'link', 'video', 'hr']],
      //['customButtons', ['testBtn']]
    ],
   /* buttons: {
      'testBtn': this.customButton()
    }*/
  };

  constructor(private fb: FormBuilder, private commonService: CommonService,
    private messageService: MessageService, private router: Router, private title: Title,
    private meta: Meta) {
     let prevEighteenYear = (new Date().getFullYear()) - 18;
     let datePrev = new Date().getDate();
     let monthPrev = new Date().getMonth();
     this.prevEighteenYearDate= new Date( Number(monthPrev +1) + '-' + datePrev + '-' + prevEighteenYear);
  }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      name: ['', [Validators.required]],
      mid_name: ['', [Validators.required]],
      family_name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone_no: ['', [Validators.required]],
      skype_id: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      date_of_birth: ['', [Validators.required]],
      country_id: ['', [Validators.required]],
      city_id: ['', [Validators.required]],
      trainer_type: ['', [Validators.required]],
      address: ['', [Validators.required]],
      industry_id: ['', [Validators.required]],
      current_designation: ['', [Validators.required]],
      highest_education: ['', [Validators.required]],
      year_of_exp: ['', [Validators.required]],
      skills: ['', [Validators.required]],
      about_profile: ['', [Validators.required]],
      cv_path: ['', [Validators.required]],
      profile_image: ['', [Validators.required]],
      identity_proof: ['', [Validators.required]],
      name_account_holder: ['', [Validators.required]],
      bank_name: ['', [Validators.required]],
      account_no: ['', [Validators.required]],
      swift_code: ['', [Validators.required]],
      ifsc_code: ['', [Validators.required]],
      bank_portal_address: ['', [Validators.required]],
      expected_days: ['', [Validators.required]],
      from_time: ['', [Validators.required]],
      from_am_pm: ['', [Validators.required]],
      to_time: ['', [Validators.required]],
      to_am_pm: ['', [Validators.required]],
      declaration_user: [''],

    });

    this.registrationForm.get('country_id').valueChanges.subscribe(value=>{
      if(value){
        let country = this.countries.find(obj=>obj._id == value);
        this.coutryWiseCity = this.cities.filter(obj=>obj.country_code == country.code);
      }

    });
    this.asyncInit();
    this.title.setTitle('Trainer Registration Form | Login Form - bsbstaffing.com');
    this.meta.updateTag({ name: 'description', content: 'Are you Trainer? Create your Trainer account on bsbstaffing.com.Registered Now!' });
    this.meta.updateTag({ name: 'keywords', content: 'Trainer Registration Form' });
  }

  asyncInit() {
    this.commonService.getCommonData.subscribe(data=>{
      this.commonService.callFooterMenu(1);
      if(data && data.status ==200){
          this.countries = data['countries'];
          this.cities = data['cities'];
          this.degress = data['degrees'];
          this.industries = data['all_industries'];
          this.expYears = data['other_mst'].filter(obj=>obj.entity =='experiance_year');
          let underGraduate = data['under_graduate'];
          let postGraduate = data['post_graduate'];
          this.degreeData = data['under_graduate'];
          this.skill_data = data['skills'].map((obj)=> {return {display: obj.skill, value: obj.skill}});
      }
    });
  }

  submit(e) {
    e.preventDefault();
    this.validation_errors = '';
    
    let heighstEdu = [];
     if(this.storeMultiSelect.education_value){
       this.storeMultiSelect.education_value.forEach(value=>{
         heighstEdu =[...heighstEdu, value];
       });
       
     }
    if(this.storeMultiSelect.education_sub_value && this.storeMultiSelect.education_sub_value.length){
      this.storeMultiSelect.education_sub_value.forEach(obj=>{
        heighstEdu.push(obj.value);
      });      
    }
    this.registrationForm.controls['highest_education'].setValue(heighstEdu);

    let formValue = this.changeSubmitValue();
    this.commonService.create('/api/trainer-registration', formValue)
      .subscribe(data => {
            heighstEdu.length =0;
        if (data.status == 200) {
          this.registrationForm.reset();
          this.messageService.success(data.status_text);
          let timerInterval
          Swal.fire({
            title: 'Successfully Register',
            html: '<p>You have registered successfully.<br/> After verified from admin you can login to system by email and password.</p>' +
              'The window will close within <strong></strong> seconds.',
            timer: 15000,
            allowOutsideClick: false,
            onBeforeOpen: () => {
              Swal.showLoading()
              timerInterval = setInterval(() => {
                Swal.getContent().querySelector('strong')
                  .textContent = (Swal.getTimerLeft() / 1000)
                    .toFixed(0)
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval);
              this.router.navigate(['/trainer/login']);
            }
          });
        }
        else if (data.status === 422) {
          this.validation_errors = data.error;
        }
      }, error => {  heighstEdu.length =0; });
  }

  eventUpload(event, param) {
    let file = event.target.files[0];
    //console.log(file);
    if (param == 'cv') {
      this.registrationForm.get('cv_path').setValue(file);
      const reader = new FileReader();
      reader.onload = (event:any) => {
        this.trainer_cv = event.target.result;
      }
      reader.readAsDataURL(file);
    } else if (param == 'photo') {
      this.registrationForm.get('profile_image').setValue(file);
      const reader = new FileReader();
      reader.onload = (event:any) => {
        this.trainer_profile_image = event.target.result;
      }
      reader.readAsDataURL(file);
    } else if (param == 'id_card') {
      this.registrationForm.get('identity_proof').setValue(file);
      const reader = new FileReader();
      reader.onload = (event:any) => {
        this.trainer_identity_proof = event.target.result;
      }
      reader.readAsDataURL(file);
    }
  }
  removelogo(param){
    if(param == 'cv'){
    this.registrationForm.get('cv_path').setValue('');
    this.trainer_cv ='';
    }else if (param == 'photo') {
      this.registrationForm.get('profile_image').setValue('');
    } else if (param == 'id_card') {
      this.registrationForm.get('identity_proof').setValue('');
    }
  }

  expectedDayAdd(event) {
    console.log(event.target.checked);
    if (event.target.checked) {
      this.exprectedDayModel.push(event.target.value)
    } else {
      let index = this.exprectedDayModel.indexOf(event.target.value);
      this.exprectedDayModel.splice(index, 1);
    }
    this.registrationForm.get('expected_days').setValue(this.exprectedDayModel);
    console.log(this.exprectedDayModel);
  }

  changeSubmitValue() {
    var regex = /(<([^>]+)>)/ig
    let formData = new FormData();
    let skills = this.skill_key ? this.skill_key.map(obj => obj.value) : '';
    this.registrationForm.get('skills').setValue(skills);
    Object.keys(this.registrationForm.controls).forEach(key => {
      if(key == 'about_profile'){
        formData.append('about_profile_copy', this.registrationForm.get(key).value);
        formData.append(key, this.registrationForm.get(key).value.replace(regex,''));
      }else{
        formData.append(key, this.registrationForm.get(key).value);
      }
      
    });
    return formData;
  }

  getOnchangeEvent(type){
    if(type =='education'){
      this.educationDiv = true;
    }
  }

  getChange(e){
    if(e.event){
      let index = e.storedValue.indexOf(e.value);
      if(index ==-1){
        e.storedValue.push(e.value);
        e.storedName.push(e.name); 
      }
    }else{
      let index = e.storedValue.indexOf(e.value);
      if(index !=-1){
        e.storedValue.splice(index,1);
        e.storedName.splice(index,1);
        if (e.field_name == 'education') {
         this.storeMultiSelect.education_sub_value = this.storeMultiSelect.education_sub_value.filter(obj=> obj.parent_name !== String(e.name));
         /*this.registrationForm.patchValue({
          highest_education_stream: this.storeMultiSelect.education_sub_value
         });*/
        }
      }
    }
    if(e.field_name =='education'){
      this.registrationForm.patchValue({
      highest_education: e.storedValue
      });
    }
    console.log(this.storeMultiSelect);
  }

    getSubChange(e){
    let event : any= e;
    if (e.event) {
      let index = e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index == -1) {
        e.storedSubValue.push({'parent_name' :e.parent_name, 'value': e.value});
      }
    } else {
      let index =  e.storedSubValue.findIndex(obj=>obj.value == e.value);
      if (index != -1) {
        e.storedSubValue.splice(index, 1);
      }
    }  
    if (e.field_name == 'education') {
     /* this.registrationForm.patchValue({
        highest_education_stream: e.storedSubValue
      });*/
    }
  } 

  checkmultiselect(e){
    if(e.field_name =='education'){
      this.educationDiv = e.status;
    }

  }

  multiselectAll(e){  
    let filed =e.field_name;
    if(e.event){
      e.arrValue.forEach((value, i)=>{
        let index = e.storedValue.indexOf(value);
        if(index == -1){
          e.storedValue.push(value);
          e.storedName.push(e.arrName[i]);
        }       
      });
    }else{
      e.arrValue.forEach(value=>{
        let index = e.storedValue.indexOf(value);
        if(index !=-1){
          e.storedValue.splice(index,1);
          e.storedName.splice(index,1);
        }
      });    
    }
    if(filed =='education'){
      this.registrationForm.patchValue({
      highest_education: e.storedValue
      });
    }
    
  }

  removeMultiSelect(indx, type){
    switch (type) {
      case "education":
      let nameUnd = this.storeMultiSelect.education_name[indx];
        this.storeMultiSelect.education_sub_value = this.storeMultiSelect.education_sub_value.filter(obj=> obj.parent_name !== String(nameUnd));         
         /* this.registrationForm.patchValue({
            highest_education_stream: this.storeMultiSelect.education_sub_value
          });*/
       this.storeMultiSelect.education_value.splice(indx, 1);
       this.storeMultiSelect.education_name.splice(indx, 1);
       this.registrationForm.patchValue({
          highest_education: this.storeMultiSelect.education_value
       });
        break;   
      default:
        // code...
        break;
    }
  }

  getSubData(parent,type){
    if(type =='education'){
      if(this.storeMultiSelect.education_sub_value){
        let subData = this.storeMultiSelect.education_sub_value.filter(obj=>obj.parent_name == parent);
        return subData.length ? subData.length: '';        
      }else{
        return '';
      }

    }

  }

  resetForm() {

  }

}
