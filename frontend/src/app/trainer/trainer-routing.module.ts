import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { TrainerRegistrationComponent } from './trainer-registration/trainer-registration.component';
import { TrainerProfileComponent } from './trainer-profile/trainer-profile.component';
import { TrainerScheduleComponent } from './trainer-schedule/trainer-schedule.component';
import { TrainerHomeComponent } from './trainer-home/trainer-home.component';
import { TrainerBookingListComponent } from './trainer-booking-list/trainer-booking-list.component';
import { TrainerScheduleListComponent } from './trainer-schedule-list/trainer-schedule-list.component';

const routes: Routes = [
	{path:'registration',component: TrainerRegistrationComponent},
	{path:'', loadChildren:'../auth/auth.module#AuthModule'},
	{path:'profile', canActivate: [AuthGuard],component: TrainerProfileComponent},
	{path:'schedule', canActivate: [AuthGuard], component: TrainerScheduleComponent},
	{path:'home', canActivate: [AuthGuard], component: TrainerHomeComponent},
	{path:'booking-list', canActivate: [AuthGuard], component: TrainerBookingListComponent},
	{path:'schedule-list', canActivate: [AuthGuard], component: TrainerScheduleListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainerRoutingModule { }
