import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-trainer-schedule-list',
  templateUrl: './trainer-schedule-list.component.html',
  styleUrls: ['./trainer-schedule-list.component.css']
})
export class TrainerScheduleListComponent implements OnInit {

 scheduleType: string;
 allScheduleData: any=[];
 currentDate: any;
 selectedSchedule: any=[];
  constructor(private commonService: CommonService) { 
  	this.scheduleType = 'current';
  	this.currentDate = new Date().getTime();
  }

  ngOnInit() {
  	this.asyncInit();
  }

    asyncInit(){
  	this.commonService.getAll('/api/trainer/schedule/list')
  		.subscribe(data=>{
        this.commonService.callFooterMenu(1);
  			this.selectedSchedule = data.data.length ? data.data.sort((a, b) => (a.sc_timestamp > b.sc_timestamp) ? 1 : -1):[];
  			/*this.allScheduleData = data.data.map(obj=>({
  				_id: obj._id,
  				training_date: new Date(obj.training_year + '-' + (obj.training_month +1) + '-'+ obj.training_date).toLocaleDateString('en-GB'),
  				training_timeStamp: new Date(obj.training_year + '-' + (obj.training_month +1) + '-'+ obj.training_date).getTime(),
  				sc_year: obj.training_year,
  				sc_month: obj.training_month,
  				from_time: obj.from_time,
  				to_time: obj.to_time,
  				created_at:obj.created_at,
  				updated_at: obj.updated_at
  			}));
  			this.selectedSchedule = this.allScheduleData.filter(obj=>obj.training_timeStamp >=this.currentDate).sort((a, b) => (a.training_timeStamp > b.training_timeStamp) ? 1 : -1);*/
  		},error=>{});
  }

  handleType(type){
  	if(type =='prev'){
  		this.selectedSchedule = this.allScheduleData.filter(obj=>obj.training_timeStamp <this.currentDate).sort((a, b) => (a.training_timeStamp > b.training_timeStamp) ? 1 : -1);
  	}else{
  		this.selectedSchedule = this.allScheduleData.filter(obj=>obj.training_timeStamp >=this.currentDate).sort((a, b) => (a.training_timeStamp > b.training_timeStamp) ? 1 : -1);
  	}
  }

}
