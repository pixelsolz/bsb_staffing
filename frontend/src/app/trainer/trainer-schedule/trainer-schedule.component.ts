import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trainer-schedule',
  templateUrl: './trainer-schedule.component.html',
  styleUrls: ['./trainer-schedule.component.css']
})
export class TrainerScheduleComponent implements OnInit {

  showForm: boolean = false;
  currentMonth: any;
  currentYear: any;
  totalDaysInMonth: any;
  dayMonthArray: any = [];
  monthStart: any;
  totDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  totRows = Array(6).fill(1).map((x, y) => x + y);
  dateModel: any = {};
  selected_date: any=0;
  scheduleArray: FormArray;
  scheduleForm: FormGroup;
  scheduleData: any = [];
  savedScheduleDates: any = [];
  selectDatesArray: any=[];
  isSaved: boolean =false;
  userData: any={};
  allData: any={};
  use_role: string;
  allowPrevMonths: any=[];
  allowNxtMonths: any=[];
  paramYear: string;
  paramMonth: string;
  trainingCount: number;
  bookingCount: number;
  constructor(private fb: FormBuilder, private commonService: CommonService, private messageService: MessageService,
    private router: Router, private activateRoute: ActivatedRoute) {
    this.userData = localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')):'';    
    this.use_role = this.userData.user_role;             
    this.paramYear = this.activateRoute.snapshot.queryParams['sc_year'] ? this.activateRoute.snapshot.queryParams['sc_year']:'';
    this.paramMonth = this.activateRoute.snapshot.queryParams['sc_month'] ? this.activateRoute.snapshot.queryParams['sc_month']:'';

  }

  ngOnInit() {
    let d = new Date();
    for(let i= d.getMonth() +1; i<= d.getMonth() + 6; i++){
      if(i >=11){
        this.allowPrevMonths = [...this.allowPrevMonths, i -12];
        }else{
          this.allowPrevMonths = [...this.allowPrevMonths, i];
        }
      }
    for(let i= d.getMonth(); i< d.getMonth() + 6; i++){
      if(i >11){
        this.allowNxtMonths = [...this.allowNxtMonths, i -12];
        }else{
          this.allowNxtMonths = [...this.allowNxtMonths, i];
        }
      }

    this.scheduleForm = this.fb.group({
      scheduleArray:this.fb.array([this.createSchedule()])
    });


    /*this.scheduleForm.controls['scheduleArray'].valueChanges.subscribe((data)=>{
      let map_num = data.map((obj, i)=>{

        let tot = (Number(obj.point1) + Number(obj.point2) 
          + Number(obj.point3) + Number(obj.point4));
        return tot;
      });
    });*/

    this.setMonthData();
    this.commonService.getAll('/api/other-user/home')
  		.subscribe((data)=>{
        this.commonService.callFooterMenu(1);
  			this.allData = data.data;
        
  		},(error)=>{});

  }

  createSchedule(): FormGroup{
    return this.fb.group({
      id:'',
      training_year: [''],
      training_month: [''],
      training_date: [''],
      from_time: ['', [Validators.required]],
      to_time: ['', [Validators.required]]
    });
  }

  addScheduleArray(): void {
    this.scheduleArray = this.scheduleForm.get('scheduleArray') as FormArray;
    this.scheduleArray.push(this.createSchedule());
  }

  deleteScheduleArray(index: number){
    this.scheduleArray = this.scheduleForm.get('scheduleArray') as FormArray;
    this.scheduleArray.removeAt(index); 
  }

   get formData() { return <FormArray>this.scheduleForm.get('scheduleArray')['controls']; }

  asyncInit(month, year) {
    this.commonService.getAll('/api/trainer/schedule-get/' + month + '/' + year)
      .subscribe(data => {
        if (data.status === 200) {
          this.trainingCount = data.schedule_count;
          this.bookingCount = data.booked_count;
          this.scheduleData = data.data;
          this.savedScheduleDates = this.scheduleData.map(obj => obj.training_date);
        }
      }, error => { });
  }

  getdateNumber(i, num) {
    const index = (num + (i * 7) - this.monthStart);
    if (index >= 0) {
      return this.dayMonthArray[index];
    }
  }

  setMonthData() {
    const d = new Date();
    this.currentMonth = this.paramMonth ? Number(this.paramMonth):d.getMonth();
    this.currentYear = this.paramYear ? Number(this.paramYear) :d.getFullYear();
    this.totalDaysInMonth = new Date(this.currentYear, this.currentMonth +1, 0).getDate();
    this.dayMonthArray = Array(this.totalDaysInMonth).fill(1).map((x, y) => x + y);
    const objectData = new Date(this.monthNames[this.currentMonth] + '' + 1 + ',' + this.currentYear);
    this.monthStart = objectData.getDay();
    this.asyncInit(this.currentMonth, this.currentYear);
  }

  nextMonth(month) {
    let month_set = (month >=11)? month -12 : month;
    this.currentYear = (month >=11)? this.currentYear +1 : this.currentYear;
    this.showForm = false;
    this.selected_date = '';
    this.currentMonth = Number(month_set) + 1;
    this.totalDaysInMonth = new Date(this.currentYear, this.currentMonth +1, 0).getDate();
    this.dayMonthArray = Array(this.totalDaysInMonth).fill(1).map((x, y) => x + y);
    const objectData = new Date(this.monthNames[this.currentMonth] + '' + 1 + ',' + this.currentYear);
    this.monthStart = objectData.getDay();
    this.asyncInit(this.currentMonth, this.currentYear);
  }

  previousMonth(month) {
    let month_set = (month <=0)? 12 - month  : month;
    this.currentYear = (month <=0)? this.currentYear -1 : this.currentYear;
    this.showForm = false;
    this.selected_date = '';
    this.currentMonth = month_set - 1;
    this.totalDaysInMonth = new Date(this.currentYear, this.currentMonth +1, 0).getDate();
    this.dayMonthArray = Array(this.totalDaysInMonth).fill(1).map((x, y) => x + y);
    const objectData = new Date(this.monthNames[this.currentMonth] + '' + 1 + ',' + this.currentYear);
    this.monthStart = objectData.getDay();
    this.asyncInit(this.currentMonth, this.currentYear);
  }

  setSchedule(value, date) {
    const d = new Date();
    this.isSaved = false;
    this.scheduleForm.reset();
    this.scheduleForm.get('scheduleArray')['controls'][0].get('from_time').setValue('');
    this.scheduleForm.get('scheduleArray')['controls'][0].get('to_time').setValue('');
    this.clearFormArray(this.scheduleArray);
    if(this.currentMonth == d.getMonth() &&  this.currentYear == d.getFullYear() && date< d.getDate()){
      this.selected_date ='';
      this.showForm =false;
      this.dateModel[date] = false;
      return false;
    }
    const savedDate = this.scheduleData.filter(obj => obj.training_date === date);
    if (savedDate.length) {
      this.isSaved = true;
      this.scheduleArray = <FormArray> this.scheduleForm.get('scheduleArray'); 
      savedDate.forEach((schedule,i)=>{
        this.scheduleForm.get('scheduleArray')['controls'][i].get('id').setValue(schedule._id);
        this.scheduleForm.get('scheduleArray')['controls'][i].get('training_year').setValue(schedule.training_year);
        this.scheduleForm.get('scheduleArray')['controls'][i].get('training_month').setValue(schedule.training_month);
        this.scheduleForm.get('scheduleArray')['controls'][i].get('training_date').setValue(schedule.training_date);
        this.scheduleForm.get('scheduleArray')['controls'][i].get('from_time').setValue(schedule.from_time);
        this.scheduleForm.get('scheduleArray')['controls'][i].get('to_time').setValue(schedule.to_time);
        if((savedDate.length -i) !=1){
          this.scheduleArray.push(this.createSchedule());
        }
      });
    } else {
      this.isSaved = false;
      this.scheduleForm.patchValue({
        id: '',
        from_time: '',
        to_time: ''
      });
    }

    this.selected_date = date;
    this.showForm = true;
  }

  submitSchedule() {
    const length = this.scheduleForm.get('scheduleArray')['controls'].length;
    for(let i=0; i<length; i++){
      this.scheduleForm.get('scheduleArray')['controls'][i].get('training_year').setValue(this.currentYear);
      this.scheduleForm.get('scheduleArray')['controls'][i].get('training_month').setValue(this.currentMonth);
      this.scheduleForm.get('scheduleArray')['controls'][i].get('training_date').setValue(this.selected_date);
    }
    this.commonService.create('/api/trainer/schedule/create', this.scheduleForm.value)
      .subscribe(data => {
        if (data.status === 200) {
          this.messageService.success(data.status_text);
          this.scheduleData.push(data.data);
          this.savedScheduleDates = this.scheduleData.map(obj => obj.training_date);
          this.redirectTo(this.router.url);
        } else if (data.status === 422) {

        }
      }, error => { });
  }

  updateSchedule(){
    this.commonService.create('/api/trainer/schedule/update', this.scheduleForm.value)
      .subscribe(data => {
        if (data.status === 200) {
          this.messageService.success(data.status_text);
           this.redirectTo(this.router.url);
          //this.scheduleData.push(data.data);
          //this.savedScheduleDates = this.scheduleData.map(obj => obj.training_date);
        } else if (data.status === 422) {

        }
      }, error => { });
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

 clearFormArray = (formArray: FormArray) => {
   if(formArray && formArray.length){
      while (formArray.length !== 1) {
      formArray.removeAt(1)
    }
   }

  }


}
