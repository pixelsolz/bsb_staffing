import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import {AuthModule} from '../auth/auth.module';

import { TrainerRoutingModule } from './trainer-routing.module';
import { TrainerRegistrationComponent } from './trainer-registration/trainer-registration.component';
import { TrainerProfileComponent } from './trainer-profile/trainer-profile.component';
import { TrainerScheduleComponent } from './trainer-schedule/trainer-schedule.component';
import { TrainerHomeComponent } from './trainer-home/trainer-home.component';
import { TrainerBookingListComponent } from './trainer-booking-list/trainer-booking-list.component';
import { TrainerScheduleListComponent } from './trainer-schedule-list/trainer-schedule-list.component';

@NgModule({
  declarations: [TrainerRegistrationComponent, TrainerProfileComponent, TrainerScheduleComponent, TrainerHomeComponent, TrainerBookingListComponent, TrainerScheduleListComponent],
  imports: [
    CommonModule,
    TrainerRoutingModule,
    CoreModule,
    AuthModule,
  ]
})
export class TrainerModule { }
