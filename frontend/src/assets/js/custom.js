var Promise = window.Promise;
if (!Promise) {
    Promise = JSZip.external.Promise;
}


$(document).ready(function(){

 	$(document).on('click',".mobileQuick h3",function(){
	   $('.mobileQuick ul').toggle();
	});
	//mail open
	/*$(document).on('click',".mailrow",function(){
	    $(this).parents('.mailbox-holder').children('.mailbox-messages').hide();
	    $(this).parents('.mailbox-holder').children('.mail-content').show();

	    $(this).parents('.mailBlock').children('.box-header').hide();
	});*/

	/*$(document).on('click',".back",function(){ 
		$(this).parents('.mailbox-holder').children('.mailbox-messages').show();
	    $(this).parents('.mailbox-holder').children('.mail-content').hide();
	    $(this).parents('.mailBlock').children('.box-header').show();
	});*/
 
	//mail reply 
	$(document).on('click',".reply",function(){
	    $('.composemailhold').show(); 
	});

	$(document).on('click',".cancel",function(){
	    $(this).parents('.composemailhold').hide(); 
	});

	//slot
	$('.slotdiff').hide();
	 $(document).on('click',".slot",function(){
	    $(this).parents('.diffsec').slideUp(1000);
	    $('.slotdiff').show().slideDown(1000);
	  });

	 //benefit
	 $(".benefit").click(function(){ 
	    $('.benfitholder').slideToggle(1000);
	  });


	 //slotpanel remove
	  $(document).on('click',".slotcross",function(){
	    $(this).parents('.slotdiff').slideUp(1000).hide();
	    $('.normdiff').slideDown(1000);
	  });

	//edit
	$('.editable').hide();
	 $(document).on('click',".edit",function(){
	    $(this).parents('.profileInfoOuter').children('.noraml').slideUp(1000);
	    $(this).parents('.profileInfoOuter').children('.editable').show().slideDown(1000);
	  });
	 $(document).on('click',".cross",function(){
	 	$(this).parents('.profileInfoOuter').children('.noraml').show();
	    $(this).parents('.profileInfoOuter').children('.editable').hide();
	    $(this).parents('.profileInfoOuter').children('.addsecable').hide();
	  });

	//add
	 $('.addsecable').hide();
	 $(document).on('click',".addicon",function(){
	    $(this).parents('.profileInfoOuter').children('.noraml').slideUp(1000);
	    $(this).parents('.profileInfoOuter').children('.addsecable').show().slideDown(1000);
	  });
 	
	//delete
 	

 	//select
 	$('.slctdv').formSelect();

	  // Share job
	// $('.btnListing li').click(function(){
    // 	$('.btnListing li .share').toggle();
	// });
	$(document).on('click',"#btnListing1",function(){
		if(!window.Sharer.init()){
			window.Sharer.init();
		}
	    $('.btnListing li #share1').toggle();
	});
	$(document).on('click',"#btnListing2",function(){
		if(!window.Sharer.init()){
			window.Sharer.init();
		}
		$('.btnListing li #share2').toggle();
	});
	$(document).on('click',"#btnListing3 li",function(){
		if(!window.Sharer.init()){
			window.Sharer.init();
		}
		$('.btnListing li #share3').toggle();
	});  

//profiledrodown
	// $(".user").click(function(){
	//     $('.profiledorpdown').toggle(); 
	// });
	$(document).on('click',".user",function(){
		$('.profiledorpdown').toggle(); 
	}); 

 	//photo gallery 
 	/*$(".tabphotogallery").slick({
 	 	infinite: true, 
	    slidesToShow: 4,
	    slidesToScroll: 1
	});	*/

 	 //video gallery   
	var owl = $('.tabvideogallery');
      owl.owlCarousel({
        margin: 10,
        loop: true,
        nav:true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 2
          },
          1000: {
            items: 4
          }
        }
      })

	// viewbox
	/*$('image-link').fancybox({
		// Options will go here
	});
	$('image-link1').fancybox({
		// Options will go here
	});*/

	//smooth scroll
	var headerheight = $('header').height(); 

	$("a.ql").on('click', function(event) { 
    if (this.hash !== "") { 
      event.preventDefault(); 
      var hash = this.hash; 
      $('html, body').animate({
        scrollTop: $(hash).offset().top  -headerheight
      }, 800, function(){
    
       // window.location.hash = hash;
      });
    }  
  });

	// cycle-add-remove 
	$(".cycleico").click(function(){
	  $(this).parents().toggleClass("cycleon");
	});

	// Sticky Header
	$(window).scroll(function() {
	    stickyHeader();
	});

	function stickyHeader() {
	    if ($(this).scrollTop() > 1){
	        // if ($(window).width() > 767) {
	           $('header').addClass("sticky");
	        // }
	    } else {
	        $('header').removeClass("sticky");
	    }
	}


	/*setTimeout(function(){
		$('body').removeClass('home_loader')

	}, 6000);*/


	// Banner Slider
 

	/*$(".bannerSlider").slick({
	    dots: false, 
	    centerMode: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay:true,
		autoplaySpeed:3000,
		fade: true,
	});	
 
	$(".clientLogo").slick({
	    dots: true, 
	    centerMode: true,
	    slidesToShow: 5,
	    slidesToScroll: 1,
	    autoplay:true,
		autoplaySpeed:1500,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 374,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }    
		]
	});*/



    // Menu    
    /*$('.hmburger').click(function(){ 
		$('.menu-wrap').addClass( "menuadd" );
	});*/

	/*$(".menuClose").click(function(){
	    $('.menu-wrap').removeClass( "menuadd" );
	});*/


	// Gallery Slider
	/*$(".gallerySlider").slick({
	    dots: true, 
	    centerMode: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay:true,
		autoplaySpeed:1500,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 374,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }    
		]
	});*/

	
	$('.input-field').removeClass('activeIcon');
	$('.registrationFormInner input').focus(function(){
		$('.input-field').removeClass('activeIcon');
		$(this).parent('.input-field').addClass('activeIcon');
	});

	$('.registrationFormInner select').focus(function(){
		$('.input-field').removeClass('activeIcon');
		$(this).parent('.input-field').addClass('activeIcon');
	});

	$('.registrationFormInner textarea').focus(function(){
		$('.input-field').removeClass('activeIcon');
		$(this).parent('.input-field').addClass('activeIcon');
	});

	// Radio Check
	


	//wow
	// new WOW().init();

	$('.menuhold').click(function(){
		var sum = 0;
		
		$(".mobileMenu > li").each(function(li_len) {
			var time = .2;
			sum += +time;
	            $(this).attr("style", "transition-delay:" + sum +"s");
	      });
	});

	$(document).on('click','.menu-wrap ul.mobileMenu li.hasDropdown', function(){
		$(this).find('ul').toggle();
  		$(this).toggleClass('submenuOpened');
	});

  /*	$('.menu-wrap ul.mobileMenu li.hasDropdown').click(function(){
  		$(this).find('ul').toggle();
  		$(this).toggleClass('submenuOpened');
  	});*/


	$(document).on('click','.registerBtn', function(){
  		$('.regSection').removeClass('deactiveSection');
  		$('.regSection').addClass('activeSection');
  		$('.loginSection').removeClass('activeSection');
  		$('.loginSection').addClass('deactiveSection');
  		$(".loginmodalOuter").addClass('reginmodalOuter');
	});

	$(document).on('click','.loginBtn', function(){
  		$('.loginSection').removeClass('deactiveSection');
  		$('.loginSection').addClass('activeSection');
  		$('.regSection').removeClass('activeSection');
  		$('.regSection').addClass('deactiveSection');
  		$(".loginmodalOuter").removeClass('reginmodalOuter');
	});

	$(document).on('click','.forgotPass', function(){
	   $('.forgotSection').removeClass('deactiveSection');
	  	$('.forgotSection').addClass('activeSection');
	  	$('.loginSection').removeClass('activeSection');
	  	$('.loginSection').addClass('deactiveSection');
	});

	$(document).on('click','.bckloginBtn', function(){
		$('.forgotSection').removeClass('activeSection');
  		$('.loginSection').addClass('activeSection');
	});


  	$('.profileMenu > a').click(function(){
  		$('.profileMenu ul').toggle();
  	});

  	$('.multipleSelect').formSelect();


	//in-view-js
    function check_if_in_view() {
      var $animation_elements = $('.animation-element');
      var $window = $(window);
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }
  	$(document).ready(function(){
		var $animation_elements = $('.animation-element');
		var $window = $(window);

		$window.on('scroll resize', check_if_in_view);
		$window.trigger('scroll');
  	});

  	//saurabh_26.3.20
  	/*$('.registerBtn').click(function(){ 
  		$(".loginmodalOuter").addClass('reginmodalOuter');
  	});

  	$('.loginBtn').click(function(){
  		$(".loginmodalOuter").removeClass('reginmodalOuter');
  	});*/

  	/*$('.loadmoreOuter a').click(function(){
  		$('.loadmoreCity ').append('<div class="row"><div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="findjobContent"><a href="#"><div class="findjobOuter"><img src="images/cityImg1.png"><div class="findjobInner"><h4>India</h4><h5>3,107 Jobs Posted</h5></div><div class="findjobHover"><span></span><h4>India</h4><h5>3,107 Jobs Posted</h5></div></div></a></div></div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="findjobContent"><a href="#"><div class="findjobOuter"><img src="images/cityImg2.png"><div class="findjobInner"><h4>United kingdom</h4><h5>3,107 Jobs Posted</h5></div><div class="findjobHover"><span></span><h4>United kingdom</h4><h5>3,107 Jobs Posted</h5></div></div></a></div></div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="findjobContent"><a href="#"><div class="findjobOuter"><img src="images/cityImg3.png"><div class="findjobInner"><h4>Australia</h4><h5>3,107 Jobs Posted</h5></div><div class="findjobHover"><span></span><h4>Australia</h4><h5>3,107 Jobs Posted</h5></div></div></a></div></div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="findjobContent"><a href="#"><div class="findjobOuter"><img src="images/cityImg4.png"><div class="findjobInner"><h4>New Zealand</h4><h5>3,107 Jobs Posted</h5></div><div class="findjobHover"><span></span><h4>New Zealand</h4><h5>3,107 Jobs Posted</h5></div></div></a></div></div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="findjobContent"><a href="#"><div class="findjobOuter"><img src="images/cityImg5.png"><div class="findjobInner"><h4>CHINA</h4><h5>3,107 Jobs Posted</h5></div><div class="findjobHover"><span></span><h4>CHINA</h4><h5>3,107 Jobs Posted</h5></div></div></a></div></div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="findjobContent"><a href="#"><div class="findjobOuter"><img src="images/cityImg6.png"><div class="findjobInner"><h4>Hongkong </h4><h5>3,107 Jobs Posted</h5></div><div class="findjobHover"><span></span><h4>Hongkong </h4><h5>3,107 Jobs Posted</h5></div></div></a></div></div></div>');
  	});*/


  	/*$('.registerBtn').click(function(){
  		alert();
  		$('.regSection').removeClass('deactiveSection');
  		$('.regSection').addClass('activeSection');
  		$('.loginSection').removeClass('activeSection');
  		$('.loginSection').addClass('deactiveSection');
  	});

  	$('.loginBtn').click(function(){
  		$('.loginSection').removeClass('deactiveSection');
  		$('.loginSection').addClass('activeSection');
  		$('.regSection').removeClass('activeSection');
  		$('.regSection').addClass('deactiveSection');
  	});
  	
	$('.forgotPass').click(function(){
	  		$('.forgotSection').removeClass('deactiveSection');
	  		$('.forgotSection').addClass('activeSection');
	  		$('.loginSection').removeClass('activeSection');
	  		$('.loginSection').addClass('deactiveSection');
	  	});

  	$('.bckloginBtn').click(function(){
  		$('.forgotSection').removeClass('activeSection');
  		$('.loginSection').addClass('activeSection');
  	});*/



  	$('.moreOptionBtn').click(function(){
  		$('.filterSection').toggleClass('filterActive');
  	});

  	$('.exsistingJobEmail .checkboxOuter input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $('.exsistingJobEmail .prevDropdown').show();
            }
            else if($(this).prop("checked") == false){
                $('.exsistingJobEmail .prevDropdown').hide();
            }
    });

    $('#chooseDate1').datepicker();
    $('#formdate').datepicker();
  	$('#todate').datepicker();


  	/*$('.desktopIcon').click(function(){
  		$('body').addClass('noScroll');
  		$('.desktopVersionOuter').addClass('desktopVersionOpen');
  		$('.desktopVersionTop').addClass('desktopVersionTopOpen');
  	});

  	$('.desktopCross').click(function(){
  		$('body').removeClass('noScroll');
  		$('.desktopVersionOuter').removeClass('desktopVersionOpen');
  		$('.desktopVersionTop').removeClass('desktopVersionTopOpen');
  	});

  	$('.tabletIcon').click(function(){
  		$('body').addClass('noScroll');
  		$('.desktopVersionTop').addClass('desktopVersionTopOpen');
  		$('.tabletOuter').addClass('tabletOuterOpen');
  	});

  	$('.desktopCross').click(function(){
  		$('body').removeClass('noScroll');
  		$('.tabletOuter').removeClass('tabletOuterOpen');
  		$('.desktopVersionTop').removeClass('desktopVersionTopOpen');
  	});

  	$('.mobileIcon').click(function(){
  		$('.smartphone1').toggleClass('smartphone1Open');
  	});*/
	//Mobile menu
	$(document.body).append("<div class='menuClose'></div>");

	$(document).on('click',".hmburger",function(){
		$('.menu-wrap').addClass( "menuadd" );
		$('.menuClose').addClass('menuCloseoverlay');	
	});
	//   $('.hmburger').click(function(){ 
	// 	  $('.menu-wrap').addClass( "menuadd" );
	// 	  $('.menuClose').addClass('menuCloseoverlay');			
	//   });
	$(document).on('click',".menuClose",function(){
		$('.menu-wrap').removeClass( "menuadd" );
		$('.menuClose').removeClass('menuCloseoverlay');	
	});
	$(document).on('click',".menu-close",function(){
		$('.menu-wrap').removeClass( "menuadd" );
		$('.menuClose').removeClass('menuCloseoverlay');	
	});

	$(document).on('click',".mobileQuick h3",function(){
		$('.mobileQuick ul').show();	
	});

	$(document).on('click',".filterLeftOuter h4",function(){
		$('.filterContent').toggleClass('filteropen');
	});

	$(document).on('click',".mobilefilterTobar h4",function(){
		$('.filterContent').removeClass('filteropen');
	});

	$(document).on('change','input[type="checkbox"]',function(e){
		console.log(e);
		$('.filterContent').removeClass('filteropen');
	});
	
	//   $(".menuClose").click(function(){
	// 	  console.log('guyg');
	// 	  $('.menu-wrap').removeClass( "menuadd" );
	// 	  $('.menuClose').removeClass('menuCloseoverlay');
	//   }); 

	$(document).on('click',".mailpanelOpenBtn",function(){
		$('.maillist ul').toggle(); 
	}); 




});

 function urlToPromise(url) {
    return new Promise(function(resolve, reject) {
        JSZipUtils.getBinaryContent(url, function (err, data) {
            if(err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

$(document).on('click','.download_resume',function(){
  var zip = new JSZip();
   // find every checked item
    $(".resumechecked:checked").each(function () {
        var $this = $(this);
        var url = $this.data("url");
        console.log(url, '666')
        var filename = url.replace(/.*\//g, "");

        zip.file(filename, urlToPromise(url), {binary:true});
    });


    // when everything has been downloaded, we can trigger the dl
    zip.generateAsync({type:"blob"}, function updateCallback(metadata) {
        var msg = "progression : " + metadata.percent.toFixed(2) + " %";
        if(metadata.currentFile) {
            msg += ", current file = " + metadata.currentFile;
        }

    })
    .then(function callback(blob) {

        // see FileSaver.js
        saveAs(blob, "example.zip");

    }, function (e) {
        console.log(e)
    });

    return false;

})




