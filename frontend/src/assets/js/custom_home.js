
$(window).ready(function(){

    // Sticky Header
    $(window).scroll(function() {
        stickyHeader();
    });

    function stickyHeader() {
        if ($(this).scrollTop() > 1){
            // if ($(window).width() > 767) {
               $('body').addClass("sticky");
            // }
        } else {
            $('body').removeClass("sticky");
        }
    }

 
    $('.bannerslider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        smartSpeed: 2000,     
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 1,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 1
          }
        }
      ]
    });


     $("#slider").slick({
        slidesToShow: 5,
        rows: 3,
        draggable:true,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 0,
        speed: 7000,
        cssEase: "linear",
      });
      
       
 
   //testimonial slider
     $('.slider-for1').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 3000,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav',
      pauseOnHover:true,
      responsive: [
        {
          breakpoint: 768,
          settings: { 
            arrows:true,
            autoplay: false,
            autoplaySpeed: 4000,
            speed: 4000,
            
          }
        },
      ]
      
    });

    $('.slider-nav1').slick({ 
      slidesToShow: 3,
      slidesToScroll: 1, 
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 3000,
      asNavFor: '.slider-for1',
      arrows: false,
      dots: false,
      centerMode: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 768,
          settings: { 
            centerPadding: '0',
            slidesToShow: 1,
            autoplaySpeed: 1500,
            speed: 1500
          }
        },
        {
          breakpoint: 480,
          settings: { 
            centerPadding: '0',
            slidesToShow: 1,
            autoplaySpeed: 1500,
            speed: 1500
          }
        }
      ]
    });

    
//jobseeker testimonial
//slick slider
     $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      autoplaySpeed: 3000,
      speed: 3000,
      fade: true,
      pauseOnHover:true,
      asNavFor: '.slider-nav'
      
    });
    $('.slider-nav').slick({ 
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      asNavFor: '.slider-for',
      arrows: false,
      dots: false,
      centerMode: false,
      focusOnSelect: true
    });
//banner slider
// $('.footershareimg').owlCarousel({
//     loop:true,
//     margin:0,
//     autoplay:true,
//     autoplayTimeout:8000,
//     slideSpeed: 1,
//     responsiveClass:true,
//     autoplayHoverPause: false,
//     smartSpeed:50000,
//     responsive:{
//         0:{
//             items:1,
//             nav:true
//         },
//         600:{
//             items:1,
//             nav:false
//         },
//         1000:{
//             items:1,
//             nav:true,
//             loop:true
//         }
//     }
// })

 

});




//in-view-js
var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);
 
  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);
 
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');



//timing counter

/* var number =document.getElementById("demo").innerHTML;
 output = []; 

for (var i = 0, len = number.length; i < len; i += 1) {
    output.push('<span class="splt">'+number.charAt(i)+'</span>');
} 
$(".countno").html(output);



var count = 0;  

setInterval(function(){
  if (count == 10 ) {
        count = 0; 
    }
    $(".splt:first-child").html(count);
    count += 1;  
},9000) 


var count2 = 0; 
setInterval(function(){
  if (count2 == 10 ) {
        count2 = 0; 
    }
    $(".splt:nth-child(2)").html(count2);
    count2 += 1;    
},8000) 


var count3 = 0; 
setInterval(function(){
  if (count3 == 10 ) {
        count3 = 0; 
    }
    $(".splt:nth-child(3)").html(count3);
    count3 += 1;    
},5000)

var count4 = 0; 
setInterval(function(){
  if (count4 == 10 ) {
        count4 = 0; 
    }
    $(".splt:nth-child(4)").html(count4);
    count4 += 1;    
},3000)

var count5 = 0; 
setInterval(function(){
  if (count5 == 10 ) {
        count5 = 0; 
    }
    $(".splt:nth-child(5)").html(count5);
    count5 += 1;    
},2000)*/


  /*var odometer = new Odometer({ el: $('.countno')[0], value: 50000, theme: 'train-station' });
  odometer.render();
    let count = 1
   setInterval(function () {
          if(count == 1000){
            count =1;
          }
            count ++
              odometer.update((5000+count));
            }, 2000);*/
  

//circluar round
function moveToSelected(element) {

  if (element == "next") {
    var selected = $(".selected").next();
  } else if (element == "prev") {
    var selected = $(".selected").prev();
  } else {
    var selected = element;
  }

  var next = $(selected).next();
  var prev = $(selected).prev();
  var prevSecond = $(prev).prev();
  var nextSecond = $(next).next();

  $(selected).removeClass().addClass("selected");

  $(prev).removeClass().addClass("prev");
  $(next).removeClass().addClass("next");

  $(nextSecond).removeClass().addClass("nextRightSecond");
  $(prevSecond).removeClass().addClass("prevLeftSecond");

  $(nextSecond).nextAll().removeClass().addClass('hideRight');
  $(prevSecond).prevAll().removeClass().addClass('hideLeft');

}
 
$(document).keydown(function(e) {
    switch(e.which) {
        case 37:  
        moveToSelected('prev');
        break;

        case 39: 
        moveToSelected('next');
        break;

        default: return;
    }
    e.preventDefault();
});

$('#carousel div').click(function() {
  moveToSelected($(this));
});

$('#prev').click(function() {
  moveToSelected('prev');
});

$('#next').click(function() {
  moveToSelected('next');
});


/*$(document).ready(()=>{
   $('.roundlist li:nth-child(1)').attr('style','left: 46%; top: -8px;')
   $('.roundlist li:nth-child(2)').attr('style','right: 22px; top: 14%;')
   $('.roundlist li:nth-child(3)').attr('style','right: -7px; top: 54%;')
   $('.roundlist li:nth-child(4)').attr('style','right: 46px; top: 90%;')
   $('.roundlist li:nth-child(5)').attr('style','right: 169px; top: 93%;')
   $('.roundlist li:nth-child(6)').attr('style','left: 0px; top: 63%;')
   $('.roundlist li:nth-child(7)').attr('style','left: 17px; top: 17%;')

    $('.roundlist li:nth-child(1) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 
})*/

$('.abc').click(function() {
  var dataId = $(this).attr("data-id");
  $(".recruiter").hide();
  $("."+dataId).show();
  
 /* var existing_class = $(".roundlist").attr('class').split(' ').filter(item => item !== 'roundlist');

  existing_class.map(item => {
    $(".roundlist").removeClass(item);
  }); */
  console.log(dataId, '6666')
    //$(".roundlist").addClass("round"+dataId);
    //$('[data-id="'+ dataId +'"]').attr('style','transform:rotate("0deg")')
    //$('.roundlist li:nth-child(1)').removeAttr("style");
    /*setTimeout(()=>{
 if(dataId == 'recruiter7'){
      $('.roundlist li:nth-child(7)').attr('style','left: 46%; top: -8px;')
      $('.roundlist li:nth-child(1)').attr('style','right: 22px; top: 14%')
      $('.roundlist li:nth-child(2)').attr('style','right: -7px; top: 54%;')
      $('.roundlist li:nth-child(3)').attr('style','right: 46px; top: 90%;')
      $('.roundlist li:nth-child(4)').attr('style','right: 169px; top: 93%;')
      $('.roundlist li:nth-child(5)').attr('style','left: 0px; top: 63%;')
      $('.roundlist li:nth-child(6)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 

    }else if(dataId == 'recruiter6'){
      $('.roundlist li:nth-child(6)').attr('style','left: 46%; top: -8px;')
      $('.roundlist li:nth-child(7)').attr('style','right: 22px; top: 14%')
      $('.roundlist li:nth-child(1)').attr('style','right: -7px; top: 54%;')
      $('.roundlist li:nth-child(2)').attr('style','right: 46px; top: 90%;')
      $('.roundlist li:nth-child(3)').attr('style','right: 169px; top: 93%;')
      $('.roundlist li:nth-child(4)').attr('style','left: 0px; top: 63%;')
      $('.roundlist li:nth-child(5)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 

    }else if(dataId == 'recruiter5'){
      $('.roundlist li:nth-child(5)').attr('style','left: 46%; top: -8px;')
      $('.roundlist li:nth-child(6)').attr('style','right: 22px; top: 14%')
      $('.roundlist li:nth-child(7)').attr('style','right: -7px; top: 54%;')
      $('.roundlist li:nth-child(1)').attr('style','right: 46px; top: 90%;')
      $('.roundlist li:nth-child(2)').attr('style','right: 169px; top: 93%;')
      $('.roundlist li:nth-child(3)').attr('style','left: 0px; top: 63%;')
      $('.roundlist li:nth-child(4)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 

    }else if(dataId == 'recruiter4'){
      $('.roundlist li:nth-child(4)').attr('style','left: 46%; top: -8px;')
      $('.roundlist li:nth-child(5)').attr('style','right: 22px; top: 14%')
      $('.roundlist li:nth-child(6)').attr('style','right: -7px; top: 54%;')
      $('.roundlist li:nth-child(7)').attr('style','right: 46px; top: 90%;')
      $('.roundlist li:nth-child(1)').attr('style','right: 169px; top: 93%;')
      $('.roundlist li:nth-child(2)').attr('style','left: 0px; top: 63%;')
      $('.roundlist li:nth-child(3)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 

    }else if(dataId == 'recruiter3'){
      $('.roundlist li:nth-child(3)').attr('style','left: 46%; top: -8px;')
      $('.roundlist li:nth-child(4)').attr('style','right: 22px; top: 14%')
      $('.roundlist li:nth-child(5)').attr('style','right: -7px; top: 54%;')
      $('.roundlist li:nth-child(6)').attr('style','right: 46px; top: 90%;')
      $('.roundlist li:nth-child(7)').attr('style','right: 169px; top: 93%;')
      $('.roundlist li:nth-child(1)').attr('style','left: 0px; top: 63%;')
      $('.roundlist li:nth-child(2)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 

    }else if(dataId == 'recruiter2'){
      $('.roundlist li:nth-child(2)').attr('style','left: 46%; top: -8px;')
      $('.roundlist li:nth-child(3)').attr('style','right: 22px; top: 14%')
      $('.roundlist li:nth-child(4)').attr('style','right: -7px; top: 54%;')
      $('.roundlist li:nth-child(5)').attr('style','right: 46px; top: 90%;')
      $('.roundlist li:nth-child(6)').attr('style','right: 169px; top: 93%;')
      $('.roundlist li:nth-child(7)').attr('style','left: 0px; top: 63%;')
      $('.roundlist li:nth-child(1)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 

    }else if(dataId == 'recruiter1'){
       $('.roundlist li:nth-child(1)').attr('style','left: 46%; top: -8px;')
       $('.roundlist li:nth-child(2)').attr('style','right: 22px; top: 14%;')
       $('.roundlist li:nth-child(3)').attr('style','right: -7px; top: 54%;')
       $('.roundlist li:nth-child(4)').attr('style','right: 46px; top: 90%;')
       $('.roundlist li:nth-child(5)').attr('style','right: 169px; top: 93%;')
       $('.roundlist li:nth-child(6)').attr('style','left: 0px; top: 63%;')
       $('.roundlist li:nth-child(7)').attr('style','left: 17px; top: 17%;')

      $('.roundlist li:nth-child(1) span').attr('style','transform: translate(-25px,-30px) rotate(0deg);')
      $('.roundlist li:nth-child(2) span').attr('style','transform: translate(0px,-22px) rotate(48deg);')
      $('.roundlist li:nth-child(3) span').attr('style','transform: translate(3px,-5px) rotate(98deg);')
      $('.roundlist li:nth-child(4) span').attr('style','transform: translate(0px,18px) rotate(-19deg);')
      $('.roundlist li:nth-child(5) span').attr('style','transform: translate(-16px,18px) rotate(5deg);')
      $('.roundlist li:nth-child(6) span').attr('style','transform: translate(-40px,2px) rotate(-4deg);')
      $('.roundlist li:nth-child(7) span').attr('style','transform: translate(-76px,-9px) rotate(-4deg);') 
    }

    },500)*/
   

});



//range slider

//rangebar1
var slider = document.getElementById("myRange");
var output= document.getElementById("value");
var miles= document.querySelector(".miletext"); 

output.innerHTML=slider.value;

slider.oniput = function(){
  output.innerHTML=this.value;

}

slider.addEventListener("touchmove",function(){
    var x = parseInt((slider.value));
    let y  = x <10000 ? parseInt((slider.value)/250) +1 : parseInt((slider.value)/250);
    var color ='linear-gradient(90deg, rgb(252,0,0)'+ y + '%, rgb(232,232,232)' + y + '%)';
    slider.style.background=color;
    output.innerHTML=x;
    miles.style.left= y + "%";
})

slider.addEventListener("mousemove",function(){
    var x = parseInt((slider.value));
    let y  = x <10000 ? parseInt((slider.value)/250) +1 : parseInt((slider.value)/250);
    var color ='linear-gradient(90deg, rgb(252,0,0)'+ y + '%, rgb(232,232,232)' + y + '%)';
    slider.style.background=color;
    output.innerHTML=x;
    miles.style.left= y + "%";
})





//rangebar2
var slider1 = document.getElementById("myRange1");
var output1= document.getElementById("value1");
var miles1= document.querySelector(".miletext1"); 

output1.innerHTML=slider1.value;

slider1.oniput = function(){
  output1.innerHTML=this.value;

}

slider1.addEventListener("touchmove",function(){
    var y = parseInt((slider1.value));
    let z = y <10000 ? parseInt((slider.value)/250) +1 : parseInt((slider.value)/250);
    var color1 ='linear-gradient(90deg, rgb(252,0,0)'+ z + '%, rgb(232,232,232)' + z + '%)';
    slider1.style.background=color1;
    output1.innerHTML=y;
    miles1.style.left= z + "%";
})

slider1.addEventListener("mousemove",function(){
    var y = parseInt((slider1.value));
    console.log(y)
    let z = y <10000 ? parseInt((slider1.value)/250) +1 : parseInt((slider1.value)/250);
    var color1 ='linear-gradient(90deg, rgb(252,0,0)'+ z + '%, rgb(232,232,232)' + z + '%)';
    slider1.style.background=color1;
    output1.innerHTML=y;
    miles1.style.left= z + "%";
})

$(document).ready(()=>{
  $('.countno').each(function(index,element){
             let randomValue = Math.floor((Math.random() * 3500) + (1000 + index))            
             let count = 1
             let totNum = randomValue+count
             let totNumArr = Array.from(String(totNum), Number);
                 $(element).html(`<span class="split">${totNumArr[0]}</span><span class="split">${totNumArr[1]}</span>
                   <span class="split">${totNumArr[2]}</span><span class="split">${totNumArr[3]}</span>`)

              setInterval(function () {
              if(count == 1000){
                count =1;
              }
                count ++
                let totNum = randomValue+count
                let totNumArr = Array.from(String(totNum), Number);
                 $(element).html(`<span class="split">${totNumArr[0]}</span><span class="split">${totNumArr[1]}</span>
                   <span class="split">${totNumArr[2]}</span><span class="split">${totNumArr[3]}</span>`)
                }, (2000*60));
              })
         /*$('.countno').each(function(index,element){
             let randomValue = Math.floor((Math.random() * 3500) + (1000 + index))
              let odometer = new Odometer({ el: this, value: randomValue, theme: 'train-station' });
                odometer.render();
              let count = 1
          setInterval(function () {
          if(count == 1000){
            count =1;
          }
            count ++
              odometer.update((randomValue+count));
            }, 2000);
          })*/

   window.Sharer.init();
})

//odometer
// var qty = 1;

// setInterval(function(){
//     odometer.innerHTML = qty ++;
// }, 2000);
 // setTimeout(function(){
 //    $('.odometer').html(423234234);
 //  }, 1000);





