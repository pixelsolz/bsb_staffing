(function($){
    $.fn.pixelImageDigit = function(arrValue) {
        const digits = [
            {
                numeric: 0,
                html: `<span class="number zero"><span class="flag"><img src="assets/images/home-images/flag/singapore.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/skorea.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/sri.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/sweden.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/thailand.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/turkey.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/uganda.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/uk.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ind.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ukraine.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/usa.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/vietnam.png" alt=""/></span></span>`
            },
            {
                numeric: 1,
                html: `<span class="number one"> 
              <span class="flag">
                <img src="assets/images/home-images/flag/afg.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/arg.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/aus.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/bang.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/belgium.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/brazil.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/canada.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/canada.png" alt=""/>
              </span>
            </span>`
            },
            {
                numeric: 2,
                html: `<span class="number two"> <span class="flag"><img src="assets/images/home-images/flag/china.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/colombia.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/congo.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/denmark.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/egypt.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ethiop.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/france.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/gcc.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ger.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ind.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/france.png" alt=""/></span></span>`
            },
            {
                numeric: 3,
                html: `<span class="number three"><span class="flag"><img src="assets/images/home-images/flag/iran.png" alt=""/>
              </span><span class="flag"><img src="assets/images/home-images/flag/iraq.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/ire.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/isreal.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/itl.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/france.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/jordan.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/kenya.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/lebanon.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/malaysia.png" alt=""/></span></span>`
            },
            {
                numeric: 4,
                html: `<span class="number four"> <span class="flag"><img src="assets/images/home-images/flag/maldives.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/max.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/mororcco.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/myanmar.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/nigeria.png" alt=""/></span><span class="flag"> <img src="assets/images/home-images/flag/norway.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/nz.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/phillip.png" alt=""/></span><span class="flag"> <img src="assets/images/home-images/flag/poland.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/portugal.png" alt=""/></span></span>`
            },
            {
                numeric: 5,
                html: `<span class="number five"> <span class="flag"><img src="assets/images/home-images/flag/france.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/sau.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/france.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/skorea.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/sri.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/sweden.png" alt=""/></span><span class="flag"> <img src="assets/images/home-images/flag/thailand.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/turkey.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/uganda.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/uk.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ukraine.png" alt=""/></span></span>`
            },
             {
                numeric: 6,
                html: `<span class="number six"><span class="flag"><img src="assets/images/home-images/flag/usa.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/vietnam.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/afg.png" alt=""/></span> <span class="flag"><img src="assets/images/home-images/flag/arg.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/aus.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/bang.png" alt=""/> </span><span class="flag"> <img src="assets/images/home-images/flag/belgium.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/brazil.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/canada.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/china.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/colombia.png" alt=""/></span> <span class="flag"><img src="assets/images/home-images/flag/brazil.png" alt=""/></span></span>`
            }, 
            {
                numeric: 7,
                html: `<span class="number seven"><span class="flag"><img src="assets/images/home-images/flag/congo.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/denmark.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/egypt.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ethiop.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/france.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/gcc.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/ger.png" alt=""/></span></span>`
            }, 
            {
                numeric: 8,
                html: `<span class="number eight"><span class="flag"><img src="assets/images/home-images/flag/ind.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/indo.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/iran.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/iraq.png" alt=""/> </span><span class="flag"><img src="assets/images/home-images/flag/ire.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/isreal.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/itl.png" alt=""/></span> <span class="flag"><img src="assets/images/home-images/flag/iraq.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/jordan.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/kenya.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/lebanon.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/malaysia.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/maldives.png" alt=""/></span></span>`
            },
            {
                numeric: 9,
                html: `<span class="number nine"> 
              <span class="flag">
                <img src="assets/images/home-images/flag/max.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/mororcco.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/myanmar.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/nigeria.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/norway.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/nz.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/phillip.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/poland.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/portugal.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/russia.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/sau.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/flag/portugal.png" alt=""/>
              </span> 
            </span> `
            }, 
            {
                numeric: 10,
                html: `<span class="number plus"> <span class="flag"> <img src="assets/images/home-images/flag/afg.png" alt=""/></span>
              <span class="flag"><img src="assets/images/home-images/flag/arg.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/aus.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/bang.png" alt=""/></span><span class="flag"> <img src="assets/images/home-images/flag/belgium.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/brazil.png" alt=""/></span><span class="flag"><img src="assets/images/home-images/flag/canada.png" alt=""/></span></span>`
            },          
        ];

        
        this.each(function() {
            
            const $this = $(this);
            if(!$this.hasClass('number-holder')) {
                const text_array = arrValue;
                let output = '';
                let output_loop_index = 0;
                let output_appendable_classes = [];
                for(let item of text_array) {
                    let digit_html_index = digits.map(digit => digit.numeric.toString()).indexOf(item.toString());
                    if(digit_html_index > -1) {
                        if(output) output_appendable_classes.push(output_loop_index);
                        output += digits[digit_html_index].html;
                    }
                    output_loop_index++;
                }
                if(output) 
                $this.html(`<div class="number_holder">${output + digits[10].html}</div>` );
                if(output_appendable_classes && output_appendable_classes.length > 0) {
                    for (let item of output_appendable_classes) {
                        $this.children('.numeric-digit').eq(item).addClass('mrgn-left');
                    }
                }
            }
        });
    };

    $.fn.pixelImageDigitSecond = function(arrValue) {
        const digit_next =[
                 {
                numeric: 0,
                html: `<span class="number zero"> 
                            <span class="flag">
                                <img src="assets/images/home-images/man/man2.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man3.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man4.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man5.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man6.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man7.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man8.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man9.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man10.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man1.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man2.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man3.png" alt=""/>
                            </span>
                        </span>`
            },
            {
                numeric: 1,
                html: `<span class="number one"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man1.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man2.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man3.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man4.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man5.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man6.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man7.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man17.png" alt=""/>
              </span>
            </span>`
            },
            {
                numeric: 2,
                html: `<span class="number two"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man8.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man9.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man10.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man11.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man12.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man13.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man14.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man15.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man16.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man17.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man18.png" alt=""/>
              </span>
            </span>`
            },
            {
                numeric: 3,
                html: `<span class="number three"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man19.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man20.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man21.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man22.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man23.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man24.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man25.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man26.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man27.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man28.png" alt=""/>
              </span>
            </span>`
            },
            {
                numeric: 4,
                html: `<span class="number four"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man29.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man30.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man31.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man32.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man33.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man34.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man35.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man36.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man37.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man38.png" alt=""/>
              </span>
            </span>`
            },
            {
                numeric: 5,
                html: `<span class="number five"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man39.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man40.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man41.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man42.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man43.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man44.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man45.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man46.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man47.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man48.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man49.png" alt=""/>
              </span>
            </span>`
            },
             {
                numeric: 6,
                html: `<span class="number six"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man50.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man1.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man2.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man3.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man4.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man5.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man6.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man7.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man8.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man9.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man10.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man17.png" alt=""/>
              </span>    
            </span>`
            }, 
            {
                numeric: 7,
                html: `<span class="number seven"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man11.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man12.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man13.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man14.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man15.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man16.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man17.png" alt=""/>
              </span>
            </span>`
            }, 
            {
                numeric: 8,
                html: `<span class="number eight"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man18.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man19.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man20.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man21.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man22.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man23.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man24.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man25.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man26.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man27.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man28.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man29.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man30.png" alt=""/>
              </span>
            </span>`
            },
            {
                numeric: 9,
                html: `<span class="number nine"> 
                            <span class="flag">
                                <img src="assets/images/home-images/man/man1.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man2.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man3.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man4.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man5.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man6.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man7.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man8.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man9.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man10.png" alt=""/>
                            </span>
                            <span class="flag">
                                <img src="assets/images/home-images/man/man1.png" alt=""/>
                            </span> 
                             <span class="flag">
                                <img src="assets/images/home-images/man/man5.png" alt=""/>
                            </span>
                        </span>`
            }, 
            {
                numeric: 10,
                html: `<span class="number plus"> 
              <span class="flag">
                <img src="assets/images/home-images/man/man4.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man5.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man6.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man7.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man8.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man9.png" alt=""/>
              </span>
              <span class="flag">
                <img src="assets/images/home-images/man/man10.png" alt=""/>
              </span>
            </span>`
            },          
         ];

        
        this.each(function() {
            
            const $this = $(this);
            if(!$this.hasClass('number-holder')) {
                const text_array = arrValue;
                let output = '';
                let output_loop_index = 0;
                let output_appendable_classes = [];
                for(let item of text_array) {
                    let digit_html_index = digit_next.map(digit => digit.numeric.toString()).indexOf(item.toString());
                    if(digit_html_index > -1) {
                        if(output) output_appendable_classes.push(output_loop_index);
                        output += digit_next[digit_html_index].html;
                    }
                    output_loop_index++;
                }
                if(output) 
                $this.html(`<div class="number_holder">${output + digit_next[10].html}</div>` );
            }
        });
    };
    $(function(){
        if($('#myRange').val()){
            var rangeValue = ($('#myRange').val()) / 10;
            let arrValue = Array.from(String((rangeValue * 271.30)), Number)
            $(document).find('#firstNumber').pixelImageDigit(arrValue);
        }
         $(document).on('mousedown','#myRange',(e)=>{            
           $('.jobs_mile_sec.animation-element.in-view').removeClass('in-view');
        })
        $(document).on('mouseup','#myRange',(e)=>{   
          let rangeValue = ($('#myRange').val()) / 10;
          let arrValue = Array.from(String(rangeValue), Number)
          if(e.target.value > 200){
            rangeValue = 5426 + (Math.round((e.target.value  - 200) /10) *5)
            arrValue = Array.from(String(rangeValue), Number)
          }else{
            rangeValue = ($('#myRange').val()) / 10;
            arrValue = Array.from(String(Math.round((rangeValue * 271.30))), Number)
          }        
            
            setTimeout(()=>{
              $('.jobs_mile_sec.animation-element').addClass('in-view')
            $(document).find('#firstNumber').pixelImageDigit(arrValue);
            },1000)
            
        })
        $(document).on('touchmove','#myRange',(e)=>{            
           $('.jobs_mile_sec.animation-element.in-view').removeClass('in-view');
        })
        $(document).on('touchend','#myRange',(e)=>{            
          let rangeValue = ($('#myRange').val()) / 10;
          let arrValue = Array.from(String(rangeValue), Number)
          if(e.target.value > 200){
            rangeValue = 5426 + (Math.round((e.target.value  - 200) /10) *5)
            arrValue = Array.from(String(rangeValue), Number)
          }else{
            rangeValue = ($('#myRange').val()) / 10;
            arrValue = Array.from(String(Math.round((rangeValue * 271.30))), Number)
          }                    
            setTimeout(()=>{
              $('.jobs_mile_sec.animation-element').addClass('in-view')
            $(document).find('#firstNumber').pixelImageDigit(arrValue);
            },1000)

        })

        if($('#myRange1').val()){
            let arrValue = Array.from(String('206290'), Number)
            $(document).find('#secondNumber').pixelImageDigitSecond(arrValue);
        }
        $(document).on('mousedown','#myRange1',(e)=>{            
           $('.jobseeker_mile_sec.animation-element.in-view').removeClass('in-view');
        })
        $(document).on('mouseup','#myRange1',(e)=>{          
            let rangeValue = (e.target.value)*150.5;
            let arrValue = Array.from(String(rangeValue), Number)
            if(e.target.value > 200){
              rangeValue = 206290 + (Math.round((e.target.value  - 200)) *620)
              arrValue = Array.from(String(rangeValue), Number)
            }else{
              rangeValue = ($('#myRange1').val());
              arrValue = Array.from(String(Math.round((rangeValue * 1031.45))), Number)
            }  
            setTimeout(()=>{
              $('.jobseeker_mile_sec.animation-element').addClass('in-view')
              $(document).find('#secondNumber').pixelImageDigitSecond(arrValue);              
            },1000)
        })
       $(document).on('touchmove','#myRange1',(e)=>{            
           $('.jobseeker_mile_sec.animation-element.in-view').removeClass('in-view');
        })
        $(document).on('touchend','#myRange1',(e)=>{          
            let rangeValue = (e.target.value)*150.5;
            let arrValue = Array.from(String(rangeValue), Number)
            if(e.target.value > 200){
              rangeValue = 206290 + (Math.round((e.target.value  - 200)) *620)
              arrValue = Array.from(String(rangeValue), Number)
            }else{
              rangeValue = ($('#myRange1').val());
              arrValue = Array.from(String(Math.round((rangeValue * 1031.45))), Number)
            }  
            setTimeout(()=>{
              $('.jobseeker_mile_sec.animation-element').addClass('in-view')
              $(document).find('#secondNumber').pixelImageDigitSecond(arrValue);              
            },1000)
        })
       
    });
})(jQuery);